package com.gts.cms.converter;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.HashSet;
import java.util.Set;

@Converter
@Slf4j
public class HashSetLongToStringConverter implements AttributeConverter<Set<Long>, String> {

    @Override
    public String convertToDatabaseColumn(Set<Long> attribute) {
        if (attribute == null || attribute.isEmpty()) {
            return "";
        }

        Set<String> strings = new HashSet<>();
        for (Long att : attribute) {
            strings.add(Long.toString(att));
        }

        return StringUtils.join(strings, "|");
    }

    @Override
    public Set<Long> convertToEntityAttribute(String dbData) {
        if (dbData == null || dbData.trim().length() == 0) {
            return new HashSet<>();
        }

        String[] data = dbData.split("\\s*\\|\\s*");
        Set<Long> intSet = new HashSet<>();
        for (String d : data) {
            intSet.add(Long.valueOf(d));
        }

        return intSet;
    }
}
