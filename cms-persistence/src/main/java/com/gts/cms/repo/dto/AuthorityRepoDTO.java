package com.gts.cms.repo.dto;

import java.io.Serializable;

public class AuthorityRepoDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long rolePrivilageId;
	private String moduleCode;
	private String pageCode;
	private String subModuleCode;
	private Boolean canView;
	private Boolean canAdd;
	private Boolean canEdit;
	private Boolean canDelete;

	public AuthorityRepoDTO(Long rolePrivilageId, String moduleCode, String pageCode, String subModuleCode, Boolean canView,
                            Boolean canAdd, Boolean canEdit, Boolean canDelete) {
		super();
		this.rolePrivilageId = rolePrivilageId;
		this.moduleCode = moduleCode;
		this.pageCode = pageCode;
		this.subModuleCode = subModuleCode;
		this.canView = canView;
		this.canAdd = canAdd;
		this.canEdit = canEdit;
		this.canDelete = canDelete;
	}
	public AuthorityRepoDTO(Long rolePrivilageId, String pageCode, Boolean canView,
                            Boolean canAdd, Boolean canEdit, Boolean canDelete) {
		super();
		this.rolePrivilageId = rolePrivilageId;
		this.pageCode = pageCode;
		this.canView = canView;
		this.canAdd = canAdd;
		this.canEdit = canEdit;
		this.canDelete = canDelete;
	}

	public Long getRolePrivilageId() {
		return rolePrivilageId;
	}

	public void setRolePrivilageId(Long rolePrivilageId) {
		this.rolePrivilageId = rolePrivilageId;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getPageCode() {
		return pageCode;
	}

	public void setPageCode(String pageCode) {
		this.pageCode = pageCode;
	}

	public String getSubModuleCode() {
		return subModuleCode;
	}

	public void setSubModuleCode(String subModuleCode) {
		this.subModuleCode = subModuleCode;
	}

	public Boolean getCanView() {
		return canView;
	}

	public void setCanView(Boolean canView) {
		this.canView = canView;
	}

	public Boolean getCanAdd() {
		return canAdd;
	}

	public void setCanAdd(Boolean canAdd) {
		this.canAdd = canAdd;
	}

	public Boolean getCanEdit() {
		return canEdit;
	}

	public void setCanEdit(Boolean canEdit) {
		this.canEdit = canEdit;
	}

	public Boolean getCanDelete() {
		return canDelete;
	}

	public void setCanDelete(Boolean canDelete) {
		this.canDelete = canDelete;
	}

}
