package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
	


	/**
	 * The persistent class for the t_exam_stg_marks database table.
	 * 
	 */
	@Entity
	@Table(name = "t_exam_stg_marks")
	@NamedQuery(name = "ExamStgMarks.findAll", query = "SELECT s FROM ExamStgMarks s")
	public class ExamStgMarks implements Serializable {
		private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "pk_exam_stg_marks_id")
		private Long examStgMarksId;
		
		@Column(name = "school_name")
		private String schoolName;
		
	
		private String course;

		
		@Column(name = "course_year")
		private String courseYear;
		

		private String subject;
		
	
		private String exam;
		
		@Column(name="sno")
		private String sno;
		
		@Column(name = "roll_number")
		private String rollNumber;
		
		@Column(name = "student_name")
		private String studentName;
		
		
		@Column(name = "attendance_status")
		private Boolean attendanceStatus;

		@Column(name = "exam_marks")
		private Integer examMarks;
		
		@Temporal(TemporalType.TIMESTAMP)
		@Column(name = "created_dt")
		private Date createdDt;

		@Column(name = "created_user")
		private Long createdUser;

		public Long getExamStgMarksId() {
			return examStgMarksId;
		}

		public void setExamStgMarksId(Long examStgMarksId) {
			this.examStgMarksId = examStgMarksId;
		}

		public String getSchoolName() {
			return schoolName;
		}

		public void setSchoolName(String schoolName) {
			this.schoolName = schoolName;
		}

		public String getCourse() {
			return course;
		}

		public void setCourse(String course) {
			this.course = course;
		}



		public String getCourseYear() {
			return courseYear;
		}

		public void setCourseYear(String courseYear) {
			this.courseYear = courseYear;
		}

		public String getSubject() {
			return subject;
		}

		public void setSubject(String subject) {
			this.subject = subject;
		}

		public String getExam() {
			return exam;
		}

		public void setExam(String exam) {
			this.exam = exam;
		}

		public String getSno() {
			return sno;
		}

		public void setSno(String sno) {
			this.sno = sno;
		}

		public String getRollNumber() {
			return rollNumber;
		}

		public void setRollNumber(String rollNumber) {
			this.rollNumber = rollNumber;
		}

		public String getStudentName() {
			return studentName;
		}

		public void setStudentName(String studentName) {
			this.studentName = studentName;
		}

		public Boolean getAttendanceStatus() {
			return attendanceStatus;
		}

		public void setAttendanceStatus(Boolean attendanceStatus) {
			this.attendanceStatus = attendanceStatus;
		}

		public Integer getExamMarks() {
			return examMarks;
		}

		public void setExamMarks(Integer examMarks) {
			this.examMarks = examMarks;
		}

		public Date getCreatedDt() {
			return createdDt;
		}

		public void setCreatedDt(Date createdDt) {
			this.createdDt = createdDt;
		}

		public Long getCreatedUser() {
			return createdUser;
		}

		public void setCreatedUser(Long createdUser) {
			this.createdUser = createdUser;
		}

		

		

		
}
