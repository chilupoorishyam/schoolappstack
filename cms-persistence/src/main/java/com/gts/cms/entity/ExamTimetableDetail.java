package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_exam_timetable_details database table.
 * 
 */
@Entity
@Table(name = "t_exam_timetable_details")
@NamedQuery(name = "ExamTimetableDetail.findAll", query = "SELECT e FROM ExamTimetableDetail e")
public class ExamTimetableDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_timetable_det_id")
	private Long examTimetableDetId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to ExamTimetable
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_timetable_id")
	private ExamTimetable examTimetable;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;


	// bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_year_id")
	private CourseYear courseYear;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_examtype_catdet_id")
	private GeneralDetail examTypeCat;




	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_subject_id")
	private Subject subject;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_online_paper_id ")
	private ExamOnlinePaper examOnlinePaper;

	public ExamTimetableDetail() {
	}

	public Long getExamTimetableDetId() {
		return this.examTimetableDetId;
	}

	public void setExamTimetableDetId(Long examTimetableDetId) {
		this.examTimetableDetId = examTimetableDetId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}



	public CourseYear getCourseYear() {
		return courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public GeneralDetail getExamTypeCat() {
		return examTypeCat;
	}

	public void setExamTypeCat(GeneralDetail examTypeCat) {
		this.examTypeCat = examTypeCat;
	}


	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public ExamTimetable getExamTimetable() {
		return examTimetable;
	}

	public void setExamTimetable(ExamTimetable examTimetable) {
		this.examTimetable = examTimetable;
	}

	public ExamOnlinePaper getExamOnlinePaper() {
		return examOnlinePaper;
	}

	public void setExamOnlinePaper(ExamOnlinePaper examOnlinePaper) {
		this.examOnlinePaper = examOnlinePaper;
	}
}