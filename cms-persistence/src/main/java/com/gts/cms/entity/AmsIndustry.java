package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_ams_industry database table.
 * 
 */
@Entity
@Table(name = "t_ams_industry")
@NamedQuery(name = "AmsIndustry.findAll", query = "SELECT a FROM AmsIndustry a")
public class AmsIndustry implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_industry_id")
	private Long industryId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	@Column(name = "industry_code")
	private String industryCode;
	
	

	@Column(name = "industry_name")
	private String industryName;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

//	@Column(name = "groups")
//	private String group;

	private String tags;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to AmsProfileDetail
	@OneToMany(mappedBy = "amsIndustry")
	private List<AmsProfileDetail> amsProfileDetails;

	public AmsIndustry() {
	}

	public Long getIndustryId() {
		return this.industryId;
	}

	public void setIndustryId(Long industryId) {
		this.industryId = industryId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public String getIndustryCode() {
		return this.industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getIndustryName() {
		return this.industryName;
	}

	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getTags() {
		return this.tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<AmsProfileDetail> getAmsProfileDetails() {
		return this.amsProfileDetails;
	}

	public void setAmsProfileDetails(List<AmsProfileDetail> amsProfileDetails) {
		this.amsProfileDetails = amsProfileDetails;
	}

	public AmsProfileDetail addAmsProfileDetail(AmsProfileDetail amsProfileDetail) {
		getAmsProfileDetails().add(amsProfileDetail);
		amsProfileDetail.setAmsIndustry(this);

		return amsProfileDetail;
	}

	public AmsProfileDetail removeAmsProfileDetail(AmsProfileDetail amsProfileDetail) {
		getAmsProfileDetails().remove(amsProfileDetail);
		amsProfileDetail.setAmsIndustry(null);

		return amsProfileDetail;
	}

//	public String getGroup() {
//		return group;
//	}
//
//	public void setGroup(String group) {
//		this.group = group;
//	}
}