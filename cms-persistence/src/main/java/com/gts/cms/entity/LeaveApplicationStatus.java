package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_hr_leave_application_status database table.
 * 
 */
@Entity
@Table(name="t_hr_leave_application_status")
@NamedQuery(name="LeaveApplicationStatus.findAll", query="SELECT l FROM LeaveApplicationStatus l")
public class LeaveApplicationStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_leaveappliction_status_id")
	private Long leaveapplictionStatusId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="transaction_date")
	private Date transactionDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;
	
	@Column(name="leave_reason")
	private String leaveReason;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_or_manager_empid")
	private EmployeeDetail employeeOrManagerEmployeeDetail;

	//bi-directional many-to-one association to LeaveApplication
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_leave_appliction_id")
	private LeaveApplication leaveApplication;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_leaveprocess_status_from_catdet_id")
	private GeneralDetail leaveprocessStatusFrom;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_leaveprocess_status_to_catdet_id")
	private GeneralDetail leaveprocessStatusTo;

	public LeaveApplicationStatus() {
	}

	public Long getLeaveapplictionStatusId() {
		return this.leaveapplictionStatusId;
	}

	public void setLeaveapplictionStatusId(Long leaveapplictionStatusId) {
		this.leaveapplictionStatusId = leaveapplictionStatusId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getTransactionDate() {
		return this.transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getEmployeeOrManagerEmployeeDetail() {
		return this.employeeOrManagerEmployeeDetail;
	}

	public void setEmployeeOrManagerEmployeeDetail(EmployeeDetail employeeOrManagerEmployeeDetail) {
		this.employeeOrManagerEmployeeDetail = employeeOrManagerEmployeeDetail;
	}

	public LeaveApplication getLeaveApplication() {
		return this.leaveApplication;
	}

	public void setLeaveApplication(LeaveApplication leaveApplication) {
		this.leaveApplication = leaveApplication;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getLeaveprocessStatusFrom() {
		return this.leaveprocessStatusFrom;
	}

	public void setLeaveprocessStatusFrom(GeneralDetail leaveprocessStatusFrom) {
		this.leaveprocessStatusFrom = leaveprocessStatusFrom;
	}

	public GeneralDetail getLeaveprocessStatusTo() {
		return this.leaveprocessStatusTo;
	}

	public void setLeaveprocessStatusTo(GeneralDetail leaveprocessStatusTo) {
		this.leaveprocessStatusTo = leaveprocessStatusTo;
	}

	public String getLeaveReason() {
		return leaveReason;
	}

	public void setLeaveReason(String leaveReason) {
		this.leaveReason = leaveReason;
	}

}