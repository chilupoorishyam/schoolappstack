package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_ams_post_comments database table.
 * 
 */
@Entity
@Table(name="t_ams_post_comments")
@NamedQuery(name="AmsPostComment.findAll", query="SELECT a FROM AmsPostComment a")
public class AmsPostComment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_ams_post_comment_id")
	private Long amsPostCommentId;

	private String attachment1;

	private String attachment2;

	@Column(name="`COMMENT`")
	private String comment;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	//bi-directional many-to-one association to EmployeeDetail
		@ManyToOne(fetch = FetchType.LAZY)
		@JoinColumn(name="fk_ams_emp_id")
		private EmployeeDetail amsEmpId;


	@Column(name="fk_dislike_std_ids")
	private String fkDislikeStdIds;

	@Column(name="fk_likes_std_ids")
	private String fkLikesStdIds;

	//bi-directional many-to-one association to EmployeeDetail
			@ManyToOne(fetch = FetchType.LAZY)
			@JoinColumn(name="fk_verifiedby_emp_id")
			private EmployeeDetail verifiedbyEmpId;
			
	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_new_comment")
	private Boolean isNewComment;

	@Column(name="is_verifiedby_incharge")
	private Boolean isVerifiedbyIncharge;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	@Column(name="verification_comments")
	private String verificationComments;

	//bi-directional many-to-one association to AmsPost
	@ManyToOne
	@JoinColumn(name="fk_ams_post_id")
	private AmsPost amsPost;

	//bi-directional many-to-one association to AmsProfileDetail
	@ManyToOne
	@JoinColumn(name="fk_ams_profile_id")
	private AmsProfileDetail amsProfileDetail;

	//bi-directional many-to-one association to AmsPostComment
	@ManyToOne
	@JoinColumn(name="fk_reply_comment_id")
	private AmsPostComment amsPostComment;

	//bi-directional many-to-one association to AmsPostComment
	@OneToMany(mappedBy="amsPostComment")
	private List<AmsPostComment> amsPostComments;

	public AmsPostComment() {
	}

	public Long getAmsPostCommentId() {
		return this.amsPostCommentId;
	}

	public void setAmsPostCommentId(Long amsPostCommentId) {
		this.amsPostCommentId = amsPostCommentId;
	}

	public String getAttachment1() {
		return this.attachment1;
	}

	public void setAttachment1(String attachment1) {
		this.attachment1 = attachment1;
	}

	public String getAttachment2() {
		return this.attachment2;
	}

	public void setAttachment2(String attachment2) {
		this.attachment2 = attachment2;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	

	public String getFkDislikeStdIds() {
		return this.fkDislikeStdIds;
	}

	public void setFkDislikeStdIds(String fkDislikeStdIds) {
		this.fkDislikeStdIds = fkDislikeStdIds;
	}

	public String getFkLikesStdIds() {
		return this.fkLikesStdIds;
	}

	public void setFkLikesStdIds(String fkLikesStdIds) {
		this.fkLikesStdIds = fkLikesStdIds;
	}


	public EmployeeDetail getAmsEmpId() {
		return amsEmpId;
	}

	public void setAmsEmpId(EmployeeDetail amsEmpId) {
		this.amsEmpId = amsEmpId;
	}

	public EmployeeDetail getVerifiedbyEmpId() {
		return verifiedbyEmpId;
	}

	public void setVerifiedbyEmpId(EmployeeDetail verifiedbyEmpId) {
		this.verifiedbyEmpId = verifiedbyEmpId;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsNewComment() {
		return this.isNewComment;
	}

	public void setIsNewComment(Boolean isNewComment) {
		this.isNewComment = isNewComment;
	}

	public Boolean getIsVerifiedbyIncharge() {
		return this.isVerifiedbyIncharge;
	}

	public void setIsVerifiedbyIncharge(Boolean isVerifiedbyIncharge) {
		this.isVerifiedbyIncharge = isVerifiedbyIncharge;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getVerificationComments() {
		return this.verificationComments;
	}

	public void setVerificationComments(String verificationComments) {
		this.verificationComments = verificationComments;
	}

	public AmsPost getAmsPost() {
		return this.amsPost;
	}

	public void setAmsPost(AmsPost amsPost) {
		this.amsPost = amsPost;
	}

	public AmsProfileDetail getAmsProfileDetail() {
		return this.amsProfileDetail;
	}

	public void setAmsProfileDetail(AmsProfileDetail amsProfileDetail) {
		this.amsProfileDetail = amsProfileDetail;
	}

	public AmsPostComment getAmsPostComment() {
		return this.amsPostComment;
	}

	public void setAmsPostComment(AmsPostComment amsPostComment) {
		this.amsPostComment = amsPostComment;
	}

	public List<AmsPostComment> getAmsPostComments() {
		return this.amsPostComments;
	}

	public void setAmsPostComments(List<AmsPostComment> amsPostComments) {
		this.amsPostComments = amsPostComments;
	}

	public AmsPostComment addAmsPostComment(AmsPostComment amsPostComment) {
		getAmsPostComments().add(amsPostComment);
		amsPostComment.setAmsPostComment(this);

		return amsPostComment;
	}

	public AmsPostComment removeAmsPostComment(AmsPostComment amsPostComment) {
		getAmsPostComments().remove(amsPostComment);
		amsPostComment.setAmsPostComment(null);

		return amsPostComment;
	}

}