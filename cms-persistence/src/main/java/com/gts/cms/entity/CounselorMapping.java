package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_cm_std_counselor_mapping database table.
 * 
 */
@Entity
@Table(name="t_cm_std_counselor_mapping")
@NamedQuery(name="CounselorMapping.findAll", query="SELECT c FROM CounselorMapping c")
public class CounselorMapping implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_std_counselor_id")
	private Long counselorId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	@Column(name="from_date")
	private Date fromDate;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.DATE)
	@Column(name="to_date")
	private Date toDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to CounselorActivity
	@OneToMany(mappedBy="counselorMapping")
	private List<CounselorActivity> counselorActivities;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne
	@JoinColumn(name="fk_emp_id")
	private EmployeeDetail employeeDetail;

	//bi-directional many-to-one association to School
	@ManyToOne
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to StudentDetail
	@ManyToOne
	@JoinColumn(name="fk_student_id")
	private StudentDetail studentDetail;
	
	//bi-directional many-to-one association to StudentDetail
	@ManyToOne
	@JoinColumn(name="fk_group_section_id")
	private GroupSection groupSection;

	public CounselorMapping() {
	}

	public Long getCounselorId() {
		return this.counselorId;
	}

	public void setCounselorId(Long counselorId) {
		this.counselorId = counselorId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<CounselorActivity> getCounselorActivities() {
		return this.counselorActivities;
	}

	public void setCounselorActivities(List<CounselorActivity> counselorActivities) {
		this.counselorActivities = counselorActivities;
	}

	public CounselorActivity addCounselorActivity(CounselorActivity counselorActivity) {
		getCounselorActivities().add(counselorActivity);
		counselorActivity.setCounselorMapping(this);

		return counselorActivity;
	}

	public CounselorActivity removeCounselorActivity(CounselorActivity counselorActivity) {
		getCounselorActivities().remove(counselorActivity);
		counselorActivity.setCounselorMapping(null);

		return counselorActivity;
	}

	public EmployeeDetail getEmployeeDetail() {
		return this.employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public GroupSection getGroupSection() {
		return groupSection;
	}

	public void setGroupSection(GroupSection groupSection) {
		this.groupSection = groupSection;
	}

}