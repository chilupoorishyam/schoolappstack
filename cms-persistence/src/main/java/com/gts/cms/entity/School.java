package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_schools database table.
 * 
 */
@Entity
@Table(name="t_m_schools")
@NamedQuery(name="School.findAll", query="SELECT c FROM School c")
public class School implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_school_id")
	private Long schoolId;

	private String address;

	@Column(name="approved_by")
	private String approvedBy;

	@Column(name="school_code")
	private String schoolCode;

	@Column(name="school_name")
	private String schoolName;

	@Column(name="school_shortname")
	private String schoolShortname;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String email;

	@Column(name="facebook_url")
	private String facebookUrl;

	private String fax;

	@Column(name="google_url")
	private String googleUrl;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="landline_number")
	private String landlineNumber;

	@Column(name="linkedin_url")
	private String linkedinUrl;

	@Column(name="logo_filename")
	private String logoFilename;

	private String mandal;

	@Column(name="mobile_number")
	private String mobileNumber;

	private String pincode;

	@Column(name="print_prefix")
	private String printPrefix;

	private String reason;

	private String reportline1;

	private String reportline2;

	private String reportline3;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to BatchwiseStudent
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<BatchwiseStudent> batchwiseStudents;

	//bi-directional many-to-one association to ElectiveGroupyrMapping
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<ElectiveGroupyrMapping> electiveGroupyrMappings;

	//bi-directional many-to-one association to GroupyrRegulationDetail
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<GroupyrRegulationDetail> groupyrRegulationDetails;

	//bi-directional many-to-one association to StaffCourseyrSubject
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<StaffCourseyrSubject> staffCourseyrSubjects;

	//bi-directional many-to-one association to StaffSyllabusPlan
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<StaffSyllabusPlan> staffSyllabusPlans;

	//bi-directional many-to-one association to SubjectBook
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<SubjectBook> subjectBooks;

	//bi-directional many-to-one association to SubjectCourseyear
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<SubjectCourseyear> subjectCourseyears;

	//bi-directional many-to-one association to SubjectUnitTopic
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<SubjectUnitTopic> subjectUnitTopics;

	//bi-directional many-to-one association to SubjectUnit
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<SubjectUnit> subjectUnits;

	//bi-directional many-to-one association to Subjectregulation
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<Subjectregulation> subjectregulations;

	//bi-directional many-to-one association to EmployeeAttendance
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<EmployeeAttendance> empAttendances;

	//bi-directional many-to-one association to EmployeeDailylog
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<EmployeeDailylog> empDailylogs;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails;

	//bi-directional many-to-one association to EmployeeStgDailylog
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<EmployeeStgDailylog> empStgDailylogs;

	//bi-directional many-to-one association to FeeCategory
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeCategory> feeCategories;

	//bi-directional many-to-one association to FeeDiscountApplyFor
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeDiscountApplyFor> feeDiscountApplyFors;

	//bi-directional many-to-one association to FeeDiscount
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeDiscount> feeDiscounts;

	//bi-directional many-to-one association to FeeDiscountsParticular
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeDiscountsParticular> feeDiscountsParticulars;

	//bi-directional many-to-one association to FeeFine
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeFine> feeFines;

	//bi-directional many-to-one association to FeeFineSlab
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeFineSlab> feeFineSlabs;

	//bi-directional many-to-one association to FeeInstantCategory
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeInstantCategory> feeInstantCategories;

	//bi-directional many-to-one association to FeeInstantParticular
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeInstantParticular> feeInstantParticulars;

	//bi-directional many-to-one association to FeeInstantPaymentParticular
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeInstantPaymentParticular> feeInstantPaymentParticulars;

	//bi-directional many-to-one association to FeeInstantPayment
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeInstantPayment> feeInstantPayments;

	//bi-directional many-to-one association to FeeParticular
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeParticular> feeParticulars;

	//bi-directional many-to-one association to FeeParticularwisePayment
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeParticularwisePayment> feeParticularwisePayments;

	//bi-directional many-to-one association to FeeReceipt
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeReceipt> feeReceipts;

	//bi-directional many-to-one association to FeeRefundRule
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeRefundRule> feeRefundRules;

	//bi-directional many-to-one association to FeeStructure
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeStructure> feeStructures;

	//bi-directional many-to-one association to FeeStructureCourseyr
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeStructureCourseyr> feeStructureCourseyrs;

	//bi-directional many-to-one association to FeeStructureParticular
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeStructureParticular> feeStructureParticulars;

	//bi-directional many-to-one association to FeeStudentData
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeStudentData> feeStudentData;

	//bi-directional many-to-one association to FeeStudentDataDetail
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeStudentDataDetail> studentDataDetails;

	//bi-directional many-to-one association to FeeStudentRefund
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeStudentRefund> feeStudentRefunds;

	//bi-directional many-to-one association to FeeStudentWiseParticular
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeStudentWiseParticular> feeStudentWiseParticulars;

	//bi-directional many-to-one association to FeeStudentwiseDiscount
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeStudentwiseDiscount> feeStudentwiseDiscounts;

	//bi-directional many-to-one association to FeeStudentwiseFine
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeStudentwiseFine> feeStudentwiseFines;

	//bi-directional many-to-one association to FeeTransactionDetail
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeTransactionDetail> feeTransactionDetails;

	//bi-directional many-to-one association to FeeTransactionMaster
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeeTransactionMaster> feeTransactionMasters;

	//bi-directional many-to-one association to FinAccountType
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FinAccountType> finAccountTypes;

	//bi-directional many-to-one association to FinCategory
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FinCategory> finCategories;

	//bi-directional many-to-one association to FinSubCategory
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FinSubCategory> finSubCategories;

	//bi-directional many-to-one association to FinTransaction
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FinTransaction> finTransactions;

	//bi-directional many-to-one association to FinTransactionHistory
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FinTransactionHistory> finTransactionHistories;

	//bi-directional many-to-one association to EmployeePayrollGroup
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<EmployeePayrollGroup> employeePayrollGroups;

	//bi-directional many-to-one association to EmployeePayslipDetail
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<EmployeePayslipDetail> employeePayslipDetails;

	//bi-directional many-to-one association to EmployeePayslipGeneration
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<EmployeePayslipGeneration> employeePayslipGenerations;

	//bi-directional many-to-one association to EmployeePayslipGenerationStatus
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<EmployeePayslipGenerationStatus> employeePayslipGenerationStatuses;

	//bi-directional many-to-one association to EmployeeSalaryStructure
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<EmployeeSalaryStructure> employeeSalaryStructures;

	//bi-directional many-to-one association to FbOptionGroup
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FbOptionGroup> fbOptionGroups;

	//bi-directional many-to-one association to FbOptionchoice
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FbOptionchoice> fbOptionchoices;

	//bi-directional many-to-one association to FeedbackQuestion
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeedbackQuestion> feedbackQuestions;

	//bi-directional many-to-one association to FeedbackSection
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FeedbackSection> feedbackSections;

	//bi-directional many-to-one association to PayrollCategory
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<PayrollCategory> payrollCategories;

	//bi-directional many-to-one association to PayrollCategoryGroup
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<PayrollCategoryGroup> payrollCategoryGroups;

	//bi-directional many-to-one association to PayrollGroup
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<PayrollGroup> payrollGroups;

	//bi-directional many-to-one association to PayslipBranchSetting
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<PayslipBranchSetting> payslipBranchSettings;

	//bi-directional many-to-one association to PayslipSetting
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<PayslipSetting> payslipSettings;

	//bi-directional many-to-one association to SurveyDetail
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<SurveyDetail> surveyDetails;

	//bi-directional many-to-one association to SurveyFeedback
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<SurveyFeedback> surveyFeedbacks;

	//bi-directional many-to-one association to SurveyFeedbackDetail
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<SurveyFeedbackDetail> surveyFeedbackDetails;

	//bi-directional many-to-one association to SurveyForm
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<SurveyForm> surveyForms;

	//bi-directional many-to-one association to LibraryDetail
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<LibraryDetail> libraryDetails;

	//bi-directional many-to-one association to AcademicYear
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<AcademicYear> academicYears;

	//bi-directional many-to-one association to Batch
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<Batch> batches;

	//bi-directional many-to-one association to Campus
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_campus_id")
	private Campus campus;

	//bi-directional many-to-one association to City
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_city_id")
	private City city;

	//bi-directional many-to-one association to District
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_district_id")
	private District district;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_affiliatedto_catdet_id")
	private GeneralDetail affiliatedto;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_schooltype_catdet_id")
	private GeneralDetail schooltype;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to ConfigAutonumber
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<ConfigAutonumber> configAutonumbers;

	//bi-directional many-to-one association to Course
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<Course> courses;


	//bi-directional many-to-one association to CourseYear
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<CourseYear> courseYears;

	//bi-directional many-to-one association to Department
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<Department> departments;

	//bi-directional many-to-one association to Electivetype
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<Electivetype> electivetypes;

	//bi-directional many-to-one association to EventAudience
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<EventAudience> eventAudiences;

	//bi-directional many-to-one association to EventType
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<EventType> eventTypes;

	//bi-directional many-to-one association to Event
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<Event> events;

	//bi-directional many-to-one association to FinancialYear
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<FinancialYear> financialYears;

	//bi-directional many-to-one association to GeneralSetting
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<GeneralSetting> generalSettings;

	//bi-directional many-to-one association to Messaging
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<Messaging> messagings;

	//bi-directional many-to-one association to MessagingRecipient
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<MessagingRecipient> messagingRecipients;

	//bi-directional many-to-one association to NotificationAudience
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<NotificationAudience> notificationAudiences;

	//bi-directional many-to-one association to Notification
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<Notification> notifications;

	//bi-directional many-to-one association to Qualification
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<Qualification> qualifications;


	//bi-directional many-to-one association to Studentbatch
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<Studentbatch> studentbatches;

	//bi-directional many-to-one association to Subject
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<Subject> subjects;

	//bi-directional many-to-one association to User
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<User> users;

	//bi-directional many-to-one association to WorkflowStage
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<WorkflowStage> workflowStages;

	//bi-directional many-to-one association to RolePrivilege
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<RolePrivilege> rolePrivileges;

	//bi-directional many-to-one association to Role
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<Role> roles;

	//bi-directional many-to-one association to UserSchoolsGroup
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<UserSchoolsGroup> userSchoolsGroups;

	//bi-directional many-to-one association to SmsPattern
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<SmsPattern> smsPatterns;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications;

	//bi-directional many-to-one association to StudentDailylog
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<StudentDailylog> stdDailylogs;

	//bi-directional many-to-one association to StudentEnquiry
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<StudentEnquiry> stdEnquiries;

	//bi-directional many-to-one association to StudentStgDailylog
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<StudentStgDailylog> stdStgDailylogs;

	//bi-directional many-to-one association to StudentAcademicbatch
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<StudentAcademicbatch> studentAcademicbatches;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails;

	//bi-directional many-to-one association to StudentSubject
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<StudentSubject> studentSubjects;

	//bi-directional many-to-one association to TransportDetail
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<TransportDetail> transportDetails;

	//bi-directional many-to-one association to ActualClassesSchedule
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<ActualClassesSchedule> actualClassesSchedules;

	//bi-directional many-to-one association to ClassTiming
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<ClassTiming> classTimings;

	//bi-directional many-to-one association to ClassWeekday
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<ClassWeekday> classWeekdays;

	//bi-directional many-to-one association to Lessonstatus
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<Lessonstatus> lessonstatuses;

	//bi-directional many-to-one association to Schedule
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<Schedule> schedules;

	//bi-directional many-to-one association to StaffProxy
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<StaffProxy> staffProxies;

	//bi-directional many-to-one association to StudentAttendance
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<StudentAttendance> stdAttendances;

	//bi-directional many-to-one association to StudentAttendanceTimeSetting
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<StudentAttendanceTimeSetting> stdAttendanceTimeSettings;

	//bi-directional many-to-one association to SubjectResource
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<SubjectResource> subjectResources;

	//bi-directional many-to-one association to Timetable
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<Timetable> timetables;

	//bi-directional many-to-one association to Timingset
	@OneToMany(mappedBy="school",fetch = FetchType.LAZY)
	private List<Timingset> timingsets;

	public School() {
	}

	public Long getSchoolId() {
		return this.schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getApprovedBy() {
		return this.approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String getSchoolCode() {
		return this.schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getSchoolName() {
		return this.schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolShortname() {
		return this.schoolShortname;
	}

	public void setSchoolShortname(String schoolShortname) {
		this.schoolShortname = schoolShortname;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFacebookUrl() {
		return this.facebookUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getGoogleUrl() {
		return this.googleUrl;
	}

	public void setGoogleUrl(String googleUrl) {
		this.googleUrl = googleUrl;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getLandlineNumber() {
		return this.landlineNumber;
	}

	public void setLandlineNumber(String landlineNumber) {
		this.landlineNumber = landlineNumber;
	}

	public String getLinkedinUrl() {
		return this.linkedinUrl;
	}

	public void setLinkedinUrl(String linkedinUrl) {
		this.linkedinUrl = linkedinUrl;
	}

	public String getLogoFilename() {
		return this.logoFilename;
	}

	public void setLogoFilename(String logoFilename) {
		this.logoFilename = logoFilename;
	}

	public String getMandal() {
		return this.mandal;
	}

	public void setMandal(String mandal) {
		this.mandal = mandal;
	}

	public String getMobileNumber() {
		return this.mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPincode() {
		return this.pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getPrintPrefix() {
		return this.printPrefix;
	}

	public void setPrintPrefix(String printPrefix) {
		this.printPrefix = printPrefix;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReportline1() {
		return this.reportline1;
	}

	public void setReportline1(String reportline1) {
		this.reportline1 = reportline1;
	}

	public String getReportline2() {
		return this.reportline2;
	}

	public void setReportline2(String reportline2) {
		this.reportline2 = reportline2;
	}

	public String getReportline3() {
		return this.reportline3;
	}

	public void setReportline3(String reportline3) {
		this.reportline3 = reportline3;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<BatchwiseStudent> getBatchwiseStudents() {
		return this.batchwiseStudents;
	}

	public void setBatchwiseStudents(List<BatchwiseStudent> batchwiseStudents) {
		this.batchwiseStudents = batchwiseStudents;
	}

	public BatchwiseStudent addBatchwiseStudent(BatchwiseStudent batchwiseStudent) {
		getBatchwiseStudents().add(batchwiseStudent);
		batchwiseStudent.setSchool(this);

		return batchwiseStudent;
	}

	public BatchwiseStudent removeBatchwiseStudent(BatchwiseStudent batchwiseStudent) {
		getBatchwiseStudents().remove(batchwiseStudent);
		batchwiseStudent.setSchool(null);

		return batchwiseStudent;
	}

	public List<ElectiveGroupyrMapping> getElectiveGroupyrMappings() {
		return this.electiveGroupyrMappings;
	}

	public void setElectiveGroupyrMappings(List<ElectiveGroupyrMapping> electiveGroupyrMappings) {
		this.electiveGroupyrMappings = electiveGroupyrMappings;
	}

	public ElectiveGroupyrMapping addElectiveGroupyrMapping(ElectiveGroupyrMapping electiveGroupyrMapping) {
		getElectiveGroupyrMappings().add(electiveGroupyrMapping);
		electiveGroupyrMapping.setSchool(this);

		return electiveGroupyrMapping;
	}

	public ElectiveGroupyrMapping removeElectiveGroupyrMapping(ElectiveGroupyrMapping electiveGroupyrMapping) {
		getElectiveGroupyrMappings().remove(electiveGroupyrMapping);
		electiveGroupyrMapping.setSchool(null);

		return electiveGroupyrMapping;
	}

	public List<GroupyrRegulationDetail> getGroupyrRegulationDetails() {
		return this.groupyrRegulationDetails;
	}

	public void setGroupyrRegulationDetails(List<GroupyrRegulationDetail> groupyrRegulationDetails) {
		this.groupyrRegulationDetails = groupyrRegulationDetails;
	}

	public GroupyrRegulationDetail addGroupyrRegulationDetail(GroupyrRegulationDetail groupyrRegulationDetail) {
		getGroupyrRegulationDetails().add(groupyrRegulationDetail);
		groupyrRegulationDetail.setSchool(this);

		return groupyrRegulationDetail;
	}

	public GroupyrRegulationDetail removeGroupyrRegulationDetail(GroupyrRegulationDetail groupyrRegulationDetail) {
		getGroupyrRegulationDetails().remove(groupyrRegulationDetail);
		groupyrRegulationDetail.setSchool(null);

		return groupyrRegulationDetail;
	}

	public List<StaffCourseyrSubject> getStaffCourseyrSubjects() {
		return this.staffCourseyrSubjects;
	}

	public void setStaffCourseyrSubjects(List<StaffCourseyrSubject> staffCourseyrSubjects) {
		this.staffCourseyrSubjects = staffCourseyrSubjects;
	}

	public StaffCourseyrSubject addStaffCourseyrSubject(StaffCourseyrSubject staffCourseyrSubject) {
		getStaffCourseyrSubjects().add(staffCourseyrSubject);
		staffCourseyrSubject.setSchool(this);

		return staffCourseyrSubject;
	}

	public StaffCourseyrSubject removeStaffCourseyrSubject(StaffCourseyrSubject staffCourseyrSubject) {
		getStaffCourseyrSubjects().remove(staffCourseyrSubject);
		staffCourseyrSubject.setSchool(null);

		return staffCourseyrSubject;
	}

	public List<StaffSyllabusPlan> getStaffSyllabusPlans() {
		return this.staffSyllabusPlans;
	}

	public void setStaffSyllabusPlans(List<StaffSyllabusPlan> staffSyllabusPlans) {
		this.staffSyllabusPlans = staffSyllabusPlans;
	}

	public StaffSyllabusPlan addStaffSyllabusPlan(StaffSyllabusPlan staffSyllabusPlan) {
		getStaffSyllabusPlans().add(staffSyllabusPlan);
		staffSyllabusPlan.setSchool(this);

		return staffSyllabusPlan;
	}

	public StaffSyllabusPlan removeStaffSyllabusPlan(StaffSyllabusPlan staffSyllabusPlan) {
		getStaffSyllabusPlans().remove(staffSyllabusPlan);
		staffSyllabusPlan.setSchool(null);

		return staffSyllabusPlan;
	}

	public List<SubjectBook> getSubjectBooks() {
		return this.subjectBooks;
	}

	public void setSubjectBooks(List<SubjectBook> subjectBooks) {
		this.subjectBooks = subjectBooks;
	}

	public SubjectBook addSubjectBook(SubjectBook subjectBook) {
		getSubjectBooks().add(subjectBook);
		subjectBook.setSchool(this);

		return subjectBook;
	}

	public SubjectBook removeSubjectBook(SubjectBook subjectBook) {
		getSubjectBooks().remove(subjectBook);
		subjectBook.setSchool(null);

		return subjectBook;
	}

	public List<SubjectCourseyear> getSubjectCourseyears() {
		return this.subjectCourseyears;
	}

	public void setSubjectCourseyears(List<SubjectCourseyear> subjectCourseyears) {
		this.subjectCourseyears = subjectCourseyears;
	}

	public SubjectCourseyear addSubjectCourseyear(SubjectCourseyear subjectCourseyear) {
		getSubjectCourseyears().add(subjectCourseyear);
		subjectCourseyear.setSchool(this);

		return subjectCourseyear;
	}

	public SubjectCourseyear removeSubjectCourseyear(SubjectCourseyear subjectCourseyear) {
		getSubjectCourseyears().remove(subjectCourseyear);
		subjectCourseyear.setSchool(null);

		return subjectCourseyear;
	}

	public List<SubjectUnitTopic> getSubjectUnitTopics() {
		return this.subjectUnitTopics;
	}

	public void setSubjectUnitTopics(List<SubjectUnitTopic> subjectUnitTopics) {
		this.subjectUnitTopics = subjectUnitTopics;
	}

	public SubjectUnitTopic addSubjectUnitTopic(SubjectUnitTopic subjectUnitTopic) {
		getSubjectUnitTopics().add(subjectUnitTopic);
		subjectUnitTopic.setSchool(this);

		return subjectUnitTopic;
	}

	public SubjectUnitTopic removeSubjectUnitTopic(SubjectUnitTopic subjectUnitTopic) {
		getSubjectUnitTopics().remove(subjectUnitTopic);
		subjectUnitTopic.setSchool(null);

		return subjectUnitTopic;
	}

	public List<SubjectUnit> getSubjectUnits() {
		return this.subjectUnits;
	}

	public void setSubjectUnits(List<SubjectUnit> subjectUnits) {
		this.subjectUnits = subjectUnits;
	}

	public SubjectUnit addSubjectUnit(SubjectUnit subjectUnit) {
		getSubjectUnits().add(subjectUnit);
		subjectUnit.setSchool(this);

		return subjectUnit;
	}

	public SubjectUnit removeSubjectUnit(SubjectUnit subjectUnit) {
		getSubjectUnits().remove(subjectUnit);
		subjectUnit.setSchool(null);

		return subjectUnit;
	}

	public List<Subjectregulation> getSubjectregulations() {
		return this.subjectregulations;
	}

	public void setSubjectregulations(List<Subjectregulation> subjectregulations) {
		this.subjectregulations = subjectregulations;
	}

	public Subjectregulation addSubjectregulation(Subjectregulation subjectregulation) {
		getSubjectregulations().add(subjectregulation);
		subjectregulation.setSchool(this);

		return subjectregulation;
	}

	public Subjectregulation removeSubjectregulation(Subjectregulation subjectregulation) {
		getSubjectregulations().remove(subjectregulation);
		subjectregulation.setSchool(null);

		return subjectregulation;
	}

	public List<EmployeeAttendance> getEmpAttendances() {
		return this.empAttendances;
	}

	public void setEmpAttendances(List<EmployeeAttendance> empAttendances) {
		this.empAttendances = empAttendances;
	}

	public EmployeeAttendance addEmpAttendance(EmployeeAttendance empAttendance) {
		getEmpAttendances().add(empAttendance);
		empAttendance.setSchool(this);

		return empAttendance;
	}

	public EmployeeAttendance removeEmpAttendance(EmployeeAttendance empAttendance) {
		getEmpAttendances().remove(empAttendance);
		empAttendance.setSchool(null);

		return empAttendance;
	}

	public List<EmployeeDailylog> getEmpDailylogs() {
		return this.empDailylogs;
	}

	public void setEmpDailylogs(List<EmployeeDailylog> empDailylogs) {
		this.empDailylogs = empDailylogs;
	}

	public EmployeeDailylog addEmpDailylog(EmployeeDailylog empDailylog) {
		getEmpDailylogs().add(empDailylog);
		empDailylog.setSchool(this);

		return empDailylog;
	}

	public EmployeeDailylog removeEmpDailylog(EmployeeDailylog empDailylog) {
		getEmpDailylogs().remove(empDailylog);
		empDailylog.setSchool(null);

		return empDailylog;
	}

	public List<EmployeeDetail> getEmpDetails() {
		return this.empDetails;
	}

	public void setEmpDetails(List<EmployeeDetail> empDetails) {
		this.empDetails = empDetails;
	}

	public EmployeeDetail addEmpDetail(EmployeeDetail empDetail) {
		getEmpDetails().add(empDetail);
		empDetail.setSchool(this);

		return empDetail;
	}

	public EmployeeDetail removeEmpDetail(EmployeeDetail empDetail) {
		getEmpDetails().remove(empDetail);
		empDetail.setSchool(null);

		return empDetail;
	}

	public List<EmployeeStgDailylog> getEmpStgDailylogs() {
		return this.empStgDailylogs;
	}

	public void setEmpStgDailylogs(List<EmployeeStgDailylog> empStgDailylogs) {
		this.empStgDailylogs = empStgDailylogs;
	}

	public EmployeeStgDailylog addEmpStgDailylog(EmployeeStgDailylog empStgDailylog) {
		getEmpStgDailylogs().add(empStgDailylog);
		empStgDailylog.setSchool(this);

		return empStgDailylog;
	}

	public EmployeeStgDailylog removeEmpStgDailylog(EmployeeStgDailylog empStgDailylog) {
		getEmpStgDailylogs().remove(empStgDailylog);
		empStgDailylog.setSchool(null);

		return empStgDailylog;
	}

	public List<FeeCategory> getFeeCategories() {
		return this.feeCategories;
	}

	public void setFeeCategories(List<FeeCategory> feeCategories) {
		this.feeCategories = feeCategories;
	}

	public FeeCategory addFeeCategory(FeeCategory feeCategory) {
		getFeeCategories().add(feeCategory);
		feeCategory.setSchool(this);

		return feeCategory;
	}

	public FeeCategory removeFeeCategory(FeeCategory feeCategory) {
		getFeeCategories().remove(feeCategory);
		feeCategory.setSchool(null);

		return feeCategory;
	}

	public List<FeeDiscountApplyFor> getFeeDiscountApplyFors() {
		return this.feeDiscountApplyFors;
	}

	public void setFeeDiscountApplyFors(List<FeeDiscountApplyFor> feeDiscountApplyFors) {
		this.feeDiscountApplyFors = feeDiscountApplyFors;
	}

	public FeeDiscountApplyFor addFeeDiscountApplyFor(FeeDiscountApplyFor feeDiscountApplyFor) {
		getFeeDiscountApplyFors().add(feeDiscountApplyFor);
		feeDiscountApplyFor.setSchool(this);

		return feeDiscountApplyFor;
	}

	public FeeDiscountApplyFor removeFeeDiscountApplyFor(FeeDiscountApplyFor feeDiscountApplyFor) {
		getFeeDiscountApplyFors().remove(feeDiscountApplyFor);
		feeDiscountApplyFor.setSchool(null);

		return feeDiscountApplyFor;
	}

	public List<FeeDiscount> getFeeDiscounts() {
		return this.feeDiscounts;
	}

	public void setFeeDiscounts(List<FeeDiscount> feeDiscounts) {
		this.feeDiscounts = feeDiscounts;
	}

	public FeeDiscount addFeeDiscount(FeeDiscount feeDiscount) {
		getFeeDiscounts().add(feeDiscount);
		feeDiscount.setSchool(this);

		return feeDiscount;
	}

	public FeeDiscount removeFeeDiscount(FeeDiscount feeDiscount) {
		getFeeDiscounts().remove(feeDiscount);
		feeDiscount.setSchool(null);

		return feeDiscount;
	}

	public List<FeeDiscountsParticular> getFeeDiscountsParticulars() {
		return this.feeDiscountsParticulars;
	}

	public void setFeeDiscountsParticulars(List<FeeDiscountsParticular> feeDiscountsParticulars) {
		this.feeDiscountsParticulars = feeDiscountsParticulars;
	}

	public FeeDiscountsParticular addFeeDiscountsParticular(FeeDiscountsParticular feeDiscountsParticular) {
		getFeeDiscountsParticulars().add(feeDiscountsParticular);
		feeDiscountsParticular.setSchool(this);

		return feeDiscountsParticular;
	}

	public FeeDiscountsParticular removeFeeDiscountsParticular(FeeDiscountsParticular feeDiscountsParticular) {
		getFeeDiscountsParticulars().remove(feeDiscountsParticular);
		feeDiscountsParticular.setSchool(null);

		return feeDiscountsParticular;
	}

	public List<FeeFine> getFeeFines() {
		return this.feeFines;
	}

	public void setFeeFines(List<FeeFine> feeFines) {
		this.feeFines = feeFines;
	}

	public FeeFine addFeeFine(FeeFine feeFine) {
		getFeeFines().add(feeFine);
		feeFine.setSchool(this);

		return feeFine;
	}

	public FeeFine removeFeeFine(FeeFine feeFine) {
		getFeeFines().remove(feeFine);
		feeFine.setSchool(null);

		return feeFine;
	}

	public List<FeeFineSlab> getFeeFineSlabs() {
		return this.feeFineSlabs;
	}

	public void setFeeFineSlabs(List<FeeFineSlab> feeFineSlabs) {
		this.feeFineSlabs = feeFineSlabs;
	}

	public FeeFineSlab addFeeFineSlab(FeeFineSlab feeFineSlab) {
		getFeeFineSlabs().add(feeFineSlab);
		feeFineSlab.setSchool(this);

		return feeFineSlab;
	}

	public FeeFineSlab removeFeeFineSlab(FeeFineSlab feeFineSlab) {
		getFeeFineSlabs().remove(feeFineSlab);
		feeFineSlab.setSchool(null);

		return feeFineSlab;
	}

	public List<FeeInstantCategory> getFeeInstantCategories() {
		return this.feeInstantCategories;
	}

	public void setFeeInstantCategories(List<FeeInstantCategory> feeInstantCategories) {
		this.feeInstantCategories = feeInstantCategories;
	}

	public FeeInstantCategory addFeeInstantCategory(FeeInstantCategory feeInstantCategory) {
		getFeeInstantCategories().add(feeInstantCategory);
		feeInstantCategory.setSchool(this);

		return feeInstantCategory;
	}

	public FeeInstantCategory removeFeeInstantCategory(FeeInstantCategory feeInstantCategory) {
		getFeeInstantCategories().remove(feeInstantCategory);
		feeInstantCategory.setSchool(null);

		return feeInstantCategory;
	}

	public List<FeeInstantParticular> getFeeInstantParticulars() {
		return this.feeInstantParticulars;
	}

	public void setFeeInstantParticulars(List<FeeInstantParticular> feeInstantParticulars) {
		this.feeInstantParticulars = feeInstantParticulars;
	}

	public FeeInstantParticular addFeeInstantParticular(FeeInstantParticular feeInstantParticular) {
		getFeeInstantParticulars().add(feeInstantParticular);
		feeInstantParticular.setSchool(this);

		return feeInstantParticular;
	}

	public FeeInstantParticular removeFeeInstantParticular(FeeInstantParticular feeInstantParticular) {
		getFeeInstantParticulars().remove(feeInstantParticular);
		feeInstantParticular.setSchool(null);

		return feeInstantParticular;
	}

	public List<FeeInstantPaymentParticular> getFeeInstantPaymentParticulars() {
		return this.feeInstantPaymentParticulars;
	}

	public void setFeeInstantPaymentParticulars(List<FeeInstantPaymentParticular> feeInstantPaymentParticulars) {
		this.feeInstantPaymentParticulars = feeInstantPaymentParticulars;
	}

	public FeeInstantPaymentParticular addFeeInstantPaymentParticular(FeeInstantPaymentParticular feeInstantPaymentParticular) {
		getFeeInstantPaymentParticulars().add(feeInstantPaymentParticular);
		feeInstantPaymentParticular.setSchool(this);

		return feeInstantPaymentParticular;
	}

	public FeeInstantPaymentParticular removeFeeInstantPaymentParticular(FeeInstantPaymentParticular feeInstantPaymentParticular) {
		getFeeInstantPaymentParticulars().remove(feeInstantPaymentParticular);
		feeInstantPaymentParticular.setSchool(null);

		return feeInstantPaymentParticular;
	}

	public List<FeeInstantPayment> getFeeInstantPayments() {
		return this.feeInstantPayments;
	}

	public void setFeeInstantPayments(List<FeeInstantPayment> feeInstantPayments) {
		this.feeInstantPayments = feeInstantPayments;
	}

	public FeeInstantPayment addFeeInstantPayment(FeeInstantPayment feeInstantPayment) {
		getFeeInstantPayments().add(feeInstantPayment);
		feeInstantPayment.setSchool(this);

		return feeInstantPayment;
	}

	public FeeInstantPayment removeFeeInstantPayment(FeeInstantPayment feeInstantPayment) {
		getFeeInstantPayments().remove(feeInstantPayment);
		feeInstantPayment.setSchool(null);

		return feeInstantPayment;
	}

	public List<FeeParticular> getFeeParticulars() {
		return this.feeParticulars;
	}

	public void setFeeParticulars(List<FeeParticular> feeParticulars) {
		this.feeParticulars = feeParticulars;
	}

	public FeeParticular addFeeParticular(FeeParticular feeParticular) {
		getFeeParticulars().add(feeParticular);
		feeParticular.setSchool(this);

		return feeParticular;
	}

	public FeeParticular removeFeeParticular(FeeParticular feeParticular) {
		getFeeParticulars().remove(feeParticular);
		feeParticular.setSchool(null);

		return feeParticular;
	}

	public List<FeeParticularwisePayment> getFeeParticularwisePayments() {
		return this.feeParticularwisePayments;
	}

	public void setFeeParticularwisePayments(List<FeeParticularwisePayment> feeParticularwisePayments) {
		this.feeParticularwisePayments = feeParticularwisePayments;
	}

	public FeeParticularwisePayment addFeeParticularwisePayment(FeeParticularwisePayment feeParticularwisePayment) {
		getFeeParticularwisePayments().add(feeParticularwisePayment);
		feeParticularwisePayment.setSchool(this);

		return feeParticularwisePayment;
	}

	public FeeParticularwisePayment removeFeeParticularwisePayment(FeeParticularwisePayment feeParticularwisePayment) {
		getFeeParticularwisePayments().remove(feeParticularwisePayment);
		feeParticularwisePayment.setSchool(null);

		return feeParticularwisePayment;
	}

	public List<FeeReceipt> getFeeReceipts() {
		return this.feeReceipts;
	}

	public void setFeeReceipts(List<FeeReceipt> feeReceipts) {
		this.feeReceipts = feeReceipts;
	}

	public FeeReceipt addFeeReceipt(FeeReceipt feeReceipt) {
		getFeeReceipts().add(feeReceipt);
		feeReceipt.setSchool(this);

		return feeReceipt;
	}

	public FeeReceipt removeFeeReceipt(FeeReceipt feeReceipt) {
		getFeeReceipts().remove(feeReceipt);
		feeReceipt.setSchool(null);

		return feeReceipt;
	}

	public List<FeeRefundRule> getFeeRefundRules() {
		return this.feeRefundRules;
	}

	public void setFeeRefundRules(List<FeeRefundRule> feeRefundRules) {
		this.feeRefundRules = feeRefundRules;
	}

	public FeeRefundRule addFeeRefundRule(FeeRefundRule feeRefundRule) {
		getFeeRefundRules().add(feeRefundRule);
		feeRefundRule.setSchool(this);

		return feeRefundRule;
	}

	public FeeRefundRule removeFeeRefundRule(FeeRefundRule feeRefundRule) {
		getFeeRefundRules().remove(feeRefundRule);
		feeRefundRule.setSchool(null);

		return feeRefundRule;
	}

	public List<FeeStructure> getFeeStructures() {
		return this.feeStructures;
	}

	public void setFeeStructures(List<FeeStructure> feeStructures) {
		this.feeStructures = feeStructures;
	}

	public FeeStructure addFeeStructure(FeeStructure feeStructure) {
		getFeeStructures().add(feeStructure);
		feeStructure.setSchool(this);

		return feeStructure;
	}

	public FeeStructure removeFeeStructure(FeeStructure feeStructure) {
		getFeeStructures().remove(feeStructure);
		feeStructure.setSchool(null);

		return feeStructure;
	}

	public List<FeeStructureCourseyr> getFeeStructureCourseyrs() {
		return this.feeStructureCourseyrs;
	}

	public void setFeeStructureCourseyrs(List<FeeStructureCourseyr> feeStructureCourseyrs) {
		this.feeStructureCourseyrs = feeStructureCourseyrs;
	}

	public FeeStructureCourseyr addFeeStructureCourseyr(FeeStructureCourseyr feeStructureCourseyr) {
		getFeeStructureCourseyrs().add(feeStructureCourseyr);
		feeStructureCourseyr.setSchool(this);

		return feeStructureCourseyr;
	}

	public FeeStructureCourseyr removeFeeStructureCourseyr(FeeStructureCourseyr feeStructureCourseyr) {
		getFeeStructureCourseyrs().remove(feeStructureCourseyr);
		feeStructureCourseyr.setSchool(null);

		return feeStructureCourseyr;
	}

	public List<FeeStructureParticular> getFeeStructureParticulars() {
		return this.feeStructureParticulars;
	}

	public void setFeeStructureParticulars(List<FeeStructureParticular> feeStructureParticulars) {
		this.feeStructureParticulars = feeStructureParticulars;
	}

	public FeeStructureParticular addFeeStructureParticular(FeeStructureParticular feeStructureParticular) {
		getFeeStructureParticulars().add(feeStructureParticular);
		feeStructureParticular.setSchool(this);

		return feeStructureParticular;
	}

	public FeeStructureParticular removeFeeStructureParticular(FeeStructureParticular feeStructureParticular) {
		getFeeStructureParticulars().remove(feeStructureParticular);
		feeStructureParticular.setSchool(null);

		return feeStructureParticular;
	}

	public List<FeeStudentData> getFeeStudentData() {
		return this.feeStudentData;
	}

	public void setFeeStudentData(List<FeeStudentData> feeStudentData) {
		this.feeStudentData = feeStudentData;
	}

	public FeeStudentData addFeeStudentData(FeeStudentData feeStudentData) {
		getFeeStudentData().add(feeStudentData);
		feeStudentData.setSchool(this);

		return feeStudentData;
	}

	public FeeStudentData removeFeeStudentData(FeeStudentData feeStudentData) {
		getFeeStudentData().remove(feeStudentData);
		feeStudentData.setSchool(null);

		return feeStudentData;
	}

	public List<FeeStudentDataDetail> getStudentDataDetails() {
		return this.studentDataDetails;
	}

	public void setStudentDataDetails(List<FeeStudentDataDetail> studentDataDetails) {
		this.studentDataDetails = studentDataDetails;
	}

	public FeeStudentDataDetail addStudentDataDetail(FeeStudentDataDetail studentDataDetail) {
		getStudentDataDetails().add(studentDataDetail);
		studentDataDetail.setSchool(this);

		return studentDataDetail;
	}

	public FeeStudentDataDetail removeStudentDataDetail(FeeStudentDataDetail studentDataDetail) {
		getStudentDataDetails().remove(studentDataDetail);
		studentDataDetail.setSchool(null);

		return studentDataDetail;
	}

	public List<FeeStudentRefund> getFeeStudentRefunds() {
		return this.feeStudentRefunds;
	}

	public void setFeeStudentRefunds(List<FeeStudentRefund> feeStudentRefunds) {
		this.feeStudentRefunds = feeStudentRefunds;
	}

	public FeeStudentRefund addFeeStudentRefund(FeeStudentRefund feeStudentRefund) {
		getFeeStudentRefunds().add(feeStudentRefund);
		feeStudentRefund.setSchool(this);

		return feeStudentRefund;
	}

	public FeeStudentRefund removeFeeStudentRefund(FeeStudentRefund feeStudentRefund) {
		getFeeStudentRefunds().remove(feeStudentRefund);
		feeStudentRefund.setSchool(null);

		return feeStudentRefund;
	}

	public List<FeeStudentWiseParticular> getFeeStudentWiseParticulars() {
		return this.feeStudentWiseParticulars;
	}

	public void setFeeStudentWiseParticulars(List<FeeStudentWiseParticular> feeStudentWiseParticulars) {
		this.feeStudentWiseParticulars = feeStudentWiseParticulars;
	}

	public FeeStudentWiseParticular addFeeStudentWiseParticular(FeeStudentWiseParticular feeStudentWiseParticular) {
		getFeeStudentWiseParticulars().add(feeStudentWiseParticular);
		feeStudentWiseParticular.setSchool(this);

		return feeStudentWiseParticular;
	}

	public FeeStudentWiseParticular removeFeeStudentWiseParticular(FeeStudentWiseParticular feeStudentWiseParticular) {
		getFeeStudentWiseParticulars().remove(feeStudentWiseParticular);
		feeStudentWiseParticular.setSchool(null);

		return feeStudentWiseParticular;
	}

	public List<FeeStudentwiseDiscount> getFeeStudentwiseDiscounts() {
		return this.feeStudentwiseDiscounts;
	}

	public void setFeeStudentwiseDiscounts(List<FeeStudentwiseDiscount> feeStudentwiseDiscounts) {
		this.feeStudentwiseDiscounts = feeStudentwiseDiscounts;
	}

	public FeeStudentwiseDiscount addFeeStudentwiseDiscount(FeeStudentwiseDiscount feeStudentwiseDiscount) {
		getFeeStudentwiseDiscounts().add(feeStudentwiseDiscount);
		feeStudentwiseDiscount.setSchool(this);

		return feeStudentwiseDiscount;
	}

	public FeeStudentwiseDiscount removeFeeStudentwiseDiscount(FeeStudentwiseDiscount feeStudentwiseDiscount) {
		getFeeStudentwiseDiscounts().remove(feeStudentwiseDiscount);
		feeStudentwiseDiscount.setSchool(null);

		return feeStudentwiseDiscount;
	}

	public List<FeeStudentwiseFine> getFeeStudentwiseFines() {
		return this.feeStudentwiseFines;
	}

	public void setFeeStudentwiseFines(List<FeeStudentwiseFine> feeStudentwiseFines) {
		this.feeStudentwiseFines = feeStudentwiseFines;
	}

	public FeeStudentwiseFine addFeeStudentwiseFine(FeeStudentwiseFine feeStudentwiseFine) {
		getFeeStudentwiseFines().add(feeStudentwiseFine);
		feeStudentwiseFine.setSchool(this);

		return feeStudentwiseFine;
	}

	public FeeStudentwiseFine removeFeeStudentwiseFine(FeeStudentwiseFine feeStudentwiseFine) {
		getFeeStudentwiseFines().remove(feeStudentwiseFine);
		feeStudentwiseFine.setSchool(null);

		return feeStudentwiseFine;
	}

	public List<FeeTransactionDetail> getFeeTransactionDetails() {
		return this.feeTransactionDetails;
	}

	public void setFeeTransactionDetails(List<FeeTransactionDetail> feeTransactionDetails) {
		this.feeTransactionDetails = feeTransactionDetails;
	}

	public FeeTransactionDetail addFeeTransactionDetail(FeeTransactionDetail feeTransactionDetail) {
		getFeeTransactionDetails().add(feeTransactionDetail);
		feeTransactionDetail.setSchool(this);

		return feeTransactionDetail;
	}

	public FeeTransactionDetail removeFeeTransactionDetail(FeeTransactionDetail feeTransactionDetail) {
		getFeeTransactionDetails().remove(feeTransactionDetail);
		feeTransactionDetail.setSchool(null);

		return feeTransactionDetail;
	}

	public List<FeeTransactionMaster> getFeeTransactionMasters() {
		return this.feeTransactionMasters;
	}

	public void setFeeTransactionMasters(List<FeeTransactionMaster> feeTransactionMasters) {
		this.feeTransactionMasters = feeTransactionMasters;
	}

	public FeeTransactionMaster addFeeTransactionMaster(FeeTransactionMaster feeTransactionMaster) {
		getFeeTransactionMasters().add(feeTransactionMaster);
		feeTransactionMaster.setSchool(this);

		return feeTransactionMaster;
	}

	public FeeTransactionMaster removeFeeTransactionMaster(FeeTransactionMaster feeTransactionMaster) {
		getFeeTransactionMasters().remove(feeTransactionMaster);
		feeTransactionMaster.setSchool(null);

		return feeTransactionMaster;
	}

	public List<FinAccountType> getFinAccountTypes() {
		return this.finAccountTypes;
	}

	public void setFinAccountTypes(List<FinAccountType> finAccountTypes) {
		this.finAccountTypes = finAccountTypes;
	}

	public FinAccountType addFinAccountType(FinAccountType finAccountType) {
		getFinAccountTypes().add(finAccountType);
		finAccountType.setSchool(this);

		return finAccountType;
	}

	public FinAccountType removeFinAccountType(FinAccountType finAccountType) {
		getFinAccountTypes().remove(finAccountType);
		finAccountType.setSchool(null);

		return finAccountType;
	}

	public List<FinCategory> getFinCategories() {
		return this.finCategories;
	}

	public void setFinCategories(List<FinCategory> finCategories) {
		this.finCategories = finCategories;
	}

	public FinCategory addFinCategory(FinCategory finCategory) {
		getFinCategories().add(finCategory);
		finCategory.setSchool(this);

		return finCategory;
	}

	public FinCategory removeFinCategory(FinCategory finCategory) {
		getFinCategories().remove(finCategory);
		finCategory.setSchool(null);

		return finCategory;
	}

	public List<FinSubCategory> getFinSubCategories() {
		return this.finSubCategories;
	}

	public void setFinSubCategories(List<FinSubCategory> finSubCategories) {
		this.finSubCategories = finSubCategories;
	}

	public FinSubCategory addFinSubCategory(FinSubCategory finSubCategory) {
		getFinSubCategories().add(finSubCategory);
		finSubCategory.setSchool(this);

		return finSubCategory;
	}

	public FinSubCategory removeFinSubCategory(FinSubCategory finSubCategory) {
		getFinSubCategories().remove(finSubCategory);
		finSubCategory.setSchool(null);

		return finSubCategory;
	}

	public List<FinTransaction> getFinTransactions() {
		return this.finTransactions;
	}

	public void setFinTransactions(List<FinTransaction> finTransactions) {
		this.finTransactions = finTransactions;
	}

	public FinTransaction addFinTransaction(FinTransaction finTransaction) {
		getFinTransactions().add(finTransaction);
		finTransaction.setSchool(this);

		return finTransaction;
	}

	public FinTransaction removeFinTransaction(FinTransaction finTransaction) {
		getFinTransactions().remove(finTransaction);
		finTransaction.setSchool(null);

		return finTransaction;
	}

	public List<FinTransactionHistory> getFinTransactionHistories() {
		return this.finTransactionHistories;
	}

	public void setFinTransactionHistories(List<FinTransactionHistory> finTransactionHistories) {
		this.finTransactionHistories = finTransactionHistories;
	}

	public FinTransactionHistory addFinTransactionHistory(FinTransactionHistory finTransactionHistory) {
		getFinTransactionHistories().add(finTransactionHistory);
		finTransactionHistory.setSchool(this);

		return finTransactionHistory;
	}

	public FinTransactionHistory removeFinTransactionHistory(FinTransactionHistory finTransactionHistory) {
		getFinTransactionHistories().remove(finTransactionHistory);
		finTransactionHistory.setSchool(null);

		return finTransactionHistory;
	}

	public List<EmployeePayrollGroup> getEmployeePayrollGroups() {
		return this.employeePayrollGroups;
	}

	public void setEmployeePayrollGroups(List<EmployeePayrollGroup> employeePayrollGroups) {
		this.employeePayrollGroups = employeePayrollGroups;
	}

	public EmployeePayrollGroup addEmployeePayrollGroup(EmployeePayrollGroup employeePayrollGroup) {
		getEmployeePayrollGroups().add(employeePayrollGroup);
		employeePayrollGroup.setSchool(this);

		return employeePayrollGroup;
	}

	public EmployeePayrollGroup removeEmployeePayrollGroup(EmployeePayrollGroup employeePayrollGroup) {
		getEmployeePayrollGroups().remove(employeePayrollGroup);
		employeePayrollGroup.setSchool(null);

		return employeePayrollGroup;
	}

	public List<EmployeePayslipDetail> getEmployeePayslipDetails() {
		return this.employeePayslipDetails;
	}

	public void setEmployeePayslipDetails(List<EmployeePayslipDetail> employeePayslipDetails) {
		this.employeePayslipDetails = employeePayslipDetails;
	}

	public EmployeePayslipDetail addEmployeePayslipDetail(EmployeePayslipDetail employeePayslipDetail) {
		getEmployeePayslipDetails().add(employeePayslipDetail);
		employeePayslipDetail.setSchool(this);

		return employeePayslipDetail;
	}

	public EmployeePayslipDetail removeEmployeePayslipDetail(EmployeePayslipDetail employeePayslipDetail) {
		getEmployeePayslipDetails().remove(employeePayslipDetail);
		employeePayslipDetail.setSchool(null);

		return employeePayslipDetail;
	}

	public List<EmployeePayslipGeneration> getEmployeePayslipGenerations() {
		return this.employeePayslipGenerations;
	}

	public void setEmployeePayslipGenerations(List<EmployeePayslipGeneration> employeePayslipGenerations) {
		this.employeePayslipGenerations = employeePayslipGenerations;
	}

	public EmployeePayslipGeneration addEmployeePayslipGeneration(EmployeePayslipGeneration employeePayslipGeneration) {
		getEmployeePayslipGenerations().add(employeePayslipGeneration);
		employeePayslipGeneration.setSchool(this);

		return employeePayslipGeneration;
	}

	public EmployeePayslipGeneration removeEmployeePayslipGeneration(EmployeePayslipGeneration employeePayslipGeneration) {
		getEmployeePayslipGenerations().remove(employeePayslipGeneration);
		employeePayslipGeneration.setSchool(null);

		return employeePayslipGeneration;
	}

	public List<EmployeePayslipGenerationStatus> getEmployeePayslipGenerationStatuses() {
		return this.employeePayslipGenerationStatuses;
	}

	public void setEmployeePayslipGenerationStatuses(List<EmployeePayslipGenerationStatus> employeePayslipGenerationStatuses) {
		this.employeePayslipGenerationStatuses = employeePayslipGenerationStatuses;
	}

	public EmployeePayslipGenerationStatus addEmployeePayslipGenerationStatus(EmployeePayslipGenerationStatus employeePayslipGenerationStatus) {
		getEmployeePayslipGenerationStatuses().add(employeePayslipGenerationStatus);
		employeePayslipGenerationStatus.setSchool(this);

		return employeePayslipGenerationStatus;
	}

	public EmployeePayslipGenerationStatus removeEmployeePayslipGenerationStatus(EmployeePayslipGenerationStatus employeePayslipGenerationStatus) {
		getEmployeePayslipGenerationStatuses().remove(employeePayslipGenerationStatus);
		employeePayslipGenerationStatus.setSchool(null);

		return employeePayslipGenerationStatus;
	}

	public List<EmployeeSalaryStructure> getEmployeeSalaryStructures() {
		return this.employeeSalaryStructures;
	}

	public void setEmployeeSalaryStructures(List<EmployeeSalaryStructure> employeeSalaryStructures) {
		this.employeeSalaryStructures = employeeSalaryStructures;
	}

	public EmployeeSalaryStructure addEmployeeSalaryStructure(EmployeeSalaryStructure employeeSalaryStructure) {
		getEmployeeSalaryStructures().add(employeeSalaryStructure);
		employeeSalaryStructure.setSchool(this);

		return employeeSalaryStructure;
	}

	public EmployeeSalaryStructure removeEmployeeSalaryStructure(EmployeeSalaryStructure employeeSalaryStructure) {
		getEmployeeSalaryStructures().remove(employeeSalaryStructure);
		employeeSalaryStructure.setSchool(null);

		return employeeSalaryStructure;
	}

	public List<FbOptionGroup> getFbOptionGroups() {
		return this.fbOptionGroups;
	}

	public void setFbOptionGroups(List<FbOptionGroup> fbOptionGroups) {
		this.fbOptionGroups = fbOptionGroups;
	}

	public FbOptionGroup addFbOptionGroup(FbOptionGroup fbOptionGroup) {
		getFbOptionGroups().add(fbOptionGroup);
		fbOptionGroup.setSchool(this);

		return fbOptionGroup;
	}

	public FbOptionGroup removeFbOptionGroup(FbOptionGroup fbOptionGroup) {
		getFbOptionGroups().remove(fbOptionGroup);
		fbOptionGroup.setSchool(null);

		return fbOptionGroup;
	}

	public List<FbOptionchoice> getFbOptionchoices() {
		return this.fbOptionchoices;
	}

	public void setFbOptionchoices(List<FbOptionchoice> fbOptionchoices) {
		this.fbOptionchoices = fbOptionchoices;
	}

	public FbOptionchoice addFbOptionchoice(FbOptionchoice fbOptionchoice) {
		getFbOptionchoices().add(fbOptionchoice);
		fbOptionchoice.setSchool(this);

		return fbOptionchoice;
	}

	public FbOptionchoice removeFbOptionchoice(FbOptionchoice fbOptionchoice) {
		getFbOptionchoices().remove(fbOptionchoice);
		fbOptionchoice.setSchool(null);

		return fbOptionchoice;
	}

	public List<FeedbackQuestion> getFeedbackQuestions() {
		return this.feedbackQuestions;
	}

	public void setFeedbackQuestions(List<FeedbackQuestion> feedbackQuestions) {
		this.feedbackQuestions = feedbackQuestions;
	}

	public FeedbackQuestion addFeedbackQuestion(FeedbackQuestion feedbackQuestion) {
		getFeedbackQuestions().add(feedbackQuestion);
		feedbackQuestion.setSchool(this);

		return feedbackQuestion;
	}

	public FeedbackQuestion removeFeedbackQuestion(FeedbackQuestion feedbackQuestion) {
		getFeedbackQuestions().remove(feedbackQuestion);
		feedbackQuestion.setSchool(null);

		return feedbackQuestion;
	}

	public List<FeedbackSection> getFeedbackSections() {
		return this.feedbackSections;
	}

	public void setFeedbackSections(List<FeedbackSection> feedbackSections) {
		this.feedbackSections = feedbackSections;
	}

	public FeedbackSection addFeedbackSection(FeedbackSection feedbackSection) {
		getFeedbackSections().add(feedbackSection);
		feedbackSection.setSchool(this);

		return feedbackSection;
	}

	public FeedbackSection removeFeedbackSection(FeedbackSection feedbackSection) {
		getFeedbackSections().remove(feedbackSection);
		feedbackSection.setSchool(null);

		return feedbackSection;
	}

	public List<PayrollCategory> getPayrollCategories() {
		return this.payrollCategories;
	}

	public void setPayrollCategories(List<PayrollCategory> payrollCategories) {
		this.payrollCategories = payrollCategories;
	}

	public PayrollCategory addPayrollCategory(PayrollCategory payrollCategory) {
		getPayrollCategories().add(payrollCategory);
		payrollCategory.setSchool(this);

		return payrollCategory;
	}

	public PayrollCategory removePayrollCategory(PayrollCategory payrollCategory) {
		getPayrollCategories().remove(payrollCategory);
		payrollCategory.setSchool(null);

		return payrollCategory;
	}

	public List<PayrollCategoryGroup> getPayrollCategoryGroups() {
		return this.payrollCategoryGroups;
	}

	public void setPayrollCategoryGroups(List<PayrollCategoryGroup> payrollCategoryGroups) {
		this.payrollCategoryGroups = payrollCategoryGroups;
	}

	public PayrollCategoryGroup addPayrollCategoryGroup(PayrollCategoryGroup payrollCategoryGroup) {
		getPayrollCategoryGroups().add(payrollCategoryGroup);
		payrollCategoryGroup.setSchool(this);

		return payrollCategoryGroup;
	}

	public PayrollCategoryGroup removePayrollCategoryGroup(PayrollCategoryGroup payrollCategoryGroup) {
		getPayrollCategoryGroups().remove(payrollCategoryGroup);
		payrollCategoryGroup.setSchool(null);

		return payrollCategoryGroup;
	}

	public List<PayrollGroup> getPayrollGroups() {
		return this.payrollGroups;
	}

	public void setPayrollGroups(List<PayrollGroup> payrollGroups) {
		this.payrollGroups = payrollGroups;
	}

	public PayrollGroup addPayrollGroup(PayrollGroup payrollGroup) {
		getPayrollGroups().add(payrollGroup);
		payrollGroup.setSchool(this);

		return payrollGroup;
	}

	public PayrollGroup removePayrollGroup(PayrollGroup payrollGroup) {
		getPayrollGroups().remove(payrollGroup);
		payrollGroup.setSchool(null);

		return payrollGroup;
	}

	public List<PayslipBranchSetting> getPayslipBranchSettings() {
		return this.payslipBranchSettings;
	}

	public void setPayslipBranchSettings(List<PayslipBranchSetting> payslipBranchSettings) {
		this.payslipBranchSettings = payslipBranchSettings;
	}

	public PayslipBranchSetting addPayslipBranchSetting(PayslipBranchSetting payslipBranchSetting) {
		getPayslipBranchSettings().add(payslipBranchSetting);
		payslipBranchSetting.setSchool(this);

		return payslipBranchSetting;
	}

	public PayslipBranchSetting removePayslipBranchSetting(PayslipBranchSetting payslipBranchSetting) {
		getPayslipBranchSettings().remove(payslipBranchSetting);
		payslipBranchSetting.setSchool(null);

		return payslipBranchSetting;
	}

	public List<PayslipSetting> getPayslipSettings() {
		return this.payslipSettings;
	}

	public void setPayslipSettings(List<PayslipSetting> payslipSettings) {
		this.payslipSettings = payslipSettings;
	}

	public PayslipSetting addPayslipSetting(PayslipSetting payslipSetting) {
		getPayslipSettings().add(payslipSetting);
		payslipSetting.setSchool(this);

		return payslipSetting;
	}

	public PayslipSetting removePayslipSetting(PayslipSetting payslipSetting) {
		getPayslipSettings().remove(payslipSetting);
		payslipSetting.setSchool(null);

		return payslipSetting;
	}

	public List<SurveyDetail> getSurveyDetails() {
		return this.surveyDetails;
	}

	public void setSurveyDetails(List<SurveyDetail> surveyDetails) {
		this.surveyDetails = surveyDetails;
	}

	public SurveyDetail addSurveyDetail(SurveyDetail surveyDetail) {
		getSurveyDetails().add(surveyDetail);
		surveyDetail.setSchool(this);

		return surveyDetail;
	}

	public SurveyDetail removeSurveyDetail(SurveyDetail surveyDetail) {
		getSurveyDetails().remove(surveyDetail);
		surveyDetail.setSchool(null);

		return surveyDetail;
	}

	public List<SurveyFeedback> getSurveyFeedbacks() {
		return this.surveyFeedbacks;
	}

	public void setSurveyFeedbacks(List<SurveyFeedback> surveyFeedbacks) {
		this.surveyFeedbacks = surveyFeedbacks;
	}

	public SurveyFeedback addSurveyFeedback(SurveyFeedback surveyFeedback) {
		getSurveyFeedbacks().add(surveyFeedback);
		surveyFeedback.setSchool(this);

		return surveyFeedback;
	}

	public SurveyFeedback removeSurveyFeedback(SurveyFeedback surveyFeedback) {
		getSurveyFeedbacks().remove(surveyFeedback);
		surveyFeedback.setSchool(null);

		return surveyFeedback;
	}

	public List<SurveyFeedbackDetail> getSurveyFeedbackDetails() {
		return this.surveyFeedbackDetails;
	}

	public void setSurveyFeedbackDetails(List<SurveyFeedbackDetail> surveyFeedbackDetails) {
		this.surveyFeedbackDetails = surveyFeedbackDetails;
	}

	public SurveyFeedbackDetail addSurveyFeedbackDetail(SurveyFeedbackDetail surveyFeedbackDetail) {
		getSurveyFeedbackDetails().add(surveyFeedbackDetail);
		surveyFeedbackDetail.setSchool(this);

		return surveyFeedbackDetail;
	}

	public SurveyFeedbackDetail removeSurveyFeedbackDetail(SurveyFeedbackDetail surveyFeedbackDetail) {
		getSurveyFeedbackDetails().remove(surveyFeedbackDetail);
		surveyFeedbackDetail.setSchool(null);

		return surveyFeedbackDetail;
	}

	public List<SurveyForm> getSurveyForms() {
		return this.surveyForms;
	}

	public void setSurveyForms(List<SurveyForm> surveyForms) {
		this.surveyForms = surveyForms;
	}

	public SurveyForm addSurveyForm(SurveyForm surveyForm) {
		getSurveyForms().add(surveyForm);
		surveyForm.setSchool(this);

		return surveyForm;
	}

	public SurveyForm removeSurveyForm(SurveyForm surveyForm) {
		getSurveyForms().remove(surveyForm);
		surveyForm.setSchool(null);

		return surveyForm;
	}

	public List<LibraryDetail> getLibraryDetails() {
		return this.libraryDetails;
	}

	public void setLibraryDetails(List<LibraryDetail> libraryDetails) {
		this.libraryDetails = libraryDetails;
	}

	public LibraryDetail addLibraryDetail(LibraryDetail libraryDetail) {
		getLibraryDetails().add(libraryDetail);
		libraryDetail.setSchool(this);

		return libraryDetail;
	}

	public LibraryDetail removeLibraryDetail(LibraryDetail libraryDetail) {
		getLibraryDetails().remove(libraryDetail);
		libraryDetail.setSchool(null);

		return libraryDetail;
	}

	public List<AcademicYear> getAcademicYears() {
		return this.academicYears;
	}

	public void setAcademicYears(List<AcademicYear> academicYears) {
		this.academicYears = academicYears;
	}

	public AcademicYear addAcademicYear(AcademicYear academicYear) {
		getAcademicYears().add(academicYear);
		academicYear.setSchool(this);

		return academicYear;
	}

	public AcademicYear removeAcademicYear(AcademicYear academicYear) {
		getAcademicYears().remove(academicYear);
		academicYear.setSchool(null);

		return academicYear;
	}

	public List<Batch> getBatches() {
		return this.batches;
	}

	public void setBatches(List<Batch> batches) {
		this.batches = batches;
	}

	public Batch addBatch(Batch batch) {
		getBatches().add(batch);
		batch.setSchool(this);

		return batch;
	}

	public Batch removeBatch(Batch batch) {
		getBatches().remove(batch);
		batch.setSchool(null);

		return batch;
	}

	public Campus getCampus() {
		return this.campus;
	}

	public void setCampus(Campus campus) {
		this.campus = campus;
	}

	public City getCity() {
		return this.city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public District getDistrict() {
		return this.district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public GeneralDetail getAffiliatedto() {
		return this.affiliatedto;
	}

	public void setAffiliatedto(GeneralDetail affiliatedto) {
		this.affiliatedto = affiliatedto;
	}

	public GeneralDetail getSchooltype() {
		return this.schooltype;
	}

	public void setSchooltype(GeneralDetail schooltype) {
		this.schooltype = schooltype;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public List<ConfigAutonumber> getConfigAutonumbers() {
		return this.configAutonumbers;
	}

	public void setConfigAutonumbers(List<ConfigAutonumber> configAutonumbers) {
		this.configAutonumbers = configAutonumbers;
	}

	public ConfigAutonumber addConfigAutonumber(ConfigAutonumber configAutonumber) {
		getConfigAutonumbers().add(configAutonumber);
		configAutonumber.setSchool(this);

		return configAutonumber;
	}

	public ConfigAutonumber removeConfigAutonumber(ConfigAutonumber configAutonumber) {
		getConfigAutonumbers().remove(configAutonumber);
		configAutonumber.setSchool(null);

		return configAutonumber;
	}

	public List<Course> getCourses() {
		return this.courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	public Course addCours(Course cours) {
		getCourses().add(cours);
		cours.setSchool(this);

		return cours;
	}

	public Course removeCours(Course cours) {
		getCourses().remove(cours);
		cours.setSchool(null);

		return cours;
	}

	public List<CourseYear> getCourseYears() {
		return this.courseYears;
	}

	public void setCourseYears(List<CourseYear> courseYears) {
		this.courseYears = courseYears;
	}

	public CourseYear addCourseYear(CourseYear courseYear) {
		getCourseYears().add(courseYear);
		courseYear.setSchool(this);

		return courseYear;
	}

	public CourseYear removeCourseYear(CourseYear courseYear) {
		getCourseYears().remove(courseYear);
		courseYear.setSchool(null);

		return courseYear;
	}

	public List<Department> getDepartments() {
		return this.departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

	public Department addDepartment(Department department) {
		getDepartments().add(department);
		department.setSchool(this);

		return department;
	}

	public Department removeDepartment(Department department) {
		getDepartments().remove(department);
		department.setSchool(null);

		return department;
	}

	public List<Electivetype> getElectivetypes() {
		return this.electivetypes;
	}

	public void setElectivetypes(List<Electivetype> electivetypes) {
		this.electivetypes = electivetypes;
	}

	public Electivetype addElectivetype(Electivetype electivetype) {
		getElectivetypes().add(electivetype);
		electivetype.setSchool(this);

		return electivetype;
	}

	public Electivetype removeElectivetype(Electivetype electivetype) {
		getElectivetypes().remove(electivetype);
		electivetype.setSchool(null);

		return electivetype;
	}

	public List<EventAudience> getEventAudiences() {
		return this.eventAudiences;
	}

	public void setEventAudiences(List<EventAudience> eventAudiences) {
		this.eventAudiences = eventAudiences;
	}

	public EventAudience addEventAudience(EventAudience eventAudience) {
		getEventAudiences().add(eventAudience);
		eventAudience.setSchool(this);

		return eventAudience;
	}

	public EventAudience removeEventAudience(EventAudience eventAudience) {
		getEventAudiences().remove(eventAudience);
		eventAudience.setSchool(null);

		return eventAudience;
	}

	public List<EventType> getEventTypes() {
		return this.eventTypes;
	}

	public void setEventTypes(List<EventType> eventTypes) {
		this.eventTypes = eventTypes;
	}

	public EventType addEventType(EventType eventType) {
		getEventTypes().add(eventType);
		eventType.setSchool(this);

		return eventType;
	}

	public EventType removeEventType(EventType eventType) {
		getEventTypes().remove(eventType);
		eventType.setSchool(null);

		return eventType;
	}

	public List<Event> getEvents() {
		return this.events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public Event addEvent(Event event) {
		getEvents().add(event);
		event.setSchool(this);

		return event;
	}

	public Event removeEvent(Event event) {
		getEvents().remove(event);
		event.setSchool(null);

		return event;
	}

	public List<FinancialYear> getFinancialYears() {
		return this.financialYears;
	}

	public void setFinancialYears(List<FinancialYear> financialYears) {
		this.financialYears = financialYears;
	}

	public FinancialYear addFinancialYear(FinancialYear financialYear) {
		getFinancialYears().add(financialYear);
		financialYear.setSchool(this);

		return financialYear;
	}

	public FinancialYear removeFinancialYear(FinancialYear financialYear) {
		getFinancialYears().remove(financialYear);
		financialYear.setSchool(null);

		return financialYear;
	}

	public List<GeneralSetting> getGeneralSettings() {
		return this.generalSettings;
	}

	public void setGeneralSettings(List<GeneralSetting> generalSettings) {
		this.generalSettings = generalSettings;
	}

	public GeneralSetting addGeneralSetting(GeneralSetting generalSetting) {
		getGeneralSettings().add(generalSetting);
		generalSetting.setSchool(this);

		return generalSetting;
	}

	public GeneralSetting removeGeneralSetting(GeneralSetting generalSetting) {
		getGeneralSettings().remove(generalSetting);
		generalSetting.setSchool(null);

		return generalSetting;
	}

	public List<Messaging> getMessagings() {
		return this.messagings;
	}

	public void setMessagings(List<Messaging> messagings) {
		this.messagings = messagings;
	}

	public Messaging addMessaging(Messaging messaging) {
		getMessagings().add(messaging);
		messaging.setSchool(this);

		return messaging;
	}

	public Messaging removeMessaging(Messaging messaging) {
		getMessagings().remove(messaging);
		messaging.setSchool(null);

		return messaging;
	}

	public List<MessagingRecipient> getMessagingRecipients() {
		return this.messagingRecipients;
	}

	public void setMessagingRecipients(List<MessagingRecipient> messagingRecipients) {
		this.messagingRecipients = messagingRecipients;
	}

	public MessagingRecipient addMessagingRecipient(MessagingRecipient messagingRecipient) {
		getMessagingRecipients().add(messagingRecipient);
		messagingRecipient.setSchool(this);

		return messagingRecipient;
	}

	public MessagingRecipient removeMessagingRecipient(MessagingRecipient messagingRecipient) {
		getMessagingRecipients().remove(messagingRecipient);
		messagingRecipient.setSchool(null);

		return messagingRecipient;
	}

	public List<NotificationAudience> getNotificationAudiences() {
		return this.notificationAudiences;
	}

	public void setNotificationAudiences(List<NotificationAudience> notificationAudiences) {
		this.notificationAudiences = notificationAudiences;
	}

	public NotificationAudience addNotificationAudience(NotificationAudience notificationAudience) {
		getNotificationAudiences().add(notificationAudience);
		notificationAudience.setSchool(this);

		return notificationAudience;
	}

	public NotificationAudience removeNotificationAudience(NotificationAudience notificationAudience) {
		getNotificationAudiences().remove(notificationAudience);
		notificationAudience.setSchool(null);

		return notificationAudience;
	}

	public List<Notification> getNotifications() {
		return this.notifications;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}

	public Notification addNotification(Notification notification) {
		getNotifications().add(notification);
		notification.setSchool(this);

		return notification;
	}

	public Notification removeNotification(Notification notification) {
		getNotifications().remove(notification);
		notification.setSchool(null);

		return notification;
	}

	public List<Qualification> getQualifications() {
		return this.qualifications;
	}

	public void setQualifications(List<Qualification> qualifications) {
		this.qualifications = qualifications;
	}

	public Qualification addQualification(Qualification qualification) {
		getQualifications().add(qualification);
		qualification.setSchool(this);

		return qualification;
	}

	public Qualification removeQualification(Qualification qualification) {
		getQualifications().remove(qualification);
		qualification.setSchool(null);

		return qualification;
	}


	public List<Studentbatch> getStudentbatches() {
		return this.studentbatches;
	}

	public void setStudentbatches(List<Studentbatch> studentbatches) {
		this.studentbatches = studentbatches;
	}

	public Studentbatch addStudentbatch(Studentbatch studentbatch) {
		getStudentbatches().add(studentbatch);
		studentbatch.setSchool(this);

		return studentbatch;
	}

	public Studentbatch removeStudentbatch(Studentbatch studentbatch) {
		getStudentbatches().remove(studentbatch);
		studentbatch.setSchool(null);

		return studentbatch;
	}

	public List<Subject> getSubjects() {
		return this.subjects;
	}

	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}

	public Subject addSubject(Subject subject) {
		getSubjects().add(subject);
		subject.setSchool(this);

		return subject;
	}

	public Subject removeSubject(Subject subject) {
		getSubjects().remove(subject);
		subject.setSchool(null);

		return subject;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User addUser(User user) {
		getUsers().add(user);
		user.setSchool(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setSchool(null);

		return user;
	}

	public List<WorkflowStage> getWorkflowStages() {
		return this.workflowStages;
	}

	public void setWorkflowStages(List<WorkflowStage> workflowStages) {
		this.workflowStages = workflowStages;
	}

	public WorkflowStage addWorkflowStage(WorkflowStage workflowStage) {
		getWorkflowStages().add(workflowStage);
		workflowStage.setSchool(this);

		return workflowStage;
	}

	public WorkflowStage removeWorkflowStage(WorkflowStage workflowStage) {
		getWorkflowStages().remove(workflowStage);
		workflowStage.setSchool(null);

		return workflowStage;
	}

	public List<RolePrivilege> getRolePrivileges() {
		return this.rolePrivileges;
	}

	public void setRolePrivileges(List<RolePrivilege> rolePrivileges) {
		this.rolePrivileges = rolePrivileges;
	}

	public RolePrivilege addRolePrivilege(RolePrivilege rolePrivilege) {
		getRolePrivileges().add(rolePrivilege);
		rolePrivilege.setSchool(this);

		return rolePrivilege;
	}

	public RolePrivilege removeRolePrivilege(RolePrivilege rolePrivilege) {
		getRolePrivileges().remove(rolePrivilege);
		rolePrivilege.setSchool(null);

		return rolePrivilege;
	}

	public List<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public Role addRole(Role role) {
		getRoles().add(role);
		role.setSchool(this);

		return role;
	}

	public Role removeRole(Role role) {
		getRoles().remove(role);
		role.setSchool(null);

		return role;
	}

	public List<UserSchoolsGroup> getUserSchoolsGroups() {
		return this.userSchoolsGroups;
	}

	public void setUserSchoolsGroups(List<UserSchoolsGroup> userSchoolsGroups) {
		this.userSchoolsGroups = userSchoolsGroups;
	}

	public UserSchoolsGroup addUserSchoolsGroup(UserSchoolsGroup userSchoolsGroup) {
		getUserSchoolsGroups().add(userSchoolsGroup);
		userSchoolsGroup.setSchool(this);

		return userSchoolsGroup;
	}

	public UserSchoolsGroup removeUserSchoolsGroup(UserSchoolsGroup userSchoolsGroup) {
		getUserSchoolsGroups().remove(userSchoolsGroup);
		userSchoolsGroup.setSchool(null);

		return userSchoolsGroup;
	}

	public List<SmsPattern> getSmsPatterns() {
		return this.smsPatterns;
	}

	public void setSmsPatterns(List<SmsPattern> smsPatterns) {
		this.smsPatterns = smsPatterns;
	}

	public SmsPattern addSmsPattern(SmsPattern smsPattern) {
		getSmsPatterns().add(smsPattern);
		smsPattern.setSchool(this);

		return smsPattern;
	}

	public SmsPattern removeSmsPattern(SmsPattern smsPattern) {
		getSmsPatterns().remove(smsPattern);
		smsPattern.setSchool(null);

		return smsPattern;
	}

	public List<StudentApplication> getStdApplications() {
		return this.stdApplications;
	}

	public void setStdApplications(List<StudentApplication> stdApplications) {
		this.stdApplications = stdApplications;
	}

	public StudentApplication addStdApplication(StudentApplication stdApplication) {
		getStdApplications().add(stdApplication);
		stdApplication.setSchool(this);

		return stdApplication;
	}

	public StudentApplication removeStdApplication(StudentApplication stdApplication) {
		getStdApplications().remove(stdApplication);
		stdApplication.setSchool(null);

		return stdApplication;
	}

	public List<StudentDailylog> getStdDailylogs() {
		return this.stdDailylogs;
	}

	public void setStdDailylogs(List<StudentDailylog> stdDailylogs) {
		this.stdDailylogs = stdDailylogs;
	}

	public StudentDailylog addStdDailylog(StudentDailylog stdDailylog) {
		getStdDailylogs().add(stdDailylog);
		stdDailylog.setSchool(this);

		return stdDailylog;
	}

	public StudentDailylog removeStdDailylog(StudentDailylog stdDailylog) {
		getStdDailylogs().remove(stdDailylog);
		stdDailylog.setSchool(null);

		return stdDailylog;
	}

	public List<StudentEnquiry> getStdEnquiries() {
		return this.stdEnquiries;
	}

	public void setStdEnquiries(List<StudentEnquiry> stdEnquiries) {
		this.stdEnquiries = stdEnquiries;
	}

	public StudentEnquiry addStdEnquiry(StudentEnquiry stdEnquiry) {
		getStdEnquiries().add(stdEnquiry);
		stdEnquiry.setSchool(this);

		return stdEnquiry;
	}

	public StudentEnquiry removeStdEnquiry(StudentEnquiry stdEnquiry) {
		getStdEnquiries().remove(stdEnquiry);
		stdEnquiry.setSchool(null);

		return stdEnquiry;
	}

	public List<StudentStgDailylog> getStdStgDailylogs() {
		return this.stdStgDailylogs;
	}

	public void setStdStgDailylogs(List<StudentStgDailylog> stdStgDailylogs) {
		this.stdStgDailylogs = stdStgDailylogs;
	}

	public StudentStgDailylog addStdStgDailylog(StudentStgDailylog stdStgDailylog) {
		getStdStgDailylogs().add(stdStgDailylog);
		stdStgDailylog.setSchool(this);

		return stdStgDailylog;
	}

	public StudentStgDailylog removeStdStgDailylog(StudentStgDailylog stdStgDailylog) {
		getStdStgDailylogs().remove(stdStgDailylog);
		stdStgDailylog.setSchool(null);

		return stdStgDailylog;
	}

	public List<StudentAcademicbatch> getStudentAcademicbatches() {
		return this.studentAcademicbatches;
	}

	public void setStudentAcademicbatches(List<StudentAcademicbatch> studentAcademicbatches) {
		this.studentAcademicbatches = studentAcademicbatches;
	}

	public StudentAcademicbatch addStudentAcademicbatch(StudentAcademicbatch studentAcademicbatch) {
		getStudentAcademicbatches().add(studentAcademicbatch);
		studentAcademicbatch.setSchool(this);

		return studentAcademicbatch;
	}

	public StudentAcademicbatch removeStudentAcademicbatch(StudentAcademicbatch studentAcademicbatch) {
		getStudentAcademicbatches().remove(studentAcademicbatch);
		studentAcademicbatch.setSchool(null);

		return studentAcademicbatch;
	}

	public List<StudentDetail> getStdStudentDetails() {
		return this.stdStudentDetails;
	}

	public void setStdStudentDetails(List<StudentDetail> stdStudentDetails) {
		this.stdStudentDetails = stdStudentDetails;
	}

	public StudentDetail addStdStudentDetail(StudentDetail stdStudentDetail) {
		getStdStudentDetails().add(stdStudentDetail);
		stdStudentDetail.setSchool(this);

		return stdStudentDetail;
	}

	public StudentDetail removeStdStudentDetail(StudentDetail stdStudentDetail) {
		getStdStudentDetails().remove(stdStudentDetail);
		stdStudentDetail.setSchool(null);

		return stdStudentDetail;
	}

	public List<StudentSubject> getStudentSubjects() {
		return this.studentSubjects;
	}

	public void setStudentSubjects(List<StudentSubject> studentSubjects) {
		this.studentSubjects = studentSubjects;
	}

	public StudentSubject addStudentSubject(StudentSubject studentSubject) {
		getStudentSubjects().add(studentSubject);
		studentSubject.setSchool(this);

		return studentSubject;
	}

	public StudentSubject removeStudentSubject(StudentSubject studentSubject) {
		getStudentSubjects().remove(studentSubject);
		studentSubject.setSchool(null);

		return studentSubject;
	}

	public List<TransportDetail> getTransportDetails() {
		return this.transportDetails;
	}

	public void setTransportDetails(List<TransportDetail> transportDetails) {
		this.transportDetails = transportDetails;
	}

	public TransportDetail addTransportDetail(TransportDetail transportDetail) {
		getTransportDetails().add(transportDetail);
		transportDetail.setSchool(this);

		return transportDetail;
	}

	public TransportDetail removeTransportDetail(TransportDetail transportDetail) {
		getTransportDetails().remove(transportDetail);
		transportDetail.setSchool(null);

		return transportDetail;
	}

	public List<ActualClassesSchedule> getActualClassesSchedules() {
		return this.actualClassesSchedules;
	}

	public void setActualClassesSchedules(List<ActualClassesSchedule> actualClassesSchedules) {
		this.actualClassesSchedules = actualClassesSchedules;
	}

	public ActualClassesSchedule addActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		getActualClassesSchedules().add(actualClassesSchedule);
		actualClassesSchedule.setSchool(this);

		return actualClassesSchedule;
	}

	public ActualClassesSchedule removeActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		getActualClassesSchedules().remove(actualClassesSchedule);
		actualClassesSchedule.setSchool(null);

		return actualClassesSchedule;
	}

	public List<ClassTiming> getClassTimings() {
		return this.classTimings;
	}

	public void setClassTimings(List<ClassTiming> classTimings) {
		this.classTimings = classTimings;
	}

	public ClassTiming addClassTiming(ClassTiming classTiming) {
		getClassTimings().add(classTiming);
		classTiming.setSchool(this);

		return classTiming;
	}

	public ClassTiming removeClassTiming(ClassTiming classTiming) {
		getClassTimings().remove(classTiming);
		classTiming.setSchool(null);

		return classTiming;
	}

	public List<ClassWeekday> getClassWeekdays() {
		return this.classWeekdays;
	}

	public void setClassWeekdays(List<ClassWeekday> classWeekdays) {
		this.classWeekdays = classWeekdays;
	}

	public ClassWeekday addClassWeekday(ClassWeekday classWeekday) {
		getClassWeekdays().add(classWeekday);
		classWeekday.setSchool(this);

		return classWeekday;
	}

	public ClassWeekday removeClassWeekday(ClassWeekday classWeekday) {
		getClassWeekdays().remove(classWeekday);
		classWeekday.setSchool(null);

		return classWeekday;
	}

	public List<Lessonstatus> getLessonstatuses() {
		return this.lessonstatuses;
	}

	public void setLessonstatuses(List<Lessonstatus> lessonstatuses) {
		this.lessonstatuses = lessonstatuses;
	}

	public Lessonstatus addLessonstatus(Lessonstatus lessonstatus) {
		getLessonstatuses().add(lessonstatus);
		lessonstatus.setSchool(this);

		return lessonstatus;
	}

	public Lessonstatus removeLessonstatus(Lessonstatus lessonstatus) {
		getLessonstatuses().remove(lessonstatus);
		lessonstatus.setSchool(null);

		return lessonstatus;
	}

	public List<Schedule> getSchedules() {
		return this.schedules;
	}

	public void setSchedules(List<Schedule> schedules) {
		this.schedules = schedules;
	}

	public Schedule addSchedule(Schedule schedule) {
		getSchedules().add(schedule);
		schedule.setSchool(this);

		return schedule;
	}

	public Schedule removeSchedule(Schedule schedule) {
		getSchedules().remove(schedule);
		schedule.setSchool(null);

		return schedule;
	}

	public List<StaffProxy> getStaffProxies() {
		return this.staffProxies;
	}

	public void setStaffProxies(List<StaffProxy> staffProxies) {
		this.staffProxies = staffProxies;
	}

	public StaffProxy addStaffProxy(StaffProxy staffProxy) {
		getStaffProxies().add(staffProxy);
		staffProxy.setSchool(this);

		return staffProxy;
	}

	public StaffProxy removeStaffProxy(StaffProxy staffProxy) {
		getStaffProxies().remove(staffProxy);
		staffProxy.setSchool(null);

		return staffProxy;
	}

	public List<StudentAttendance> getStdAttendances() {
		return this.stdAttendances;
	}

	public void setStdAttendances(List<StudentAttendance> stdAttendances) {
		this.stdAttendances = stdAttendances;
	}

	public StudentAttendance addStdAttendance(StudentAttendance stdAttendance) {
		getStdAttendances().add(stdAttendance);
		stdAttendance.setSchool(this);

		return stdAttendance;
	}

	public StudentAttendance removeStdAttendance(StudentAttendance stdAttendance) {
		getStdAttendances().remove(stdAttendance);
		stdAttendance.setSchool(null);

		return stdAttendance;
	}

	public List<StudentAttendanceTimeSetting> getStdAttendanceTimeSettings() {
		return this.stdAttendanceTimeSettings;
	}

	public void setStdAttendanceTimeSettings(List<StudentAttendanceTimeSetting> stdAttendanceTimeSettings) {
		this.stdAttendanceTimeSettings = stdAttendanceTimeSettings;
	}

	public StudentAttendanceTimeSetting addStdAttendanceTimeSetting(StudentAttendanceTimeSetting stdAttendanceTimeSetting) {
		getStdAttendanceTimeSettings().add(stdAttendanceTimeSetting);
		stdAttendanceTimeSetting.setSchool(this);

		return stdAttendanceTimeSetting;
	}

	public StudentAttendanceTimeSetting removeStdAttendanceTimeSetting(StudentAttendanceTimeSetting stdAttendanceTimeSetting) {
		getStdAttendanceTimeSettings().remove(stdAttendanceTimeSetting);
		stdAttendanceTimeSetting.setSchool(null);

		return stdAttendanceTimeSetting;
	}

	public List<SubjectResource> getSubjectResources() {
		return this.subjectResources;
	}

	public void setSubjectResources(List<SubjectResource> subjectResources) {
		this.subjectResources = subjectResources;
	}

	public SubjectResource addSubjectResource(SubjectResource subjectResource) {
		getSubjectResources().add(subjectResource);
		subjectResource.setSchool(this);

		return subjectResource;
	}

	public SubjectResource removeSubjectResource(SubjectResource subjectResource) {
		getSubjectResources().remove(subjectResource);
		subjectResource.setSchool(null);

		return subjectResource;
	}

	public List<Timetable> getTimetables() {
		return this.timetables;
	}

	public void setTimetables(List<Timetable> timetables) {
		this.timetables = timetables;
	}

	public Timetable addTimetable(Timetable timetable) {
		getTimetables().add(timetable);
		timetable.setSchool(this);

		return timetable;
	}

	public Timetable removeTimetable(Timetable timetable) {
		getTimetables().remove(timetable);
		timetable.setSchool(null);

		return timetable;
	}

	public List<Timingset> getTimingsets() {
		return this.timingsets;
	}

	public void setTimingsets(List<Timingset> timingsets) {
		this.timingsets = timingsets;
	}

	public Timingset addTimingset(Timingset timingset) {
		getTimingsets().add(timingset);
		timingset.setSchool(this);

		return timingset;
	}

	public Timingset removeTimingset(Timingset timingset) {
		getTimingsets().remove(timingset);
		timingset.setSchool(null);

		return timingset;
	}

}