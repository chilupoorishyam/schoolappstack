package com.gts.cms.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_de_dept_event_resource database table.
 */
@Entity
@Setter
@Getter
@Table(name = "t_de_dept_event_resource")
@NamedQuery(name = "DepartmentEventResource.findAll", query = "SELECT d FROM DepartmentEventResource d")
public class DepartmentEventResource implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_de_resource_id")
    private Long deptResourceId;

    //bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_dept_event_id")
    private DepartmentEvent deptEventId;

    //bi-directional many-to-one association to School
    @JoinColumn(name = "name")
    private String name;

    @Column(name = "institute")
    private String institute;

    @Column(name = "profile_url")
    private String profileUrl;

    @Column(name = "is_active")
    private Boolean isActive;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

}