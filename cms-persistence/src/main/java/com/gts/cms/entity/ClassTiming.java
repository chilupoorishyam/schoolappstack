package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_tt_class_timings database table.
 * 
 */
@Entity
@Table(name="t_tt_class_timings")
@NamedQuery(name="ClassTiming.findAll", query="SELECT c FROM ClassTiming c")
public class ClassTiming implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_class_timings_id")
	private Long classTimingsId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="credit_hours")
	private Integer creditHours;

	@Column(name="end_time")
	private Time endTime;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_break")
	private Boolean isBreak;

	private String name;

	private Integer periodno;

	private String reason;

	@Column(name="sort_order")
	private Integer sortOrder;

	@Column(name="start_time")
	private Time startTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to Timingset
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_timingsets_id")
	private Timingset timingset;

	//bi-directional many-to-one association to Schedule
	@OneToMany(mappedBy="classTiming",fetch = FetchType.LAZY)
	private List<Schedule> schedules;

	public ClassTiming() {
	}

	public Long getClassTimingsId() {
		return this.classTimingsId;
	}

	public void setClassTimingsId(Long classTimingsId) {
		this.classTimingsId = classTimingsId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Integer getCreditHours() {
		return this.creditHours;
	}

	public void setCreditHours(Integer creditHours) {
		this.creditHours = creditHours;
	}

	public Time getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsBreak() {
		return this.isBreak;
	}

	public void setIsBreak(Boolean isBreak) {
		this.isBreak = isBreak;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPeriodno() {
		return this.periodno;
	}

	public void setPeriodno(Integer periodno) {
		this.periodno = periodno;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Time getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Timingset getTimingset() {
		return this.timingset;
	}

	public void setTimingset(Timingset timingset) {
		this.timingset = timingset;
	}

	public List<Schedule> getSchedules() {
		return this.schedules;
	}

	public void setSchedules(List<Schedule> schedules) {
		this.schedules = schedules;
	}

	public Schedule addSchedule(Schedule schedule) {
		getSchedules().add(schedule);
		schedule.setClassTiming(this);

		return schedule;
	}

	public Schedule removeSchedule(Schedule schedule) {
		getSchedules().remove(schedule);
		schedule.setClassTiming(null);

		return schedule;
	}

}