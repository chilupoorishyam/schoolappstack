package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_m_school_suggestions database table.
 * 
 */
@Entity
@Table(name = "t_m_school_suggestions")
@NamedQuery(name = "SchoolSuggestion.findAll", query = "SELECT c FROM SchoolSuggestion c")
public class SchoolSuggestion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_suggestion_id")
	private Long suggestionId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ack_date")
	private Date ackDate;

	@Column(name = "acknowledgement_comments")
	private String acknowledgementComments;

	@Column(name = "acknowledgement_reply")
	private String acknowledgementReply;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;
	
	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_acknowledgedby_emp_id")
	private EmployeeDetail acknowledgedbyEmp;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;
	
	// bi-directional many-to-one association to User
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_suggestionby_user_id")
	private User suggestionbyUser;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_suggestionfor_catdet_id")
	private GeneralDetail suggestionforCat;

	
	@Column(name = "fk_suggestionfor_ids")
	private String suggestionforIds;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_suggestiontype_catdet_id")
	private GeneralDetail suggestiontypeCat;

	@Column(name = "is_acknowledged")
	private Boolean isAcknowledged;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_completed")
	private Boolean isCompleted;

	private String reason;

	@Column(name = "suggestion_description")
	private String suggestionDescription;

	@Column(name = "suggestion_subject")
	private String suggestionSubject;

	private String suggestionby;

	private String suggestionfor;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	public SchoolSuggestion() {
	}

	public Long getSuggestionId() {
		return this.suggestionId;
	}

	public void setSuggestionId(Long suggestionId) {
		this.suggestionId = suggestionId;
	}

	public Date getAckDate() {
		return this.ackDate;
	}

	public void setAckDate(Date ackDate) {
		this.ackDate = ackDate;
	}

	public String getAcknowledgementComments() {
		return this.acknowledgementComments;
	}

	public void setAcknowledgementComments(String acknowledgementComments) {
		this.acknowledgementComments = acknowledgementComments;
	}

	public String getAcknowledgementReply() {
		return this.acknowledgementReply;
	}

	public void setAcknowledgementReply(String acknowledgementReply) {
		this.acknowledgementReply = acknowledgementReply;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	

	public EmployeeDetail getAcknowledgedbyEmp() {
		return acknowledgedbyEmp;
	}

	public void setAcknowledgedbyEmp(EmployeeDetail acknowledgedbyEmp) {
		this.acknowledgedbyEmp = acknowledgedbyEmp;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public User getSuggestionbyUser() {
		return suggestionbyUser;
	}

	public void setSuggestionbyUser(User suggestionbyUser) {
		this.suggestionbyUser = suggestionbyUser;
	}

	public GeneralDetail getSuggestionforCat() {
		return suggestionforCat;
	}

	public void setSuggestionforCat(GeneralDetail suggestionforCat) {
		this.suggestionforCat = suggestionforCat;
	}



	public String getSuggestionforIds() {
		return suggestionforIds;
	}

	public void setSuggestionforIds(String suggestionforIds) {
		this.suggestionforIds = suggestionforIds;
	}

	
	public GeneralDetail getSuggestiontypeCat() {
		return suggestiontypeCat;
	}

	public void setSuggestiontypeCat(GeneralDetail suggestiontypeCat) {
		this.suggestiontypeCat = suggestiontypeCat;
	}

	public Boolean getIsAcknowledged() {
		return this.isAcknowledged;
	}

	public void setIsAcknowledged(Boolean isAcknowledged) {
		this.isAcknowledged = isAcknowledged;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsCompleted() {
		return this.isCompleted;
	}

	public void setIsCompleted(Boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSuggestionDescription() {
		return this.suggestionDescription;
	}

	public void setSuggestionDescription(String suggestionDescription) {
		this.suggestionDescription = suggestionDescription;
	}

	public String getSuggestionSubject() {
		return this.suggestionSubject;
	}

	public void setSuggestionSubject(String suggestionSubject) {
		this.suggestionSubject = suggestionSubject;
	}

	public String getSuggestionby() {
		return this.suggestionby;
	}

	public void setSuggestionby(String suggestionby) {
		this.suggestionby = suggestionby;
	}

	public String getSuggestionfor() {
		return this.suggestionfor;
	}

	public void setSuggestionfor(String suggestionfor) {
		this.suggestionfor = suggestionfor;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}



}