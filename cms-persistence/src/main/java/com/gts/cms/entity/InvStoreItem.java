package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_inv_store_items database table.
 * 
 */
@Entity
@Table(name = "t_inv_store_items")
@NamedQuery(name = "InvStoreItem.findAll", query = "SELECT i FROM InvStoreItem i")
public class InvStoreItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_store_item_id")
	private Long storeItemId;

	@Column(name = "balance_qty")
	private BigDecimal balanceQty;

	@Column(name = "batch_no")
	private String batchNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "issue_balance_qty")
	private BigDecimal issueBalanceQty;

	@Column(name = "item_price")
	private BigDecimal itemPrice;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to InvStoresmaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_store_id")
	private InvStoresmaster invStoresmaster;

	// bi-directional many-to-one association to InvItemmaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_item_id")
	private InvItemmaster invItemMaster;

	// bi-directional many-to-one association to InvUommaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_uom_id")
	private InvUommaster invUommaster;

	// bi-directional many-to-one association to InvItemmaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_issue_uom_id")
	private InvItemmaster issueUOMItemMaster;

	public InvStoreItem() {
	}

	public Long getStoreItemId() {
		return this.storeItemId;
	}

	public void setStoreItemId(Long storeItemId) {
		this.storeItemId = storeItemId;
	}

	public BigDecimal getBalanceQty() {
		return this.balanceQty;
	}

	public void setBalanceQty(BigDecimal balanceQty) {
		this.balanceQty = balanceQty;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public BigDecimal getIssueBalanceQty() {
		return this.issueBalanceQty;
	}

	public void setIssueBalanceQty(BigDecimal issueBalanceQty) {
		this.issueBalanceQty = issueBalanceQty;
	}

	public BigDecimal getItemPrice() {
		return this.itemPrice;
	}

	public void setItemPrice(BigDecimal itemPrice) {
		this.itemPrice = itemPrice;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public InvStoresmaster getInvStoresmaster() {
		return invStoresmaster;
	}

	public void setInvStoresmaster(InvStoresmaster invStoresmaster) {
		this.invStoresmaster = invStoresmaster;
	}

	public InvUommaster getInvUommaster() {
		return invUommaster;
	}

	public void setInvUommaster(InvUommaster invUommaster) {
		this.invUommaster = invUommaster;
	}

	public InvItemmaster getInvItemMaster() {
		return invItemMaster;
	}

	public void setInvItemMaster(InvItemmaster invItemMaster) {
		this.invItemMaster = invItemMaster;
	}

	public InvItemmaster getIssueUOMItemMaster() {
		return issueUOMItemMaster;
	}

	public void setIssueUOMItemMaster(InvItemmaster issueUOMItemMaster) {
		this.issueUOMItemMaster = issueUOMItemMaster;
	}
}