package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_fee_fine database table.
 * 
 */
@Entity
@Table(name="t_fee_fine")
@NamedQuery(name="FeeFine.findAll", query="SELECT f FROM FeeFine f")
public class FeeFine implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_fine_id")
	private Long feeFineId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="fine_name")
	private String fineName;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FeeCategory
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_category_id")
	private FeeCategory feeCategory;

	//bi-directional many-to-one association to FeeParticular
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_particulars_id")
	private FeeParticular feeParticular;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to FeeFineSlab
	@OneToMany(mappedBy="feeFine",cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	private List<FeeFineSlab> feeFineSlabs;

	//bi-directional many-to-one association to FeeStudentwiseFine
	@OneToMany(mappedBy="feeFine",fetch = FetchType.LAZY)
	private List<FeeStudentwiseFine> feeStudentwiseFines;

	public FeeFine() {
	}

	public Long getFeeFineId() {
		return this.feeFineId;
	}

	public void setFeeFineId(Long feeFineId) {
		this.feeFineId = feeFineId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getFineName() {
		return this.fineName;
	}

	public void setFineName(String fineName) {
		this.fineName = fineName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public FeeCategory getFeeCategory() {
		return this.feeCategory;
	}

	public void setFeeCategory(FeeCategory feeCategory) {
		this.feeCategory = feeCategory;
	}

	public FeeParticular getFeeParticular() {
		return this.feeParticular;
	}

	public void setFeeParticular(FeeParticular feeParticular) {
		this.feeParticular = feeParticular;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<FeeFineSlab> getFeeFineSlabs() {
		return this.feeFineSlabs;
	}

	public void setFeeFineSlabs(List<FeeFineSlab> feeFineSlabs) {
		this.feeFineSlabs = feeFineSlabs;
	}

	public FeeFineSlab addFeeFineSlab(FeeFineSlab feeFineSlab) {
		getFeeFineSlabs().add(feeFineSlab);
		feeFineSlab.setFeeFine(this);

		return feeFineSlab;
	}

	public FeeFineSlab removeFeeFineSlab(FeeFineSlab feeFineSlab) {
		getFeeFineSlabs().remove(feeFineSlab);
		feeFineSlab.setFeeFine(null);

		return feeFineSlab;
	}

	public List<FeeStudentwiseFine> getFeeStudentwiseFines() {
		return this.feeStudentwiseFines;
	}

	public void setFeeStudentwiseFines(List<FeeStudentwiseFine> feeStudentwiseFines) {
		this.feeStudentwiseFines = feeStudentwiseFines;
	}

	public FeeStudentwiseFine addFeeStudentwiseFine(FeeStudentwiseFine feeStudentwiseFine) {
		getFeeStudentwiseFines().add(feeStudentwiseFine);
		feeStudentwiseFine.setFeeFine(this);

		return feeStudentwiseFine;
	}

	public FeeStudentwiseFine removeFeeStudentwiseFine(FeeStudentwiseFine feeStudentwiseFine) {
		getFeeStudentwiseFines().remove(feeStudentwiseFine);
		feeStudentwiseFine.setFeeFine(null);

		return feeStudentwiseFine;
	}

}