package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_hr_survey_details database table.
 * 
 */
@Entity
@Table(name="t_hr_survey_details")
@NamedQuery(name="SurveyDetail.findAll", query="SELECT s FROM SurveyDetail s")
public class SurveyDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_survey_details_id")
	private Long surveyDetailsId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="question_sort_order")
	private Integer questionSortOrder;

	private String reason;

	@Column(name="section_sort_order")
	private Integer sectionSortOrder;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FeedbackQuestion
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fb_question_id")
	private FeedbackQuestion feedbackQuestion;

	//bi-directional many-to-one association to FeedbackSection
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fb_section_id")
	private FeedbackSection feedbackSection;

	//bi-directional many-to-one association to SurveyForm
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_survey_form_id")
	private SurveyForm surveyForm;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to SurveyFeedbackDetail
	@OneToMany(mappedBy="surveyDetail",fetch = FetchType.LAZY)
	private List<SurveyFeedbackDetail> surveyFeedbackDetails;

	public SurveyDetail() {
	}

	public Long getSurveyDetailsId() {
		return this.surveyDetailsId;
	}

	public void setSurveyDetailsId(Long surveyDetailsId) {
		this.surveyDetailsId = surveyDetailsId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getQuestionSortOrder() {
		return this.questionSortOrder;
	}

	public void setQuestionSortOrder(Integer questionSortOrder) {
		this.questionSortOrder = questionSortOrder;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getSectionSortOrder() {
		return this.sectionSortOrder;
	}

	public void setSectionSortOrder(Integer sectionSortOrder) {
		this.sectionSortOrder = sectionSortOrder;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public FeedbackQuestion getFeedbackQuestion() {
		return this.feedbackQuestion;
	}

	public void setFeedbackQuestion(FeedbackQuestion feedbackQuestion) {
		this.feedbackQuestion = feedbackQuestion;
	}

	public FeedbackSection getFeedbackSection() {
		return this.feedbackSection;
	}

	public void setFeedbackSection(FeedbackSection feedbackSection) {
		this.feedbackSection = feedbackSection;
	}

	public SurveyForm getSurveyForm() {
		return this.surveyForm;
	}

	public void setSurveyForm(SurveyForm surveyForm) {
		this.surveyForm = surveyForm;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<SurveyFeedbackDetail> getSurveyFeedbackDetails() {
		return this.surveyFeedbackDetails;
	}

	public void setSurveyFeedbackDetails(List<SurveyFeedbackDetail> surveyFeedbackDetails) {
		this.surveyFeedbackDetails = surveyFeedbackDetails;
	}

	public SurveyFeedbackDetail addSurveyFeedbackDetail(SurveyFeedbackDetail surveyFeedbackDetail) {
		getSurveyFeedbackDetails().add(surveyFeedbackDetail);
		surveyFeedbackDetail.setSurveyDetail(this);

		return surveyFeedbackDetail;
	}

	public SurveyFeedbackDetail removeSurveyFeedbackDetail(SurveyFeedbackDetail surveyFeedbackDetail) {
		getSurveyFeedbackDetails().remove(surveyFeedbackDetail);
		surveyFeedbackDetail.setSurveyDetail(null);

		return surveyFeedbackDetail;
	}

}