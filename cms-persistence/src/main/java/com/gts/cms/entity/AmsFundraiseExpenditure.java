package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the t_ams_fundraise_expenditure database table.
 * 
 */
@Entity
@Table(name = "t_ams_fundraise_expenditure")
@NamedQuery(name = "AmsFundraiseExpenditure.findAll", query = "SELECT f FROM AmsFundraiseExpenditure f")
public class AmsFundraiseExpenditure implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_fundraise_expenditure_id")
	private Long fundraiseExpenditureId;

	@Column(name = "audit_amount")
	private BigDecimal auditAmount;

	@Column(name = "audit_comments")
	private String auditComments;

	@Column(name = "conclusion_message")
	private String conclusionMessage;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	@Column(name = "expended_on")
	private Date expendedOn;

	@Column(name = "expenditure_amount")
	private BigDecimal expenditureAmount;

	@Column(name = "expenditure_attachment1")
	private String expenditureAttachment1;

	@Column(name = "expenditure_attachment2")
	private String expenditureAttachment2;

	@Column(name = "expenditure_for")
	private String expenditureFor;

	@Column(name = "expenditure_notes")
	private String expenditureNotes;

	@ManyToOne
	@JoinColumn(name = "fk_audit_emp_id")
	private EmployeeDetail auditEmp;

	@ManyToOne
	@JoinColumn(name = "fk_expended_emp_id")
	private EmployeeDetail expendedEmp;

	@ManyToOne
	@JoinColumn(name = "fk_paymentmode_catdet_id")
	private GeneralDetail paymentmodeCat;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_audit_completed")
	private Boolean isAuditCompleted;

	private String reason;

	@Column(name = "received_by")
	private String receivedBy;

	@Column(name = "received_by_details")
	private String receivedByDetails;

	@Column(name = "ref_amount_doc_url")
	private String refAmountDocUrl;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to FundraiseEvent
	@ManyToOne
	@JoinColumn(name = "fk_fundraise_event_id")
	private AmsFundraiseEvent fundraiseEvent;

	public AmsFundraiseExpenditure() {
	}

	public Long getFundraiseExpenditureId() {
		return this.fundraiseExpenditureId;
	}

	public void setFundraiseExpenditureId(Long fundraiseExpenditureId) {
		this.fundraiseExpenditureId = fundraiseExpenditureId;
	}

	public BigDecimal getAuditAmount() {
		return this.auditAmount;
	}

	public void setAuditAmount(BigDecimal auditAmount) {
		this.auditAmount = auditAmount;
	}

	public String getAuditComments() {
		return this.auditComments;
	}

	public void setAuditComments(String auditComments) {
		this.auditComments = auditComments;
	}

	public String getConclusionMessage() {
		return this.conclusionMessage;
	}

	public void setConclusionMessage(String conclusionMessage) {
		this.conclusionMessage = conclusionMessage;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getExpendedOn() {
		return this.expendedOn;
	}

	public void setExpendedOn(Date expendedOn) {
		this.expendedOn = expendedOn;
	}

	public BigDecimal getExpenditureAmount() {
		return this.expenditureAmount;
	}

	public void setExpenditureAmount(BigDecimal expenditureAmount) {
		this.expenditureAmount = expenditureAmount;
	}

	public String getExpenditureAttachment1() {
		return this.expenditureAttachment1;
	}

	public void setExpenditureAttachment1(String expenditureAttachment1) {
		this.expenditureAttachment1 = expenditureAttachment1;
	}

	public String getExpenditureAttachment2() {
		return this.expenditureAttachment2;
	}

	public void setExpenditureAttachment2(String expenditureAttachment2) {
		this.expenditureAttachment2 = expenditureAttachment2;
	}

	public String getExpenditureFor() {
		return this.expenditureFor;
	}

	public void setExpenditureFor(String expenditureFor) {
		this.expenditureFor = expenditureFor;
	}

	public String getExpenditureNotes() {
		return this.expenditureNotes;
	}

	public void setExpenditureNotes(String expenditureNotes) {
		this.expenditureNotes = expenditureNotes;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsAuditCompleted() {
		return this.isAuditCompleted;
	}

	public void setIsAuditCompleted(Boolean isAuditCompleted) {
		this.isAuditCompleted = isAuditCompleted;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReceivedBy() {
		return this.receivedBy;
	}

	public void setReceivedBy(String receivedBy) {
		this.receivedBy = receivedBy;
	}

	public String getReceivedByDetails() {
		return this.receivedByDetails;
	}

	public void setReceivedByDetails(String receivedByDetails) {
		this.receivedByDetails = receivedByDetails;
	}

	public String getRefAmountDocUrl() {
		return this.refAmountDocUrl;
	}

	public void setRefAmountDocUrl(String refAmountDocUrl) {
		this.refAmountDocUrl = refAmountDocUrl;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getAuditEmp() {
		return auditEmp;
	}

	public void setAuditEmp(EmployeeDetail auditEmp) {
		this.auditEmp = auditEmp;
	}

	public EmployeeDetail getExpendedEmp() {
		return expendedEmp;
	}

	public void setExpendedEmp(EmployeeDetail expendedEmp) {
		this.expendedEmp = expendedEmp;
	}

	public GeneralDetail getPaymentmodeCat() {
		return paymentmodeCat;
	}

	public void setPaymentmodeCat(GeneralDetail paymentmodeCat) {
		this.paymentmodeCat = paymentmodeCat;
	}

	public AmsFundraiseEvent getFundraiseEvent() {
		return fundraiseEvent;
	}

	public void setFundraiseEvent(AmsFundraiseEvent fundraiseEvent) {
		this.fundraiseEvent = fundraiseEvent;
	}
}