package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_fin_transaction database table.
 */
@Entity
@Table(name = "t_fin_transaction")
@NamedQuery(name = "FinTransaction.findAll", query = "SELECT f FROM FinTransaction f")
public class FinTransaction implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_fin_transaction_id")
    private Long finTransactionId;

    private BigDecimal amount;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    private String description;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    private String remarks;

    private String title;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "transaction_date")
    private Date transactionDate;

    @Column(name = "transaction_type")
    private String transactionType;

    @Column(name = "voucher_url")
    private String voucherUrl;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    //bi-directional many-to-one association to FinSubCategory
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_fin_sub_category_id")
    private FinSubCategory finSubCategory;

    //bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_school_id")
    private School school;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_financial_year_id")
    private FinancialYear financialYear;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_vouchertype_catdet_id ")
    private GeneralDetail vouchertypeCatdetId;

    private String transactionNumber;

    //bi-directional many-to-one association to FinTransactionHistory
    @OneToMany(mappedBy = "finTransaction", fetch = FetchType.LAZY)
    private List<FinTransactionHistory> finTransactionHistories;

    public FinTransaction() {
    }

    public Long getFinTransactionId() {
        return this.finTransactionId;
    }

    public void setFinTransactionId(Long finTransactionId) {
        this.finTransactionId = finTransactionId;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getCreatedDt() {
        return this.createdDt;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    public Long getCreatedUser() {
        return this.createdUser;
    }

    public void setCreatedUser(Long createdUser) {
        this.createdUser = createdUser;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getTransactionDate() {
        return this.transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionType() {
        return this.transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Date getUpdatedDt() {
        return this.updatedDt;
    }

    public void setUpdatedDt(Date updatedDt) {
        this.updatedDt = updatedDt;
    }

    public Long getUpdatedUser() {
        return this.updatedUser;
    }

    public void setUpdatedUser(Long updatedUser) {
        this.updatedUser = updatedUser;
    }

    public FinSubCategory getFinSubCategory() {
        return this.finSubCategory;
    }

    public void setFinSubCategory(FinSubCategory finSubCategory) {
        this.finSubCategory = finSubCategory;
    }

    public School getSchool() {
        return this.school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public List<FinTransactionHistory> getFinTransactionHistories() {
        return this.finTransactionHistories;
    }

    public void setFinTransactionHistories(List<FinTransactionHistory> finTransactionHistories) {
        this.finTransactionHistories = finTransactionHistories;
    }

    public FinTransactionHistory addFinTransactionHistory(FinTransactionHistory finTransactionHistory) {
        getFinTransactionHistories().add(finTransactionHistory);
        finTransactionHistory.setFinTransaction(this);

        return finTransactionHistory;
    }

    public FinTransactionHistory removeFinTransactionHistory(FinTransactionHistory finTransactionHistory) {
        getFinTransactionHistories().remove(finTransactionHistory);
        finTransactionHistory.setFinTransaction(null);

        return finTransactionHistory;
    }

    public FinancialYear getFinancialYear() {
        return financialYear;
    }

    public void setFinancialYear(FinancialYear financialYear) {
        this.financialYear = financialYear;
    }

    public GeneralDetail getVouchertypeCatdetId() {
        return vouchertypeCatdetId;
    }

    public void setVouchertypeCatdetId(GeneralDetail vouchertypeCatdetId) {
        this.vouchertypeCatdetId = vouchertypeCatdetId;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public String getVoucherUrl() {
        return voucherUrl;
    }

    public void setVoucherUrl(String voucherUrl) {
        this.voucherUrl = voucherUrl;
    }
}