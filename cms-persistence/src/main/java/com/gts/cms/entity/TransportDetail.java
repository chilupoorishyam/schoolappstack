package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_tm_transport_details database table.
 * 
 */
@Entity
@Table(name="t_tm_transport_details")
@NamedQuery(name="TransportDetail.findAll", query="SELECT t FROM TransportDetail t")
public class TransportDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_transport_detail_id")
	private Long transportDetailId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;
	
	@Column(name="transport_name")
	private String transportName;

	//bi-directional many-to-one association to DistanceFee
	@OneToMany(mappedBy="transportDetail",fetch = FetchType.LAZY)
	private List<DistanceFee> distanceFees;

	//bi-directional many-to-one association to Driver
	@OneToMany(mappedBy="transportDetail",fetch = FetchType.LAZY)
	private List<Driver> drivers;

	//bi-directional many-to-one association to FeePayment
	@OneToMany(mappedBy="transportDetail",fetch = FetchType.LAZY)
	private List<TransportFeePayment> feePayments;

	//bi-directional many-to-one association to Helper
	@OneToMany(mappedBy="transportDetail",fetch = FetchType.LAZY)
	private List<Helper> helpers;

	//bi-directional many-to-one association to Route
	@OneToMany(mappedBy="transportDetail",fetch = FetchType.LAZY)
	private List<Route> routes;

	//bi-directional many-to-one association to RouteStop
	@OneToMany(mappedBy="transportDetail",fetch = FetchType.LAZY)
	private List<RouteStop> routeStops;

	//bi-directional many-to-one association to TransportAllocation
	@OneToMany(mappedBy="transportDetail",fetch = FetchType.LAZY)
	private List<TransportAllocation> transportAllocations;

	//bi-directional many-to-one association to Campus
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_campus_id")
	private Campus campus;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to VechicleRoute
	@OneToMany(mappedBy="transportDetail",fetch = FetchType.LAZY)
	private List<VechicleRoute> vechicleRoutes;

	//bi-directional many-to-one association to VehicleDetail
	@OneToMany(mappedBy="transportDetail",fetch = FetchType.LAZY)
	private List<VehicleDetail> vehicleDetails;

	//bi-directional many-to-one association to VehicleDriver
	@OneToMany(mappedBy="transportDetail",fetch = FetchType.LAZY)
	private List<VehicleDriver> vehicleDrivers;

	//bi-directional many-to-one association to VehicleMaintenance
	@OneToMany(mappedBy="transportDetail",fetch = FetchType.LAZY)
	private List<VehicleMaintenance> vehicleMaintenances;

	public TransportDetail() {
	}

	public Long getTransportDetailId() {
		return this.transportDetailId;
	}

	public void setTransportDetailId(Long transportDetailId) {
		this.transportDetailId = transportDetailId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<DistanceFee> getDistanceFees() {
		return this.distanceFees;
	}

	public void setDistanceFees(List<DistanceFee> distanceFees) {
		this.distanceFees = distanceFees;
	}

	public DistanceFee addDistanceFee(DistanceFee distanceFee) {
		getDistanceFees().add(distanceFee);
		distanceFee.setTransportDetail(this);

		return distanceFee;
	}

	public DistanceFee removeDistanceFee(DistanceFee distanceFee) {
		getDistanceFees().remove(distanceFee);
		distanceFee.setTransportDetail(null);

		return distanceFee;
	}

	public List<Driver> getDrivers() {
		return this.drivers;
	}

	public void setDrivers(List<Driver> drivers) {
		this.drivers = drivers;
	}

	public Driver addDriver(Driver driver) {
		getDrivers().add(driver);
		driver.setTransportDetail(this);

		return driver;
	}

	public Driver removeDriver(Driver driver) {
		getDrivers().remove(driver);
		driver.setTransportDetail(null);

		return driver;
	}

	public List<TransportFeePayment> getFeePayments() {
		return this.feePayments;
	}

	public void setFeePayments(List<TransportFeePayment> feePayments) {
		this.feePayments = feePayments;
	}

	public TransportFeePayment addFeePayment(TransportFeePayment feePayment) {
		getFeePayments().add(feePayment);
		feePayment.setTransportDetail(this);

		return feePayment;
	}

	public TransportFeePayment removeFeePayment(TransportFeePayment feePayment) {
		getFeePayments().remove(feePayment);
		feePayment.setTransportDetail(null);

		return feePayment;
	}

	public List<Helper> getHelpers() {
		return this.helpers;
	}

	public void setHelpers(List<Helper> helpers) {
		this.helpers = helpers;
	}

	public Helper addHelper(Helper helper) {
		getHelpers().add(helper);
		helper.setTransportDetail(this);

		return helper;
	}

	public Helper removeHelper(Helper helper) {
		getHelpers().remove(helper);
		helper.setTransportDetail(null);

		return helper;
	}

	public List<Route> getRoutes() {
		return this.routes;
	}

	public void setRoutes(List<Route> routes) {
		this.routes = routes;
	}

	public Route addRoute(Route route) {
		getRoutes().add(route);
		route.setTransportDetail(this);

		return route;
	}

	public Route removeRoute(Route route) {
		getRoutes().remove(route);
		route.setTransportDetail(null);

		return route;
	}

	public List<RouteStop> getRouteStops() {
		return this.routeStops;
	}

	public void setRouteStops(List<RouteStop> routeStops) {
		this.routeStops = routeStops;
	}

	public RouteStop addRouteStop(RouteStop routeStop) {
		getRouteStops().add(routeStop);
		routeStop.setTransportDetail(this);

		return routeStop;
	}

	public RouteStop removeRouteStop(RouteStop routeStop) {
		getRouteStops().remove(routeStop);
		routeStop.setTransportDetail(null);

		return routeStop;
	}

	public List<TransportAllocation> getTransportAllocations() {
		return this.transportAllocations;
	}

	public void setTransportAllocations(List<TransportAllocation> transportAllocations) {
		this.transportAllocations = transportAllocations;
	}

	public TransportAllocation addTransportAllocation(TransportAllocation transportAllocation) {
		getTransportAllocations().add(transportAllocation);
		transportAllocation.setTransportDetail(this);

		return transportAllocation;
	}

	public TransportAllocation removeTransportAllocation(TransportAllocation transportAllocation) {
		getTransportAllocations().remove(transportAllocation);
		transportAllocation.setTransportDetail(null);

		return transportAllocation;
	}

	public Campus getCampus() {
		return this.campus;
	}

	public void setCampus(Campus campus) {
		this.campus = campus;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public List<VechicleRoute> getVechicleRoutes() {
		return this.vechicleRoutes;
	}

	public void setVechicleRoutes(List<VechicleRoute> vechicleRoutes) {
		this.vechicleRoutes = vechicleRoutes;
	}

	public VechicleRoute addVechicleRoute(VechicleRoute vechicleRoute) {
		getVechicleRoutes().add(vechicleRoute);
		vechicleRoute.setTransportDetail(this);

		return vechicleRoute;
	}

	public VechicleRoute removeVechicleRoute(VechicleRoute vechicleRoute) {
		getVechicleRoutes().remove(vechicleRoute);
		vechicleRoute.setTransportDetail(null);

		return vechicleRoute;
	}

	public List<VehicleDetail> getVehicleDetails() {
		return this.vehicleDetails;
	}

	public void setVehicleDetails(List<VehicleDetail> vehicleDetails) {
		this.vehicleDetails = vehicleDetails;
	}

	public VehicleDetail addVehicleDetail(VehicleDetail vehicleDetail) {
		getVehicleDetails().add(vehicleDetail);
		vehicleDetail.setTransportDetail(this);

		return vehicleDetail;
	}

	public VehicleDetail removeVehicleDetail(VehicleDetail vehicleDetail) {
		getVehicleDetails().remove(vehicleDetail);
		vehicleDetail.setTransportDetail(null);

		return vehicleDetail;
	}

	public List<VehicleDriver> getVehicleDrivers() {
		return this.vehicleDrivers;
	}

	public void setVehicleDrivers(List<VehicleDriver> vehicleDrivers) {
		this.vehicleDrivers = vehicleDrivers;
	}

	public VehicleDriver addVehicleDriver(VehicleDriver vehicleDriver) {
		getVehicleDrivers().add(vehicleDriver);
		vehicleDriver.setTransportDetail(this);

		return vehicleDriver;
	}

	public VehicleDriver removeVehicleDriver(VehicleDriver vehicleDriver) {
		getVehicleDrivers().remove(vehicleDriver);
		vehicleDriver.setTransportDetail(null);

		return vehicleDriver;
	}

	public List<VehicleMaintenance> getVehicleMaintenances() {
		return this.vehicleMaintenances;
	}

	public void setVehicleMaintenances(List<VehicleMaintenance> vehicleMaintenances) {
		this.vehicleMaintenances = vehicleMaintenances;
	}

	public VehicleMaintenance addVehicleMaintenance(VehicleMaintenance vehicleMaintenance) {
		getVehicleMaintenances().add(vehicleMaintenance);
		vehicleMaintenance.setTransportDetail(this);

		return vehicleMaintenance;
	}

	public VehicleMaintenance removeVehicleMaintenance(VehicleMaintenance vehicleMaintenance) {
		getVehicleMaintenances().remove(vehicleMaintenance);
		vehicleMaintenance.setTransportDetail(null);

		return vehicleMaintenance;
	}

	public String getTransportName() {
		return transportName;
	}

	public void setTransportName(String transportName) {
		this.transportName = transportName;
	}
	
}