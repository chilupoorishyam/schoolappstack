package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_cm_staff_courseyr_subjects database table.
 * 
 */
@Entity
@Table(name="t_cm_staff_courseyr_subjects")
@NamedQuery(name="StaffCourseyrSubject.findAll", query="SELECT s FROM StaffCourseyrSubject s")
public class StaffCourseyrSubject implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_staff_courseyr_subject_id")
	private Long staffCourseyrSubjectId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	@Column(name="from_date")
	private Date fromDate;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.DATE)
	@Column(name="to_date")
	private Date toDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to SubjectCourseyear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_sub_courseyear_id")
	private SubjectCourseyear subjectCourseyear;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_id")
	private EmployeeDetail employeeDetail;

	//bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to StaffSyllabusPlan
	@OneToMany(mappedBy="staffCourseyrSubject",fetch = FetchType.LAZY)
	private List<StaffSyllabusPlan> staffSyllabusPlans;

	//bi-directional many-to-one association to ActualClassesSchedule
	/*@OneToMany(mappedBy="staffCourseyrSubject")
	private List<ActualClassesSchedule> actualClassesSchedules;*/

	//bi-directional many-to-one association to StaffProxy
	@OneToMany(mappedBy="staffCourseyrSubject",fetch = FetchType.LAZY)
	private List<StaffProxy> staffProxies;

	//bi-directional many-to-one association to SubjectResource
	@OneToMany(mappedBy="staffCourseyrSubject",fetch = FetchType.LAZY)
	private List<SubjectResource> subjectResources;

	public StaffCourseyrSubject() {
	}

	public Long getStaffCourseyrSubjectId() {
		return this.staffCourseyrSubjectId;
	}

	public void setStaffCourseyrSubjectId(Long staffCourseyrSubjectId) {
		this.staffCourseyrSubjectId = staffCourseyrSubjectId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public SubjectCourseyear getSubjectCourseyear() {
		return this.subjectCourseyear;
	}

	public void setSubjectCourseyear(SubjectCourseyear subjectCourseyear) {
		this.subjectCourseyear = subjectCourseyear;
	}

	public EmployeeDetail getEmployeeDetail() {
		return this.employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<StaffSyllabusPlan> getStaffSyllabusPlans() {
		return this.staffSyllabusPlans;
	}

	public void setStaffSyllabusPlans(List<StaffSyllabusPlan> staffSyllabusPlans) {
		this.staffSyllabusPlans = staffSyllabusPlans;
	}

	public StaffSyllabusPlan addStaffSyllabusPlan(StaffSyllabusPlan staffSyllabusPlan) {
		getStaffSyllabusPlans().add(staffSyllabusPlan);
		staffSyllabusPlan.setStaffCourseyrSubject(this);

		return staffSyllabusPlan;
	}

	public StaffSyllabusPlan removeStaffSyllabusPlan(StaffSyllabusPlan staffSyllabusPlan) {
		getStaffSyllabusPlans().remove(staffSyllabusPlan);
		staffSyllabusPlan.setStaffCourseyrSubject(null);

		return staffSyllabusPlan;
	}

	/*public List<ActualClassesSchedule> getActualClassesSchedules() {
		return this.actualClassesSchedules;
	}

	public void setActualClassesSchedules(List<ActualClassesSchedule> actualClassesSchedules) {
		this.actualClassesSchedules = actualClassesSchedules;
	}*/

/*	public ActualClassesSchedule addActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		getActualClassesSchedules().add(actualClassesSchedule);
		actualClassesSchedule.setStaffCourseyrSubject(this);

		return actualClassesSchedule;
	}

	public ActualClassesSchedule removeActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		getActualClassesSchedules().remove(actualClassesSchedule);
		actualClassesSchedule.setStaffCourseyrSubject(null);

		return actualClassesSchedule;
	}
*/
	public List<StaffProxy> getStaffProxies() {
		return this.staffProxies;
	}

	public void setStaffProxies(List<StaffProxy> staffProxies) {
		this.staffProxies = staffProxies;
	}

	public StaffProxy addStaffProxy(StaffProxy staffProxy) {
		getStaffProxies().add(staffProxy);
		staffProxy.setStaffCourseyrSubject(this);

		return staffProxy;
	}

	public StaffProxy removeStaffProxy(StaffProxy staffProxy) {
		getStaffProxies().remove(staffProxy);
		staffProxy.setStaffCourseyrSubject(null);

		return staffProxy;
	}

	public List<SubjectResource> getSubjectResources() {
		return this.subjectResources;
	}

	public void setSubjectResources(List<SubjectResource> subjectResources) {
		this.subjectResources = subjectResources;
	}

	public SubjectResource addSubjectResource(SubjectResource subjectResource) {
		getSubjectResources().add(subjectResource);
		subjectResource.setStaffCourseyrSubject(this);

		return subjectResource;
	}

	public SubjectResource removeSubjectResource(SubjectResource subjectResource) {
		getSubjectResources().remove(subjectResource);
		subjectResource.setStaffCourseyrSubject(null);

		return subjectResource;
	}

}