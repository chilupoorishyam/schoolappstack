package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_cm_subject_units database table.
 * 
 */
@Entity
@Table(name="t_cm_subject_units")
@NamedQuery(name="SubjectUnit.findAll", query="SELECT s FROM SubjectUnit s")
public class SubjectUnit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_sub_units_id")
	private Long subjectUnitsId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Column(name="unit_code")
	private String unitCode;

	@Column(name="unit_description")
	private String unitDescription;

	@Column(name="unit_name")
	private String unitName;
	
	@Column(name="no_of_periods")
	private Integer noOfPeriods;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to SubjectUnitTopic
	@OneToMany(mappedBy="subjectUnit",cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	private List<SubjectUnitTopic> subjectUnitTopics;

	//bi-directional many-to-one association to Subjectregulation
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subreg_id")
	private Subjectregulation subjectregulation;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	public SubjectUnit() {
	}

	public Long getSubjectUnitsId() {
		return this.subjectUnitsId;
	}

	public void setSubjectUnitsId(Long subjectUnitsId) {
		this.subjectUnitsId = subjectUnitsId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public String getUnitDescription() {
		return this.unitDescription;
	}

	public void setUnitDescription(String unitDescription) {
		this.unitDescription = unitDescription;
	}

	public String getUnitName() {
		return this.unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<SubjectUnitTopic> getSubjectUnitTopics() {
		return this.subjectUnitTopics;
	}

	public void setSubjectUnitTopics(List<SubjectUnitTopic> subjectUnitTopics) {
		this.subjectUnitTopics = subjectUnitTopics;
	}

	public SubjectUnitTopic addSubjectUnitTopic(SubjectUnitTopic subjectUnitTopic) {
		getSubjectUnitTopics().add(subjectUnitTopic);
		subjectUnitTopic.setSubjectUnit(this);

		return subjectUnitTopic;
	}

	public SubjectUnitTopic removeSubjectUnitTopic(SubjectUnitTopic subjectUnitTopic) {
		getSubjectUnitTopics().remove(subjectUnitTopic);
		subjectUnitTopic.setSubjectUnit(null);

		return subjectUnitTopic;
	}

	public Subjectregulation getSubjectregulation() {
		return this.subjectregulation;
	}

	public void setSubjectregulation(Subjectregulation subjectregulation) {
		this.subjectregulation = subjectregulation;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Integer getNoOfPeriods() {
		return noOfPeriods;
	}

	public void setNoOfPeriods(Integer noOfPeriods) {
		this.noOfPeriods = noOfPeriods;
	}

}