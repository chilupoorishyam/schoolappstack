package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_dl_member_assessment_summary database table.
 * 
 */
@Entity
@Table(name = "t_dl_member_assessment_summary")
@NamedQuery(name = "MemberAssessmentSummary.findAll", query = "SELECT m FROM MemberAssessmentSummary m")
public class MemberAssessmentSummary implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_dl_member_assessment_summary_id")
	private Long memberAssessmentSummaryId;

	@ManyToOne
	@JoinColumn(name = "fk_dl_course_member_id")
	private CourseMember courseMember;

	@ManyToOne
	@JoinColumn(name = "fk_dl_onlinecourse_id")
	private OnlineCourses onlineCourses;

	@ManyToOne
	@JoinColumn(name = "fk_subscription_membership_id")
	private DigitalLibraryMember subscriptionMember;

	@ManyToOne
	@JoinColumn(name = "fk_dl_assessment_id")
	private Assessment assessment;

	@Column(name = "attempt_no")
	private Integer attemptNo;

	@ManyToOne
	@JoinColumn(name = "fk_dl_member_assessment_id")
	private MemberAssessment memberAssessment;

	@Column(name = "is_attempts_completed")
	private Boolean isAttemptsCompleted;

	@Column(name = "total_marks")
	private Integer totalMarks;

	private Integer percentage;

	@Column(name = "is_certification")
	private Boolean isCertification;

	@Column(name = "result_url")
	private String resultUrl;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	public Long getMemberAssessmentSummaryId() {
		return memberAssessmentSummaryId;
	}

	public void setMemberAssessmentSummaryId(Long memberAssessmentSummaryId) {
		this.memberAssessmentSummaryId = memberAssessmentSummaryId;
	}

	public CourseMember getCourseMember() {
		return courseMember;
	}

	public void setCourseMember(CourseMember courseMember) {
		this.courseMember = courseMember;
	}

	public OnlineCourses getOnlineCourses() {
		return onlineCourses;
	}

	public void setOnlineCourses(OnlineCourses onlineCourses) {
		this.onlineCourses = onlineCourses;
	}

	public DigitalLibraryMember getSubscriptionMember() {
		return subscriptionMember;
	}

	public void setSubscriptionMember(DigitalLibraryMember subscriptionMember) {
		this.subscriptionMember = subscriptionMember;
	}

	public Assessment getAssessment() {
		return assessment;
	}

	public void setAssessment(Assessment assessment) {
		this.assessment = assessment;
	}

	public Integer getAttemptNo() {
		return attemptNo;
	}

	public void setAttemptNo(Integer attemptNo) {
		this.attemptNo = attemptNo;
	}

	public MemberAssessment getMemberAssessment() {
		return memberAssessment;
	}

	public void setMemberAssessment(MemberAssessment memberAssessment) {
		this.memberAssessment = memberAssessment;
	}

	public Boolean getIsAttemptsCompleted() {
		return isAttemptsCompleted;
	}

	public void setIsAttemptsCompleted(Boolean isAttemptsCompleted) {
		this.isAttemptsCompleted = isAttemptsCompleted;
	}

	public Integer getTotalMarks() {
		return totalMarks;
	}

	public void setTotalMarks(Integer totalMarks) {
		this.totalMarks = totalMarks;
	}

	public Integer getPercentage() {
		return percentage;
	}

	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}

	public Boolean getIsCertification() {
		return isCertification;
	}

	public void setIsCertification(Boolean isCertification) {
		this.isCertification = isCertification;
	}

	public String getResultUrl() {
		return resultUrl;
	}

	public void setResultUrl(String resultUrl) {
		this.resultUrl = resultUrl;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

}