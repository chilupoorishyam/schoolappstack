package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_grv_grievance_committees database table.
 * 
 */
@Entity
@Table(name = "t_grv_grievance_committees")
@NamedQuery(name = "GrievanceCommittee.findAll", query = "SELECT g FROM GrievanceCommittee g")
public class GrievanceCommittee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_grv_committee_id")
	private Long grvCommitteeId;

	@Column(name = "committee_code")
	private String committeeCode;

	@Column(name = "committee_name")
	private String committeeName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "escalate_in_days")
	private Long escalateInDays;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	@Column(name = "hierarchy_level")
	private Long hierarchyLevel;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to CommitteeMember
	@OneToMany(mappedBy = "grievanceCommittee")
	private List<CommitteeMember> committeeMembers;

	// bi-directional many-to-one association to Complaint
	@OneToMany(mappedBy = "grievanceCommittee")
	private List<Complaint> complaints;

	// bi-directional many-to-one association to ComplaintsWf
	@OneToMany(mappedBy = "fromGrievanceCommittee")
	private List<ComplaintsWf> complaintsWfs1;

	// bi-directional many-to-one association to ComplaintsWf
	@OneToMany(mappedBy = "toGrievanceCommittee")
	private List<ComplaintsWf> complaintsWfs2;

	public GrievanceCommittee() {
	}

	public Long getGrvCommitteeId() {
		return this.grvCommitteeId;
	}

	public void setGrvCommitteeId(Long grvCommitteeId) {
		this.grvCommitteeId = grvCommitteeId;
	}

	public String getCommitteeCode() {
		return this.committeeCode;
	}

	public void setCommitteeCode(String committeeCode) {
		this.committeeCode = committeeCode;
	}

	public String getCommitteeName() {
		return this.committeeName;
	}

	public void setCommitteeName(String committeeName) {
		this.committeeName = committeeName;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Long getEscalateInDays() {
		return this.escalateInDays;
	}

	public void setEscalateInDays(Long escalateInDays) {
		this.escalateInDays = escalateInDays;
	}

	public Long getHierarchyLevel() {
		return this.hierarchyLevel;
	}

	public void setHierarchyLevel(Long hierarchyLevel) {
		this.hierarchyLevel = hierarchyLevel;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public List<CommitteeMember> getCommitteeMembers() {
		return committeeMembers;
	}

	public void setCommitteeMembers(List<CommitteeMember> committeeMembers) {
		this.committeeMembers = committeeMembers;
	}

	public List<Complaint> getComplaints() {
		return complaints;
	}

	public void setComplaints(List<Complaint> complaints) {
		this.complaints = complaints;
	}

	public List<ComplaintsWf> getComplaintsWfs1() {
		return complaintsWfs1;
	}

	public void setComplaintsWfs1(List<ComplaintsWf> complaintsWfs1) {
		this.complaintsWfs1 = complaintsWfs1;
	}

	public List<ComplaintsWf> getComplaintsWfs2() {
		return complaintsWfs2;
	}

	public void setComplaintsWfs2(List<ComplaintsWf> complaintsWfs2) {
		this.complaintsWfs2 = complaintsWfs2;
	}

}