package com.gts.cms.entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_pa_training_details database table.
 * 
 */
@Entity
@Table(name = "t_pa_training_details")
@NamedQuery(name = "TrainingDetail.findAll", query = "SELECT t FROM TrainingDetail t")
public class TrainingDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_pa_traning_det_id")
	private Long traningDetId;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to Training
	@ManyToOne
	@JoinColumn(name = "fk_pa_training_id")
	private Training training;

	@Column(name = "training_detail_title")
	private String trainingDetailTitle;

	@Column(name = "training_detail_description")
	private String trainingDetailDesc;

	@Column(name = "trainer_name")
	private String trainerName;

	@Column(name = "trainer_details")
	private String trainerDetails;

	@Column(name = "is_recurring")
	private Boolean isRecurring;

	@Column(name = "fk_day_ids")
	private String fkDayIds;

	@Column(name = "start_time")
	private Time startTime;

	@Column(name = "end_time")
	private Time endTime;

	@Column(name = "no_of_students")
	private Integer noOfStudents;

	// bi-directional many-to-one association to Room
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_room_id")
	private Room room;

	private String location;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_audience_type_catdet_id")
	private GeneralDetail audienceTypeCat;

	@Column(name = "category_name")
	private String categoryName;

	@Column(name = "category_value")
	private String categoryValue;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to TrainingSession
	@OneToMany(mappedBy = "trainingDetail")
	private List<TrainingSession> trainingSessions;

	public TrainingDetail() {
	}

	public Long getTraningDetId() {
		return this.traningDetId;
	}

	public void setTraningDetId(Long traningDetId) {
		this.traningDetId = traningDetId;
	}

	public String getCategoryName() {
		return this.categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryValue() {
		return this.categoryValue;
	}

	public void setCategoryValue(String categoryValue) {
		this.categoryValue = categoryValue;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	
	public GeneralDetail getAudienceTypeCat() {
		return audienceTypeCat;
	}

	public void setAudienceTypeCat(GeneralDetail audienceTypeCat) {
		this.audienceTypeCat = audienceTypeCat;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public String getFkDayIds() {
		return this.fkDayIds;
	}

	public void setFkDayIds(String fkDayIds) {
		this.fkDayIds = fkDayIds;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsRecurring() {
		return this.isRecurring;
	}

	public void setIsRecurring(Boolean isRecurring) {
		this.isRecurring = isRecurring;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getNoOfStudents() {
		return this.noOfStudents;
	}

	public void setNoOfStudents(Integer noOfStudents) {
		this.noOfStudents = noOfStudents;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	

	public String getTrainerDetails() {
		return this.trainerDetails;
	}

	public void setTrainerDetails(String trainerDetails) {
		this.trainerDetails = trainerDetails;
	}

	public String getTrainerName() {
		return this.trainerName;
	}

	public void setTrainerName(String trainerName) {
		this.trainerName = trainerName;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Training getTraining() {
		return training;
	}

	public void setTraining(Training training) {
		this.training = training;
	}

	public List<TrainingSession> getTrainingSessions() {
		return trainingSessions;
	}

	public void setTrainingSessions(List<TrainingSession> trainingSessions) {
		this.trainingSessions = trainingSessions;
	}

	public String getTrainingDetailTitle() {
		return trainingDetailTitle;
	}

	public void setTrainingDetailTitle(String trainingDetailTitle) {
		this.trainingDetailTitle = trainingDetailTitle;
	}

	public String getTrainingDetailDesc() {
		return trainingDetailDesc;
	}

	public void setTrainingDetailDesc(String trainingDetailDesc) {
		this.trainingDetailDesc = trainingDetailDesc;
	}

	public Time getStartTime() {
		return startTime;
	}

	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}

	public Time getEndTime() {
		return endTime;
	}

	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}
}