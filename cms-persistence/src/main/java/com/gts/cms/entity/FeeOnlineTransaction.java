package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the t_fee_online_transaction database table.
 * 
 */
@Entity
@Table(name = "t_fee_online_transaction")
@NamedQuery(name = "FeeOnlineTransaction.findAll", query = "SELECT f FROM FeeOnlineTransaction f")
public class FeeOnlineTransaction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_online_trans_id")
	private Long onlineTransId;

	@Column(name = "access_code")
	private String accessCode;

	private BigDecimal amount;

	@Column(name = "bank_name")
	private String bankName;

	@Column(name = "bank_ref_no")
	private String bankRefNo;

	@Column(name = "billing_info")
	private String billingInfo;

	@Column(name = "card_name")
	private String cardName;

	@Column(name = "card_number")
	private String cardNumber;

	@Column(name = "`CHECKSUM`")
	private String checksum;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "enc_key")
	private String encKey;

	@Column(name = "failure_message")
	private String failureMessage;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_amountreceived_tobank")
	private Boolean isAmountreceivedTobank;

	@Column(name = "merchant_param1")
	private String merchantParam1;

	@Column(name = "merchant_param2")
	private String merchantParam2;

	@Column(name = "merchant_param3")
	private String merchantParam3;

	@Column(name = "merchant_param4")
	private String merchantParam4;

	@Column(name = "merchant_param5")
	private String merchantParam5;

	@Column(name = "mobile_no")
	private String mobileNo;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "received_on")
	private Date receivedOn;

	private String request;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "request_on")
	private Date requestOn;

	private String response;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "response_on")
	private Date responseOn;

	@Column(name = "shipping_info")
	private String shippingInfo;

	@Column(name = "status_code")
	private String statusCode;

	@Column(name = "status_message")
	private String statusMessage;

	@Column(name = "tracking_id")
	private String trackingId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to TEmpDetail
	@ManyToOne
	@JoinColumn(name = "fk_emp_id")
	private EmployeeDetail empDetail;

	// bi-directional many-to-one association to TFeeStructure
	@ManyToOne
	@JoinColumn(name = "fk_fee_structure_id")
	private FeeStructure feeStructure;

	// bi-directional many-to-one association to TMSchool
	@ManyToOne
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to TMFinancialYear
	@ManyToOne
	@JoinColumn(name = "fk_financial_year_id")
	private FinancialYear financialYear;

	// bi-directional many-to-one association to TMGeneralDetail
	@ManyToOne
	@JoinColumn(name = "fk_ol_req_status_catdet_id")
	private GeneralDetail olReqStatusCat;

	// bi-directional many-to-one association to TMGeneralDetail
	@ManyToOne
	@JoinColumn(name = "fk_payment_cardtype_catdet_id")
	private GeneralDetail paymentCardTypeCat;

	// bi-directional many-to-one association to TMGeneralDetail
	@ManyToOne
	@JoinColumn(name = "fk_ol_orderstatus_catdet_id")
	private GeneralDetail olOrderStatusCat;

	// bi-directional many-to-one association to TStdStudentDetail
	@ManyToOne
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	// bi-directional many-to-one association to TFeeReceipt
	@ManyToOne
	@JoinColumn(name = "fk_receipt_id")
	private FeeReceipt feeReceipt;

	public FeeOnlineTransaction() {
	}

	public Long getOnlineTransId() {
		return this.onlineTransId;
	}

	public void setOnlineTransId(Long onlineTransId) {
		this.onlineTransId = onlineTransId;
	}

	public String getAccessCode() {
		return this.accessCode;
	}

	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankRefNo() {
		return this.bankRefNo;
	}

	public void setBankRefNo(String bankRefNo) {
		this.bankRefNo = bankRefNo;
	}

	public String getBillingInfo() {
		return this.billingInfo;
	}

	public void setBillingInfo(String billingInfo) {
		this.billingInfo = billingInfo;
	}

	public String getCardName() {
		return this.cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public String getCardNumber() {
		return this.cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getChecksum() {
		return this.checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getEncKey() {
		return this.encKey;
	}

	public void setEncKey(String encKey) {
		this.encKey = encKey;
	}

	public String getFailureMessage() {
		return this.failureMessage;
	}

	public void setFailureMessage(String failureMessage) {
		this.failureMessage = failureMessage;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsAmountreceivedTobank() {
		return this.isAmountreceivedTobank;
	}

	public void setIsAmountreceivedTobank(Boolean isAmountreceivedTobank) {
		this.isAmountreceivedTobank = isAmountreceivedTobank;
	}

	public String getMerchantParam1() {
		return this.merchantParam1;
	}

	public void setMerchantParam1(String merchantParam1) {
		this.merchantParam1 = merchantParam1;
	}

	public String getMerchantParam2() {
		return this.merchantParam2;
	}

	public void setMerchantParam2(String merchantParam2) {
		this.merchantParam2 = merchantParam2;
	}

	public String getMerchantParam3() {
		return this.merchantParam3;
	}

	public void setMerchantParam3(String merchantParam3) {
		this.merchantParam3 = merchantParam3;
	}

	public String getMerchantParam4() {
		return this.merchantParam4;
	}

	public void setMerchantParam4(String merchantParam4) {
		this.merchantParam4 = merchantParam4;
	}

	public String getMerchantParam5() {
		return this.merchantParam5;
	}

	public void setMerchantParam5(String merchantParam5) {
		this.merchantParam5 = merchantParam5;
	}

	public String getMobileNo() {
		return this.mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getReceivedOn() {
		return this.receivedOn;
	}

	public void setReceivedOn(Date receivedOn) {
		this.receivedOn = receivedOn;
	}

	public String getRequest() {
		return this.request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public Date getRequestOn() {
		return this.requestOn;
	}

	public void setRequestOn(Date requestOn) {
		this.requestOn = requestOn;
	}

	public String getResponse() {
		return this.response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Date getResponseOn() {
		return this.responseOn;
	}

	public void setResponseOn(Date responseOn) {
		this.responseOn = responseOn;
	}

	public String getShippingInfo() {
		return this.shippingInfo;
	}

	public void setShippingInfo(String shippingInfo) {
		this.shippingInfo = shippingInfo;
	}

	public String getStatusCode() {
		return this.statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return this.statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getTrackingId() {
		return this.trackingId;
	}

	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getEmpDetail() {
		return empDetail;
	}

	public void setEmpDetail(EmployeeDetail empDetail) {
		this.empDetail = empDetail;
	}

	public FeeStructure getFeeStructure() {
		return feeStructure;
	}

	public void setFeeStructure(FeeStructure feeStructure) {
		this.feeStructure = feeStructure;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public FinancialYear getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}

	public GeneralDetail getOlReqStatusCat() {
		return olReqStatusCat;
	}

	public void setOlReqStatusCat(GeneralDetail olReqStatusCat) {
		this.olReqStatusCat = olReqStatusCat;
	}

	public GeneralDetail getPaymentCardTypeCat() {
		return paymentCardTypeCat;
	}

	public void setPaymentCardTypeCat(GeneralDetail paymentCardTypeCat) {
		this.paymentCardTypeCat = paymentCardTypeCat;
	}

	public GeneralDetail getOlOrderStatusCat() {
		return olOrderStatusCat;
	}

	public void setOlOrderStatusCat(GeneralDetail olOrderStatusCat) {
		this.olOrderStatusCat = olOrderStatusCat;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public FeeReceipt getFeeReceipt() {
		return feeReceipt;
	}

	public void setFeeReceipt(FeeReceipt feeReceipt) {
		this.feeReceipt = feeReceipt;
	}

}