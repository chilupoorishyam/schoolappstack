package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the t_dl_course_subscription database table.
 * 
 */
@Entity
@Table(name = "t_dl_course_subscription")
@NamedQuery(name = "CourseSubscription.findAll", query = "SELECT c FROM CourseSubscription c")
public class CourseSubscription implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_course_subscription_id")
	private Long courseSubscriptionId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne
	@JoinColumn(name = "fk_coursesub_catdet_id")
	private GeneralDetail coursesubCat;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_lifetime")
	private Boolean isLifetime;

	private String reason;

	@Column(name = "subscription_cost")
	private BigDecimal subscriptionCost;

	@Column(name = "subscription_notes")
	private String subscriptionNotes;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "validation_days")
	private Integer validationDays;

	// bi-directional many-to-one association to OnlineCourses
	@ManyToOne
	@JoinColumn(name = "fk_dl_onlinecourse_id")
	private OnlineCourses onlineCourses;

	public CourseSubscription() {
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsLifetime() {
		return this.isLifetime;
	}

	public void setIsLifetime(Boolean isLifetime) {
		this.isLifetime = isLifetime;
	}

	public Long getCourseSubscriptionId() {
		return this.courseSubscriptionId;
	}

	public void setCourseSubscriptionId(Long courseSubscriptionId) {
		this.courseSubscriptionId = courseSubscriptionId;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getSubscriptionCost() {
		return this.subscriptionCost;
	}

	public void setSubscriptionCost(BigDecimal subscriptionCost) {
		this.subscriptionCost = subscriptionCost;
	}

	public String getSubscriptionNotes() {
		return this.subscriptionNotes;
	}

	public void setSubscriptionNotes(String subscriptionNotes) {
		this.subscriptionNotes = subscriptionNotes;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Integer getValidationDays() {
		return this.validationDays;
	}

	public void setValidationDays(Integer validationDays) {
		this.validationDays = validationDays;
	}

	public GeneralDetail getCoursesubCat() {
		return coursesubCat;
	}

	public void setCoursesubCat(GeneralDetail coursesubCat) {
		this.coursesubCat = coursesubCat;
	}

	public OnlineCourses getOnlineCourses() {
		return onlineCourses;
	}

	public void setOnlineCourses(OnlineCourses onlineCourses) {
		this.onlineCourses = onlineCourses;
	}

}