package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * The persistent class for the t_dl_m_onlinecourses database table.
 * 
 */
@Entity
@Table(name = "t_dl_m_onlinecourses")
@NamedQuery(name = "OnlineCourses.findAll", query = "SELECT o FROM OnlineCourses o")
public class OnlineCourses implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_dl_onlinecourse_id")
	private Long onlineCourseId;

	@Lob
	private String benefit;

	@Column(name = "course_displayimage_url")
	private String courseDisplayimageUrl;

	@Column(name = "course_duration")
	private Integer courseDuration;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Lob
	private String eligibility;

	@ManyToOne
	@JoinColumn(name = "fk_school_id")
	private School school;

	@ManyToOne
	@JoinColumn(name = "fk_onlinecourse_academicmap_id")
	private CourseAcademicMap courseAcademicMap;

	@ManyToOne
	@JoinColumn(name = "fk_course_level_catdet_id")
	private GeneralDetail courseLevelCat;

	@ManyToOne
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_moneyback_guarantee")
	private Boolean isMoneybackGuarantee;

	@Column(name = "onlinecourse_desc")
	private String onlineCourseDesc;

	@Column(name = "onlinecourse_name")
	private String onlineCourseName;
	
	@Column(name = "onlinecourse_code")
	private String onlineCourseCode;

	@Lob
	private String overview;

	@Lob
	@Column(name = "pre_requisites")
	private String preRequisites;

	private String reason;

	@Lob
	@Column(name = "sample_video")
	private String sampleVideo;

	@Column(name = "syllabus_url")
	private String syllabusUrl;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to CourseDetail
	@OneToMany(mappedBy = "onlineCourses", cascade = CascadeType.ALL)
	private List<CourseDetail> courseDetails;

	// bi-directional many-to-one association to CourseDiscussion
	@OneToMany(mappedBy = "onlineCourses", cascade = CascadeType.ALL)
	private List<CourseDiscussion> courseDiscussions;

	// bi-directional many-to-one association to CourseFaq
	@OneToMany(mappedBy = "onlineCourses", cascade = CascadeType.ALL)
	private List<CourseFaq> courseFaqs;

	// bi-directional many-to-one association to CourseLesson
	@OneToMany(mappedBy = "onlineCourses", cascade = CascadeType.ALL)
	private List<CourseLesson> courseLessons;

	// bi-directional many-to-one association to CourseMemberTopicLog
	@OneToMany(mappedBy = "onlineCourses", cascade = CascadeType.ALL)
	private List<CourseMemberTopicLog> courseMemberTopicLogs;

	// bi-directional many-to-one association to CourseMember
	@OneToMany(mappedBy = "onlineCourses", cascade = CascadeType.ALL)
	private List<CourseMember> courseMembers;

	// bi-directional many-to-one association to CourseReview
	@OneToMany(mappedBy = "onlineCourses", cascade = CascadeType.ALL)
	private List<CourseReview> courseReviews;

	// bi-directional many-to-one association to CourseSubscription
	@OneToMany(mappedBy = "onlineCourses", cascade = CascadeType.ALL)
	private List<CourseSubscription> courseSubscriptions;

	// bi-directional many-to-one association to TDlMember
	@ManyToOne
	@JoinColumn(name = "fk_peer_membership_id")
	private DigitalLibraryMember member;

	// bi-directional many-to-one association to TDlMemberLearningNote
	@OneToMany(mappedBy = "onlineCourses")
	private List<MemberLearningNote> memberLearningNotes;

	public OnlineCourses() {
	}

	public Long getOnlineCourseId() {
		return onlineCourseId;
	}

	public void setOnlineCourseId(Long onlineCourseId) {
		this.onlineCourseId = onlineCourseId;
	}

	public String getBenefit() {
		return benefit;
	}

	public void setBenefit(String benefit) {
		this.benefit = benefit;
	}

	public String getCourseDisplayimageUrl() {
		return courseDisplayimageUrl;
	}

	public void setCourseDisplayimageUrl(String courseDisplayimageUrl) {
		this.courseDisplayimageUrl = courseDisplayimageUrl;
	}

	public Integer getCourseDuration() {
		return courseDuration;
	}

	public void setCourseDuration(Integer courseDuration) {
		this.courseDuration = courseDuration;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getEligibility() {
		return eligibility;
	}

	public void setEligibility(String eligibility) {
		this.eligibility = eligibility;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getCourseLevelCat() {
		return courseLevelCat;
	}

	public void setCourseLevelCat(GeneralDetail courseLevelCat) {
		this.courseLevelCat = courseLevelCat;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsMoneybackGuarantee() {
		return isMoneybackGuarantee;
	}

	public void setIsMoneybackGuarantee(Boolean isMoneybackGuarantee) {
		this.isMoneybackGuarantee = isMoneybackGuarantee;
	}

	public String getOnlineCourseDesc() {
		return onlineCourseDesc;
	}

	public void setOnlineCourseDesc(String onlineCourseDesc) {
		this.onlineCourseDesc = onlineCourseDesc;
	}

	public String getOnlineCourseName() {
		return onlineCourseName;
	}

	public void setOnlineCourseName(String onlineCourseName) {
		this.onlineCourseName = onlineCourseName;
	}

	public String getOverview() {
		return overview;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public String getPreRequisites() {
		return preRequisites;
	}

	public void setPreRequisites(String preRequisites) {
		this.preRequisites = preRequisites;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSampleVideo() {
		return sampleVideo;
	}

	public void setSampleVideo(String sampleVideo) {
		this.sampleVideo = sampleVideo;
	}

	public String getSyllabusUrl() {
		return syllabusUrl;
	}

	public void setSyllabusUrl(String syllabusUrl) {
		this.syllabusUrl = syllabusUrl;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<CourseDetail> getCourseDetails() {
		return courseDetails;
	}

	public void setCourseDetails(List<CourseDetail> courseDetails) {
		this.courseDetails = courseDetails;
	}

	public List<CourseDiscussion> getCourseDiscussions() {
		return courseDiscussions;
	}

	public void setCourseDiscussions(List<CourseDiscussion> courseDiscussions) {
		this.courseDiscussions = courseDiscussions;
	}

	public List<CourseFaq> getCourseFaqs() {
		return courseFaqs;
	}

	public void setCourseFaqs(List<CourseFaq> courseFaqs) {
		this.courseFaqs = courseFaqs;
	}

	public List<CourseLesson> getCourseLessons() {
		return courseLessons;
	}

	public void setCourseLessons(List<CourseLesson> courseLessons) {
		this.courseLessons = courseLessons;
	}

	public List<CourseMemberTopicLog> getCourseMemberTopicLogs() {
		return courseMemberTopicLogs;
	}

	public void setCourseMemberTopicLogs(List<CourseMemberTopicLog> courseMemberTopicLogs) {
		this.courseMemberTopicLogs = courseMemberTopicLogs;
	}

	public List<CourseMember> getCourseMembers() {
		return courseMembers;
	}

	public void setCourseMembers(List<CourseMember> courseMembers) {
		this.courseMembers = courseMembers;
	}

	public List<CourseReview> getCourseReviews() {
		return courseReviews;
	}

	public void setCourseReviews(List<CourseReview> courseReviews) {
		this.courseReviews = courseReviews;
	}

	public List<CourseSubscription> getCourseSubscriptions() {
		return courseSubscriptions;
	}

	public void setCourseSubscriptions(List<CourseSubscription> courseSubscriptions) {
		this.courseSubscriptions = courseSubscriptions;
	}

	public DigitalLibraryMember getMember() {
		return member;
	}

	public void setMember(DigitalLibraryMember member) {
		this.member = member;
	}

	public List<MemberLearningNote> getMemberLearningNotes() {
		return memberLearningNotes;
	}

	public void setMemberLearningNotes(List<MemberLearningNote> memberLearningNotes) {
		this.memberLearningNotes = memberLearningNotes;
	}

	public String getOnlineCourseCode() {
		return onlineCourseCode;
	}

	public void setOnlineCourseCode(String onlineCourseCode) {
		this.onlineCourseCode = onlineCourseCode;
	}

	public CourseAcademicMap getCourseAcademicMap() {
		return courseAcademicMap;
	}

	public void setCourseAcademicMap(CourseAcademicMap courseAcademicMap) {
		this.courseAcademicMap = courseAcademicMap;
	}
}