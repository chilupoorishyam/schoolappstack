package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_messaging database table.
 * 
 */
@Entity
@Table(name="t_m_messaging")
@NamedQuery(name="Messaging.findAll", query="SELECT m FROM Messaging m")
public class Messaging implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_messaging_id")
	private Long messagingId;

	private String comments;

	@Column(name="config_selections")
	private String configSelections;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_email_alert")
	private Boolean isEmailAlert;

	@Column(name="is_sms_alert")
	private Boolean isSmsAlert;

	@Column(name="message_content")
	private String messageContent;

	@Column(name="message_content_html")
	private String messageContentHtml;

	@Column(name="message_signature")
	private String messageSignature;

	@Column(name="message_subject")
	private String messageSubject;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="messaging_date")
	private Date messagingDate;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="schedule_date")
	private Date scheduleDate;

	@Column(name="sender_code")
	private String senderCode;

	@Column(name="sms_text_format")
	private String smsTextFormat;

	@Column(name="total_faild_msgs")
	private Integer totalFaildMsgs;

	@Column(name="total_success_msgs")
	private Integer totalSuccessMsgs;

	@Column(name="total_users")
	private Integer totalUsers;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_sent_by_emp_id")
	private EmployeeDetail sentByEmployeeDetail;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to SmsPattern
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_message_pattern_id")
	private SmsPattern smsPattern;

	//bi-directional many-to-one association to MessagingRecipient
	@OneToMany(mappedBy="messaging",fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	private List<MessagingRecipient> messagingRecipients;
	
	@Column(name="api_job_id")
	private String apiJobId;

	public Messaging() {
	}

	public Long getMessagingId() {
		return this.messagingId;
	}

	public void setMessagingId(Long messagingId) {
		this.messagingId = messagingId;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getConfigSelections() {
		return this.configSelections;
	}

	public void setConfigSelections(String configSelections) {
		this.configSelections = configSelections;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsEmailAlert() {
		return this.isEmailAlert;
	}

	public void setIsEmailAlert(Boolean isEmailAlert) {
		this.isEmailAlert = isEmailAlert;
	}

	public Boolean getIsSmsAlert() {
		return this.isSmsAlert;
	}

	public void setIsSmsAlert(Boolean isSmsAlert) {
		this.isSmsAlert = isSmsAlert;
	}

	public String getMessageContent() {
		return this.messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public String getMessageContentHtml() {
		return this.messageContentHtml;
	}

	public void setMessageContentHtml(String messageContentHtml) {
		this.messageContentHtml = messageContentHtml;
	}

	public String getMessageSignature() {
		return this.messageSignature;
	}

	public void setMessageSignature(String messageSignature) {
		this.messageSignature = messageSignature;
	}

	public String getMessageSubject() {
		return this.messageSubject;
	}

	public void setMessageSubject(String messageSubject) {
		this.messageSubject = messageSubject;
	}

	public Date getMessagingDate() {
		return this.messagingDate;
	}

	public void setMessagingDate(Date messagingDate) {
		this.messagingDate = messagingDate;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getScheduleDate() {
		return this.scheduleDate;
	}

	public void setScheduleDate(Date scheduleDate) {
		this.scheduleDate = scheduleDate;
	}

	public String getSenderCode() {
		return this.senderCode;
	}

	public void setSenderCode(String senderCode) {
		this.senderCode = senderCode;
	}

	public String getSmsTextFormat() {
		return this.smsTextFormat;
	}

	public void setSmsTextFormat(String smsTextFormat) {
		this.smsTextFormat = smsTextFormat;
	}

	public Integer getTotalFaildMsgs() {
		return this.totalFaildMsgs;
	}

	public void setTotalFaildMsgs(Integer totalFaildMsgs) {
		this.totalFaildMsgs = totalFaildMsgs;
	}

	public Integer getTotalSuccessMsgs() {
		return this.totalSuccessMsgs;
	}

	public void setTotalSuccessMsgs(Integer totalSuccessMsgs) {
		this.totalSuccessMsgs = totalSuccessMsgs;
	}

	public Integer getTotalUsers() {
		return this.totalUsers;
	}

	public void setTotalUsers(Integer totalUsers) {
		this.totalUsers = totalUsers;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getSentByEmployeeDetail() {
		return this.sentByEmployeeDetail;
	}

	public void setSentByEmployeeDetail(EmployeeDetail sentByEmployeeDetail) {
		this.sentByEmployeeDetail = sentByEmployeeDetail;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public SmsPattern getSmsPattern() {
		return this.smsPattern;
	}

	public void setSmsPattern(SmsPattern smsPattern) {
		this.smsPattern = smsPattern;
	}

	public List<MessagingRecipient> getMessagingRecipients() {
		return this.messagingRecipients;
	}

	public void setMessagingRecipients(List<MessagingRecipient> messagingRecipients) {
		this.messagingRecipients = messagingRecipients;
	}

	public MessagingRecipient addMessagingRecipient(MessagingRecipient messagingRecipient) {
		getMessagingRecipients().add(messagingRecipient);
		messagingRecipient.setMessaging(this);

		return messagingRecipient;
	}

	public MessagingRecipient removeMessagingRecipient(MessagingRecipient messagingRecipient) {
		getMessagingRecipients().remove(messagingRecipient);
		messagingRecipient.setMessaging(null);

		return messagingRecipient;
	}

	public String getApiJobId() {
		return apiJobId;
	}

	public void setApiJobId(String apiJobId) {
		this.apiJobId = apiJobId;
	}
	
	

}