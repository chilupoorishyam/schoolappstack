package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_hr_payslip_settings database table.
 * 
 */
@Entity
@Table(name="t_hr_payslip_settings")
@NamedQuery(name="PayslipSetting.findAll", query="SELECT p FROM PayslipSetting p")
public class PayslipSetting implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_payslip_setting_id")
	private Long payslipSettingId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="field_name")
	private String fieldName;

	@Column(name="field_sort_order")
	private Integer fieldSortOrder;

	@Column(name="group_name")
	private String groupName;

	@Column(name="group_sort_order")
	private Integer groupSortOrder;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to PayslipBranchSetting
	@OneToMany(mappedBy="payslipSetting",fetch = FetchType.LAZY)
	private List<PayslipBranchSetting> payslipBranchSettings;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	public PayslipSetting() {
	}

	public Long getPayslipSettingId() {
		return this.payslipSettingId;
	}

	public void setPayslipSettingId(Long payslipSettingId) {
		this.payslipSettingId = payslipSettingId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getFieldName() {
		return this.fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Integer getFieldSortOrder() {
		return this.fieldSortOrder;
	}

	public void setFieldSortOrder(Integer fieldSortOrder) {
		this.fieldSortOrder = fieldSortOrder;
	}

	public String getGroupName() {
		return this.groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getGroupSortOrder() {
		return this.groupSortOrder;
	}

	public void setGroupSortOrder(Integer groupSortOrder) {
		this.groupSortOrder = groupSortOrder;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<PayslipBranchSetting> getPayslipBranchSettings() {
		return this.payslipBranchSettings;
	}

	public void setPayslipBranchSettings(List<PayslipBranchSetting> payslipBranchSettings) {
		this.payslipBranchSettings = payslipBranchSettings;
	}

	public PayslipBranchSetting addPayslipBranchSetting(PayslipBranchSetting payslipBranchSetting) {
		getPayslipBranchSettings().add(payslipBranchSetting);
		payslipBranchSetting.setPayslipSetting(this);

		return payslipBranchSetting;
	}

	public PayslipBranchSetting removePayslipBranchSetting(PayslipBranchSetting payslipBranchSetting) {
		getPayslipBranchSettings().remove(payslipBranchSetting);
		payslipBranchSetting.setPayslipSetting(null);

		return payslipBranchSetting;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

}