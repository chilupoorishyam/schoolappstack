package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_weekdays database table.
 * 
 */
@Entity
@Table(name="t_m_weekdays")
@NamedQuery(name="Weekday.findAll", query="SELECT w FROM Weekday w")
public class Weekday implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_weekday_id")
	private Long weekdayId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="day_of_week")
	private Integer dayOfWeek;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_editable")
	private Boolean isEditable;

	private String name;

	private String reason;

	@Column(name="sort_order")
	private Integer sortOrder;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	private String weekday;

	//bi-directional many-to-one association to ClassWeekday
	@OneToMany(mappedBy="weekday",fetch = FetchType.LAZY)
	private List<ClassWeekday> classWeekdays;

	//bi-directional many-to-one association to Schedule
	@OneToMany(mappedBy="weekday",fetch = FetchType.LAZY)
	private List<Schedule> schedules;

	public Weekday() {
	}

	public Long getWeekdayId() {
		return this.weekdayId;
	}

	public void setWeekdayId(Long weekdayId) {
		this.weekdayId = weekdayId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Integer getDayOfWeek() {
		return this.dayOfWeek;
	}

	public void setDayOfWeek(Integer dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsEditable() {
		return this.isEditable;
	}

	public void setIsEditable(Boolean isEditable) {
		this.isEditable = isEditable;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getWeekday() {
		return this.weekday;
	}

	public void setWeekday(String weekday) {
		this.weekday = weekday;
	}

	public List<ClassWeekday> getClassWeekdays() {
		return this.classWeekdays;
	}

	public void setClassWeekdays(List<ClassWeekday> classWeekdays) {
		this.classWeekdays = classWeekdays;
	}

	public ClassWeekday addClassWeekday(ClassWeekday classWeekday) {
		getClassWeekdays().add(classWeekday);
		classWeekday.setWeekday(this);

		return classWeekday;
	}

	public ClassWeekday removeClassWeekday(ClassWeekday classWeekday) {
		getClassWeekdays().remove(classWeekday);
		classWeekday.setWeekday(null);

		return classWeekday;
	}

	public List<Schedule> getSchedules() {
		return this.schedules;
	}

	public void setSchedules(List<Schedule> schedules) {
		this.schedules = schedules;
	}

	public Schedule addSchedule(Schedule schedule) {
		getSchedules().add(schedule);
		schedule.setWeekday(this);

		return schedule;
	}

	public Schedule removeSchedule(Schedule schedule) {
		getSchedules().remove(schedule);
		schedule.setWeekday(null);

		return schedule;
	}

}