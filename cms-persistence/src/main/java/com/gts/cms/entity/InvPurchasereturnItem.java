package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the t_inv_purchasereturn_items database table.
 * 
 */
@Entity
@Table(name="t_inv_purchasereturn_items")
@NamedQuery(name="InvPurchasereturnItem.findAll", query="SELECT i FROM InvPurchasereturnItem i")
public class InvPurchasereturnItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_pr_item_id")
	private Long prItemId;

	@Column(name="batch_no")
	private String batchNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_free")
	private Boolean isFree;

	@Column(name="item_discount_percentage")
	private BigDecimal itemDiscountPercentage;

	@Column(name="item_total_actual_amount")
	private BigDecimal itemTotalActualAmount;

	@Column(name="item_total_cost")
	private BigDecimal itemTotalCost;

	@Column(name="item_total_discount_amount")
	private BigDecimal itemTotalDiscountAmount;

	@Column(name="item_total_tax_amount")
	private BigDecimal itemTotalTaxAmount;

	@Column(name="item_unit_amount")
	private BigDecimal itemUnitAmount;

	private String reason;

	@Column(name="returned_qty")
	private BigDecimal returnedQty;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to InvPurchasereturn
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_purchasereturn_id")
	private InvPurchasereturn invPurchasereturn;

	//bi-directional many-to-one association to InvSrvItem
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_srv_item_id")
	private InvSrvItem invSrvItem;

	//bi-directional many-to-one association to InvItemmaster
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_item_id")
	private InvItemmaster invItemMaster;

	//bi-directional many-to-one association to InvItemmaster
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_freefor_item_id")
	private InvItemmaster freeForItemMaster;

	public InvPurchasereturnItem() {
	}

	public Long getPrItemId() {
		return this.prItemId;
	}

	public void setPrItemId(Long prItemId) {
		this.prItemId = prItemId;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsFree() {
		return this.isFree;
	}

	public void setIsFree(Boolean isFree) {
		this.isFree = isFree;
	}

	public BigDecimal getItemDiscountPercentage() {
		return this.itemDiscountPercentage;
	}

	public void setItemDiscountPercentage(BigDecimal itemDiscountPercentage) {
		this.itemDiscountPercentage = itemDiscountPercentage;
	}

	public BigDecimal getItemTotalActualAmount() {
		return this.itemTotalActualAmount;
	}

	public void setItemTotalActualAmount(BigDecimal itemTotalActualAmount) {
		this.itemTotalActualAmount = itemTotalActualAmount;
	}

	public BigDecimal getItemTotalCost() {
		return this.itemTotalCost;
	}

	public void setItemTotalCost(BigDecimal itemTotalCost) {
		this.itemTotalCost = itemTotalCost;
	}

	public BigDecimal getItemTotalDiscountAmount() {
		return this.itemTotalDiscountAmount;
	}

	public void setItemTotalDiscountAmount(BigDecimal itemTotalDiscountAmount) {
		this.itemTotalDiscountAmount = itemTotalDiscountAmount;
	}

	public BigDecimal getItemTotalTaxAmount() {
		return this.itemTotalTaxAmount;
	}

	public void setItemTotalTaxAmount(BigDecimal itemTotalTaxAmount) {
		this.itemTotalTaxAmount = itemTotalTaxAmount;
	}

	public BigDecimal getItemUnitAmount() {
		return this.itemUnitAmount;
	}

	public void setItemUnitAmount(BigDecimal itemUnitAmount) {
		this.itemUnitAmount = itemUnitAmount;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getReturnedQty() {
		return this.returnedQty;
	}

	public void setReturnedQty(BigDecimal returnedQty) {
		this.returnedQty = returnedQty;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public InvPurchasereturn getInvPurchasereturn() {
		return invPurchasereturn;
	}

	public void setInvPurchasereturn(InvPurchasereturn invPurchasereturn) {
		this.invPurchasereturn = invPurchasereturn;
	}

	public InvSrvItem getInvSrvItem() {
		return invSrvItem;
	}

	public void setInvSrvItem(InvSrvItem invSrvItem) {
		this.invSrvItem = invSrvItem;
	}

	public InvItemmaster getInvItemMaster() {
		return invItemMaster;
	}

	public void setInvItemMaster(InvItemmaster invItemMaster) {
		this.invItemMaster = invItemMaster;
	}

	public InvItemmaster getFreeForItemMaster() {
		return freeForItemMaster;
	}

	public void setFreeForItemMaster(InvItemmaster freeForItemMaster) {
		this.freeForItemMaster = freeForItemMaster;
	}
}