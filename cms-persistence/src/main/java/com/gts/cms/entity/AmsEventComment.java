package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_ams_event_comments database table.
 * 
 */
@Entity
@Table(name="t_ams_event_comments")
@NamedQuery(name="AmsEventComment.findAll", query="SELECT a FROM AmsEventComment a")
public class AmsEventComment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_ams_event_comment_id")
	private Long amsEventCommentId;

	private String attachment1;

	private String attachment2;

	@Column(name="`COMMENT`")
	private String comment;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="fk_dislike_profile_ids")
	private String fkDislikeProfileIds;

	@Column(name="fk_likes_profile_ids")
	private String fkLikesProfileIds;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_verifiedby_emp_id")
	private EmployeeDetail verifiedbyEmpId;
	
	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_new_comment")
	private Boolean isNewComment;

	@Column(name="is_verifiedby_incharge")
	private Boolean isVerifiedbyIncharge;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	@Column(name="verification_comments")
	private String verificationComments;

	//bi-directional many-to-one association to AmsEvent
	@ManyToOne
	@JoinColumn(name="fk_event_id")
	private AmsEvent amsEvent;

	//bi-directional many-to-one association to AmsProfileDetail
	@ManyToOne
	@JoinColumn(name="fk_ams_profile_id")
	private AmsProfileDetail amsProfileDetail;

	//bi-directional many-to-one association to AmsEventComment
	@ManyToOne
	@JoinColumn(name="fk_reply_comment_id")
	private AmsEventComment amsEventComment;

	//bi-directional many-to-one association to AmsEventComment
	@OneToMany(mappedBy="amsEventComment")
	private List<AmsEventComment> amsEventComments;

	public AmsEventComment() {
	}

	public Long getAmsEventCommentId() {
		return this.amsEventCommentId;
	}

	public void setAmsEventCommentId(Long amsEventCommentId) {
		this.amsEventCommentId = amsEventCommentId;
	}

	public String getAttachment1() {
		return this.attachment1;
	}

	public void setAttachment1(String attachment1) {
		this.attachment1 = attachment1;
	}

	public String getAttachment2() {
		return this.attachment2;
	}

	public void setAttachment2(String attachment2) {
		this.attachment2 = attachment2;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getFkDislikeProfileIds() {
		return this.fkDislikeProfileIds;
	}

	public void setFkDislikeProfileIds(String fkDislikeProfileIds) {
		this.fkDislikeProfileIds = fkDislikeProfileIds;
	}

	public String getFkLikesProfileIds() {
		return this.fkLikesProfileIds;
	}

	public void setFkLikesProfileIds(String fkLikesProfileIds) {
		this.fkLikesProfileIds = fkLikesProfileIds;
	}

	

	public EmployeeDetail getVerifiedbyEmpId() {
		return verifiedbyEmpId;
	}

	public void setVerifiedbyEmpId(EmployeeDetail verifiedbyEmpId) {
		this.verifiedbyEmpId = verifiedbyEmpId;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsNewComment() {
		return this.isNewComment;
	}

	public void setIsNewComment(Boolean isNewComment) {
		this.isNewComment = isNewComment;
	}

	public Boolean getIsVerifiedbyIncharge() {
		return this.isVerifiedbyIncharge;
	}

	public void setIsVerifiedbyIncharge(Boolean isVerifiedbyIncharge) {
		this.isVerifiedbyIncharge = isVerifiedbyIncharge;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getVerificationComments() {
		return this.verificationComments;
	}

	public void setVerificationComments(String verificationComments) {
		this.verificationComments = verificationComments;
	}

	public AmsEvent getAmsEvent() {
		return this.amsEvent;
	}

	public void setAmsEvent(AmsEvent amsEvent) {
		this.amsEvent = amsEvent;
	}

	public AmsProfileDetail getAmsProfileDetail() {
		return this.amsProfileDetail;
	}

	public void setAmsProfileDetail(AmsProfileDetail amsProfileDetail) {
		this.amsProfileDetail = amsProfileDetail;
	}

	public AmsEventComment getAmsEventComment() {
		return this.amsEventComment;
	}

	public void setAmsEventComment(AmsEventComment amsEventComment) {
		this.amsEventComment = amsEventComment;
	}

	public List<AmsEventComment> getAmsEventComments() {
		return this.amsEventComments;
	}

	public void setAmsEventComments(List<AmsEventComment> amsEventComments) {
		this.amsEventComments = amsEventComments;
	}

	public AmsEventComment addAmsEventComment(AmsEventComment amsEventComment) {
		getAmsEventComments().add(amsEventComment);
		amsEventComment.setAmsEventComment(this);

		return amsEventComment;
	}

	public AmsEventComment removeAmsEventComment(AmsEventComment amsEventComment) {
		getAmsEventComments().remove(amsEventComment);
		amsEventComment.setAmsEventComment(null);

		return amsEventComment;
	}

}