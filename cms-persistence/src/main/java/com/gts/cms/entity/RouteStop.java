package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_tm_route_stops database table.
 * 
 */
@Entity
@Table(name="t_tm_route_stops")
@NamedQuery(name="RouteStop.findAll", query="SELECT r FROM RouteStop r")
public class RouteStop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_route_stop_id")
	private Long routeStopId;

	private BigDecimal amount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="distance_from_school_km")
	private BigDecimal distanceFromSchoolKm;

	@Column(name="drop_time")
	private Time dropTime;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="pick_time")
	private Time pickTime;

	private String reason;

	@Column(name="stop_name")
	private String stopName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_frequency_catdet_id")
	private GeneralDetail feeFrequency;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to Route
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_route_id")
	private Route route;

	//bi-directional many-to-one association to TransportDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_transport_detail_id")
	private TransportDetail transportDetail;

	//bi-directional many-to-one association to TransportAllocation
	@OneToMany(mappedBy="pickupRouteStop",fetch = FetchType.LAZY)
	private List<TransportAllocation> transportAllocations1;

	//bi-directional many-to-one association to TransportAllocation
	@OneToMany(mappedBy="dropRouteStop",fetch = FetchType.LAZY)
	private List<TransportAllocation> transportAllocations2;

	public RouteStop() {
	}

	public Long getRouteStopId() {
		return this.routeStopId;
	}

	public void setRouteStopId(Long routeStopId) {
		this.routeStopId = routeStopId;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public BigDecimal getDistanceFromSchoolKm() {
		return this.distanceFromSchoolKm;
	}

	public void setDistanceFromSchoolKm(BigDecimal distanceFromSchoolKm) {
		this.distanceFromSchoolKm = distanceFromSchoolKm;
	}

	public Time getDropTime() {
		return this.dropTime;
	}

	public void setDropTime(Time dropTime) {
		this.dropTime = dropTime;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Time getPickTime() {
		return this.pickTime;
	}

	public void setPickTime(Time pickTime) {
		this.pickTime = pickTime;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStopName() {
		return this.stopName;
	}

	public void setStopName(String stopName) {
		this.stopName = stopName;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public GeneralDetail getFeeFrequency() {
		return this.feeFrequency;
	}

	public void setFeeFrequency(GeneralDetail feeFrequency) {
		this.feeFrequency = feeFrequency;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Route getRoute() {
		return this.route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public TransportDetail getTransportDetail() {
		return this.transportDetail;
	}

	public void setTransportDetail(TransportDetail transportDetail) {
		this.transportDetail = transportDetail;
	}

	public List<TransportAllocation> getTransportAllocations1() {
		return this.transportAllocations1;
	}

	public void setTransportAllocations1(List<TransportAllocation> transportAllocations1) {
		this.transportAllocations1 = transportAllocations1;
	}

	public TransportAllocation addTransportAllocations1(TransportAllocation transportAllocations1) {
		getTransportAllocations1().add(transportAllocations1);
		transportAllocations1.setPickupRouteStop(this);

		return transportAllocations1;
	}

	public TransportAllocation removeTransportAllocations1(TransportAllocation transportAllocations1) {
		getTransportAllocations1().remove(transportAllocations1);
		transportAllocations1.setPickupRouteStop(null);

		return transportAllocations1;
	}

	public List<TransportAllocation> getTransportAllocations2() {
		return this.transportAllocations2;
	}

	public void setTransportAllocations2(List<TransportAllocation> transportAllocations2) {
		this.transportAllocations2 = transportAllocations2;
	}

	public TransportAllocation addTransportAllocations2(TransportAllocation transportAllocations2) {
		getTransportAllocations2().add(transportAllocations2);
		transportAllocations2.setDropRouteStop(this);

		return transportAllocations2;
	}

	public TransportAllocation removeTransportAllocations2(TransportAllocation transportAllocations2) {
		getTransportAllocations2().remove(transportAllocations2);
		transportAllocations2.setDropRouteStop(null);

		return transportAllocations2;
	}

}