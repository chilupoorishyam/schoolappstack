package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_hstl_rooms database table.
 * 
 */
@Entity
@Table(name = "t_hstl_rooms")
@NamedQuery(name = "HostelRoom.findAll", query = "SELECT h FROM HostelRoom h")
public class HostelRoom implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_hstl_room_id")
	private Long hstlRoomId;

	@Column(name = "alloted_beds")
	private Long allotedBeds;

	private BigDecimal amount;

	@Column(name = "available_beds")
	private Long availableBeds;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "floor_name")
	private String floorName;

	@Column(name = "floor_no")
	private Long floorNo;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "no_of_beds")
	private Long noOfBeds;

	private String reason;

	@Column(name = "room_number")
	private String roomNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_room_type_catdet_id")
	private GeneralDetail roomTypeCatdet;

	// bi-directional many-to-one association to HstlRoomAllocation
	@OneToMany(mappedBy = "hstlRoom")
	private List<HostelRoomAllocation> hstlRoomAllocations;

	// bi-directional many-to-one association to HostelDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_hstl_hostel_id")
	private HostelDetail hostelDetail;

	public HostelRoom() {
	}

	public Long getHstlRoomId() {
		return hstlRoomId;
	}

	public void setHstlRoomId(Long hstlRoomId) {
		this.hstlRoomId = hstlRoomId;
	}

	public Long getAllotedBeds() {
		return allotedBeds;
	}

	public void setAllotedBeds(Long allotedBeds) {
		this.allotedBeds = allotedBeds;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getAvailableBeds() {
		return availableBeds;
	}

	public void setAvailableBeds(Long availableBeds) {
		this.availableBeds = availableBeds;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getFloorName() {
		return floorName;
	}

	public void setFloorName(String floorName) {
		this.floorName = floorName;
	}

	public Long getFloorNo() {
		return floorNo;
	}

	public void setFloorNo(Long floorNo) {
		this.floorNo = floorNo;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Long getNoOfBeds() {
		return noOfBeds;
	}

	public void setNoOfBeds(Long noOfBeds) {
		this.noOfBeds = noOfBeds;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public GeneralDetail getRoomTypeCatdet() {
		return roomTypeCatdet;
	}

	public void setRoomTypeCatdet(GeneralDetail roomTypeCatdet) {
		this.roomTypeCatdet = roomTypeCatdet;
	}

	public List<HostelRoomAllocation> getHstlRoomAllocations() {
		return hstlRoomAllocations;
	}

	public void setHstlRoomAllocations(List<HostelRoomAllocation> hstlRoomAllocations) {
		this.hstlRoomAllocations = hstlRoomAllocations;
	}

	public HostelDetail getHostelDetail() {
		return hostelDetail;
	}

	public void setHostelDetail(HostelDetail hostelDetail) {
		this.hostelDetail = hostelDetail;
	}
}