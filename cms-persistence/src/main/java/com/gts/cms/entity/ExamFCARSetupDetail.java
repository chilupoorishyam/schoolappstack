package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_exam_fcar_setup_details database table.
 * 
 */
@Entity
@Table(name = "t_exam_fcar_setup_details")
@NamedQuery(name = "ExamFCARSetupDetail.findAll", query = "SELECT e FROM ExamFCARSetupDetail e")
public class ExamFCARSetupDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_fcar_set_det_id")
	private Long examFCARSetDetId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "detail_code")
	private String detailCode;
	
	@ManyToOne
	@JoinColumn(name = "fk_school_id")
	private School school;
	
	@ManyToOne
	@JoinColumn(name = "fk_internalpattern_catdet_id")
	private GeneralDetail internalpatternCat;

	@Column(name = "is_active")
	private Boolean isActive;

	private Integer marks;

	@Column(name = "option_name")
	private String optionName;

	private String question;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to ExamFCARSetupMaster
	@ManyToOne
	@JoinColumn(name = "fk_exam_fcar_set_master_id")
	private ExamFCARSetupMaster examFcarSetupMaster;

	// bi-directional many-to-one association to ExamFCARStudentSubMark
	@OneToMany(mappedBy = "examFcarSetupDetail")
	private List<ExamFCARStudentSubMark> examFcarStudentSubMarks;

	// bi-directional many-to-one association to ExamFCARSubjectSyllabus
	@OneToMany(mappedBy = "examFcarSetupDetail")
	private List<ExamFCARSubjectSyllabus> examFcarSubjectSyllabuses;

	public ExamFCARSetupDetail() {
	}

	public Long getExamFCARSetDetId() {
		return this.examFCARSetDetId;
	}

	public void setExamFCARSetDetId(Long examFCARSetDetId) {
		this.examFCARSetDetId = examFCARSetDetId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDetailCode() {
		return this.detailCode;
	}

	public void setDetailCode(String detailCode) {
		this.detailCode = detailCode;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getMarks() {
		return this.marks;
	}

	public void setMarks(Integer marks) {
		this.marks = marks;
	}

	public String getOptionName() {
		return this.optionName;
	}

	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}

	public String getQuestion() {
		return this.question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public ExamFCARSetupMaster getExamFcarSetupMaster() {
		return examFcarSetupMaster;
	}

	public void setExamFcarSetupMaster(ExamFCARSetupMaster examFcarSetupMaster) {
		this.examFcarSetupMaster = examFcarSetupMaster;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getInternalpatternCat() {
		return internalpatternCat;
	}

	public void setInternalpatternCat(GeneralDetail internalpatternCat) {
		this.internalpatternCat = internalpatternCat;
	}

	public List<ExamFCARStudentSubMark> getExamFcarStudentSubMarks() {
		return examFcarStudentSubMarks;
	}

	public void setExamFcarStudentSubMarks(List<ExamFCARStudentSubMark> examFcarStudentSubMarks) {
		this.examFcarStudentSubMarks = examFcarStudentSubMarks;
	}

	public List<ExamFCARSubjectSyllabus> getExamFcarSubjectSyllabuses() {
		return examFcarSubjectSyllabuses;
	}

	public void setExamFcarSubjectSyllabuses(List<ExamFCARSubjectSyllabus> examFcarSubjectSyllabuses) {
		this.examFcarSubjectSyllabuses = examFcarSubjectSyllabuses;
	}
}