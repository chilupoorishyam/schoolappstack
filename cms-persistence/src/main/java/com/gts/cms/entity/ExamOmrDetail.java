package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_exam_omr_details database table.
 * 
 */
@Entity
@Table(name="t_exam_omr_details")
@NamedQuery(name="ExamOmrDetail.findAll", query="SELECT e FROM ExamOmrDetail e")
public class ExamOmrDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_exam_omr_id")
	private Long examOmrId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_id")
	private Course course;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_exam_std_det_id")
	private ExamStudentDetail examStdDet;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="omr_barcode")
	private String omrBarcode;

	@Column(name="omr_bundle_no")
	private Integer omrBundleNo;

	@Column(name="omr_serial_no")
	private Integer omrSerialNo;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	public ExamOmrDetail() {
	}

	public Long getExamOmrId() {
		return this.examOmrId;
	}

	public void setExamOmrId(Long examOmrId) {
		this.examOmrId = examOmrId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public ExamStudentDetail getExamStdDet() {
		return examStdDet;
	}

	public void setExamStdDet(ExamStudentDetail examStdDet) {
		this.examStdDet = examStdDet;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getOmrBarcode() {
		return this.omrBarcode;
	}

	public void setOmrBarcode(String omrBarcode) {
		this.omrBarcode = omrBarcode;
	}

	public Integer getOmrBundleNo() {
		return this.omrBundleNo;
	}

	public void setOmrBundleNo(Integer omrBundleNo) {
		this.omrBundleNo = omrBundleNo;
	}

	public Integer getOmrSerialNo() {
		return this.omrSerialNo;
	}

	public void setOmrSerialNo(Integer omrSerialNo) {
		this.omrSerialNo = omrSerialNo;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

}