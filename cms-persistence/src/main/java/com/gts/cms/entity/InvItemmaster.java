package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_inv_itemmaster database table.
 * 
 */
@Entity
@Table(name="t_inv_itemmaster")
@NamedQuery(name="InvItemmaster.findAll", query="SELECT i FROM InvItemmaster i")
public class InvItemmaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_item_id")
	private Long itemId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;
	
	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_item_type_catdet_id")
	private GeneralDetail itemTypeCatdet;
	
	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	@Column(name="fk_supplier_ids")
	private String supplierIds;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_req_tracking")
	private Boolean isReqTracking;

	@Column(name="item_aliasname")
	private String itemAliasname;

	@Column(name="item_code")
	private String itemCode;

	@Column(name="item_name")
	private String itemName;

	private String make;

	private String model;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;
	
	// bi-directional many-to-one association to InvItemcategory
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_item_category_id")
	private InvItemcategory invItemcategory;

	// bi-directional many-to-one association to InvItemsubcategory
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_item_subcategory_id")
	private InvItemsubcategory invItemsubcategory;

	// bi-directional many-to-one association to InvBrandmaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_brandmaster_id")
	private InvBrandmaster invBrandmaster;
/*
	//bi-directional many-to-one association to InvInternalIndentitem
	@OneToMany(mappedBy="InvItemmaster")
	private List<InvInternalIndentitem> invInternalIndentitems;

	//bi-directional many-to-one association to InvInternalIssueItem
	@OneToMany(mappedBy="InvItemmaster")
	private List<InvInternalIssueItem> invInternalIssueItems;

	//bi-directional many-to-one association to InvInternalReturnItem
	@OneToMany(mappedBy="InvItemmaster")
	private List<InvInternalReturnItem> invInternalReturnItems;

	//bi-directional many-to-one association to InvItemDetail
	@OneToMany(mappedBy="InvItemmaster")
	private List<InvItemDetail> invItemDetails;

	//bi-directional many-to-one association to InvItemopeningStock
	@OneToMany(mappedBy="InvItemmaster")
	private List<InvItemopeningStock> invItemopeningStocks;

	//bi-directional many-to-one association to InvPoItem
	@OneToMany(mappedBy="InvItemmaster1")
	private List<InvPoItem> invPoItems1;

	//bi-directional many-to-one association to InvPoItem
	@OneToMany(mappedBy="InvItemmaster2")
	private List<InvPoItem> invPoItems2;

	//bi-directional many-to-one association to InvPurchasereturnItem
	@OneToMany(mappedBy="InvItemmaster1")
	private List<InvPurchasereturnItem> invPurchasereturnItems1;

	//bi-directional many-to-one association to InvPurchasereturnItem
	@OneToMany(mappedBy="InvItemmaster2")
	private List<InvPurchasereturnItem> invPurchasereturnItems2;

	//bi-directional many-to-one association to InvSrvItem
	@OneToMany(mappedBy="InvItemmaster1")
	private List<InvSrvItem> invSrvItems1;

	//bi-directional many-to-one association to InvSrvItem
	@OneToMany(mappedBy="InvItemmaster2")
	private List<InvSrvItem> invSrvItems2;

	//bi-directional many-to-one association to InvStockledger
	@OneToMany(mappedBy="InvItemmaster")
	private List<InvStockledger> invStockledgers;

	//bi-directional many-to-one association to InvStoreItem
	@OneToMany(mappedBy="InvItemmaster1")
	private List<InvStoreItem> invStoreItems1;

	//bi-directional many-to-one association to InvStoreItem
	@OneToMany(mappedBy="InvItemmaster2")
	private List<InvStoreItem> invStoreItems2;*/

	public InvItemmaster() {
	}

	public Long getItemId() {
		return this.itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getSupplierIds() {
		return this.supplierIds;
	}

	public void setSupplierIds(String supplierIds) {
		this.supplierIds = supplierIds;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsReqTracking() {
		return this.isReqTracking;
	}

	public void setIsReqTracking(Boolean isReqTracking) {
		this.isReqTracking = isReqTracking;
	}

	public String getItemAliasname() {
		return this.itemAliasname;
	}

	public void setItemAliasname(String itemAliasname) {
		this.itemAliasname = itemAliasname;
	}

	public String getItemCode() {
		return this.itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return this.itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getMake() {
		return this.make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return this.model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public GeneralDetail getItemTypeCatdet() {
		return itemTypeCatdet;
	}

	public void setItemTypeCatdet(GeneralDetail itemTypeCatdet) {
		this.itemTypeCatdet = itemTypeCatdet;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public InvItemcategory getInvItemcategory() {
		return invItemcategory;
	}

	public void setInvItemcategory(InvItemcategory invItemcategory) {
		this.invItemcategory = invItemcategory;
	}

	public InvItemsubcategory getInvItemsubcategory() {
		return invItemsubcategory;
	}

	public void setInvItemsubcategory(InvItemsubcategory invItemsubcategory) {
		this.invItemsubcategory = invItemsubcategory;
	}

	public InvBrandmaster getInvBrandmaster() {
		return invBrandmaster;
	}

	public void setInvBrandmaster(InvBrandmaster invBrandmaster) {
		this.invBrandmaster = invBrandmaster;
	}
	
	
	
/*
	public List<InvInternalIndentitem> getInvInternalIndentitems() {
		return invInternalIndentitems;
	}

	public void setInvInternalIndentitems(List<InvInternalIndentitem> invInternalIndentitems) {
		this.invInternalIndentitems = invInternalIndentitems;
	}

	public List<InvInternalIssueItem> getInvInternalIssueItems() {
		return invInternalIssueItems;
	}

	public void setInvInternalIssueItems(List<InvInternalIssueItem> invInternalIssueItems) {
		this.invInternalIssueItems = invInternalIssueItems;
	}

	public List<InvInternalReturnItem> getInvInternalReturnItems() {
		return invInternalReturnItems;
	}

	public void setInvInternalReturnItems(List<InvInternalReturnItem> invInternalReturnItems) {
		this.invInternalReturnItems = invInternalReturnItems;
	}

	public List<InvItemDetail> getInvItemDetails() {
		return invItemDetails;
	}

	public void setInvItemDetails(List<InvItemDetail> invItemDetails) {
		this.invItemDetails = invItemDetails;
	}

	public InvItemcategory getInvItemcategory() {
		return invItemcategory;
	}

	public void setInvItemcategory(InvItemcategory invItemcategory) {
		this.invItemcategory = invItemcategory;
	}

	public InvItemsubcategory getInvItemsubcategory() {
		return invItemsubcategory;
	}

	public void setInvItemsubcategory(InvItemsubcategory invItemsubcategory) {
		this.invItemsubcategory = invItemsubcategory;
	}

	public InvBrandmaster getInvBrandmaster() {
		return invBrandmaster;
	}

	public void setInvBrandmaster(InvBrandmaster invBrandmaster) {
		this.invBrandmaster = invBrandmaster;
	}

	public List<InvItemopeningStock> getInvItemopeningStocks() {
		return invItemopeningStocks;
	}

	public void setInvItemopeningStocks(List<InvItemopeningStock> invItemopeningStocks) {
		this.invItemopeningStocks = invItemopeningStocks;
	}

	public List<InvPoItem> getInvPoItems1() {
		return invPoItems1;
	}

	public void setInvPoItems1(List<InvPoItem> invPoItems1) {
		this.invPoItems1 = invPoItems1;
	}

	public List<InvPoItem> getInvPoItems2() {
		return invPoItems2;
	}

	public void setInvPoItems2(List<InvPoItem> invPoItems2) {
		this.invPoItems2 = invPoItems2;
	}

	public List<InvPurchasereturnItem> getInvPurchasereturnItems1() {
		return invPurchasereturnItems1;
	}

	public void setInvPurchasereturnItems1(List<InvPurchasereturnItem> invPurchasereturnItems1) {
		this.invPurchasereturnItems1 = invPurchasereturnItems1;
	}

	public List<InvPurchasereturnItem> getInvPurchasereturnItems2() {
		return invPurchasereturnItems2;
	}

	public void setInvPurchasereturnItems2(List<InvPurchasereturnItem> invPurchasereturnItems2) {
		this.invPurchasereturnItems2 = invPurchasereturnItems2;
	}

	public List<InvSrvItem> getInvSrvItems1() {
		return invSrvItems1;
	}

	public void setInvSrvItems1(List<InvSrvItem> invSrvItems1) {
		this.invSrvItems1 = invSrvItems1;
	}

	public List<InvSrvItem> getInvSrvItems2() {
		return invSrvItems2;
	}

	public void setInvSrvItems2(List<InvSrvItem> invSrvItems2) {
		this.invSrvItems2 = invSrvItems2;
	}

	public List<InvStockledger> getInvStockledgers() {
		return invStockledgers;
	}

	public void setInvStockledgers(List<InvStockledger> invStockledgers) {
		this.invStockledgers = invStockledgers;
	}

	public List<InvStoreItem> getInvStoreItems1() {
		return invStoreItems1;
	}

	public void setInvStoreItems1(List<InvStoreItem> invStoreItems1) {
		this.invStoreItems1 = invStoreItems1;
	}

	public List<InvStoreItem> getInvStoreItems2() {
		return invStoreItems2;
	}

	public void setInvStoreItems2(List<InvStoreItem> invStoreItems2) {
		this.invStoreItems2 = invStoreItems2;
	}
	
*/}