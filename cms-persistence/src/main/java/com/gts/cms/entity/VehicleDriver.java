package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_tm_vehicle_drivers database table.
 * 
 */
@Entity
@Table(name="t_tm_vehicle_drivers")
@NamedQuery(name="VehicleDriver.findAll", query="SELECT v FROM VehicleDriver v")
public class VehicleDriver implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_vehicle_driver_id")
	private Long vehicleDriverId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to Driver
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_driver_id")
	private Driver driver;

	//bi-directional many-to-one association to Helper
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_helper_id")
	private Helper helper;

	//bi-directional many-to-one association to TransportDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_transport_detail_id")
	private TransportDetail transportDetail;

	//bi-directional many-to-one association to VehicleDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_vehicle_detail_id")
	private VehicleDetail vehicleDetail;

	public VehicleDriver() {
	}

	public Long getVehicleDriverId() {
		return this.vehicleDriverId;
	}

	public void setVehicleDriverId(Long vehicleDriverId) {
		this.vehicleDriverId = vehicleDriverId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Driver getDriver() {
		return this.driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public Helper getHelper() {
		return this.helper;
	}

	public void setHelper(Helper helper) {
		this.helper = helper;
	}

	public TransportDetail getTransportDetail() {
		return this.transportDetail;
	}

	public void setTransportDetail(TransportDetail transportDetail) {
		this.transportDetail = transportDetail;
	}

	public VehicleDetail getVehicleDetail() {
		return this.vehicleDetail;
	}

	public void setVehicleDetail(VehicleDetail vehicleDetail) {
		this.vehicleDetail = vehicleDetail;
	}

}