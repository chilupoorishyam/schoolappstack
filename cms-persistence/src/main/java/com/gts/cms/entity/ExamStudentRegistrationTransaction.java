package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the t_exam_student_registration_transactions
 * database table.
 * 
 */
@Entity
@Table(name = "t_exam_student_registration_transactions")
@NamedQuery(name = "ExamStudentRegistrationTransaction.findAll", query = "SELECT e FROM ExamStudentRegistrationTransaction e")
public class ExamStudentRegistrationTransaction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_std_reg_transaction_id")
	private Long examStdRegTransactionId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_id")
	private ExamMaster examMaster;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_reg_payment_id")
	private ExamStudentRegistrationPayment examRegPayment;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_transaction_verified_emp_id")
	private EmployeeDetail transactionVerifiedEmp;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_transactionapp_catdet_id")
	private GeneralDetail transactionAppCat;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_transaction_verified")
	private Boolean isTransactionVerified;

	private String reason;

	@Column(name = "transaction_amount")
	private BigDecimal transactionAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "transaction_date")
	private Date transactionDate;

	@Column(name = "transaction_path")
	private String transactionPath;

	@Column(name = "transaction_refno")
	private String transactionRefno;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "verification_comments")
	private String verificationComments;

	public ExamStudentRegistrationTransaction() {
	}

	public Long getExamStdRegTransactionId() {
		return examStdRegTransactionId;
	}

	public void setExamStdRegTransactionId(Long examStdRegTransactionId) {
		this.examStdRegTransactionId = examStdRegTransactionId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public ExamMaster getExamMaster() {
		return examMaster;
	}

	public void setExamMaster(ExamMaster examMaster) {
		this.examMaster = examMaster;
	}

	public ExamStudentRegistrationPayment getExamRegPayment() {
		return examRegPayment;
	}

	public void setExamRegPayment(ExamStudentRegistrationPayment examRegPayment) {
		this.examRegPayment = examRegPayment;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public EmployeeDetail getTransactionVerifiedEmp() {
		return transactionVerifiedEmp;
	}

	public void setTransactionVerifiedEmp(EmployeeDetail transactionVerifiedEmp) {
		this.transactionVerifiedEmp = transactionVerifiedEmp;
	}

	public GeneralDetail getTransactionAppCat() {
		return transactionAppCat;
	}

	public void setTransactionAppCat(GeneralDetail transactionAppCat) {
		this.transactionAppCat = transactionAppCat;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsTransactionVerified() {
		return isTransactionVerified;
	}

	public void setIsTransactionVerified(Boolean isTransactionVerified) {
		this.isTransactionVerified = isTransactionVerified;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionPath() {
		return transactionPath;
	}

	public void setTransactionPath(String transactionPath) {
		this.transactionPath = transactionPath;
	}

	public String getTransactionRefno() {
		return transactionRefno;
	}

	public void setTransactionRefno(String transactionRefno) {
		this.transactionRefno = transactionRefno;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getVerificationComments() {
		return verificationComments;
	}

	public void setVerificationComments(String verificationComments) {
		this.verificationComments = verificationComments;
	}
}