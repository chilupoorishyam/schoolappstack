package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_cm_staff_syllabus_plan database table.
 * 
 */
@Entity
@Table(name="t_cm_staff_syllabus_plan")
@NamedQuery(name="StaffSyllabusPlan.findAll", query="SELECT s FROM StaffSyllabusPlan s")
public class StaffSyllabusPlan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_staff_syllabus_plan_id")
	private Long staffSyllabusPlanId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	@Column(name="from_date")
	private Date fromDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="no_of_periods")
	private Integer noOfPeriods;

	private String reason;

	@Temporal(TemporalType.DATE)
	@Column(name="to_date")
	private Date toDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to StaffCourseyrSubject
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subcourseyr_staff_id")
	private StaffCourseyrSubject staffCourseyrSubject;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;

	public StaffSyllabusPlan() {
	}

	public Long getStaffSyllabusPlanId() {
		return this.staffSyllabusPlanId;
	}

	public void setStaffSyllabusPlanId(Long staffSyllabusPlanId) {
		this.staffSyllabusPlanId = staffSyllabusPlanId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getNoOfPeriods() {
		return this.noOfPeriods;
	}

	public void setNoOfPeriods(Integer noOfPeriods) {
		this.noOfPeriods = noOfPeriods;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public StaffCourseyrSubject getStaffCourseyrSubject() {
		return this.staffCourseyrSubject;
	}

	public void setStaffCourseyrSubject(StaffCourseyrSubject staffCourseyrSubject) {
		this.staffCourseyrSubject = staffCourseyrSubject;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

}