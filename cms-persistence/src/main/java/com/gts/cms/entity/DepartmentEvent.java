package com.gts.cms.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_de_dept_event database table.
 */
@Entity
@Setter
@Getter
@Table(name = "t_de_dept_event")
@NamedQuery(name = "DepartmentEvent.findAll", query = "SELECT d FROM DepartmentEvent d")
public class DepartmentEvent implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_dept_event_id")
    private Long deptEventId;

    //bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fk_school_id")
    private School schoolId;

    //bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fk_dept_id")
    private Department departmentId;

    //bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fk_academic_year_id")
    private AcademicYear academicYearId;

    @Column(name = "dept_event_name")
    private String deptEventName;

    @Column(name = "dept_event_description")
    private String deptEventDescription;

    @Column(name = "venue")
    private String venue;

    @Column(name = "total_expenditure")
    private Double totalExpenditure;

    @Column(name = "total_regisration_amount")
    private Double totalRegisrationAmount;

    @Column(name = "total_fee_collected")
    private Double totalFeeCollected;

    @Column(name = "broucher_url")
    private String broucherUrl;

    @Column(name = "poster_url")
    private String posterUrl;

    @Column(name = "permission_letter")
    private String permissionLetter;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_date")
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "bills_url")
    private String billsUrl;

    @Column(name = "feedback_url")
    private String feedbackUrl;

    @Column(name = "certificate1")
    private String certificate1;

    @Column(name = "certificate2")
    private String certificate2;

    @Column(name = "is_cancelled")
    private Boolean isCancelled;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cancelled_date")
    private Date cancelledDate;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "reason")
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    @OneToMany(mappedBy="deptEventId",fetch = FetchType.LAZY)
    private List<DepartmentEventAudience> departmentEventAudiences;

    @OneToMany(mappedBy="departmentEventId",fetch = FetchType.LAZY)
    private List<DepartmentEventPhoto> departmentEventPhoto;

    @OneToMany(mappedBy="deptEventId",fetch = FetchType.LAZY)
    private List<DepartmentEventResource> departmentEventResources;
}