package com.gts.cms.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

/**
 * The persistent class for the t_dl_assessments_settings database table.
 */
@Entity
@Setter
@Getter
@Table(name = "t_dl_assessments_settings")
@NamedQuery(name = "AssessmentSettings.findAll", query = "SELECT a FROM AssessmentSettings a")
public class AssessmentSettings implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_dl_assessment_settings_id")
    private Long assessmentSettingsId;

    @ManyToOne
    @JoinColumn(name = "fk_dl_assessment_id")
    private Assessment assessment;

    @Column(name = "is_test_available_always")
    private Boolean isTestAvailableAlways;

    @Column(name = "no_of_attempts")
    private Integer noOfAttempts;

    @Column(name = "reattempt_gap_days")
    private Integer reattemptGapDays;

    @Column(name = "reattempt_gap_time")
    private Time reattemptGapTime;

    @Column(name = "is_test_resume_allowed")
    private Boolean isTestResumeAllowed;

    @Column(name = "no_of_resume_allowed")
    private Integer noOfResumeAllowed;

    @Column(name = "is_question_insequence")
    private Boolean isQuestionInsequence;

    @Column(name = "is_displayquestion_all")
    private Boolean isDisplayquestionAll;

    @Column(name = "is_section_navigation_userchoice")
    private Boolean isSectionNavigationUserchoice;

    @Column(name = "question_attempt_limit")
    private Integer questionAttemptLimit;

    @Column(name = "is_previous_question_allowed")
    private Boolean isPreviousQuestionAllowed;

    @ManyToOne
    @JoinColumn(name = "fk_result_format_catdet_id")
    private GeneralDetail resultFormatCatdetId;

    @ManyToOne
    @JoinColumn(name = "fk_result_declaration_catdet_id")
    private GeneralDetail resultDeclarationCatdetId;

    @Column(name = "is_show_correctanswer")
    private Boolean isShowCorrectanswer;

    @Column(name = "is_test_public")
    private Boolean isTestPublic;

    @Column(name = "is_user_comment")
    private Boolean isUserComment;

    @Column(name = "is_email_notification")
    private Boolean isEmailNotification;

    @Column(name = "is_auto_debit")
    private Boolean isAutoDebit;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

}