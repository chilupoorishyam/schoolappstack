package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_dl_course_academic_map database table.
 * 
 */
@Entity
@Table(name = "t_dl_course_academic_map")
@NamedQuery(name = "CourseAcademicMap.findAll", query = "SELECT c FROM CourseAcademicMap c")
public class CourseAcademicMap implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_onlinecourse_academicmap_id")
	private Long onlinecourseAcademicmapId;

	@Temporal(TemporalType.DATE)
	@Column(name = "course_end_date")
	private Date courseEndDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "course_start_date")
	private Date courseStartDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne
	@JoinColumn(name = "fk_academic_year_id")
	private AcademicYear academicYear;

	@ManyToOne
	@JoinColumn(name = "fk_school_id")
	private School school;


	@ManyToOne
	@JoinColumn(name = "fk_course_year_id")
	private CourseYear courseYear;

	@ManyToOne
	@JoinColumn(name = "fk_electivetype_id")
	private Electivetype electiveType;



	@ManyToOne
	@JoinColumn(name = "fk_subject_id")
	private Subject subject;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	public CourseAcademicMap() {
	}

	public Long getOnlinecourseAcademicmapId() {
		return this.onlinecourseAcademicmapId;
	}

	public void setOnlinecourseAcademicmapId(Long onlinecourseAcademicmapId) {
		this.onlinecourseAcademicmapId = onlinecourseAcademicmapId;
	}

	public Date getCourseEndDate() {
		return this.courseEndDate;
	}

	public void setCourseEndDate(Date courseEndDate) {
		this.courseEndDate = courseEndDate;
	}

	public Date getCourseStartDate() {
		return this.courseStartDate;
	}

	public void setCourseStartDate(Date courseStartDate) {
		this.courseStartDate = courseStartDate;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public CourseYear getCourseYear() {
		return courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}



	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}



	public Electivetype getElectiveType() {
		return electiveType;
	}

	public void setElectiveType(Electivetype electiveType) {
		this.electiveType = electiveType;
	}

}