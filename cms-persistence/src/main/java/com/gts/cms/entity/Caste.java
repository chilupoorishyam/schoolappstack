package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_caste database table.
 * 
 */
@Entity
@Table(name="t_m_caste")
@NamedQuery(name="Caste.findAll", query="SELECT c FROM Caste c")
public class Caste implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_caste_id")
	private Long casteId;

	private String caste;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_eligible_for_reservation")
	private Boolean isEligibleForReservation;

	private String reason;

	@Column(name="sort_order")
	private Integer sortOrder;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="caste",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to SubCaste
	@OneToMany(mappedBy="caste",fetch = FetchType.LAZY)
	private List<SubCaste> subCastes;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="caste",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="caste",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails;

	public Caste() {
	}

	public Long getCasteId() {
		return this.casteId;
	}

	public void setCasteId(Long casteId) {
		this.casteId = casteId;
	}

	public String getCaste() {
		return this.caste;
	}

	public void setCaste(String caste) {
		this.caste = caste;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsEligibleForReservation() {
		return this.isEligibleForReservation;
	}

	public void setIsEligibleForReservation(Boolean isEligibleForReservation) {
		this.isEligibleForReservation = isEligibleForReservation;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<EmployeeDetail> getEmpDetails() {
		return this.empDetails;
	}

	public void setEmpDetails(List<EmployeeDetail> empDetails) {
		this.empDetails = empDetails;
	}

	public EmployeeDetail addEmpDetail(EmployeeDetail empDetail) {
		getEmpDetails().add(empDetail);
		empDetail.setCaste(this);

		return empDetail;
	}

	public EmployeeDetail removeEmpDetail(EmployeeDetail empDetail) {
		getEmpDetails().remove(empDetail);
		empDetail.setCaste(null);

		return empDetail;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public List<SubCaste> getSubCastes() {
		return this.subCastes;
	}

	public void setSubCastes(List<SubCaste> subCastes) {
		this.subCastes = subCastes;
	}

	public SubCaste addSubCaste(SubCaste subCaste) {
		getSubCastes().add(subCaste);
		subCaste.setCaste(this);

		return subCaste;
	}

	public SubCaste removeSubCaste(SubCaste subCaste) {
		getSubCastes().remove(subCaste);
		subCaste.setCaste(null);

		return subCaste;
	}

	public List<StudentApplication> getStdApplications() {
		return this.stdApplications;
	}

	public void setStdApplications(List<StudentApplication> stdApplications) {
		this.stdApplications = stdApplications;
	}

	public StudentApplication addStdApplication(StudentApplication stdApplication) {
		getStdApplications().add(stdApplication);
		stdApplication.setCaste(this);

		return stdApplication;
	}

	public StudentApplication removeStdApplication(StudentApplication stdApplication) {
		getStdApplications().remove(stdApplication);
		stdApplication.setCaste(null);

		return stdApplication;
	}

	public List<StudentDetail> getStdStudentDetails() {
		return this.stdStudentDetails;
	}

	public void setStdStudentDetails(List<StudentDetail> stdStudentDetails) {
		this.stdStudentDetails = stdStudentDetails;
	}

	public StudentDetail addStdStudentDetail(StudentDetail stdStudentDetail) {
		getStdStudentDetails().add(stdStudentDetail);
		stdStudentDetail.setCaste(this);

		return stdStudentDetail;
	}

	public StudentDetail removeStdStudentDetail(StudentDetail stdStudentDetail) {
		getStdStudentDetails().remove(stdStudentDetail);
		stdStudentDetail.setCaste(null);

		return stdStudentDetail;
	}

}