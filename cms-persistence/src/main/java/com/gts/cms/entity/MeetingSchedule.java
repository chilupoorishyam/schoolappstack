package com.gts.cms.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import com.gts.cms.converter.HashSetLongToStringConverter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
@Entity
public class MeetingSchedule {

    @Id
    @GeneratedValue(generator = "uid")
    @GenericGenerator(name = "uid", strategy = "uuid2")
    @Column(length = 36)
    private String id;

    private Long zoomMeetingId;
    private String zoomJoinUrl;

    @Column(columnDefinition = "text")
    private String zoomStartUrl;

    private String zoomHostId;
    private String zoomMeetingUuid;

    @NotNull
    private LocalDate eventDate;

    @NotNull
    private String topic; // name

    private String agenda; // description. long.

    @Pattern(regexp = "^[a-zA-Z0-9]+$")
    @NotNull
    @Size(min = 5, max = 10)
    private String password;

    @NotNull
    private Boolean hostVideo;

    @NotNull
    private Long hostId;

    @NotNull
    @NotEmpty
    @Convert(converter = HashSetLongToStringConverter.class)
    private Set<Long> sessionIds;

}
