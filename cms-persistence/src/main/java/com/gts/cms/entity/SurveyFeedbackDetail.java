package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_hr_survey_feedback_details database table.
 * 
 */
@Entity
@Table(name="t_hr_survey_feedback_details")
@NamedQuery(name="SurveyFeedbackDetail.findAll", query="SELECT s FROM SurveyFeedbackDetail s")
public class SurveyFeedbackDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_survery_fb_detail_id")
	private Long surveryFbDetailId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="fb_answer")
	private String fbAnswer;

	@Column(name="fb_answer_rating")
	private Integer fbAnswerRating;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FbOptionchoice
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fb_optionchoice_id")
	private FbOptionchoice fbOptionchoice;

	//bi-directional many-to-one association to SurveyDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_survey_details_id")
	private SurveyDetail surveyDetail;

	//bi-directional many-to-one association to SurveyFeedback
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_survery_fb_id")
	private SurveyFeedback surveyFeedback;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	public SurveyFeedbackDetail() {
	}

	public Long getSurveryFbDetailId() {
		return this.surveryFbDetailId;
	}

	public void setSurveryFbDetailId(Long surveryFbDetailId) {
		this.surveryFbDetailId = surveryFbDetailId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getFbAnswer() {
		return this.fbAnswer;
	}

	public void setFbAnswer(String fbAnswer) {
		this.fbAnswer = fbAnswer;
	}

	public Integer getFbAnswerRating() {
		return this.fbAnswerRating;
	}

	public void setFbAnswerRating(Integer fbAnswerRating) {
		this.fbAnswerRating = fbAnswerRating;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public FbOptionchoice getFbOptionchoice() {
		return this.fbOptionchoice;
	}

	public void setFbOptionchoice(FbOptionchoice fbOptionchoice) {
		this.fbOptionchoice = fbOptionchoice;
	}

	public SurveyDetail getSurveyDetail() {
		return this.surveyDetail;
	}

	public void setSurveyDetail(SurveyDetail surveyDetail) {
		this.surveyDetail = surveyDetail;
	}

	public SurveyFeedback getSurveyFeedback() {
		return this.surveyFeedback;
	}

	public void setSurveyFeedback(SurveyFeedback surveyFeedback) {
		this.surveyFeedback = surveyFeedback;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

}