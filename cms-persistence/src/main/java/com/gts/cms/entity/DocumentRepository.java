package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_document_repository database table.
 * 
 */
@Entity
@Table(name="t_m_document_repository")
@NamedQuery(name="DocumentRepository.findAll", query="SELECT d FROM DocumentRepository d")
public class DocumentRepository implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_doc_rep_id")
	private Long documentRepositoryId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="doc_code")
	private String docCode;

	@Column(name="doc_name")
	private String docName;

	@Column(name="filename_format")
	private String filenameFormat;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_for_emp")
	private Boolean isForEmp;

	@Column(name="is_for_student")
	private Boolean isForStudent;

	@Column(name="is_mandatory")
	private Boolean isMandatory;

	private String reason;

	@Column(name="sort_order")
	private Integer sortOrder;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeeDocumentCollection
	@OneToMany(mappedBy="documentRepository",fetch = FetchType.LAZY)
	private List<EmployeeDocumentCollection> empDocumentCollections;

	//bi-directional many-to-one association to Course
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_id")
	private Course course;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_doctype_catdet_id")
	private GeneralDetail doctype;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_docform_catdet_id")
	private GeneralDetail docform;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to StudentAppDocCollection
	@OneToMany(mappedBy="documentRepository",fetch = FetchType.LAZY)
	private List<StudentAppDocCollection> stdAppDocCollections;

	//bi-directional many-to-one association to StudentDocumentCollection
	@OneToMany(mappedBy="documentRepository",fetch = FetchType.LAZY)
	private List<StudentDocumentCollection> stdDocumentCollections;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	public DocumentRepository() {
	}

	public Long getDocumentRepositoryId() {
		return this.documentRepositoryId;
	}

	public void setDocumentRepositoryId(Long documentRepositoryId) {
		this.documentRepositoryId = documentRepositoryId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDocCode() {
		return this.docCode;
	}

	public void setDocCode(String docCode) {
		this.docCode = docCode;
	}

	public String getDocName() {
		return this.docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public String getFilenameFormat() {
		return this.filenameFormat;
	}

	public void setFilenameFormat(String filenameFormat) {
		this.filenameFormat = filenameFormat;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsForEmp() {
		return this.isForEmp;
	}

	public void setIsForEmp(Boolean isForEmp) {
		this.isForEmp = isForEmp;
	}

	public Boolean getIsForStudent() {
		return this.isForStudent;
	}

	public void setIsForStudent(Boolean isForStudent) {
		this.isForStudent = isForStudent;
	}

	public Boolean getIsMandatory() {
		return this.isMandatory;
	}

	public void setIsMandatory(Boolean isMandatory) {
		this.isMandatory = isMandatory;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<EmployeeDocumentCollection> getEmpDocumentCollections() {
		return this.empDocumentCollections;
	}

	public void setEmpDocumentCollections(List<EmployeeDocumentCollection> empDocumentCollections) {
		this.empDocumentCollections = empDocumentCollections;
	}

	public EmployeeDocumentCollection addEmpDocumentCollection(EmployeeDocumentCollection empDocumentCollection) {
		getEmpDocumentCollections().add(empDocumentCollection);
		empDocumentCollection.setDocumentRepository(this);

		return empDocumentCollection;
	}

	public EmployeeDocumentCollection removeEmpDocumentCollection(EmployeeDocumentCollection empDocumentCollection) {
		getEmpDocumentCollections().remove(empDocumentCollection);
		empDocumentCollection.setDocumentRepository(null);

		return empDocumentCollection;
	}

	public Course getCourse() {
		return this.course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public GeneralDetail getDoctype() {
		return this.doctype;
	}

	public void setDoctype(GeneralDetail doctype) {
		this.doctype = doctype;
	}

	public GeneralDetail getDocform() {
		return this.docform;
	}

	public void setDocform(GeneralDetail docform) {
		this.docform = docform;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public List<StudentAppDocCollection> getStdAppDocCollections() {
		return this.stdAppDocCollections;
	}

	public void setStdAppDocCollections(List<StudentAppDocCollection> stdAppDocCollections) {
		this.stdAppDocCollections = stdAppDocCollections;
	}

	public StudentAppDocCollection addStdAppDocCollection(StudentAppDocCollection stdAppDocCollection) {
		getStdAppDocCollections().add(stdAppDocCollection);
		stdAppDocCollection.setDocumentRepository(this);

		return stdAppDocCollection;
	}

	public StudentAppDocCollection removeStdAppDocCollection(StudentAppDocCollection stdAppDocCollection) {
		getStdAppDocCollections().remove(stdAppDocCollection);
		stdAppDocCollection.setDocumentRepository(null);

		return stdAppDocCollection;
	}

	public List<StudentDocumentCollection> getStdDocumentCollections() {
		return this.stdDocumentCollections;
	}

	public void setStdDocumentCollections(List<StudentDocumentCollection> stdDocumentCollections) {
		this.stdDocumentCollections = stdDocumentCollections;
	}

	public StudentDocumentCollection addStdDocumentCollection(StudentDocumentCollection stdDocumentCollection) {
		getStdDocumentCollections().add(stdDocumentCollection);
		stdDocumentCollection.setDocumentRepository(this);

		return stdDocumentCollection;
	}

	public StudentDocumentCollection removeStdDocumentCollection(StudentDocumentCollection stdDocumentCollection) {
		getStdDocumentCollections().remove(stdDocumentCollection);
		stdDocumentCollection.setDocumentRepository(null);

		return stdDocumentCollection;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

}