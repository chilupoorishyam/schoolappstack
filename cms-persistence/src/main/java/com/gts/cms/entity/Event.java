package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_events database table.
 * 
 */
@Entity
@Table(name="t_m_events")
@NamedQuery(name="Event.findAll", query="SELECT e FROM Event e")
public class Event implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_event_id")
	private Long eventId;

	@Column(name="can_enable_reminders")
	private Boolean canEnableReminders;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String description;

	@Lob
	@Column(name="email_message")
	private String emailMessage;

	@Temporal(TemporalType.DATE)
	@Column(name="end_date")
	private Date endDate;

	@Column(name="event_name")
	private String eventName;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_holiday")
	private Boolean isHoliday;

	@Column(name="is_published")
	private Boolean isPublished;
	
	@Column(name="is_week_off")
	private Boolean isWeekOff;

	@Column(name="organizer_details")
	private String organizerDetails;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="publish_date")
	private Date publishDate;

	private String reason;

	@Column(name="remainder_mode_email")
	private Boolean remainderModeEmail;

	@Column(name="remainder_mode_sms_popup")
	private Boolean remainderModeSmsPopup;

	@Column(name="remainder1_before_x_days")
	private Integer remainder1BeforeXDays;

	@Column(name="remainder2_before_x_days")
	private Integer remainder2BeforeXDays;

	@Column(name="remainder3_before_x_days")
	private Integer remainder3BeforeXDays;

	@Lob
	@Column(name="sms_popup_message")
	private String smsPopupMessage;

	@Temporal(TemporalType.DATE)
	@Column(name="start_date")
	private Date startDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EventAudience
	@OneToMany(mappedBy="event",fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	private List<EventAudience> eventAudiences;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_incharge_emp_id")
	private EmployeeDetail inchargeEmployeeDetail;

	//bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to EventType
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_event_type")
	private EventType eventType;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_eventstatus_catdet_id")
	private GeneralDetail eventStatus;

	public Event() {
	}

	public Long getEventId() {
		return this.eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public Boolean getCanEnableReminders() {
		return this.canEnableReminders;
	}

	public void setCanEnableReminders(Boolean canEnableReminders) {
		this.canEnableReminders = canEnableReminders;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEmailMessage() {
		return this.emailMessage;
	}

	public void setEmailMessage(String emailMessage) {
		this.emailMessage = emailMessage;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getEventName() {
		return this.eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsHoliday() {
		return this.isHoliday;
	}

	public void setIsHoliday(Boolean isHoliday) {
		this.isHoliday = isHoliday;
	}

	public Boolean getIsPublished() {
		return this.isPublished;
	}

	public void setIsPublished(Boolean isPublished) {
		this.isPublished = isPublished;
	}

	public String getOrganizerDetails() {
		return this.organizerDetails;
	}

	public void setOrganizerDetails(String organizerDetails) {
		this.organizerDetails = organizerDetails;
	}

	public Date getPublishDate() {
		return this.publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Boolean getRemainderModeEmail() {
		return this.remainderModeEmail;
	}

	public void setRemainderModeEmail(Boolean remainderModeEmail) {
		this.remainderModeEmail = remainderModeEmail;
	}

	public Boolean getRemainderModeSmsPopup() {
		return this.remainderModeSmsPopup;
	}

	public void setRemainderModeSmsPopup(Boolean remainderModeSmsPopup) {
		this.remainderModeSmsPopup = remainderModeSmsPopup;
	}

	public Integer getRemainder1BeforeXDays() {
		return this.remainder1BeforeXDays;
	}

	public void setRemainder1BeforeXDays(Integer remainder1BeforeXDays) {
		this.remainder1BeforeXDays = remainder1BeforeXDays;
	}

	public Integer getRemainder2BeforeXDays() {
		return this.remainder2BeforeXDays;
	}

	public void setRemainder2BeforeXDays(Integer remainder2BeforeXDays) {
		this.remainder2BeforeXDays = remainder2BeforeXDays;
	}

	public Integer getRemainder3BeforeXDays() {
		return this.remainder3BeforeXDays;
	}

	public void setRemainder3BeforeXDays(Integer remainder3BeforeXDays) {
		this.remainder3BeforeXDays = remainder3BeforeXDays;
	}

	public String getSmsPopupMessage() {
		return this.smsPopupMessage;
	}

	public void setSmsPopupMessage(String smsPopupMessage) {
		this.smsPopupMessage = smsPopupMessage;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<EventAudience> getEventAudiences() {
		return this.eventAudiences;
	}

	public void setEventAudiences(List<EventAudience> eventAudiences) {
		this.eventAudiences = eventAudiences;
	}

	public EventAudience addEventAudience(EventAudience eventAudience) {
		getEventAudiences().add(eventAudience);
		eventAudience.setEvent(this);

		return eventAudience;
	}

	public EventAudience removeEventAudience(EventAudience eventAudience) {
		getEventAudiences().remove(eventAudience);
		eventAudience.setEvent(null);

		return eventAudience;
	}

	public EmployeeDetail getInchargeEmployeeDetail() {
		return this.inchargeEmployeeDetail;
	}

	public void setInchargeEmployeeDetail(EmployeeDetail inchargeEmployeeDetail) {
		this.inchargeEmployeeDetail = inchargeEmployeeDetail;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public EventType getEventType() {
		return this.eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	public GeneralDetail getEventStatus() {
		return this.eventStatus;
	}

	public void setEventStatus(GeneralDetail eventStatus) {
		this.eventStatus = eventStatus;
	}

	public Boolean getIsWeekOff() {
		return isWeekOff;
	}

	public void setIsWeekOff(Boolean isWeekOff) {
		this.isWeekOff = isWeekOff;
	}
}