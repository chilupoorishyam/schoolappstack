package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_exam_yearwise_marks database table.
 */
@Data
@Entity
@Table(name = "t_exam_yearwise_marks")
@NamedQuery(name = "ExamYearwiseMarks.findAll", query = "SELECT e FROM ExamYearwiseMarks e")
public class ExamYearwiseMarks implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_exam_yearwise_marks_id")
    private Long examYearwiseMarksId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "total_subjects")
    private Integer totalSubjects;

    @Column(name = "present_subjects")
    private Integer presentSubjects;

    @Column(name = "pass_subjects")
    private Integer passSubjects;

    @Column(name = "total_marks_secured")
    private Double totalMarksSecured;

    @Column(name = "total_marks")
    private Double totalMarks;

    @Column(name = "grade")
    private String grade;

    @Column(name = "grade_points")
    private String gradePoints;

    @Column(name = "marks_comments")
    private String marksComments;

    @Column(name = "is_marks_published")
    private Boolean isMarksPublished;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_school_id")
    private School school;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_student_id")
    private StudentDetail studentDetail;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_course_year_id")
    private CourseYear courseYear;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_examgroup_id")
    private ExamGrouping examGrouping;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_exam_grades_id")
    private ExamGrade examGrade;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_marks_entered_emp_id")
    private EmployeeDetail marksEnteredEmployeeDetail;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;
}