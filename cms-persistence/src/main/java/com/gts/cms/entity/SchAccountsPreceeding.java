package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_sch_accounts_preceedings database table.
 * 
 */
@Entity
@Table(name = "t_sch_accounts_preceedings")
@NamedQuery(name = "SchAccountsPreceeding.findAll", query = "SELECT s FROM SchAccountsPreceeding s")
public class SchAccountsPreceeding implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_sch_acc_preceedings_id")
	private Long schAccountsPreceedingsId;

	@Column(name = "bank_code")
	private String bankCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cheque_date")
	private Date chequeDate;

	@Column(name = "cheque_no")
	private String chequeNo;

	private String comments;

	@Column(name = "cheque_amount")
	private String chequeAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "fk_sch_preceeding_ids")
	private String schPreceedingIds;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_hand_overto_acc")
	private Boolean isHandOvertoAcc;

	private String reason;

	private String title;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to Bank
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_bank_id")
	private Bank bank;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to SchPreceeding
	@OneToMany(mappedBy = "accountsPreceeding", fetch = FetchType.LAZY)
	private List<SchPreceeding> preceedings;

	public SchAccountsPreceeding() {
	}

	public Long getSchAccountsPreceedingsId() {
		return this.schAccountsPreceedingsId;
	}

	public void setSchAccountsPreceedingsId(Long schAccountsPreceedingsId) {
		this.schAccountsPreceedingsId = schAccountsPreceedingsId;
	}

	public String getBankCode() {
		return this.bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public Date getChequeDate() {
		return this.chequeDate;
	}

	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getChequeNo() {
		return this.chequeNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getSchPreceedingIds() {
		return schPreceedingIds;
	}

	public void setSchPreceedingIds(String schPreceedingIds) {
		this.schPreceedingIds = schPreceedingIds;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsHandOvertoAcc() {
		return this.isHandOvertoAcc;
	}

	public void setIsHandOvertoAcc(Boolean isHandOvertoAcc) {
		this.isHandOvertoAcc = isHandOvertoAcc;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Bank getBank() {
		return this.bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<SchPreceeding> getPreceedings() {
		return this.preceedings;
	}

	public void setPreceedings(List<SchPreceeding> preceedings) {
		this.preceedings = preceedings;
	}

	public String getChequeAmount() {
		return chequeAmount;
	}

	public void setChequeAmount(String chequeAmount) {
		this.chequeAmount = chequeAmount;
	}

	public SchPreceeding addPreceeding(SchPreceeding preceeding) {
		getPreceedings().add(preceeding);
		preceeding.setAccountsPreceeding(this);

		return preceeding;
	}

	public SchPreceeding removePreceeding(SchPreceeding preceeding) {
		getPreceedings().remove(preceeding);
		preceeding.setAccountsPreceeding(null);

		return preceeding;
	}

}