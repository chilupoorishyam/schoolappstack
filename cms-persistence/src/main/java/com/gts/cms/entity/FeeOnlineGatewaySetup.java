package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_fee_online_gateway_setup database table.
 * 
 */
@Entity
@Table(name = "t_fee_online_gateway_setup")
@NamedQuery(name = "FeeOnlineGatewaySetup.findAll", query = "SELECT f FROM FeeOnlineGatewaySetup f")
public class FeeOnlineGatewaySetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_online_gateway_setup_id")
	private Long onlineGatewaySetupId;

	@Column(name = "auth_modes")
	private String authModes;

	@Column(name = "callback_url")
	private String callbackUrl;

	@Column(name = "channel_id")
	private String channelId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "email_id")
	private String emailId;

	@ManyToOne
	@JoinColumn(name = "fk_school_id")
	private School school;

	@ManyToOne
	@JoinColumn(name = "fk_gateway_catdet_id")
	private GeneralDetail gatewayCat;

	@ManyToOne
	@JoinColumn(name = "fk_module_id")
	private Module module;

	@Column(name = "industry_type_id")
	private String industryTypeId;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_allow_custome_paymode")
	private Boolean isAllowCustomePaymode;

	@Column(name = "merchant_id")
	private String merchantId;

	@Column(name = "mobile_no")
	private String mobileNo;

	@Column(name = "payment_type_code")
	private String paymentTypeCode;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	private String website;

	@Column(name = "accesskey")
	private String accessKey;

	@Column(name = "securitytoken")
	private String securityToken;

	public FeeOnlineGatewaySetup() {
	}

	public Long getOnlineGatewaySetupId() {
		return this.onlineGatewaySetupId;
	}

	public void setOnlineGatewaySetupId(Long onlineGatewaySetupId) {
		this.onlineGatewaySetupId = onlineGatewaySetupId;
	}

	public String getAuthModes() {
		return this.authModes;
	}

	public void setAuthModes(String authModes) {
		this.authModes = authModes;
	}

	public String getCallbackUrl() {
		return this.callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	public String getChannelId() {
		return this.channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getEmailId() {
		return this.emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getIndustryTypeId() {
		return this.industryTypeId;
	}

	public void setIndustryTypeId(String industryTypeId) {
		this.industryTypeId = industryTypeId;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsAllowCustomePaymode() {
		return this.isAllowCustomePaymode;
	}

	public void setIsAllowCustomePaymode(Boolean isAllowCustomePaymode) {
		this.isAllowCustomePaymode = isAllowCustomePaymode;
	}

	public String getMerchantId() {
		return this.merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getMobileNo() {
		return this.mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getPaymentTypeCode() {
		return this.paymentTypeCode;
	}

	public void setPaymentTypeCode(String paymentTypeCode) {
		this.paymentTypeCode = paymentTypeCode;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getGatewayCat() {
		return gatewayCat;
	}

	public void setGatewayCat(GeneralDetail gatewayCat) {
		this.gatewayCat = gatewayCat;
	}

	public Module getModule() {
		return module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

	public Boolean getActive() {
		return isActive;
	}

	public void setActive(Boolean active) {
		isActive = active;
	}

	public Boolean getAllowCustomePaymode() {
		return isAllowCustomePaymode;
	}

	public void setAllowCustomePaymode(Boolean allowCustomePaymode) {
		isAllowCustomePaymode = allowCustomePaymode;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public String getSecurityToken() {
		return securityToken;
	}

	public void setSecurityToken(String securityToken) {
		this.securityToken = securityToken;
	}
}