package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_inv_internal_indentitems database table.
 * 
 */
@Entity
@Table(name = "t_inv_internal_indentitems")
@NamedQuery(name = "InvInternalIndentitem.findAll", query = "SELECT i FROM InvInternalIndentitem i")
public class InvInternalIndentitem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_inter_ind_item_id")
	private Long interIndItemId;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "delivery_date")
	private Date deliveryDate;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_indent_status_catdet_id")
	private GeneralDetail indentStatusCatdetId;

	@Column(name = "indent_quantity")
	private BigDecimal indentQuantity;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "issued_qty")
	private BigDecimal issuedQty;

	private String reason;

	@Column(name = "rejected_qty")
	private BigDecimal rejectedQty;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to InvStoresmaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_store_id")
	private InvStoresmaster invStoresmaster;

	// bi-directional many-to-one association to InvItemmaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_item_id")
	private InvItemmaster invItemmaster;
	
	// bi-directional many-to-one association to InvInternalIndent
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_internal_ind_id")
	private InvInternalIndent invInternalIndent;
/*
	// bi-directional many-to-one association to InvInternalIssueItem
	@OneToMany(mappedBy = "TInvInternalIndentitem")
	private List<InvInternalIssueItem> invInternalIssueItems;
*/
	public InvInternalIndentitem() {
	}

	public Long getInterIndItemId() {
		return this.interIndItemId;
	}

	public void setInterIndItemId(Long interIndItemId) {
		this.interIndItemId = interIndItemId;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getDeliveryDate() {
		return this.deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public GeneralDetail getIndentStatusCatdetId() {
		return indentStatusCatdetId;
	}

	public void setIndentStatusCatdetId(GeneralDetail indentStatusCatdetId) {
		this.indentStatusCatdetId = indentStatusCatdetId;
	}

	public BigDecimal getIndentQuantity() {
		return this.indentQuantity;
	}

	public void setIndentQuantity(BigDecimal indentQuantity) {
		this.indentQuantity = indentQuantity;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public BigDecimal getIssuedQty() {
		return this.issuedQty;
	}

	public void setIssuedQty(BigDecimal issuedQty) {
		this.issuedQty = issuedQty;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getRejectedQty() {
		return this.rejectedQty;
	}

	public void setRejectedQty(BigDecimal rejectedQty) {
		this.rejectedQty = rejectedQty;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public InvStoresmaster getInvStoresmaster() {
		return invStoresmaster;
	}

	public void setInvStoresmaster(InvStoresmaster invStoresmaster) {
		this.invStoresmaster = invStoresmaster;
	}

	public InvItemmaster getInvItemmaster() {
		return invItemmaster;
	}

	public void setInvItemmaster(InvItemmaster invItemmaster) {
		this.invItemmaster = invItemmaster;
	}

	public InvInternalIndent getInvInternalIndent() {
		return invInternalIndent;
	}

	public void setInvInternalIndent(InvInternalIndent invInternalIndent) {
		this.invInternalIndent = invInternalIndent;
	}

/*	public List<InvInternalIssueItem> getInvInternalIssueItems() {
		return invInternalIssueItems;
	}

	public void setInvInternalIssueItems(List<InvInternalIssueItem> invInternalIssueItems) {
		this.invInternalIssueItems = invInternalIssueItems;
	}
*/
	
}