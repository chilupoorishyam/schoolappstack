package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_exam_student_internal_marks database table.
 * 
 */
@Entity
@Table(name="t_exam_student_internal_marks")
@NamedQuery(name="ExamStudentInternalMark.findAll", query="SELECT e FROM ExamStudentInternalMark e")
public class ExamStudentInternalMark implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_exam_std_internal_mark_id")
	private Long examStdInternalMarkId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	@Column(name="exam_date")
	private Date examDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_present")
	private Boolean isPresent;

	@Column(name="is_published")
	private Boolean isPublished;

	private Integer marks;

	@Temporal(TemporalType.DATE)
	@Column(name="published_on")
	private Date publishedOn;

	private String reason;

	@Column(name="review_comments")
	private String reviewComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="reviewed_on")
	private Date reviewedOn;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to TMSchool
	@ManyToOne
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to ExamMaster
	@ManyToOne
	@JoinColumn(name="fk_exam_id")
	private ExamMaster examMaster;

	//bi-directional many-to-one association to TStdStudentDetail
	@ManyToOne
	@JoinColumn(name="fk_student_id")
	private StudentDetail studentDetail;

	//bi-directional many-to-one association to TMCourseYear
	@ManyToOne
	@JoinColumn(name="fk_course_year_id")
	private CourseYear courseYear;

	//bi-directional many-to-one association to TMSubject
	@ManyToOne
	@JoinColumn(name="fk_subject_id")
	private Subject subject;

	//bi-directional many-to-one association to ExamTimetable
	@ManyToOne
	@JoinColumn(name="fk_exam_timetable_id")
	private ExamTimetable examTimetable;

	//bi-directional many-to-one association to ExamTimetableDetail
	@ManyToOne
	@JoinColumn(name="fk_exam_timetable_det_id")
	private ExamTimetableDetail examTimetableDetail;

	//bi-directional many-to-one association to TEmpDetail
	@ManyToOne
	@JoinColumn(name="fk_reviewer_emp_id")
	private EmployeeDetail employeeDetail;

	public ExamStudentInternalMark() {
	}

	public Long getExamStdInternalMarkId() {
		return examStdInternalMarkId;
	}

	public void setExamStdInternalMarkId(Long examStdInternalMarkId) {
		this.examStdInternalMarkId = examStdInternalMarkId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getExamDate() {
		return examDate;
	}

	public void setExamDate(Date examDate) {
		this.examDate = examDate;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsPresent() {
		return isPresent;
	}

	public void setIsPresent(Boolean isPresent) {
		this.isPresent = isPresent;
	}

	public Boolean getIsPublished() {
		return isPublished;
	}

	public void setIsPublished(Boolean isPublished) {
		this.isPublished = isPublished;
	}

	public Integer getMarks() {
		return marks;
	}

	public void setMarks(Integer marks) {
		this.marks = marks;
	}

	public Date getPublishedOn() {
		return publishedOn;
	}

	public void setPublishedOn(Date publishedOn) {
		this.publishedOn = publishedOn;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReviewComments() {
		return reviewComments;
	}

	public void setReviewComments(String reviewComments) {
		this.reviewComments = reviewComments;
	}

	public Date getReviewedOn() {
		return reviewedOn;
	}

	public void setReviewedOn(Date reviewedOn) {
		this.reviewedOn = reviewedOn;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public ExamMaster getExamMaster() {
		return examMaster;
	}

	public void setExamMaster(ExamMaster examMaster) {
		this.examMaster = examMaster;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public CourseYear getCourseYear() {
		return courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public ExamTimetable getExamTimetable() {
		return examTimetable;
	}

	public void setExamTimetable(ExamTimetable examTimetable) {
		this.examTimetable = examTimetable;
	}

	public ExamTimetableDetail getExamTimetableDetail() {
		return examTimetableDetail;
	}

	public void setExamTimetableDetail(ExamTimetableDetail examTimetableDetail) {
		this.examTimetableDetail = examTimetableDetail;
	}

	public EmployeeDetail getEmployeeDetail() {
		return employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}
	
	
}