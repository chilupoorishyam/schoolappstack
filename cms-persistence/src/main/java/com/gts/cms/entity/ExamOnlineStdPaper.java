package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_exam_online_std_paper database table.
 */
@Data
@Entity
@Table(name = "t_exam_online_std_paper")
@NamedQuery(name = "ExamOnlineStdPaper.findAll", query = "SELECT e FROM ExamOnlineStdPaper e")
public class ExamOnlineStdPaper implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_exam_online_std_paper_id")
    private Long examOnlineStdPaperId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    // bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_school_id")
    private School school;

    // bi-directional many-to-one association to Course
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_exam_online_paper_id")
    private ExamOnlinePaper examOnlinePaperId;

    // bi-directional many-to-one association to Course
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_wf_exampapersubmission_status_id")
    private WorkflowStage wfExampapersubmissionStatusId;

    // bi-directional many-to-one association to Course
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_student_id   ")
    private StudentDetail studentDetail;

    @Column(name = "submssion_file")
    private String submssionFile;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "status_updated_on")
    private Date statusUpdatedOn;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "status_comments")
    private String statusComments;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "examstdpaper_submitted_on")
    private Date examstdpaperSubmittedOn;

    @Column(name = "is_review_completed")
    private Boolean isReviewCompleted;

    @Column(name = "review_submission_file")
    private String reviewSubmissionFile;

    @Column(name = "marks_secured")
    private String marksSecured;

    @Column(name = "review_comments")
    private String reviewComments;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    //bi-directional many-to-one association to StaffCourseyrSubject
    @OneToOne(mappedBy="fkExamOnlineStdPaperId",fetch = FetchType.LAZY)
    private ExamOnlineStdWorkflow examOnlineStdWorkflow;
}