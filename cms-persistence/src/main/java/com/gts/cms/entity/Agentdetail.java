package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_std_agentdetails database table.
 * 
 */
@Entity
@Table(name="t_std_agentdetails")
@NamedQuery(name="Agentdetail.findAll", query="SELECT a FROM Agentdetail a")
public class Agentdetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_agentdetial_id")
	private Long agentdetialId;

	private Integer age;

	private String agentname;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String details;

	@Column(name="email_id")
	private String emailId;

	private String fathername;

	@Column(name="is_active")
	private Boolean isActive;

	private String mobileno;

	private String reason;

	private String spousename;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_gender_catdet_id")
	private GeneralDetail gender;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to ReferralDetail
	@OneToMany(mappedBy="agentdetail",fetch = FetchType.LAZY)
	private List<ReferralDetail> referralDetails;

	public Agentdetail() {
	}

	public Long getAgentdetialId() {
		return this.agentdetialId;
	}

	public void setAgentdetialId(Long agentdetialId) {
		this.agentdetialId = agentdetialId;
	}

	public Integer getAge() {
		return this.age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getAgentname() {
		return this.agentname;
	}

	public void setAgentname(String agentname) {
		this.agentname = agentname;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDetails() {
		return this.details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getEmailId() {
		return this.emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFathername() {
		return this.fathername;
	}

	public void setFathername(String fathername) {
		this.fathername = fathername;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getMobileno() {
		return this.mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSpousename() {
		return this.spousename;
	}

	public void setSpousename(String spousename) {
		this.spousename = spousename;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getGender() {
		return this.gender;
	}

	public void setGender(GeneralDetail gender) {
		this.gender = gender;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public List<ReferralDetail> getReferralDetails() {
		return this.referralDetails;
	}

	public void setReferralDetails(List<ReferralDetail> referralDetails) {
		this.referralDetails = referralDetails;
	}

	public ReferralDetail addReferralDetail(ReferralDetail referralDetail) {
		getReferralDetails().add(referralDetail);
		referralDetail.setAgentdetail(this);

		return referralDetail;
	}

	public ReferralDetail removeReferralDetail(ReferralDetail referralDetail) {
		getReferralDetails().remove(referralDetail);
		referralDetail.setAgentdetail(null);

		return referralDetail;
	}

}