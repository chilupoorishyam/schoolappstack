package com.gts.cms.entity;

import com.gts.cms.converter.HashSetLongToStringConverter;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
@Entity
@Table(name = "t_codiss_live_cls_schedule")
@NamedQuery(name = "CodissLiveClassSchedule.findAll", query = "SELECT ls FROM CodissLiveClassSchedule ls")
public class CodissLiveClassSchedule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_codiss_live_cls_schedule_id")
    private Long codissLiveClsScheduleId;

    @Column(name = "scheduled_on_date")
    private LocalDate scheduledOnDate;

    @Column(name = "from_time")
    private LocalTime fromTime;

    @Column(name = "to_time")
    private LocalTime toTime;

    @Column(name = "no")
    private Long no;

    @Column(name = "is_onetime")
    private Boolean isOnetime;

    @Column(name = "host_video")
    private Boolean hostVideo;

    @Column(name = "is_recurring")
    private Boolean isRecurring;

    @Column(name = "agenda")
    private String agenda;

    @Column(name = "codiis_host_url")
    private String codiisHostUrl;

    @Column(name = "password")
    private String password;

    @Column(name = "sessionIds")
    @Convert(converter = HashSetLongToStringConverter.class)
    private Set<Long> sessionIds;

    private String topic;

    @Column(name = "codiis_meeting_id")
    private String codiisMeetingId;

    @Column(name = "live_cls_schedule_id")
    private String liveClsScheduleId;

    @Column(name = "token")
    private String token;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "reason")
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private String createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private String updatedUser;

    @ManyToOne
    @JoinColumn(name = "fk_school_id")
    private School school;

    @ManyToOne
    @JoinColumn(name = "fk_group_section_id")
    private GroupSection groupSection;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_timetable_schedule_id")
    private Schedule schedule;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_cls_emp_id")
    private EmployeeDetail classEmployeeDetail;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_subjecttype_catdet_id")
    private GeneralDetail subjecttype;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_subject_id")
    private Subject subject;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_stdbatch_id")
    private Studentbatch studentbatch;

    //bi-directional many-to-one association to Weekday
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_weekday_id")
    private Weekday weekday;

    //bi-directional many-to-one association to ClassTiming
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_class_timing_id")
    private ClassTiming classTiming;
    //bi-directional many-to-one association to ClassTiming
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_session_id")
    private ZoomSession sessionId;
}
