package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_grv_grievance_categories database table.
 * 
 */
@Entity
@Table(name = "t_grv_grievance_categories")
@NamedQuery(name = "GrievanceCategory.findAll", query = "SELECT g FROM GrievanceCategory g")
public class GrievanceCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_grv_category_id")
	private Long categoryId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "grievance_category")
	private String grievanceCategory;

	@Column(name = "grievance_category_code")
	private String grievanceCategoryCode;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to ComplaintsList
	@OneToMany(mappedBy = "grievanceCategory")
	private List<ComplaintsList> complaintsLists;

	public GrievanceCategory() {
	}

	public Long getCategoryId() {
		return this.categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getGrievanceCategory() {
		return this.grievanceCategory;
	}

	public void setGrievanceCategory(String grievanceCategory) {
		this.grievanceCategory = grievanceCategory;
	}

	public String getGrievanceCategoryCode() {
		return this.grievanceCategoryCode;
	}

	public void setGrievanceCategoryCode(String grievanceCategoryCode) {
		this.grievanceCategoryCode = grievanceCategoryCode;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<ComplaintsList> getComplaintsLists() {
		return complaintsLists;
	}

	public void setComplaintsLists(List<ComplaintsList> complaintsLists) {
		this.complaintsLists = complaintsLists;
	}

}