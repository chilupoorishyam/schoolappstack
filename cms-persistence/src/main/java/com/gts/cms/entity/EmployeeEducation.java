package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the t_emp_employee_education database table.
 * 
 */
@Entity
@Table(name="t_emp_employee_education")
@NamedQuery(name="EmployeeEducation.findAll", query="SELECT e FROM EmployeeEducation e")
public class EmployeeEducation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_emp_education_id")
	private Long empEducationId;

	private String address;

	private String board;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="grade_class_secured")
	private String gradeClassSecured;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="major_subjects")
	private String majorSubjects;

	private String medium;

	@Column(name="name_of_institution")
	private String nameOfInstitution;

	private BigDecimal precentage;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	@Column(name="year_of_completion")
	private String yearOfCompletion;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_id")
	private EmployeeDetail empDetail;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_modeofstudy_catdet_id")
	private GeneralDetail modeofstudy;

	public EmployeeEducation() {
	}

	public Long getEmpEducationId() {
		return this.empEducationId;
	}

	public void setEmpEducationId(Long empEducationId) {
		this.empEducationId = empEducationId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBoard() {
		return this.board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getGradeClassSecured() {
		return this.gradeClassSecured;
	}

	public void setGradeClassSecured(String gradeClassSecured) {
		this.gradeClassSecured = gradeClassSecured;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getMajorSubjects() {
		return this.majorSubjects;
	}

	public void setMajorSubjects(String majorSubjects) {
		this.majorSubjects = majorSubjects;
	}

	public String getMedium() {
		return this.medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

	public String getNameOfInstitution() {
		return this.nameOfInstitution;
	}

	public void setNameOfInstitution(String nameOfInstitution) {
		this.nameOfInstitution = nameOfInstitution;
	}

	public BigDecimal getPrecentage() {
		return this.precentage;
	}

	public void setPrecentage(BigDecimal precentage) {
		this.precentage = precentage;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getYearOfCompletion() {
		return this.yearOfCompletion;
	}

	public void setYearOfCompletion(String yearOfCompletion) {
		this.yearOfCompletion = yearOfCompletion;
	}

	public EmployeeDetail getEmpDetail() {
		return this.empDetail;
	}

	public void setEmpDetail(EmployeeDetail empDetail) {
		this.empDetail = empDetail;
	}

	public GeneralDetail getModeofstudy() {
		return this.modeofstudy;
	}

	public void setModeofstudy(GeneralDetail modeofstudy) {
		this.modeofstudy = modeofstudy;
	}

}