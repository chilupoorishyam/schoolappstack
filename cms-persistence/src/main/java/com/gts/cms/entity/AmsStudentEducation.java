package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_ams_student_education database table.
 * 
 */
@Entity
@Table(name="t_ams_student_education")
@NamedQuery(name="AmsStudentEducation.findAll", query="SELECT a FROM AmsStudentEducation a")
public class AmsStudentEducation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_ams_student_education_id")
	private Long amsStudentEducationId;

	@Column(name="city_name")
	private String cityName;

	@Temporal(TemporalType.DATE)
	@Column(name="completed_year")
	private Date completedYear;

	private String country;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String degree;

	private String description;

	@Column(name="field_of_study")
	private String fieldOfStudy;


	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_verifiedby_emp_id")
	private EmployeeDetail verifiedbyEmpId;

	private String grade;

	@Column(name="institute_name")
	private String instituteName;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_course_completed")
	private Boolean isCourseCompleted;

	@Column(name="is_verifiedby_incharge")
	private Boolean isVerifiedbyIncharge;

	private String location;

	private String pincode;

	private String reason;

	@Column(name="show_in_profile")
	private Boolean showInProfile;

	@Temporal(TemporalType.DATE)
	@Column(name="start_year")
	private Date startYear;

	private String state;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	@Column(name="verification_comments")
	private String verificationComments;

	//bi-directional many-to-one association to AmsProfileDetail
	@ManyToOne
	@JoinColumn(name="fk_ams_profile_id")
	private AmsProfileDetail amsProfileDetail;

	public AmsStudentEducation() {
	}

	public Long getAmsStudentEducationId() {
		return this.amsStudentEducationId;
	}

	public void setAmsStudentEducationId(Long amsStudentEducationId) {
		this.amsStudentEducationId = amsStudentEducationId;
	}

	public String getCityName() {
		return this.cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Date getCompletedYear() {
		return this.completedYear;
	}

	public void setCompletedYear(Date completedYear) {
		this.completedYear = completedYear;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDegree() {
		return this.degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFieldOfStudy() {
		return this.fieldOfStudy;
	}

	public void setFieldOfStudy(String fieldOfStudy) {
		this.fieldOfStudy = fieldOfStudy;
	}


	public EmployeeDetail getVerifiedbyEmpId() {
		return verifiedbyEmpId;
	}

	public void setVerifiedbyEmpId(EmployeeDetail verifiedbyEmpId) {
		this.verifiedbyEmpId = verifiedbyEmpId;
	}

	public String getGrade() {
		return this.grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getInstituteName() {
		return this.instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsCourseCompleted() {
		return this.isCourseCompleted;
	}

	public void setIsCourseCompleted(Boolean isCourseCompleted) {
		this.isCourseCompleted = isCourseCompleted;
	}

	public Boolean getIsVerifiedbyIncharge() {
		return this.isVerifiedbyIncharge;
	}

	public void setIsVerifiedbyIncharge(Boolean isVerifiedbyIncharge) {
		this.isVerifiedbyIncharge = isVerifiedbyIncharge;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPincode() {
		return this.pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Boolean getShowInProfile() {
		return this.showInProfile;
	}

	public void setShowInProfile(Boolean showInProfile) {
		this.showInProfile = showInProfile;
	}

	public Date getStartYear() {
		return this.startYear;
	}

	public void setStartYear(Date startYear) {
		this.startYear = startYear;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getVerificationComments() {
		return this.verificationComments;
	}

	public void setVerificationComments(String verificationComments) {
		this.verificationComments = verificationComments;
	}

	public AmsProfileDetail getAmsProfileDetail() {
		return this.amsProfileDetail;
	}

	public void setAmsProfileDetail(AmsProfileDetail amsProfileDetail) {
		this.amsProfileDetail = amsProfileDetail;
	}

}