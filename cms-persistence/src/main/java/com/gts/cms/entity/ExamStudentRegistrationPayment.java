package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_exam_student_registration_payment database
 * table.
 * 
 */
@Entity
@Table(name = "t_exam_student_registration_payment")
@NamedQuery(name = "ExamStudentRegistrationPayment.findAll", query = "SELECT e FROM ExamStudentRegistrationPayment e")
public class ExamStudentRegistrationPayment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_std_reg_payment_id")
	private Long examStdRegPaymentId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_id")
	private ExamMaster examMaster;

	@Column(name = "fk_exam_std_reg_ids")
	private String examStdRegIds;

	@Column(name = "fk_exam_std_reg_sub_ids")
	private String examStdRegSubIds;

	@Column(name = "fk_exam_std_reg_transaction_ids")
	private String examStdRegTransactionIds;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_payment_mode_catdet_id")
	private GeneralDetail paymentModeCat;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_reg_payment_status_catdet_id")
	private GeneralDetail regPaymentStatusCat;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_status_emp_id")
	private EmployeeDetail statusEmp;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_payment_settled")
	private Boolean isPaymentSettled;

	private String reason;

	@Column(name = "receipt_amount")
	private BigDecimal receiptAmount;

	@Column(name = "receipt_no")
	private String receiptNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "settled_date")
	private Date settledDate;

	@Column(name = "status_comments")
	private String statusComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;
	
	// bi-directional many-to-one association to ExamStudentRegistrationSubject
	@OneToMany(mappedBy = "examRegPayment", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ExamStudentRegistrationTransaction> examStdRegTxns;
	

	public ExamStudentRegistrationPayment() {
	}

	public Long getExamStdRegPaymentId() {
		return examStdRegPaymentId;
	}

	public void setExamStdRegPaymentId(Long examStdRegPaymentId) {
		this.examStdRegPaymentId = examStdRegPaymentId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public ExamMaster getExamMaster() {
		return examMaster;
	}

	public void setExamMaster(ExamMaster examMaster) {
		this.examMaster = examMaster;
	}

	public String getExamStdRegIds() {
		return examStdRegIds;
	}

	public void setExamStdRegIds(String examStdRegIds) {
		this.examStdRegIds = examStdRegIds;
	}

	public String getExamStdRegSubIds() {
		return examStdRegSubIds;
	}

	public void setExamStdRegSubIds(String examStdRegSubIds) {
		this.examStdRegSubIds = examStdRegSubIds;
	}

	public String getExamStdRegTransactionIds() {
		return examStdRegTransactionIds;
	}

	public void setExamStdRegTransactionIds(String examStdRegTransactionIds) {
		this.examStdRegTransactionIds = examStdRegTransactionIds;
	}

	public GeneralDetail getPaymentModeCat() {
		return paymentModeCat;
	}

	public void setPaymentModeCat(GeneralDetail paymentModeCat) {
		this.paymentModeCat = paymentModeCat;
	}

	public GeneralDetail getRegPaymentStatusCat() {
		return regPaymentStatusCat;
	}

	public void setRegPaymentStatusCat(GeneralDetail regPaymentStatusCat) {
		this.regPaymentStatusCat = regPaymentStatusCat;
	}

	public EmployeeDetail getStatusEmp() {
		return statusEmp;
	}

	public void setStatusEmp(EmployeeDetail statusEmp) {
		this.statusEmp = statusEmp;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsPaymentSettled() {
		return isPaymentSettled;
	}

	public void setIsPaymentSettled(Boolean isPaymentSettled) {
		this.isPaymentSettled = isPaymentSettled;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getReceiptAmount() {
		return receiptAmount;
	}

	public void setReceiptAmount(BigDecimal receiptAmount) {
		this.receiptAmount = receiptAmount;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public Date getSettledDate() {
		return settledDate;
	}

	public void setSettledDate(Date settledDate) {
		this.settledDate = settledDate;
	}

	public String getStatusComments() {
		return statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<ExamStudentRegistrationTransaction> getExamStdRegTxns() {
		return examStdRegTxns;
	}

	public void setExamStdRegTxns(List<ExamStudentRegistrationTransaction> examStdRegTxns) {
		this.examStdRegTxns = examStdRegTxns;
	}
}