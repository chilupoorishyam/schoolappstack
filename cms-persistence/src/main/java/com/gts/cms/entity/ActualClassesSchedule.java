package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_tt_actual_classes_schedule database table.
 * 
 */
@Entity
@Table(name="t_tt_actual_classes_schedule")
@NamedQuery(name="ActualClassesSchedule.findAll", query="SELECT a FROM ActualClassesSchedule a")
public class ActualClassesSchedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_actual_cls_schedule_id")
	private Long actualClsScheduleId;

	@Temporal(TemporalType.DATE)
	@Column(name="class_date")
	private Date classDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="attendance_taken_date")
	private Date attendanceTakenDate;

	@Column(name="from_time")
	private Time fromTime;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Column(name="to_time")
	private Time toTime;
	
	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_attendance_status_id")
	private GeneralDetail attendanceStatus;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="status_date")
	private Date statusDate;
	
	@Column(name="status_comments")
	private String statusComments;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_att_status_emp_id")
	private EmployeeDetail attStatusEmp;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;
	
	@Column(name ="notes_path")
	private String notesPath;
	
	@Column(name ="video_path")
	private String videoPath;
	
	//bi-directional many-to-one association to StaffCourseyrSubject
	/*TO BE REMOVED
	@ManyToOne
	@JoinColumn(name="fk_subcourseyr_staff_id")
	private StaffCourseyrSubject staffCourseyrSubject;*/

	//bi-directional many-to-one association to SubjectCourseyear
	/*TO BE REMOVED
	@ManyToOne
	@JoinColumn(name="fk_sub_courseyear_id")
	private SubjectCourseyear subjectCourseyear;*/

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subjecttype_catdet_id")
	private GeneralDetail subjecttype;

	//bi-directional many-to-one association to GroupSection
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_group_section_id")
	private GroupSection groupSection;

	//bi-directional many-to-one association to Room
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_room_id")
	private Room room;

	//bi-directional many-to-one association to Studentbatch
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_stdbatch_id")
	private Studentbatch studentbatch;

	//bi-directional many-to-one association to Schedule
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_timetable_schedule_id")
	private Schedule schedule;

	//bi-directional many-to-one association to SubjectResource
	/*TO BE REMOVED 
	 * @ManyToOne
	@JoinColumn(name="fk_subject_resource_id")
	private SubjectResource subjectResource;*/

	//bi-directional many-to-one association to Lessonstatus
	@OneToMany(mappedBy="actualClassesSchedule",fetch = FetchType.LAZY)
	private List<Lessonstatus> lessonstatuses;

	//bi-directional many-to-one association to StudentAttendance
	@OneToMany(mappedBy="actualClassesSchedule",fetch = FetchType.LAZY)
	private List<StudentAttendance> stdAttendances;

	//bi-directional many-to-one association to StudentAttendance
	@OneToMany(mappedBy="actualClassesSchedule",fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	private List<ActualClassesScheduleWorkflow> actualClassesScheduleWorkflows;

	// Added by Naveen on 1-11-2018
	
	//bi-directional many-to-one association to Employee
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_cls_emp_id")
	private EmployeeDetail classEmployeeDetail;
	
	//bi-directional many-to-one association to Subject
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subject_id")
	private Subject subject;
	
	public ActualClassesSchedule() {
	}

	public Long getActualClsScheduleId() {
		return this.actualClsScheduleId;
	}

	public void setActualClsScheduleId(Long actualClsScheduleId) {
		this.actualClsScheduleId = actualClsScheduleId;
	}

	public Date getClassDate() {
		return this.classDate;
	}

	public void setClassDate(Date classDate) {
		this.classDate = classDate;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Time getFromTime() {
		return this.fromTime;
	}

	public void setFromTime(Time fromTime) {
		this.fromTime = fromTime;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Time getToTime() {
		return this.toTime;
	}

	public void setToTime(Time toTime) {
		this.toTime = toTime;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

/*	public StaffCourseyrSubject getStaffCourseyrSubject() {
		return this.staffCourseyrSubject;
	}

	public void setStaffCourseyrSubject(StaffCourseyrSubject staffCourseyrSubject) {
		this.staffCourseyrSubject = staffCourseyrSubject;
	}

	public SubjectCourseyear getSubjectCourseyear() {
		return this.subjectCourseyear;
	}

	public void setSubjectCourseyear(SubjectCourseyear subjectCourseyear) {
		this.subjectCourseyear = subjectCourseyear;
	}*/

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getSubjecttype() {
		return this.subjecttype;
	}

	public void setSubjecttype(GeneralDetail subjecttype) {
		this.subjecttype = subjecttype;
	}

	public GroupSection getGroupSection() {
		return this.groupSection;
	}

	public void setGroupSection(GroupSection groupSection) {
		this.groupSection = groupSection;
	}

	public Room getRoom() {
		return this.room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public Studentbatch getStudentbatch() {
		return this.studentbatch;
	}

	public void setStudentbatch(Studentbatch studentbatch) {
		this.studentbatch = studentbatch;
	}

	public Schedule getSchedule() {
		return this.schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	/*public SubjectResource getSubjectResource() {
		return this.subjectResource;
	}

	public void setSubjectResource(SubjectResource subjectResource) {
		this.subjectResource = subjectResource;
	}*/

	public List<Lessonstatus> getLessonstatuses() {
		return this.lessonstatuses;
	}

	public void setLessonstatuses(List<Lessonstatus> lessonstatuses) {
		this.lessonstatuses = lessonstatuses;
	}

	public Lessonstatus addLessonstatus(Lessonstatus lessonstatus) {
		getLessonstatuses().add(lessonstatus);
		lessonstatus.setActualClassesSchedule(this);

		return lessonstatus;
	}

	public Lessonstatus removeLessonstatus(Lessonstatus lessonstatus) {
		getLessonstatuses().remove(lessonstatus);
		lessonstatus.setActualClassesSchedule(null);

		return lessonstatus;
	}

	public List<StudentAttendance> getStdAttendances() {
		return this.stdAttendances;
	}

	public void setStdAttendances(List<StudentAttendance> stdAttendances) {
		this.stdAttendances = stdAttendances;
	}

	public StudentAttendance addStdAttendance(StudentAttendance stdAttendance) {
		getStdAttendances().add(stdAttendance);
		stdAttendance.setActualClassesSchedule(this);

		return stdAttendance;
	}

	public StudentAttendance removeStdAttendance(StudentAttendance stdAttendance) {
		getStdAttendances().remove(stdAttendance);
		stdAttendance.setActualClassesSchedule(null);

		return stdAttendance;
	}

	public Date getAttendanceTakenDate() {
		return attendanceTakenDate;
	}

	public void setAttendanceTakenDate(Date attendanceTakenDate) {
		this.attendanceTakenDate = attendanceTakenDate;
	}

	public EmployeeDetail getClassEmployeeDetail() {
		return classEmployeeDetail;
	}

	public void setClassEmployeeDetail(EmployeeDetail classEmployeeDetail) {
		this.classEmployeeDetail = classEmployeeDetail;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public String getNotesPath() {
		return notesPath;
	}

	public void setNotesPath(String notesPath) {
		this.notesPath = notesPath;
	}

	public String getVideoPath() {
		return videoPath;
	}

	public void setVideoPath(String videoPath) {
		this.videoPath = videoPath;
	}

	public Date getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}

	public String getStatusComments() {
		return statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public EmployeeDetail getAttStatusEmp() {
		return attStatusEmp;
	}

	public void setAttStatusEmp(EmployeeDetail attStatusEmp) {
		this.attStatusEmp = attStatusEmp;
	}

	public GeneralDetail getAttendanceStatus() {
		return attendanceStatus;
	}

	public void setAttendanceStatus(GeneralDetail attendanceStatus) {
		this.attendanceStatus = attendanceStatus;
	}

	public List<ActualClassesScheduleWorkflow> getActualClassesScheduleWorkflows() {
		return actualClassesScheduleWorkflows;
	}

	public void setActualClassesScheduleWorkflows(List<ActualClassesScheduleWorkflow> actualClassesScheduleWorkflows) {
		this.actualClassesScheduleWorkflows = actualClassesScheduleWorkflows;
	}
}