package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_tt_staff_proxy database table.
 * 
 */
@Entity
@Table(name = "t_tt_staff_proxy")
@NamedQuery(name = "StaffProxy.findAll", query = "SELECT s FROM StaffProxy s")
public class StaffProxy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_staff_proxy_id")
	private Long staffProxyId;

	@Column(name = "approve_comments")
	private String approveComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_assignedby_emp_id")
	private EmployeeDetail assignedbyEmployeeDetail;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_approved_by_emp_id")
	private EmployeeDetail approvedByEmployeeDetail;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_leave_appliction_id")
	private LeaveApplication leaveAppliction;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_process_status_catdet_id")
	private GeneralDetail processStatusCatdet;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_proxy_emp_id")
	private EmployeeDetail proxyEmp;

	// bi-directional many-to-one association to Room
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_proxy_room_id")
	private Room room;

	// bi-directional many-to-one association to Studentbatch
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_proxy_stdbatch_id")
	private Studentbatch studentbatch;

	// bi-directional many-to-one association to SubjectCourseyear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_proxy_sub_courseyear_id")
	private SubjectCourseyear subjectCourseyear;

	// bi-directional many-to-one association to StaffCourseyrSubject
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_proxy_subcourseyr_staff_id")
	private StaffCourseyrSubject staffCourseyrSubject;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_proxy_subjecttype_catdet_id")
	private GeneralDetail proxySubjecttype;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_subject_id")
	private Subject subject;

	// bi-directional many-to-one association to Schedule
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_timetable_schedule_id")
	private Schedule schedule;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_approved")
	private Boolean isApproved;

	@Temporal(TemporalType.DATE)
	@Column(name = "proxy_date")
	private Date proxyDate;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	public StaffProxy() {
	}

	public Long getStaffProxyId() {
		return staffProxyId;
	}

	public String getApproveComments() {
		return approveComments;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public EmployeeDetail getAssignedbyEmployeeDetail() {
		return assignedbyEmployeeDetail;
	}

	public EmployeeDetail getApprovedByEmployeeDetail() {
		return approvedByEmployeeDetail;
	}

	public LeaveApplication getLeaveAppliction() {
		return leaveAppliction;
	}

	public GeneralDetail getProcessStatusCatdet() {
		return processStatusCatdet;
	}

	public School getSchool() {
		return school;
	}

	public EmployeeDetail getProxyEmp() {
		return proxyEmp;
	}

	public Room getRoom() {
		return room;
	}

	public Studentbatch getStudentbatch() {
		return studentbatch;
	}

	public SubjectCourseyear getSubjectCourseyear() {
		return subjectCourseyear;
	}

	public StaffCourseyrSubject getStaffCourseyrSubject() {
		return staffCourseyrSubject;
	}

	public GeneralDetail getProxySubjecttype() {
		return proxySubjecttype;
	}

	public Subject getSubject() {
		return subject;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public Boolean getIsApproved() {
		return isApproved;
	}

	public Date getProxyDate() {
		return proxyDate;
	}

	public String getReason() {
		return reason;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setStaffProxyId(Long staffProxyId) {
		this.staffProxyId = staffProxyId;
	}

	public void setApproveComments(String approveComments) {
		this.approveComments = approveComments;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public void setAssignedbyEmployeeDetail(EmployeeDetail assignedbyEmployeeDetail) {
		this.assignedbyEmployeeDetail = assignedbyEmployeeDetail;
	}

	public void setApprovedByEmployeeDetail(EmployeeDetail approvedByEmployeeDetail) {
		this.approvedByEmployeeDetail = approvedByEmployeeDetail;
	}

	public void setLeaveAppliction(LeaveApplication leaveAppliction) {
		this.leaveAppliction = leaveAppliction;
	}

	public void setProcessStatusCatdet(GeneralDetail processStatusCatdet) {
		this.processStatusCatdet = processStatusCatdet;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public void setProxyEmp(EmployeeDetail proxyEmp) {
		this.proxyEmp = proxyEmp;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public void setStudentbatch(Studentbatch studentbatch) {
		this.studentbatch = studentbatch;
	}

	public void setSubjectCourseyear(SubjectCourseyear subjectCourseyear) {
		this.subjectCourseyear = subjectCourseyear;
	}

	public void setStaffCourseyrSubject(StaffCourseyrSubject staffCourseyrSubject) {
		this.staffCourseyrSubject = staffCourseyrSubject;
	}

	public void setProxySubjecttype(GeneralDetail proxySubjecttype) {
		this.proxySubjecttype = proxySubjecttype;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public void setIsApproved(Boolean isApproved) {
		this.isApproved = isApproved;
	}

	public void setProxyDate(Date proxyDate) {
		this.proxyDate = proxyDate;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

}