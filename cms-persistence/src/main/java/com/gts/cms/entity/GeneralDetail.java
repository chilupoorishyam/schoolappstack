package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_general_details database table.
 * 
 */
@Entity
@Table(name="t_m_general_details")
@NamedQuery(name="GeneralDetail.findAll", query="SELECT g FROM GeneralDetail g")
public class GeneralDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_gd_id")
	private Long generalDetailId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="gd_code")
	private String generalDetailCode;

	@Column(name="gd_description")
	private String generalDetailDescription;

	@Column(name="gd_display_name")
	private String generalDetailDisplayName;

	@Column(name="gd_sort_order")
	private Integer generalDetailSortOrder;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_editable")
	private Boolean isEditable;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to BatchwiseStudent
	@OneToMany(mappedBy="subjectType",fetch = FetchType.LAZY)
	private List<BatchwiseStudent> batchwiseStudents;

	//bi-directional many-to-one association to GroupyrRegulationDetail
	@OneToMany(mappedBy="subjecttype",fetch = FetchType.LAZY)
	private List<GroupyrRegulationDetail> groupyrRegulationDetails;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="maritalStatus",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails1;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="employeeStatus",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails2;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="employeeState",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails3;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="employeeType",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails4;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="employeeCategory",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails5;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="employeeWorkCategory",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails6;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="teachingFor",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails7;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="appointment",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails8;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="bloodGroup",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails9;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="paymode",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails10;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="resident",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails11;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="accommodation",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails12;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="title",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails13;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="gender",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails14;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="religion",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails15;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="nationality",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails16;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="employeeGrade",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails17;

	//bi-directional many-to-one association to EmployeeDocumentCollection
	@OneToMany(mappedBy="doctype",fetch = FetchType.LAZY)
	private List<EmployeeDocumentCollection> empDocumentCollections;

	//bi-directional many-to-one association to EmployeeEducation
	@OneToMany(mappedBy="modeofstudy",fetch = FetchType.LAZY)
	private List<EmployeeEducation> empEmployeeEducations;

	//bi-directional many-to-one association to FeeInstantPayment
	@OneToMany(mappedBy="payerType",fetch = FetchType.LAZY)
	private List<FeeInstantPayment> feeInstantPayments;

	//bi-directional many-to-one association to FeeReceipt
	@OneToMany(mappedBy="payerType",fetch = FetchType.LAZY)
	private List<FeeReceipt> feeReceipts1;

	//bi-directional many-to-one association to FeeReceipt
	@OneToMany(mappedBy="paymentType",fetch = FetchType.LAZY)
	private List<FeeReceipt> feeReceipts2;

	//bi-directional many-to-one association to FeeReceipt
	@OneToMany(mappedBy="paymentMode",fetch = FetchType.LAZY)
	private List<FeeReceipt> feeReceipts3;

	//bi-directional many-to-one association to FeeStructureCourseyr
	@OneToMany(mappedBy="quota",fetch = FetchType.LAZY)
	private List<FeeStructureCourseyr> feeStructureCourseyrs;

	//bi-directional many-to-one association to FeeStudentData
	@OneToMany(mappedBy="paymentStatus",fetch = FetchType.LAZY)
	private List<FeeStudentData> feeStudentData;

	//bi-directional many-to-one association to FeeStudentDataDetail
	@OneToMany(mappedBy="feedataType",fetch = FetchType.LAZY)
	private List<FeeStudentDataDetail> feeStudentDataDetails;

	//bi-directional many-to-one association to FeeTransactionMaster
	@OneToMany(mappedBy="paymentMode",fetch = FetchType.LAZY)
	private List<FeeTransactionMaster> feeTransactionMasters;

	//bi-directional many-to-one association to FinAccountType
	/*@OneToMany(mappedBy="accountType")
	private List<FinAccountType> finAccountTypes;*/

	//bi-directional many-to-one association to FeedbackQuestion
	@OneToMany(mappedBy="fbinputtype",fetch = FetchType.LAZY)
	private List<FeedbackQuestion> feedbackQuestions;

	//bi-directional many-to-one association to FeedbackSection
	@OneToMany(mappedBy="fbinputtype",fetch = FetchType.LAZY)
	private List<FeedbackSection> feedbackSections;

	//bi-directional many-to-one association to LeaveType
	@OneToMany(mappedBy="leavetypeDuration",fetch = FetchType.LAZY)
	private List<LeaveType> hrLeaveTypes;

	//bi-directional many-to-one association to SurveyForm
	@OneToMany(mappedBy="fbfrom",fetch = FetchType.LAZY)
	private List<SurveyForm> surveyForms1;

	//bi-directional many-to-one association to SurveyForm
	@OneToMany(mappedBy="fbfor",fetch = FetchType.LAZY)
	private List<SurveyForm> surveyForms2;

	//bi-directional many-to-one association to BookDetail
	@OneToMany(mappedBy="bookcondition",fetch = FetchType.LAZY)
	private List<BookDetail> bookDetails;

	//bi-directional many-to-one association to BookIssuedetail
	@OneToMany(mappedBy="returnBookcondition",fetch = FetchType.LAZY)
	private List<BookIssuedetail> bookIssuedetails;

	//bi-directional many-to-one association to BookIssuedetailsHistory
	@OneToMany(mappedBy="returnBookcondition",fetch = FetchType.LAZY)
	private List<BookIssuedetailsHistory> bookIssuedetailsHistories;

	//bi-directional many-to-one association to School
	@OneToMany(mappedBy="affiliatedto",fetch = FetchType.LAZY)
	private List<School> schools1;

	//bi-directional many-to-one association to School
	@OneToMany(mappedBy="schooltype",fetch = FetchType.LAZY)
	private List<School> schools2;

	//bi-directional many-to-one association to DocumentRepository
	@OneToMany(mappedBy="doctype",fetch = FetchType.LAZY)
	private List<DocumentRepository> documentRepositories1;

	//bi-directional many-to-one association to DocumentRepository
	@OneToMany(mappedBy="docform",fetch = FetchType.LAZY)
	private List<DocumentRepository> documentRepositories2;

	//bi-directional many-to-one association to EventAudience
	@OneToMany(mappedBy="audienceType",fetch = FetchType.LAZY)
	private List<EventAudience> eventAudiences;

	//bi-directional many-to-one association to Event
	@OneToMany(mappedBy="eventStatus",fetch = FetchType.LAZY)
	private List<Event> events;

	//bi-directional many-to-one association to GeneralMaster
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="fk_gm_id")
	private GeneralMaster generalMaster;

	//bi-directional many-to-one association to NotificationAudience
	@OneToMany(mappedBy="audienceType",fetch = FetchType.LAZY)
	private List<NotificationAudience> notificationAudiences;

	//bi-directional many-to-one association to Studentbatch
	@OneToMany(mappedBy="subjecttype",fetch = FetchType.LAZY)
	private List<Studentbatch> studentbatches;

	//bi-directional many-to-one association to Subject
	@OneToMany(mappedBy="subjecttype",fetch = FetchType.LAZY)
	private List<Subject> subjects1;

	//bi-directional many-to-one association to Subject
	@OneToMany(mappedBy="subjectCategory",fetch = FetchType.LAZY)
	private List<Subject> subjects2;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="disability",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications1;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="nationality",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications2;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="religion",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications3;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="bloodGroup",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications4;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="language1",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications5;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="language2",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications6;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="language3",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications7;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="language4",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications8;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="language5",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications9;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="quota",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications10;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="studenttype",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications11;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="title",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications12;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="gender",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications13;

	//bi-directional many-to-one association to StudentEducationDetail
	@OneToMany(mappedBy="TMGeneralDetail",fetch = FetchType.LAZY)
	private List<StudentEducationDetail> TStdEducationDetails;

	//bi-directional many-to-one association to StudentEnquiry
	@OneToMany(mappedBy="enquirystatus",fetch = FetchType.LAZY)
	private List<StudentEnquiry> stdEnquiries1;

	//bi-directional many-to-one association to StudentEnquiry
	@OneToMany(mappedBy="modeofenquiry",fetch = FetchType.LAZY)
	private List<StudentEnquiry> stdEnquiries2;

	//bi-directional many-to-one association to StudentEnquiry
	@OneToMany(mappedBy="knowaboutus",fetch = FetchType.LAZY)
	private List<StudentEnquiry> enquiries3;

	//bi-directional many-to-one association to StudentEnquiry
	@OneToMany(mappedBy="gender",fetch = FetchType.LAZY)
	private List<StudentEnquiry> enquiries4;

	//bi-directional many-to-one association to StudentAcademicbatch
	@OneToMany(mappedBy="studentStatus",fetch = FetchType.LAZY)
	private List<StudentAcademicbatch> studentAcademicbatches;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="disability",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails1;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="nationality",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails2;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="religion",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails3;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="bloodgroup",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails4;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="maritalstatus",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails5;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="language1",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails6;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="language2",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails7;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="language3",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails8;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="language4",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails9;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="language5",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails10;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="quota",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails11;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="studentType",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails12;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="gender",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails13;

	//bi-directional many-to-one association to StudentSubject
	@OneToMany(mappedBy="subjectType",fetch = FetchType.LAZY)
	private List<StudentSubject> studentSubjects;

	//bi-directional many-to-one association to DistanceFee
	@OneToMany(mappedBy="feeFrequency",fetch = FetchType.LAZY)
	private List<DistanceFee> distanceFees;

	//bi-directional many-to-one association to Driver
	@OneToMany(mappedBy="gender",fetch = FetchType.LAZY)
	private List<Driver> drivers1;

	//bi-directional many-to-one association to Driver
	@OneToMany(mappedBy="maritalStatus",fetch = FetchType.LAZY)
	private List<Driver> drivers2;

	//bi-directional many-to-one association to Driver
	@OneToMany(mappedBy="bloodgroup",fetch = FetchType.LAZY)
	private List<Driver> drivers3;

	//bi-directional many-to-one association to Helper
	@OneToMany(mappedBy="gender",fetch = FetchType.LAZY)
	private List<Helper> helpers1;

	//bi-directional many-to-one association to Helper
	@OneToMany(mappedBy="maritalStatus",fetch = FetchType.LAZY)
	private List<Helper> helpers2;

	//bi-directional many-to-one association to Helper
	@OneToMany(mappedBy="bloodgroup",fetch = FetchType.LAZY)
	private List<Helper> helpers3;

	//bi-directional many-to-one association to RouteStop
	@OneToMany(mappedBy="feeFrequency",fetch = FetchType.LAZY)
	private List<RouteStop> routeStops;

	//bi-directional many-to-one association to VehicleDetail
	@OneToMany(mappedBy="vehicleType",fetch = FetchType.LAZY)
	private List<VehicleDetail> vehicleDetails;

	//bi-directional many-to-one association to ActualClassesSchedule
	@OneToMany(mappedBy="subjecttype",fetch = FetchType.LAZY)
	private List<ActualClassesSchedule> actualClassesSchedules;

	//bi-directional many-to-one association to Lessonstatus
	@OneToMany(mappedBy="lessonstatus",fetch = FetchType.LAZY)
	private List<Lessonstatus> lessonstatuses;

	//bi-directional many-to-one association to StaffProxy
	@OneToMany(mappedBy="proxySubjecttype",fetch = FetchType.LAZY)
	private List<StaffProxy> staffProxies;

	//bi-directional many-to-one association to SubjectResource
	@OneToMany(mappedBy="subjecttype",fetch = FetchType.LAZY)
	private List<SubjectResource> subjectResources;

	public GeneralDetail() {
	}

	public Long getGeneralDetailId() {
		return this.generalDetailId;
	}

	public void setGeneralDetailId(Long generalDetailId) {
		this.generalDetailId = generalDetailId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getGeneralDetailCode() {
		return this.generalDetailCode;
	}

	public void setGeneralDetailCode(String generalDetailCode) {
		this.generalDetailCode = generalDetailCode;
	}

	public String getGeneralDetailDescription() {
		return this.generalDetailDescription;
	}

	public void setGeneralDetailDescription(String generalDetailDescription) {
		this.generalDetailDescription = generalDetailDescription;
	}

	public String getGeneralDetailDisplayName() {
		return this.generalDetailDisplayName;
	}

	public void setGeneralDetailDisplayName(String generalDetailDisplayName) {
		this.generalDetailDisplayName = generalDetailDisplayName;
	}

	public Integer getGeneralDetailSortOrder() {
		return this.generalDetailSortOrder;
	}

	public void setGeneralDetailSortOrder(Integer generalDetailSortOrder) {
		this.generalDetailSortOrder = generalDetailSortOrder;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsEditable() {
		return this.isEditable;
	}

	public void setIsEditable(Boolean isEditable) {
		this.isEditable = isEditable;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<BatchwiseStudent> getBatchwiseStudents() {
		return this.batchwiseStudents;
	}

	public void setBatchwiseStudents(List<BatchwiseStudent> batchwiseStudents) {
		this.batchwiseStudents = batchwiseStudents;
	}

	public BatchwiseStudent addBatchwiseStudent(BatchwiseStudent batchwiseStudent) {
		getBatchwiseStudents().add(batchwiseStudent);
		batchwiseStudent.setSubjectType(this);

		return batchwiseStudent;
	}

	public BatchwiseStudent removeBatchwiseStudent(BatchwiseStudent batchwiseStudent) {
		getBatchwiseStudents().remove(batchwiseStudent);
		batchwiseStudent.setSubjectType(null);

		return batchwiseStudent;
	}

	public List<GroupyrRegulationDetail> getGroupyrRegulationDetails() {
		return this.groupyrRegulationDetails;
	}

	public void setGroupyrRegulationDetails(List<GroupyrRegulationDetail> groupyrRegulationDetails) {
		this.groupyrRegulationDetails = groupyrRegulationDetails;
	}

	public GroupyrRegulationDetail addGroupyrRegulationDetail(GroupyrRegulationDetail groupyrRegulationDetail) {
		getGroupyrRegulationDetails().add(groupyrRegulationDetail);
		groupyrRegulationDetail.setSubjecttype(this);

		return groupyrRegulationDetail;
	}

	public GroupyrRegulationDetail removeGroupyrRegulationDetail(GroupyrRegulationDetail groupyrRegulationDetail) {
		getGroupyrRegulationDetails().remove(groupyrRegulationDetail);
		groupyrRegulationDetail.setSubjecttype(null);

		return groupyrRegulationDetail;
	}

	public List<EmployeeDetail> getEmpDetails1() {
		return this.empDetails1;
	}

	public void setEmpDetails1(List<EmployeeDetail> empDetails1) {
		this.empDetails1 = empDetails1;
	}

	public EmployeeDetail addEmpDetails1(EmployeeDetail empDetails1) {
		getEmpDetails1().add(empDetails1);
		empDetails1.setMaritalStatus(this);

		return empDetails1;
	}

	public EmployeeDetail removeEmpDetails1(EmployeeDetail empDetails1) {
		getEmpDetails1().remove(empDetails1);
		empDetails1.setMaritalStatus(null);

		return empDetails1;
	}

	public List<EmployeeDetail> getEmpDetails2() {
		return this.empDetails2;
	}

	public void setEmpDetails2(List<EmployeeDetail> empDetails2) {
		this.empDetails2 = empDetails2;
	}

	public EmployeeDetail addEmpDetails2(EmployeeDetail empDetails2) {
		getEmpDetails2().add(empDetails2);
		empDetails2.setEmployeeStatus(this);

		return empDetails2;
	}

	public EmployeeDetail removeEmpDetails2(EmployeeDetail empDetails2) {
		getEmpDetails2().remove(empDetails2);
		empDetails2.setEmployeeStatus(null);

		return empDetails2;
	}

	public List<EmployeeDetail> getEmpDetails3() {
		return this.empDetails3;
	}

	public void setEmpDetails3(List<EmployeeDetail> empDetails3) {
		this.empDetails3 = empDetails3;
	}

	public EmployeeDetail addEmpDetails3(EmployeeDetail empDetails3) {
		getEmpDetails3().add(empDetails3);
		empDetails3.setEmployeeState(this);

		return empDetails3;
	}

	public EmployeeDetail removeEmpDetails3(EmployeeDetail empDetails3) {
		getEmpDetails3().remove(empDetails3);
		empDetails3.setEmployeeState(null);

		return empDetails3;
	}

	public List<EmployeeDetail> getEmpDetails4() {
		return this.empDetails4;
	}

	public void setEmpDetails4(List<EmployeeDetail> empDetails4) {
		this.empDetails4 = empDetails4;
	}

	public EmployeeDetail addEmpDetails4(EmployeeDetail empDetails4) {
		getEmpDetails4().add(empDetails4);
		empDetails4.setEmployeeType(this);

		return empDetails4;
	}

	public EmployeeDetail removeEmpDetails4(EmployeeDetail empDetails4) {
		getEmpDetails4().remove(empDetails4);
		empDetails4.setEmployeeType(null);

		return empDetails4;
	}

	public List<EmployeeDetail> getEmpDetails5() {
		return this.empDetails5;
	}

	public void setEmpDetails5(List<EmployeeDetail> empDetails5) {
		this.empDetails5 = empDetails5;
	}

	public EmployeeDetail addEmpDetails5(EmployeeDetail empDetails5) {
		getEmpDetails5().add(empDetails5);
		empDetails5.setEmployeeCategory(this);

		return empDetails5;
	}

	public EmployeeDetail removeEmpDetails5(EmployeeDetail empDetails5) {
		getEmpDetails5().remove(empDetails5);
		empDetails5.setEmployeeCategory(null);

		return empDetails5;
	}

	public List<EmployeeDetail> getEmpDetails6() {
		return this.empDetails6;
	}

	public void setEmpDetails6(List<EmployeeDetail> empDetails6) {
		this.empDetails6 = empDetails6;
	}

	public EmployeeDetail addEmpDetails6(EmployeeDetail empDetails6) {
		getEmpDetails6().add(empDetails6);
		empDetails6.setEmployeeWorkCategory(this);

		return empDetails6;
	}

	public EmployeeDetail removeEmpDetails6(EmployeeDetail empDetails6) {
		getEmpDetails6().remove(empDetails6);
		empDetails6.setEmployeeWorkCategory(null);

		return empDetails6;
	}

	public List<EmployeeDetail> getEmpDetails7() {
		return this.empDetails7;
	}

	public void setEmpDetails7(List<EmployeeDetail> empDetails7) {
		this.empDetails7 = empDetails7;
	}

	public EmployeeDetail addEmpDetails7(EmployeeDetail empDetails7) {
		getEmpDetails7().add(empDetails7);
		empDetails7.setTeachingFor(this);

		return empDetails7;
	}

	public EmployeeDetail removeEmpDetails7(EmployeeDetail empDetails7) {
		getEmpDetails7().remove(empDetails7);
		empDetails7.setTeachingFor(null);

		return empDetails7;
	}

	public List<EmployeeDetail> getEmpDetails8() {
		return this.empDetails8;
	}

	public void setEmpDetails8(List<EmployeeDetail> empDetails8) {
		this.empDetails8 = empDetails8;
	}

	public EmployeeDetail addEmpDetails8(EmployeeDetail empDetails8) {
		getEmpDetails8().add(empDetails8);
		empDetails8.setAppointment(this);

		return empDetails8;
	}

	public EmployeeDetail removeEmpDetails8(EmployeeDetail empDetails8) {
		getEmpDetails8().remove(empDetails8);
		empDetails8.setAppointment(null);

		return empDetails8;
	}

	public List<EmployeeDetail> getEmpDetails9() {
		return this.empDetails9;
	}

	public void setEmpDetails9(List<EmployeeDetail> empDetails9) {
		this.empDetails9 = empDetails9;
	}

	public EmployeeDetail addEmpDetails9(EmployeeDetail empDetails9) {
		getEmpDetails9().add(empDetails9);
		empDetails9.setBloodGroup(this);

		return empDetails9;
	}

	public EmployeeDetail removeEmpDetails9(EmployeeDetail empDetails9) {
		getEmpDetails9().remove(empDetails9);
		empDetails9.setBloodGroup(null);

		return empDetails9;
	}

	public List<EmployeeDetail> getEmpDetails10() {
		return this.empDetails10;
	}

	public void setEmpDetails10(List<EmployeeDetail> empDetails10) {
		this.empDetails10 = empDetails10;
	}

	public EmployeeDetail addEmpDetails10(EmployeeDetail empDetails10) {
		getEmpDetails10().add(empDetails10);
		empDetails10.setPaymode(this);

		return empDetails10;
	}

	public EmployeeDetail removeEmpDetails10(EmployeeDetail empDetails10) {
		getEmpDetails10().remove(empDetails10);
		empDetails10.setPaymode(null);

		return empDetails10;
	}

	public List<EmployeeDetail> getEmpDetails11() {
		return this.empDetails11;
	}

	public void setEmpDetails11(List<EmployeeDetail> empDetails11) {
		this.empDetails11 = empDetails11;
	}

	public EmployeeDetail addEmpDetails11(EmployeeDetail empDetails11) {
		getEmpDetails11().add(empDetails11);
		empDetails11.setResident(this);

		return empDetails11;
	}

	public EmployeeDetail removeEmpDetails11(EmployeeDetail empDetails11) {
		getEmpDetails11().remove(empDetails11);
		empDetails11.setResident(null);

		return empDetails11;
	}

	public List<EmployeeDetail> getEmpDetails12() {
		return this.empDetails12;
	}

	public void setEmpDetails12(List<EmployeeDetail> empDetails12) {
		this.empDetails12 = empDetails12;
	}

	public EmployeeDetail addEmpDetails12(EmployeeDetail empDetails12) {
		getEmpDetails12().add(empDetails12);
		empDetails12.setAccommodation(this);

		return empDetails12;
	}

	public EmployeeDetail removeEmpDetails12(EmployeeDetail empDetails12) {
		getEmpDetails12().remove(empDetails12);
		empDetails12.setAccommodation(null);

		return empDetails12;
	}

	public List<EmployeeDetail> getEmpDetails13() {
		return this.empDetails13;
	}

	public void setEmpDetails13(List<EmployeeDetail> empDetails13) {
		this.empDetails13 = empDetails13;
	}

	public EmployeeDetail addEmpDetails13(EmployeeDetail empDetails13) {
		getEmpDetails13().add(empDetails13);
		empDetails13.setTitle(this);

		return empDetails13;
	}

	public EmployeeDetail removeEmpDetails13(EmployeeDetail empDetails13) {
		getEmpDetails13().remove(empDetails13);
		empDetails13.setTitle(null);

		return empDetails13;
	}

	public List<EmployeeDetail> getEmpDetails14() {
		return this.empDetails14;
	}

	public void setEmpDetails14(List<EmployeeDetail> empDetails14) {
		this.empDetails14 = empDetails14;
	}

	public EmployeeDetail addEmpDetails14(EmployeeDetail empDetails14) {
		getEmpDetails14().add(empDetails14);
		empDetails14.setGender(this);

		return empDetails14;
	}

	public EmployeeDetail removeEmpDetails14(EmployeeDetail empDetails14) {
		getEmpDetails14().remove(empDetails14);
		empDetails14.setGender(null);

		return empDetails14;
	}

	public List<EmployeeDetail> getEmpDetails15() {
		return this.empDetails15;
	}

	public void setEmpDetails15(List<EmployeeDetail> empDetails15) {
		this.empDetails15 = empDetails15;
	}

	public EmployeeDetail addEmpDetails15(EmployeeDetail empDetails15) {
		getEmpDetails15().add(empDetails15);
		empDetails15.setReligion(this);

		return empDetails15;
	}

	public EmployeeDetail removeEmpDetails15(EmployeeDetail empDetails15) {
		getEmpDetails15().remove(empDetails15);
		empDetails15.setReligion(null);

		return empDetails15;
	}

	public List<EmployeeDetail> getEmpDetails16() {
		return this.empDetails16;
	}

	public void setEmpDetails16(List<EmployeeDetail> empDetails16) {
		this.empDetails16 = empDetails16;
	}

	public EmployeeDetail addEmpDetails16(EmployeeDetail empDetails16) {
		getEmpDetails16().add(empDetails16);
		empDetails16.setNationality(this);

		return empDetails16;
	}

	public EmployeeDetail removeEmpDetails16(EmployeeDetail empDetails16) {
		getEmpDetails16().remove(empDetails16);
		empDetails16.setNationality(null);

		return empDetails16;
	}

	public List<EmployeeDetail> getEmpDetails17() {
		return this.empDetails17;
	}

	public void setEmpDetails17(List<EmployeeDetail> empDetails17) {
		this.empDetails17 = empDetails17;
	}

	public EmployeeDetail addEmpDetails17(EmployeeDetail empDetails17) {
		getEmpDetails17().add(empDetails17);
		empDetails17.setEmployeeGrade(this);

		return empDetails17;
	}

	public EmployeeDetail removeEmpDetails17(EmployeeDetail empDetails17) {
		getEmpDetails17().remove(empDetails17);
		empDetails17.setEmployeeGrade(null);

		return empDetails17;
	}

	public List<EmployeeDocumentCollection> getEmpDocumentCollections() {
		return this.empDocumentCollections;
	}

	public void setEmpDocumentCollections(List<EmployeeDocumentCollection> empDocumentCollections) {
		this.empDocumentCollections = empDocumentCollections;
	}

	public EmployeeDocumentCollection addEmpDocumentCollection(EmployeeDocumentCollection empDocumentCollection) {
		getEmpDocumentCollections().add(empDocumentCollection);
		empDocumentCollection.setDoctype(this);

		return empDocumentCollection;
	}

	public EmployeeDocumentCollection removeEmpDocumentCollection(EmployeeDocumentCollection empDocumentCollection) {
		getEmpDocumentCollections().remove(empDocumentCollection);
		empDocumentCollection.setDoctype(null);

		return empDocumentCollection;
	}

	public List<EmployeeEducation> getEmpEmployeeEducations() {
		return this.empEmployeeEducations;
	}

	public void setEmpEmployeeEducations(List<EmployeeEducation> empEmployeeEducations) {
		this.empEmployeeEducations = empEmployeeEducations;
	}

	public EmployeeEducation addEmpEmployeeEducation(EmployeeEducation empEmployeeEducation) {
		getEmpEmployeeEducations().add(empEmployeeEducation);
		empEmployeeEducation.setModeofstudy(this);

		return empEmployeeEducation;
	}

	public EmployeeEducation removeEmpEmployeeEducation(EmployeeEducation empEmployeeEducation) {
		getEmpEmployeeEducations().remove(empEmployeeEducation);
		empEmployeeEducation.setModeofstudy(null);

		return empEmployeeEducation;
	}

	public List<FeeInstantPayment> getFeeInstantPayments() {
		return this.feeInstantPayments;
	}

	public void setFeeInstantPayments(List<FeeInstantPayment> feeInstantPayments) {
		this.feeInstantPayments = feeInstantPayments;
	}

	public FeeInstantPayment addFeeInstantPayment(FeeInstantPayment feeInstantPayment) {
		getFeeInstantPayments().add(feeInstantPayment);
		feeInstantPayment.setPayerType(this);

		return feeInstantPayment;
	}

	public FeeInstantPayment removeFeeInstantPayment(FeeInstantPayment feeInstantPayment) {
		getFeeInstantPayments().remove(feeInstantPayment);
		feeInstantPayment.setPayerType(null);

		return feeInstantPayment;
	}

	public List<FeeReceipt> getFeeReceipts1() {
		return this.feeReceipts1;
	}

	public void setFeeReceipts1(List<FeeReceipt> feeReceipts1) {
		this.feeReceipts1 = feeReceipts1;
	}

	public FeeReceipt addFeeReceipts1(FeeReceipt feeReceipts1) {
		getFeeReceipts1().add(feeReceipts1);
		feeReceipts1.setPayerType(this);

		return feeReceipts1;
	}

	public FeeReceipt removeFeeReceipts1(FeeReceipt feeReceipts1) {
		getFeeReceipts1().remove(feeReceipts1);
		feeReceipts1.setPayerType(null);

		return feeReceipts1;
	}

	public List<FeeReceipt> getFeeReceipts2() {
		return this.feeReceipts2;
	}

	public void setFeeReceipts2(List<FeeReceipt> feeReceipts2) {
		this.feeReceipts2 = feeReceipts2;
	}

	public FeeReceipt addFeeReceipts2(FeeReceipt feeReceipts2) {
		getFeeReceipts2().add(feeReceipts2);
		feeReceipts2.setPaymentType(this);

		return feeReceipts2;
	}

	public FeeReceipt removeFeeReceipts2(FeeReceipt feeReceipts2) {
		getFeeReceipts2().remove(feeReceipts2);
		feeReceipts2.setPaymentType(null);

		return feeReceipts2;
	}

	public List<FeeReceipt> getFeeReceipts3() {
		return this.feeReceipts3;
	}

	public void setFeeReceipts3(List<FeeReceipt> feeReceipts3) {
		this.feeReceipts3 = feeReceipts3;
	}

	public FeeReceipt addFeeReceipts3(FeeReceipt feeReceipts3) {
		getFeeReceipts3().add(feeReceipts3);
		feeReceipts3.setPaymentMode(this);

		return feeReceipts3;
	}

	public FeeReceipt removeFeeReceipts3(FeeReceipt feeReceipts3) {
		getFeeReceipts3().remove(feeReceipts3);
		feeReceipts3.setPaymentMode(null);

		return feeReceipts3;
	}

	public List<FeeStructureCourseyr> getFeeStructureCourseyrs() {
		return this.feeStructureCourseyrs;
	}

	public void setFeeStructureCourseyrs(List<FeeStructureCourseyr> feeStructureCourseyrs) {
		this.feeStructureCourseyrs = feeStructureCourseyrs;
	}

	public FeeStructureCourseyr addFeeStructureCourseyr(FeeStructureCourseyr feeStructureCourseyr) {
		getFeeStructureCourseyrs().add(feeStructureCourseyr);
		feeStructureCourseyr.setQuota(this);

		return feeStructureCourseyr;
	}

	public FeeStructureCourseyr removeFeeStructureCourseyr(FeeStructureCourseyr feeStructureCourseyr) {
		getFeeStructureCourseyrs().remove(feeStructureCourseyr);
		feeStructureCourseyr.setQuota(null);

		return feeStructureCourseyr;
	}

	public List<FeeStudentData> getFeeStudentData() {
		return this.feeStudentData;
	}

	public void setFeeStudentData(List<FeeStudentData> feeStudentData) {
		this.feeStudentData = feeStudentData;
	}

	public FeeStudentData addFeeStudentData(FeeStudentData feeStudentData) {
		getFeeStudentData().add(feeStudentData);
		feeStudentData.setPaymentStatus(this);

		return feeStudentData;
	}

	public FeeStudentData removeFeeStudentData(FeeStudentData feeStudentData) {
		getFeeStudentData().remove(feeStudentData);
		feeStudentData.setPaymentStatus(null);

		return feeStudentData;
	}

	public List<FeeStudentDataDetail> getFeeStudentDataDetails() {
		return this.feeStudentDataDetails;
	}

	public void setFeeStudentDataDetails(List<FeeStudentDataDetail> feeStudentDataDetails) {
		this.feeStudentDataDetails = feeStudentDataDetails;
	}

	public FeeStudentDataDetail addFeeStudentDataDetail(FeeStudentDataDetail feeStudentDataDetail) {
		getFeeStudentDataDetails().add(feeStudentDataDetail);
		feeStudentDataDetail.setFeedataType(this);

		return feeStudentDataDetail;
	}

	public FeeStudentDataDetail removeFeeStudentDataDetail(FeeStudentDataDetail feeStudentDataDetail) {
		getFeeStudentDataDetails().remove(feeStudentDataDetail);
		feeStudentDataDetail.setFeedataType(null);

		return feeStudentDataDetail;
	}

	public List<FeeTransactionMaster> getFeeTransactionMasters() {
		return this.feeTransactionMasters;
	}

	public void setFeeTransactionMasters(List<FeeTransactionMaster> feeTransactionMasters) {
		this.feeTransactionMasters = feeTransactionMasters;
	}

	public FeeTransactionMaster addFeeTransactionMaster(FeeTransactionMaster feeTransactionMaster) {
		getFeeTransactionMasters().add(feeTransactionMaster);
		feeTransactionMaster.setPaymentMode(this);

		return feeTransactionMaster;
	}

	public FeeTransactionMaster removeFeeTransactionMaster(FeeTransactionMaster feeTransactionMaster) {
		getFeeTransactionMasters().remove(feeTransactionMaster);
		feeTransactionMaster.setPaymentMode(null);

		return feeTransactionMaster;
	}
/*
	public List<FinAccountType> getFinAccountTypes() {
		return this.finAccountTypes;
	}

	public void setFinAccountTypes(List<FinAccountType> finAccountTypes) {
		this.finAccountTypes = finAccountTypes;
	}*/

/*	public FinAccountType addFinAccountType(FinAccountType finAccountType) {
		getFinAccountTypes().add(finAccountType);
		finAccountType.setAccountType(this);

		return finAccountType;
	}

	public FinAccountType removeFinAccountType(FinAccountType finAccountType) {
		getFinAccountTypes().remove(finAccountType);
		finAccountType.setAccountType(null);

		return finAccountType;
	}*/

	public List<FeedbackQuestion> getFeedbackQuestions() {
		return this.feedbackQuestions;
	}

	public void setFeedbackQuestions(List<FeedbackQuestion> feedbackQuestions) {
		this.feedbackQuestions = feedbackQuestions;
	}

	public FeedbackQuestion addFeedbackQuestion(FeedbackQuestion feedbackQuestion) {
		getFeedbackQuestions().add(feedbackQuestion);
		feedbackQuestion.setFbinputtype(this);

		return feedbackQuestion;
	}

	public FeedbackQuestion removeFeedbackQuestion(FeedbackQuestion feedbackQuestion) {
		getFeedbackQuestions().remove(feedbackQuestion);
		feedbackQuestion.setFbinputtype(null);

		return feedbackQuestion;
	}

	public List<FeedbackSection> getFeedbackSections() {
		return this.feedbackSections;
	}

	public void setFeedbackSections(List<FeedbackSection> feedbackSections) {
		this.feedbackSections = feedbackSections;
	}

	public FeedbackSection addFeedbackSection(FeedbackSection feedbackSection) {
		getFeedbackSections().add(feedbackSection);
		feedbackSection.setFbinputtype(this);

		return feedbackSection;
	}

	public FeedbackSection removeFeedbackSection(FeedbackSection feedbackSection) {
		getFeedbackSections().remove(feedbackSection);
		feedbackSection.setFbinputtype(null);

		return feedbackSection;
	}

	public List<LeaveType> getHrLeaveTypes() {
		return this.hrLeaveTypes;
	}

	public void setHrLeaveTypes(List<LeaveType> hrLeaveTypes) {
		this.hrLeaveTypes = hrLeaveTypes;
	}

	public LeaveType addHrLeaveType(LeaveType hrLeaveType) {
		getHrLeaveTypes().add(hrLeaveType);
		hrLeaveType.setLeavetypeDuration(this);

		return hrLeaveType;
	}

	public LeaveType removeHrLeaveType(LeaveType hrLeaveType) {
		getHrLeaveTypes().remove(hrLeaveType);
		hrLeaveType.setLeavetypeDuration(null);

		return hrLeaveType;
	}

	public List<SurveyForm> getSurveyForms1() {
		return this.surveyForms1;
	}

	public void setSurveyForms1(List<SurveyForm> surveyForms1) {
		this.surveyForms1 = surveyForms1;
	}

	public SurveyForm addSurveyForms1(SurveyForm surveyForms1) {
		getSurveyForms1().add(surveyForms1);
		surveyForms1.setFbfrom(this);

		return surveyForms1;
	}

	public SurveyForm removeSurveyForms1(SurveyForm surveyForms1) {
		getSurveyForms1().remove(surveyForms1);
		surveyForms1.setFbfrom(null);

		return surveyForms1;
	}

	public List<SurveyForm> getSurveyForms2() {
		return this.surveyForms2;
	}

	public void setSurveyForms2(List<SurveyForm> surveyForms2) {
		this.surveyForms2 = surveyForms2;
	}

	public SurveyForm addSurveyForms2(SurveyForm surveyForms2) {
		getSurveyForms2().add(surveyForms2);
		surveyForms2.setFbfor(this);

		return surveyForms2;
	}

	public SurveyForm removeSurveyForms2(SurveyForm surveyForms2) {
		getSurveyForms2().remove(surveyForms2);
		surveyForms2.setFbfor(null);

		return surveyForms2;
	}

	public List<BookDetail> getBookDetails() {
		return this.bookDetails;
	}

	public void setBookDetails(List<BookDetail> bookDetails) {
		this.bookDetails = bookDetails;
	}

	public BookDetail addBookDetail(BookDetail bookDetail) {
		getBookDetails().add(bookDetail);
		bookDetail.setBookcondition(this);

		return bookDetail;
	}

	public BookDetail removeBookDetail(BookDetail bookDetail) {
		getBookDetails().remove(bookDetail);
		bookDetail.setBookcondition(null);

		return bookDetail;
	}

	public List<BookIssuedetail> getBookIssuedetails() {
		return this.bookIssuedetails;
	}

	public void setBookIssuedetails(List<BookIssuedetail> bookIssuedetails) {
		this.bookIssuedetails = bookIssuedetails;
	}

	public BookIssuedetail addBookIssuedetail(BookIssuedetail bookIssuedetail) {
		getBookIssuedetails().add(bookIssuedetail);
		bookIssuedetail.setReturnBookcondition(this);

		return bookIssuedetail;
	}

	public BookIssuedetail removeBookIssuedetail(BookIssuedetail bookIssuedetail) {
		getBookIssuedetails().remove(bookIssuedetail);
		bookIssuedetail.setReturnBookcondition(null);

		return bookIssuedetail;
	}

	public List<BookIssuedetailsHistory> getBookIssuedetailsHistories() {
		return this.bookIssuedetailsHistories;
	}

	public void setBookIssuedetailsHistories(List<BookIssuedetailsHistory> bookIssuedetailsHistories) {
		this.bookIssuedetailsHistories = bookIssuedetailsHistories;
	}

	public BookIssuedetailsHistory addBookIssuedetailsHistory(BookIssuedetailsHistory bookIssuedetailsHistory) {
		getBookIssuedetailsHistories().add(bookIssuedetailsHistory);
		bookIssuedetailsHistory.setReturnBookcondition(this);

		return bookIssuedetailsHistory;
	}

	public BookIssuedetailsHistory removeBookIssuedetailsHistory(BookIssuedetailsHistory bookIssuedetailsHistory) {
		getBookIssuedetailsHistories().remove(bookIssuedetailsHistory);
		bookIssuedetailsHistory.setReturnBookcondition(null);

		return bookIssuedetailsHistory;
	}

	public List<School> getSchools1() {
		return this.schools1;
	}

	public void setSchools1(List<School> schools1) {
		this.schools1 = schools1;
	}

	public School addSchools1(School schools1) {
		getSchools1().add(schools1);
		schools1.setAffiliatedto(this);

		return schools1;
	}

	public School removeSchools1(School schools1) {
		getSchools1().remove(schools1);
		schools1.setAffiliatedto(null);

		return schools1;
	}

	public List<School> getSchools2() {
		return this.schools2;
	}

	public void setSchools2(List<School> schools2) {
		this.schools2 = schools2;
	}

	public School addSchools2(School schools2) {
		getSchools2().add(schools2);
		schools2.setSchooltype(this);

		return schools2;
	}

	public School removeSchools2(School schools2) {
		getSchools2().remove(schools2);
		schools2.setSchooltype(null);

		return schools2;
	}

	public List<DocumentRepository> getDocumentRepositories1() {
		return this.documentRepositories1;
	}

	public void setDocumentRepositories1(List<DocumentRepository> documentRepositories1) {
		this.documentRepositories1 = documentRepositories1;
	}

	public DocumentRepository addDocumentRepositories1(DocumentRepository documentRepositories1) {
		getDocumentRepositories1().add(documentRepositories1);
		documentRepositories1.setDoctype(this);

		return documentRepositories1;
	}

	public DocumentRepository removeDocumentRepositories1(DocumentRepository documentRepositories1) {
		getDocumentRepositories1().remove(documentRepositories1);
		documentRepositories1.setDoctype(null);

		return documentRepositories1;
	}

	public List<DocumentRepository> getDocumentRepositories2() {
		return this.documentRepositories2;
	}

	public void setDocumentRepositories2(List<DocumentRepository> documentRepositories2) {
		this.documentRepositories2 = documentRepositories2;
	}

	public DocumentRepository addDocumentRepositories2(DocumentRepository documentRepositories2) {
		getDocumentRepositories2().add(documentRepositories2);
		documentRepositories2.setDocform(this);

		return documentRepositories2;
	}

	public DocumentRepository removeDocumentRepositories2(DocumentRepository documentRepositories2) {
		getDocumentRepositories2().remove(documentRepositories2);
		documentRepositories2.setDocform(null);

		return documentRepositories2;
	}

	public List<EventAudience> getEventAudiences() {
		return this.eventAudiences;
	}

	public void setEventAudiences(List<EventAudience> eventAudiences) {
		this.eventAudiences = eventAudiences;
	}

	public EventAudience addEventAudience(EventAudience eventAudience) {
		getEventAudiences().add(eventAudience);
		eventAudience.setAudienceType(this);

		return eventAudience;
	}

	public EventAudience removeEventAudience(EventAudience eventAudience) {
		getEventAudiences().remove(eventAudience);
		eventAudience.setAudienceType(null);

		return eventAudience;
	}

	public List<Event> getEvents() {
		return this.events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public Event addEvent(Event event) {
		getEvents().add(event);
		event.setEventStatus(this);

		return event;
	}

	public Event removeEvent(Event event) {
		getEvents().remove(event);
		event.setEventStatus(null);

		return event;
	}

	public GeneralMaster getGeneralMaster() {
		return this.generalMaster;
	}

	public void setGeneralMaster(GeneralMaster generalMaster) {
		this.generalMaster = generalMaster;
	}

	public List<NotificationAudience> getNotificationAudiences() {
		return this.notificationAudiences;
	}

	public void setNotificationAudiences(List<NotificationAudience> notificationAudiences) {
		this.notificationAudiences = notificationAudiences;
	}

	public NotificationAudience addNotificationAudience(NotificationAudience notificationAudience) {
		getNotificationAudiences().add(notificationAudience);
		notificationAudience.setAudienceType(this);

		return notificationAudience;
	}

	public NotificationAudience removeNotificationAudience(NotificationAudience notificationAudience) {
		getNotificationAudiences().remove(notificationAudience);
		notificationAudience.setAudienceType(null);

		return notificationAudience;
	}

	public List<Studentbatch> getStudentbatches() {
		return this.studentbatches;
	}

	public void setStudentbatches(List<Studentbatch> studentbatches) {
		this.studentbatches = studentbatches;
	}

	public Studentbatch addStudentbatch(Studentbatch studentbatch) {
		getStudentbatches().add(studentbatch);
		studentbatch.setSubjecttype(this);

		return studentbatch;
	}

	public Studentbatch removeStudentbatch(Studentbatch studentbatch) {
		getStudentbatches().remove(studentbatch);
		studentbatch.setSubjecttype(null);

		return studentbatch;
	}

	public List<Subject> getSubjects1() {
		return this.subjects1;
	}

	public void setSubjects1(List<Subject> subjects1) {
		this.subjects1 = subjects1;
	}

	public Subject addSubjects1(Subject subjects1) {
		getSubjects1().add(subjects1);
		subjects1.setSubjecttype(this);

		return subjects1;
	}

	public Subject removeSubjects1(Subject subjects1) {
		getSubjects1().remove(subjects1);
		subjects1.setSubjecttype(null);

		return subjects1;
	}

	public List<Subject> getSubjects2() {
		return this.subjects2;
	}

	public void setSubjects2(List<Subject> subjects2) {
		this.subjects2 = subjects2;
	}

	public Subject addSubjects2(Subject subjects2) {
		getSubjects2().add(subjects2);
		subjects2.setSubjectCategory(this);

		return subjects2;
	}

	public Subject removeSubjects2(Subject subjects2) {
		getSubjects2().remove(subjects2);
		subjects2.setSubjectCategory(null);

		return subjects2;
	}

	public List<StudentApplication> getStdApplications1() {
		return this.stdApplications1;
	}

	public void setStdApplications1(List<StudentApplication> stdApplications1) {
		this.stdApplications1 = stdApplications1;
	}

	public StudentApplication addStdApplications1(StudentApplication stdApplications1) {
		getStdApplications1().add(stdApplications1);
		stdApplications1.setDisability(this);

		return stdApplications1;
	}

	public StudentApplication removeStdApplications1(StudentApplication stdApplications1) {
		getStdApplications1().remove(stdApplications1);
		stdApplications1.setDisability(null);

		return stdApplications1;
	}

	public List<StudentApplication> getStdApplications2() {
		return this.stdApplications2;
	}

	public void setStdApplications2(List<StudentApplication> stdApplications2) {
		this.stdApplications2 = stdApplications2;
	}

	public StudentApplication addStdApplications2(StudentApplication stdApplications2) {
		getStdApplications2().add(stdApplications2);
		stdApplications2.setNationality(this);

		return stdApplications2;
	}

	public StudentApplication removeStdApplications2(StudentApplication stdApplications2) {
		getStdApplications2().remove(stdApplications2);
		stdApplications2.setNationality(null);

		return stdApplications2;
	}

	public List<StudentApplication> getStdApplications3() {
		return this.stdApplications3;
	}

	public void setStdApplications3(List<StudentApplication> stdApplications3) {
		this.stdApplications3 = stdApplications3;
	}

	public StudentApplication addStdApplications3(StudentApplication stdApplications3) {
		getStdApplications3().add(stdApplications3);
		stdApplications3.setReligion(this);

		return stdApplications3;
	}

	public StudentApplication removeStdApplications3(StudentApplication stdApplications3) {
		getStdApplications3().remove(stdApplications3);
		stdApplications3.setReligion(null);

		return stdApplications3;
	}

	public List<StudentApplication> getStdApplications4() {
		return this.stdApplications4;
	}

	public void setStdApplications4(List<StudentApplication> stdApplications4) {
		this.stdApplications4 = stdApplications4;
	}

	public StudentApplication addStdApplications4(StudentApplication stdApplications4) {
		getStdApplications4().add(stdApplications4);
		stdApplications4.setBloodGroup(this);

		return stdApplications4;
	}

	public StudentApplication removeStdApplications4(StudentApplication stdApplications4) {
		getStdApplications4().remove(stdApplications4);
		stdApplications4.setBloodGroup(null);

		return stdApplications4;
	}

	public List<StudentApplication> getStdApplications5() {
		return this.stdApplications5;
	}

	public void setStdApplications5(List<StudentApplication> stdApplications5) {
		this.stdApplications5 = stdApplications5;
	}

	public StudentApplication addStdApplications5(StudentApplication stdApplications5) {
		getStdApplications5().add(stdApplications5);
		stdApplications5.setLanguage1(this);

		return stdApplications5;
	}

	public StudentApplication removeStdApplications5(StudentApplication stdApplications5) {
		getStdApplications5().remove(stdApplications5);
		stdApplications5.setLanguage1(null);

		return stdApplications5;
	}

	public List<StudentApplication> getStdApplications6() {
		return this.stdApplications6;
	}

	public void setStdApplications6(List<StudentApplication> stdApplications6) {
		this.stdApplications6 = stdApplications6;
	}

	public StudentApplication addStdApplications6(StudentApplication stdApplications6) {
		getStdApplications6().add(stdApplications6);
		stdApplications6.setLanguage2(this);

		return stdApplications6;
	}

	public StudentApplication removeStdApplications6(StudentApplication stdApplications6) {
		getStdApplications6().remove(stdApplications6);
		stdApplications6.setLanguage2(null);

		return stdApplications6;
	}

	public List<StudentApplication> getStdApplications7() {
		return this.stdApplications7;
	}

	public void setStdApplications7(List<StudentApplication> stdApplications7) {
		this.stdApplications7 = stdApplications7;
	}

	public StudentApplication addStdApplications7(StudentApplication stdApplications7) {
		getStdApplications7().add(stdApplications7);
		stdApplications7.setLanguage3(this);

		return stdApplications7;
	}

	public StudentApplication removeStdApplications7(StudentApplication stdApplications7) {
		getStdApplications7().remove(stdApplications7);
		stdApplications7.setLanguage3(null);

		return stdApplications7;
	}

	public List<StudentApplication> getStdApplications8() {
		return this.stdApplications8;
	}

	public void setStdApplications8(List<StudentApplication> stdApplications8) {
		this.stdApplications8 = stdApplications8;
	}

	public StudentApplication addStdApplications8(StudentApplication stdApplications8) {
		getStdApplications8().add(stdApplications8);
		stdApplications8.setLanguage4(this);

		return stdApplications8;
	}

	public StudentApplication removeStdApplications8(StudentApplication stdApplications8) {
		getStdApplications8().remove(stdApplications8);
		stdApplications8.setLanguage4(null);

		return stdApplications8;
	}

	public List<StudentApplication> getStdApplications9() {
		return this.stdApplications9;
	}

	public void setStdApplications9(List<StudentApplication> stdApplications9) {
		this.stdApplications9 = stdApplications9;
	}

	public StudentApplication addStdApplications9(StudentApplication stdApplications9) {
		getStdApplications9().add(stdApplications9);
		stdApplications9.setLanguage5(this);

		return stdApplications9;
	}

	public StudentApplication removeStdApplications9(StudentApplication stdApplications9) {
		getStdApplications9().remove(stdApplications9);
		stdApplications9.setLanguage5(null);

		return stdApplications9;
	}

	public List<StudentApplication> getStdApplications10() {
		return this.stdApplications10;
	}

	public void setStdApplications10(List<StudentApplication> stdApplications10) {
		this.stdApplications10 = stdApplications10;
	}

	public StudentApplication addStdApplications10(StudentApplication stdApplications10) {
		getStdApplications10().add(stdApplications10);
		stdApplications10.setQuota(this);

		return stdApplications10;
	}

	public StudentApplication removeStdApplications10(StudentApplication stdApplications10) {
		getStdApplications10().remove(stdApplications10);
		stdApplications10.setQuota(null);

		return stdApplications10;
	}

	public List<StudentApplication> getStdApplications11() {
		return this.stdApplications11;
	}

	public void setStdApplications11(List<StudentApplication> stdApplications11) {
		this.stdApplications11 = stdApplications11;
	}

	public StudentApplication addStdApplications11(StudentApplication stdApplications11) {
		getStdApplications11().add(stdApplications11);
		stdApplications11.setStudenttype(this);

		return stdApplications11;
	}

	public StudentApplication removeStdApplications11(StudentApplication stdApplications11) {
		getStdApplications11().remove(stdApplications11);
		stdApplications11.setStudenttype(null);

		return stdApplications11;
	}

	public List<StudentApplication> getStdApplications12() {
		return this.stdApplications12;
	}

	public void setStdApplications12(List<StudentApplication> stdApplications12) {
		this.stdApplications12 = stdApplications12;
	}

	public StudentApplication addStdApplications12(StudentApplication stdApplications12) {
		getStdApplications12().add(stdApplications12);
		stdApplications12.setTitle(this);

		return stdApplications12;
	}

	public StudentApplication removeStdApplications12(StudentApplication stdApplications12) {
		getStdApplications12().remove(stdApplications12);
		stdApplications12.setTitle(null);

		return stdApplications12;
	}

	public List<StudentApplication> getStdApplications13() {
		return this.stdApplications13;
	}

	public void setStdApplications13(List<StudentApplication> stdApplications13) {
		this.stdApplications13 = stdApplications13;
	}

	public StudentApplication addStdApplications13(StudentApplication stdApplications13) {
		getStdApplications13().add(stdApplications13);
		stdApplications13.setGender(this);

		return stdApplications13;
	}

	public StudentApplication removeStdApplications13(StudentApplication stdApplications13) {
		getStdApplications13().remove(stdApplications13);
		stdApplications13.setGender(null);

		return stdApplications13;
	}

	public List<StudentEducationDetail> getTStdEducationDetails() {
		return this.TStdEducationDetails;
	}

	public void setTStdEducationDetails(List<StudentEducationDetail> TStdEducationDetails) {
		this.TStdEducationDetails = TStdEducationDetails;
	}

	public StudentEducationDetail addTStdEducationDetail(StudentEducationDetail TStdEducationDetail) {
		getTStdEducationDetails().add(TStdEducationDetail);
		TStdEducationDetail.setTMGeneralDetail(this);

		return TStdEducationDetail;
	}

	public StudentEducationDetail removeTStdEducationDetail(StudentEducationDetail TStdEducationDetail) {
		getTStdEducationDetails().remove(TStdEducationDetail);
		TStdEducationDetail.setTMGeneralDetail(null);

		return TStdEducationDetail;
	}

	public List<StudentEnquiry> getStdEnquiries1() {
		return this.stdEnquiries1;
	}

	public void setStdEnquiries1(List<StudentEnquiry> stdEnquiries1) {
		this.stdEnquiries1 = stdEnquiries1;
	}

	public StudentEnquiry addStdEnquiries1(StudentEnquiry stdEnquiries1) {
		getStdEnquiries1().add(stdEnquiries1);
		stdEnquiries1.setEnquirystatus(this);

		return stdEnquiries1;
	}

	public StudentEnquiry removeStdEnquiries1(StudentEnquiry stdEnquiries1) {
		getStdEnquiries1().remove(stdEnquiries1);
		stdEnquiries1.setEnquirystatus(null);

		return stdEnquiries1;
	}

	public List<StudentEnquiry> getStdEnquiries2() {
		return this.stdEnquiries2;
	}

	public void setStdEnquiries2(List<StudentEnquiry> stdEnquiries2) {
		this.stdEnquiries2 = stdEnquiries2;
	}

	public StudentEnquiry addStdEnquiries2(StudentEnquiry stdEnquiries2) {
		getStdEnquiries2().add(stdEnquiries2);
		stdEnquiries2.setModeofenquiry(this);

		return stdEnquiries2;
	}

	public StudentEnquiry removeStdEnquiries2(StudentEnquiry stdEnquiries2) {
		getStdEnquiries2().remove(stdEnquiries2);
		stdEnquiries2.setModeofenquiry(null);

		return stdEnquiries2;
	}

	public List<StudentEnquiry> getEnquiries3() {
		return this.enquiries3;
	}

	public void setEnquiries3(List<StudentEnquiry> enquiries3) {
		this.enquiries3 = enquiries3;
	}

	public StudentEnquiry addEnquiries3(StudentEnquiry enquiries3) {
		getEnquiries3().add(enquiries3);
		enquiries3.setKnowaboutus(this);

		return enquiries3;
	}

	public StudentEnquiry removeEnquiries3(StudentEnquiry enquiries3) {
		getEnquiries3().remove(enquiries3);
		enquiries3.setKnowaboutus(null);

		return enquiries3;
	}

	public List<StudentEnquiry> getEnquiries4() {
		return this.enquiries4;
	}

	public void setEnquiries4(List<StudentEnquiry> enquiries4) {
		this.enquiries4 = enquiries4;
	}

	public StudentEnquiry addEnquiries4(StudentEnquiry enquiries4) {
		getEnquiries4().add(enquiries4);
		enquiries4.setGender(this);

		return enquiries4;
	}

	public StudentEnquiry removeEnquiries4(StudentEnquiry enquiries4) {
		getEnquiries4().remove(enquiries4);
		enquiries4.setGender(null);

		return enquiries4;
	}

	public List<StudentAcademicbatch> getStudentAcademicbatches() {
		return this.studentAcademicbatches;
	}

	public void setStudentAcademicbatches(List<StudentAcademicbatch> studentAcademicbatches) {
		this.studentAcademicbatches = studentAcademicbatches;
	}

	public StudentAcademicbatch addStudentAcademicbatch(StudentAcademicbatch studentAcademicbatch) {
		getStudentAcademicbatches().add(studentAcademicbatch);
		studentAcademicbatch.setStudentStatus(this);

		return studentAcademicbatch;
	}

	public StudentAcademicbatch removeStudentAcademicbatch(StudentAcademicbatch studentAcademicbatch) {
		getStudentAcademicbatches().remove(studentAcademicbatch);
		studentAcademicbatch.setStudentStatus(null);

		return studentAcademicbatch;
	}

	public List<StudentDetail> getStdStudentDetails1() {
		return this.stdStudentDetails1;
	}

	public void setStdStudentDetails1(List<StudentDetail> stdStudentDetails1) {
		this.stdStudentDetails1 = stdStudentDetails1;
	}

	public StudentDetail addStdStudentDetails1(StudentDetail stdStudentDetails1) {
		getStdStudentDetails1().add(stdStudentDetails1);
		stdStudentDetails1.setDisability(this);

		return stdStudentDetails1;
	}

	public StudentDetail removeStdStudentDetails1(StudentDetail stdStudentDetails1) {
		getStdStudentDetails1().remove(stdStudentDetails1);
		stdStudentDetails1.setDisability(null);

		return stdStudentDetails1;
	}

	public List<StudentDetail> getStdStudentDetails2() {
		return this.stdStudentDetails2;
	}

	public void setStdStudentDetails2(List<StudentDetail> stdStudentDetails2) {
		this.stdStudentDetails2 = stdStudentDetails2;
	}

	public StudentDetail addStdStudentDetails2(StudentDetail stdStudentDetails2) {
		getStdStudentDetails2().add(stdStudentDetails2);
		stdStudentDetails2.setNationality(this);

		return stdStudentDetails2;
	}

	public StudentDetail removeStdStudentDetails2(StudentDetail stdStudentDetails2) {
		getStdStudentDetails2().remove(stdStudentDetails2);
		stdStudentDetails2.setNationality(null);

		return stdStudentDetails2;
	}

	public List<StudentDetail> getStdStudentDetails3() {
		return this.stdStudentDetails3;
	}

	public void setStdStudentDetails3(List<StudentDetail> stdStudentDetails3) {
		this.stdStudentDetails3 = stdStudentDetails3;
	}

	public StudentDetail addStdStudentDetails3(StudentDetail stdStudentDetails3) {
		getStdStudentDetails3().add(stdStudentDetails3);
		stdStudentDetails3.setReligion(this);

		return stdStudentDetails3;
	}

	public StudentDetail removeStdStudentDetails3(StudentDetail stdStudentDetails3) {
		getStdStudentDetails3().remove(stdStudentDetails3);
		stdStudentDetails3.setReligion(null);

		return stdStudentDetails3;
	}

	public List<StudentDetail> getStdStudentDetails4() {
		return this.stdStudentDetails4;
	}

	public void setStdStudentDetails4(List<StudentDetail> stdStudentDetails4) {
		this.stdStudentDetails4 = stdStudentDetails4;
	}

	public StudentDetail addStdStudentDetails4(StudentDetail stdStudentDetails4) {
		getStdStudentDetails4().add(stdStudentDetails4);
		stdStudentDetails4.setBloodgroup(this);

		return stdStudentDetails4;
	}

	public StudentDetail removeStdStudentDetails4(StudentDetail stdStudentDetails4) {
		getStdStudentDetails4().remove(stdStudentDetails4);
		stdStudentDetails4.setBloodgroup(null);

		return stdStudentDetails4;
	}

	public List<StudentDetail> getStdStudentDetails5() {
		return this.stdStudentDetails5;
	}

	public void setStdStudentDetails5(List<StudentDetail> stdStudentDetails5) {
		this.stdStudentDetails5 = stdStudentDetails5;
	}

	public StudentDetail addStdStudentDetails5(StudentDetail stdStudentDetails5) {
		getStdStudentDetails5().add(stdStudentDetails5);
		stdStudentDetails5.setMaritalstatus(this);

		return stdStudentDetails5;
	}

	public StudentDetail removeStdStudentDetails5(StudentDetail stdStudentDetails5) {
		getStdStudentDetails5().remove(stdStudentDetails5);
		stdStudentDetails5.setMaritalstatus(null);

		return stdStudentDetails5;
	}

	public List<StudentDetail> getStdStudentDetails6() {
		return this.stdStudentDetails6;
	}

	public void setStdStudentDetails6(List<StudentDetail> stdStudentDetails6) {
		this.stdStudentDetails6 = stdStudentDetails6;
	}

	public StudentDetail addStdStudentDetails6(StudentDetail stdStudentDetails6) {
		getStdStudentDetails6().add(stdStudentDetails6);
		stdStudentDetails6.setLanguage1(this);

		return stdStudentDetails6;
	}

	public StudentDetail removeStdStudentDetails6(StudentDetail stdStudentDetails6) {
		getStdStudentDetails6().remove(stdStudentDetails6);
		stdStudentDetails6.setLanguage1(null);

		return stdStudentDetails6;
	}

	public List<StudentDetail> getStdStudentDetails7() {
		return this.stdStudentDetails7;
	}

	public void setStdStudentDetails7(List<StudentDetail> stdStudentDetails7) {
		this.stdStudentDetails7 = stdStudentDetails7;
	}

	public StudentDetail addStdStudentDetails7(StudentDetail stdStudentDetails7) {
		getStdStudentDetails7().add(stdStudentDetails7);
		stdStudentDetails7.setLanguage2(this);

		return stdStudentDetails7;
	}

	public StudentDetail removeStdStudentDetails7(StudentDetail stdStudentDetails7) {
		getStdStudentDetails7().remove(stdStudentDetails7);
		stdStudentDetails7.setLanguage2(null);

		return stdStudentDetails7;
	}

	public List<StudentDetail> getStdStudentDetails8() {
		return this.stdStudentDetails8;
	}

	public void setStdStudentDetails8(List<StudentDetail> stdStudentDetails8) {
		this.stdStudentDetails8 = stdStudentDetails8;
	}

	public StudentDetail addStdStudentDetails8(StudentDetail stdStudentDetails8) {
		getStdStudentDetails8().add(stdStudentDetails8);
		stdStudentDetails8.setLanguage3(this);

		return stdStudentDetails8;
	}

	public StudentDetail removeStdStudentDetails8(StudentDetail stdStudentDetails8) {
		getStdStudentDetails8().remove(stdStudentDetails8);
		stdStudentDetails8.setLanguage3(null);

		return stdStudentDetails8;
	}

	public List<StudentDetail> getStdStudentDetails9() {
		return this.stdStudentDetails9;
	}

	public void setStdStudentDetails9(List<StudentDetail> stdStudentDetails9) {
		this.stdStudentDetails9 = stdStudentDetails9;
	}

	public StudentDetail addStdStudentDetails9(StudentDetail stdStudentDetails9) {
		getStdStudentDetails9().add(stdStudentDetails9);
		stdStudentDetails9.setLanguage4(this);

		return stdStudentDetails9;
	}

	public StudentDetail removeStdStudentDetails9(StudentDetail stdStudentDetails9) {
		getStdStudentDetails9().remove(stdStudentDetails9);
		stdStudentDetails9.setLanguage4(null);

		return stdStudentDetails9;
	}

	public List<StudentDetail> getStdStudentDetails10() {
		return this.stdStudentDetails10;
	}

	public void setStdStudentDetails10(List<StudentDetail> stdStudentDetails10) {
		this.stdStudentDetails10 = stdStudentDetails10;
	}

	public StudentDetail addStdStudentDetails10(StudentDetail stdStudentDetails10) {
		getStdStudentDetails10().add(stdStudentDetails10);
		stdStudentDetails10.setLanguage5(this);

		return stdStudentDetails10;
	}

	public StudentDetail removeStdStudentDetails10(StudentDetail stdStudentDetails10) {
		getStdStudentDetails10().remove(stdStudentDetails10);
		stdStudentDetails10.setLanguage5(null);

		return stdStudentDetails10;
	}

	public List<StudentDetail> getStdStudentDetails11() {
		return this.stdStudentDetails11;
	}

	public void setStdStudentDetails11(List<StudentDetail> stdStudentDetails11) {
		this.stdStudentDetails11 = stdStudentDetails11;
	}

	public StudentDetail addStdStudentDetails11(StudentDetail stdStudentDetails11) {
		getStdStudentDetails11().add(stdStudentDetails11);
		stdStudentDetails11.setQuota(this);

		return stdStudentDetails11;
	}

	public StudentDetail removeStdStudentDetails11(StudentDetail stdStudentDetails11) {
		getStdStudentDetails11().remove(stdStudentDetails11);
		stdStudentDetails11.setQuota(null);

		return stdStudentDetails11;
	}

	public List<StudentDetail> getStdStudentDetails12() {
		return this.stdStudentDetails12;
	}

	public void setStdStudentDetails12(List<StudentDetail> stdStudentDetails12) {
		this.stdStudentDetails12 = stdStudentDetails12;
	}

	public StudentDetail addStdStudentDetails12(StudentDetail stdStudentDetails12) {
		getStdStudentDetails12().add(stdStudentDetails12);
		stdStudentDetails12.setStudentType(this);

		return stdStudentDetails12;
	}

	public StudentDetail removeStdStudentDetails12(StudentDetail stdStudentDetails12) {
		getStdStudentDetails12().remove(stdStudentDetails12);
		stdStudentDetails12.setStudentType(null);

		return stdStudentDetails12;
	}

	public List<StudentDetail> getStdStudentDetails13() {
		return this.stdStudentDetails13;
	}

	public void setStdStudentDetails13(List<StudentDetail> stdStudentDetails13) {
		this.stdStudentDetails13 = stdStudentDetails13;
	}

	public StudentDetail addStdStudentDetails13(StudentDetail stdStudentDetails13) {
		getStdStudentDetails13().add(stdStudentDetails13);
		stdStudentDetails13.setGender(this);

		return stdStudentDetails13;
	}

	public StudentDetail removeStdStudentDetails13(StudentDetail stdStudentDetails13) {
		getStdStudentDetails13().remove(stdStudentDetails13);
		stdStudentDetails13.setGender(null);

		return stdStudentDetails13;
	}

	public List<StudentSubject> getStudentSubjects() {
		return this.studentSubjects;
	}

	public void setStudentSubjects(List<StudentSubject> studentSubjects) {
		this.studentSubjects = studentSubjects;
	}

	public StudentSubject addStudentSubject(StudentSubject studentSubject) {
		getStudentSubjects().add(studentSubject);
		studentSubject.setSubjectType(this);

		return studentSubject;
	}

	public StudentSubject removeStudentSubject(StudentSubject studentSubject) {
		getStudentSubjects().remove(studentSubject);
		studentSubject.setSubjectType(null);

		return studentSubject;
	}

	public List<DistanceFee> getDistanceFees() {
		return this.distanceFees;
	}

	public void setDistanceFees(List<DistanceFee> distanceFees) {
		this.distanceFees = distanceFees;
	}

	public DistanceFee addDistanceFee(DistanceFee distanceFee) {
		getDistanceFees().add(distanceFee);
		distanceFee.setFeeFrequency(this);

		return distanceFee;
	}

	public DistanceFee removeDistanceFee(DistanceFee distanceFee) {
		getDistanceFees().remove(distanceFee);
		distanceFee.setFeeFrequency(null);

		return distanceFee;
	}

	public List<Driver> getDrivers1() {
		return this.drivers1;
	}

	public void setDrivers1(List<Driver> drivers1) {
		this.drivers1 = drivers1;
	}

	public Driver addDrivers1(Driver drivers1) {
		getDrivers1().add(drivers1);
		drivers1.setGender(this);

		return drivers1;
	}

	public Driver removeDrivers1(Driver drivers1) {
		getDrivers1().remove(drivers1);
		drivers1.setGender(null);

		return drivers1;
	}

	public List<Driver> getDrivers2() {
		return this.drivers2;
	}

	public void setDrivers2(List<Driver> drivers2) {
		this.drivers2 = drivers2;
	}

	public Driver addDrivers2(Driver drivers2) {
		getDrivers2().add(drivers2);
		drivers2.setMaritalStatus(this);

		return drivers2;
	}

	public Driver removeDrivers2(Driver drivers2) {
		getDrivers2().remove(drivers2);
		drivers2.setMaritalStatus(null);

		return drivers2;
	}

	public List<Driver> getDrivers3() {
		return this.drivers3;
	}

	public void setDrivers3(List<Driver> drivers3) {
		this.drivers3 = drivers3;
	}

	public Driver addDrivers3(Driver drivers3) {
		getDrivers3().add(drivers3);
		drivers3.setBloodgroup(this);

		return drivers3;
	}

	public Driver removeDrivers3(Driver drivers3) {
		getDrivers3().remove(drivers3);
		drivers3.setBloodgroup(null);

		return drivers3;
	}

	public List<Helper> getHelpers1() {
		return this.helpers1;
	}

	public void setHelpers1(List<Helper> helpers1) {
		this.helpers1 = helpers1;
	}

	public Helper addHelpers1(Helper helpers1) {
		getHelpers1().add(helpers1);
		helpers1.setGender(this);

		return helpers1;
	}

	public Helper removeHelpers1(Helper helpers1) {
		getHelpers1().remove(helpers1);
		helpers1.setGender(null);

		return helpers1;
	}

	public List<Helper> getHelpers2() {
		return this.helpers2;
	}

	public void setHelpers2(List<Helper> helpers2) {
		this.helpers2 = helpers2;
	}

	public Helper addHelpers2(Helper helpers2) {
		getHelpers2().add(helpers2);
		helpers2.setMaritalStatus(this);

		return helpers2;
	}

	public Helper removeHelpers2(Helper helpers2) {
		getHelpers2().remove(helpers2);
		helpers2.setMaritalStatus(null);

		return helpers2;
	}

	public List<Helper> getHelpers3() {
		return this.helpers3;
	}

	public void setHelpers3(List<Helper> helpers3) {
		this.helpers3 = helpers3;
	}

	public Helper addHelpers3(Helper helpers3) {
		getHelpers3().add(helpers3);
		helpers3.setBloodgroup(this);

		return helpers3;
	}

	public Helper removeHelpers3(Helper helpers3) {
		getHelpers3().remove(helpers3);
		helpers3.setBloodgroup(null);

		return helpers3;
	}

	public List<RouteStop> getRouteStops() {
		return this.routeStops;
	}

	public void setRouteStops(List<RouteStop> routeStops) {
		this.routeStops = routeStops;
	}

	public RouteStop addRouteStop(RouteStop routeStop) {
		getRouteStops().add(routeStop);
		routeStop.setFeeFrequency(this);

		return routeStop;
	}

	public RouteStop removeRouteStop(RouteStop routeStop) {
		getRouteStops().remove(routeStop);
		routeStop.setFeeFrequency(null);

		return routeStop;
	}

	public List<VehicleDetail> getVehicleDetails() {
		return this.vehicleDetails;
	}

	public void setVehicleDetails(List<VehicleDetail> vehicleDetails) {
		this.vehicleDetails = vehicleDetails;
	}

	public VehicleDetail addVehicleDetail(VehicleDetail vehicleDetail) {
		getVehicleDetails().add(vehicleDetail);
		vehicleDetail.setVehicleType(this);

		return vehicleDetail;
	}

	public VehicleDetail removeVehicleDetail(VehicleDetail vehicleDetail) {
		getVehicleDetails().remove(vehicleDetail);
		vehicleDetail.setVehicleType(null);

		return vehicleDetail;
	}

	public List<ActualClassesSchedule> getActualClassesSchedules() {
		return this.actualClassesSchedules;
	}

	public void setActualClassesSchedules(List<ActualClassesSchedule> actualClassesSchedules) {
		this.actualClassesSchedules = actualClassesSchedules;
	}

	public ActualClassesSchedule addActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		getActualClassesSchedules().add(actualClassesSchedule);
		actualClassesSchedule.setSubjecttype(this);

		return actualClassesSchedule;
	}

	public ActualClassesSchedule removeActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		getActualClassesSchedules().remove(actualClassesSchedule);
		actualClassesSchedule.setSubjecttype(null);

		return actualClassesSchedule;
	}

	public List<Lessonstatus> getLessonstatuses() {
		return this.lessonstatuses;
	}

	public void setLessonstatuses(List<Lessonstatus> lessonstatuses) {
		this.lessonstatuses = lessonstatuses;
	}

	public Lessonstatus addLessonstatus(Lessonstatus lessonstatus) {
		getLessonstatuses().add(lessonstatus);
		lessonstatus.setLessonstatus(this);

		return lessonstatus;
	}

	public Lessonstatus removeLessonstatus(Lessonstatus lessonstatus) {
		getLessonstatuses().remove(lessonstatus);
		lessonstatus.setLessonstatus(null);

		return lessonstatus;
	}

	public List<StaffProxy> getStaffProxies() {
		return this.staffProxies;
	}

	public void setStaffProxies(List<StaffProxy> staffProxies) {
		this.staffProxies = staffProxies;
	}

	public StaffProxy addStaffProxy(StaffProxy staffProxy) {
		getStaffProxies().add(staffProxy);
		staffProxy.setProxySubjecttype(this);

		return staffProxy;
	}

	public StaffProxy removeStaffProxy(StaffProxy staffProxy) {
		getStaffProxies().remove(staffProxy);
		staffProxy.setProxySubjecttype(null);

		return staffProxy;
	}

	public List<SubjectResource> getSubjectResources() {
		return this.subjectResources;
	}

	public void setSubjectResources(List<SubjectResource> subjectResources) {
		this.subjectResources = subjectResources;
	}

	public SubjectResource addSubjectResource(SubjectResource subjectResource) {
		getSubjectResources().add(subjectResource);
		subjectResource.setSubjecttype(this);

		return subjectResource;
	}

	public SubjectResource removeSubjectResource(SubjectResource subjectResource) {
		getSubjectResources().remove(subjectResource);
		subjectResource.setSubjecttype(null);

		return subjectResource;
	}

}