package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_prj_project_master database table.
 */
@Entity
@Table(name = "t_prj_project_master")
public class ProjectMaster implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_prj_master_id")
    private Long prjMasterId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_school_id")
    private School schoolId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_academic_project_id")
    private AcademicProject academicProjectId;

    @Column(name = "project_title")
    private String projectTitle;

    @Column(name = "project_description")
    private String projectDescription;

    @Column(name = "fk_prj_technology_ids")
    private String prjTechnologyIds;

    @Column(name = "fk_prj_keyword_ids")
    private String prjKeywordIds;

    //bi-directional many-to-one association to Organization
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_projecttype_catdet_id")
    private GeneralDetail projecttypeCatdetId;

    //bi-directional many-to-one association to Organization
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_incharge_emp_id")
    private EmployeeDetail inchargeEmployeeDetail;

    //bi-directional many-to-one association to Organization
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_primary_std_id")
    private StudentDetail primaryStudentDetail;

    @Column(name = "outsource_company_name")
    private String outsourceCompanyName;

    @Column(name = "outsource_company_contact_det")
    private String outsourceCompanyContactDet;

    @Column(name = "is_project_approved")
    private Boolean isProjectApproved;

    //bi-directional many-to-one association to Organization
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_approvedby_emp_id")
    private EmployeeDetail approvedEmpDetails;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "approved_date")
    private Date approvedDate;

    @Column(name = "approval_comments")
    private String approvalComments;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "next_review_date")
    private Date nextReviewDate;

    @Column(name = "is_project_submitted")
    private Boolean isProjectSubmitted;

    //bi-directional many-to-one association to Organization
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_submittedby_emp_id")
    private EmployeeDetail submittedbyEmployeeDetail;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "submitted_date")
    private Date submittedDate;

    @Column(name = "submission_comments")
    private String submissionComments;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.DATE)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    public Long getPrjMasterId() {
        return prjMasterId;
    }

    public void setPrjMasterId(Long prjMasterId) {
        this.prjMasterId = prjMasterId;
    }

    public School getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(School schoolId) {
        this.schoolId = schoolId;
    }

    public AcademicProject getAcademicProjectId() {
        return academicProjectId;
    }

    public void setAcademicProjectId(AcademicProject academicProjectId) {
        this.academicProjectId = academicProjectId;
    }

    public String getProjectTitle() {
        return projectTitle;
    }

    public void setProjectTitle(String projectTitle) {
        this.projectTitle = projectTitle;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public String getPrjTechnologyIds() {
        return prjTechnologyIds;
    }

    public void setPrjTechnologyIds(String prjTechnologyIds) {
        this.prjTechnologyIds = prjTechnologyIds;
    }

    public String getPrjKeywordIds() {
        return prjKeywordIds;
    }

    public void setPrjKeywordIds(String prjKeywordIds) {
        this.prjKeywordIds = prjKeywordIds;
    }

    public GeneralDetail getProjecttypeCatdetId() {
        return projecttypeCatdetId;
    }

    public void setProjecttypeCatdetId(GeneralDetail projecttypeCatdetId) {
        this.projecttypeCatdetId = projecttypeCatdetId;
    }

    public EmployeeDetail getInchargeEmployeeDetail() {
        return inchargeEmployeeDetail;
    }

    public void setInchargeEmployeeDetail(EmployeeDetail inchargeEmployeeDetail) {
        this.inchargeEmployeeDetail = inchargeEmployeeDetail;
    }

    public StudentDetail getPrimaryStudentDetail() {
        return primaryStudentDetail;
    }

    public void setPrimaryStudentDetail(StudentDetail primaryStudentDetail) {
        this.primaryStudentDetail = primaryStudentDetail;
    }

    public String getOutsourceCompanyName() {
        return outsourceCompanyName;
    }

    public void setOutsourceCompanyName(String outsourceCompanyName) {
        this.outsourceCompanyName = outsourceCompanyName;
    }

    public String getOutsourceCompanyContactDet() {
        return outsourceCompanyContactDet;
    }

    public void setOutsourceCompanyContactDet(String outsourceCompanyContactDet) {
        this.outsourceCompanyContactDet = outsourceCompanyContactDet;
    }

    public Boolean getProjectApproved() {
        return isProjectApproved;
    }

    public void setProjectApproved(Boolean projectApproved) {
        isProjectApproved = projectApproved;
    }

    public EmployeeDetail getApprovedEmpDetails() {
        return approvedEmpDetails;
    }

    public void setApprovedEmpDetails(EmployeeDetail approvedEmpDetails) {
        this.approvedEmpDetails = approvedEmpDetails;
    }

    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getApprovalComments() {
        return approvalComments;
    }

    public void setApprovalComments(String approvalComments) {
        this.approvalComments = approvalComments;
    }

    public Date getNextReviewDate() {
        return nextReviewDate;
    }

    public void setNextReviewDate(Date nextReviewDate) {
        this.nextReviewDate = nextReviewDate;
    }

    public Boolean getProjectSubmitted() {
        return isProjectSubmitted;
    }

    public void setProjectSubmitted(Boolean projectSubmitted) {
        isProjectSubmitted = projectSubmitted;
    }

    public EmployeeDetail getSubmittedbyEmployeeDetail() {
        return submittedbyEmployeeDetail;
    }

    public void setSubmittedbyEmployeeDetail(EmployeeDetail submittedbyEmployeeDetail) {
        this.submittedbyEmployeeDetail = submittedbyEmployeeDetail;
    }

    public Date getSubmittedDate() {
        return submittedDate;
    }

    public void setSubmittedDate(Date submittedDate) {
        this.submittedDate = submittedDate;
    }

    public String getSubmissionComments() {
        return submissionComments;
    }

    public void setSubmissionComments(String submissionComments) {
        this.submissionComments = submissionComments;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    public Long getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(Long createdUser) {
        this.createdUser = createdUser;
    }

    public Date getUpdatedDt() {
        return updatedDt;
    }

    public void setUpdatedDt(Date updatedDt) {
        this.updatedDt = updatedDt;
    }

    public Long getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(Long updatedUser) {
        this.updatedUser = updatedUser;
    }


}