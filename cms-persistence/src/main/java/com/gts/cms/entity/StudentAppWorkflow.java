package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_std_app_workflow database table.
 * 
 */
@Entity
@Table(name="t_std_app_workflow")
@NamedQuery(name="StudentAppWorkflow.findAll", query="SELECT s FROM StudentAppWorkflow s")
public class StudentAppWorkflow implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_app_wf_id")
	private Long studentAppWorkflowId;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_current_status")
	private Boolean isCurrentStatus;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to WorkflowStage
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_from_app_status_id")
	private WorkflowStage fromAppStatus;

	//bi-directional many-to-one association to WorkflowStage
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_to_app_status_id")
	private WorkflowStage toAppStatus;

	//bi-directional many-to-one association to StudentApplication
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_app_id")
	private StudentApplication studentApplication;

	public StudentAppWorkflow() {
	}

	public Long getStudentAppWorkflowId() {
		return this.studentAppWorkflowId;
	}

	public void setStudentAppWorkflowId(Long studentAppWorkflowId) {
		this.studentAppWorkflowId = studentAppWorkflowId;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsCurrentStatus() {
		return this.isCurrentStatus;
	}

	public void setIsCurrentStatus(Boolean isCurrentStatus) {
		this.isCurrentStatus = isCurrentStatus;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public WorkflowStage getFromAppStatus() {
		return this.fromAppStatus;
	}

	public void setFromAppStatus(WorkflowStage fromAppStatus) {
		this.fromAppStatus = fromAppStatus;
	}

	public WorkflowStage getToAppStatus() {
		return this.toAppStatus;
	}

	public void setToAppStatus(WorkflowStage toAppStatus) {
		this.toAppStatus = toAppStatus;
	}

	public StudentApplication getStudentApplication() {
		return this.studentApplication;
	}

	public void setStudentApplication(StudentApplication studentApplication) {
		this.studentApplication = studentApplication;
	}

}