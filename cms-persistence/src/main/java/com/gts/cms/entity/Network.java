package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_ams_network database table.
 * 
 */
@Entity
@Table(name = "t_ams_network")
@NamedQuery(name = "Network.findAll", query = "SELECT n FROM Network n")
public class Network implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_ams_network_id")
	private Long networkId;

	@Column(name = "call_alums")
	private String callAlums;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "established_year")
	private Integer establishedYear;

	@ManyToOne
	@JoinColumn(name = "fk_school_id")
	private School school;

	@ManyToOne
	@JoinColumn(name = "fk_network_created_profile_id")
	private AmsProfileDetail networkCreatedProfile;

	@ManyToOne
	@JoinColumn(name = "fk_network_type_catdet_id")
	private GeneralDetail networkTypeCat;

	@ManyToOne
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	@Column(name = "is_active")
	private Boolean isActive;

	private String location;

	@Column(name = "network_icon")
	private String networkIcon;

	@Column(name = "network_name")
	private String networkName;

	@Column(name = "network_shortname")
	private String networkShortName;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	private String website;

	// bi-directional many-to-one association to MembershipInvitation
	@OneToMany(mappedBy = "network", cascade = CascadeType.ALL)
	private List<MembershipInvitation> membershipInvitations;

	public Network() {
	}

	public Long getNetworkId() {
		return this.networkId;
	}

	public void setNetworkId(Long networkId) {
		this.networkId = networkId;
	}

	public String getCallAlums() {
		return this.callAlums;
	}

	public void setCallAlums(String callAlums) {
		this.callAlums = callAlums;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Integer getEstablishedYear() {
		return this.establishedYear;
	}

	public void setEstablishedYear(Integer establishedYear) {
		this.establishedYear = establishedYear;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getNetworkIcon() {
		return this.networkIcon;
	}

	public void setNetworkIcon(String networkIcon) {
		this.networkIcon = networkIcon;
	}

	public String getNetworkName() {
		return this.networkName;
	}

	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}

	public String getNetworkShortName() {
		return this.networkShortName;
	}

	public void setNetworkShortName(String networkShortName) {
		this.networkShortName = networkShortName;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public AmsProfileDetail getNetworkCreatedProfile() {
		return networkCreatedProfile;
	}

	public void setNetworkCreatedProfile(AmsProfileDetail networkCreatedProfile) {
		this.networkCreatedProfile = networkCreatedProfile;
	}


	public GeneralDetail getNetworkTypeCat() {
		return networkTypeCat;
	}

	public void setNetworkTypeCat(GeneralDetail networkTypeCat) {
		this.networkTypeCat = networkTypeCat;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public List<MembershipInvitation> getMembershipInvitations() {
		return membershipInvitations;
	}

	public void setMembershipInvitations(List<MembershipInvitation> membershipInvitations) {
		this.membershipInvitations = membershipInvitations;
	}

}