package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_emp_contributions database table.
 */
@Data
@Entity
@Table(name = "t_emp_contributions")
@NamedQuery(name = "EmpContribution.findAll", query = "SELECT c FROM EmpContribution c")
public class EmpContribution implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_emp_contribution_id")
    private Long empContributionId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "title")
    private String title;

    @Column(name = "pagenos")
    private String pagenos;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "published_date")
    private Date publishedDate;

    @Column(name = "issn_isbn_scopus_no")
    private String issnIsbnScopusNo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_date")
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "noof_co_authors")
    private Integer noofCoAuthors;

    @Column(name = "is_main_author")
    private Boolean isMainAuthor;

    @Column(name = "other_info")
    private String otherInfo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "reviwed_on")
    private Date reviwedOn;

    @Column(name = "review_comments")
    private String reviewComments;

    @Column(name = "editor_publiser")
    private String editorPubliser;

    @Column(name = "sponsoring_agency")
    private String sponsoringAgency;

    @Column(name = "expenditure")
    private Integer expenditure;

    @Column(name = "type_of_book")
    private String typeOfBook;

    @Column(name = "authorship")
    private String authorship;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "expected_completiondate")
    private Date expectedCompletiondate;

    @Column(name = "period")
    private String period;

    @Column(name = "grant_mobilized_amount")
    private Integer grantMobilizedAmount;

    @ManyToOne
    @JoinColumn(name = "fk_emp_id")
    private EmployeeDetail employeeDetail;

    @ManyToOne
    @JoinColumn(name = "fk_contributiontype_catdet_id")
    private GeneralDetail contributiontypeCatdetId;

    @ManyToOne
    @JoinColumn(name = "fk_reviwed_emp_id")
    private EmployeeDetail reviwedEmp;

    @ManyToOne
    @JoinColumn(name = "fk_approval_status__catdet_id")
    private GeneralDetail approvalStatusCatdet;

    @ManyToOne
    @JoinColumn(name = "fk_contribution_status_catdet_id")
    private GeneralDetail contributionStatusCatdet;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;
}