package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;

import java.sql.Time;
import java.util.Date;
;

/**
 * The persistent class for the t_m_shifts database table.
 * 
 */
@Entity
@Table(name = "t_m_shifts")
@NamedQuery(name = "Shift.findAll", query = "SELECT s FROM Shift s")
public class Shift implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_shift_id")
	private Long shiftId;

	
	@Column(name = "begintime")
	private Time beginTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	
	@Column(name = "endtime")
	private Time endTime;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_shifttype_catdet_id")
	private GeneralDetail shiftTypeCat;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Column(name = "shift_code")
	private String shiftCode;

	@Column(name = "shift_duration")
	private Integer shiftDuration;

	@Column(name = "shift_name")
	private String shiftName;

	private Long shiftid;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	public Shift() {
	}

	public Long getShiftId() {
		return this.shiftId;
	}

	public void setShiftId(Long shiftId) {
		this.shiftId = shiftId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getShiftCode() {
		return this.shiftCode;
	}

	public void setShiftCode(String shiftCode) {
		this.shiftCode = shiftCode;
	}

	public Integer getShiftDuration() {
		return this.shiftDuration;
	}

	public void setShiftDuration(Integer shiftDuration) {
		this.shiftDuration = shiftDuration;
	}

	public String getShiftName() {
		return this.shiftName;
	}

	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}

	public Long getShiftid() {
		return shiftid;
	}

	public void setShiftid(Long shiftid) {
		this.shiftid = shiftid;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	

	public Time getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Time beginTime) {
		this.beginTime = beginTime;
	}

	public Time getEndTime() {
		return endTime;
	}

	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getShiftTypeCat() {
		return shiftTypeCat;
	}

	public void setShiftTypeCat(GeneralDetail shiftTypeCat) {
		this.shiftTypeCat = shiftTypeCat;
	}

}