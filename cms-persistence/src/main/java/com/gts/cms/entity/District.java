package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_district database table.
 * 
 */
@Entity
@Table(name="t_m_district")
@NamedQuery(name="District.findAll", query="SELECT d FROM District d")
public class District implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_district_id")
	private Long districtId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="district_code")
	private String districtCode;

	@Column(name="district_name")
	private String districtName;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="district",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails1;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="presentDistrict",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails2;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="permanentDistrict",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails3;

	//bi-directional many-to-one association to Campus
	@OneToMany(mappedBy="district",fetch = FetchType.LAZY)
	private List<Campus> campuses;

	//bi-directional many-to-one association to City
	@OneToMany(mappedBy="district",fetch = FetchType.LAZY)
	private List<City> cities;

	//bi-directional many-to-one association to School
	@OneToMany(mappedBy="district",fetch = FetchType.LAZY)
	private List<School> schools;

	//bi-directional many-to-one association to State
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_state_id")
	private State state;

	//bi-directional many-to-one association to Organization
	@OneToMany(mappedBy="district",fetch = FetchType.LAZY)
	private List<Organization> organizations;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="presentDistrict",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications1;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="permanentDistrict",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications2;

	//bi-directional many-to-one association to StudentEnquiry
	@OneToMany(mappedBy="district",fetch = FetchType.LAZY)
	private List<StudentEnquiry> enquiries;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="presentDistrict",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails1;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="permanentDistrict",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails2;

	public District() {
	}

	public Long getDistrictId() {
		return this.districtId;
	}

	public void setDistrictId(Long districtId) {
		this.districtId = districtId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDistrictCode() {
		return this.districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public String getDistrictName() {
		return this.districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<EmployeeDetail> getEmpDetails1() {
		return this.empDetails1;
	}

	public void setEmpDetails1(List<EmployeeDetail> empDetails1) {
		this.empDetails1 = empDetails1;
	}

	public EmployeeDetail addEmpDetails1(EmployeeDetail empDetails1) {
		getEmpDetails1().add(empDetails1);
		empDetails1.setDistrict(this);

		return empDetails1;
	}

	public EmployeeDetail removeEmpDetails1(EmployeeDetail empDetails1) {
		getEmpDetails1().remove(empDetails1);
		empDetails1.setDistrict(null);

		return empDetails1;
	}

	public List<EmployeeDetail> getEmpDetails2() {
		return this.empDetails2;
	}

	public void setEmpDetails2(List<EmployeeDetail> empDetails2) {
		this.empDetails2 = empDetails2;
	}

	public EmployeeDetail addEmpDetails2(EmployeeDetail empDetails2) {
		getEmpDetails2().add(empDetails2);
		empDetails2.setPresentDistrict(this);

		return empDetails2;
	}

	public EmployeeDetail removeEmpDetails2(EmployeeDetail empDetails2) {
		getEmpDetails2().remove(empDetails2);
		empDetails2.setPresentDistrict(null);

		return empDetails2;
	}

	public List<EmployeeDetail> getEmpDetails3() {
		return this.empDetails3;
	}

	public void setEmpDetails3(List<EmployeeDetail> empDetails3) {
		this.empDetails3 = empDetails3;
	}

	public EmployeeDetail addEmpDetails3(EmployeeDetail empDetails3) {
		getEmpDetails3().add(empDetails3);
		empDetails3.setPermanentDistrict(this);

		return empDetails3;
	}

	public EmployeeDetail removeEmpDetails3(EmployeeDetail empDetails3) {
		getEmpDetails3().remove(empDetails3);
		empDetails3.setPermanentDistrict(null);

		return empDetails3;
	}

	public List<Campus> getCampuses() {
		return this.campuses;
	}

	public void setCampuses(List<Campus> campuses) {
		this.campuses = campuses;
	}

	public Campus addCampus(Campus campus) {
		getCampuses().add(campus);
		campus.setDistrict(this);

		return campus;
	}

	public Campus removeCampus(Campus campus) {
		getCampuses().remove(campus);
		campus.setDistrict(null);

		return campus;
	}

	public List<City> getCities() {
		return this.cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}

	public City addCity(City city) {
		getCities().add(city);
		city.setDistrict(this);

		return city;
	}

	public City removeCity(City city) {
		getCities().remove(city);
		city.setDistrict(null);

		return city;
	}

	public List<School> getSchools() {
		return this.schools;
	}

	public void setSchools(List<School> schools) {
		this.schools = schools;
	}

	public School addSchool(School school) {
		getSchools().add(school);
		school.setDistrict(this);

		return school;
	}

	public School removeSchool(School school) {
		getSchools().remove(school);
		school.setDistrict(null);

		return school;
	}

	public State getState() {
		return this.state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public List<Organization> getOrganizations() {
		return this.organizations;
	}

	public void setOrganizations(List<Organization> organizations) {
		this.organizations = organizations;
	}

	public Organization addOrganization(Organization organization) {
		getOrganizations().add(organization);
		organization.setDistrict(this);

		return organization;
	}

	public Organization removeOrganization(Organization organization) {
		getOrganizations().remove(organization);
		organization.setDistrict(null);

		return organization;
	}

	public List<StudentApplication> getStdApplications1() {
		return this.stdApplications1;
	}

	public void setStdApplications1(List<StudentApplication> stdApplications1) {
		this.stdApplications1 = stdApplications1;
	}

	public StudentApplication addStdApplications1(StudentApplication stdApplications1) {
		getStdApplications1().add(stdApplications1);
		stdApplications1.setPresentDistrict(this);

		return stdApplications1;
	}

	public StudentApplication removeStdApplications1(StudentApplication stdApplications1) {
		getStdApplications1().remove(stdApplications1);
		stdApplications1.setPresentDistrict(null);

		return stdApplications1;
	}

	public List<StudentApplication> getStdApplications2() {
		return this.stdApplications2;
	}

	public void setStdApplications2(List<StudentApplication> stdApplications2) {
		this.stdApplications2 = stdApplications2;
	}

	public StudentApplication addStdApplications2(StudentApplication stdApplications2) {
		getStdApplications2().add(stdApplications2);
		stdApplications2.setPermanentDistrict(this);

		return stdApplications2;
	}

	public StudentApplication removeStdApplications2(StudentApplication stdApplications2) {
		getStdApplications2().remove(stdApplications2);
		stdApplications2.setPermanentDistrict(null);

		return stdApplications2;
	}

	public List<StudentEnquiry> getEnquiries() {
		return this.enquiries;
	}

	public void setEnquiries(List<StudentEnquiry> enquiries) {
		this.enquiries = enquiries;
	}

	public StudentEnquiry addEnquiry(StudentEnquiry enquiry) {
		getEnquiries().add(enquiry);
		enquiry.setDistrict(this);

		return enquiry;
	}

	public StudentEnquiry removeEnquiry(StudentEnquiry enquiry) {
		getEnquiries().remove(enquiry);
		enquiry.setDistrict(null);

		return enquiry;
	}

	public List<StudentDetail> getStdStudentDetails1() {
		return this.stdStudentDetails1;
	}

	public void setStdStudentDetails1(List<StudentDetail> stdStudentDetails1) {
		this.stdStudentDetails1 = stdStudentDetails1;
	}

	public StudentDetail addStdStudentDetails1(StudentDetail stdStudentDetails1) {
		getStdStudentDetails1().add(stdStudentDetails1);
		stdStudentDetails1.setPresentDistrict(this);

		return stdStudentDetails1;
	}

	public StudentDetail removeStdStudentDetails1(StudentDetail stdStudentDetails1) {
		getStdStudentDetails1().remove(stdStudentDetails1);
		stdStudentDetails1.setPresentDistrict(null);

		return stdStudentDetails1;
	}

	public List<StudentDetail> getStdStudentDetails2() {
		return this.stdStudentDetails2;
	}

	public void setStdStudentDetails2(List<StudentDetail> stdStudentDetails2) {
		this.stdStudentDetails2 = stdStudentDetails2;
	}

	public StudentDetail addStdStudentDetails2(StudentDetail stdStudentDetails2) {
		getStdStudentDetails2().add(stdStudentDetails2);
		stdStudentDetails2.setPermanentDistrict(this);

		return stdStudentDetails2;
	}

	public StudentDetail removeStdStudentDetails2(StudentDetail stdStudentDetails2) {
		getStdStudentDetails2().remove(stdStudentDetails2);
		stdStudentDetails2.setPermanentDistrict(null);

		return stdStudentDetails2;
	}

}