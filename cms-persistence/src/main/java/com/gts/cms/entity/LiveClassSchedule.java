package com.gts.cms.entity;

import com.gts.cms.converter.HashSetLongToStringConverter;
import lombok.*;

import javax.persistence.*;
import java.sql.Time;
import java.time.LocalDate;
import java.util.Date;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
@Entity
@Table(name = "t_tt_live_cls_schedule")
@NamedQuery(name = "LiveClassSchedule.findAll", query = "SELECT ls FROM LiveClassSchedule ls")
public class LiveClassSchedule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_live_cls_schedule_id")
    private Long liveClsScheduleId;

    @Column(name = "scheduled_on_date")
    private LocalDate scheduledOnDate;

    @Column(name = "from_time")
    private Time fromTime;

    @Column(name = "to_time")
    private Time toTime;

    @Column(name = "is_onetime")
    private Boolean isOnetime;

    @Column(name = "is_recurring")
    private Boolean isRecurring;

    @Column(name = "start_date")
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "agenda")
    private String agenda;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "zoom_id")
    private ZoomHost zoomId;

    @Column(name = "host_video_url")
    private String hostVideoUrl;

    @Column(name = "password")
    private String password;

    @Column(name = "sessionIds")
    @Convert(converter = HashSetLongToStringConverter.class)
    private Set<Long> sessionIds;
    private Boolean recordingFilesAvailable;

    @Enumerated(EnumType.STRING)
    private RecordingFileStatus recordingFileStatus;

    @Column(name = "topic")
    private String topic;

    @Column(name = "zoom_host_id")
    private String zoomHostId;

    @Column(name = "zoom_join_url")
    private String zoomJoinUrl;

    @Column(name = "zoom_meeting_id")
    private String zoomMeetingId;

    @Column(name = "zoom_meeting_uuid")
    private String zoomMeetingUuid;

    @Column(name = "zoom_start_url")
    private String zoomStartUrl;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "reason")
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private String createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private String updatedUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "team_host_id")
    private TeamHost team;

    @Column(name = "team_event_id")
    private String teamEventId;

    @Column(name = "team_join_url")
    private String teamJoinUrl;

    @ManyToOne
    @JoinColumn(name = "fk_school_id")
    private School school;

    @ManyToOne
    @JoinColumn(name = "fk_group_section_id")
    private GroupSection groupSection;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_timetable_schedule_id")
    private Schedule schedule;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_cls_emp_id")
    private EmployeeDetail classEmployeeDetail;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_subjecttype_catdet_id")
    private GeneralDetail subjecttype;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_subject_id")
    private Subject subject;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_stdbatch_id")
    private Studentbatch studentbatch;

    //bi-directional many-to-one association to Weekday
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_weekday_id")
    private Weekday weekday;

    //bi-directional many-to-one association to ClassTiming
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_class_timing_id")
    private ClassTiming classTiming;

}
