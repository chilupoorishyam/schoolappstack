package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "t_hr_emp_perf_assessment_questions")
@NamedQuery(name="EmpPerfAssessmentQuestions.findAll", query="SELECT e FROM EmpPerfAssessmentQuestions e")
public class EmpPerfAssessmentQuestions implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_assessment_question_id", nullable = false)
    private Long assessmentQuestionId;

    @Column(name = "question_name")
    private String questionName;

    @Column(name = "question_code")
    private String questionCode;

    @Column(name = "dependant_question_id")
    private Long dependantQuestionId;

    @Column(name = "is_active", nullable = false)
    private Boolean active;

    @Column(name = "reason")
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt", nullable = false)
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;
    @OneToMany(mappedBy = "assessmentQuestionId")
    private List<EmpPerfAssessmentQutnOpts> empPerfAssessmentQutnOpts;

    public Long getAssessmentQuestionId() {
        return assessmentQuestionId;
    }

    public void setAssessmentQuestionId(Long assessmentQuestionId) {
        this.assessmentQuestionId = assessmentQuestionId;
    }

    public String getQuestionName() {
        return questionName;
    }

    public void setQuestionName(String questionName) {
        this.questionName = questionName;
    }

    public String getQuestionCode() {
        return questionCode;
    }

    public void setQuestionCode(String questionCode) {
        this.questionCode = questionCode;
    }

    public Long getDependantQuestionId() {
        return dependantQuestionId;
    }

    public void setDependantQuestionId(Long dependantQuestionId) {
        this.dependantQuestionId = dependantQuestionId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    public Long getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(Long createdUser) {
        this.createdUser = createdUser;
    }

    public Date getUpdatedDt() {
        return updatedDt;
    }

    public void setUpdatedDt(Date updatedDt) {
        this.updatedDt = updatedDt;
    }

    public Long getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(Long updatedUser) {
        this.updatedUser = updatedUser;
    }

    public List<EmpPerfAssessmentQutnOpts> getEmpPerfAssessmentQutnOpts() {
        return empPerfAssessmentQutnOpts;
    }

    public void setEmpPerfAssessmentQutnOpts(List<EmpPerfAssessmentQutnOpts> empPerfAssessmentQutnOpts) {
        this.empPerfAssessmentQutnOpts = empPerfAssessmentQutnOpts;
    }
}
