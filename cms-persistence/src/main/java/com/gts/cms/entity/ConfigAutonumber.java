package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_m_config_autonumbers database table.
 * 
 */
@Entity
@Table(name="t_m_config_autonumbers")
@NamedQuery(name="ConfigAutonumber.findAll", query="SELECT c FROM ConfigAutonumber c")
public class ConfigAutonumber implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_autoconfig_id")
	private Long autoconfigId;

	@Column(name="config_attribute_name")
	private String configAttributeName;

	@Column(name="config_atttribute_code")
	private String configAtttributeCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="current_number")
	private String currentNumber;

	private String formula;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_auto_inc_required")
	private Boolean isAutoIncRequired;

	@Column(name="is_formula")
	private Boolean isFormula;

	private String prefix;

	private String reason;

	@Column(name="reset_by")
	private String resetBy;

	private String suffix;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to Course
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_id")
	private Course course;


	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	public ConfigAutonumber() {
	}

	public Long getAutoconfigId() {
		return this.autoconfigId;
	}

	public void setAutoconfigId(Long autoconfigId) {
		this.autoconfigId = autoconfigId;
	}

	public String getConfigAttributeName() {
		return this.configAttributeName;
	}

	public void setConfigAttributeName(String configAttributeName) {
		this.configAttributeName = configAttributeName;
	}

	public String getConfigAtttributeCode() {
		return this.configAtttributeCode;
	}

	public void setConfigAtttributeCode(String configAtttributeCode) {
		this.configAtttributeCode = configAtttributeCode;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getCurrentNumber() {
		return this.currentNumber;
	}

	public void setCurrentNumber(String currentNumber) {
		this.currentNumber = currentNumber;
	}

	public String getFormula() {
		return this.formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsAutoIncRequired() {
		return this.isAutoIncRequired;
	}

	public void setIsAutoIncRequired(Boolean isAutoIncRequired) {
		this.isAutoIncRequired = isAutoIncRequired;
	}

	public Boolean getIsFormula() {
		return this.isFormula;
	}

	public void setIsFormula(Boolean isFormula) {
		this.isFormula = isFormula;
	}

	public String getPrefix() {
		return this.prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getResetBy() {
		return this.resetBy;
	}

	public void setResetBy(String resetBy) {
		this.resetBy = resetBy;
	}

	public String getSuffix() {
		return this.suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Course getCourse() {
		return this.course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}



	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

}