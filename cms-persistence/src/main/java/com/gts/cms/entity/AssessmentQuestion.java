package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_dl_assessment_questions database table.
 * 
 */
@Entity
@Table(name = "t_dl_assessment_questions")
@NamedQuery(name = "AssessmentQuestion.findAll", query = "SELECT a FROM AssessmentQuestion a")
public class AssessmentQuestion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_dl_assessment_question_id")
	private Long assessmentQuestionId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne
	@JoinColumn(name = "fk_dl_onlinecourse_id")
	private OnlineCourses onlineCourses;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "question_order")
	private Integer questionOrder;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to Assessment
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_dl_assessment_id")
	private Assessment assessment;

	// bi-directional many-to-one association to CourseQuestion
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_dl_course_question_id")
	private CourseQuestion courseQuestion;

	// bi-directional many-to-one association to MemberAssessmentDetail
	@OneToMany(mappedBy = "assessmentQuestion")
	private List<MemberAssessmentDetail> memberAssessmentDetails;
	
	// bi-directional many-to-one association to AssessmentAnswers
	@OneToMany(mappedBy = "assessmentQuestion",cascade = CascadeType.ALL)
	private List<AssessmentAnswers> assessmentAnswers;

	public AssessmentQuestion() {
	}

	public Long getAssessmentQuestionId() {
		return this.assessmentQuestionId;
	}

	public void setAssessmentQuestionId(Long assessmentQuestionId) {
		this.assessmentQuestionId = assessmentQuestionId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getQuestionOrder() {
		return this.questionOrder;
	}

	public void setQuestionOrder(Integer questionOrder) {
		this.questionOrder = questionOrder;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public OnlineCourses getOnlineCourses() {
		return onlineCourses;
	}

	public void setOnlineCourses(OnlineCourses onlineCourses) {
		this.onlineCourses = onlineCourses;
	}

	public Assessment getAssessment() {
		return assessment;
	}

	public void setAssessment(Assessment assessment) {
		this.assessment = assessment;
	}

	public CourseQuestion getCourseQuestion() {
		return courseQuestion;
	}

	public void setCourseQuestion(CourseQuestion courseQuestion) {
		this.courseQuestion = courseQuestion;
	}

	public List<MemberAssessmentDetail> getMemberAssessmentDetails() {
		return memberAssessmentDetails;
	}

	public void setMemberAssessmentDetails(List<MemberAssessmentDetail> memberAssessmentDetails) {
		this.memberAssessmentDetails = memberAssessmentDetails;
	}

	public List<AssessmentAnswers> getAssessmentAnswers() {
		return assessmentAnswers;
	}

	public void setAssessmentAnswers(List<AssessmentAnswers> assessmentAnswers) {
		this.assessmentAnswers = assessmentAnswers;
	}
}