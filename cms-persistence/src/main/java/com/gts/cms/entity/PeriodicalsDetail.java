package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_lib_periodicals_details database table.
 * 
 */
@Entity
@Table(name="t_lib_periodicals_details")
@NamedQuery(name="PeriodicalsDetail.findAll", query="SELECT p FROM PeriodicalsDetail p")
public class PeriodicalsDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_lib_periodical_det_id")
	private Long periodicalDetId;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String description;

	@Column(name="is_active")
	private Boolean isActive;

	private String issue;

	@Column(name="no_of_pages")
	private Integer noOfPages;

	private String reason;

	@Column(name="received_by")
	private String receivedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="received_date")
	private Date receivedDate;

	@Column(name="ref_image1")
	private String refImage1;

	@Column(name="ref_image2")
	private String refImage2;

	@Column(name="ref_image3")
	private String refImage3;

	@Column(name="sr_no")
	private Integer srNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	private String volume;

	//bi-directional many-to-one association to Organization
	@ManyToOne
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to LibraryDetail
	@ManyToOne
	@JoinColumn(name="fk_library_id")
	private LibraryDetail libraryDetail;

	//bi-directional many-to-one association to Periodical
	@ManyToOne
	@JoinColumn(name="fk_lib_periodical_id")
	private Periodical periodical;

	//bi-directional many-to-one association to LibShelve
	@ManyToOne
	@JoinColumn(name="fk_lib_shelve_id")
	private LibShelve libShelve;

	public PeriodicalsDetail() {
	}

	public Long getPeriodicalDetId() {
		return this.periodicalDetId;
	}

	public void setPeriodicalDetId(Long periodicalDetId) {
		this.periodicalDetId = periodicalDetId;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getIssue() {
		return this.issue;
	}

	public void setIssue(String issue) {
		this.issue = issue;
	}

	public Integer getNoOfPages() {
		return this.noOfPages;
	}

	public void setNoOfPages(Integer noOfPages) {
		this.noOfPages = noOfPages;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReceivedBy() {
		return this.receivedBy;
	}

	public void setReceivedBy(String receivedBy) {
		this.receivedBy = receivedBy;
	}

	public Date getReceivedDate() {
		return this.receivedDate;
	}

	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	public String getRefImage1() {
		return this.refImage1;
	}

	public void setRefImage1(String refImage1) {
		this.refImage1 = refImage1;
	}

	public String getRefImage2() {
		return this.refImage2;
	}

	public void setRefImage2(String refImage2) {
		this.refImage2 = refImage2;
	}

	public String getRefImage3() {
		return this.refImage3;
	}

	public void setRefImage3(String refImage3) {
		this.refImage3 = refImage3;
	}

	public Integer getSrNo() {
		return this.srNo;
	}

	public void setSrNo(Integer srNo) {
		this.srNo = srNo;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getVolume() {
		return this.volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public LibraryDetail getLibraryDetail() {
		return this.libraryDetail;
	}

	public void setLibraryDetail(LibraryDetail libraryDetail) {
		this.libraryDetail = libraryDetail;
	}

	public Periodical getPeriodical() {
		return this.periodical;
	}

	public void setPeriodical(Periodical periodical) {
		this.periodical = periodical;
	}

	public LibShelve getLibShelve() {
		return this.libShelve;
	}

	public void setLibShelve(LibShelve libShelve) {
		this.libShelve = libShelve;
	}

}