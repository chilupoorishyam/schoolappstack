package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.math.BigInteger;
import java.util.List;


/**
 * The persistent class for the t_hr_payroll_group database table.
 * 
 */
@Entity
@Table(name="t_hr_payroll_group")
@NamedQuery(name="PayrollGroup.findAll", query="SELECT p FROM PayrollGroup p")
public class PayrollGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_payroll_group_id")
	private Long payrollGroupId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="emp_sal_type")
	private String empSalType;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_lopa_as_deduction")
	private Boolean isLopaAsDeduction;

	@Column(name="is_lopa_from_payrollcategory")
	private Boolean isLopaFromPayrollcategory;

	@Column(name="lopa_default_value")
	private String lopaDefaultValue;

	@Column(name="lopa_value")
	private String lopaValue;

	@Column(name="lopa_value_type")
	private String lopaValueType;

	@Column(name="payment_frequency")
	private BigInteger paymentFrequency;

	@Column(name="payroll_group_name")
	private String payrollGroupName;

	@Column(name="payslip_generation_day")
	private Integer payslipGenerationDay;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeePayrollGroup
	@OneToMany(mappedBy="payrollGroup",fetch = FetchType.LAZY)
	private List<EmployeePayrollGroup> employeePayrollGroups;

	//bi-directional many-to-one association to EmployeePayslipGeneration
	@OneToMany(mappedBy="payrollGroup",fetch = FetchType.LAZY)
	private List<EmployeePayslipGeneration> employeePayslipGenerations;

	//bi-directional many-to-one association to PayrollCategoryGroup
	@OneToMany(mappedBy="payrollGroup",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	private List<PayrollCategoryGroup> payrollCategoryGroups;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	public PayrollGroup() {
	}

	public Long getPayrollGroupId() {
		return this.payrollGroupId;
	}

	public void setPayrollGroupId(Long payrollGroupId) {
		this.payrollGroupId = payrollGroupId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getEmpSalType() {
		return this.empSalType;
	}

	public void setEmpSalType(String empSalType) {
		this.empSalType = empSalType;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsLopaAsDeduction() {
		return this.isLopaAsDeduction;
	}

	public void setIsLopaAsDeduction(Boolean isLopaAsDeduction) {
		this.isLopaAsDeduction = isLopaAsDeduction;
	}

	public Boolean getIsLopaFromPayrollcategory() {
		return this.isLopaFromPayrollcategory;
	}

	public void setIsLopaFromPayrollcategory(Boolean isLopaFromPayrollcategory) {
		this.isLopaFromPayrollcategory = isLopaFromPayrollcategory;
	}

	public String getLopaDefaultValue() {
		return this.lopaDefaultValue;
	}

	public void setLopaDefaultValue(String lopaDefaultValue) {
		this.lopaDefaultValue = lopaDefaultValue;
	}

	public String getLopaValue() {
		return this.lopaValue;
	}

	public void setLopaValue(String lopaValue) {
		this.lopaValue = lopaValue;
	}

	public String getLopaValueType() {
		return this.lopaValueType;
	}

	public void setLopaValueType(String lopaValueType) {
		this.lopaValueType = lopaValueType;
	}

	public BigInteger getPaymentFrequency() {
		return this.paymentFrequency;
	}

	public void setPaymentFrequency(BigInteger paymentFrequency) {
		this.paymentFrequency = paymentFrequency;
	}

	public String getPayrollGroupName() {
		return this.payrollGroupName;
	}

	public void setPayrollGroupName(String payrollGroupName) {
		this.payrollGroupName = payrollGroupName;
	}

	public Integer getPayslipGenerationDay() {
		return this.payslipGenerationDay;
	}

	public void setPayslipGenerationDay(Integer payslipGenerationDay) {
		this.payslipGenerationDay = payslipGenerationDay;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<EmployeePayrollGroup> getEmployeePayrollGroups() {
		return this.employeePayrollGroups;
	}

	public void setEmployeePayrollGroups(List<EmployeePayrollGroup> employeePayrollGroups) {
		this.employeePayrollGroups = employeePayrollGroups;
	}

	public EmployeePayrollGroup addEmployeePayrollGroup(EmployeePayrollGroup employeePayrollGroup) {
		getEmployeePayrollGroups().add(employeePayrollGroup);
		employeePayrollGroup.setPayrollGroup(this);

		return employeePayrollGroup;
	}

	public EmployeePayrollGroup removeEmployeePayrollGroup(EmployeePayrollGroup employeePayrollGroup) {
		getEmployeePayrollGroups().remove(employeePayrollGroup);
		employeePayrollGroup.setPayrollGroup(null);

		return employeePayrollGroup;
	}

	public List<EmployeePayslipGeneration> getEmployeePayslipGenerations() {
		return this.employeePayslipGenerations;
	}

	public void setEmployeePayslipGenerations(List<EmployeePayslipGeneration> employeePayslipGenerations) {
		this.employeePayslipGenerations = employeePayslipGenerations;
	}

	public EmployeePayslipGeneration addEmployeePayslipGeneration(EmployeePayslipGeneration employeePayslipGeneration) {
		getEmployeePayslipGenerations().add(employeePayslipGeneration);
		employeePayslipGeneration.setPayrollGroup(this);

		return employeePayslipGeneration;
	}

	public EmployeePayslipGeneration removeEmployeePayslipGeneration(EmployeePayslipGeneration employeePayslipGeneration) {
		getEmployeePayslipGenerations().remove(employeePayslipGeneration);
		employeePayslipGeneration.setPayrollGroup(null);

		return employeePayslipGeneration;
	}

	public List<PayrollCategoryGroup> getPayrollCategoryGroups() {
		return this.payrollCategoryGroups;
	}

	public void setPayrollCategoryGroups(List<PayrollCategoryGroup> payrollCategoryGroups) {
		this.payrollCategoryGroups = payrollCategoryGroups;
	}

	public PayrollCategoryGroup addPayrollCategoryGroup(PayrollCategoryGroup payrollCategoryGroup) {
		getPayrollCategoryGroups().add(payrollCategoryGroup);
		payrollCategoryGroup.setPayrollGroup(this);

		return payrollCategoryGroup;
	}

	public PayrollCategoryGroup removePayrollCategoryGroup(PayrollCategoryGroup payrollCategoryGroup) {
		getPayrollCategoryGroups().remove(payrollCategoryGroup);
		payrollCategoryGroup.setPayrollGroup(null);

		return payrollCategoryGroup;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

}