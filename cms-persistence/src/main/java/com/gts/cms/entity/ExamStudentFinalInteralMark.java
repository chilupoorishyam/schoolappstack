package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_exam_student_final_interal_marks database
 * table.
 */
@Data
@Entity
@Table(name = "t_exam_student_final_interal_marks")
@NamedQuery(name = "ExamStudentFinalInteralMark.findAll", query = "SELECT e FROM ExamStudentFinalInteralMark e")
public class ExamStudentFinalInteralMark implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_exam_final_int_mark_id")
    private Long examFinalIntMarkId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "final_marks")
    private Integer finalMarks;

    @Column(name = "grade")
    private String grade;

    @Column(name = "fk_exam_ids")
    private String examIds;

    @Column(name = "internal_marks")
    private String internalMarks;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "is_published")
    private Boolean isPublished;

    @Temporal(TemporalType.DATE)
    @Column(name = "published_on")
    private Date publishedOn;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    // bi-directional many-to-one association to TMSchool
    @ManyToOne
    @JoinColumn(name = "fk_school_id")
    private School school;

    // bi-directional many-to-one association to TStdStudentDetail
    @ManyToOne
    @JoinColumn(name = "fk_student_id")
    private StudentDetail studentDetail;

    // bi-directional many-to-one association to TMCourseYear
    @ManyToOne
    @JoinColumn(name = "fk_course_year_id")
    private CourseYear courseYear;

    // bi-directional many-to-one association to TMSubject
    @ManyToOne
    @JoinColumn(name = "fk_subject_id")
    private Subject subject;

    // bi-directional many-to-one association to TMSubject
    @ManyToOne
    @JoinColumn(name = "fk_examgroup_id")
    private ExamGrouping examGrouping;

    @ManyToOne
    @JoinColumn(name = "fk_exam_type_id")
    private ExamTypes examType;
}