package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_ams_fundraise_account database table.
 * 
 */
@Entity
@Table(name = "t_ams_fundraise_account")
@NamedQuery(name = "AmsFundraiseAccount.findAll", query = "SELECT f FROM AmsFundraiseAccount f")
public class AmsFundraiseAccount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_fundraise_account_id")
	private Long fundraiseAccountId;

	@Column(name = "account_name")
	private String accountName;

	@Column(name = "account_number")
	private String accountNumber;

	private String address;

	@Column(name = "bank_name")
	private String bankName;

	private String branch;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne
	@JoinColumn(name = "fk_school_id")
	private School school;

	@ManyToOne
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	@Column(name = "googlepay_number")
	private String googlepayNumber;

	@Column(name = "ifsc_code")
	private String ifscCode;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "phonepay_number")
	private String phonepayNumber;

	private String reason;

	@Column(name = "total_balance")
	private BigDecimal totalBalance;

	@Column(name = "total_fund_expended")
	private BigDecimal totalFundExpended;

	@Column(name = "total_fund_raised")
	private BigDecimal totalFundRaised;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "upi_id")
	private String upiId;

	@Column(name = "upi_qr_code_url")
	private String upiQrCodeUrl;

	// bi-directional many-to-one association to AmsFundraiseEvent
	@OneToMany(mappedBy = "fundraiseAccount")
	private List<AmsFundraiseEvent> fundraiseEvents;

	public AmsFundraiseAccount() {
	}

	public Long getFundraiseAccountId() {
		return this.fundraiseAccountId;
	}

	public void setFundraiseAccountId(Long fundraiseAccountId) {
		this.fundraiseAccountId = fundraiseAccountId;
	}

	public String getAccountName() {
		return this.accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranch() {
		return this.branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getGooglepayNumber() {
		return this.googlepayNumber;
	}

	public void setGooglepayNumber(String googlepayNumber) {
		this.googlepayNumber = googlepayNumber;
	}

	public String getIfscCode() {
		return this.ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getPhonepayNumber() {
		return this.phonepayNumber;
	}

	public void setPhonepayNumber(String phonepayNumber) {
		this.phonepayNumber = phonepayNumber;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getTotalBalance() {
		return this.totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public BigDecimal getTotalFundExpended() {
		return this.totalFundExpended;
	}

	public void setTotalFundExpended(BigDecimal totalFundExpended) {
		this.totalFundExpended = totalFundExpended;
	}

	public BigDecimal getTotalFundRaised() {
		return this.totalFundRaised;
	}

	public void setTotalFundRaised(BigDecimal totalFundRaised) {
		this.totalFundRaised = totalFundRaised;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getUpiId() {
		return this.upiId;
	}

	public void setUpiId(String upiId) {
		this.upiId = upiId;
	}

	public String getUpiQrCodeUrl() {
		return this.upiQrCodeUrl;
	}

	public void setUpiQrCodeUrl(String upiQrCodeUrl) {
		this.upiQrCodeUrl = upiQrCodeUrl;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public List<AmsFundraiseEvent> getFundraiseEvents() {
		return fundraiseEvents;
	}

	public void setFundraiseEvents(List<AmsFundraiseEvent> fundraiseEvents) {
		this.fundraiseEvents = fundraiseEvents;
	}

}