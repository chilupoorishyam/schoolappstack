package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_fee_discounts_particulars database table.
 * 
 */
@Entity
@Table(name="t_fee_discounts_particulars")
@NamedQuery(name="FeeDiscountsParticular.findAll", query="SELECT f FROM FeeDiscountsParticular f")
public class FeeDiscountsParticular implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_discount_particular_id")
	private Long feeDiscountParticularId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FeeCategory
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_category_id")
	private FeeCategory feeCategory;

	//bi-directional many-to-one association to FeeDiscount
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_discount_id")
	private FeeDiscount feeDiscount;

	//bi-directional many-to-one association to FeeParticular
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_particulars_id")
	private FeeParticular feeParticular;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	public FeeDiscountsParticular() {
	}

	public Long getFeeDiscountParticularId() {
		return this.feeDiscountParticularId;
	}

	public void setFeeDiscountParticularId(Long feeDiscountParticularId) {
		this.feeDiscountParticularId = feeDiscountParticularId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public FeeCategory getFeeCategory() {
		return this.feeCategory;
	}

	public void setFeeCategory(FeeCategory feeCategory) {
		this.feeCategory = feeCategory;
	}

	public FeeDiscount getFeeDiscount() {
		return this.feeDiscount;
	}

	public void setFeeDiscount(FeeDiscount feeDiscount) {
		this.feeDiscount = feeDiscount;
	}

	public FeeParticular getFeeParticular() {
		return this.feeParticular;
	}

	public void setFeeParticular(FeeParticular feeParticular) {
		this.feeParticular = feeParticular;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

}