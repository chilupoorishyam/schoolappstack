package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_inv_storesmaster database table.
 * 
 */
@Entity
@Table(name = "t_inv_storesmaster")
@NamedQuery(name = "InvStoresmaster.findAll", query = "SELECT i FROM InvStoresmaster i")
public class InvStoresmaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_store_id")
	private Long storeId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "fk_school_ids")
	private String schoolIds;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_incharge_emp_id")
	private EmployeeDetail inchargeEmp;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Column(name = "store_code")
	private String storeCode;

	@Column(name = "store_name")
	private String storeName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	/*
	 * // bi-directional many-to-one association to InvInternalIndent
	 * 
	 * @OneToMany(mappedBy = "InvStoresmaster") private List<InvInternalIndent>
	 * invInternalIndents;
	 * 
	 * // bi-directional many-to-one association to InvInternalIndentitem
	 * 
	 * @OneToMany(mappedBy = "InvStoresmaster") private List<InvInternalIndentitem>
	 * invInternalIndentitems;
	 * 
	 * // bi-directional many-to-one association to InvInternalIssue
	 * 
	 * @OneToMany(mappedBy = "InvStoresmaster") private List<InvInternalIssue>
	 * invInternalIssues;
	 * 
	 * // bi-directional many-to-one association to InvInternalReturn
	 * 
	 * @OneToMany(mappedBy = "InvStoresmaster") private List<InvInternalReturn>
	 * invInternalReturns;
	 * 
	 * // bi-directional many-to-one association to InvItemDetail
	 * 
	 * @OneToMany(mappedBy = "InvStoresmaster") private List<InvItemDetail>
	 * invItemDetails;
	 * 
	 * // bi-directional many-to-one association to InvItemopeningStock
	 * 
	 * @OneToMany(mappedBy = "InvStoresmaster") private List<InvItemopeningStock>
	 * invItemopeningStocks;
	 * 
	 * // bi-directional many-to-one association to InvPurchaseOrder
	 * 
	 * @OneToMany(mappedBy = "InvStoresmaster") private List<InvPurchaseOrder>
	 * invPurchaseOrders;
	 * 
	 * // bi-directional many-to-one association to InvPurchasereturn
	 * 
	 * @OneToMany(mappedBy = "InvStoresmaster") private List<InvPurchasereturn>
	 * invPurchasereturns;
	 * 
	 * // bi-directional many-to-one association to InvSrv
	 * 
	 * @OneToMany(mappedBy = "InvStoresmaster") private List<InvSrv> invSrvs;
	 * 
	 * // bi-directional many-to-one association to InvStockledger
	 * 
	 * @OneToMany(mappedBy = "InvStoresmaster") private List<InvStockledger>
	 * invStockledgers;
	 * 
	 * // bi-directional many-to-one association to InvStoreItem
	 * 
	 * @OneToMany(mappedBy = "InvStoresmaster") private List<InvStoreItem>
	 * invStoreItems;
	 */

	public InvStoresmaster() {
	}

	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public EmployeeDetail getInchargeEmp() {
		return inchargeEmp;
	}

	public void setInchargeEmp(EmployeeDetail inchargeEmp) {
		this.inchargeEmp = inchargeEmp;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getSchoolIds() {
		return schoolIds;
	}

	public void setSchoolIds(String schoolIds) {
		this.schoolIds = schoolIds;
	}

	/*
	 * public List<InvInternalIndent> getInvInternalIndents() { return
	 * invInternalIndents; }
	 * 
	 * public void setInvInternalIndents(List<InvInternalIndent> invInternalIndents)
	 * { this.invInternalIndents = invInternalIndents; }
	 * 
	 * public List<InvInternalIndentitem> getInvInternalIndentitems() { return
	 * invInternalIndentitems; }
	 * 
	 * public void setInvInternalIndentitems(List<InvInternalIndentitem>
	 * invInternalIndentitems) { this.invInternalIndentitems =
	 * invInternalIndentitems; }
	 * 
	 * public List<InvInternalIssue> getInvInternalIssues() { return
	 * invInternalIssues; }
	 * 
	 * public void setInvInternalIssues(List<InvInternalIssue> invInternalIssues) {
	 * this.invInternalIssues = invInternalIssues; }
	 * 
	 * public List<InvInternalReturn> getInvInternalReturns() { return
	 * invInternalReturns; }
	 * 
	 * public void setInvInternalReturns(List<InvInternalReturn> invInternalReturns)
	 * { this.invInternalReturns = invInternalReturns; }
	 * 
	 * public List<InvItemDetail> getInvItemDetails() { return invItemDetails; }
	 * 
	 * public void setInvItemDetails(List<InvItemDetail> invItemDetails) {
	 * this.invItemDetails = invItemDetails; }
	 * 
	 * public List<InvItemopeningStock> getInvItemopeningStocks() { return
	 * invItemopeningStocks; }
	 * 
	 * public void setInvItemopeningStocks(List<InvItemopeningStock>
	 * invItemopeningStocks) { this.invItemopeningStocks = invItemopeningStocks; }
	 * 
	 * public List<InvPurchaseOrder> getInvPurchaseOrders() { return
	 * invPurchaseOrders; }
	 * 
	 * public void setInvPurchaseOrders(List<InvPurchaseOrder> invPurchaseOrders) {
	 * this.invPurchaseOrders = invPurchaseOrders; }
	 * 
	 * public List<InvPurchasereturn> getInvPurchasereturns() { return
	 * invPurchasereturns; }
	 * 
	 * public void setInvPurchasereturns(List<InvPurchasereturn> invPurchasereturns)
	 * { this.invPurchasereturns = invPurchasereturns; }
	 * 
	 * public List<InvSrv> getInvSrvs() { return invSrvs; }
	 * 
	 * public void setInvSrvs(List<InvSrv> invSrvs) { this.invSrvs = invSrvs; }
	 * 
	 * public List<InvStockledger> getInvStockledgers() { return invStockledgers; }
	 * 
	 * public void setInvStockledgers(List<InvStockledger> invStockledgers) {
	 * this.invStockledgers = invStockledgers; }
	 * 
	 * public List<InvStoreItem> getInvStoreItems() { return invStoreItems; }
	 * 
	 * public void setInvStoreItems(List<InvStoreItem> invStoreItems) {
	 * this.invStoreItems = invStoreItems; }
	 */

}