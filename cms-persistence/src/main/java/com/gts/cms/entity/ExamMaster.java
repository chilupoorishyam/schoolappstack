package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_exam_master database table.
 * 
 */
@Entity
@Table(name="t_exam_master")
@NamedQuery(name="ExamMaster.findAll", query="SELECT e FROM ExamMaster e")
public class ExamMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_exam_id")
	private Long examId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="exam_month_yr")
	private String examMonthYr;

	@Column(name="exam_name")
	private String examName;
	
	@Column(name="exam_short_name")
	private String examShortName;

	@Column(name="fee_notification_file_path")
	private String feeNotificationFilePath;

	@Temporal(TemporalType.DATE)
	@Column(name="fee_notification_published_on")
	private Date feeNotificationPublishedOn;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_academic_year_id")
	private AcademicYear academicYear;

	// bi-directional many-to-one association to Course
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_id")
	private Course course;

	// bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="from_date")
	private Date fromDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_internal_exam")
	private Boolean isInternalExam;

	@Column(name="is_regular_exam")
	private Boolean isRegularExam;

	@Column(name="is_supply_exam")
	private Boolean isSupplyExam;

	@Column(name="notification_file_path")
	private String notificationFilePath;

	@Temporal(TemporalType.DATE)
	@Column(name="notification_published_on")
	private Date notificationPublishedOn;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="to_date")
	private Date toDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to ExamFeeFine
	@OneToMany(mappedBy="examMaster")
	private List<ExamFeeFine> examFeeFines;

	//bi-directional many-to-one association to ExamFeeStructure
	@OneToMany(mappedBy="examMaster")
	private List<ExamFeeStructure> examFeeStructures;

	/*//bi-directional many-to-one association to ExamStdCourseyrSubject
	@OneToMany(mappedBy="examMaster")
	private List<ExamStdCourseyrSubject> examStdCourseyrSubjects;
*/
	//bi-directional many-to-one association to ExamStudent
	@OneToMany(mappedBy="examMaster")
	private List<ExamStudent> examStudents;

	//bi-directional many-to-one association to ExamTimetable
	@OneToMany(mappedBy="examMaster")
	private List<ExamTimetable> examTimetables;

	public ExamMaster() {
	}

	public Long getExamId() {
		return this.examId;
	}

	public void setExamId(Long examId) {
		this.examId = examId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getExamMonthYr() {
		return this.examMonthYr;
	}

	public void setExamMonthYr(String examMonthYr) {
		this.examMonthYr = examMonthYr;
	}

	public String getExamName() {
		return this.examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	public String getFeeNotificationFilePath() {
		return this.feeNotificationFilePath;
	}

	public void setFeeNotificationFilePath(String feeNotificationFilePath) {
		this.feeNotificationFilePath = feeNotificationFilePath;
	}

	public Date getFeeNotificationPublishedOn() {
		return this.feeNotificationPublishedOn;
	}

	public void setFeeNotificationPublishedOn(Date feeNotificationPublishedOn) {
		this.feeNotificationPublishedOn = feeNotificationPublishedOn;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsInternalExam() {
		return this.isInternalExam;
	}

	public void setIsInternalExam(Boolean isInternalExam) {
		this.isInternalExam = isInternalExam;
	}

	public Boolean getIsRegularExam() {
		return this.isRegularExam;
	}

	public void setIsRegularExam(Boolean isRegularExam) {
		this.isRegularExam = isRegularExam;
	}

	public Boolean getIsSupplyExam() {
		return this.isSupplyExam;
	}

	public void setIsSupplyExam(Boolean isSupplyExam) {
		this.isSupplyExam = isSupplyExam;
	}

	public String getNotificationFilePath() {
		return this.notificationFilePath;
	}

	public void setNotificationFilePath(String notificationFilePath) {
		this.notificationFilePath = notificationFilePath;
	}

	public Date getNotificationPublishedOn() {
		return this.notificationPublishedOn;
	}

	public void setNotificationPublishedOn(Date notificationPublishedOn) {
		this.notificationPublishedOn = notificationPublishedOn;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public List<ExamFeeFine> getExamFeeFines() {
		return examFeeFines;
	}

	public void setExamFeeFines(List<ExamFeeFine> examFeeFines) {
		this.examFeeFines = examFeeFines;
	}

	public List<ExamFeeStructure> getExamFeeStructures() {
		return examFeeStructures;
	}

	public void setExamFeeStructures(List<ExamFeeStructure> examFeeStructures) {
		this.examFeeStructures = examFeeStructures;
	}

	/*public List<ExamStdCourseyrSubject> getExamStdCourseyrSubjects() {
		return examStdCourseyrSubjects;
	}

	public void setExamStdCourseyrSubjects(List<ExamStdCourseyrSubject> examStdCourseyrSubjects) {
		this.examStdCourseyrSubjects = examStdCourseyrSubjects;
	}
*/
	public List<ExamStudent> getExamStudents() {
		return examStudents;
	}

	public void setExamStudents(List<ExamStudent> examStudents) {
		this.examStudents = examStudents;
	}

	public List<ExamTimetable> getExamTimetables() {
		return examTimetables;
	}

	public void setExamTimetables(List<ExamTimetable> examTimetables) {
		this.examTimetables = examTimetables;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public String getExamShortName() {
		return examShortName;
	}

	public void setExamShortName(String examShortName) {
		this.examShortName = examShortName;
	}
}