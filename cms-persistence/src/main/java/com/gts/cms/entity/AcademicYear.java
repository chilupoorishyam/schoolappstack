package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_academic_year database table.
 * 
 */
@Entity
@Table(name="t_m_academic_year")
@NamedQuery(name="AcademicYear.findAll", query="SELECT a FROM AcademicYear a")
public class AcademicYear implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_academic_year_id")
	private Long academicYearId;

	@Column(name="academic_year")
	private String academicYear;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	@Column(name="from_date")
	private Date fromDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_default")
	private Boolean isDefault;

	private String reason;

	@Temporal(TemporalType.DATE)
	@Column(name="to_date")
	private Date toDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;
	
	@Column(name="sort_order")
	private Integer sortOrder;

	//bi-directional many-to-one association to BatchwiseStudent
	@OneToMany(mappedBy="academicYear",fetch = FetchType.LAZY )
	private List<BatchwiseStudent> batchwiseStudents;

	//bi-directional many-to-one association to ElectiveGroupyrMapping
	@OneToMany(mappedBy="academicYear",fetch = FetchType.LAZY)
	private List<ElectiveGroupyrMapping> electiveGroupyrMappings;

	//bi-directional many-to-one association to StaffCourseyrSubject
	@OneToMany(mappedBy="academicYear",fetch = FetchType.LAZY)
	private List<StaffCourseyrSubject> staffCourseyrSubjects;

	//bi-directional many-to-one association to SubjectBook
	/*@OneToMany(mappedBy="academicYear")
	private List<SubjectBook> subjectBooks;*/

	//bi-directional many-to-one association to SubjectCourseyear
	@OneToMany(mappedBy="academicYear",fetch = FetchType.LAZY)
	private List<SubjectCourseyear> subjectCourseyears;

	//bi-directional many-to-one association to SubjectUnitTopic
	/*@OneToMany(mappedBy="academicYear")
	private List<SubjectUnitTopic> subjectUnitTopics;*/

	//bi-directional many-to-one association to SubjectUnit
	/*@OneToMany(mappedBy="academicYear")
	private List<SubjectUnit> subjectUnits;*/

	//bi-directional many-to-one association to Subjectregulation
	@OneToMany(mappedBy="academicYear",fetch = FetchType.LAZY)
	private List<Subjectregulation> subjectregulations;

	//bi-directional many-to-one association to FeeStructure
	@OneToMany(mappedBy="academicYear",fetch = FetchType.LAZY)
	private List<FeeStructure> feeStructures;

	//bi-directional many-to-one association to FeeTransactionMaster
	@OneToMany(mappedBy="academicYear",fetch = FetchType.LAZY)
	private List<FeeTransactionMaster> feeTransactionMasters;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to Event
	@OneToMany(mappedBy="academicYear",fetch = FetchType.LAZY)
	private List<Event> events;

	//bi-directional many-to-one association to GroupSection
	@OneToMany(mappedBy="academicYear",fetch = FetchType.LAZY)
	private List<GroupSection> groupSections;

	//bi-directional many-to-one association to Notification
	@OneToMany(mappedBy="academicYear",fetch = FetchType.LAZY)
	private List<Notification> notifications;

	//bi-directional many-to-one association to StudentAcademicbatch
	@OneToMany(mappedBy="academicYear",fetch = FetchType.LAZY)
	private List<StudentAcademicbatch> studentAcademicbatches;

	//bi-directional many-to-one association to StudentSubject
	@OneToMany(mappedBy="academicYear",fetch = FetchType.LAZY)
	private List<StudentSubject> studentSubjects;

	//bi-directional many-to-one association to FeePayment
	@OneToMany(mappedBy="academicYear",fetch = FetchType.LAZY)
	private List<TransportFeePayment> feePayments;

	//bi-directional many-to-one association to Schedule
	@OneToMany(mappedBy="academicYear",fetch = FetchType.LAZY)
	private List<Schedule> schedules;

	//bi-directional many-to-one association to Timetable
	@OneToMany(mappedBy="academicYear",fetch = FetchType.LAZY)
	private List<Timetable> timetables;

	//bi-directional many-to-one association to Timingset
	@OneToMany(mappedBy="academicYear",fetch = FetchType.LAZY)
	private List<Timingset> timingsets;

	public AcademicYear() {
	}

	public Long getAcademicYearId() {
		return this.academicYearId;
	}

	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}

	public String getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDefault() {
		return this.isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<BatchwiseStudent> getBatchwiseStudents() {
		return this.batchwiseStudents;
	}

	public void setBatchwiseStudents(List<BatchwiseStudent> batchwiseStudents) {
		this.batchwiseStudents = batchwiseStudents;
	}

	public BatchwiseStudent addBatchwiseStudent(BatchwiseStudent batchwiseStudent) {
		getBatchwiseStudents().add(batchwiseStudent);
		batchwiseStudent.setAcademicYear(this);

		return batchwiseStudent;
	}

	public BatchwiseStudent removeBatchwiseStudent(BatchwiseStudent batchwiseStudent) {
		getBatchwiseStudents().remove(batchwiseStudent);
		batchwiseStudent.setAcademicYear(null);

		return batchwiseStudent;
	}

	public List<ElectiveGroupyrMapping> getElectiveGroupyrMappings() {
		return this.electiveGroupyrMappings;
	}

	public void setElectiveGroupyrMappings(List<ElectiveGroupyrMapping> electiveGroupyrMappings) {
		this.electiveGroupyrMappings = electiveGroupyrMappings;
	}

	public ElectiveGroupyrMapping addElectiveGroupyrMapping(ElectiveGroupyrMapping electiveGroupyrMapping) {
		getElectiveGroupyrMappings().add(electiveGroupyrMapping);
		electiveGroupyrMapping.setAcademicYear(this);

		return electiveGroupyrMapping;
	}

	public ElectiveGroupyrMapping removeElectiveGroupyrMapping(ElectiveGroupyrMapping electiveGroupyrMapping) {
		getElectiveGroupyrMappings().remove(electiveGroupyrMapping);
		electiveGroupyrMapping.setAcademicYear(null);

		return electiveGroupyrMapping;
	}

	public List<StaffCourseyrSubject> getStaffCourseyrSubjects() {
		return this.staffCourseyrSubjects;
	}

	public void setStaffCourseyrSubjects(List<StaffCourseyrSubject> staffCourseyrSubjects) {
		this.staffCourseyrSubjects = staffCourseyrSubjects;
	}

	public StaffCourseyrSubject addStaffCourseyrSubject(StaffCourseyrSubject staffCourseyrSubject) {
		getStaffCourseyrSubjects().add(staffCourseyrSubject);
		staffCourseyrSubject.setAcademicYear(this);

		return staffCourseyrSubject;
	}

	public StaffCourseyrSubject removeStaffCourseyrSubject(StaffCourseyrSubject staffCourseyrSubject) {
		getStaffCourseyrSubjects().remove(staffCourseyrSubject);
		staffCourseyrSubject.setAcademicYear(null);

		return staffCourseyrSubject;
	}

	/*public List<SubjectBook> getSubjectBooks() {
		return this.subjectBooks;
	}

	public void setSubjectBooks(List<SubjectBook> subjectBooks) {
		this.subjectBooks = subjectBooks;
	}*/



	public List<SubjectCourseyear> getSubjectCourseyears() {
		return this.subjectCourseyears;
	}

	public void setSubjectCourseyears(List<SubjectCourseyear> subjectCourseyears) {
		this.subjectCourseyears = subjectCourseyears;
	}

	public SubjectCourseyear addSubjectCourseyear(SubjectCourseyear subjectCourseyear) {
		getSubjectCourseyears().add(subjectCourseyear);
		subjectCourseyear.setAcademicYear(this);

		return subjectCourseyear;
	}

	public SubjectCourseyear removeSubjectCourseyear(SubjectCourseyear subjectCourseyear) {
		getSubjectCourseyears().remove(subjectCourseyear);
		subjectCourseyear.setAcademicYear(null);

		return subjectCourseyear;
	}

	/*public List<SubjectUnitTopic> getSubjectUnitTopics() {
		return this.subjectUnitTopics;
	}

	public void setSubjectUnitTopics(List<SubjectUnitTopic> subjectUnitTopics) {
		this.subjectUnitTopics = subjectUnitTopics;
	}
*/


	/*public List<SubjectUnit> getSubjectUnits() {
		return this.subjectUnits;
	}

	public void setSubjectUnits(List<SubjectUnit> subjectUnits) {
		this.subjectUnits = subjectUnits;
	}
*/

	public List<Subjectregulation> getSubjectregulations() {
		return this.subjectregulations;
	}

	public void setSubjectregulations(List<Subjectregulation> subjectregulations) {
		this.subjectregulations = subjectregulations;
	}

	public Subjectregulation addSubjectregulation(Subjectregulation subjectregulation) {
		getSubjectregulations().add(subjectregulation);
		subjectregulation.setAcademicYear(this);

		return subjectregulation;
	}

	public Subjectregulation removeSubjectregulation(Subjectregulation subjectregulation) {
		getSubjectregulations().remove(subjectregulation);
		subjectregulation.setAcademicYear(null);

		return subjectregulation;
	}



	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public List<Event> getEvents() {
		return this.events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public Event addEvent(Event event) {
		getEvents().add(event);
		event.setAcademicYear(this);

		return event;
	}

	public Event removeEvent(Event event) {
		getEvents().remove(event);
		event.setAcademicYear(null);

		return event;
	}

	public List<GroupSection> getGroupSections() {
		return this.groupSections;
	}

	public void setGroupSections(List<GroupSection> groupSections) {
		this.groupSections = groupSections;
	}

	public GroupSection addGroupSection(GroupSection groupSection) {
		getGroupSections().add(groupSection);
		groupSection.setAcademicYear(this);

		return groupSection;
	}

	public GroupSection removeGroupSection(GroupSection groupSection) {
		getGroupSections().remove(groupSection);
		groupSection.setAcademicYear(null);

		return groupSection;
	}

	public List<Notification> getNotifications() {
		return this.notifications;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}

	public Notification addNotification(Notification notification) {
		getNotifications().add(notification);
		notification.setAcademicYear(this);

		return notification;
	}

	public Notification removeNotification(Notification notification) {
		getNotifications().remove(notification);
		notification.setAcademicYear(null);

		return notification;
	}

	public List<StudentAcademicbatch> getStudentAcademicbatches() {
		return this.studentAcademicbatches;
	}

	public void setStudentAcademicbatches(List<StudentAcademicbatch> studentAcademicbatches) {
		this.studentAcademicbatches = studentAcademicbatches;
	}

	public StudentAcademicbatch addStudentAcademicbatch(StudentAcademicbatch studentAcademicbatch) {
		getStudentAcademicbatches().add(studentAcademicbatch);
		studentAcademicbatch.setAcademicYear(this);

		return studentAcademicbatch;
	}

	public StudentAcademicbatch removeStudentAcademicbatch(StudentAcademicbatch studentAcademicbatch) {
		getStudentAcademicbatches().remove(studentAcademicbatch);
		studentAcademicbatch.setAcademicYear(null);

		return studentAcademicbatch;
	}

	public List<StudentSubject> getStudentSubjects() {
		return this.studentSubjects;
	}

	public void setStudentSubjects(List<StudentSubject> studentSubjects) {
		this.studentSubjects = studentSubjects;
	}

	public StudentSubject addStudentSubject(StudentSubject studentSubject) {
		getStudentSubjects().add(studentSubject);
		studentSubject.setAcademicYear(this);

		return studentSubject;
	}

	public StudentSubject removeStudentSubject(StudentSubject studentSubject) {
		getStudentSubjects().remove(studentSubject);
		studentSubject.setAcademicYear(null);

		return studentSubject;
	}

	public List<TransportFeePayment> getFeePayments() {
		return this.feePayments;
	}

	public void setFeePayments(List<TransportFeePayment> feePayments) {
		this.feePayments = feePayments;
	}

	public TransportFeePayment addFeePayment(TransportFeePayment feePayment) {
		getFeePayments().add(feePayment);
		feePayment.setAcademicYear(this);

		return feePayment;
	}

	public TransportFeePayment removeFeePayment(TransportFeePayment feePayment) {
		getFeePayments().remove(feePayment);
		feePayment.setAcademicYear(null);

		return feePayment;
	}

	public List<Schedule> getSchedules() {
		return this.schedules;
	}

	public void setSchedules(List<Schedule> schedules) {
		this.schedules = schedules;
	}

	public Schedule addSchedule(Schedule schedule) {
		getSchedules().add(schedule);
		schedule.setAcademicYear(this);

		return schedule;
	}

	public Schedule removeSchedule(Schedule schedule) {
		getSchedules().remove(schedule);
		schedule.setAcademicYear(null);

		return schedule;
	}

	public List<Timetable> getTimetables() {
		return this.timetables;
	}

	public void setTimetables(List<Timetable> timetables) {
		this.timetables = timetables;
	}

	public Timetable addTimetable(Timetable timetable) {
		getTimetables().add(timetable);
		timetable.setAcademicYear(this);

		return timetable;
	}

	public Timetable removeTimetable(Timetable timetable) {
		getTimetables().remove(timetable);
		timetable.setAcademicYear(null);

		return timetable;
	}

	public List<Timingset> getTimingsets() {
		return this.timingsets;
	}

	public void setTimingsets(List<Timingset> timingsets) {
		this.timingsets = timingsets;
	}

	public Timingset addTimingset(Timingset timingset) {
		getTimingsets().add(timingset);
		timingset.setAcademicYear(this);

		return timingset;
	}

	public Timingset removeTimingset(Timingset timingset) {
		getTimingsets().remove(timingset);
		timingset.setAcademicYear(null);

		return timingset;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}
}