package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_inv_complaints_wf database table.
 * 
 */
@Entity
@Table(name = "t_inv_complaints_wf")
@NamedQuery(name = "InvComplaintsWf.findAll", query = "SELECT c FROM InvComplaintsWf c")
public class InvComplaintsWf implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_inv_complaint_wf_id")
	private Long complaintWfId;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;
	
	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;
	
	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_from_emp_id")
	private EmployeeDetail fromEmp;
	
	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_from_wf_status_id")
	private WorkflowStage fromWfStatus;
	
	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_to_emp_id")
	private EmployeeDetail toEmp;
	
	// bi-directional many-to-one association to WorkflowStage
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_to_wf_status_id")
	private WorkflowStage toWfStatus;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "from_date")
	private Date fromDate;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_current_status")
	private Boolean isCurrentStatus;

	private String reason;

	@Column(name = "ref_path")
	private String refPath;

	@Column(name = "ref1_path")
	private String ref1Path;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "to_date")
	private Date toDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to ClgManagementIssue
	@ManyToOne
	@JoinColumn(name = "fk_clg_management_issue_id")
	private ClgManagementIssue clgManagementIssue;

	public InvComplaintsWf() {
	}

	public Long getComplaintWfId() {
		return this.complaintWfId;
	}

	public void setComplaintWfId(Long complaintWfId) {
		this.complaintWfId = complaintWfId;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public EmployeeDetail getFromEmp() {
		return fromEmp;
	}

	public void setFromEmp(EmployeeDetail fromEmp) {
		this.fromEmp = fromEmp;
	}

	public WorkflowStage getFromWfStatus() {
		return fromWfStatus;
	}

	public void setFromWfStatus(WorkflowStage fromWfStatus) {
		this.fromWfStatus = fromWfStatus;
	}

	public EmployeeDetail getToEmp() {
		return toEmp;
	}

	public void setToEmp(EmployeeDetail toEmp) {
		this.toEmp = toEmp;
	}

	public WorkflowStage getToWfStatus() {
		return toWfStatus;
	}

	public void setToWfStatus(WorkflowStage toWfStatus) {
		this.toWfStatus = toWfStatus;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsCurrentStatus() {
		return isCurrentStatus;
	}

	public void setIsCurrentStatus(Boolean isCurrentStatus) {
		this.isCurrentStatus = isCurrentStatus;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRefPath() {
		return refPath;
	}

	public void setRefPath(String refPath) {
		this.refPath = refPath;
	}

	public String getRef1Path() {
		return ref1Path;
	}

	public void setRef1Path(String ref1Path) {
		this.ref1Path = ref1Path;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public ClgManagementIssue getClgManagementIssue() {
		return clgManagementIssue;
	}

	public void setClgManagementIssue(ClgManagementIssue clgManagementIssue) {
		this.clgManagementIssue = clgManagementIssue;
	}
}