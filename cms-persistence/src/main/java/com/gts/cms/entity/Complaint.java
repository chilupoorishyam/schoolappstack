package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_grv_complaints database table.
 * 
 */
@Entity
@Table(name = "t_grv_complaints")
@NamedQuery(name = "Complaint.findAll", query = "SELECT c FROM Complaint c")
public class Complaint implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_grv_complaint_id")
	private Long complaintId;

	@Column(name = "area_of_grievance")
	private String areaOfGrievance;

	@Column(name = "caus_of_dissatisfaction")
	private String causOfDissatisfaction;

	@Column(name = "closing_comments")
	private String closingComments;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "complain_date")
	private Date complainDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "duration_of_incident")
	private Date durationOfIncident;

	@Column(name = "duration_of_incident_text")
	private String durationOfIncidentText;
	
	@Column(name ="grievance_doc_path")
	private String complaintDocPath;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne
	@JoinColumn(name = "fk_acknowledged_emp_id")
	private EmployeeDetail ackEmp;

	@Column(name = "fk_existing_complaint_id")
	private Long existingComplaint;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne
	@JoinColumn(name = "fk_grievance_on_catdet_id")
	private GeneralDetail grvOnCatdet;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne
	@JoinColumn(name = "fk_grievance_type_catdet_id")
	private GeneralDetail grvTypeCatdet;

	@Column(name = "fk_grv_on_resourse_id")
	private Long grvOnResourse;

	// bi-directional many-to-one association to Organization
	@ManyToOne
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne
	@JoinColumn(name = "fk_priority_catdet_id")
	private GeneralDetail priorityCatdet;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne
	@JoinColumn(name = "fk_closed_emp_id")
	private EmployeeDetail closedEmp;

	// bi-directional many-to-one association to StudentDetail
	@ManyToOne
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	// bi-directional many-to-one association to WorkflowStage
	@ManyToOne
	@JoinColumn(name = "fk_wf_stage_id")
	private WorkflowStage workflowStage;

	private String incident;

	@Column(name = "incident_description")
	private String incidentDescription;

	@Column(name = "is_acknowledged")
	private Boolean isAcknowledged;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_already_reported")
	private Boolean isAlreadyReported;

	@Column(name = "is_closed")
	private Boolean isClosed;

	@Column(name = "other_text")
	private String otherText;

	private String reason;

	@Column(name = "remedy_seeking")
	private String remedySeeking;

	@Column(name = "student_experience")
	private Long studentExperience;

	@Column(name = "student_feedback")
	private String studentFeedback;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "wf_comments")
	private String wfComments;

	// bi-directional many-to-one association to ComplaintDetail
	@OneToMany(mappedBy = "complaint", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ComplaintDetail> complaintDetails;

	// bi-directional many-to-one association to ComplaintTask
	@OneToMany(mappedBy = "complaint", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ComplaintTask> complaintTasks;

	// bi-directional many-to-one association to GrievanceCommittee
	@ManyToOne
	@JoinColumn(name = "fk_assigned_committee_id")
	private GrievanceCommittee grievanceCommittee;

	// bi-directional many-to-one association to ComplaintsList
	@ManyToOne
	@JoinColumn(name = "fk_complaint_list_id")
	private ComplaintsList complaintsList;

	// bi-directional many-to-one association to ComplaintsWf
	@OneToMany(mappedBy = "complaint",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ComplaintsWf> complaintsWfs;

	public Complaint() {
	}

	public Long getComplaintId() {
		return complaintId;
	}

	public void setComplaintId(Long complaintId) {
		this.complaintId = complaintId;
	}

	public String getAreaOfGrievance() {
		return areaOfGrievance;
	}

	public void setAreaOfGrievance(String areaOfGrievance) {
		this.areaOfGrievance = areaOfGrievance;
	}

	public String getCausOfDissatisfaction() {
		return causOfDissatisfaction;
	}

	public void setCausOfDissatisfaction(String causOfDissatisfaction) {
		this.causOfDissatisfaction = causOfDissatisfaction;
	}

	public String getClosingComments() {
		return closingComments;
	}

	public void setClosingComments(String closingComments) {
		this.closingComments = closingComments;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getComplainDate() {
		return complainDate;
	}

	public void setComplainDate(Date complainDate) {
		this.complainDate = complainDate;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getDurationOfIncident() {
		return durationOfIncident;
	}

	public void setDurationOfIncident(Date durationOfIncident) {
		this.durationOfIncident = durationOfIncident;
	}

	public String getDurationOfIncidentText() {
		return durationOfIncidentText;
	}

	public void setDurationOfIncidentText(String durationOfIncidentText) {
		this.durationOfIncidentText = durationOfIncidentText;
	}

	public EmployeeDetail getAckEmp() {
		return ackEmp;
	}

	public void setAckEmp(EmployeeDetail ackEmp) {
		this.ackEmp = ackEmp;
	}

	public EmployeeDetail getClosedEmp() {
		return closedEmp;
	}

	public void setClosedEmp(EmployeeDetail closedEmp) {
		this.closedEmp = closedEmp;
	}

	public Long getExistingComplaint() {
		return existingComplaint;
	}

	public void setExistingComplaint(Long existingComplaint) {
		this.existingComplaint = existingComplaint;
	}

	public GeneralDetail getGrvOnCatdet() {
		return grvOnCatdet;
	}

	public void setGrvOnCatdet(GeneralDetail grvOnCatdet) {
		this.grvOnCatdet = grvOnCatdet;
	}

	public GeneralDetail getGrvTypeCatdet() {
		return grvTypeCatdet;
	}

	public void setGrvTypeCatdet(GeneralDetail grvTypeCatdet) {
		this.grvTypeCatdet = grvTypeCatdet;
	}

	public Long getGrvOnResourse() {
		return grvOnResourse;
	}

	public void setGrvOnResourse(Long grvOnResourse) {
		this.grvOnResourse = grvOnResourse;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public GeneralDetail getPriorityCatdet() {
		return priorityCatdet;
	}

	public void setPriorityCatdet(GeneralDetail priorityCatdet) {
		this.priorityCatdet = priorityCatdet;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public WorkflowStage getWorkflowStage() {
		return workflowStage;
	}

	public void setWorkflowStage(WorkflowStage workflowStage) {
		this.workflowStage = workflowStage;
	}

	public String getIncident() {
		return incident;
	}

	public void setIncident(String incident) {
		this.incident = incident;
	}

	public String getIncidentDescription() {
		return incidentDescription;
	}

	public void setIncidentDescription(String incidentDescription) {
		this.incidentDescription = incidentDescription;
	}

	public Boolean getIsAcknowledged() {
		return isAcknowledged;
	}

	public void setIsAcknowledged(Boolean isAcknowledged) {
		this.isAcknowledged = isAcknowledged;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsAlreadyReported() {
		return isAlreadyReported;
	}

	public void setIsAlreadyReported(Boolean isAlreadyReported) {
		this.isAlreadyReported = isAlreadyReported;
	}

	public Boolean getIsClosed() {
		return isClosed;
	}

	public void setIsClosed(Boolean isClosed) {
		this.isClosed = isClosed;
	}

	public String getOtherText() {
		return otherText;
	}

	public void setOtherText(String otherText) {
		this.otherText = otherText;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRemedySeeking() {
		return remedySeeking;
	}

	public void setRemedySeeking(String remedySeeking) {
		this.remedySeeking = remedySeeking;
	}

	public Long getStudentExperience() {
		return studentExperience;
	}

	public void setStudentExperience(Long studentExperience) {
		this.studentExperience = studentExperience;
	}

	public String getStudentFeedback() {
		return studentFeedback;
	}

	public void setStudentFeedback(String studentFeedback) {
		this.studentFeedback = studentFeedback;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getWfComments() {
		return wfComments;
	}

	public void setWfComments(String wfComments) {
		this.wfComments = wfComments;
	}

	public List<ComplaintDetail> getComplaintDetails() {
		return complaintDetails;
	}

	public void setComplaintDetails(List<ComplaintDetail> complaintDetails) {
		this.complaintDetails = complaintDetails;
	}

	public List<ComplaintTask> getComplaintTasks() {
		return complaintTasks;
	}

	public void setComplaintTasks(List<ComplaintTask> complaintTasks) {
		this.complaintTasks = complaintTasks;
	}

	public GrievanceCommittee getGrievanceCommittee() {
		return grievanceCommittee;
	}

	public void setGrievanceCommittee(GrievanceCommittee grievanceCommittee) {
		this.grievanceCommittee = grievanceCommittee;
	}

	public ComplaintsList getComplaintsList() {
		return complaintsList;
	}

	public void setComplaintsList(ComplaintsList complaintsList) {
		this.complaintsList = complaintsList;
	}

	public List<ComplaintsWf> getComplaintsWfs() {
		return complaintsWfs;
	}

	public void setComplaintsWfs(List<ComplaintsWf> complaintsWfs) {
		this.complaintsWfs = complaintsWfs;
	}

	public String getComplaintDocPath() {
		return complaintDocPath;
	}

	public void setComplaintDocPath(String complaintDocPath) {
		this.complaintDocPath = complaintDocPath;
	}
}