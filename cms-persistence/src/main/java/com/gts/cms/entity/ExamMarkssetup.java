package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the t_exam_markssetup database table.
 * 
 */
@Entity
@Table(name = "t_exam_markssetup")
@NamedQuery(name = "ExamMarkssetup.findAll", query = "SELECT e FROM ExamMarkssetup e")
public class ExamMarkssetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_markssetup_id")
	private Long markssetupId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "external_marks")
	private Integer externalMarks;

	@Column(name = "external_pass_percentage")
	private BigDecimal externalPassPercentage;

	@Column(name = "internal_marks")
	private Integer internalMarks;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "marks_setup_name")
	private String marksSetupName;

	@Column(name = "pass_percentage")
	private BigDecimal passPercentage;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to Course
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_id")
	private Course course;



	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_subjecttype_catdet_id")
	private GeneralDetail subjectTypeCat;

	public ExamMarkssetup() {
	}

	public Long getMarkssetupId() {
		return this.markssetupId;
	}

	public void setMarkssetupId(Long markssetupId) {
		this.markssetupId = markssetupId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Integer getExternalMarks() {
		return this.externalMarks;
	}

	public void setExternalMarks(Integer externalMarks) {
		this.externalMarks = externalMarks;
	}

	public BigDecimal getExternalPassPercentage() {
		return this.externalPassPercentage;
	}

	public void setExternalPassPercentage(BigDecimal externalPassPercentage) {
		this.externalPassPercentage = externalPassPercentage;
	}

	public Integer getInternalMarks() {
		return this.internalMarks;
	}

	public void setInternalMarks(Integer internalMarks) {
		this.internalMarks = internalMarks;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getMarksSetupName() {
		return this.marksSetupName;
	}

	public void setMarksSetupName(String marksSetupName) {
		this.marksSetupName = marksSetupName;
	}

	public BigDecimal getPassPercentage() {
		return this.passPercentage;
	}

	public void setPassPercentage(BigDecimal passPercentage) {
		this.passPercentage = passPercentage;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}


	public GeneralDetail getSubjectTypeCat() {
		return subjectTypeCat;
	}

	public void setSubjectTypeCat(GeneralDetail subjectTypeCat) {
		this.subjectTypeCat = subjectTypeCat;
	}

}