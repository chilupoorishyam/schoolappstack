package com.gts.cms.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "t_dl_questions_stg")
@Setter
@Getter
public class QuestionStg implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_dl_question_id")
    private Long subjectId;
    @Column(name = "question")
    private String question;
    @Column(name = "inputtype_catdet_id")
    private Long inputTypeCatdetId;
    @Column(name = "option1")
    private String option1;
    @Column(name = "option2")
    private String option2;
    @Column(name = "option3")
    private String option3;
    @Column(name = "option4")
    private String option4;
    @Column(name = "option5")
    private String option5;
    @Column(name = "option6")
    private String option6;
    @Column(name = "answer")
    private String answer;
    @Column(name = "keyword")
    private String keyword;
    @Column(name = "explanation")
    private String explanation;


}