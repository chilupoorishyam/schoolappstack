package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_counselor_activity_types database table.
 * 
 */
@Entity
@Table(name="t_m_counselor_activity_types")
@NamedQuery(name="CounselorActivityType.findAll", query="SELECT c FROM CounselorActivityType c")
public class CounselorActivityType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_counselor_activity_type_id")
	private Long counselorActivityTypeId;

	@Column(name="activity_type_code")
	private String activityTypeCode;

	@Column(name="activity_type_name")
	private String activityTypeName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to CounselorActivity
	@OneToMany(mappedBy="counselorActivityType")
	private List<CounselorActivity> counselorActivities;

	//bi-directional many-to-one association to School
	@ManyToOne
	@JoinColumn(name="fk_school_id")
	private School school;

	public CounselorActivityType() {
	}

	public Long getCounselorActivityTypeId() {
		return this.counselorActivityTypeId;
	}

	public void setCounselorActivityTypeId(Long counselorActivityTypeId) {
		this.counselorActivityTypeId = counselorActivityTypeId;
	}

	public String getActivityTypeCode() {
		return this.activityTypeCode;
	}

	public void setActivityTypeCode(String activityTypeCode) {
		this.activityTypeCode = activityTypeCode;
	}

	public String getActivityTypeName() {
		return this.activityTypeName;
	}

	public void setActivityTypeName(String activityTypeName) {
		this.activityTypeName = activityTypeName;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<CounselorActivity> getCounselorActivities() {
		return this.counselorActivities;
	}

	public void setCounselorActivities(List<CounselorActivity> counselorActivities) {
		this.counselorActivities = counselorActivities;
	}

	public CounselorActivity addCounselorActivity(CounselorActivity counselorActivity) {
		getCounselorActivities().add(counselorActivity);
		counselorActivity.setCounselorActivityType(this);

		return counselorActivity;
	}

	public CounselorActivity removeCounselorActivity(CounselorActivity counselorActivity) {
		getCounselorActivities().remove(counselorActivity);
		counselorActivity.setCounselorActivityType(null);

		return counselorActivity;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

}