package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_prj_academic_projects database table.
 */
@Entity
@Table(name = "t_prj_academic_projects")
public class AcademicProject implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_academic_project_id")
    private Long academicProjectId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_school_id")
    private School schoolId;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_course_year_id")
    private CourseYear courseYear;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_academic_year_id")
    private AcademicYear academicYear;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_projecttype_catdet_id")
    private GeneralDetail projecttypeCatdetId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "schedule_start_date")
    private Date scheduleStartDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "schedule_end_date")
    private Date scheduleEndDate;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.DATE)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    public Long getAcademicProjectId() {
        return academicProjectId;
    }

    public void setAcademicProjectId(Long academicProjectId) {
        this.academicProjectId = academicProjectId;
    }

    public School getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(School schoolId) {
        this.schoolId = schoolId;
    }


    public CourseYear getCourseYear() {
        return courseYear;
    }

    public void setCourseYear(CourseYear courseYear) {
        this.courseYear = courseYear;
    }

    public AcademicYear getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(AcademicYear academicYear) {
        this.academicYear = academicYear;
    }

    public GeneralDetail getProjecttypeCatdetId() {
        return projecttypeCatdetId;
    }

    public void setProjecttypeCatdetId(GeneralDetail projecttypeCatdetId) {
        this.projecttypeCatdetId = projecttypeCatdetId;
    }

    public Date getScheduleStartDate() {
        return scheduleStartDate;
    }

    public void setScheduleStartDate(Date scheduleStartDate) {
        this.scheduleStartDate = scheduleStartDate;
    }

    public Date getScheduleEndDate() {
        return scheduleEndDate;
    }

    public void setScheduleEndDate(Date scheduleEndDate) {
        this.scheduleEndDate = scheduleEndDate;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    public Long getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(Long createdUser) {
        this.createdUser = createdUser;
    }

    public Date getUpdatedDt() {
        return updatedDt;
    }

    public void setUpdatedDt(Date updatedDt) {
        this.updatedDt = updatedDt;
    }

    public Long getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(Long updatedUser) {
        this.updatedUser = updatedUser;
    }
}