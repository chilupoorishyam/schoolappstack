package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_fee_receipts database table.
 */
@Data
@Entity
@Table(name = "t_stg_onl_fee_receipts")
@NamedQuery(name = "StgOnlineFeeReceipt.findAll", query = "SELECT f FROM StgOnlineFeeReceipt f")
public class StgOnlineFeeReceipt implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_onl_fee_receipts_id")
    private Long feeReceiptsId;

    @Column(name = "cheque_no")
    private String chequeNo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    private String ddno;

    @Column(name = "fk_hstl_fee_payment_id")
    private Long hstlFeePaymentId;

    @Column(name = "fk_tm_fee_payment_id")
    private Long tmFeePaymentId;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "is_reverted")
    private Boolean isReverted;

    @Column(name = "other_payment_number")
    private String otherPaymentNumber;

    @Column(name = "payer_name")
    private String payerName;

    @Column(name = "payment_for")
    private String paymentFor;

    @Column(name = "order_id")
    private Long orderId;

    @Column(name = "payment_receipts_no")
    private String paymentReceiptsNo;

    private String reason;

    @Column(name = "receipt_amount")
    private BigDecimal receiptAmount;

    @Column(name = "reference_number")
    private String referenceNumber;

    @Column(name = "revert_reason")
    private String revertReason;

    @Column(name = "transaction_no")
    private String transactionNo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "receipt_date")
    private Date receiptDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    // bi-directional many-to-one association to EmployeeDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_emp_id")
    private EmployeeDetail employeeDetail;

    // bi-directional many-to-one association to EmployeeDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_revert_by_emp_id")
    private EmployeeDetail revertbByEmployeeDetail;

    // bi-directional many-to-one association to FeeInstantCategory
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_fee_instant_category_id")
    private FeeInstantCategory feeInstantCategory;

    // bi-directional many-to-one association to FeeInstantPayment
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_instant_payment_id")
    private FeeInstantPayment feeInstantPayment;

    @OneToMany(mappedBy = "feeReceipt", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<StgOnlineFeeParticularwisePayment> feeParticularwisePayments;


    @OneToMany(mappedBy = "feeReceipt", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<FeeStudentRefund> feeStudentRefund;

    // bi-directional many-to-one association to FeeStructure
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_fee_structure_id")
    private FeeStructure feeStructure;

    // bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_school_id")
    private School school;

    // bi-directional many-to-one association to GeneralDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_payer_type_catdet_id")
    private GeneralDetail payerType;

    // bi-directional many-to-one association to GeneralDetail
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fk_payment_type_catdet_id")
    private GeneralDetail paymentType;

    // bi-directional many-to-one association to GeneralDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_payment_mode_catdet_id")
    private GeneralDetail paymentMode;

    // bi-directional many-to-one association to StudentDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_student_id")
    private StudentDetail studentDetail;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_financial_year_id")
    private FinancialYear financialYear;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fk_academic_year_id")
    private AcademicYear academicYear;
}