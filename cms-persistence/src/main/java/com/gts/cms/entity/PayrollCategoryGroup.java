package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_hr_payroll_category_group database table.
 * 
 */
@Entity
@Table(name="t_hr_payroll_category_group")
@NamedQuery(name="PayrollCategoryGroup.findAll", query="SELECT p FROM PayrollCategoryGroup p")
public class PayrollCategoryGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_payroll_category_group_id")
	private Long payrollCategoryGroupId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="include_in_payroll")
	private Boolean includeInPayroll;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="lop_condition_value")
	private String lopConditionValue;

	private String reason;

	@Column(name="sort_order")
	private Integer sortOrder;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeePayslipDetail
	@OneToMany(mappedBy="payrollCategoryGroup",fetch = FetchType.LAZY)
	private List<EmployeePayslipDetail> employeePayslipDetails;

	//bi-directional many-to-one association to EmployeeSalaryStructure
	@OneToMany(mappedBy="payrollCategoryGroup",fetch = FetchType.LAZY)
	private List<EmployeeSalaryStructure> employeeSalaryStructures;

	//bi-directional many-to-one association to PayrollCategory
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_payroll_category_id")
	private PayrollCategory payrollCategory;

	//bi-directional many-to-one association to PayrollGroup
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_payroll_group_id")
	private PayrollGroup payrollGroup;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	public PayrollCategoryGroup() {
	}

	public Long getPayrollCategoryGroupId() {
		return this.payrollCategoryGroupId;
	}

	public void setPayrollCategoryGroupId(Long payrollCategoryGroupId) {
		this.payrollCategoryGroupId = payrollCategoryGroupId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIncludeInPayroll() {
		return this.includeInPayroll;
	}

	public void setIncludeInPayroll(Boolean includeInPayroll) {
		this.includeInPayroll = includeInPayroll;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getLopConditionValue() {
		return this.lopConditionValue;
	}

	public void setLopConditionValue(String lopConditionValue) {
		this.lopConditionValue = lopConditionValue;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<EmployeePayslipDetail> getEmployeePayslipDetails() {
		return this.employeePayslipDetails;
	}

	public void setEmployeePayslipDetails(List<EmployeePayslipDetail> employeePayslipDetails) {
		this.employeePayslipDetails = employeePayslipDetails;
	}

	public EmployeePayslipDetail addEmployeePayslipDetail(EmployeePayslipDetail employeePayslipDetail) {
		getEmployeePayslipDetails().add(employeePayslipDetail);
		employeePayslipDetail.setPayrollCategoryGroup(this);

		return employeePayslipDetail;
	}

	public EmployeePayslipDetail removeEmployeePayslipDetail(EmployeePayslipDetail employeePayslipDetail) {
		getEmployeePayslipDetails().remove(employeePayslipDetail);
		employeePayslipDetail.setPayrollCategoryGroup(null);

		return employeePayslipDetail;
	}

	public List<EmployeeSalaryStructure> getEmployeeSalaryStructures() {
		return this.employeeSalaryStructures;
	}

	public void setEmployeeSalaryStructures(List<EmployeeSalaryStructure> employeeSalaryStructures) {
		this.employeeSalaryStructures = employeeSalaryStructures;
	}

	public EmployeeSalaryStructure addEmployeeSalaryStructure(EmployeeSalaryStructure employeeSalaryStructure) {
		getEmployeeSalaryStructures().add(employeeSalaryStructure);
		employeeSalaryStructure.setPayrollCategoryGroup(this);

		return employeeSalaryStructure;
	}

	public EmployeeSalaryStructure removeEmployeeSalaryStructure(EmployeeSalaryStructure employeeSalaryStructure) {
		getEmployeeSalaryStructures().remove(employeeSalaryStructure);
		employeeSalaryStructure.setPayrollCategoryGroup(null);

		return employeeSalaryStructure;
	}

	public PayrollCategory getPayrollCategory() {
		return this.payrollCategory;
	}

	public void setPayrollCategory(PayrollCategory payrollCategory) {
		this.payrollCategory = payrollCategory;
	}

	public PayrollGroup getPayrollGroup() {
		return this.payrollGroup;
	}

	public void setPayrollGroup(PayrollGroup payrollGroup) {
		this.payrollGroup = payrollGroup;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

}