package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_fee_student_data_particulars database table.
 * 
 */
@Entity
@Table(name="t_fee_student_data_particulars")
@NamedQuery(name="FeeStudentDataParticular.findAll", query="SELECT f FROM FeeStudentDataParticular f")
public class FeeStudentDataParticular implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_std_data_particulars_id")
	private Long feeStdDataParticularsId;

	@Column(name="balance_amount")
	private BigDecimal balanceAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="discount_amount")
	private BigDecimal discountAmount;

	@Column(name="fine_amount")
	private BigDecimal fineAmount;

	@Column(name="gross_amount")
	private BigDecimal grossAmount;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_from_stdwise")
	private Boolean isFromStdwise;

	@Column(name="is_from_structure")
	private Boolean isFromStructure;

	@Column(name="net_amount")
	private BigDecimal netAmount;

	@Column(name="paid_amount")
	private BigDecimal paidAmount;

	private String reason;

	@Column(name="ref_id")
	private String refId;

	@Column(name="refund_amount")
	private BigDecimal refundAmount;

	@Column(name="scholarship_amount")
	private BigDecimal scholarshipAmount;

	private BigDecimal scholarshipHoldAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FeeConvenorStdRemittance
	@OneToMany(mappedBy="feeStudentDataParticular",fetch = FetchType.LAZY)
	private List<FeeConvenorStdRemittance> feeConvenorStdRemittances;

	//bi-directional many-to-one association to FeeManagmentStdDetail
	@OneToMany(mappedBy="feeStudentDataParticular",fetch = FetchType.LAZY)
	private List<FeeManagmentStdDetail> feeManagmentStdDetails;

	//bi-directional many-to-one association to FeeParticularwisePayment
	@OneToMany(mappedBy="feeStudentDataParticular",fetch = FetchType.LAZY)
	private List<FeeParticularwisePayment> feeParticularwisePayments;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_student_id")
	private StudentDetail studentDetail;

	//bi-directional many-to-one association to FeeStructure
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_structure_id")
	private FeeStructure feeStructure;

	//bi-directional many-to-one association to FeeCategory
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_category_id")
	private FeeCategory feeCategory;

	//bi-directional many-to-one association to FeeParticular
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_particulars_id")
	private FeeParticular feeParticular;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_payment_status_catdet_id")
	private GeneralDetail paymentStatus;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_pay_schedule_catdet_id")
	private GeneralDetail paySchedule;

	public FeeStudentDataParticular() {
	}

	public Long getFeeStdDataParticularsId() {
		return this.feeStdDataParticularsId;
	}

	public void setFeeStdDataParticularsId(Long feeStdDataParticularsId) {
		this.feeStdDataParticularsId = feeStdDataParticularsId;
	}

	public BigDecimal getBalanceAmount() {
		return this.balanceAmount;
	}

	public void setBalanceAmount(BigDecimal balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public BigDecimal getDiscountAmount() {
		return this.discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public BigDecimal getFineAmount() {
		return this.fineAmount;
	}

	public void setFineAmount(BigDecimal fineAmount) {
		this.fineAmount = fineAmount;
	}

	public BigDecimal getGrossAmount() {
		return this.grossAmount;
	}

	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsFromStdwise() {
		return this.isFromStdwise;
	}

	public void setIsFromStdwise(Boolean isFromStdwise) {
		this.isFromStdwise = isFromStdwise;
	}

	public Boolean getIsFromStructure() {
		return this.isFromStructure;
	}

	public void setIsFromStructure(Boolean isFromStructure) {
		this.isFromStructure = isFromStructure;
	}

	public BigDecimal getNetAmount() {
		return this.netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRefId() {
		return this.refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public BigDecimal getRefundAmount() {
		return this.refundAmount;
	}

	public void setRefundAmount(BigDecimal refundAmount) {
		this.refundAmount = refundAmount;
	}

	public BigDecimal getScholarshipAmount() {
		return this.scholarshipAmount;
	}

	public void setScholarshipAmount(BigDecimal scholarshipAmount) {
		this.scholarshipAmount = scholarshipAmount;
	}
	
	public BigDecimal getScholarshipHoldAmount() {
		return scholarshipHoldAmount;
	}

	public void setScholarshipHoldAmount(BigDecimal scholarshipHoldAmount) {
		this.scholarshipHoldAmount = scholarshipHoldAmount;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<FeeConvenorStdRemittance> getFeeConvenorStdRemittances() {
		return this.feeConvenorStdRemittances;
	}

	public void setFeeConvenorStdRemittances(List<FeeConvenorStdRemittance> feeConvenorStdRemittances) {
		this.feeConvenorStdRemittances = feeConvenorStdRemittances;
	}

	public FeeConvenorStdRemittance addFeeConvenorStdRemittance(FeeConvenorStdRemittance feeConvenorStdRemittance) {
		getFeeConvenorStdRemittances().add(feeConvenorStdRemittance);
		feeConvenorStdRemittance.setFeeStudentDataParticular(this);

		return feeConvenorStdRemittance;
	}

	public FeeConvenorStdRemittance removeFeeConvenorStdRemittance(FeeConvenorStdRemittance feeConvenorStdRemittance) {
		getFeeConvenorStdRemittances().remove(feeConvenorStdRemittance);
		feeConvenorStdRemittance.setFeeStudentDataParticular(null);

		return feeConvenorStdRemittance;
	}

	public List<FeeManagmentStdDetail> getFeeManagmentStdDetails() {
		return this.feeManagmentStdDetails;
	}

	public void setFeeManagmentStdDetails(List<FeeManagmentStdDetail> feeManagmentStdDetails) {
		this.feeManagmentStdDetails = feeManagmentStdDetails;
	}

	public FeeManagmentStdDetail addFeeManagmentStdDetail(FeeManagmentStdDetail feeManagmentStdDetail) {
		getFeeManagmentStdDetails().add(feeManagmentStdDetail);
		feeManagmentStdDetail.setFeeStudentDataParticular(this);

		return feeManagmentStdDetail;
	}

	public FeeManagmentStdDetail removeFeeManagmentStdDetail(FeeManagmentStdDetail feeManagmentStdDetail) {
		getFeeManagmentStdDetails().remove(feeManagmentStdDetail);
		feeManagmentStdDetail.setFeeStudentDataParticular(null);

		return feeManagmentStdDetail;
	}

	public List<FeeParticularwisePayment> getFeeParticularwisePayments() {
		return this.feeParticularwisePayments;
	}

	public void setFeeParticularwisePayments(List<FeeParticularwisePayment> feeParticularwisePayments) {
		this.feeParticularwisePayments = feeParticularwisePayments;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public FeeStructure getFeeStructure() {
		return this.feeStructure;
	}

	public void setFeeStructure(FeeStructure feeStructure) {
		this.feeStructure = feeStructure;
	}

	public FeeCategory getFeeCategory() {
		return this.feeCategory;
	}

	public void setFeeCategory(FeeCategory feeCategory) {
		this.feeCategory = feeCategory;
	}

	public FeeParticular getFeeParticular() {
		return this.feeParticular;
	}

	public void setFeeParticular(FeeParticular feeParticular) {
		this.feeParticular = feeParticular;
	}

	public GeneralDetail getPaymentStatus() {
		return this.paymentStatus;
	}

	public void setPaymentStatus(GeneralDetail paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public GeneralDetail getPaySchedule() {
		return this.paySchedule;
	}

	public void setPaySchedule(GeneralDetail paySchedule) {
		this.paySchedule = paySchedule;
	}

}