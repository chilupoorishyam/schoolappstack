package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_exam_fcar_subject_syllabus database table.
 * 
 */
@Entity
@Table(name = "t_exam_fcar_subject_syllabus")
@NamedQuery(name = "ExamFCARSubjectSyllabus.findAll", query = "SELECT e FROM ExamFCARSubjectSyllabus e")
public class ExamFCARSubjectSyllabus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_fcar_sub_syllabus_id")
	private Long examFCARSubSyllabusId;

	private String comments;

	@Temporal(TemporalType.DATE)
	@Column(name = "configured_on")
	private Date configuredOn;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne
	@JoinColumn(name = "fk_school_id")
	private School school;

	@ManyToOne
	@JoinColumn(name = "fk_config_emp_id")
	private EmployeeDetail configEmp;

	@ManyToOne
	@JoinColumn(name = "fk_course_year_id")
	private CourseYear courseYear;



	@ManyToOne
	@JoinColumn(name = "fk_group_section_id")
	private GroupSection groupSection;

	@ManyToOne
	@JoinColumn(name = "fk_exam_id")
	private ExamMaster examMaster;

	@Column(name = "fk_sub_unit_topic_ids")
	private String subUnitTopicIds;

	@ManyToOne
	@JoinColumn(name = "fk_sub_units_id")
	private SubjectUnit subUnits;

	@ManyToOne
	@JoinColumn(name = "fk_subject_id")
	private Subject subject;

	@ManyToOne
	@JoinColumn(name = "fk_course_outcome_catdet_id")
	private GeneralDetail courseOutcomeCatdetId;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to ExamFCARSetupDetail
	@ManyToOne
	@JoinColumn(name = "fk_exam_fcar_set_det_id")
	private ExamFCARSetupDetail examFcarSetupDetail;

	public ExamFCARSubjectSyllabus() {
	}

	public Long getExamFCARSubSyllabusId() {
		return this.examFCARSubSyllabusId;
	}

	public void setExamFCARSubSyllabusId(Long examFCARSubSyllabusId) {
		this.examFCARSubSyllabusId = examFCARSubSyllabusId;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getConfiguredOn() {
		return this.configuredOn;
	}

	public void setConfiguredOn(Date configuredOn) {
		this.configuredOn = configuredOn;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public EmployeeDetail getConfigEmp() {
		return configEmp;
	}

	public void setConfigEmp(EmployeeDetail configEmp) {
		this.configEmp = configEmp;
	}

	public CourseYear getCourseYear() {
		return courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public ExamMaster getExamMaster() {
		return examMaster;
	}

	public void setExamMaster(ExamMaster examMaster) {
		this.examMaster = examMaster;
	}

	public String getSubUnitTopicIds() {
		return subUnitTopicIds;
	}

	public void setSubUnitTopicIds(String subUnitTopicIds) {
		this.subUnitTopicIds = subUnitTopicIds;
	}

	public SubjectUnit getSubUnits() {
		return subUnits;
	}

	public void setSubUnits(SubjectUnit subUnits) {
		this.subUnits = subUnits;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public ExamFCARSetupDetail getExamFcarSetupDetail() {
		return examFcarSetupDetail;
	}

	public void setExamFcarSetupDetail(ExamFCARSetupDetail examFcarSetupDetail) {
		this.examFcarSetupDetail = examFcarSetupDetail;
	}



	public GroupSection getGroupSection() {
		return groupSection;
	}

	public void setGroupSection(GroupSection groupSection) {
		this.groupSection = groupSection;
	}

	public Boolean getActive() {
		return isActive;
	}

	public void setActive(Boolean active) {
		isActive = active;
	}

	public GeneralDetail getCourseOutcomeCatdetId() {
		return courseOutcomeCatdetId;
	}

	public void setCourseOutcomeCatdetId(GeneralDetail courseOutcomeCatdetId) {
		this.courseOutcomeCatdetId = courseOutcomeCatdetId;
	}
}