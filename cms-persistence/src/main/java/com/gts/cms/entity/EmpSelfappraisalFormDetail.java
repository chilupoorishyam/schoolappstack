package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_emp_selfappraisal_form_details database table.
 */
@Data
@Entity
@Table(name = "t_emp_selfappraisal_form_details")
@NamedQuery(name = "EmpSelfappraisalFormDetail.findAll", query = "SELECT c FROM EmpSelfappraisalFormDetail c")
public class EmpSelfappraisalFormDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_selfappraisal_form_det_id")
    private Long selfappraisalFormDetId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "serial_number")
    private Double serialNumber;

    @Column(name = "sub_serial_number")
    private Double subSerialNumber;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "formula")
    private String formula;

    @ManyToOne
    @JoinColumn(name = "fk_school_id")
    private School school;

    @ManyToOne
    @JoinColumn(name = "fk_selfappraisal_form_id")
    private EmpSelfappraisalForm selfappraisalFormId;

    @ManyToOne
    @JoinColumn(name = "fk_formpoint_catdet_id")
    private GeneralDetail formpointCatdetId;

    @ManyToOne
    @JoinColumn(name = "fk_inputtype_catdet_id")
    private GeneralDetail inputtypeCatdetId;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;
}