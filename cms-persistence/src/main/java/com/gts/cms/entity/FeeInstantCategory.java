package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_fee_instant_category database table.
 * 
 */
@Entity
@Table(name="t_fee_instant_category")
@NamedQuery(name="FeeInstantCategory.findAll", query="SELECT f FROM FeeInstantCategory f")
public class FeeInstantCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_instant_category_id")
	private Long feeInstantCategoryId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String description;

	@Column(name="instant_category_name")
	private String instantCategoryName;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to FeeInstantParticular
	@OneToMany(mappedBy="feeInstantCategory",fetch = FetchType.LAZY)
	private List<FeeInstantParticular> feeInstantParticulars;

	//bi-directional many-to-one association to FeeInstantPaymentParticular
	@OneToMany(mappedBy="feeInstantCategory",fetch = FetchType.LAZY)
	private List<FeeInstantPaymentParticular> feeInstantPaymentParticulars;

	//bi-directional many-to-one association to FeeReceipt
	@OneToMany(mappedBy="feeInstantCategory",fetch = FetchType.LAZY)
	private List<FeeReceipt> feeReceipts;

	public FeeInstantCategory() {
	}

	public Long getFeeInstantCategoryId() {
		return this.feeInstantCategoryId;
	}

	public void setFeeInstantCategoryId(Long feeInstantCategoryId) {
		this.feeInstantCategoryId = feeInstantCategoryId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getInstantCategoryName() {
		return this.instantCategoryName;
	}

	public void setInstantCategoryName(String instantCategoryName) {
		this.instantCategoryName = instantCategoryName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<FeeInstantParticular> getFeeInstantParticulars() {
		return this.feeInstantParticulars;
	}

	public void setFeeInstantParticulars(List<FeeInstantParticular> feeInstantParticulars) {
		this.feeInstantParticulars = feeInstantParticulars;
	}

	public FeeInstantParticular addFeeInstantParticular(FeeInstantParticular feeInstantParticular) {
		getFeeInstantParticulars().add(feeInstantParticular);
		feeInstantParticular.setFeeInstantCategory(this);

		return feeInstantParticular;
	}

	public FeeInstantParticular removeFeeInstantParticular(FeeInstantParticular feeInstantParticular) {
		getFeeInstantParticulars().remove(feeInstantParticular);
		feeInstantParticular.setFeeInstantCategory(null);

		return feeInstantParticular;
	}

	public List<FeeInstantPaymentParticular> getFeeInstantPaymentParticulars() {
		return this.feeInstantPaymentParticulars;
	}

	public void setFeeInstantPaymentParticulars(List<FeeInstantPaymentParticular> feeInstantPaymentParticulars) {
		this.feeInstantPaymentParticulars = feeInstantPaymentParticulars;
	}

	public FeeInstantPaymentParticular addFeeInstantPaymentParticular(FeeInstantPaymentParticular feeInstantPaymentParticular) {
		getFeeInstantPaymentParticulars().add(feeInstantPaymentParticular);
		feeInstantPaymentParticular.setFeeInstantCategory(this);

		return feeInstantPaymentParticular;
	}

	public FeeInstantPaymentParticular removeFeeInstantPaymentParticular(FeeInstantPaymentParticular feeInstantPaymentParticular) {
		getFeeInstantPaymentParticulars().remove(feeInstantPaymentParticular);
		feeInstantPaymentParticular.setFeeInstantCategory(null);

		return feeInstantPaymentParticular;
	}

	public List<FeeReceipt> getFeeReceipts() {
		return this.feeReceipts;
	}

	public void setFeeReceipts(List<FeeReceipt> feeReceipts) {
		this.feeReceipts = feeReceipts;
	}

	public FeeReceipt addFeeReceipt(FeeReceipt feeReceipt) {
		getFeeReceipts().add(feeReceipt);
		feeReceipt.setFeeInstantCategory(this);

		return feeReceipt;
	}

	public FeeReceipt removeFeeReceipt(FeeReceipt feeReceipt) {
		getFeeReceipts().remove(feeReceipt);
		feeReceipt.setFeeInstantCategory(null);

		return feeReceipt;
	}

}