package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the t_std_referral_details database table.
 * 
 */
@Entity
@Table(name="t_std_referral_details")
@NamedQuery(name="ReferralDetail.findAll", query="SELECT r FROM ReferralDetail r")
public class ReferralDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_std_referral_det_id")
	private Long referralDetailId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_transaction_settled")
	private Boolean isTransactionSettled;

	private Boolean isstudentadmitted;

	private String notes;

	@Column(name="paid_amount")
	private BigDecimal paidAmount;

	private String reason;

	@Column(name="referral_amount")
	private BigDecimal referralAmount;

	@Column(name="status_comments")
	private String statusComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="status_date")
	private Date statusDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="transaction_completed_date")
	private Date transactionCompletedDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_id")
	private EmployeeDetail employeeDetail;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_status_emp_id")
	private EmployeeDetail statusEmployeeDetail;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_paid_emp_id")
	private EmployeeDetail paidEmployeeDetail;

	//bi-directional many-to-one association to FinTransaction
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fin_transaction_id")
	private FinTransaction finTransaction;

	//bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_approval_status_catdet_id")
	private GeneralDetail approvalStatus;

	//bi-directional many-to-one association to Agentdetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_agentdetial_id")
	private Agentdetail agentdetail;

	//bi-directional many-to-one association to StudentEnquiry
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_std_enquiry_id")
	private StudentEnquiry studentEnquiry;

	public ReferralDetail() {
	}

	public Long getReferralDetailId() {
		return this.referralDetailId;
	}

	public void setReferralDetailId(Long referralDetailId) {
		this.referralDetailId = referralDetailId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsTransactionSettled() {
		return this.isTransactionSettled;
	}

	public void setIsTransactionSettled(Boolean isTransactionSettled) {
		this.isTransactionSettled = isTransactionSettled;
	}

	public Boolean getIsstudentadmitted() {
		return this.isstudentadmitted;
	}

	public void setIsstudentadmitted(Boolean isstudentadmitted) {
		this.isstudentadmitted = isstudentadmitted;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getReferralAmount() {
		return this.referralAmount;
	}

	public void setReferralAmount(BigDecimal referralAmount) {
		this.referralAmount = referralAmount;
	}

	public String getStatusComments() {
		return this.statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public Date getStatusDate() {
		return this.statusDate;
	}

	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}

	public Date getTransactionCompletedDate() {
		return this.transactionCompletedDate;
	}

	public void setTransactionCompletedDate(Date transactionCompletedDate) {
		this.transactionCompletedDate = transactionCompletedDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getEmployeeDetail() {
		return this.employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public EmployeeDetail getStatusEmployeeDetail() {
		return this.statusEmployeeDetail;
	}

	public void setStatusEmployeeDetail(EmployeeDetail statusEmployeeDetail) {
		this.statusEmployeeDetail = statusEmployeeDetail;
	}

	public EmployeeDetail getPaidEmployeeDetail() {
		return this.paidEmployeeDetail;
	}

	public void setPaidEmployeeDetail(EmployeeDetail paidEmployeeDetail) {
		this.paidEmployeeDetail = paidEmployeeDetail;
	}

	public FinTransaction getFinTransaction() {
		return this.finTransaction;
	}

	public void setFinTransaction(FinTransaction finTransaction) {
		this.finTransaction = finTransaction;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getApprovalStatus() {
		return this.approvalStatus;
	}

	public void setApprovalStatus(GeneralDetail approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public Agentdetail getAgentdetail() {
		return this.agentdetail;
	}

	public void setAgentdetail(Agentdetail agentdetail) {
		this.agentdetail = agentdetail;
	}

	public StudentEnquiry getStudentEnquiry() {
		return this.studentEnquiry;
	}

	public void setStudentEnquiry(StudentEnquiry studentEnquiry) {
		this.studentEnquiry = studentEnquiry;
	}

}