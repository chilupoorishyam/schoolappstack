package com.gts.cms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "t_sec_authorization")
@NamedQuery(name = "SecurityAuthorization.findAll", query = "SELECT s FROM SecurityAuthorization s")
public class SecurityAuthorization implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_authorization_id")
	private Long authorizationId;
	
	@Column(name = "authorization_key")
	private String authorizationKey;
}
