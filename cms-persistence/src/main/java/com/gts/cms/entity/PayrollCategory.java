package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_hr_payroll_category database table.
 * 
 */
@Entity
@Table(name="t_hr_payroll_category")
@NamedQuery(name="PayrollCategory.findAll", query="SELECT p FROM PayrollCategory p")
public class PayrollCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_payroll_category_id")
	private Long payrollCategoryId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="default_value")
	private String defaultValue;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="payroll_category_code")
	private String payrollCategoryCode;

	@Column(name="payroll_category_name")
	private String payrollCategoryName;

	@Column(name="payroll_category_type")
	private String payrollCategoryType;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	private String value;

	@Column(name="value_type")
	private String valueType;

	//bi-directional many-to-one association to EmployeePayslipDetail
	@OneToMany(mappedBy="payrollCategory",fetch = FetchType.LAZY)
	private List<EmployeePayslipDetail> employeePayslipDetails;

	//bi-directional many-to-one association to EmployeeSalaryStructure
	@OneToMany(mappedBy="payrollCategory",fetch = FetchType.LAZY)
	private List<EmployeeSalaryStructure> employeeSalaryStructures;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to PayrollCategoryGroup
	@OneToMany(mappedBy="payrollCategory",fetch = FetchType.LAZY)
	private List<PayrollCategoryGroup> payrollCategoryGroups;

	public PayrollCategory() {
	}

	public Long getPayrollCategoryId() {
		return this.payrollCategoryId;
	}

	public void setPayrollCategoryId(Long payrollCategoryId) {
		this.payrollCategoryId = payrollCategoryId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getPayrollCategoryCode() {
		return this.payrollCategoryCode;
	}

	public void setPayrollCategoryCode(String payrollCategoryCode) {
		this.payrollCategoryCode = payrollCategoryCode;
	}

	public String getPayrollCategoryName() {
		return this.payrollCategoryName;
	}

	public void setPayrollCategoryName(String payrollCategoryName) {
		this.payrollCategoryName = payrollCategoryName;
	}

	public String getPayrollCategoryType() {
		return this.payrollCategoryType;
	}

	public void setPayrollCategoryType(String payrollCategoryType) {
		this.payrollCategoryType = payrollCategoryType;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValueType() {
		return this.valueType;
	}

	public void setValueType(String valueType) {
		this.valueType = valueType;
	}

	public List<EmployeePayslipDetail> getEmployeePayslipDetails() {
		return this.employeePayslipDetails;
	}

	public void setEmployeePayslipDetails(List<EmployeePayslipDetail> employeePayslipDetails) {
		this.employeePayslipDetails = employeePayslipDetails;
	}

	public EmployeePayslipDetail addEmployeePayslipDetail(EmployeePayslipDetail employeePayslipDetail) {
		getEmployeePayslipDetails().add(employeePayslipDetail);
		employeePayslipDetail.setPayrollCategory(this);

		return employeePayslipDetail;
	}

	public EmployeePayslipDetail removeEmployeePayslipDetail(EmployeePayslipDetail employeePayslipDetail) {
		getEmployeePayslipDetails().remove(employeePayslipDetail);
		employeePayslipDetail.setPayrollCategory(null);

		return employeePayslipDetail;
	}

	public List<EmployeeSalaryStructure> getEmployeeSalaryStructures() {
		return this.employeeSalaryStructures;
	}

	public void setEmployeeSalaryStructures(List<EmployeeSalaryStructure> employeeSalaryStructures) {
		this.employeeSalaryStructures = employeeSalaryStructures;
	}

	public EmployeeSalaryStructure addEmployeeSalaryStructure(EmployeeSalaryStructure employeeSalaryStructure) {
		getEmployeeSalaryStructures().add(employeeSalaryStructure);
		employeeSalaryStructure.setPayrollCategory(this);

		return employeeSalaryStructure;
	}

	public EmployeeSalaryStructure removeEmployeeSalaryStructure(EmployeeSalaryStructure employeeSalaryStructure) {
		getEmployeeSalaryStructures().remove(employeeSalaryStructure);
		employeeSalaryStructure.setPayrollCategory(null);

		return employeeSalaryStructure;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<PayrollCategoryGroup> getPayrollCategoryGroups() {
		return this.payrollCategoryGroups;
	}

	public void setPayrollCategoryGroups(List<PayrollCategoryGroup> payrollCategoryGroups) {
		this.payrollCategoryGroups = payrollCategoryGroups;
	}

	public PayrollCategoryGroup addPayrollCategoryGroup(PayrollCategoryGroup payrollCategoryGroup) {
		getPayrollCategoryGroups().add(payrollCategoryGroup);
		payrollCategoryGroup.setPayrollCategory(this);

		return payrollCategoryGroup;
	}

	public PayrollCategoryGroup removePayrollCategoryGroup(PayrollCategoryGroup payrollCategoryGroup) {
		getPayrollCategoryGroups().remove(payrollCategoryGroup);
		payrollCategoryGroup.setPayrollCategory(null);

		return payrollCategoryGroup;
	}

}