package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_fee_transaction_master database table.
 * 
 */
@Entity
@Table(name="t_fee_transaction_master")
@NamedQuery(name="FeeTransactionMaster.findAll", query="SELECT f FROM FeeTransactionMaster f")
public class FeeTransactionMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_transaction_master_id")
	private Long feeTransactionMasterId;

	@Column(name="applied_discounts")
	private String appliedDiscounts;

	@Column(name="applied_fines")
	private String appliedFines;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="discount_amount")
	private BigDecimal discountAmount;

	@Column(name="discount_reason")
	private String discountReason;

	@Column(name="due_amount")
	private BigDecimal dueAmount;

	@Column(name="fine_amount")
	private BigDecimal fineAmount;

	@Column(name="gross_amount")
	private BigDecimal grossAmount;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="net_amount")
	private BigDecimal netAmount;

	@Column(name="paid_amount")
	private BigDecimal paidAmount;

	@Column(name="payment_notes")
	private String paymentNotes;

	private String reason;

	@Column(name="reference_number")
	private String referenceNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="transaction_date")
	private Date transactionDate;

	@Column(name="transaction_receipt_ids")
	private String transactionReceiptIds;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FeeTransactionDetail
	@OneToMany(mappedBy="feeTransactionMaster",cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	private List<FeeTransactionDetail> feeTransactionDetails;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_collected_emp_id")
	private EmployeeDetail collectedEmployeeDetail;

	//bi-directional many-to-one association to FeeStructure
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_structure_id")
	private FeeStructure feeStructure;

	//bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_payment_mode_catdet_id")
	private GeneralDetail paymentMode;

	//bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_student_id")
	private StudentDetail studentDetail;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_financial_year_id")
	private FinancialYear financialYear;

	public FeeTransactionMaster() {
	}

	public Long getFeeTransactionMasterId() {
		return this.feeTransactionMasterId;
	}

	public void setFeeTransactionMasterId(Long feeTransactionMasterId) {
		this.feeTransactionMasterId = feeTransactionMasterId;
	}

	public String getAppliedDiscounts() {
		return this.appliedDiscounts;
	}

	public void setAppliedDiscounts(String appliedDiscounts) {
		this.appliedDiscounts = appliedDiscounts;
	}

	public String getAppliedFines() {
		return this.appliedFines;
	}

	public void setAppliedFines(String appliedFines) {
		this.appliedFines = appliedFines;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public BigDecimal getDiscountAmount() {
		return this.discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getDiscountReason() {
		return this.discountReason;
	}

	public void setDiscountReason(String discountReason) {
		this.discountReason = discountReason;
	}

	public BigDecimal getDueAmount() {
		return this.dueAmount;
	}

	public void setDueAmount(BigDecimal dueAmount) {
		this.dueAmount = dueAmount;
	}

	public BigDecimal getFineAmount() {
		return this.fineAmount;
	}

	public void setFineAmount(BigDecimal fineAmount) {
		this.fineAmount = fineAmount;
	}

	public BigDecimal getGrossAmount() {
		return this.grossAmount;
	}

	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public BigDecimal getNetAmount() {
		return this.netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getPaymentNotes() {
		return this.paymentNotes;
	}

	public void setPaymentNotes(String paymentNotes) {
		this.paymentNotes = paymentNotes;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReferenceNumber() {
		return this.referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public Date getTransactionDate() {
		return this.transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionReceiptIds() {
		return this.transactionReceiptIds;
	}

	public void setTransactionReceiptIds(String transactionReceiptIds) {
		this.transactionReceiptIds = transactionReceiptIds;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<FeeTransactionDetail> getFeeTransactionDetails() {
		return this.feeTransactionDetails;
	}

	public void setFeeTransactionDetails(List<FeeTransactionDetail> feeTransactionDetails) {
		this.feeTransactionDetails = feeTransactionDetails;
	}

	public FeeTransactionDetail addFeeTransactionDetail(FeeTransactionDetail feeTransactionDetail) {
		getFeeTransactionDetails().add(feeTransactionDetail);
		feeTransactionDetail.setFeeTransactionMaster(this);

		return feeTransactionDetail;
	}

	public FeeTransactionDetail removeFeeTransactionDetail(FeeTransactionDetail feeTransactionDetail) {
		getFeeTransactionDetails().remove(feeTransactionDetail);
		feeTransactionDetail.setFeeTransactionMaster(null);

		return feeTransactionDetail;
	}

	public EmployeeDetail getCollectedEmployeeDetail() {
		return this.collectedEmployeeDetail;
	}

	public void setCollectedEmployeeDetail(EmployeeDetail collectedEmployeeDetail) {
		this.collectedEmployeeDetail = collectedEmployeeDetail;
	}

	public FeeStructure getFeeStructure() {
		return this.feeStructure;
	}

	public void setFeeStructure(FeeStructure feeStructure) {
		this.feeStructure = feeStructure;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getPaymentMode() {
		return this.paymentMode;
	}

	public void setPaymentMode(GeneralDetail paymentMode) {
		this.paymentMode = paymentMode;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public FinancialYear getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}
	

}