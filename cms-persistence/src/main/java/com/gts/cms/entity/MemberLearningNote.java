package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_dl_member_learning_notes database table.
 * 
 */
@Entity
@Table(name = "t_dl_member_learning_notes")
@NamedQuery(name = "MemberLearningNote.findAll", query = "SELECT t FROM MemberLearningNote t")
public class MemberLearningNote implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_dl_member_learning_notes_id")
	private String memberLearningNotesId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	private String description;

	@Column(name = "is_active")
	private Boolean isActive;

	private String label;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to CourseMember
	@ManyToOne
	@JoinColumn(name = "fk_dl_course_member_id")
	private CourseMember courseMember;

	// bi-directional many-to-one association to TDlMember
	@ManyToOne
	@JoinColumn(name = "fk_dl_membership_id")
	private DigitalLibraryMember member;

	// bi-directional many-to-one association to OnlineCourses
	@ManyToOne
	@JoinColumn(name = "fk_dl_onlinecourse_id")
	private OnlineCourses onlineCourses;

	// bi-directional many-to-one association to CourseLessonsTopic
	@ManyToOne
	@JoinColumn(name = "fk_dl_course_lesson_topic_id")
	private CourseLessonsTopic courseLessonsTopic;

	// bi-directional many-to-one association to CourseTopicDetail
	@ManyToOne
	@JoinColumn(name = "fk_dl_course_topic_det_id")
	private CourseTopicDetail courseTopicDetail;

	public String getMemberLearningNotesId() {
		return memberLearningNotesId;
	}

	public void setMemberLearningNotesId(String memberLearningNotesId) {
		this.memberLearningNotesId = memberLearningNotesId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public CourseMember getCourseMember() {
		return courseMember;
	}

	public void setCourseMember(CourseMember courseMember) {
		this.courseMember = courseMember;
	}

	public DigitalLibraryMember getMember() {
		return member;
	}

	public void setMember(DigitalLibraryMember member) {
		this.member = member;
	}

	public OnlineCourses getOnlineCourses() {
		return onlineCourses;
	}

	public void setOnlineCourses(OnlineCourses onlineCourses) {
		this.onlineCourses = onlineCourses;
	}

	public CourseLessonsTopic getCourseLessonsTopic() {
		return courseLessonsTopic;
	}

	public void setCourseLessonsTopic(CourseLessonsTopic courseLessonsTopic) {
		this.courseLessonsTopic = courseLessonsTopic;
	}

	public CourseTopicDetail getCourseTopicDetail() {
		return courseTopicDetail;
	}

	public void setCourseTopicDetail(CourseTopicDetail courseTopicDetail) {
		this.courseTopicDetail = courseTopicDetail;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

}