package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

/**
 * The persistent class for the t_exam_online_students database table.
 */
@Data
@Entity
@Table(name = "t_exam_online_students")
@NamedQuery(name = "ExamOnlineStudent.findAll", query = "SELECT m FROM ExamOnlineStudent m")
public class ExamOnlineStudent implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_exam_online_student_id")
    private Long examOnlineStudentId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "end_time")
    private Time endTime;

    @ManyToOne
    @JoinColumn(name = "fk_school_id")
    private School school;

    @ManyToOne
    @JoinColumn(name = "fk_exam_online_paper_id")
    private ExamOnlinePaper examOnlinePaper;

    @ManyToOne
    @JoinColumn(name = "fk_student_id")
    private StudentDetail studentDetail;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "is_completed")
    private Boolean isCompleted;

    @Column(name = "is_paused")
    private Boolean isPaused;

    private Integer percentage;

    private String reason;

    @Column(name = "remaining_time")
    private Time remainingTime;

    @Column(name = "start_time")
    private Time startTime;

    @Column(name = "total_marks")
    private Integer totalMarks;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;
}