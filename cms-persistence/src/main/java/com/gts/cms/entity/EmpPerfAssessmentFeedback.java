package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "t_hr_emp_perf_assessment_feedback")
@NamedQuery(name="EmpPerfAssessmentFeedback.findAll", query="SELECT e FROM EmpPerfAssessmentFeedback e")
public class EmpPerfAssessmentFeedback implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_assessment_feedback_id", nullable = false)
    private Long assessmentFeedbackId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_academic_year_id")
    private AcademicYear academicYearId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_emp_id")
    private EmployeeDetail empId;

    @Column(name = "feedback_date")
    private LocalDate feedbackDate;

    @Column(name = "overall_rating")
    private Integer overallRating;

    @Column(name = "comments")
    private String comments;

    @Column(name = "is_active", nullable = false)
    private Boolean active;

    @Column(name = "reason")
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt", nullable = false)
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    @OneToMany(mappedBy = "assessmentFeedbackId")
    private List<EmpPerfAssessmentFbDetails> empPerfAssessmentFbDetails;
    @OneToMany(mappedBy = "assessmentQuestionId")
    private List<EmpPerfAssessmentQuestions> empPerfAssessmentQuestions;
    @OneToMany(mappedBy = "assessmentOptionId")
    private List<EmpPerfAssessmentQutnOpts> empPerfAssessmentQutnOpts;
    @OneToMany(mappedBy = "assessmentFeedbackId")
    private List<EmpPerfLevelWorks> empPerfLevelWorks;
    @OneToMany(mappedBy = "assessmentFeedbackId")
    private List<EmpPerfMentorAchievement> empPerfMentorAchievements;
    @OneToMany(mappedBy = "fkAssessmentFeedbackId")
    private List<EmpPerfSubjPasspercentage> empPerfSubjPasspercentages;

    public Long getAssessmentFeedbackId() {
        return assessmentFeedbackId;
    }

    public void setAssessmentFeedbackId(Long assessmentFeedbackId) {
        this.assessmentFeedbackId = assessmentFeedbackId;
    }

    public AcademicYear getAcademicYearId() {
        return academicYearId;
    }

    public void setAcademicYearId(AcademicYear academicYearId) {
        this.academicYearId = academicYearId;
    }

    public EmployeeDetail getEmpId() {
        return empId;
    }

    public void setEmpId(EmployeeDetail empId) {
        this.empId = empId;
    }

    public LocalDate getFeedbackDate() {
        return feedbackDate;
    }

    public void setFeedbackDate(LocalDate feedbackDate) {
        this.feedbackDate = feedbackDate;
    }

    public Integer getOverallRating() {
        return overallRating;
    }

    public void setOverallRating(Integer overallRating) {
        this.overallRating = overallRating;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    public Long getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(Long createdUser) {
        this.createdUser = createdUser;
    }

    public Date getUpdatedDt() {
        return updatedDt;
    }

    public void setUpdatedDt(Date updatedDt) {
        this.updatedDt = updatedDt;
    }

    public Long getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(Long updatedUser) {
        this.updatedUser = updatedUser;
    }

    public List<EmpPerfAssessmentFbDetails> getEmpPerfAssessmentFbDetails() {
        return empPerfAssessmentFbDetails;
    }

    public void setEmpPerfAssessmentFbDetails(List<EmpPerfAssessmentFbDetails> empPerfAssessmentFbDetails) {
        this.empPerfAssessmentFbDetails = empPerfAssessmentFbDetails;
    }

    public List<EmpPerfAssessmentQuestions> getEmpPerfAssessmentQuestions() {
        return empPerfAssessmentQuestions;
    }

    public void setEmpPerfAssessmentQuestions(List<EmpPerfAssessmentQuestions> empPerfAssessmentQuestions) {
        this.empPerfAssessmentQuestions = empPerfAssessmentQuestions;
    }

    public List<EmpPerfAssessmentQutnOpts> getEmpPerfAssessmentQutnOpts() {
        return empPerfAssessmentQutnOpts;
    }

    public void setEmpPerfAssessmentQutnOpts(List<EmpPerfAssessmentQutnOpts> empPerfAssessmentQutnOpts) {
        this.empPerfAssessmentQutnOpts = empPerfAssessmentQutnOpts;
    }

    public List<EmpPerfLevelWorks> getEmpPerfLevelWorks() {
        return empPerfLevelWorks;
    }

    public void setEmpPerfLevelWorks(List<EmpPerfLevelWorks> empPerfLevelWorks) {
        this.empPerfLevelWorks = empPerfLevelWorks;
    }

    public List<EmpPerfMentorAchievement> getEmpPerfMentorAchievements() {
        return empPerfMentorAchievements;
    }

    public void setEmpPerfMentorAchievements(List<EmpPerfMentorAchievement> empPerfMentorAchievements) {
        this.empPerfMentorAchievements = empPerfMentorAchievements;
    }

    public List<EmpPerfSubjPasspercentage> getEmpPerfSubjPasspercentages() {
        return empPerfSubjPasspercentages;
    }

    public void setEmpPerfSubjPasspercentages(List<EmpPerfSubjPasspercentage> empPerfSubjPasspercentages) {
        this.empPerfSubjPasspercentages = empPerfSubjPasspercentages;
    }
}
