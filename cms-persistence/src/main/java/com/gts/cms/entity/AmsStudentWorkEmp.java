package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_ams_student_work_emp database table.
 * 
 */
@Entity
@Table(name="t_ams_student_work_emp")
@NamedQuery(name="AmsStudentWorkEmp.findAll", query="SELECT a FROM AmsStudentWorkEmp a")
public class AmsStudentWorkEmp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_ams_std_work_exp_id")
	private Long amsStdWorkExpId;

	@Column(name="city_name")
	private String cityName;

	private String company;

	private String country;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String description;

	@Temporal(TemporalType.DATE)
	@Column(name="end_date")
	private Date endDate;

	//bi-directional many-to-one association to EmployeeDetail
		@ManyToOne(fetch = FetchType.LAZY)
		@JoinColumn(name="fk_verifiedby_emp_id")
		private EmployeeDetail verifiedbyEmpId;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_currently_working")
	private Boolean isCurrentlyWorking;

	@Column(name="is_verifiedby_incharge")
	private Boolean isVerifiedbyIncharge;

	@Column(name="job_title")
	private String jobTitle;

	private String location;

	private String pincode;

	private String reason;

	private String role;

	@Column(name="show_in_profile")
	private Boolean showInProfile;

	@Temporal(TemporalType.DATE)
	@Column(name="start_date")
	private Date startDate;

	private String state;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	@Column(name="verification_comments")
	private String verificationComments;

	//bi-directional many-to-one association to AmsProfileDetail
	@ManyToOne
	@JoinColumn(name="fk_ams_profile_id")
	private AmsProfileDetail amsProfileDetail;

	public AmsStudentWorkEmp() {
	}

	public Long getAmsStdWorkExpId() {
		return this.amsStdWorkExpId;
	}

	public void setAmsStdWorkExpId(Long amsStdWorkExpId) {
		this.amsStdWorkExpId = amsStdWorkExpId;
	}

	public String getCityName() {
		return this.cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCompany() {
		return this.company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}



	public EmployeeDetail getVerifiedbyEmpId() {
		return verifiedbyEmpId;
	}

	public void setVerifiedbyEmpId(EmployeeDetail verifiedbyEmpId) {
		this.verifiedbyEmpId = verifiedbyEmpId;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsCurrentlyWorking() {
		return this.isCurrentlyWorking;
	}

	public void setIsCurrentlyWorking(Boolean isCurrentlyWorking) {
		this.isCurrentlyWorking = isCurrentlyWorking;
	}

	public Boolean getIsVerifiedbyIncharge() {
		return this.isVerifiedbyIncharge;
	}

	public void setIsVerifiedbyIncharge(Boolean isVerifiedbyIncharge) {
		this.isVerifiedbyIncharge = isVerifiedbyIncharge;
	}

	public String getJobTitle() {
		return this.jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPincode() {
		return this.pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Boolean getShowInProfile() {
		return this.showInProfile;
	}

	public void setShowInProfile(Boolean showInProfile) {
		this.showInProfile = showInProfile;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getVerificationComments() {
		return this.verificationComments;
	}

	public void setVerificationComments(String verificationComments) {
		this.verificationComments = verificationComments;
	}

	public AmsProfileDetail getAmsProfileDetail() {
		return this.amsProfileDetail;
	}

	public void setAmsProfileDetail(AmsProfileDetail amsProfileDetail) {
		this.amsProfileDetail = amsProfileDetail;
	}

}