package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_exam_fee_receipts database table.
 * 
 */
@Entity
@Table(name = "t_exam_fee_receipts")
@NamedQuery(name = "ExamFeeReceipt.findAll", query = "SELECT e FROM ExamFeeReceipt e")
public class ExamFeeReceipt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_fee_receipt_id")
	private Long examFeeReceiptId;

	@Column(name = "cheque_no")
	private String chequeNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	private String ddno;

	@Column(name = "exam_addt_fee")
	private BigDecimal examAddtFee;

	@Column(name = "exam_fee_amount")
	private BigDecimal examFeeAmount;

	@Column(name = "exam_fine_amount")
	private BigDecimal examFineAmount;

	@Column(name = "fee_comments")
	private String feeComments;

	@Column(name = "fee_receipt_no")
	private String feeReceiptNo;
	
	@Column(name ="exam_total_amount")
	private BigDecimal examTotalAmount;
	
	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_collected_emp_id")
	private EmployeeDetail collectedEmp;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_year_id")
	private CourseYear courseYear;

	/*@Column(name = "fk_exam_fee_addt_ids")
	private String examFeeAddtIds;*/
	
	// bi-directional many-to-one association to ExamFeeFine
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_fee_fine_id")
	private ExamFeeFine examFeeFine;
	
	// bi-directional many-to-one association to ExamFeeStructure
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_fee_structure_id")
	private ExamFeeStructure examFeeStructure;
	
	// bi-directional many-to-one association to ExamMaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_id")
	private ExamMaster exam;
	
	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_examtype_catdet_id")
	private GeneralDetail examtypeCat;
	
	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_payment_mode_catdet_id")
	private GeneralDetail paymentModeCat;

	// bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;
	
	@Column(name = "fk_subject_ids")
	private String subjectIds;
	
	@Column(name="fk_exam_std_reg_payment_ids")
	private String examStdRegPaymentIds;
	
	@Column(name="fk_exam_std_reg_transaction_ids")
	private String examStdRegTxnIds;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "other_payment_number")
	private String otherPaymentNumber;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "receipt_date")
	private Date receiptDate;

	@Column(name = "reference_number")
	private String referenceNumber;

	@Column(name = "transaction_no")
	private String transactionNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;
	
	@Column(name ="is_Refund")
	private Boolean isRefund;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_refund_emp_id")
	private EmployeeDetail refundEmp;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "refund_date")
	private Date refundDate;
	
	@Column(name = "refund_Reason")
	private String refundReason;
	
	@OneToMany(mappedBy = "examFeeReceipt",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	private List<ExamStudent> examStudents;
	
	@OneToMany(mappedBy = "examFeeReceipt",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	private List<ExamAdditionalFeeReceipt> examAdditionalFeeReceipt;
	
	public ExamFeeReceipt() {
	}

	public Long getExamFeeReceiptId() {
		return this.examFeeReceiptId;
	}

	public void setExamFeeReceiptId(Long examFeeReceiptId) {
		this.examFeeReceiptId = examFeeReceiptId;
	}

	public String getChequeNo() {
		return this.chequeNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDdno() {
		return this.ddno;
	}

	public void setDdno(String ddno) {
		this.ddno = ddno;
	}

	public BigDecimal getExamAddtFee() {
		return this.examAddtFee;
	}

	public void setExamAddtFee(BigDecimal examAddtFee) {
		this.examAddtFee = examAddtFee;
	}

	public BigDecimal getExamFeeAmount() {
		return this.examFeeAmount;
	}

	public void setExamFeeAmount(BigDecimal examFeeAmount) {
		this.examFeeAmount = examFeeAmount;
	}

	public BigDecimal getExamFineAmount() {
		return this.examFineAmount;
	}

	public void setExamFineAmount(BigDecimal examFineAmount) {
		this.examFineAmount = examFineAmount;
	}

	public String getFeeComments() {
		return this.feeComments;
	}

	public void setFeeComments(String feeComments) {
		this.feeComments = feeComments;
	}

	public String getFeeReceiptNo() {
		return this.feeReceiptNo;
	}

	public void setFeeReceiptNo(String feeReceiptNo) {
		this.feeReceiptNo = feeReceiptNo;
	}

	
	public String getSubjectIds() {
		return this.subjectIds;
	}

	public void setSubjectIds(String subjectIds) {
		this.subjectIds = subjectIds;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getOtherPaymentNumber() {
		return this.otherPaymentNumber;
	}

	public void setOtherPaymentNumber(String otherPaymentNumber) {
		this.otherPaymentNumber = otherPaymentNumber;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getReceiptDate() {
		return this.receiptDate;
	}

	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}

	public String getReferenceNumber() {
		return this.referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getTransactionNo() {
		return this.transactionNo;
	}

	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public CourseYear getCourseYear() {
		return courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	/*public String getExamFeeAddtIds() {
		return examFeeAddtIds;
	}

	public void setExamFeeAddtIds(String examFeeAddtIds) {
		this.examFeeAddtIds = examFeeAddtIds;
	}*/

	public EmployeeDetail getCollectedEmp() {
		return collectedEmp;
	}

	public void setCollectedEmp(EmployeeDetail collectedEmp) {
		this.collectedEmp = collectedEmp;
	}

	public ExamFeeFine getExamFeeFine() {
		return examFeeFine;
	}

	public void setExamFeeFine(ExamFeeFine examFeeFine) {
		this.examFeeFine = examFeeFine;
	}

	public ExamFeeStructure getExamFeeStructure() {
		return examFeeStructure;
	}

	public void setExamFeeStructure(ExamFeeStructure examFeeStructure) {
		this.examFeeStructure = examFeeStructure;
	}

	public ExamMaster getExam() {
		return exam;
	}

	public void setExam(ExamMaster exam) {
		this.exam = exam;
	}

	public GeneralDetail getExamtypeCat() {
		return examtypeCat;
	}

	public void setExamtypeCat(GeneralDetail examtypeCat) {
		this.examtypeCat = examtypeCat;
	}

	public GeneralDetail getPaymentModeCat() {
		return paymentModeCat;
	}

	public void setPaymentModeCat(GeneralDetail paymentModeCat) {
		this.paymentModeCat = paymentModeCat;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public List<ExamStudent> getExamStudents() {
		return examStudents;
	}

	public void setExamStudents(List<ExamStudent> examStudents) {
		this.examStudents = examStudents;
	}

	public BigDecimal getExamTotalAmount() {
		return examTotalAmount;
	}

	public void setExamTotalAmount(BigDecimal examTotalAmount) {
		this.examTotalAmount = examTotalAmount;
	}

	public List<ExamAdditionalFeeReceipt> getExamAdditionalFeeReceipt() {
		return examAdditionalFeeReceipt;
	}

	public void setExamAdditionalFeeReceipt(List<ExamAdditionalFeeReceipt> examAdditionalFeeReceipt) {
		this.examAdditionalFeeReceipt = examAdditionalFeeReceipt;
	}

	public String getExamStdRegPaymentIds() {
		return examStdRegPaymentIds;
	}

	public void setExamStdRegPaymentIds(String examStdRegPaymentIds) {
		this.examStdRegPaymentIds = examStdRegPaymentIds;
	}

	public String getExamStdRegTxnIds() {
		return examStdRegTxnIds;
	}

	public void setExamStdRegTxnIds(String examStdRegTxnIds) {
		this.examStdRegTxnIds = examStdRegTxnIds;
	}

	public Boolean getIsRefund() {
		return isRefund;
	}

	public void setIsRefund(Boolean isRefund) {
		this.isRefund = isRefund;
	}

	public EmployeeDetail getRefundEmp() {
		return refundEmp;
	}

	public void setRefundEmp(EmployeeDetail refundEmp) {
		this.refundEmp = refundEmp;
	}

	public Date getRefundDate() {
		return refundDate;
	}

	public void setRefundDate(Date refundDate) {
		this.refundDate = refundDate;
	}

	public String getRefundReason() {
		return refundReason;
	}

	public void setRefundReason(String refundReason) {
		this.refundReason = refundReason;
	}
}
