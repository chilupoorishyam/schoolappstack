package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_fee_student_data database table.
 * 
 */
@Entity
@Table(name="t_fee_student_data")
@NamedQuery(name="FeeStudentData.findAll", query="SELECT f FROM FeeStudentData f")
public class FeeStudentData implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_std_data_id")
	private Long feeStdDataId;

	@Column(name="balance_amount")
	private BigDecimal balanceAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="discount_amount")
	private BigDecimal discountAmount;

	@Column(name="fine_amount")
	private BigDecimal fineAmount;

	@Column(name="gross_amount")
	private BigDecimal grossAmount;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="net_amount")
	private BigDecimal netAmount;

	@Column(name="paid_amount")
	private BigDecimal paidAmount;

	private String reason;

	@Column(name="refund_amount")
	private BigDecimal refundAmount;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FeeStructure
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_structure_id")
	private FeeStructure feeStructure;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_payment_status_catdet_id")
	private GeneralDetail paymentStatus;

	//bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_student_id")
	private StudentDetail studentDetail;

	//bi-directional many-to-one association to FeeStudentDataDetail
	@OneToMany(mappedBy="feeStudentData",cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	private List<FeeStudentDataDetail> feeStudentDataDetails;
	

	
	//bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_year_id")
	private CourseYear courseYear;

	//Added by Naveen on 04-12-2018
	@Column(name="scholarship_amount")
	private BigDecimal scholarshipAmount;

	public FeeStudentData() {
	}

	public Long getFeeStdDataId() {
		return this.feeStdDataId;
	}

	public void setFeeStdDataId(Long feeStdDataId) {
		this.feeStdDataId = feeStdDataId;
	}

	public BigDecimal getBalanceAmount() {
		return this.balanceAmount;
	}

	public void setBalanceAmount(BigDecimal balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public BigDecimal getDiscountAmount() {
		return this.discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public BigDecimal getFineAmount() {
		return this.fineAmount;
	}

	public void setFineAmount(BigDecimal fineAmount) {
		this.fineAmount = fineAmount;
	}

	public BigDecimal getGrossAmount() {
		return this.grossAmount;
	}

	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public BigDecimal getNetAmount() {
		return this.netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getRefundAmount() {
		return this.refundAmount;
	}

	public void setRefundAmount(BigDecimal refundAmount) {
		this.refundAmount = refundAmount;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public FeeStructure getFeeStructure() {
		return this.feeStructure;
	}

	public void setFeeStructure(FeeStructure feeStructure) {
		this.feeStructure = feeStructure;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getPaymentStatus() {
		return this.paymentStatus;
	}

	public void setPaymentStatus(GeneralDetail paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public List<FeeStudentDataDetail> getFeeStudentDataDetails() {
		return this.feeStudentDataDetails;
	}

	public void setFeeStudentDataDetails(List<FeeStudentDataDetail> feeStudentDataDetails) {
		this.feeStudentDataDetails = feeStudentDataDetails;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public FeeStudentDataDetail addFeeStudentDataDetail(FeeStudentDataDetail feeStudentDataDetail) {
		getFeeStudentDataDetails().add(feeStudentDataDetail);
		feeStudentDataDetail.setFeeStudentData(this);

		return feeStudentDataDetail;
	}

	public FeeStudentDataDetail removeFeeStudentDataDetail(FeeStudentDataDetail feeStudentDataDetail) {
		getFeeStudentDataDetails().remove(feeStudentDataDetail);
		feeStudentDataDetail.setFeeStudentData(null);

		return feeStudentDataDetail;
	}

	public BigDecimal getScholarshipAmount() {
		return scholarshipAmount;
	}

	public void setScholarshipAmount(BigDecimal scholarshipAmount) {
		this.scholarshipAmount = scholarshipAmount;
	}


	public CourseYear getCourseYear() {
		return courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}
}