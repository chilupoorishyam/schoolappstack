package com.gts.cms.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the t_emp_details_stg database table.
 */
@Entity
@Table(name = "t_emp_details_stg")
@Setter
@Getter
public class EmployeeDetailStg implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "pk_emp_id")
    private Long empId;

    @Column(name = "organization")
    private String organization;

    @Column(name = "school")
    private String school;

    @Column(name = "emp_no")
    private String empNo;

    @Column(name = "date_of_join")
    private String dateOfJoin;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "nationality")
    private String nationality;

    @Column(name = "gender")
    private String gender;

    @Column(name = "date_of_birth")
    private String dateOfBirth;

    @Column(name = "mobile_number")
    private String mobileNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "aadhaar_no")
    private String aadhaarNo;

    @Column(name = "pan_no")
    private String panNo;

    @Column(name = "department")
    private String department;

    @Column(name = "working_department")
    private String workingDepartment;

    @Column(name = "qualification")
    private String qualification;

    @Column(name = "designation")
    private String designation;

    @Column(name = "employee_category")
    private String employeeCategory;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "district")
    private String district;
}