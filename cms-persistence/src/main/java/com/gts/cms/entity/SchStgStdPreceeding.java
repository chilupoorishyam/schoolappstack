package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the t_sch_std_preceedings database table.
 * 
 */
@Entity
@Table(name="t_sch_stg_std_preceedings")
@NamedQuery(name="SchStgStdPreceeding.findAll", query="SELECT s FROM SchStgStdPreceeding s")
public class SchStgStdPreceeding implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_sch_stg_std_preceeding_id")
	private Long schStgStdPreceedingId;
	
	@Column(name="school_name")
	private String schoolName;
	
	@Column(name="preceeding_no")
	private String preceedingNo;
	
	@Column(name="sno")
	private String sno;
	
	@Column(name="applicant_no")
	private String applicantNo;
	
	@Column(name="student_name")
	private String studentName;
	
	@Column(name="course_and_year")
	private String courseAndYear;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="released_from_dt")
	private Date releasedFromDt;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="released_to_dt")
	private Date releasedToDt;
	
	@Column(name="months")
	private Integer months;
	
	@Column(name="tution_fee")
	private BigDecimal tutionFee;
	
	@Column(name="spl_fee")
	private BigDecimal splFee;
	
	@Column(name="other_fee")
	private BigDecimal otherFee;
	
	@Column(name="rtf_amount")
	private BigDecimal rtfAmount;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	public SchStgStdPreceeding() {
	}

	public BigDecimal getTutionFee() {
		return tutionFee;
	}

	public void setTutionFee(BigDecimal tutionFee) {
		this.tutionFee = tutionFee;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}


	public BigDecimal getSplFee() {
		return splFee;
	}

	public void setSplFee(BigDecimal splFee) {
		this.splFee = splFee;
	}

	public BigDecimal getOtherFee() {
		return otherFee;
	}

	public void setOtherFee(BigDecimal otherFee) {
		this.otherFee = otherFee;
	}

	public BigDecimal getRtfAmount() {
		return rtfAmount;
	}

	public void setRtfAmount(BigDecimal rtfAmount) {
		this.rtfAmount = rtfAmount;
	}

	public Long getSchStgStdPreceedingId() {
		return schStgStdPreceedingId;
	}

	public void setSchStgStdPreceedingId(Long schStgStdPreceedingId) {
		this.schStgStdPreceedingId = schStgStdPreceedingId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getPreceedingNo() {
		return preceedingNo;
	}

	public void setPreceedingNo(String preceedingNo) {
		this.preceedingNo = preceedingNo;
	}

	public String getSno() {
		return sno;
	}

	public void setSno(String sno) {
		this.sno = sno;
	}

	public String getApplicantNo() {
		return applicantNo;
	}

	public void setApplicantNo(String applicantNo) {
		this.applicantNo = applicantNo;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getCourseAndYear() {
		return courseAndYear;
	}

	public void setCourseAndYear(String courseAndYear) {
		this.courseAndYear = courseAndYear;
	}

	public Date getReleasedFromDt() {
		return releasedFromDt;
	}

	public void setReleasedFromDt(Date releasedFromDt) {
		this.releasedFromDt = releasedFromDt;
	}

	public Date getReleasedToDt() {
		return releasedToDt;
	}

	public void setReleasedToDt(Date releasedToDt) {
		this.releasedToDt = releasedToDt;
	}

	public Integer getMonths() {
		return months;
	}

	public void setMonths(Integer months) {
		this.months = months;
	}
	
	
	
}