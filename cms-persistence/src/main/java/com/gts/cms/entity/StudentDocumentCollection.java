package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_std_document_collection database table.
 * 
 */
@Entity
@Table(name="t_std_document_collection")
@NamedQuery(name="StudentDocumentCollection.findAll", query="SELECT s FROM StudentDocumentCollection s")
public class StudentDocumentCollection implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_std_doc_coll_id")
	private Long studentDocCollId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="file_name")
	private String fileName;

	@Column(name="file_path")
	private String filePath;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_hard_copy")
	private Boolean isHardCopy;

	@Column(name="is_original")
	private Boolean isOriginal;

	@Column(name="is_soft_copy")
	private Boolean isSoftCopy;

	@Column(name="is_verified")
	private Boolean isVerified;

	@Column(name="rack_number")
	private String rackNumber;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_verifiedby_emp_id")
	private EmployeeDetail verifiedbyEmployeeDetail;

	//bi-directional many-to-one association to DocumentRepository
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_doc_rep_id")
	private DocumentRepository documentRepository;

	//bi-directional many-to-one association to StudentAppDocCollection
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_app_doc_coll_id")
	private StudentAppDocCollection studentAppDocCollection;

	//bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_student_id")
	private StudentDetail studentDetail;

	public StudentDocumentCollection() {
	}

	public Long getStudentDocCollId() {
		return this.studentDocCollId;
	}

	public void setStudentDocCollId(Long studentDocCollId) {
		this.studentDocCollId = studentDocCollId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return this.filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsHardCopy() {
		return this.isHardCopy;
	}

	public void setIsHardCopy(Boolean isHardCopy) {
		this.isHardCopy = isHardCopy;
	}

	public Boolean getIsOriginal() {
		return this.isOriginal;
	}

	public void setIsOriginal(Boolean isOriginal) {
		this.isOriginal = isOriginal;
	}

	public Boolean getIsSoftCopy() {
		return this.isSoftCopy;
	}

	public void setIsSoftCopy(Boolean isSoftCopy) {
		this.isSoftCopy = isSoftCopy;
	}

	public Boolean getIsVerified() {
		return this.isVerified;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	public String getRackNumber() {
		return this.rackNumber;
	}

	public void setRackNumber(String rackNumber) {
		this.rackNumber = rackNumber;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getVerifiedbyEmployeeDetail() {
		return this.verifiedbyEmployeeDetail;
	}

	public void setVerifiedbyEmployeeDetail(EmployeeDetail verifiedbyEmployeeDetail) {
		this.verifiedbyEmployeeDetail = verifiedbyEmployeeDetail;
	}

	public DocumentRepository getDocumentRepository() {
		return this.documentRepository;
	}

	public void setDocumentRepository(DocumentRepository documentRepository) {
		this.documentRepository = documentRepository;
	}

	public StudentAppDocCollection getStudentAppDocCollection() {
		return this.studentAppDocCollection;
	}

	public void setStudentAppDocCollection(StudentAppDocCollection studentAppDocCollection) {
		this.studentAppDocCollection = studentAppDocCollection;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

}