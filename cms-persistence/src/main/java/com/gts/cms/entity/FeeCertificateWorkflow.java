package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_fee_certificate_workflow database table.
 * 
 */
@Entity
@Table(name="t_fee_certificate_workflow")
@NamedQuery(name="FeeCertificateWorkflow.findAll", query="SELECT f FROM FeeCertificateWorkflow f")
public class FeeCertificateWorkflow implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_certificate_wf_id")
	private Long feeCertificateWfId;

	@Column(name="approval_dept")
	private String approvalDept;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to Department
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_approval_dept_id")
	private Department department;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_dept_emp_id")
	private EmployeeDetail deptEmpDetail;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_approval_status_catdet_id")
	private GeneralDetail approvalStatus;

	//bi-directional many-to-one association to FeeCertificateIssue
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_certificate_issue_id")
	private FeeCertificateIssue feeCertificateIssue;

	public FeeCertificateWorkflow() {
	}

	public Long getFeeCertificateWfId() {
		return this.feeCertificateWfId;
	}

	public void setFeeCertificateWfId(Long feeCertificateWfId) {
		this.feeCertificateWfId = feeCertificateWfId;
	}

	public String getApprovalDept() {
		return this.approvalDept;
	}

	public void setApprovalDept(String approvalDept) {
		this.approvalDept = approvalDept;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Department getDepartment() {
		return this.department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public EmployeeDetail getDeptEmpDetail() {
		return this.deptEmpDetail;
	}

	public void setDeptEmpDetail(EmployeeDetail deptEmpDetail) {
		this.deptEmpDetail = deptEmpDetail;
	}

	public GeneralDetail getApprovalStatus() {
		return this.approvalStatus;
	}

	public void setApprovalStatus(GeneralDetail approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public FeeCertificateIssue getFeeCertificateIssue() {
		return this.feeCertificateIssue;
	}

	public void setFeeCertificateIssue(FeeCertificateIssue feeCertificateIssue) {
		this.feeCertificateIssue = feeCertificateIssue;
	}

}