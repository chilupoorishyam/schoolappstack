package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_cm_elective_groupyr_mapping database table.
 * 
 */
@Entity
@Table(name = "t_cm_elective_groupyr_mapping")
@NamedQuery(name = "ElectiveGroupyrMapping.findAll", query = "SELECT e FROM ElectiveGroupyrMapping e")
public class ElectiveGroupyrMapping implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_elective_groupyr_mapping_id")
	private Long electiveGroupyrMappingId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	private String electivegroupname;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "electivegroupcode")
	private String electiveGroupCode;

	@Column(name = "is_Advanced_Elective")
	private Boolean isAdvancedElective;

	// bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_academic_year_id")
	private AcademicYear academicYear;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to GroupSection
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_group_section_id")
	private GroupSection groupSection;

	// bi-directional many-to-one association to Subject
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_elective_subject_id")
	private Subject subject;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_staff_emp_id")
	private EmployeeDetail employeeDetail;

	@OneToMany(mappedBy = "electiveGroupyrMapping", fetch = FetchType.LAZY)
	private List<BatchwiseStudent> batchwiseStudents;

	public ElectiveGroupyrMapping() {
	}

	public Long getElectiveGroupyrMappingId() {
		return this.electiveGroupyrMappingId;
	}

	public void setElectiveGroupyrMappingId(Long electiveGroupyrMappingId) {
		this.electiveGroupyrMappingId = electiveGroupyrMappingId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getElectivegroupname() {
		return this.electivegroupname;
	}

	public void setElectivegroupname(String electivegroupname) {
		this.electivegroupname = electivegroupname;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GroupSection getGroupSection() {
		return this.groupSection;
	}

	public void setGroupSection(GroupSection groupSection) {
		this.groupSection = groupSection;
	}

	public Subject getSubject() {
		return this.subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public EmployeeDetail getEmployeeDetail() {
		return employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public List<BatchwiseStudent> getBatchwiseStudents() {
		return batchwiseStudents;
	}

	public void setBatchwiseStudents(List<BatchwiseStudent> batchwiseStudents) {
		this.batchwiseStudents = batchwiseStudents;
	}

	public String getElectiveGroupCode() {
		return electiveGroupCode;
	}

	public void setElectiveGroupCode(String electiveGroupCode) {
		this.electiveGroupCode = electiveGroupCode;
	}

	public Boolean getIsAdvancedElective() {
		return isAdvancedElective;
	}

	public void setIsAdvancedElective(Boolean isAdvancedElective) {
		this.isAdvancedElective = isAdvancedElective;
	}

}