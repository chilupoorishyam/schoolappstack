package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the t_fee_convenor_std_remittance database table.
 * 
 */
@Entity
@Table(name="t_fee_convenor_std_remittance")
@NamedQuery(name="FeeConvenorStdRemittance.findAll", query="SELECT f FROM FeeConvenorStdRemittance f")
public class FeeConvenorStdRemittance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_con_std_rem_id")
	private Long feeConvenorStdRemId;

	@Column(name="branch_code")
	private String branchCode;

	@Column(name="caste_code")
	private String casteCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="father_name")
	private String fatherName;

	@Column(name="inst_code")
	private String instCode;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_fee_exempt")
	private Boolean isFeeExempt;

	@Column(name="is_processed")
	private Boolean isProcessed;

	private String rank;

	private String reason;

	@Column(name="roll_number")
	private String rollNumber;

	@Column(name="student_name")
	private String studentName;

	@Column(name="tuition_fee_amount")
	private BigDecimal tuitionFeeAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;


	//bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_year_id")
	private CourseYear courseYear;

	//bi-directional many-to-one association to StudentApplication
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_app_id")
	private StudentApplication stdApplication;

	//bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_student_id")
	private StudentDetail studentDetail;

	//bi-directional many-to-one association to FeeStudentDataParticular
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_std_data_particulars_id")
	private FeeStudentDataParticular feeStudentDataParticular;

	public FeeConvenorStdRemittance() {
	}

	public Long getFeeConvenorStdRemId() {
		return this.feeConvenorStdRemId;
	}

	public void setFeeConvenorStdRemId(Long feeConvenorStdRemId) {
		this.feeConvenorStdRemId = feeConvenorStdRemId;
	}

	public String getBranchCode() {
		return this.branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getCasteCode() {
		return this.casteCode;
	}

	public void setCasteCode(String casteCode) {
		this.casteCode = casteCode;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getFatherName() {
		return this.fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getInstCode() {
		return this.instCode;
	}

	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsFeeExempt() {
		return this.isFeeExempt;
	}

	public void setIsFeeExempt(Boolean isFeeExempt) {
		this.isFeeExempt = isFeeExempt;
	}

	public Boolean getIsProcessed() {
		return this.isProcessed;
	}

	public void setIsProcessed(Boolean isProcessed) {
		this.isProcessed = isProcessed;
	}

	public String getRank() {
		return this.rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRollNumber() {
		return this.rollNumber;
	}

	public void setRollNumber(String rollNumber) {
		this.rollNumber = rollNumber;
	}

	public String getStudentName() {
		return this.studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public BigDecimal getTuitionFeeAmount() {
		return this.tuitionFeeAmount;
	}

	public void setTuitionFeeAmount(BigDecimal tuitionFeeAmount) {
		this.tuitionFeeAmount = tuitionFeeAmount;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}


	public CourseYear getCourseYear() {
		return this.courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public StudentApplication getStdApplication() {
		return this.stdApplication;
	}

	public void setStdApplication(StudentApplication stdApplication) {
		this.stdApplication = stdApplication;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public FeeStudentDataParticular getFeeStudentDataParticular() {
		return this.feeStudentDataParticular;
	}

	public void setFeeStudentDataParticular(FeeStudentDataParticular feeStudentDataParticular) {
		this.feeStudentDataParticular = feeStudentDataParticular;
	}

}