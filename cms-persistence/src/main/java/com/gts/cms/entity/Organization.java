package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_organizations database table.
 *
 */
@Entity
@Table(name="t_m_organizations")
@NamedQuery(name="Organization.findAll", query="SELECT o FROM Organization o")
public class Organization implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_org_id")
	private Long organizationId;

	private String address;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String email;

	@Column(name="facebook_url")
	private String facebookUrl;

	private String fax;

	@Column(name="google_url")
	private String googleUrl;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="landline_number")
	private String landlineNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="license_fdate")
	private Date licenseFdate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="license_tdate")
	private Date licenseTdate;

	@Column(name="linkedin_url")
	private String linkedinUrl;

	@Column(name="logo_filename")
	private String logoFilename;

	@Column(name="logo_path")
	private String logoPath;

	private String mandal;

	@Column(name="mobile_number")
	private String mobileNumber;

	@Column(name="no_issued_licenses")
	private Integer noIssuedLicenses;

	@Column(name="org_code")
	private String orgCode;

	@Column(name="org_name")
	private String orgName;

	private Integer pincode;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	private String url;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails;

	//bi-directional many-to-one association to EmployeeBankDetail
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<EmployeeBankDetail> empEmployeeBankDetails;

	//bi-directional many-to-one association to EmployeeReporting
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<EmployeeReporting> empEmployeeReportings;

	//bi-directional many-to-one association to EmployeeExperienceDetail
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<EmployeeExperienceDetail> empExperienceDetails;

	//bi-directional many-to-one association to LeaveType
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<LeaveType> hrLeaveTypes;

	//bi-directional many-to-one association to LibraryDetail
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<LibraryDetail> libraryDetails;

	//bi-directional many-to-one association to AcademicYear
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<AcademicYear> academicYears;

	//bi-directional many-to-one association to Campus
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<Campus> campuses;

	//bi-directional many-to-one association to Caste
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<Caste> castes;

	//bi-directional many-to-one association to School
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<School> schools;

	//bi-directional many-to-one association to ConfigAutonumber
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<ConfigAutonumber> configAutonumbers;

	//bi-directional many-to-one association to Designation
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<Designation> designations;

	//bi-directional many-to-one association to DocumentRepository
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<DocumentRepository> documentRepositories;

	//bi-directional many-to-one association to FinancialYear
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<FinancialYear> financialYears;

	//bi-directional many-to-one association to City
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_city_id")
	private City city;

	//bi-directional many-to-one association to District
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_district_id")
	private District district;

	//bi-directional many-to-one association to RoomType
	@OneToMany(mappedBy="organization")
	private List<RoomType> roomTypes;

	//bi-directional many-to-one association to SmsTag
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<SmsTag> smsTags;

	//bi-directional many-to-one association to StudentCategory
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<StudentCategory> studentCategories;

	//bi-directional many-to-one association to User
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<User> users;

	//bi-directional many-to-one association to Usertype
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<Usertype> usertypes;

	//bi-directional many-to-one association to WorkflowStage
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<WorkflowStage> workflowStages;

	//bi-directional many-to-one association to RolePrivilege
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<RolePrivilege> rolePrivileges;

	//bi-directional many-to-one association to Role
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<Role> roles;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications;

	//bi-directional many-to-one association to StudentEnquiry
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<StudentEnquiry> stdEnquiries;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<StudentDetail> studentDetails;

	//bi-directional many-to-one association to DistanceFee
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<DistanceFee> distanceFees;

	//bi-directional many-to-one association to Driver
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<Driver> drivers;

	//bi-directional many-to-one association to FeePayment
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<TransportFeePayment> feePayments;

	//bi-directional many-to-one association to Helper
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<Helper> helpers;

	//bi-directional many-to-one association to Route
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<Route> routes;

	//bi-directional many-to-one association to RouteStop
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<RouteStop> routeStops;

	//bi-directional many-to-one association to TransportAllocation
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<TransportAllocation> transportAllocations;

	//bi-directional many-to-one association to TransportDetail
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<TransportDetail> transportDetails;

	//bi-directional many-to-one association to VechicleRoute
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<VechicleRoute> vechicleRoutes;

	//bi-directional many-to-one association to VehicleDetail
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<VehicleDetail> vehicleDetails;

	//bi-directional many-to-one association to VehicleDriver
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<VehicleDriver> vehicleDrivers;

	//bi-directional many-to-one association to VehicleMaintenance
	@OneToMany(mappedBy="organization",fetch = FetchType.LAZY)
	private List<VehicleMaintenance> vehicleMaintenances;

	public Organization() {
	}

	public Long getOrganizationId() {
		return this.organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFacebookUrl() {
		return this.facebookUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getGoogleUrl() {
		return this.googleUrl;
	}

	public void setGoogleUrl(String googleUrl) {
		this.googleUrl = googleUrl;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getLandlineNumber() {
		return this.landlineNumber;
	}

	public void setLandlineNumber(String landlineNumber) {
		this.landlineNumber = landlineNumber;
	}

	public Date getLicenseFdate() {
		return this.licenseFdate;
	}

	public void setLicenseFdate(Date licenseFdate) {
		this.licenseFdate = licenseFdate;
	}

	public Date getLicenseTdate() {
		return this.licenseTdate;
	}

	public void setLicenseTdate(Date licenseTdate) {
		this.licenseTdate = licenseTdate;
	}

	public String getLinkedinUrl() {
		return this.linkedinUrl;
	}

	public void setLinkedinUrl(String linkedinUrl) {
		this.linkedinUrl = linkedinUrl;
	}

	public String getLogoFilename() {
		return this.logoFilename;
	}

	public void setLogoFilename(String logoFilename) {
		this.logoFilename = logoFilename;
	}

	public String getLogoPath() {
		return this.logoPath;
	}

	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}

	public String getMandal() {
		return this.mandal;
	}

	public void setMandal(String mandal) {
		this.mandal = mandal;
	}

	public String getMobileNumber() {
		return this.mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Integer getNoIssuedLicenses() {
		return this.noIssuedLicenses;
	}

	public void setNoIssuedLicenses(Integer noIssuedLicenses) {
		this.noIssuedLicenses = noIssuedLicenses;
	}

	public String getOrgCode() {
		return this.orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgName() {
		return this.orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public Integer getPincode() {
		return this.pincode;
	}

	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<EmployeeDetail> getEmpDetails() {
		return this.empDetails;
	}

	public void setEmpDetails(List<EmployeeDetail> empDetails) {
		this.empDetails = empDetails;
	}

	public EmployeeDetail addEmpDetail(EmployeeDetail empDetail) {
		getEmpDetails().add(empDetail);
		empDetail.setOrganization(this);

		return empDetail;
	}

	public EmployeeDetail removeEmpDetail(EmployeeDetail empDetail) {
		getEmpDetails().remove(empDetail);
		empDetail.setOrganization(null);

		return empDetail;
	}

	public List<EmployeeBankDetail> getEmpEmployeeBankDetails() {
		return this.empEmployeeBankDetails;
	}

	public void setEmpEmployeeBankDetails(List<EmployeeBankDetail> empEmployeeBankDetails) {
		this.empEmployeeBankDetails = empEmployeeBankDetails;
	}

	public EmployeeBankDetail addEmpEmployeeBankDetail(EmployeeBankDetail empEmployeeBankDetail) {
		getEmpEmployeeBankDetails().add(empEmployeeBankDetail);
		empEmployeeBankDetail.setOrganization(this);

		return empEmployeeBankDetail;
	}

	public EmployeeBankDetail removeEmpEmployeeBankDetail(EmployeeBankDetail empEmployeeBankDetail) {
		getEmpEmployeeBankDetails().remove(empEmployeeBankDetail);
		empEmployeeBankDetail.setOrganization(null);

		return empEmployeeBankDetail;
	}

	public List<EmployeeReporting> getEmpEmployeeReportings() {
		return this.empEmployeeReportings;
	}

	public void setEmpEmployeeReportings(List<EmployeeReporting> empEmployeeReportings) {
		this.empEmployeeReportings = empEmployeeReportings;
	}

	public EmployeeReporting addEmpEmployeeReporting(EmployeeReporting empEmployeeReporting) {
		getEmpEmployeeReportings().add(empEmployeeReporting);
		empEmployeeReporting.setOrganization(this);

		return empEmployeeReporting;
	}

	public EmployeeReporting removeEmpEmployeeReporting(EmployeeReporting empEmployeeReporting) {
		getEmpEmployeeReportings().remove(empEmployeeReporting);
		empEmployeeReporting.setOrganization(null);

		return empEmployeeReporting;
	}

	public List<EmployeeExperienceDetail> getEmpExperienceDetails() {
		return this.empExperienceDetails;
	}

	public void setEmpExperienceDetails(List<EmployeeExperienceDetail> empExperienceDetails) {
		this.empExperienceDetails = empExperienceDetails;
	}

	public EmployeeExperienceDetail addEmpExperienceDetail(EmployeeExperienceDetail empExperienceDetail) {
		getEmpExperienceDetails().add(empExperienceDetail);
		empExperienceDetail.setOrganization(this);

		return empExperienceDetail;
	}

	public EmployeeExperienceDetail removeEmpExperienceDetail(EmployeeExperienceDetail empExperienceDetail) {
		getEmpExperienceDetails().remove(empExperienceDetail);
		empExperienceDetail.setOrganization(null);

		return empExperienceDetail;
	}

	public List<LeaveType> getHrLeaveTypes() {
		return this.hrLeaveTypes;
	}

	public void setHrLeaveTypes(List<LeaveType> hrLeaveTypes) {
		this.hrLeaveTypes = hrLeaveTypes;
	}

	public LeaveType addHrLeaveType(LeaveType hrLeaveType) {
		getHrLeaveTypes().add(hrLeaveType);
		hrLeaveType.setOrganization(this);

		return hrLeaveType;
	}

	public LeaveType removeHrLeaveType(LeaveType hrLeaveType) {
		getHrLeaveTypes().remove(hrLeaveType);
		hrLeaveType.setOrganization(null);

		return hrLeaveType;
	}

	public List<LibraryDetail> getLibraryDetails() {
		return this.libraryDetails;
	}

	public void setLibraryDetails(List<LibraryDetail> libraryDetails) {
		this.libraryDetails = libraryDetails;
	}

	public LibraryDetail addLibraryDetail(LibraryDetail libraryDetail) {
		getLibraryDetails().add(libraryDetail);
		libraryDetail.setOrganization(this);

		return libraryDetail;
	}

	public LibraryDetail removeLibraryDetail(LibraryDetail libraryDetail) {
		getLibraryDetails().remove(libraryDetail);
		libraryDetail.setOrganization(null);

		return libraryDetail;
	}

	public List<AcademicYear> getAcademicYears() {
		return this.academicYears;
	}

	public void setAcademicYears(List<AcademicYear> academicYears) {
		this.academicYears = academicYears;
	}

	public AcademicYear addAcademicYear(AcademicYear academicYear) {
		getAcademicYears().add(academicYear);
		academicYear.setOrganization(this);

		return academicYear;
	}

	public AcademicYear removeAcademicYear(AcademicYear academicYear) {
		getAcademicYears().remove(academicYear);
		academicYear.setOrganization(null);

		return academicYear;
	}

	public List<Campus> getCampuses() {
		return this.campuses;
	}

	public void setCampuses(List<Campus> campuses) {
		this.campuses = campuses;
	}

	public Campus addCampus(Campus campus) {
		getCampuses().add(campus);
		campus.setOrganization(this);

		return campus;
	}

	public Campus removeCampus(Campus campus) {
		getCampuses().remove(campus);
		campus.setOrganization(null);

		return campus;
	}

	public List<Caste> getCastes() {
		return this.castes;
	}

	public void setCastes(List<Caste> castes) {
		this.castes = castes;
	}

	public Caste addCaste(Caste caste) {
		getCastes().add(caste);
		caste.setOrganization(this);

		return caste;
	}

	public Caste removeCaste(Caste caste) {
		getCastes().remove(caste);
		caste.setOrganization(null);

		return caste;
	}

	public List<School> getSchools() {
		return this.schools;
	}

	public void setSchools(List<School> schools) {
		this.schools = schools;
	}

	public School addSchool(School school) {
		getSchools().add(school);
		school.setOrganization(this);

		return school;
	}

	public School removeSchool(School school) {
		getSchools().remove(school);
		school.setOrganization(null);

		return school;
	}

	public List<ConfigAutonumber> getConfigAutonumbers() {
		return this.configAutonumbers;
	}

	public void setConfigAutonumbers(List<ConfigAutonumber> configAutonumbers) {
		this.configAutonumbers = configAutonumbers;
	}

	public ConfigAutonumber addConfigAutonumber(ConfigAutonumber configAutonumber) {
		getConfigAutonumbers().add(configAutonumber);
		configAutonumber.setOrganization(this);

		return configAutonumber;
	}

	public ConfigAutonumber removeConfigAutonumber(ConfigAutonumber configAutonumber) {
		getConfigAutonumbers().remove(configAutonumber);
		configAutonumber.setOrganization(null);

		return configAutonumber;
	}

	public List<Designation> getDesignations() {
		return this.designations;
	}

	public void setDesignations(List<Designation> designations) {
		this.designations = designations;
	}

	public Designation addDesignation(Designation designation) {
		getDesignations().add(designation);
		designation.setOrganization(this);

		return designation;
	}

	public Designation removeDesignation(Designation designation) {
		getDesignations().remove(designation);
		designation.setOrganization(null);

		return designation;
	}

	public List<DocumentRepository> getDocumentRepositories() {
		return this.documentRepositories;
	}

	public void setDocumentRepositories(List<DocumentRepository> documentRepositories) {
		this.documentRepositories = documentRepositories;
	}

	public DocumentRepository addDocumentRepository(DocumentRepository documentRepository) {
		getDocumentRepositories().add(documentRepository);
		documentRepository.setOrganization(this);

		return documentRepository;
	}

	public DocumentRepository removeDocumentRepository(DocumentRepository documentRepository) {
		getDocumentRepositories().remove(documentRepository);
		documentRepository.setOrganization(null);

		return documentRepository;
	}

	public List<FinancialYear> getFinancialYears() {
		return this.financialYears;
	}

	public void setFinancialYears(List<FinancialYear> financialYears) {
		this.financialYears = financialYears;
	}

	public FinancialYear addFinancialYear(FinancialYear financialYear) {
		getFinancialYears().add(financialYear);
		financialYear.setOrganization(this);

		return financialYear;
	}

	public FinancialYear removeFinancialYear(FinancialYear financialYear) {
		getFinancialYears().remove(financialYear);
		financialYear.setOrganization(null);

		return financialYear;
	}

	public City getCity() {
		return this.city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public District getDistrict() {
		return this.district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public List<RoomType> getRoomTypes() {
		return this.roomTypes;
	}

	public void setRoomTypes(List<RoomType> roomTypes) {
		this.roomTypes = roomTypes;
	}

	public RoomType addRoomType(RoomType roomType) {
		getRoomTypes().add(roomType);
		roomType.setOrganization(this);

		return roomType;
	}

	public RoomType removeRoomType(RoomType roomType) {
		getRoomTypes().remove(roomType);
		roomType.setOrganization(null);

		return roomType;
	}

	public List<SmsTag> getSmsTags() {
		return this.smsTags;
	}

	public void setSmsTags(List<SmsTag> smsTags) {
		this.smsTags = smsTags;
	}

	public SmsTag addSmsTag(SmsTag smsTag) {
		getSmsTags().add(smsTag);
		smsTag.setOrganization(this);

		return smsTag;
	}

	public SmsTag removeSmsTag(SmsTag smsTag) {
		getSmsTags().remove(smsTag);
		smsTag.setOrganization(null);

		return smsTag;
	}

	public List<StudentCategory> getStudentCategories() {
		return this.studentCategories;
	}

	public void setStudentCategories(List<StudentCategory> studentCategories) {
		this.studentCategories = studentCategories;
	}

	public StudentCategory addStudentCategory(StudentCategory studentCategory) {
		getStudentCategories().add(studentCategory);
		studentCategory.setOrganization(this);

		return studentCategory;
	}

	public StudentCategory removeStudentCategory(StudentCategory studentCategory) {
		getStudentCategories().remove(studentCategory);
		studentCategory.setOrganization(null);

		return studentCategory;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User addUser(User user) {
		getUsers().add(user);
		user.setOrganization(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setOrganization(null);

		return user;
	}

	public List<Usertype> getUsertypes() {
		return this.usertypes;
	}

	public void setUsertypes(List<Usertype> usertypes) {
		this.usertypes = usertypes;
	}

	public Usertype addUsertype(Usertype usertype) {
		getUsertypes().add(usertype);
		usertype.setOrganization(this);

		return usertype;
	}

	public Usertype removeUsertype(Usertype usertype) {
		getUsertypes().remove(usertype);
		usertype.setOrganization(null);

		return usertype;
	}

	public List<WorkflowStage> getWorkflowStages() {
		return this.workflowStages;
	}

	public void setWorkflowStages(List<WorkflowStage> workflowStages) {
		this.workflowStages = workflowStages;
	}

	public WorkflowStage addWorkflowStage(WorkflowStage workflowStage) {
		getWorkflowStages().add(workflowStage);
		workflowStage.setOrganization(this);

		return workflowStage;
	}

	public WorkflowStage removeWorkflowStage(WorkflowStage workflowStage) {
		getWorkflowStages().remove(workflowStage);
		workflowStage.setOrganization(null);

		return workflowStage;
	}

	public List<RolePrivilege> getRolePrivileges() {
		return this.rolePrivileges;
	}

	public void setRolePrivileges(List<RolePrivilege> rolePrivileges) {
		this.rolePrivileges = rolePrivileges;
	}

	public RolePrivilege addRolePrivilege(RolePrivilege rolePrivilege) {
		getRolePrivileges().add(rolePrivilege);
		rolePrivilege.setOrganization(this);

		return rolePrivilege;
	}

	public RolePrivilege removeRolePrivilege(RolePrivilege rolePrivilege) {
		getRolePrivileges().remove(rolePrivilege);
		rolePrivilege.setOrganization(null);

		return rolePrivilege;
	}

	public List<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public Role addRole(Role role) {
		getRoles().add(role);
		role.setOrganization(this);

		return role;
	}

	public Role removeRole(Role role) {
		getRoles().remove(role);
		role.setOrganization(null);

		return role;
	}

	public List<StudentApplication> getStdApplications() {
		return this.stdApplications;
	}

	public void setStdApplications(List<StudentApplication> stdApplications) {
		this.stdApplications = stdApplications;
	}

	public StudentApplication addStdApplication(StudentApplication stdApplication) {
		getStdApplications().add(stdApplication);
		stdApplication.setOrganization(this);

		return stdApplication;
	}

	public StudentApplication removeStdApplication(StudentApplication stdApplication) {
		getStdApplications().remove(stdApplication);
		stdApplication.setOrganization(null);

		return stdApplication;
	}

	public List<StudentEnquiry> getStdEnquiries() {
		return this.stdEnquiries;
	}

	public void setStdEnquiries(List<StudentEnquiry> stdEnquiries) {
		this.stdEnquiries = stdEnquiries;
	}

	public StudentEnquiry addStdEnquiry(StudentEnquiry stdEnquiry) {
		getStdEnquiries().add(stdEnquiry);
		stdEnquiry.setOrganization(this);

		return stdEnquiry;
	}

	public StudentEnquiry removeStdEnquiry(StudentEnquiry stdEnquiry) {
		getStdEnquiries().remove(stdEnquiry);
		stdEnquiry.setOrganization(null);

		return stdEnquiry;
	}

	public List<StudentDetail> getStudentDetails() {
		return this.studentDetails;
	}

	public void setStudentDetails(List<StudentDetail> studentDetails) {
		this.studentDetails = studentDetails;
	}

	public StudentDetail addStudentDetail(StudentDetail studentDetail) {
		getStudentDetails().add(studentDetail);
		studentDetail.setOrganization(this);

		return studentDetail;
	}

	public StudentDetail removeStudentDetail(StudentDetail studentDetail) {
		getStudentDetails().remove(studentDetail);
		studentDetail.setOrganization(null);

		return studentDetail;
	}

	public List<DistanceFee> getDistanceFees() {
		return this.distanceFees;
	}

	public void setDistanceFees(List<DistanceFee> distanceFees) {
		this.distanceFees = distanceFees;
	}

	public DistanceFee addDistanceFee(DistanceFee distanceFee) {
		getDistanceFees().add(distanceFee);
		distanceFee.setOrganization(this);

		return distanceFee;
	}

	public DistanceFee removeDistanceFee(DistanceFee distanceFee) {
		getDistanceFees().remove(distanceFee);
		distanceFee.setOrganization(null);

		return distanceFee;
	}

	public List<Driver> getDrivers() {
		return this.drivers;
	}

	public void setDrivers(List<Driver> drivers) {
		this.drivers = drivers;
	}

	public Driver addDriver(Driver driver) {
		getDrivers().add(driver);
		driver.setOrganization(this);

		return driver;
	}

	public Driver removeDriver(Driver driver) {
		getDrivers().remove(driver);
		driver.setOrganization(null);

		return driver;
	}

	public List<TransportFeePayment> getFeePayments() {
		return this.feePayments;
	}

	public void setFeePayments(List<TransportFeePayment> feePayments) {
		this.feePayments = feePayments;
	}

	public TransportFeePayment addFeePayment(TransportFeePayment feePayment) {
		getFeePayments().add(feePayment);
		feePayment.setOrganization(this);

		return feePayment;
	}

	public TransportFeePayment removeFeePayment(TransportFeePayment feePayment) {
		getFeePayments().remove(feePayment);
		feePayment.setOrganization(null);

		return feePayment;
	}

	public List<Helper> getHelpers() {
		return this.helpers;
	}

	public void setHelpers(List<Helper> helpers) {
		this.helpers = helpers;
	}

	public Helper addHelper(Helper helper) {
		getHelpers().add(helper);
		helper.setOrganization(this);

		return helper;
	}

	public Helper removeHelper(Helper helper) {
		getHelpers().remove(helper);
		helper.setOrganization(null);

		return helper;
	}

	public List<Route> getRoutes() {
		return this.routes;
	}

	public void setRoutes(List<Route> routes) {
		this.routes = routes;
	}

	public Route addRoute(Route route) {
		getRoutes().add(route);
		route.setOrganization(this);

		return route;
	}

	public Route removeRoute(Route route) {
		getRoutes().remove(route);
		route.setOrganization(null);

		return route;
	}

	public List<RouteStop> getRouteStops() {
		return this.routeStops;
	}

	public void setRouteStops(List<RouteStop> routeStops) {
		this.routeStops = routeStops;
	}

	public RouteStop addRouteStop(RouteStop routeStop) {
		getRouteStops().add(routeStop);
		routeStop.setOrganization(this);

		return routeStop;
	}

	public RouteStop removeRouteStop(RouteStop routeStop) {
		getRouteStops().remove(routeStop);
		routeStop.setOrganization(null);

		return routeStop;
	}

	public List<TransportAllocation> getTransportAllocations() {
		return this.transportAllocations;
	}

	public void setTransportAllocations(List<TransportAllocation> transportAllocations) {
		this.transportAllocations = transportAllocations;
	}

	public TransportAllocation addTransportAllocation(TransportAllocation transportAllocation) {
		getTransportAllocations().add(transportAllocation);
		transportAllocation.setOrganization(this);

		return transportAllocation;
	}

	public TransportAllocation removeTransportAllocation(TransportAllocation transportAllocation) {
		getTransportAllocations().remove(transportAllocation);
		transportAllocation.setOrganization(null);

		return transportAllocation;
	}

	public List<TransportDetail> getTransportDetails() {
		return this.transportDetails;
	}

	public void setTransportDetails(List<TransportDetail> transportDetails) {
		this.transportDetails = transportDetails;
	}

	public TransportDetail addTransportDetail(TransportDetail transportDetail) {
		getTransportDetails().add(transportDetail);
		transportDetail.setOrganization(this);

		return transportDetail;
	}

	public TransportDetail removeTransportDetail(TransportDetail transportDetail) {
		getTransportDetails().remove(transportDetail);
		transportDetail.setOrganization(null);

		return transportDetail;
	}

	public List<VechicleRoute> getVechicleRoutes() {
		return this.vechicleRoutes;
	}

	public void setVechicleRoutes(List<VechicleRoute> vechicleRoutes) {
		this.vechicleRoutes = vechicleRoutes;
	}

	public VechicleRoute addVechicleRoute(VechicleRoute vechicleRoute) {
		getVechicleRoutes().add(vechicleRoute);
		vechicleRoute.setOrganization(this);

		return vechicleRoute;
	}

	public VechicleRoute removeVechicleRoute(VechicleRoute vechicleRoute) {
		getVechicleRoutes().remove(vechicleRoute);
		vechicleRoute.setOrganization(null);

		return vechicleRoute;
	}

	public List<VehicleDetail> getVehicleDetails() {
		return this.vehicleDetails;
	}

	public void setVehicleDetails(List<VehicleDetail> vehicleDetails) {
		this.vehicleDetails = vehicleDetails;
	}

	public VehicleDetail addVehicleDetail(VehicleDetail vehicleDetail) {
		getVehicleDetails().add(vehicleDetail);
		vehicleDetail.setOrganization(this);

		return vehicleDetail;
	}

	public VehicleDetail removeVehicleDetail(VehicleDetail vehicleDetail) {
		getVehicleDetails().remove(vehicleDetail);
		vehicleDetail.setOrganization(null);

		return vehicleDetail;
	}

	public List<VehicleDriver> getVehicleDrivers() {
		return this.vehicleDrivers;
	}

	public void setVehicleDrivers(List<VehicleDriver> vehicleDrivers) {
		this.vehicleDrivers = vehicleDrivers;
	}

	public VehicleDriver addVehicleDriver(VehicleDriver vehicleDriver) {
		getVehicleDrivers().add(vehicleDriver);
		vehicleDriver.setOrganization(this);

		return vehicleDriver;
	}

	public VehicleDriver removeVehicleDriver(VehicleDriver vehicleDriver) {
		getVehicleDrivers().remove(vehicleDriver);
		vehicleDriver.setOrganization(null);

		return vehicleDriver;
	}

	public List<VehicleMaintenance> getVehicleMaintenances() {
		return this.vehicleMaintenances;
	}

	public void setVehicleMaintenances(List<VehicleMaintenance> vehicleMaintenances) {
		this.vehicleMaintenances = vehicleMaintenances;
	}

	public VehicleMaintenance addVehicleMaintenance(VehicleMaintenance vehicleMaintenance) {
		getVehicleMaintenances().add(vehicleMaintenance);
		vehicleMaintenance.setOrganization(this);

		return vehicleMaintenance;
	}

	public VehicleMaintenance removeVehicleMaintenance(VehicleMaintenance vehicleMaintenance) {
		getVehicleMaintenances().remove(vehicleMaintenance);
		vehicleMaintenance.setOrganization(null);

		return vehicleMaintenance;
	}

}