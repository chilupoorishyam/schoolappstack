package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_lib_library_settings database table.
 * 
 */
@Entity
@Table(name="t_lib_library_settings")
@NamedQuery(name="LibrarySetting.findAll", query="SELECT l FROM LibrarySetting l")
public class LibrarySetting implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_lib_settings_id")
	private Long libSettingsId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String description;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_fine")
	private Boolean isFine;

	@Column(name="is_issue")
	private Boolean isIssue;

	private String reason;

	@Column(name="setting_name")
	private String settingName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	private String value;

	//bi-directional many-to-one association to Organization
	@ManyToOne
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to LibraryDetail
	@ManyToOne
	@JoinColumn(name="fk_library_id")
	private LibraryDetail libraryDetail;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne
	@JoinColumn(name="fk_lib_setting_catdet_id")
	private GeneralDetail libSettingCatdet;

	public LibrarySetting() {
	}

	public Long getLibSettingsId() {
		return this.libSettingsId;
	}

	public void setLibSettingsId(Long libSettingsId) {
		this.libSettingsId = libSettingsId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsFine() {
		return this.isFine;
	}

	public void setIsFine(Boolean isFine) {
		this.isFine = isFine;
	}

	public Boolean getIsIssue() {
		return this.isIssue;
	}

	public void setIsIssue(Boolean isIssue) {
		this.isIssue = isIssue;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSettingName() {
		return this.settingName;
	}

	public void setSettingName(String settingName) {
		this.settingName = settingName;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public LibraryDetail getLibraryDetail() {
		return this.libraryDetail;
	}

	public void setLibraryDetail(LibraryDetail libraryDetail) {
		this.libraryDetail = libraryDetail;
	}

	public GeneralDetail getLibSettingCatdet() {
		return this.libSettingCatdet;
	}

	public void setLibSettingCatdet(GeneralDetail libSettingCatdet) {
		this.libSettingCatdet = libSettingCatdet;
	}

}