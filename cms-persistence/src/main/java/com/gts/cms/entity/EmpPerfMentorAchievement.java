package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "t_hr_emp_perf_mentor_achievement")
@NamedQuery(name="EmpPerfMentorAchievement.findAll", query="SELECT e FROM EmpPerfMentorAchievement e")
public class EmpPerfMentorAchievement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_emp_perf_mentor_achievement_id", nullable = false)
    private Long empPerfMentorAchievementId;

    @ManyToOne
    @JoinColumn(name = "fk_assessment_feedback_id", nullable = false)
    private EmpPerfAssessmentFeedback assessmentFeedbackId;

    @ManyToOne
    @JoinColumn(name = "fk_emp_id", nullable = false)
    private EmployeeDetail empId;

    @ManyToOne
    @JoinColumn(name = "fk_student_id", nullable = false)
    private StudentDetail studentId;

    @ManyToOne
    @JoinColumn(name = "fk_gainful_engagement_catdet_id", nullable = false)
    private GeneralDetail gainfulEngagementId;

    @Column(name = "is_active", nullable = false)
    private Boolean active;

    @Column(name = "reason")
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt", nullable = false)
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    public Long getEmpPerfMentorAchievementId() {
        return empPerfMentorAchievementId;
    }

    public void setEmpPerfMentorAchievementId(Long empPerfMentorAchievementId) {
        this.empPerfMentorAchievementId = empPerfMentorAchievementId;
    }

    public EmpPerfAssessmentFeedback getAssessmentFeedbackId() {
        return assessmentFeedbackId;
    }

    public void setAssessmentFeedbackId(EmpPerfAssessmentFeedback assessmentFeedbackId) {
        this.assessmentFeedbackId = assessmentFeedbackId;
    }

    public EmployeeDetail getEmpId() {
        return empId;
    }

    public void setEmpId(EmployeeDetail empId) {
        this.empId = empId;
    }

    public StudentDetail getStudentId() {
        return studentId;
    }

    public void setStudentId(StudentDetail studentId) {
        this.studentId = studentId;
    }

    public GeneralDetail getGainfulEngagementId() {
        return gainfulEngagementId;
    }

    public void setGainfulEngagementId(GeneralDetail gainfulEngagementId) {
        this.gainfulEngagementId = gainfulEngagementId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    public Long getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(Long createdUser) {
        this.createdUser = createdUser;
    }

    public Date getUpdatedDt() {
        return updatedDt;
    }

    public void setUpdatedDt(Date updatedDt) {
        this.updatedDt = updatedDt;
    }

    public Long getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(Long updatedUser) {
        this.updatedUser = updatedUser;
    }
}
