package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_exam_student_course_year database table.
 * 
 */
@Entity
@Table(name = "t_exam_student_course_year")
@NamedQuery(name = "ExamStudentCourseYear.findAll", query = "SELECT e FROM ExamStudentCourseYear e")
public class ExamStudentCourseYear implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_std_couryear_id")
	private Long examStdCouryearId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_year_id")
	private CourseYear courseYear;

	@Column(name = "fk_exam_ids")
	private String examIds;

	// bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Column(name = "total_credits")
	private Integer totalCredits;

	@Column(name = "total_external_marks")
	private Integer totalExternalMarks;

	@Column(name = "total_fail_subjects")
	private Integer totalFailSubjects;

	@Column(name = "total_internal_marks")
	private Integer totalInternalMarks;

	@Column(name = "total_pass_subjects")
	private Integer totalPassSubjects;

	@Column(name = "sgpa")
	private BigDecimal sgpa;

	@Column(name = "cgpa")
	private BigDecimal cgpa;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	public ExamStudentCourseYear() {
	}

	public Long getExamStdCouryearId() {
		return this.examStdCouryearId;
	}

	public void setExamStdCouryearId(Long examStdCouryearId) {
		this.examStdCouryearId = examStdCouryearId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getExamIds() {
		return examIds;
	}

	public void setExamIds(String examIds) {
		this.examIds = examIds;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getTotalCredits() {
		return this.totalCredits;
	}

	public void setTotalCredits(Integer totalCredits) {
		this.totalCredits = totalCredits;
	}

	public Integer getTotalExternalMarks() {
		return this.totalExternalMarks;
	}

	public void setTotalExternalMarks(Integer totalExternalMarks) {
		this.totalExternalMarks = totalExternalMarks;
	}

	public Integer getTotalFailSubjects() {
		return this.totalFailSubjects;
	}

	public void setTotalFailSubjects(Integer totalFailSubjects) {
		this.totalFailSubjects = totalFailSubjects;
	}

	public Integer getTotalInternalMarks() {
		return this.totalInternalMarks;
	}

	public void setTotalInternalMarks(Integer totalInternalMarks) {
		this.totalInternalMarks = totalInternalMarks;
	}

	public Integer getTotalPassSubjects() {
		return this.totalPassSubjects;
	}

	public void setTotalPassSubjects(Integer totalPassSubjects) {
		this.totalPassSubjects = totalPassSubjects;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public CourseYear getCourseYear() {
		return courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public BigDecimal getSgpa() {
		return sgpa;
	}

	public void setSgpa(BigDecimal sgpa) {
		this.sgpa = sgpa;
	}

	public BigDecimal getCgpa() {
		return cgpa;
	}

	public void setCgpa(BigDecimal cgpa) {
		this.cgpa = cgpa;
	}

}