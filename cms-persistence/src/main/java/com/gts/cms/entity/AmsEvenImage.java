package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_ams_even_images database table.
 * 
 */
@Entity
@Table(name = "t_ams_even_images")
@NamedQuery(name = "AmsEvenImage.findAll", query = "SELECT e FROM AmsEvenImage e")
public class AmsEvenImage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_ams_event_images_id")
	private Long eventImagesId;

	private String caption;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne
	@JoinColumn(name = "fk_ams_event_album_id")
	private AmsEventAlbum eventAlbum;

	@Column(name = "fk_likes_profile_ids")
	private String likesProfileIds;

	@ManyToOne
	@JoinColumn(name = "fk_uploaded_ams_profile_id")
	private AmsProfileDetail uploadedAmsProfile;

	@ManyToOne
	@JoinColumn(name = "fk_verifiedby_emp_id")
	private EmployeeDetail verifiedbyEmp;

	@Column(name = "image_url")
	private String imageUrl;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_showingallery")
	private Boolean isShowingallery;

	@Column(name = "is_video")
	private Boolean isVideo;

	private String reason;

	@Column(name = "thirdparty_url")
	private String thirdpartyUrl;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "uploaded_on")
	private Date uploadedOn;

	@Column(name = "verification_comments")
	private String verificationComments;

	public AmsEvenImage() {
	}

	public Long getEventImagesId() {
		return this.eventImagesId;
	}

	public void setEventImagesId(Long eventImagesId) {
		this.eventImagesId = eventImagesId;
	}

	public String getCaption() {
		return this.caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getImageUrl() {
		return this.imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsShowingallery() {
		return this.isShowingallery;
	}

	public void setIsShowingallery(Boolean isShowingallery) {
		this.isShowingallery = isShowingallery;
	}

	public Boolean getIsVideo() {
		return this.isVideo;
	}

	public void setIsVideo(Boolean isVideo) {
		this.isVideo = isVideo;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getThirdpartyUrl() {
		return this.thirdpartyUrl;
	}

	public void setThirdpartyUrl(String thirdpartyUrl) {
		this.thirdpartyUrl = thirdpartyUrl;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Date getUploadedOn() {
		return this.uploadedOn;
	}

	public void setUploadedOn(Date uploadedOn) {
		this.uploadedOn = uploadedOn;
	}

	public String getVerificationComments() {
		return this.verificationComments;
	}

	public void setVerificationComments(String verificationComments) {
		this.verificationComments = verificationComments;
	}

	public AmsEventAlbum getEventAlbum() {
		return eventAlbum;
	}

	public void setEventAlbum(AmsEventAlbum eventAlbum) {
		this.eventAlbum = eventAlbum;
	}

	public String getLikesProfileIds() {
		return likesProfileIds;
	}

	public void setLikesProfileIds(String likesProfileIds) {
		this.likesProfileIds = likesProfileIds;
	}

	public AmsProfileDetail getUploadedAmsProfile() {
		return uploadedAmsProfile;
	}

	public void setUploadedAmsProfile(AmsProfileDetail uploadedAmsProfile) {
		this.uploadedAmsProfile = uploadedAmsProfile;
	}

	public EmployeeDetail getVerifiedbyEmp() {
		return verifiedbyEmp;
	}

	public void setVerifiedbyEmp(EmployeeDetail verifiedbyEmp) {
		this.verifiedbyEmp = verifiedbyEmp;
	}

}