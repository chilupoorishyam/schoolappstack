package com.gts.cms.entity;

import java.io.Serializable;
import java.sql.Time;

import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_hstl_register database table.
 * 
 */
@Entity
@Table(name = "t_hstl_register")
@NamedQuery(name = "HostelRegister.findAll", query = "SELECT h FROM HostelRegister h")
public class HostelRegister implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_hstl_register_id")
	private Long hstlRegisterId;

	@Column(name = "attendees_name")
	private String attendeesName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	
	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "mobile_number")
	private String mobileNumber;

	@Column(name = "other_relation")
	private String otherRelation;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "in_date")
	private Date inDate;
	

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "out_date")
	private Date outDate;
	
	
	@Column(name = "in_time")
	private Time inTime;

	@Column(name = "out_time")
	private Time outTime;
	

	private String purpose;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to HostelDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_hstl_hostel_id")
	private HostelDetail hostelDetail;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_emp_id")
	private EmployeeDetail employeeDetail;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_relation_catdet_id")
	private GeneralDetail relationCatdet;

	// bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = " fk_student_id")
	private StudentDetail studentDetail;

	public HostelRegister() {
	}

	public Long getHstlRegisterId() {
		return hstlRegisterId;
	}

	public void setHstlRegisterId(Long hstlRegisterId) {
		this.hstlRegisterId = hstlRegisterId;
	}

	public String getAttendeesName() {
		return attendeesName;
	}

	public void setAttendeesName(String attendeesName) {
		this.attendeesName = attendeesName;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getOtherRelation() {
		return otherRelation;
	}

	public void setOtherRelation(String otherRelation) {
		this.otherRelation = otherRelation;
	}

	

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public HostelDetail getHostelDetail() {
		return hostelDetail;
	}

	public void setHostelDetail(HostelDetail hostelDetail) {
		this.hostelDetail = hostelDetail;
	}

	public EmployeeDetail getEmployeeDetail() {
		return employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public GeneralDetail getRelationCatdet() {
		return relationCatdet;
	}

	public void setRelationCatdet(GeneralDetail relationCatdet) {
		this.relationCatdet = relationCatdet;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}
	
	public Date getInDate() {
		return inDate;
	}

	public void setInDate(Date inDate) {
		this.inDate = inDate;
	}

	public Date getOutDate() {
		return outDate;
	}

	public void setOutDate(Date outDate) {
		this.outDate = outDate;
	}

	public Time getInTime() {
		return inTime;
	}

	public void setInTime(Time inTime) {
		this.inTime = inTime;
	}

	public Time getOutTime() {
		return outTime;
	}

	public void setOutTime(Time outTime) {
		this.outTime = outTime;
	}


}