package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_inv_srv_workflow database table.
 * 
 */
@Entity
@Table(name = "t_inv_srv_workflow")
@NamedQuery(name = "InvSrvWorkflow.findAll", query = "SELECT i FROM InvSrvWorkflow i")
public class InvSrvWorkflow implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_srv_wf_id")
	private Long srvWfId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_srv_from_status_catdet_id")
	private GeneralDetail srvFromStatusCatdet;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_srv_to_status_catdet_id")
	private GeneralDetail srvToStatusCatdet;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_status_emp_id")
	private EmployeeDetail statusEmp;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Column(name = "status_comments")
	private String statusComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "wf_stage_no")
	private Long wfStageNo;

	// bi-directional many-to-one association to InvSrv
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_srv_id")
	private InvSrv invSrv;

	public InvSrvWorkflow() {
	}

	public Long getSrvWfId() {
		return this.srvWfId;
	}

	public void setSrvWfId(Long srvWfId) {
		this.srvWfId = srvWfId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStatusComments() {
		return this.statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Long getWfStageNo() {
		return wfStageNo;
	}

	public void setWfStageNo(Long wfStageNo) {
		this.wfStageNo = wfStageNo;
	}

	public GeneralDetail getSrvFromStatusCatdet() {
		return srvFromStatusCatdet;
	}

	public void setSrvFromStatusCatdet(GeneralDetail srvFromStatusCatdet) {
		this.srvFromStatusCatdet = srvFromStatusCatdet;
	}

	public GeneralDetail getSrvToStatusCatdet() {
		return srvToStatusCatdet;
	}

	public void setSrvToStatusCatdet(GeneralDetail srvToStatusCatdet) {
		this.srvToStatusCatdet = srvToStatusCatdet;
	}

	public EmployeeDetail getStatusEmp() {
		return statusEmp;
	}

	public void setStatusEmp(EmployeeDetail statusEmp) {
		this.statusEmp = statusEmp;
	}

	public InvSrv getInvSrv() {
		return invSrv;
	}

	public void setInvSrv(InvSrv invSrv) {
		this.invSrv = invSrv;
	}

}