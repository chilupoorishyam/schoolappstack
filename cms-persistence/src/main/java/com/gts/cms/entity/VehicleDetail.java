package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_tm_vehicle_details database table.
 * 
 */
@Entity
@Table(name="t_tm_vehicle_details")
@NamedQuery(name="VehicleDetail.findAll", query="SELECT v FROM VehicleDetail v")
public class VehicleDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_vehicle_detail_id")
	private Long vehicleDetailId;

	@Column(name="available_seats")
	private Integer availableSeats;

	@Column(name="chasis_no")
	private String chasisNo;

	@Column(name="contact_person_name")
	private String contactPersonName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="current_address")
	private String currentAddress;

	@Column(name="engine_no")
	private String engineNo;

	@Column(name="insurance_provider")
	private String insuranceProvider;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="insurance_renewal_date")
	private Date insuranceRenewalDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="maximum_allowed")
	private Integer maximumAllowed;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="next_service_date")
	private Date nextServiceDate;

	@Column(name="no_of_seats")
	private Integer noOfSeats;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pollution_check_renewal_date")
	private Date pollutionCheckRenewalDate;

	@Column(name="rc_card_path1")
	private String rcCardPath1;

	@Column(name="rc_card_path2")
	private String rcCardPath2;

	@Column(name="rc_number")
	private String rcNumber;

	private String reason;

	@Column(name="registration_authority")
	private String registrationAuthority;

	@Temporal(TemporalType.DATE)
	@Column(name="registration_date")
	private Date registrationDate;

	@Column(name="road_tax_amount")
	private BigDecimal roadTaxAmount;

	@Column(name="service_number")
	private String serviceNumber;

	@Column(name="speedometer_reading")
	private String speedometerReading;

	@Column(name="track_id")
	private String trackId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	@Column(name="vehicle_maker")
	private String vehicleMaker;

	@Column(name="vehicle_model")
	private String vehicleModel;

	@Column(name="vehicle_name")
	private String vehicleName;

	@Column(name="vehicle_number")
	private String vehicleNumber;

	@Temporal(TemporalType.DATE)
	@Column(name="year_of_manufacture")
	private Date yearOfManufacture;

	//bi-directional many-to-one association to VechicleRoute
	@OneToMany(mappedBy="vehicleDetail",fetch = FetchType.LAZY)
	private List<VechicleRoute> vechicleRoutes;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_vechicle_type_catdet_id")
	private GeneralDetail vehicleType;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to TransportDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_transport_detail_id")
	private TransportDetail transportDetail;

	//bi-directional many-to-one association to VehicleDriver
	@OneToMany(mappedBy="vehicleDetail",fetch = FetchType.LAZY)
	private List<VehicleDriver> vehicleDrivers;

	//bi-directional many-to-one association to VehicleMaintenance
	@OneToMany(mappedBy="vehicleDetail",fetch = FetchType.LAZY)
	private List<VehicleMaintenance> vehicleMaintenances;

	public VehicleDetail() {
	}

	public Long getVehicleDetailId() {
		return this.vehicleDetailId;
	}

	public void setVehicleDetailId(Long vehicleDetailId) {
		this.vehicleDetailId = vehicleDetailId;
	}

	public Integer getAvailableSeats() {
		return this.availableSeats;
	}

	public void setAvailableSeats(Integer availableSeats) {
		this.availableSeats = availableSeats;
	}

	public String getChasisNo() {
		return this.chasisNo;
	}

	public void setChasisNo(String chasisNo) {
		this.chasisNo = chasisNo;
	}

	public String getContactPersonName() {
		return this.contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getCurrentAddress() {
		return this.currentAddress;
	}

	public void setCurrentAddress(String currentAddress) {
		this.currentAddress = currentAddress;
	}

	public String getEngineNo() {
		return this.engineNo;
	}

	public void setEngineNo(String engineNo) {
		this.engineNo = engineNo;
	}

	public String getInsuranceProvider() {
		return this.insuranceProvider;
	}

	public void setInsuranceProvider(String insuranceProvider) {
		this.insuranceProvider = insuranceProvider;
	}

	public Date getInsuranceRenewalDate() {
		return this.insuranceRenewalDate;
	}

	public void setInsuranceRenewalDate(Date insuranceRenewalDate) {
		this.insuranceRenewalDate = insuranceRenewalDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getMaximumAllowed() {
		return this.maximumAllowed;
	}

	public void setMaximumAllowed(Integer maximumAllowed) {
		this.maximumAllowed = maximumAllowed;
	}

	public Date getNextServiceDate() {
		return this.nextServiceDate;
	}

	public void setNextServiceDate(Date nextServiceDate) {
		this.nextServiceDate = nextServiceDate;
	}

	public Integer getNoOfSeats() {
		return this.noOfSeats;
	}

	public void setNoOfSeats(Integer noOfSeats) {
		this.noOfSeats = noOfSeats;
	}

	public Date getPollutionCheckRenewalDate() {
		return this.pollutionCheckRenewalDate;
	}

	public void setPollutionCheckRenewalDate(Date pollutionCheckRenewalDate) {
		this.pollutionCheckRenewalDate = pollutionCheckRenewalDate;
	}

	public String getRcCardPath1() {
		return this.rcCardPath1;
	}

	public void setRcCardPath1(String rcCardPath1) {
		this.rcCardPath1 = rcCardPath1;
	}

	public String getRcCardPath2() {
		return this.rcCardPath2;
	}

	public void setRcCardPath2(String rcCardPath2) {
		this.rcCardPath2 = rcCardPath2;
	}

	public String getRcNumber() {
		return this.rcNumber;
	}

	public void setRcNumber(String rcNumber) {
		this.rcNumber = rcNumber;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRegistrationAuthority() {
		return this.registrationAuthority;
	}

	public void setRegistrationAuthority(String registrationAuthority) {
		this.registrationAuthority = registrationAuthority;
	}

	public Date getRegistrationDate() {
		return this.registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public BigDecimal getRoadTaxAmount() {
		return this.roadTaxAmount;
	}

	public void setRoadTaxAmount(BigDecimal roadTaxAmount) {
		this.roadTaxAmount = roadTaxAmount;
	}

	public String getServiceNumber() {
		return this.serviceNumber;
	}

	public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}

	public String getSpeedometerReading() {
		return this.speedometerReading;
	}

	public void setSpeedometerReading(String speedometerReading) {
		this.speedometerReading = speedometerReading;
	}

	public String getTrackId() {
		return this.trackId;
	}

	public void setTrackId(String trackId) {
		this.trackId = trackId;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getVehicleMaker() {
		return this.vehicleMaker;
	}

	public void setVehicleMaker(String vehicleMaker) {
		this.vehicleMaker = vehicleMaker;
	}

	public String getVehicleModel() {
		return this.vehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}

	public String getVehicleName() {
		return this.vehicleName;
	}

	public void setVehicleName(String vehicleName) {
		this.vehicleName = vehicleName;
	}

	public String getVehicleNumber() {
		return this.vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public Date getYearOfManufacture() {
		return this.yearOfManufacture;
	}

	public void setYearOfManufacture(Date yearOfManufacture) {
		this.yearOfManufacture = yearOfManufacture;
	}

	public List<VechicleRoute> getVechicleRoutes() {
		return this.vechicleRoutes;
	}

	public void setVechicleRoutes(List<VechicleRoute> vechicleRoutes) {
		this.vechicleRoutes = vechicleRoutes;
	}

	public VechicleRoute addVechicleRoute(VechicleRoute vechicleRoute) {
		getVechicleRoutes().add(vechicleRoute);
		vechicleRoute.setVehicleDetail(this);

		return vechicleRoute;
	}

	public VechicleRoute removeVechicleRoute(VechicleRoute vechicleRoute) {
		getVechicleRoutes().remove(vechicleRoute);
		vechicleRoute.setVehicleDetail(null);

		return vechicleRoute;
	}

	public GeneralDetail getVehicleType() {
		return this.vehicleType;
	}

	public void setVehicleType(GeneralDetail vehicleType) {
		this.vehicleType = vehicleType;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public TransportDetail getTransportDetail() {
		return this.transportDetail;
	}

	public void setTransportDetail(TransportDetail transportDetail) {
		this.transportDetail = transportDetail;
	}

	public List<VehicleDriver> getVehicleDrivers() {
		return this.vehicleDrivers;
	}

	public void setVehicleDrivers(List<VehicleDriver> vehicleDrivers) {
		this.vehicleDrivers = vehicleDrivers;
	}

	public VehicleDriver addVehicleDriver(VehicleDriver vehicleDriver) {
		getVehicleDrivers().add(vehicleDriver);
		vehicleDriver.setVehicleDetail(this);

		return vehicleDriver;
	}

	public VehicleDriver removeVehicleDriver(VehicleDriver vehicleDriver) {
		getVehicleDrivers().remove(vehicleDriver);
		vehicleDriver.setVehicleDetail(null);

		return vehicleDriver;
	}

	public List<VehicleMaintenance> getVehicleMaintenances() {
		return this.vehicleMaintenances;
	}

	public void setVehicleMaintenances(List<VehicleMaintenance> vehicleMaintenances) {
		this.vehicleMaintenances = vehicleMaintenances;
	}

	public VehicleMaintenance addVehicleMaintenance(VehicleMaintenance vehicleMaintenance) {
		getVehicleMaintenances().add(vehicleMaintenance);
		vehicleMaintenance.setVehicleDetail(this);

		return vehicleMaintenance;
	}

	public VehicleMaintenance removeVehicleMaintenance(VehicleMaintenance vehicleMaintenance) {
		getVehicleMaintenances().remove(vehicleMaintenance);
		vehicleMaintenance.setVehicleDetail(null);

		return vehicleMaintenance;
	}

}