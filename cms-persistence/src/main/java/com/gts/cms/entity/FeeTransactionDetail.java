package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the t_fee_transaction_details database table.
 * 
 */
@Entity
@Table(name="t_fee_transaction_details")
@NamedQuery(name="FeeTransactionDetail.findAll", query="SELECT f FROM FeeTransactionDetail f")
public class FeeTransactionDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_transaction_detail_id")
	private Long feeTransactionDetailId;

	private BigDecimal amount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="discount_amount")
	private BigDecimal discountAmount;

	@Column(name="fine_amount")
	private BigDecimal fineAmount;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="paid_amount")
	private BigDecimal paidAmount;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FeeParticular
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_perticulars_id")
	private FeeParticular feeParticular;

	//bi-directional many-to-one association to FeeStructureParticular
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_structure_particular_id")
	private FeeStructureParticular feeStructureParticular;

	//bi-directional many-to-one association to FeeStudentWiseParticular
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_std_particular_id")
	private FeeStudentWiseParticular feeStudentWiseParticular;

	//bi-directional many-to-one association to FeeTransactionMaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_transaction_master_id")
	private FeeTransactionMaster feeTransactionMaster;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	public FeeTransactionDetail() {
	}

	public Long getFeeTransactionDetailId() {
		return this.feeTransactionDetailId;
	}

	public void setFeeTransactionDetailId(Long feeTransactionDetailId) {
		this.feeTransactionDetailId = feeTransactionDetailId;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public BigDecimal getDiscountAmount() {
		return this.discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public BigDecimal getFineAmount() {
		return this.fineAmount;
	}

	public void setFineAmount(BigDecimal fineAmount) {
		this.fineAmount = fineAmount;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public FeeParticular getFeeParticular() {
		return this.feeParticular;
	}

	public void setFeeParticular(FeeParticular feeParticular) {
		this.feeParticular = feeParticular;
	}

	public FeeStructureParticular getFeeStructureParticular() {
		return this.feeStructureParticular;
	}

	public void setFeeStructureParticular(FeeStructureParticular feeStructureParticular) {
		this.feeStructureParticular = feeStructureParticular;
	}

	public FeeStudentWiseParticular getFeeStudentWiseParticular() {
		return this.feeStudentWiseParticular;
	}

	public void setFeeStudentWiseParticular(FeeStudentWiseParticular feeStudentWiseParticular) {
		this.feeStudentWiseParticular = feeStudentWiseParticular;
	}

	public FeeTransactionMaster getFeeTransactionMaster() {
		return this.feeTransactionMaster;
	}

	public void setFeeTransactionMaster(FeeTransactionMaster feeTransactionMaster) {
		this.feeTransactionMaster = feeTransactionMaster;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

}