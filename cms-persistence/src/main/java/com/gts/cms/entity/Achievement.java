package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the t_pa_achievements database table.
 * 
 */
@Entity
@Table(name = "t_pa_achievements")
@NamedQuery(name = "Achievement.findAll", query = "SELECT a FROM Achievement a")
public class Achievement implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_pa_achievement_id")
	private Long achievementId;

	@Column(name = "achivement_description")
	private String achivementDescription;

	@Column(name = "achivement_title")
	private String achivementTitle;

	@Column(name = "belongs_to")
	private String belongsTo;

	@Column(name = "certificates_url")
	private String certificatesUrl;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	@Column(name = "duration_from")
	private Date durationFrom;

	@Temporal(TemporalType.DATE)
	@Column(name = "duration_to")
	private Date durationTo;

	// bi-directional many-to-one association to AcademicYear
	@JoinColumn(name = "year")
	private Integer year;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_achievementlevel_catdet_id")
	private GeneralDetail achievementLevelCat;

	@Column(name = "fk_mentor_emp_ids")
	private String mentorEmpIds;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	@Column(name = "fk_participated_emp_ids")
	private String fkParticipatedEmpIds;

	@Column(name = "fk_participated_std_ids")
	private String fkParticipatedStdIds;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_prize_catdet_id")
	private GeneralDetail prizeCat;

	private String grade;

	private String image1;

	private String image2;

	private String image3;

	private String image4;

	private String image5;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_inbannerdisplay")
	private Boolean isInbannerdisplay;

	@Column(name = "mentor_names")
	private String mentorNames;

	private BigDecimal percentage;

	private String ranks;

	private String reason;

	@Column(name = "reference_no")
	private String referenceNo;

	private String specialization;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to SubCategory
	@ManyToOne
	@JoinColumn(name = "fk_pa_subcategory_id")
	private SubCategory subCategory;

	public Achievement() {
	}

	public Long getAchievementId() {
		return this.achievementId;
	}

	public void setAchievementId(Long achievementId) {
		this.achievementId = achievementId;
	}

	public String getAchivementDescription() {
		return this.achivementDescription;
	}

	public void setAchivementDescription(String achivementDescription) {
		this.achivementDescription = achivementDescription;
	}

	public String getAchivementTitle() {
		return this.achivementTitle;
	}

	public void setAchivementTitle(String achivementTitle) {
		this.achivementTitle = achivementTitle;
	}

	public String getBelongsTo() {
		return this.belongsTo;
	}

	public void setBelongsTo(String belongsTo) {
		this.belongsTo = belongsTo;
	}

	public String getCertificatesUrl() {
		return this.certificatesUrl;
	}

	public void setCertificatesUrl(String certificatesUrl) {
		this.certificatesUrl = certificatesUrl;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getDurationFrom() {
		return this.durationFrom;
	}

	public void setDurationFrom(Date durationFrom) {
		this.durationFrom = durationFrom;
	}

	public Date getDurationTo() {
		return this.durationTo;
	}

	public void setDurationTo(Date durationTo) {
		this.durationTo = durationTo;
	}

	public String getMentorEmpIds() {
		return this.mentorEmpIds;
	}

	public void setMentorEmpIds(String mentorEmpIds) {
		this.mentorEmpIds = mentorEmpIds;
	}

	public String getFkParticipatedEmpIds() {
		return this.fkParticipatedEmpIds;
	}

	public void setFkParticipatedEmpIds(String fkParticipatedEmpIds) {
		this.fkParticipatedEmpIds = fkParticipatedEmpIds;
	}

	public String getFkParticipatedStdIds() {
		return this.fkParticipatedStdIds;
	}

	public void setFkParticipatedStdIds(String fkParticipatedStdIds) {
		this.fkParticipatedStdIds = fkParticipatedStdIds;
	}

	public String getGrade() {
		return this.grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getImage1() {
		return this.image1;
	}

	public void setImage1(String image1) {
		this.image1 = image1;
	}

	public String getImage2() {
		return this.image2;
	}

	public void setImage2(String image2) {
		this.image2 = image2;
	}

	public String getImage3() {
		return this.image3;
	}

	public void setImage3(String image3) {
		this.image3 = image3;
	}

	public String getImage4() {
		return this.image4;
	}

	public void setImage4(String image4) {
		this.image4 = image4;
	}

	public String getImage5() {
		return this.image5;
	}

	public void setImage5(String image5) {
		this.image5 = image5;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsInbannerdisplay() {
		return this.isInbannerdisplay;
	}

	public void setIsInbannerdisplay(Boolean isInbannerdisplay) {
		this.isInbannerdisplay = isInbannerdisplay;
	}

	public String getMentorNames() {
		return this.mentorNames;
	}

	public void setMentorNames(String mentorNames) {
		this.mentorNames = mentorNames;
	}

	public BigDecimal getPercentage() {
		return this.percentage;
	}

	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}

	public String getRanks() {
		return this.ranks;
	}

	public void setRanks(String ranks) {
		this.ranks = ranks;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReferenceNo() {
		return this.referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getSpecialization() {
		return this.specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	
	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public GeneralDetail getAchievementLevelCat() {
		return achievementLevelCat;
	}

	public void setAchievementLevelCat(GeneralDetail achievementLevelCat) {
		this.achievementLevelCat = achievementLevelCat;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public GeneralDetail getPrizeCat() {
		return prizeCat;
	}

	public void setPrizeCat(GeneralDetail prizeCat) {
		this.prizeCat = prizeCat;
	}

	public SubCategory getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(SubCategory subCategory) {
		this.subCategory = subCategory;
	}

}