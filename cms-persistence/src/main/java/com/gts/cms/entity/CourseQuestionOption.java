package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_dl_course_question_options database table.
 * 
 */
@Entity
@Table(name = "t_dl_course_question_options")
@NamedQuery(name = "CourseQuestionOption.findAll", query = "SELECT c FROM CourseQuestionOption c")
public class CourseQuestionOption implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_dl_course_question_option_id")
	private Long courseQuestionOptionId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_correct_answer")
	private Boolean isCorrectAnswer;

	private String options;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to CourseQuestion
	@ManyToOne
	@JoinColumn(name = "fk_dl_course_question_id")
	private CourseQuestion courseQuestion;

	public CourseQuestionOption() {
	}

	public Long getCourseQuestionOptionId() {
		return this.courseQuestionOptionId;
	}

	public void setCourseQuestionOptionId(Long courseQuestionOptionId) {
		this.courseQuestionOptionId = courseQuestionOptionId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsCorrectAnswer() {
		return this.isCorrectAnswer;
	}

	public void setIsCorrectAnswer(Boolean isCorrectAnswer) {
		this.isCorrectAnswer = isCorrectAnswer;
	}

	public String getOptions() {
		return this.options;
	}

	public void setOptions(String options) {
		this.options = options;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public CourseQuestion getCourseQuestion() {
		return courseQuestion;
	}

	public void setCourseQuestion(CourseQuestion courseQuestion) {
		this.courseQuestion = courseQuestion;
	}

}
