package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_exam_subjectwise_marksetup database table.
 */
@Data
@Entity
@Table(name = "t_exam_subjectwise_marksetup")
@NamedQuery(name = "ExamSubjectwiseMarksetup.findAll", query = "SELECT e FROM ExamSubjectwiseMarksetup e")
public class ExamSubjectwiseMarksetup implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_sub_marksetup_id")
    private Long subMarksetupId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "max_marks")
    private Integer maxMarks;

    @Column(name = "pass_marks")
    private Integer passMarks;

    @Column(name = "pass_percentage")
    private Integer passPercentage;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "from_date")
    private Date fromDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "toDate")
    private Date toDate;

    // bi-directional many-to-one association to AcademicYear
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_school_id")
    private School school;

    // bi-directional many-to-one association to AcademicYear
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_subject_id")
    private Subject subject;

    // bi-directional many-to-one association to AcademicYear
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_examtype_id")
    private ExamTypes examTypes;

    // bi-directional many-to-one association to AcademicYear
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_course_year_id")
    private CourseYear courseYear;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;
}