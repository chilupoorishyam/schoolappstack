package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.util.Date;

/**
 * The persistent class for the t_dl_member_assessment_details database table.
 * 
 */
@Entity
@Table(name = "t_dl_member_assessment_details")
@NamedQuery(name = "MemberAssessmentDetail.findAll", query = "SELECT m FROM MemberAssessmentDetail m")
public class MemberAssessmentDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_dl_member_assessment_det_id")
	private Long memberAssessmentDetId;

	@Column(name = "answer_attempted_time")
	private Time answerAttemptedTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "correction_date")
	private Date correctionDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne
	@JoinColumn(name = "fk_corrected_member_id")
	private DigitalLibraryMember correctedMember;

	@Column(name = "fk_dl_options_answer_ids")
	private String optionsAnswerIds;

	@Column(name = "fk_dl_selected_answer_ids")
	private String selectedAnswerIds;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_systemcorrected")
	private Boolean isSystemcorrected;

	@Column(name = "is_unattempted")
	private Boolean isUnattempted;

	private String reason;

	@Column(name = "secure_marks")
	private Integer secureMarks;

	@Column(name = "selected_correct_answers")
	private Integer selectedCorrectAnswers;

	@Column(name = "selected_wrong_answers")
	private Integer selectedWrongAnswers;

	@Column(name = "total_correct_answers")
	private Integer totalCorrectAnswers;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to MemberAssessment
	@ManyToOne
	@JoinColumn(name = "fk_dl_member_assessment_id")
	private MemberAssessment memberAssessment;

	// bi-directional many-to-one association to AssessmentQuestion
	@ManyToOne
	@JoinColumn(name = "fk_dl_assessment_question_id")
	private AssessmentQuestion assessmentQuestion;

	// bi-directional many-to-one association to CourseQuestion
	@ManyToOne
	@JoinColumn(name = "fk_dl_course_question_id")
	private CourseQuestion courseQuestion;

	public MemberAssessmentDetail() {
	}

	public Long getMemberAssessmentDetId() {
		return this.memberAssessmentDetId;
	}

	public void setMemberAssessmentDetId(Long memberAssessmentDetId) {
		this.memberAssessmentDetId = memberAssessmentDetId;
	}

	public Time getAnswerAttemptedTime() {
		return this.answerAttemptedTime;
	}

	public void setAnswerAttemptedTime(Time answerAttemptedTime) {
		this.answerAttemptedTime = answerAttemptedTime;
	}

	public Date getCorrectionDate() {
		return this.correctionDate;
	}

	public void setCorrectionDate(Date correctionDate) {
		this.correctionDate = correctionDate;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getOptionsAnswerIds() {
		return this.optionsAnswerIds;
	}

	public void setOptionsAnswerIds(String optionsAnswerIds) {
		this.optionsAnswerIds = optionsAnswerIds;
	}

	public String getSelectedAnswerIds() {
		return this.selectedAnswerIds;
	}

	public void setSelectedAnswerIds(String selectedAnswerIds) {
		this.selectedAnswerIds = selectedAnswerIds;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsSystemcorrected() {
		return this.isSystemcorrected;
	}

	public void setIsSystemcorrected(Boolean isSystemcorrected) {
		this.isSystemcorrected = isSystemcorrected;
	}

	public Boolean getIsUnattempted() {
		return this.isUnattempted;
	}

	public void setIsUnattempted(Boolean isUnattempted) {
		this.isUnattempted = isUnattempted;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getSecureMarks() {
		return this.secureMarks;
	}

	public void setSecureMarks(Integer secureMarks) {
		this.secureMarks = secureMarks;
	}

	public Integer getSelectedCorrectAnswers() {
		return this.selectedCorrectAnswers;
	}

	public void setSelectedCorrectAnswers(Integer selectedCorrectAnswers) {
		this.selectedCorrectAnswers = selectedCorrectAnswers;
	}

	public Integer getSelectedWrongAnswers() {
		return this.selectedWrongAnswers;
	}

	public void setSelectedWrongAnswers(Integer selectedWrongAnswers) {
		this.selectedWrongAnswers = selectedWrongAnswers;
	}

	public Integer getTotalCorrectAnswers() {
		return this.totalCorrectAnswers;
	}

	public void setTotalCorrectAnswers(Integer totalCorrectAnswers) {
		this.totalCorrectAnswers = totalCorrectAnswers;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public DigitalLibraryMember getCorrectedMember() {
		return correctedMember;
	}

	public void setCorrectedMember(DigitalLibraryMember correctedMember) {
		this.correctedMember = correctedMember;
	}

	public MemberAssessment getMemberAssessment() {
		return memberAssessment;
	}

	public void setMemberAssessment(MemberAssessment memberAssessment) {
		this.memberAssessment = memberAssessment;
	}

	public AssessmentQuestion getAssessmentQuestion() {
		return assessmentQuestion;
	}

	public void setAssessmentQuestion(AssessmentQuestion assessmentQuestion) {
		this.assessmentQuestion = assessmentQuestion;
	}

	public CourseQuestion getCourseQuestion() {
		return courseQuestion;
	}

	public void setCourseQuestion(CourseQuestion courseQuestion) {
		this.courseQuestion = courseQuestion;
	}

}