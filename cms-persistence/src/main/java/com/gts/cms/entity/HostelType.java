package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_hstl_hostel_types database table.
 * 
 */
@Entity
@Table(name = "t_hstl_hostel_types")
@NamedQuery(name = "HostelType.findAll", query = "SELECT h FROM HostelType h")
public class HostelType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_hstl_hostel_type_id")
	private Long hostelTypeId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "hostel_type_code")
	private String hostelTypeCode;

	@Column(name = "hostel_type_name")
	private String hostelTypeName;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	// bi-directional many-to-one association to HostelDetail
	@OneToMany(mappedBy = "hostelType")
	private List<HostelDetail> hstlHostelDetails;

	public HostelType() {
	}

	public Long getHostelTypeId() {
		return hostelTypeId;
	}

	public void setHostelTypeId(Long hostelTypeId) {
		this.hostelTypeId = hostelTypeId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getHostelTypeCode() {
		return hostelTypeCode;
	}

	public void setHostelTypeCode(String hostelTypeCode) {
		this.hostelTypeCode = hostelTypeCode;
	}

	public String getHostelTypeName() {
		return hostelTypeName;
	}

	public void setHostelTypeName(String hostelTypeName) {
		this.hostelTypeName = hostelTypeName;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public List<HostelDetail> getHstlHostelDetails() {
		return hstlHostelDetails;
	}

	public void setHstlHostelDetails(List<HostelDetail> hstlHostelDetails) {
		this.hstlHostelDetails = hstlHostelDetails;
	}
}