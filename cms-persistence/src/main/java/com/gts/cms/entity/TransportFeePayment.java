package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the t_tm_fee_payment database table.
 * 
 */
@Entity
@Table(name="t_tm_fee_payment")
@NamedQuery(name="TransportFeePayment.findAll", query="SELECT f FROM TransportFeePayment f")
public class TransportFeePayment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_tm_fee_payment_id")
	private Long feePaymentId;

	@Column(name="applied_discounts")
	private String appliedDiscounts;

	@Column(name="applied_fines")
	private String appliedFines;

	@Column(name="balance_amount")
	private BigDecimal balanceAmount;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="discount_amount")
	private BigDecimal discountAmount;

	@Column(name="discount_reason")
	private String discountReason;

	@Column(name="fine_amount")
	private BigDecimal fineAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="from_pay_date")
	private Date fromPayDate;

	@Column(name="gross_amount")
	private BigDecimal grossAmount;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="net_amount")
	private BigDecimal netAmount;

	@Column(name="paid_amount")
	private BigDecimal paidAmount;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="to_pay_date")
	private Date toPayDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_id")
	private EmployeeDetail employeeDetail;

	@Column(name="fk_fee_receipts_id")
	private String feeReceiptsId;

	//bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_student_id")
	private StudentDetail studentDetail;

	//bi-directional many-to-one association to TransportAllocation
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_transport_allocation_id")
	private TransportAllocation transportAllocation;

	//bi-directional many-to-one association to TransportDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_transport_detail_id")
	private TransportDetail transportDetail;

	public TransportFeePayment() {
	}

	public Long getFeePaymentId() {
		return this.feePaymentId;
	}

	public void setFeePaymentId(Long feePaymentId) {
		this.feePaymentId = feePaymentId;
	}

	public String getAppliedDiscounts() {
		return this.appliedDiscounts;
	}

	public void setAppliedDiscounts(String appliedDiscounts) {
		this.appliedDiscounts = appliedDiscounts;
	}

	public String getAppliedFines() {
		return this.appliedFines;
	}

	public void setAppliedFines(String appliedFines) {
		this.appliedFines = appliedFines;
	}

	public BigDecimal getBalanceAmount() {
		return this.balanceAmount;
	}

	public void setBalanceAmount(BigDecimal balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public BigDecimal getDiscountAmount() {
		return this.discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getDiscountReason() {
		return this.discountReason;
	}

	public void setDiscountReason(String discountReason) {
		this.discountReason = discountReason;
	}

	public BigDecimal getFineAmount() {
		return this.fineAmount;
	}

	public void setFineAmount(BigDecimal fineAmount) {
		this.fineAmount = fineAmount;
	}

	public Date getFromPayDate() {
		return this.fromPayDate;
	}

	public void setFromPayDate(Date fromPayDate) {
		this.fromPayDate = fromPayDate;
	}

	public BigDecimal getGrossAmount() {
		return this.grossAmount;
	}

	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public BigDecimal getNetAmount() {
		return this.netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getToPayDate() {
		return this.toPayDate;
	}

	public void setToPayDate(Date toPayDate) {
		this.toPayDate = toPayDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getEmployeeDetail() {
		return this.employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}


	public String getFeeReceiptsId() {
		return feeReceiptsId;
	}

	public void setFeeReceiptsId(String feeReceiptsId) {
		this.feeReceiptsId = feeReceiptsId;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public TransportAllocation getTransportAllocation() {
		return this.transportAllocation;
	}

	public void setTransportAllocation(TransportAllocation transportAllocation) {
		this.transportAllocation = transportAllocation;
	}

	public TransportDetail getTransportDetail() {
		return this.transportDetail;
	}

	public void setTransportDetail(TransportDetail transportDetail) {
		this.transportDetail = transportDetail;
	}

}