package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_pa_trainings database table.
 * 
 */
@Entity
@Table(name = "t_pa_trainings")
@NamedQuery(name = "Training.findAll", query = "SELECT t FROM Training t")
public class Training implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_pa_traning_id")
	private Long traningId;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	@Column(name = "year_name")
	private Integer yearName;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_incharge_emp_id")
	private EmployeeDetail inchargeEmp;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_trainingtype_catdet_id")
	private GeneralDetail trainingTypeCat;

	@Column(name = "training_title")
	private String trainingTitle;

	@Column(name = "training_description")
	private String trainingDescription;

	@Column(name = "trainer_name")
	private String trainerName;

	@Column(name = "trainer_details")
	private String trainerDetails;

	@Column(name = "discussion_points")
	private String discussionPoints;

	@Column(name = "is_trackaudience")
	private Boolean isTrackAudience;

	@Temporal(TemporalType.DATE)
	@Column(name = "start_date")
	private Date startDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to TrainingDetail
	@OneToMany(mappedBy = "training", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<TrainingDetail> trainingDetails;

	public Training() {
	}

	public Long getTraningId() {
		return this.traningId;
	}

	public void setTraningId(Long traningId) {
		this.traningId = traningId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDiscussionPoints() {
		return this.discussionPoints;
	}

	public void setDiscussionPoints(String discussionPoints) {
		this.discussionPoints = discussionPoints;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public EmployeeDetail getInchargeEmp() {
		return inchargeEmp;
	}

	public void setInchargeEmp(EmployeeDetail inchargeEmp) {
		this.inchargeEmp = inchargeEmp;
	}

	public GeneralDetail getTrainingTypeCat() {
		return trainingTypeCat;
	}

	public void setTrainingTypeCat(GeneralDetail trainingTypeCat) {
		this.trainingTypeCat = trainingTypeCat;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getTrainerDetails() {
		return this.trainerDetails;
	}

	public void setTrainerDetails(String trainerDetails) {
		this.trainerDetails = trainerDetails;
	}

	public String getTrainerName() {
		return this.trainerName;
	}

	public void setTrainerName(String trainerName) {
		this.trainerName = trainerName;
	}

	public String getTrainingDescription() {
		return this.trainingDescription;
	}

	public void setTrainingDescription(String trainingDescription) {
		this.trainingDescription = trainingDescription;
	}

	public String getTrainingTitle() {
		return this.trainingTitle;
	}

	public void setTrainingTitle(String trainingTitle) {
		this.trainingTitle = trainingTitle;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<TrainingDetail> getTrainingDetails() {
		return trainingDetails;
	}

	public void setTrainingDetails(List<TrainingDetail> trainingDetails) {
		this.trainingDetails = trainingDetails;
	}

	public Integer getYearName() {
		return yearName;
	}

	public void setYearName(Integer yearName) {
		this.yearName = yearName;
	}

	public Boolean getIsTrackAudience() {
		return isTrackAudience;
	}

	public void setIsTrackAudience(Boolean isTrackAudience) {
		this.isTrackAudience = isTrackAudience;
	}
}