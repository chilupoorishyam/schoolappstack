package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_hr_payslip_branch_settings database table.
 * 
 */
@Entity
@Table(name="t_hr_payslip_branch_settings")
@NamedQuery(name="PayslipBranchSetting.findAll", query="SELECT p FROM PayslipBranchSetting p")
public class PayslipBranchSetting implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_payslip_branch_setting_id")
	private Long payslipBranchSettingId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="field_sort_order")
	private Long fieldSortOrder;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to PayslipSetting
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_payslip_setting_id")
	private PayslipSetting payslipSetting;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	public PayslipBranchSetting() {
	}

	public Long getPayslipBranchSettingId() {
		return this.payslipBranchSettingId;
	}

	public void setPayslipBranchSettingId(Long payslipBranchSettingId) {
		this.payslipBranchSettingId = payslipBranchSettingId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Long getFieldSortOrder() {
		return this.fieldSortOrder;
	}

	public void setFieldSortOrder(Long fieldSortOrder) {
		this.fieldSortOrder = fieldSortOrder;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public PayslipSetting getPayslipSetting() {
		return this.payslipSetting;
	}

	public void setPayslipSetting(PayslipSetting payslipSetting) {
		this.payslipSetting = payslipSetting;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

}