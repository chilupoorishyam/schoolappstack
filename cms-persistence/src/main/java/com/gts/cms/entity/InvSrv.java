package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_inv_srv database table.
 * 
 */
@Entity
@Table(name = "t_inv_srv")
@NamedQuery(name = "InvSrv.findAll", query = "SELECT i FROM InvSrv i")
public class InvSrv implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_srv_id")
	private Long srvId;

	@Column(name = "adjustment_details")
	private String adjustmentDetails;

	@Column(name = "authorization_comments")
	private String authorizationComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	private Date deliverychallandate;

	private String deliverychallanno;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_authorized_by_emp_id")
	private EmployeeDetail authorizedByEmp;

	// bi-directional many-to-one association to Department
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_dept_id")
	private Department department;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_inv_transtype_catdet_id")
	private GeneralDetail invTranstypeCatdet;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_qc_emp_id")
	private EmployeeDetail qcEmp;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_srv_status_catdet_id")
	private GeneralDetail srvStatusCatdet;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_srv_type_catdet_id")
	private GeneralDetail srvTypeCatdet;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_status_emp_id")
	private EmployeeDetail statusEmp;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_qc_completed")
	private Boolean isQcCompleted;

	@Column(name = "qc_comments")
	private String qcComments;

	private String reason;

	@Column(name = "srv_actual_amount")
	private BigDecimal srvActualAmount;

	@Column(name = "srv_amount")
	private BigDecimal srvAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "srv_date")
	private Date srvDate;

	@Column(name = "srv_discount")
	private BigDecimal srvDiscount;

	@Column(name = "srv_no")
	private String srvNo;

	@Column(name = "srv_ref_file_path1")
	private String srvRefFilePath1;

	@Column(name = "srv_ref_file_path2")
	private String srvRefFilePath2;

	@Column(name = "srv_tax")
	private BigDecimal srvTax;

	@Column(name = "status_comments")
	private String statusComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to InvPurchasereturn
	@OneToMany(mappedBy = "invSrv")
	private List<InvPurchasereturn> invPurchaseReturns;

	// bi-directional many-to-one association to InvStoresmaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_store_id")
	private InvStoresmaster invStoresmaster;

	// bi-directional many-to-one association to InvPurchaseOrder
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_po_id")
	private InvPurchaseOrder invPurchaseOrder;

	// bi-directional many-to-one association to InvSuppliermaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_supplier_id")
	private InvSuppliermaster invSupplierMaster;

	// bi-directional many-to-one association to InvSrvItem
	@OneToMany(mappedBy = "invSrv", cascade = CascadeType.ALL)
	private List<InvSrvItem> invSrvItems;

	// bi-directional many-to-one association to InvSrvWorkflow
	@OneToMany(mappedBy = "invSrv", cascade = CascadeType.ALL)
	private List<InvSrvWorkflow> invSrvWorkflows;

	public InvSrv() {
	}

	public Long getSrvId() {
		return this.srvId;
	}

	public void setSrvId(Long srvId) {
		this.srvId = srvId;
	}

	public String getAdjustmentDetails() {
		return this.adjustmentDetails;
	}

	public void setAdjustmentDetails(String adjustmentDetails) {
		this.adjustmentDetails = adjustmentDetails;
	}

	public String getAuthorizationComments() {
		return this.authorizationComments;
	}

	public void setAuthorizationComments(String authorizationComments) {
		this.authorizationComments = authorizationComments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getDeliverychallandate() {
		return this.deliverychallandate;
	}

	public void setDeliverychallandate(Date deliverychallandate) {
		this.deliverychallandate = deliverychallandate;
	}

	public String getDeliverychallanno() {
		return this.deliverychallanno;
	}

	public void setDeliverychallanno(String deliverychallanno) {
		this.deliverychallanno = deliverychallanno;
	}

	public EmployeeDetail getAuthorizedByEmp() {
		return authorizedByEmp;
	}

	public void setAuthorizedByEmp(EmployeeDetail authorizedByEmp) {
		this.authorizedByEmp = authorizedByEmp;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public GeneralDetail getInvTranstypeCatdet() {
		return invTranstypeCatdet;
	}

	public void setInvTranstypeCatdet(GeneralDetail invTranstypeCatdet) {
		this.invTranstypeCatdet = invTranstypeCatdet;
	}

	public EmployeeDetail getQcEmp() {
		return qcEmp;
	}

	public void setQcEmp(EmployeeDetail qcEmp) {
		this.qcEmp = qcEmp;
	}

	public GeneralDetail getSrvStatusCatdet() {
		return srvStatusCatdet;
	}

	public void setSrvStatusCatdet(GeneralDetail srvStatusCatdet) {
		this.srvStatusCatdet = srvStatusCatdet;
	}

	public GeneralDetail getSrvTypeCatdet() {
		return srvTypeCatdet;
	}

	public void setSrvTypeCatdet(GeneralDetail srvTypeCatdet) {
		this.srvTypeCatdet = srvTypeCatdet;
	}

	public EmployeeDetail getStatusEmp() {
		return statusEmp;
	}

	public void setStatusEmp(EmployeeDetail statusEmp) {
		this.statusEmp = statusEmp;
	}

	public InvStoresmaster getInvStoresmaster() {
		return invStoresmaster;
	}

	public void setInvStoresmaster(InvStoresmaster invStoresmaster) {
		this.invStoresmaster = invStoresmaster;
	}

	public InvPurchaseOrder getInvPurchaseOrder() {
		return invPurchaseOrder;
	}

	public void setInvPurchaseOrder(InvPurchaseOrder invPurchaseOrder) {
		this.invPurchaseOrder = invPurchaseOrder;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsQcCompleted() {
		return this.isQcCompleted;
	}

	public void setIsQcCompleted(Boolean isQcCompleted) {
		this.isQcCompleted = isQcCompleted;
	}

	public String getQcComments() {
		return this.qcComments;
	}

	public void setQcComments(String qcComments) {
		this.qcComments = qcComments;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getSrvActualAmount() {
		return this.srvActualAmount;
	}

	public void setSrvActualAmount(BigDecimal srvActualAmount) {
		this.srvActualAmount = srvActualAmount;
	}

	public BigDecimal getSrvAmount() {
		return this.srvAmount;
	}

	public void setSrvAmount(BigDecimal srvAmount) {
		this.srvAmount = srvAmount;
	}

	public Date getSrvDate() {
		return this.srvDate;
	}

	public void setSrvDate(Date srvDate) {
		this.srvDate = srvDate;
	}

	public BigDecimal getSrvDiscount() {
		return this.srvDiscount;
	}

	public void setSrvDiscount(BigDecimal srvDiscount) {
		this.srvDiscount = srvDiscount;
	}

	public String getSrvNo() {
		return this.srvNo;
	}

	public void setSrvNo(String srvNo) {
		this.srvNo = srvNo;
	}

	public String getSrvRefFilePath1() {
		return this.srvRefFilePath1;
	}

	public void setSrvRefFilePath1(String srvRefFilePath1) {
		this.srvRefFilePath1 = srvRefFilePath1;
	}

	public String getSrvRefFilePath2() {
		return this.srvRefFilePath2;
	}

	public void setSrvRefFilePath2(String srvRefFilePath2) {
		this.srvRefFilePath2 = srvRefFilePath2;
	}

	public BigDecimal getSrvTax() {
		return this.srvTax;
	}

	public void setSrvTax(BigDecimal srvTax) {
		this.srvTax = srvTax;
	}

	public String getStatusComments() {
		return this.statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<InvSrvItem> getInvSrvItems() {
		return invSrvItems;
	}

	public void setInvSrvItems(List<InvSrvItem> invSrvItems) {
		this.invSrvItems = invSrvItems;
	}

	public List<InvSrvWorkflow> getInvSrvWorkflows() {
		return invSrvWorkflows;
	}

	public void setInvSrvWorkflows(List<InvSrvWorkflow> invSrvWorkflows) {
		this.invSrvWorkflows = invSrvWorkflows;
	}

	public List<InvPurchasereturn> getInvPurchaseReturns() {
		return invPurchaseReturns;
	}

	public void setInvPurchaseReturns(List<InvPurchasereturn> invPurchaseReturns) {
		this.invPurchaseReturns = invPurchaseReturns;
	}

	public InvSuppliermaster getInvSupplierMaster() {
		return invSupplierMaster;
	}

	public void setInvSupplierMaster(InvSuppliermaster invSupplierMaster) {
		this.invSupplierMaster = invSupplierMaster;
	}

}