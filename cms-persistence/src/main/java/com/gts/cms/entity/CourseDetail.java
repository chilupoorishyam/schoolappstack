package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_dl_course_details database table.
 * 
 */
@Entity
@Table(name = "t_dl_course_details")
@NamedQuery(name = "CourseDetail.findAll", query = "SELECT c FROM CourseDetail c")
public class CourseDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_course_details_id")
	private Long courseDetailsId;

	@Column(name ="coursedetails")
	private String courseDetails;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne
	@JoinColumn(name = "fk_coursedet_catdet_id")
	private GeneralDetail courseCat;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to OnlineCourses
	@ManyToOne
	@JoinColumn(name = "fk_dl_onlinecourse_id")
	private OnlineCourses onlineCourses;

	public Long getCourseDetailsId() {
		return courseDetailsId;
	}

	public void setCourseDetailsId(Long courseDetailsId) {
		this.courseDetailsId = courseDetailsId;
	}

	public String getCourseDetails() {
		return courseDetails;
	}

	public void setCourseDetails(String courseDetails) {
		this.courseDetails = courseDetails;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public GeneralDetail getCourseCat() {
		return courseCat;
	}

	public void setCourseCat(GeneralDetail courseCat) {
		this.courseCat = courseCat;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public OnlineCourses getOnlineCourses() {
		return onlineCourses;
	}

	public void setOnlineCourses(OnlineCourses onlineCourses) {
		this.onlineCourses = onlineCourses;
	}

}