package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_pa_placement_broadcast database table.
 * 
 */

@Entity
@Table(name = "t_pa_placement_broadcast")
@NamedQuery(name = "placementBroadcast.findAll", query = "SELECT b FROM PlacementBroadcast b")
public class PlacementBroadcast implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_pa_placement_broadcast_id")
	private Long placementBroadcastId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_company_id")
	private Company company;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;
	
	@Column(name = "year_name")
	private Integer yearName;
	
	private String reason;
	
	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "post_header")
	private String postHeader;

	@Column(name = "post")
	private String post;

	@Column(name = "post_signature")
	private String postSignature;

	@Column(name = "approved_on")
	private Date approvedOn;
	
	@Column(name = "is_approved")
	private Boolean isApproved;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_posttype_catdet_id")
    private GeneralDetail posttypeCatdetId;
	
	@Column(name = "fk_skillset_ids")
	private String skillsetIds;

	public Long getPlacementBroadcastId() {
		return placementBroadcastId;
	}

	public void setPlacementBroadcastId(Long placementBroadcastId) {
		this.placementBroadcastId = placementBroadcastId;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Integer getYearName() {
		return yearName;
	}

	public void setYearName(Integer yearName) {
		this.yearName = yearName;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getPostHeader() {
		return postHeader;
	}

	public void setPostHeader(String postHeader) {
		this.postHeader = postHeader;
	}

	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getPostSignature() {
		return postSignature;
	}

	public void setPostSignature(String postSignature) {
		this.postSignature = postSignature;
	}

	public Date getApprovedOn() {
		return approvedOn;
	}

	public void setApprovedOn(Date approvedOn) {
		this.approvedOn = approvedOn;
	}

	public Boolean getIsApproved() {
		return isApproved;
	}

	public void setIsApproved(Boolean isApproved) {
		this.isApproved = isApproved;
	}

	public GeneralDetail getPosttypeCatdetId() {
		return posttypeCatdetId;
	}

	public void setPosttypeCatdetId(GeneralDetail posttypeCatdetId) {
		this.posttypeCatdetId = posttypeCatdetId;
	}

	public String getSkillsetIds() {
		return skillsetIds;
	}

	public void setSkillsetIds(String skillsetIds) {
		this.skillsetIds = skillsetIds;
	}

	
}
