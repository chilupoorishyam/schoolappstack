package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_cm_assignments database table.
 * 
 */
@Entity
@Table(name = "t_cm_assignments")
@NamedQuery(name = "Assignment.findAll", query = "SELECT a FROM Assignment a")
public class Assignment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_assignment_id")
	private Long assignmentId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "allow_late_due_date")
	private Date allowLateDueDate;

	@Column(name = "allow_late_submission")
	private Boolean allowLateSubmission;

	@Column(name = "assgn_information")
	private String assgnInformation;

	@Column(name = "assgnment_doc_path")
	private String assgnmentDocPath;

	@Column(name = "assignment_doc_path1")
	private String assignmentDocPath1;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "assignment_start_date")
	private Date assignmentStartDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	private String description;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_marks_published")
	private Boolean isMarksPublished;

	@Column(name = "pass_marks")
	private Long passMarks;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "published_on")
	private Date publishedOn;

	private String reason;
	
	@Column(name = "reference_details")
	private String referenceDetails;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "submission_due_date")
	private Date submissionDueDate;

	private String title;

	@Column(name = "total_marks")
	private Long totalMarks;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to TCmSubjectUnitTopic
	@ManyToOne
	@JoinColumn(name = "fk_sub_unit_topic_id")
	private SubjectUnitTopic subjectUnitTopic;

	// bi-directional many-to-one association to TEmpDetail
	@ManyToOne
	@JoinColumn(name = "fk_staff_emp_id")
	private EmployeeDetail empDetail;

	// bi-directional many-to-one association to TMSchool
	@ManyToOne
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to TMGeneralDetail
	@ManyToOne
	@JoinColumn(name = "fk_assgn_type_catdet_id")
	private GeneralDetail assignTypeCat;

	// bi-directional many-to-one association to TMGeneralDetail
	@ManyToOne
	@JoinColumn(name = "fk_assignment_status_catdet_id")
	private GeneralDetail assignmentStatusCat;

	// bi-directional many-to-one association to TMGroupSection
	@ManyToOne
	@JoinColumn(name = "fk_group_section_id")
	private GroupSection groupSection;

	// bi-directional many-to-one association to TMSubject
	@ManyToOne
	@JoinColumn(name = "fk_subject_id")
	private Subject subject;

	// bi-directional many-to-one association to StudentAssignment
	@OneToMany(mappedBy = "assignment",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	private List<StudentAssignment> studentAssignments;

	public Assignment() {
	}

	public Long getAssignmentId() {
		return this.assignmentId;
	}

	public void setAssignmentId(Long assignmentId) {
		this.assignmentId = assignmentId;
	}

	public Date getAllowLateDueDate() {
		return this.allowLateDueDate;
	}

	public void setAllowLateDueDate(Date allowLateDueDate) {
		this.allowLateDueDate = allowLateDueDate;
	}

	public Boolean getAllowLateSubmission() {
		return this.allowLateSubmission;
	}

	public void setAllowLateSubmission(Boolean allowLateSubmission) {
		this.allowLateSubmission = allowLateSubmission;
	}

	public String getAssgnInformation() {
		return this.assgnInformation;
	}

	public void setAssgnInformation(String assgnInformation) {
		this.assgnInformation = assgnInformation;
	}

	public String getAssgnmentDocPath() {
		return this.assgnmentDocPath;
	}

	public void setAssgnmentDocPath(String assgnmentDocPath) {
		this.assgnmentDocPath = assgnmentDocPath;
	}

	public String getAssignmentDocPath1() {
		return this.assignmentDocPath1;
	}

	public void setAssignmentDocPath1(String assignmentDocPath1) {
		this.assignmentDocPath1 = assignmentDocPath1;
	}

	public Date getAssignmentStartDate() {
		return this.assignmentStartDate;
	}

	public void setAssignmentStartDate(Date assignmentStartDate) {
		this.assignmentStartDate = assignmentStartDate;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsMarksPublished() {
		return this.isMarksPublished;
	}

	public void setIsMarksPublished(Boolean isMarksPublished) {
		this.isMarksPublished = isMarksPublished;
	}

	public Long getPassMarks() {
		return passMarks;
	}

	public void setPassMarks(Long passMarks) {
		this.passMarks = passMarks;
	}

	public Date getPublishedOn() {
		return this.publishedOn;
	}

	public void setPublishedOn(Date publishedOn) {
		this.publishedOn = publishedOn;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReferenceDetails() {
		return this.referenceDetails;
	}

	public void setReferenceDetails(String referenceDetails) {
		this.referenceDetails = referenceDetails;
	}

	public Date getSubmissionDueDate() {
		return this.submissionDueDate;
	}

	public void setSubmissionDueDate(Date submissionDueDate) {
		this.submissionDueDate = submissionDueDate;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getTotalMarks() {
		return this.totalMarks;
	}

	public void setTotalMarks(Long totalMarks) {
		this.totalMarks = totalMarks;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public SubjectUnitTopic getSubjectUnitTopic() {
		return subjectUnitTopic;
	}

	public void setSubjectUnitTopic(SubjectUnitTopic subjectUnitTopic) {
		this.subjectUnitTopic = subjectUnitTopic;
	}

	public EmployeeDetail getEmpDetail() {
		return empDetail;
	}

	public void setEmpDetail(EmployeeDetail empDetail) {
		this.empDetail = empDetail;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getAssignTypeCat() {
		return assignTypeCat;
	}

	public void setAssignTypeCat(GeneralDetail assignTypeCat) {
		this.assignTypeCat = assignTypeCat;
	}

	public GeneralDetail getAssignmentStatusCat() {
		return assignmentStatusCat;
	}

	public void setAssignmentStatusCat(GeneralDetail assignmentStatusCat) {
		this.assignmentStatusCat = assignmentStatusCat;
	}

	public GroupSection getGroupSection() {
		return groupSection;
	}

	public void setGroupSection(GroupSection groupSection) {
		this.groupSection = groupSection;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public List<StudentAssignment> getStudentAssignments() {
		return this.studentAssignments;
	}

	public void setStudentAssignments(List<StudentAssignment> studentAssignments) {
		this.studentAssignments = studentAssignments;
	}

	public StudentAssignment addStudentAssignment(StudentAssignment studentAssignment) {
		getStudentAssignments().add(studentAssignment);
		studentAssignment.setAssignment(this);

		return studentAssignment;
	}

	public StudentAssignment removeStudentAssignment(StudentAssignment studentAssignment) {
		getStudentAssignments().remove(studentAssignment);
		studentAssignment.setAssignment(null);

		return studentAssignment;
	}

}