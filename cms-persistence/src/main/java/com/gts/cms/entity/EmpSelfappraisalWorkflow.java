package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_emp_selfappraisal_workflow database table.
 */
@Data
@Entity
@Table(name = "t_emp_selfappraisal_workflow")
@NamedQuery(name = "EmpSelfappraisalWorkflow.findAll", query = "SELECT c FROM EmpSelfappraisalWorkflow c")
public class EmpSelfappraisalWorkflow implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_emp_selfappraisal_wf_id")
    private Long empSelfappraisalWfId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "status_updated_on")
    private Date statusUpdatedOn;

    @Column(name = "status_comments")
    private String statusComments;

    @ManyToOne
    @JoinColumn(name = "fk_school_id")
    private School school;

    @ManyToOne
    @JoinColumn(name = "fk_emp_selfappraisal_id")
    private EmpSelfappraisal empSelfappraisalId;

    @ManyToOne
    @JoinColumn(name = "fk_selfappraisal_form_det_id")
    private EmpSelfappraisalFormDetail empSelfappraisalFormDetail;

    @ManyToOne
    @JoinColumn(name = "fk_appraisal_status_wf_id")
    private WorkflowStage workflowStage;

    @ManyToOne
    @JoinColumn(name = "fk_status_updated_emp_id")
    private EmployeeDetail statusUpdatedEmpDetail;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;
}