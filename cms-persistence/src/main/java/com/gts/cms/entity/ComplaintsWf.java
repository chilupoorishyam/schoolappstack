package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_grv_complaints_wf database table.
 * 
 */
@Entity
@Table(name = "t_grv_complaints_wf")
@NamedQuery(name = "ComplaintsWf.findAll", query = "SELECT c FROM ComplaintsWf c")
public class ComplaintsWf implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_grv_wf_id")
	private Long grvWfId;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_current_status")
	private Boolean isCurrentStatus;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "to_date")
	private Date toDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to Complaint
	@ManyToOne
	@JoinColumn(name = "fk_grv_complaint_id")
	private Complaint complaint;

	// bi-directional many-to-one association to GrievanceCommittee
	@ManyToOne
	@JoinColumn(name = "fk_from_committee_id")
	private GrievanceCommittee fromGrievanceCommittee;

	// bi-directional many-to-one association to GrievanceCommittee
	@ManyToOne
	@JoinColumn(name = "fk_to_committee_id")
	private GrievanceCommittee toGrievanceCommittee;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne
	@JoinColumn(name = "fk_from_emp_id")
	private EmployeeDetail fromEmp;

	// bi-directional many-to-one association to WorkflowStage
	@ManyToOne
	@JoinColumn(name = "fk_from_grv_status_id")
	private WorkflowStage fromGrvStatus;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne
	@JoinColumn(name = "fk_to_emp_id")
	private EmployeeDetail toEmp;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne
	@JoinColumn(name = "fk_to_grv_status_id")
	private WorkflowStage toGrvStatus;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "from_date")
	private Date fromDate;

	public ComplaintsWf() {
	}

	public Long getGrvWfId() {
		return this.grvWfId;
	}

	public void setGrvWfId(Long grvWfId) {
		this.grvWfId = grvWfId;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsCurrentStatus() {
		return this.isCurrentStatus;
	}

	public void setIsCurrentStatus(Boolean isCurrentStatus) {
		this.isCurrentStatus = isCurrentStatus;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getFromEmp() {
		return fromEmp;
	}

	public void setFromEmp(EmployeeDetail fromEmp) {
		this.fromEmp = fromEmp;
	}

	public WorkflowStage getFromGrvStatus() {
		return fromGrvStatus;
	}

	public void setFromGrvStatus(WorkflowStage fromGrvStatus) {
		this.fromGrvStatus = fromGrvStatus;
	}

	public EmployeeDetail getToEmp() {
		return toEmp;
	}

	public void setToEmp(EmployeeDetail toEmp) {
		this.toEmp = toEmp;
	}

	public WorkflowStage getToGrvStatus() {
		return toGrvStatus;
	}

	public void setToGrvStatus(WorkflowStage toGrvStatus) {
		this.toGrvStatus = toGrvStatus;
	}

	public Complaint getComplaint() {
		return complaint;
	}

	public void setComplaint(Complaint complaint) {
		this.complaint = complaint;
	}

	public GrievanceCommittee getFromGrievanceCommittee() {
		return fromGrievanceCommittee;
	}

	public void setFromGrievanceCommittee(GrievanceCommittee fromGrievanceCommittee) {
		this.fromGrievanceCommittee = fromGrievanceCommittee;
	}

	public GrievanceCommittee getToGrievanceCommittee() {
		return toGrievanceCommittee;
	}

	public void setToGrievanceCommittee(GrievanceCommittee toGrievanceCommittee) {
		this.toGrievanceCommittee = toGrievanceCommittee;
	}

}