package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_dl_coursecategories database table.
 * 
 */
@Entity
@Table(name = "t_dl_coursecategories")
@NamedQuery(name = "CourseCategory.findAll", query = "SELECT c FROM CourseCategory c")
public class CourseCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_dl_coursecategory_id")
	private Long courseCategoryId;

	@Column(name = "coursecategory_description")
	private String courseCategoryDescription;

	@Column(name = "coursecategory_name")
	private String courseCategoryName;

	@Column(name = "coursecategory_shortname")
	private String courseCategoryShortname;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	public CourseCategory() {
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Long getCourseCategoryId() {
		return courseCategoryId;
	}

	public void setCourseCategoryId(Long courseCategoryId) {
		this.courseCategoryId = courseCategoryId;
	}

	public String getCourseCategoryDescription() {
		return courseCategoryDescription;
	}

	public void setCourseCategoryDescription(String courseCategoryDescription) {
		this.courseCategoryDescription = courseCategoryDescription;
	}

	public String getCourseCategoryName() {
		return courseCategoryName;
	}

	public void setCourseCategoryName(String courseCategoryName) {
		this.courseCategoryName = courseCategoryName;
	}

	public String getCourseCategoryShortname() {
		return courseCategoryShortname;
	}

	public void setCourseCategoryShortname(String courseCategoryShortname) {
		this.courseCategoryShortname = courseCategoryShortname;
	}

}