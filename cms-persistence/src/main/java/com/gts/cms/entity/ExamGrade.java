package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_exam_grades database table.
 * 
 */
@Entity
@Table(name = "t_exam_grades")
@NamedQuery(name = "ExamGrade.findAll", query = "SELECT e FROM ExamGrade e")
public class ExamGrade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_grades_id")
	private Long examGradesId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "credit_points")
	private Integer creditPoints;

	private String description;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to Course
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_id")
	private Course course;



	@Column(name = "grade_code")
	private String gradeCode;

	@Column(name = "grade_name")
	private String gradeName;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "max_points")
	private Integer maxPoints;

	@Column(name = "max_score_percent")
	private Integer maxScorePercent;

	@Column(name = "min_points")
	private Integer minPoints;

	@Column(name = "min_score_percent")
	private Integer minScorePercent;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	public ExamGrade() {
	}

	public Long getExamGradesId() {
		return this.examGradesId;
	}

	public void setExamGradesId(Long examGradesId) {
		this.examGradesId = examGradesId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Integer getCreditPoints() {
		return this.creditPoints;
	}

	public void setCreditPoints(Integer creditPoints) {
		this.creditPoints = creditPoints;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}



	public String getGradeCode() {
		return this.gradeCode;
	}

	public void setGradeCode(String gradeCode) {
		this.gradeCode = gradeCode;
	}

	public String getGradeName() {
		return this.gradeName;
	}

	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getMaxPoints() {
		return this.maxPoints;
	}

	public void setMaxPoints(Integer maxPoints) {
		this.maxPoints = maxPoints;
	}

	public Integer getMaxScorePercent() {
		return this.maxScorePercent;
	}

	public void setMaxScorePercent(Integer maxScorePercent) {
		this.maxScorePercent = maxScorePercent;
	}

	public Integer getMinPoints() {
		return this.minPoints;
	}

	public void setMinPoints(Integer minPoints) {
		this.minPoints = minPoints;
	}

	public Integer getMinScorePercent() {
		return this.minScorePercent;
	}

	public void setMinScorePercent(Integer minScorePercent) {
		this.minScorePercent = minScorePercent;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

}