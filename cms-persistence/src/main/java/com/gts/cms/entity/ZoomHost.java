package com.gts.cms.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Entity
public class ZoomHost {

    @Id
    @Positive
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String zoomUserId;

    @NotNull
    private String zoomUserEmail;
    /**
     * 1: Basic;
     * 2: Licensed
     */
    @NotNull
    private Integer zoomUserType;
}
