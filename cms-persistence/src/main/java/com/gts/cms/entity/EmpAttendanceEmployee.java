package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_emp_attendance_employees database table.
 * 
 */
@Entity
@Table(name = "t_emp_attendance_employees")
@NamedQuery(name = "EmpAttendanceEmployee.findAll", query = "SELECT e FROM EmpAttendanceEmployee e")
public class EmpAttendanceEmployee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_emp_attendance_emps_id")
	private Long attendanceEmpsId;
	
	@Column(name = "categoryname")
	private String categoryName;
	
	@Column(name = "companyname")
	private String companyName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;
	
	@Column(name = "employeecode")
	private String employeeCode;
	
	@Column(name = "employeeid")
	private Long employeeId;
	
	@Column(name = "employeename")
	private String employeeName;

	@Column(name = "is_active")
	private Boolean isActive;
	
	@Column(name = "numericcode")
	private String numericCode;

	private String reason;
	
	@Column(name = "recordstatus")
	private Boolean recordstatus;
	
	@Column(name = "stringcode")
	private String stringCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_attendance_date")
	private Date startAttendanceDate;

	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "latest_attendance_date")
	private Date latestattendanceDate;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_emp_id")
	private EmployeeDetail employeeDetail;

	public EmpAttendanceEmployee() {
	}

	public Long getAttendanceEmpsId() {
		return this.attendanceEmpsId;
	}

	public void setAttendanceEmpsId(Long attendanceEmpsId) {
		this.attendanceEmpsId = attendanceEmpsId;
	}

	public String getCategoryName() {
		return this.categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Long getEmployeeId() {
		return this.employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getNumericCode() {
		return numericCode;
	}

	public void setNumericCode(String numericCode) {
		this.numericCode = numericCode;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Boolean getRecordstatus() {
		return this.recordstatus;
	}

	public void setRecordstatus(Boolean recordstatus) {
		this.recordstatus = recordstatus;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getStringCode() {
		return stringCode;
	}

	public void setStringCode(String stringCode) {
		this.stringCode = stringCode;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public EmployeeDetail getEmployeeDetail() {
		return employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public Date getStartAttendanceDate() {
		return startAttendanceDate;
	}

	public void setStartAttendanceDate(Date startAttendanceDate) {
		this.startAttendanceDate = startAttendanceDate;
	}

	public Date getLatestattendanceDate() {
		return latestattendanceDate;
	}

	public void setLatestattendanceDate(Date latestattendanceDate) {
		this.latestattendanceDate = latestattendanceDate;
	}
	
}