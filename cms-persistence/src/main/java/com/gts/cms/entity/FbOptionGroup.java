package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_hr_fb_option_group database table.
 * 
 */
@Entity
@Table(name="t_hr_fb_option_group")
@NamedQuery(name="FbOptionGroup.findAll", query="SELECT f FROM FbOptionGroup f")
public class FbOptionGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fb_option_group_id")
	private Long fbOptionGroupId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="optiongroup_code")
	private String optiongroupCode;

	@Column(name="optiongroup_name")
	private String optiongroupName;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to FbOptionchoice
	@OneToMany(mappedBy="fbOptionGroup",fetch = FetchType.LAZY)
	private List<FbOptionchoice> fbOptionchoices;

	//bi-directional many-to-one association to FeedbackQuestion
	@OneToMany(mappedBy="fbOptionGroup",fetch = FetchType.LAZY)
	private List<FeedbackQuestion> feedbackQuestions;

	//bi-directional many-to-one association to FeedbackSection
	@OneToMany(mappedBy="fbOptionGroup",fetch = FetchType.LAZY)
	private List<FeedbackSection> feedbackSections;

	public FbOptionGroup() {
	}

	public Long getFbOptionGroupId() {
		return this.fbOptionGroupId;
	}

	public void setFbOptionGroupId(Long fbOptionGroupId) {
		this.fbOptionGroupId = fbOptionGroupId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getOptiongroupCode() {
		return this.optiongroupCode;
	}

	public void setOptiongroupCode(String optiongroupCode) {
		this.optiongroupCode = optiongroupCode;
	}

	public String getOptiongroupName() {
		return this.optiongroupName;
	}

	public void setOptiongroupName(String optiongroupName) {
		this.optiongroupName = optiongroupName;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<FbOptionchoice> getFbOptionchoices() {
		return this.fbOptionchoices;
	}

	public void setFbOptionchoices(List<FbOptionchoice> fbOptionchoices) {
		this.fbOptionchoices = fbOptionchoices;
	}

	public FbOptionchoice addFbOptionchoice(FbOptionchoice fbOptionchoice) {
		getFbOptionchoices().add(fbOptionchoice);
		fbOptionchoice.setFbOptionGroup(this);

		return fbOptionchoice;
	}

	public FbOptionchoice removeFbOptionchoice(FbOptionchoice fbOptionchoice) {
		getFbOptionchoices().remove(fbOptionchoice);
		fbOptionchoice.setFbOptionGroup(null);

		return fbOptionchoice;
	}

	public List<FeedbackQuestion> getFeedbackQuestions() {
		return this.feedbackQuestions;
	}

	public void setFeedbackQuestions(List<FeedbackQuestion> feedbackQuestions) {
		this.feedbackQuestions = feedbackQuestions;
	}

	public FeedbackQuestion addFeedbackQuestion(FeedbackQuestion feedbackQuestion) {
		getFeedbackQuestions().add(feedbackQuestion);
		feedbackQuestion.setFbOptionGroup(this);

		return feedbackQuestion;
	}

	public FeedbackQuestion removeFeedbackQuestion(FeedbackQuestion feedbackQuestion) {
		getFeedbackQuestions().remove(feedbackQuestion);
		feedbackQuestion.setFbOptionGroup(null);

		return feedbackQuestion;
	}

	public List<FeedbackSection> getFeedbackSections() {
		return this.feedbackSections;
	}

	public void setFeedbackSections(List<FeedbackSection> feedbackSections) {
		this.feedbackSections = feedbackSections;
	}

	public FeedbackSection addFeedbackSection(FeedbackSection feedbackSection) {
		getFeedbackSections().add(feedbackSection);
		feedbackSection.setFbOptionGroup(this);

		return feedbackSection;
	}

	public FeedbackSection removeFeedbackSection(FeedbackSection feedbackSection) {
		getFeedbackSections().remove(feedbackSection);
		feedbackSection.setFbOptionGroup(null);

		return feedbackSection;
	}

}