package com.gts.cms.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Entity
public class TeamHost {

    @Id
    @Positive
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String teamUserId;

    @NotNull
    private String teamUserPrincipalName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="fk_user_id")
    private User user;
}
