package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

/**
 * The persistent class for the t_exam_online_student_answers database table.
 */
@Data
@Entity
@Table(name = "t_exam_online_student_answers")
@NamedQuery(name = "ExamOnlineStudentAnswers.findAll", query = "SELECT m FROM ExamOnlineStudentAnswers m")
public class ExamOnlineStudentAnswers implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_exam_online_student_answer_id")
    private Long examOnlineStudentAnswerId;

    @Column(name = "answer_attempted_time")
    private Time answerAttemptedTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "correction_date")
    private Date correctionDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @ManyToOne
    @JoinColumn(name = "fk_exam_online_student_id")
    private ExamOnlineStudent examOnlineStudent;

    @Column(name = "free_text_answer")
    private String freeTextAnswer;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "is_systemcorrected")
    private Boolean isSystemcorrected;

    @Column(name = "is_unattempted")
    private Boolean isUnattempted;

    private String reason;

    @Column(name = "secure_marks")
    private Integer secureMarks;

    @Column(name = "fk_exam_online_options_answer_ids")
    private String examOnlineOptionsAnswerIds;

    @Column(name = "fk_exam_online_selected_answer_ids")
    private String examOnlineSelectedAnswerIds;

    @Column(name = "selected_correct_answers")
    private Integer selectedCorrectAnswers;

    @Column(name = "selected_wrong_answers")
    private Integer selectedWrongAnswers;

    @Column(name = "total_correct_answers")
    private Integer totalCorrectAnswers;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    // bi-directional many-to-one association to MemberAssessment
    @ManyToOne
    @JoinColumn(name = "fk_exam_online_question_paper_id")
    private ExamOnlineQuestionPaper examOnlineQuestionPaper;

    // bi-directional many-to-one association to AssessmentQuestion
    @ManyToOne
    @JoinColumn(name = "fk_exam_online_question_id")
    private ExamOnlineQuestion examOnlineQuestion;

    // bi-directional many-to-one association to AssessmentQuestion
    @ManyToOne
    @JoinColumn(name = "fk_corrected_emp_id")
    private EmployeeDetail correctedEmployeeDetail;

}