package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_electivetypes database table.
 * 
 */
@Entity
@Table(name="t_m_electivetypes")
@NamedQuery(name="Electivetype.findAll", query="SELECT e FROM Electivetype e")
public class Electivetype implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_electivetype_id")
	private Long electivetypeId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="elective_type_code")
	private String electiveTypeCode;

	@Column(name="elective_type_name")
	private String electiveTypeName;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to GroupyrRegulationDetail
	@OneToMany(mappedBy="electivetype",fetch = FetchType.LAZY)
	private List<GroupyrRegulationDetail> groupyrRegulationDetails;

	//bi-directional many-to-one association to Subjectregulation
	@OneToMany(mappedBy="electivetype",fetch = FetchType.LAZY)
	private List<Subjectregulation> subjectregulations;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to Course
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_id")
	private Course course;

	public Electivetype() {
	}

	public Long getElectivetypeId() {
		return this.electivetypeId;
	}

	public void setElectivetypeId(Long electivetypeId) {
		this.electivetypeId = electivetypeId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getElectiveTypeCode() {
		return this.electiveTypeCode;
	}

	public void setElectiveTypeCode(String electiveTypeCode) {
		this.electiveTypeCode = electiveTypeCode;
	}

	public String getElectiveTypeName() {
		return this.electiveTypeName;
	}

	public void setElectiveTypeName(String electiveTypeName) {
		this.electiveTypeName = electiveTypeName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<GroupyrRegulationDetail> getGroupyrRegulationDetails() {
		return this.groupyrRegulationDetails;
	}

	public void setGroupyrRegulationDetails(List<GroupyrRegulationDetail> groupyrRegulationDetails) {
		this.groupyrRegulationDetails = groupyrRegulationDetails;
	}

	public GroupyrRegulationDetail addGroupyrRegulationDetail(GroupyrRegulationDetail groupyrRegulationDetail) {
		getGroupyrRegulationDetails().add(groupyrRegulationDetail);
		groupyrRegulationDetail.setElectivetype(this);

		return groupyrRegulationDetail;
	}

	public GroupyrRegulationDetail removeGroupyrRegulationDetail(GroupyrRegulationDetail groupyrRegulationDetail) {
		getGroupyrRegulationDetails().remove(groupyrRegulationDetail);
		groupyrRegulationDetail.setElectivetype(null);

		return groupyrRegulationDetail;
	}

	public List<Subjectregulation> getSubjectregulations() {
		return this.subjectregulations;
	}

	public void setSubjectregulations(List<Subjectregulation> subjectregulations) {
		this.subjectregulations = subjectregulations;
	}

	public Subjectregulation addSubjectregulation(Subjectregulation subjectregulation) {
		getSubjectregulations().add(subjectregulation);
		subjectregulation.setElectivetype(this);

		return subjectregulation;
	}

	public Subjectregulation removeSubjectregulation(Subjectregulation subjectregulation) {
		getSubjectregulations().remove(subjectregulation);
		subjectregulation.setElectivetype(null);

		return subjectregulation;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Course getCourse() {
		return this.course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

}