package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_inv_suppliermaster database table.
 * 
 */
@Entity
@Table(name = "t_inv_suppliermaster")
@NamedQuery(name = "InvSuppliermaster.findAll", query = "SELECT i FROM InvSuppliermaster i")
public class InvSuppliermaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_supplier_id")
	private Long supplierId;

	@Column(name = "contact1_email")
	private String contact1Email;

	@Column(name = "contact1_name")
	private String contact1Name;

	@Column(name = "contact1_phone")
	private String contact1Phone;

	@Column(name = "contact2_email")
	private String contact2Email;

	@Column(name = "contact2_name")
	private String contact2Name;

	@Column(name = "contact2_phone")
	private String contact2Phone;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	private String cstno;

	@Temporal(TemporalType.TIMESTAMP)
	private Date enddate;

	// bi-directional many-to-one association to City
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_city_id")
	private City city;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_state_id")
	private State state;

	private String gstno;

	@Column(name = "is_active")
	private Boolean isActive;

	private String offaddline1;

	private String offaddline2;

	@Column(name = "office_email")
	private String officeEmail;

	@Column(name = "office_fax")
	private String officeFax;

	@Column(name = "office_phone")
	private String officePhone;

	@Column(name = "office_pincode")
	private String officePincode;

	@Column(name = "office_website")
	private String officeWebsite;

	private Integer rating;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	private Date startdate;

	@Column(name = "supplier_name")
	private String supplierName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;
/*
	// bi-directional many-to-one association to InvPurchaseOrder
	@OneToMany(mappedBy = "TInvSuppliermaster")
	private List<InvPurchaseOrder> invPurchaseOrders;

	// bi-directional many-to-one association to InvPurchasereturn
	@OneToMany(mappedBy = "TInvSuppliermaster")
	private List<InvPurchasereturn> invPurchasereturns;

	// bi-directional many-to-one association to InvSrv
	@OneToMany(mappedBy = "TInvSuppliermaster")
	private List<InvSrv> invSrvs;
*/
	public InvSuppliermaster() {
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public String getContact1Email() {
		return contact1Email;
	}

	public void setContact1Email(String contact1Email) {
		this.contact1Email = contact1Email;
	}

	public String getContact1Name() {
		return contact1Name;
	}

	public void setContact1Name(String contact1Name) {
		this.contact1Name = contact1Name;
	}

	public String getContact1Phone() {
		return contact1Phone;
	}

	public void setContact1Phone(String contact1Phone) {
		this.contact1Phone = contact1Phone;
	}

	public String getContact2Email() {
		return contact2Email;
	}

	public void setContact2Email(String contact2Email) {
		this.contact2Email = contact2Email;
	}

	public String getContact2Name() {
		return contact2Name;
	}

	public void setContact2Name(String contact2Name) {
		this.contact2Name = contact2Name;
	}

	public String getContact2Phone() {
		return contact2Phone;
	}

	public void setContact2Phone(String contact2Phone) {
		this.contact2Phone = contact2Phone;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getCstno() {
		return cstno;
	}

	public void setCstno(String cstno) {
		this.cstno = cstno;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public String getGstno() {
		return gstno;
	}

	public void setGstno(String gstno) {
		this.gstno = gstno;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getOffaddline1() {
		return offaddline1;
	}

	public void setOffaddline1(String offaddline1) {
		this.offaddline1 = offaddline1;
	}

	public String getOffaddline2() {
		return offaddline2;
	}

	public void setOffaddline2(String offaddline2) {
		this.offaddline2 = offaddline2;
	}

	public String getOfficeEmail() {
		return officeEmail;
	}

	public void setOfficeEmail(String officeEmail) {
		this.officeEmail = officeEmail;
	}

	public String getOfficeFax() {
		return officeFax;
	}

	public void setOfficeFax(String officeFax) {
		this.officeFax = officeFax;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public String getOfficePincode() {
		return officePincode;
	}

	public void setOfficePincode(String officePincode) {
		this.officePincode = officePincode;
	}

	public String getOfficeWebsite() {
		return officeWebsite;
	}

	public void setOfficeWebsite(String officeWebsite) {
		this.officeWebsite = officeWebsite;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}
/*
	public List<InvPurchaseOrder> getInvPurchaseOrders() {
		return invPurchaseOrders;
	}

	public void setInvPurchaseOrders(List<InvPurchaseOrder> invPurchaseOrders) {
		this.invPurchaseOrders = invPurchaseOrders;
	}

	public List<InvPurchasereturn> getInvPurchasereturns() {
		return invPurchasereturns;
	}

	public void setInvPurchasereturns(List<InvPurchasereturn> invPurchasereturns) {
		this.invPurchasereturns = invPurchasereturns;
	}

	public List<InvSrv> getInvSrvs() {
		return invSrvs;
	}

	public void setInvSrvs(List<InvSrv> invSrvs) {
		this.invSrvs = invSrvs;
	}*/

}