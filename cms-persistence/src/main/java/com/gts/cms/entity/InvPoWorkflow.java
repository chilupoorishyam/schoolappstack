package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_inv_po_workflow database table.
 * 
 */
@Entity
@Table(name = "t_inv_po_workflow")
@NamedQuery(name = "InvPoWorkflow.findAll", query = "SELECT i FROM InvPoWorkflow i")
public class InvPoWorkflow implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_po_wf_id")
	private Long poWfId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_po_from_status_catdet_id")
	private GeneralDetail poFromStatusCatdetId;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_po_to_status_catdet_id")
	private GeneralDetail poToStatusCatdetId;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_status_emp_id")
	private EmployeeDetail statusEmp;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Column(name = "status_comments")
	private String statusComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "wf_stage_no")
	private Integer wfStageNo;

	// bi-directional many-to-one association to InvPurchaseOrder
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_po_id")
	private InvPurchaseOrder invPurchaseOrder;

	public InvPoWorkflow() {
	}

	public Long getPoWfId() {
		return this.poWfId;
	}

	public void setPoWfId(Long poWfId) {
		this.poWfId = poWfId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStatusComments() {
		return this.statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Integer getWfStageNo() {
		return this.wfStageNo;
	}

	public void setWfStageNo(Integer wfStageNo) {
		this.wfStageNo = wfStageNo;
	}

	public InvPurchaseOrder getInvPurchaseOrder() {
		return invPurchaseOrder;
	}

	public void setInvPurchaseOrder(InvPurchaseOrder invPurchaseOrder) {
		this.invPurchaseOrder = invPurchaseOrder;
	}

	public GeneralDetail getPoFromStatusCatdetId() {
		return poFromStatusCatdetId;
	}

	public void setPoFromStatusCatdetId(GeneralDetail poFromStatusCatdetId) {
		this.poFromStatusCatdetId = poFromStatusCatdetId;
	}

	public GeneralDetail getPoToStatusCatdetId() {
		return poToStatusCatdetId;
	}

	public void setPoToStatusCatdetId(GeneralDetail poToStatusCatdetId) {
		this.poToStatusCatdetId = poToStatusCatdetId;
	}

	public EmployeeDetail getStatusEmp() {
		return statusEmp;
	}

	public void setStatusEmp(EmployeeDetail statusEmp) {
		this.statusEmp = statusEmp;
	}

}