package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the t_exam_fee_fines database table.
 * 
 */
@Entity
@Table(name = "t_exam_fee_fines")
@NamedQuery(name = "ExamFeeFine.findAll", query = "SELECT e FROM ExamFeeFine e")
public class ExamFeeFine implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_fee_fine_id")
	private Long examFeeFineId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	@Column(name = "fine_from_date")
	private Date fineFromDate;

	@Column(name = "fine_name")
	private String fineName;

	@Temporal(TemporalType.DATE)
	@Column(name = "fine_to_date")
	private Date fineToDate;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Column(name = "reg_fee_fine")
	private BigDecimal regFeeFine;

	@Column(name = "supply_fee_fine")
	private BigDecimal supplyFeeFine;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to ExamMaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_id")
	private ExamMaster examMaster;

	// bi-directional many-to-one association to ExamFeeStructure
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_fee_structure_id")
	private ExamFeeStructure examFeeStructure;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	public ExamFeeFine() {
	}

	public Long getExamFeeFineId() {
		return this.examFeeFineId;
	}

	public void setExamFeeFineId(Long examFeeFineId) {
		this.examFeeFineId = examFeeFineId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getFineFromDate() {
		return this.fineFromDate;
	}

	public void setFineFromDate(Date fineFromDate) {
		this.fineFromDate = fineFromDate;
	}

	public String getFineName() {
		return this.fineName;
	}

	public void setFineName(String fineName) {
		this.fineName = fineName;
	}

	public Date getFineToDate() {
		return this.fineToDate;
	}

	public void setFineToDate(Date fineToDate) {
		this.fineToDate = fineToDate;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getRegFeeFine() {
		return this.regFeeFine;
	}

	public void setRegFeeFine(BigDecimal regFeeFine) {
		this.regFeeFine = regFeeFine;
	}

	public BigDecimal getSupplyFeeFine() {
		return this.supplyFeeFine;
	}

	public void setSupplyFeeFine(BigDecimal supplyFeeFine) {
		this.supplyFeeFine = supplyFeeFine;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public ExamMaster getExamMaster() {
		return examMaster;
	}

	public void setExamMaster(ExamMaster examMaster) {
		this.examMaster = examMaster;
	}

	public ExamFeeStructure getExamFeeStructure() {
		return examFeeStructure;
	}

	public void setExamFeeStructure(ExamFeeStructure examFeeStructure) {
		this.examFeeStructure = examFeeStructure;
	}
}