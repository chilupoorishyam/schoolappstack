package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_fee_instant_payments database table.
 * 
 */
@Entity
@Table(name="t_fee_instant_payments")
@NamedQuery(name="FeeInstantPayment.findAll", query="SELECT f FROM FeeInstantPayment f")
public class FeeInstantPayment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_instant_payment_id")
	private Long instantPaymentId;

	@Column(name="amount_paid")
	private BigDecimal amountPaid;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="payer_name")
	private String payerName;

	private String reason;

	@Column(name="total_amount")
	private BigDecimal totalAmount;

	@Column(name="total_discount")
	private BigDecimal totalDiscount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FeeInstantPaymentParticular
	@OneToMany(mappedBy="feeInstantPayment",fetch = FetchType.LAZY)
	private List<FeeInstantPaymentParticular> feeInstantPaymentParticulars;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_id")
	private EmployeeDetail employeeDetail;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_payer_type_catdet_id")
	private GeneralDetail payerType;

	//bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_student_id")
	private StudentDetail studentDetail;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_financial_year_id")
	private FinancialYear financialYear;

	//bi-directional many-to-one association to FeeReceipt
	@OneToMany(mappedBy="feeInstantPayment",fetch = FetchType.LAZY)
	private List<FeeReceipt> feeReceipts;

	public FeeInstantPayment() {
	}

	public Long getInstantPaymentId() {
		return this.instantPaymentId;
	}

	public void setInstantPaymentId(Long instantPaymentId) {
		this.instantPaymentId = instantPaymentId;
	}

	public BigDecimal getAmountPaid() {
		return this.amountPaid;
	}

	public void setAmountPaid(BigDecimal amountPaid) {
		this.amountPaid = amountPaid;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getPayerName() {
		return this.payerName;
	}

	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getTotalAmount() {
		return this.totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getTotalDiscount() {
		return this.totalDiscount;
	}

	public void setTotalDiscount(BigDecimal totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<FeeInstantPaymentParticular> getFeeInstantPaymentParticulars() {
		return this.feeInstantPaymentParticulars;
	}

	public void setFeeInstantPaymentParticulars(List<FeeInstantPaymentParticular> feeInstantPaymentParticulars) {
		this.feeInstantPaymentParticulars = feeInstantPaymentParticulars;
	}

	public FeeInstantPaymentParticular addFeeInstantPaymentParticular(FeeInstantPaymentParticular feeInstantPaymentParticular) {
		getFeeInstantPaymentParticulars().add(feeInstantPaymentParticular);
		feeInstantPaymentParticular.setFeeInstantPayment(this);

		return feeInstantPaymentParticular;
	}

	public FeeInstantPaymentParticular removeFeeInstantPaymentParticular(FeeInstantPaymentParticular feeInstantPaymentParticular) {
		getFeeInstantPaymentParticulars().remove(feeInstantPaymentParticular);
		feeInstantPaymentParticular.setFeeInstantPayment(null);

		return feeInstantPaymentParticular;
	}

	public EmployeeDetail getEmployeeDetail() {
		return this.employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getPayerType() {
		return this.payerType;
	}

	public void setPayerType(GeneralDetail payerType) {
		this.payerType = payerType;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public List<FeeReceipt> getFeeReceipts() {
		return this.feeReceipts;
	}

	public void setFeeReceipts(List<FeeReceipt> feeReceipts) {
		this.feeReceipts = feeReceipts;
	}

	public FeeReceipt addFeeReceipt(FeeReceipt feeReceipt) {
		getFeeReceipts().add(feeReceipt);
		feeReceipt.setFeeInstantPayment(this);

		return feeReceipt;
	}

	public FeeReceipt removeFeeReceipt(FeeReceipt feeReceipt) {
		getFeeReceipts().remove(feeReceipt);
		feeReceipt.setFeeInstantPayment(null);

		return feeReceipt;
	}

	public FinancialYear getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}
	
}