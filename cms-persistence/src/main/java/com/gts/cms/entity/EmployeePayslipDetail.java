package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the t_hr_employee_payslip_details database table.
 * 
 */
@Entity
@Table(name="t_hr_employee_payslip_details")
@NamedQuery(name="EmployeePayslipDetail.findAll", query="SELECT e FROM EmployeePayslipDetail e")
public class EmployeePayslipDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_payslip_detail_id")
	private Long payslipDetailId;

	private BigDecimal amount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeePayslipGeneration
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_payslip_generation_id")
	private EmployeePayslipGeneration employeePayslipGeneration;

	//bi-directional many-to-one association to PayrollCategory
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_payroll_category_id")
	private PayrollCategory payrollCategory;

	//bi-directional many-to-one association to PayrollCategoryGroup
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_payroll_category_group_id")
	private PayrollCategoryGroup payrollCategoryGroup;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	public EmployeePayslipDetail() {
	}

	public Long getPayslipDetailId() {
		return this.payslipDetailId;
	}

	public void setPayslipDetailId(Long payslipDetailId) {
		this.payslipDetailId = payslipDetailId;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeePayslipGeneration getEmployeePayslipGeneration() {
		return this.employeePayslipGeneration;
	}

	public void setEmployeePayslipGeneration(EmployeePayslipGeneration employeePayslipGeneration) {
		this.employeePayslipGeneration = employeePayslipGeneration;
	}

	public PayrollCategory getPayrollCategory() {
		return this.payrollCategory;
	}

	public void setPayrollCategory(PayrollCategory payrollCategory) {
		this.payrollCategory = payrollCategory;
	}

	public PayrollCategoryGroup getPayrollCategoryGroup() {
		return this.payrollCategoryGroup;
	}

	public void setPayrollCategoryGroup(PayrollCategoryGroup payrollCategoryGroup) {
		this.payrollCategoryGroup = payrollCategoryGroup;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

}