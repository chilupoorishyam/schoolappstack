package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_subjects database table.
 * 
 */
@Entity
@Table(name="t_m_subjects")
@NamedQuery(name="Subject.findAll", query="SELECT s FROM Subject s")
public class Subject implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_subject_id")
	private Long subjectId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_language")
	private Boolean isLanguage;

	@Column(name="order_no")
	private Integer orderNo;

	private String reason;

	@Column(name="short_name")
	private String shortName;

	@Column(name="sub_credit_hrs")
	private Integer subCreditHrs;

	@Column(name="sub_credits")
	private Integer subCredits;

	@Column(name="subject_code")
	private String subjectCode;

	@Column(name="subject_name")
	private String subjectName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to BatchwiseStudent
	@OneToMany(mappedBy="subject",fetch = FetchType.LAZY)
	private List<BatchwiseStudent> batchwiseStudents;

	//bi-directional many-to-one association to ElectiveGroupyrMapping
	@OneToMany(mappedBy="subject",fetch = FetchType.LAZY)
	private List<ElectiveGroupyrMapping> electiveGroupyrMappings;

	//bi-directional many-to-one association to GroupyrRegulationDetail
	@OneToMany(mappedBy="subject",fetch = FetchType.LAZY)
	private List<GroupyrRegulationDetail> groupyrRegulationDetails;

	/*//bi-directional many-to-one association to SubjectBook
	@OneToMany(mappedBy="subject")
	private List<SubjectBook> subjectBooks;
*/
	//bi-directional many-to-one association to SubjectCourseyear
	@OneToMany(mappedBy="subject",fetch = FetchType.LAZY)
	private List<SubjectCourseyear> subjectCourseyears;

	//bi-directional many-to-one association to SubjectUnitTopic
	/*@OneToMany(mappedBy="subject")
	private List<SubjectUnitTopic> subjectUnitTopics;

	//bi-directional many-to-one association to SubjectUnit
	@OneToMany(mappedBy="subject")
	private List<SubjectUnit> subjectUnits;*/

	//bi-directional many-to-one association to Subjectregulation
	@OneToMany(mappedBy="subject",fetch = FetchType.LAZY)
	private List<Subjectregulation> subjectregulations;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to Course
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_id")
	private Course course;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subjecttype_catdet_id")
	private GeneralDetail subjecttype;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subjectcategory_catdet_id")
	private GeneralDetail subjectCategory;

	//bi-directional many-to-one association to StudentSubject
	@OneToMany(mappedBy="subject",fetch = FetchType.LAZY)
	private List<StudentSubject> studentSubjects;

	public Subject() {
	}

	public Long getSubjectId() {
		return this.subjectId;
	}

	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsLanguage() {
		return this.isLanguage;
	}

	public void setIsLanguage(Boolean isLanguage) {
		this.isLanguage = isLanguage;
	}

	public Integer getOrderNo() {
		return this.orderNo;
	}

	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getShortName() {
		return this.shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public Integer getSubCreditHrs() {
		return this.subCreditHrs;
	}

	public void setSubCreditHrs(Integer subCreditHrs) {
		this.subCreditHrs = subCreditHrs;
	}

	public Integer getSubCredits() {
		return this.subCredits;
	}

	public void setSubCredits(Integer subCredits) {
		this.subCredits = subCredits;
	}

	public String getSubjectCode() {
		return this.subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	public String getSubjectName() {
		return this.subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<BatchwiseStudent> getBatchwiseStudents() {
		return this.batchwiseStudents;
	}

	public void setBatchwiseStudents(List<BatchwiseStudent> batchwiseStudents) {
		this.batchwiseStudents = batchwiseStudents;
	}

	public BatchwiseStudent addBatchwiseStudent(BatchwiseStudent batchwiseStudent) {
		getBatchwiseStudents().add(batchwiseStudent);
		batchwiseStudent.setSubject(this);

		return batchwiseStudent;
	}

	public BatchwiseStudent removeBatchwiseStudent(BatchwiseStudent batchwiseStudent) {
		getBatchwiseStudents().remove(batchwiseStudent);
		batchwiseStudent.setSubject(null);

		return batchwiseStudent;
	}

	public List<ElectiveGroupyrMapping> getElectiveGroupyrMappings() {
		return this.electiveGroupyrMappings;
	}

	public void setElectiveGroupyrMappings(List<ElectiveGroupyrMapping> electiveGroupyrMappings) {
		this.electiveGroupyrMappings = electiveGroupyrMappings;
	}

	public ElectiveGroupyrMapping addElectiveGroupyrMapping(ElectiveGroupyrMapping electiveGroupyrMapping) {
		getElectiveGroupyrMappings().add(electiveGroupyrMapping);
		electiveGroupyrMapping.setSubject(this);

		return electiveGroupyrMapping;
	}

	public ElectiveGroupyrMapping removeElectiveGroupyrMapping(ElectiveGroupyrMapping electiveGroupyrMapping) {
		getElectiveGroupyrMappings().remove(electiveGroupyrMapping);
		electiveGroupyrMapping.setSubject(null);

		return electiveGroupyrMapping;
	}

	public List<GroupyrRegulationDetail> getGroupyrRegulationDetails() {
		return this.groupyrRegulationDetails;
	}

	public void setGroupyrRegulationDetails(List<GroupyrRegulationDetail> groupyrRegulationDetails) {
		this.groupyrRegulationDetails = groupyrRegulationDetails;
	}

	public GroupyrRegulationDetail addGroupyrRegulationDetail(GroupyrRegulationDetail groupyrRegulationDetail) {
		getGroupyrRegulationDetails().add(groupyrRegulationDetail);
		groupyrRegulationDetail.setSubject(this);

		return groupyrRegulationDetail;
	}

	public GroupyrRegulationDetail removeGroupyrRegulationDetail(GroupyrRegulationDetail groupyrRegulationDetail) {
		getGroupyrRegulationDetails().remove(groupyrRegulationDetail);
		groupyrRegulationDetail.setSubject(null);

		return groupyrRegulationDetail;
	}

	/*public List<SubjectBook> getSubjectBooks() {
		return this.subjectBooks;
	}

	public void setSubjectBooks(List<SubjectBook> subjectBooks) {
		this.subjectBooks = subjectBooks;
	}*/

	public List<SubjectCourseyear> getSubjectCourseyears() {
		return this.subjectCourseyears;
	}

	public void setSubjectCourseyears(List<SubjectCourseyear> subjectCourseyears) {
		this.subjectCourseyears = subjectCourseyears;
	}

	public SubjectCourseyear addSubjectCourseyear(SubjectCourseyear subjectCourseyear) {
		getSubjectCourseyears().add(subjectCourseyear);
		subjectCourseyear.setSubject(this);

		return subjectCourseyear;
	}

	public SubjectCourseyear removeSubjectCourseyear(SubjectCourseyear subjectCourseyear) {
		getSubjectCourseyears().remove(subjectCourseyear);
		subjectCourseyear.setSubject(null);

		return subjectCourseyear;
	}

	/*public List<SubjectUnitTopic> getSubjectUnitTopics() {
		return this.subjectUnitTopics;
	}

	public void setSubjectUnitTopics(List<SubjectUnitTopic> subjectUnitTopics) {
		this.subjectUnitTopics = subjectUnitTopics;
	}

	public List<SubjectUnit> getSubjectUnits() {
		return this.subjectUnits;
	}

	public void setSubjectUnits(List<SubjectUnit> subjectUnits) {
		this.subjectUnits = subjectUnits;
	}*/

	public List<Subjectregulation> getSubjectregulations() {
		return this.subjectregulations;
	}

	public void setSubjectregulations(List<Subjectregulation> subjectregulations) {
		this.subjectregulations = subjectregulations;
	}

	public Subjectregulation addSubjectregulation(Subjectregulation subjectregulation) {
		getSubjectregulations().add(subjectregulation);
		subjectregulation.setSubject(this);

		return subjectregulation;
	}

	public Subjectregulation removeSubjectregulation(Subjectregulation subjectregulation) {
		getSubjectregulations().remove(subjectregulation);
		subjectregulation.setSubject(null);

		return subjectregulation;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Course getCourse() {
		return this.course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public GeneralDetail getSubjecttype() {
		return this.subjecttype;
	}

	public void setSubjecttype(GeneralDetail subjecttype) {
		this.subjecttype = subjecttype;
	}

	public GeneralDetail getSubjectCategory() {
		return this.subjectCategory;
	}

	public void setSubjectCategory(GeneralDetail subjectCategory) {
		this.subjectCategory = subjectCategory;
	}

	public List<StudentSubject> getStudentSubjects() {
		return this.studentSubjects;
	}

	public void setStudentSubjects(List<StudentSubject> studentSubjects) {
		this.studentSubjects = studentSubjects;
	}

	public StudentSubject addStudentSubject(StudentSubject studentSubject) {
		getStudentSubjects().add(studentSubject);
		studentSubject.setSubject(this);

		return studentSubject;
	}

	public StudentSubject removeStudentSubject(StudentSubject studentSubject) {
		getStudentSubjects().remove(studentSubject);
		studentSubject.setSubject(null);

		return studentSubject;
	}

}