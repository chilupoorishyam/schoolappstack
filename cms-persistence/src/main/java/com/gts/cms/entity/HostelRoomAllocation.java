package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_hstl_room_allocation database table.
 * 
 */
@Entity
@Table(name = "t_hstl_room_allocation")
@NamedQuery(name = "HostelRoomAllocation.findAll", query = "SELECT h FROM HostelRoomAllocation h")
public class HostelRoomAllocation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_hstl_room_allot_id")
	private Long hstlRoomAllotId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "from_date")
	private Date fromDate;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_amount_setteled")
	private Boolean isAmountSetteled;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "payment_due_date")
	private Date paymentDueDate;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "to_date")
	private Date toDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "vacate_comments")
	private String vacateComments;

	// bi-directional many-to-one association to HstlFeePayment
	@OneToMany(mappedBy = "hstlRoomAllocation")
	private List<HostelFeePayment> hstlFeePayments;

	// bi-directional many-to-one association to HostelDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_hstl_hostel_id")
	private HostelDetail hstlHostelDetail;

	// bi-directional many-to-one association to HstlRoom
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_hstl_room_id")
	private HostelRoom hstlRoom;

	// bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_emp_id")
	private EmployeeDetail employeeDetail;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	public Long getHstlRoomAllotId() {
		return hstlRoomAllotId;
	}

	public void setHstlRoomAllotId(Long hstlRoomAllotId) {
		this.hstlRoomAllotId = hstlRoomAllotId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsAmountSetteled() {
		return isAmountSetteled;
	}

	public void setIsAmountSetteled(Boolean isAmountSetteled) {
		this.isAmountSetteled = isAmountSetteled;
	}

	public Date getPaymentDueDate() {
		return paymentDueDate;
	}

	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getVacateComments() {
		return vacateComments;
	}

	public void setVacateComments(String vacateComments) {
		this.vacateComments = vacateComments;
	}

	public List<HostelFeePayment> getHstlFeePayments() {
		return hstlFeePayments;
	}

	public void setHstlFeePayments(List<HostelFeePayment> hstlFeePayments) {
		this.hstlFeePayments = hstlFeePayments;
	}

	public HostelDetail getHstlHostelDetail() {
		return hstlHostelDetail;
	}

	public void setHstlHostelDetail(HostelDetail hstlHostelDetail) {
		this.hstlHostelDetail = hstlHostelDetail;
	}

	public HostelRoom getHstlRoom() {
		return hstlRoom;
	}

	public void setHstlRoom(HostelRoom hstlRoom) {
		this.hstlRoom = hstlRoom;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public EmployeeDetail getEmployeeDetail() {
		return employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

}