package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the t_fee_studentwise_discount database table.
 * 
 */
@Entity
@Table(name="t_fee_studentwise_discount")
@NamedQuery(name="FeeStudentwiseDiscount.findAll", query="SELECT f FROM FeeStudentwiseDiscount f")
public class FeeStudentwiseDiscount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_std_discount_id")
	private Long feeStdDiscountId;

	@Column(name="auth_comments")
	private String authComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String description;

	@Column(name="discount_mode")
	private String discountMode;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_aproved")
	private Boolean isAproved;

	@Column(name="is_rejected")
	private Boolean isRejected;

	private String reason;

	@Column(name="ref_file_path1")
	private String refFilePath1;

	@Column(name="ref_file_path2")
	private String refFilePath2;

	@Column(name="ref_file_path3")
	private String refFilePath3;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	private Integer value;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_requestedto_emp_id")
	private EmployeeDetail requestedtoEmployeeDetail;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_auth_by_emp_id")
	private EmployeeDetail authbyEmployee;

	//bi-directional many-to-one association to FeeCategory
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_fee_category_id")
	private FeeCategory feeCategory;

	//bi-directional many-to-one association to FeeDiscount
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_fee_discount_id")
	private FeeDiscount feeDiscount;

	//bi-directional many-to-one association to FeeParticular
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_fee_particulars_id")
	private FeeParticular feeParticular;

	//bi-directional many-to-one association to FeeStructure
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_fee_structure_id")
	private FeeStructure feeStructure;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_student_id")
	private StudentDetail studentDetail;

	public FeeStudentwiseDiscount() {
	}

	public Long getFeeStdDiscountId() {
		return this.feeStdDiscountId;
	}

	public void setFeeStdDiscountId(Long feeStdDiscountId) {
		this.feeStdDiscountId = feeStdDiscountId;
	}

	public String getAuthComments() {
		return this.authComments;
	}

	public void setAuthComments(String authComments) {
		this.authComments = authComments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDiscountMode() {
		return this.discountMode;
	}

	public void setDiscountMode(String discountMode) {
		this.discountMode = discountMode;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsAproved() {
		return this.isAproved;
	}

	public void setIsAproved(Boolean isAproved) {
		this.isAproved = isAproved;
	}

	public Boolean getIsRejected() {
		return this.isRejected;
	}

	public void setIsRejected(Boolean isRejected) {
		this.isRejected = isRejected;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRefFilePath1() {
		return this.refFilePath1;
	}

	public void setRefFilePath1(String refFilePath1) {
		this.refFilePath1 = refFilePath1;
	}

	public String getRefFilePath2() {
		return this.refFilePath2;
	}

	public void setRefFilePath2(String refFilePath2) {
		this.refFilePath2 = refFilePath2;
	}

	public String getRefFilePath3() {
		return this.refFilePath3;
	}

	public void setRefFilePath3(String refFilePath3) {
		this.refFilePath3 = refFilePath3;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Integer getValue() {
		return this.value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public EmployeeDetail getRequestedtoEmployeeDetail() {
		return this.requestedtoEmployeeDetail;
	}

	public void setRequestedtoEmployeeDetail(EmployeeDetail requestedtoEmployeeDetail) {
		this.requestedtoEmployeeDetail = requestedtoEmployeeDetail;
	}

	public EmployeeDetail getAuthbyEmployee() {
		return this.authbyEmployee;
	}

	public void setAuthbyEmployee(EmployeeDetail authbyEmployee) {
		this.authbyEmployee = authbyEmployee;
	}

	public FeeCategory getFeeCategory() {
		return this.feeCategory;
	}

	public void setFeeCategory(FeeCategory feeCategory) {
		this.feeCategory = feeCategory;
	}

	public FeeDiscount getFeeDiscount() {
		return this.feeDiscount;
	}

	public void setFeeDiscount(FeeDiscount feeDiscount) {
		this.feeDiscount = feeDiscount;
	}

	public FeeParticular getFeeParticular() {
		return this.feeParticular;
	}

	public void setFeeParticular(FeeParticular feeParticular) {
		this.feeParticular = feeParticular;
	}

	public FeeStructure getFeeStructure() {
		return this.feeStructure;
	}

	public void setFeeStructure(FeeStructure feeStructure) {
		this.feeStructure = feeStructure;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

}