package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


@Data
@Entity
@Table(name = "t_stg_onl_fee_particularwise_payments")
@NamedQuery(name = "StgOnlineFeeParticularwisePayment.findAll", query = "SELECT f FROM StgOnlineFeeParticularwisePayment f")
public class StgOnlineFeeParticularwisePayment implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_onl_fee_particularwise_payment_id")
    private Long feeParticularwisePaymentId;

    private BigDecimal amount;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "discount_amount")
    private BigDecimal discountAmount;

    @Column(name = "fine_amount")
    private BigDecimal fineAmount;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "paid_amount")
    private BigDecimal paidAmount;

    @Column(name = "payer_name")
    private String payerName;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    //bi-directional many-to-one association to EmployeeDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_emp_id")
    private EmployeeDetail employeeDetail;

    //bi-directional many-to-one association to FeeParticular
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fk_fee_perticulars_id")
    private FeeParticular feeParticular;

    //bi-directional many-to-one association to FeeStructure
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_fee_structure_id")
    private FeeStructure feeStructure;

    //bi-directional many-to-one association to FeeStructureParticular
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_fee_structure_particular_id")
    private FeeStructureParticular feeStructureParticular;

    //bi-directional many-to-one association to FeeStudentWiseParticular
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_fee_std_particular_id")
    private FeeStudentWiseParticular feeStudentWiseParticular;

    //bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_school_id")
    private School school;

    //bi-directional many-to-one association to StudentDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_student_id")
    private StudentDetail studentDetail;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_financial_year_id")
    private FinancialYear financialYear;

    //bi-directional many-to-one association to FeeReceipt
	/*@OneToMany(mappedBy="feeParticularwisePayment")
	private List<FeeReceipt> feeReceipts;*/
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fk_fee_receipts_id")
    private StgOnlineFeeReceipt feeReceipt;


    @Column(name = "refund_amount")
    private BigDecimal refundAmount;

    @Column(name = "scholarship_amount")
    private BigDecimal scholarshipAmount;

    @Column(name = "net_amount")
    private BigDecimal netAmount;

    @Column(name = "balance_amount")
    private BigDecimal balanceAmount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_fee_std_data_particulars_id")
    private FeeStudentDataParticular feeStudentDataParticular;
}