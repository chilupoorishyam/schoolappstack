package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the t_inv_internal_indent database table.
 * 
 */
@Entity
@Table(name="t_inv_internal_indent")
@NamedQuery(name="InvInternalIndent.findAll", query="SELECT i FROM InvInternalIndent i")
public class InvInternalIndent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_internal_ind_id")
	private Long internalIndId;

	@Column(name="authorization_comments")
	private String authorizationComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="authorization_date")
	private Date authorizationDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;
	
	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_authorized_by_emp_id")
	private EmployeeDetail authorizedByEmpId;
	
	// bi-directional many-to-one association to Department
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_destination_dept_id")
	private Department department;
	
	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_destination_emp_id")
	private EmployeeDetail destinationEmpId;
	
	// bi-directional many-to-one association to Room
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_destination_room_id")
	private Room destinationRoomId;
	
	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_indent_raised_emp_id")
	private EmployeeDetail indentRaisedEmpId;
	
	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_inv_transtype_catdet_id")
	private GeneralDetail invTranstypeCatdetId;
	
	// bi-directional many-to-one association to Department
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_source_dept_id")
	private Department sourceDeptId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="indent_date")
	private Date indentDate;

	@Column(name="internal_ind_no")
	private String internalIndNo;

	@Column(name="is_active")
	private Boolean isActive;

	private String purpose;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to InvStoresmaster
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_store_id")
	private InvStoresmaster invStoresmaster;

	@OneToMany(mappedBy="invInternalIndent",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<InvInternalIndentitem> invInternalIndentitems;
	
	/*	//bi-directional many-to-one association to InvInternalIssue
	@OneToMany(mappedBy="TInvInternalIndent")
	private List<InvInternalIssue> invInternalIssues;*/

	public InvInternalIndent() {
	}

	public Long getInternalIndId() {
		return this.internalIndId;
	}

	public void setInternalIndId(Long internalIndId) {
		this.internalIndId = internalIndId;
	}

	public String getAuthorizationComments() {
		return this.authorizationComments;
	}

	public void setAuthorizationComments(String authorizationComments) {
		this.authorizationComments = authorizationComments;
	}

	public Date getAuthorizationDate() {
		return this.authorizationDate;
	}

	public void setAuthorizationDate(Date authorizationDate) {
		this.authorizationDate = authorizationDate;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}
	
	public EmployeeDetail getAuthorizedByEmpId() {
		return authorizedByEmpId;
	}

	public void setAuthorizedByEmpId(EmployeeDetail authorizedByEmpId) {
		this.authorizedByEmpId = authorizedByEmpId;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public EmployeeDetail getDestinationEmpId() {
		return destinationEmpId;
	}

	public void setDestinationEmpId(EmployeeDetail destinationEmpId) {
		this.destinationEmpId = destinationEmpId;
	}

	public Room getDestinationRoomId() {
		return destinationRoomId;
	}

	public void setDestinationRoomId(Room destinationRoomId) {
		this.destinationRoomId = destinationRoomId;
	}

	public EmployeeDetail getIndentRaisedEmpId() {
		return indentRaisedEmpId;
	}

	public void setIndentRaisedEmpId(EmployeeDetail indentRaisedEmpId) {
		this.indentRaisedEmpId = indentRaisedEmpId;
	}

	public GeneralDetail getInvTranstypeCatdetId() {
		return invTranstypeCatdetId;
	}

	public void setInvTranstypeCatdetId(GeneralDetail invTranstypeCatdetId) {
		this.invTranstypeCatdetId = invTranstypeCatdetId;
	}

	public Department getSourceDeptId() {
		return sourceDeptId;
	}

	public void setSourceDeptId(Department sourceDeptId) {
		this.sourceDeptId = sourceDeptId;
	}

	public Date getIndentDate() {
		return this.indentDate;
	}

	public void setIndentDate(Date indentDate) {
		this.indentDate = indentDate;
	}

	public String getInternalIndNo() {
		return this.internalIndNo;
	}

	public void setInternalIndNo(String internalIndNo) {
		this.internalIndNo = internalIndNo;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getPurpose() {
		return this.purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public InvStoresmaster getInvStoresmaster() {
		return invStoresmaster;
	}

	public void setInvStoresmaster(InvStoresmaster invStoresmaster) {
		this.invStoresmaster = invStoresmaster;
	}

	public List<InvInternalIndentitem> getInvInternalIndentitems() {
		return invInternalIndentitems;
	}

	public void setInvInternalIndentitems(List<InvInternalIndentitem> invInternalIndentitems) {
		this.invInternalIndentitems = invInternalIndentitems;
	}

/*	public InvStoresmaster getInvStoresmaster() {
		return invStoresmaster;
	}

	public void setInvStoresmaster(InvStoresmaster invStoresmaster) {
		this.invStoresmaster = invStoresmaster;
	}

	public List<InvInternalIssue> getInvInternalIssues() {
		return invInternalIssues;
	}

	public void setInvInternalIssues(List<InvInternalIssue> invInternalIssues) {
		this.invInternalIssues = invInternalIssues;
	}*/
	
}