package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_cm_outcomes_mapping database
 * table.
 */
@Data
@Entity
@Table(name = "t_cm_outcomes_mapping")
@NamedQuery(name = "CMOutcomeMapping.findAll", query = "SELECT e FROM CmOutcomeMapping e")
public class CmOutcomeMapping implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_outcomes_map_id")
    private Long outcomesMapId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    private Double credits;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "inserted_dt")
    private Date insertedDt;

    @Column(name = "inserted_user")
    private Long insertedUser;

    // bi-directional many-to-one association to TMSchool
    @ManyToOne
    @JoinColumn(name = "fk_school_id")
    private School school;

    @ManyToOne
    @JoinColumn(name = "fk_academic_year_id")
    private AcademicYear academicYear;

    // bi-directional many-to-one association to TMCourseYear
    @ManyToOne
    @JoinColumn(name = "fk_course_year_id")
    private CourseYear courseYear;



    // bi-directional many-to-one association to TMSubject
    @ManyToOne
    @JoinColumn(name = "fk_subject_id")
    private Subject subject;

    // bi-directional many-to-one association to TMSubject
    @ManyToOne
    @JoinColumn(name = "fk_emp_id")
    private EmployeeDetail employeeDetail;

    // bi-directional many-to-one association to TMSubject
    @ManyToOne
    @JoinColumn(name = "fk_program_outcome_id")
    private CmProgramOutcome cmProgramOutcome;

    @ManyToOne
    @JoinColumn(name = "fk_course_outcome_id")
    private CmCourseOutcome cmCourseOutcome;

}