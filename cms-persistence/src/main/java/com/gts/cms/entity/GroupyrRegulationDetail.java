package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_cm_groupyr_regulation_details database table.
 * 
 */
@Entity
@Table(name="t_cm_groupyr_regulation_details")
@NamedQuery(name="GroupyrRegulationDetail.findAll", query="SELECT g FROM GroupyrRegulationDetail g")
public class GroupyrRegulationDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_groupyr_reg_detail_id")
	private Long groupyrRegDetailId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private Integer credits;

	private Integer externalmarks;

	private Integer internalmarks;

	@Column(name="is_active")
	private Boolean isActive;

	private Integer lectures;

	private Integer practicals;

	private String reason;

	private int tutorials;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;



	//bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_year_id")
	private CourseYear courseYear;

	//bi-directional many-to-one association to Electivetype
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_electivetype_id")
	private Electivetype electivetype;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subjecttype_catdet_id")
	private GeneralDetail subjecttype;



	//bi-directional many-to-one association to Subject
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subject_id")
	private Subject subject;

	public GroupyrRegulationDetail() {
	}

	public Long getGroupyrRegDetailId() {
		return this.groupyrRegDetailId;
	}

	public void setGroupyrRegDetailId(Long groupyrRegDetailId) {
		this.groupyrRegDetailId = groupyrRegDetailId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Integer getCredits() {
		return this.credits;
	}

	public void setCredits(Integer credits) {
		this.credits = credits;
	}

	public Integer getExternalmarks() {
		return this.externalmarks;
	}

	public void setExternalmarks(Integer externalmarks) {
		this.externalmarks = externalmarks;
	}

	public Integer getInternalmarks() {
		return this.internalmarks;
	}

	public void setInternalmarks(Integer internalmarks) {
		this.internalmarks = internalmarks;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getLectures() {
		return this.lectures;
	}

	public void setLectures(Integer lectures) {
		this.lectures = lectures;
	}

	public Integer getPracticals() {
		return this.practicals;
	}

	public void setPracticals(Integer practicals) {
		this.practicals = practicals;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getTutorials() {
		return this.tutorials;
	}

	public void setTutorials(int tutorials) {
		this.tutorials = tutorials;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}



	public CourseYear getCourseYear() {
		return this.courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public Electivetype getElectivetype() {
		return this.electivetype;
	}

	public void setElectivetype(Electivetype electivetype) {
		this.electivetype = electivetype;
	}

	public GeneralDetail getSubjecttype() {
		return this.subjecttype;
	}

	public void setSubjecttype(GeneralDetail subjecttype) {
		this.subjecttype = subjecttype;
	}



	public Subject getSubject() {
		return this.subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

}