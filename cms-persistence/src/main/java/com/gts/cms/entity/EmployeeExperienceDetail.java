package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_emp_experience_details database table.
 * 
 */
@Entity
@Table(name="t_emp_experience_details")
@NamedQuery(name="EmployeeExperienceDetail.findAll", query="SELECT e FROM EmployeeExperienceDetail e")
public class EmployeeExperienceDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_emp_experiencedetail_id")
	private Long employeeExperiencedetailId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String designation;

	@Column(name="experience_detail")
	private String experienceDetail;

	@Column(name="experience_month")
	private Long experienceMonth;

	@Column(name="experience_year")
	private Long experienceYear;

	@Temporal(TemporalType.DATE)
	@Column(name="from_yr_month")
	private Date fromYrMonth;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="prevoius_institutions")
	private String prevoiusInstitutions;

	private String reason;

	private String subjects;

	@Temporal(TemporalType.DATE)
	@Column(name="to_yr_month")
	private Date toYrMonth;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_id")
	private EmployeeDetail employeeDetail;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	public EmployeeExperienceDetail() {
	}

	public Long getEmployeeExperiencedetailId() {
		return this.employeeExperiencedetailId;
	}

	public void setEmployeeExperiencedetailId(Long employeeExperiencedetailId) {
		this.employeeExperiencedetailId = employeeExperiencedetailId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDesignation() {
		return this.designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getExperienceDetail() {
		return this.experienceDetail;
	}

	public void setExperienceDetail(String experienceDetail) {
		this.experienceDetail = experienceDetail;
	}

	public Long getExperienceMonth() {
		return this.experienceMonth;
	}

	public void setExperienceMonth(Long experienceMonth) {
		this.experienceMonth = experienceMonth;
	}

	public Long getExperienceYear() {
		return this.experienceYear;
	}

	public void setExperienceYear(Long experienceYear) {
		this.experienceYear = experienceYear;
	}

	public Date getFromYrMonth() {
		return this.fromYrMonth;
	}

	public void setFromYrMonth(Date fromYrMonth) {
		this.fromYrMonth = fromYrMonth;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getPrevoiusInstitutions() {
		return this.prevoiusInstitutions;
	}

	public void setPrevoiusInstitutions(String prevoiusInstitutions) {
		this.prevoiusInstitutions = prevoiusInstitutions;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSubjects() {
		return this.subjects;
	}

	public void setSubjects(String subjects) {
		this.subjects = subjects;
	}

	public Date getToYrMonth() {
		return this.toYrMonth;
	}

	public void setToYrMonth(Date toYrMonth) {
		this.toYrMonth = toYrMonth;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getEmployeeDetail() {
		return this.employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

}