package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_ams_events database table.
 * 
 */
@Entity
@Table(name="t_ams_events")
@NamedQuery(name="AmsEvent.findAll", query="SELECT a FROM AmsEvent a")
public class AmsEvent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_ams_event_id")
	private Long amsEventId;

	@Column(name="cost_excludes")
	private String costExcludes;

	@Column(name="cost_includes")
	private String costIncludes;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="event_cost")
	private BigDecimal eventCost;

	@Column(name="event_description")
	private String eventDescription;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="event_end_date")
	private Date eventEndDate;

	@Column(name="event_incharge")
	private String eventIncharge;

	@Column(name="event_incharge_email")
	private String eventInchargeEmail;

	@Column(name="event_incharge_mobile")
	private String eventInchargeMobile;

	@Column(name="event_latitude")
	private BigDecimal eventLatitude;

	@Column(name="event_location")
	private String eventLocation;

	@Column(name="event_longitude")
	private BigDecimal eventLongitude;

	@Column(name="event_name")
	private String eventName;

	@Temporal(TemporalType.DATE)
	@Column(name="event_publish_date")
	private Date eventPublishDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="event_start_date")
	private Date eventStartDate;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	
	@Column(name="fk_related_group_ids")
	private String fkRelatedGroupIds;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_verifiedby_emp_id")
	private  EmployeeDetail employeedetail;


	@Column(name="going_list_std_ids")
	private String goingListStdIds;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_verifiedby_incharge")
	private Boolean isVerifiedbyIncharge;

	@Column(name="not_going_std_ids")
	private String notGoingStdIds;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	@Column(name="verification_comments")
	private String verificationComments;

	//bi-directional many-to-one association to AmsEventAlbum
	@OneToMany(mappedBy="amsEvent", fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<AmsEventAlbum> amsEventAlbums;

	//bi-directional many-to-one association to AmsEventComment
	@OneToMany(mappedBy="amsEvent", fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<AmsEventComment> amsEventComments;

	public AmsEvent() {
	}

	public Long getAmsEventId() {
		return this.amsEventId;
	}

	public void setAmsEventId(Long amsEventId) {
		this.amsEventId = amsEventId;
	}

	public String getCostExcludes() {
		return this.costExcludes;
	}

	public void setCostExcludes(String costExcludes) {
		this.costExcludes = costExcludes;
	}

	public String getCostIncludes() {
		return this.costIncludes;
	}

	public void setCostIncludes(String costIncludes) {
		this.costIncludes = costIncludes;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public BigDecimal getEventCost() {
		return this.eventCost;
	}

	public void setEventCost(BigDecimal eventCost) {
		this.eventCost = eventCost;
	}

	public String getEventDescription() {
		return this.eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	public Date getEventEndDate() {
		return this.eventEndDate;
	}

	public void setEventEndDate(Date eventEndDate) {
		this.eventEndDate = eventEndDate;
	}

	public String getEventIncharge() {
		return this.eventIncharge;
	}

	public void setEventIncharge(String eventIncharge) {
		this.eventIncharge = eventIncharge;
	}

	public String getEventInchargeEmail() {
		return this.eventInchargeEmail;
	}

	public void setEventInchargeEmail(String eventInchargeEmail) {
		this.eventInchargeEmail = eventInchargeEmail;
	}

	public String getEventInchargeMobile() {
		return this.eventInchargeMobile;
	}

	public void setEventInchargeMobile(String eventInchargeMobile) {
		this.eventInchargeMobile = eventInchargeMobile;
	}

	public BigDecimal getEventLatitude() {
		return this.eventLatitude;
	}

	public void setEventLatitude(BigDecimal eventLatitude) {
		this.eventLatitude = eventLatitude;
	}

	public String getEventLocation() {
		return this.eventLocation;
	}

	public void setEventLocation(String eventLocation) {
		this.eventLocation = eventLocation;
	}

	public BigDecimal getEventLongitude() {
		return this.eventLongitude;
	}

	public void setEventLongitude(BigDecimal eventLongitude) {
		this.eventLongitude = eventLongitude;
	}

	public String getEventName() {
		return this.eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Date getEventPublishDate() {
		return this.eventPublishDate;
	}

	public void setEventPublishDate(Date eventPublishDate) {
		this.eventPublishDate = eventPublishDate;
	}

	public Date getEventStartDate() {
		return this.eventStartDate;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public EmployeeDetail getEmployeedetail() {
		return employeedetail;
	}

	public void setEmployeedetail(EmployeeDetail employeedetail) {
		this.employeedetail = employeedetail;
	}

	public void setEventStartDate(Date eventStartDate) {
		this.eventStartDate = eventStartDate;
	}
	public String getFkRelatedGroupIds() {
		return this.fkRelatedGroupIds;
	}

	public void setFkRelatedGroupIds(String fkRelatedGroupIds) {
		this.fkRelatedGroupIds = fkRelatedGroupIds;
	}

	public String getGoingListStdIds() {
		return this.goingListStdIds;
	}

	public void setGoingListStdIds(String goingListStdIds) {
		this.goingListStdIds = goingListStdIds;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsVerifiedbyIncharge() {
		return this.isVerifiedbyIncharge;
	}

	public void setIsVerifiedbyIncharge(Boolean isVerifiedbyIncharge) {
		this.isVerifiedbyIncharge = isVerifiedbyIncharge;
	}

	public String getNotGoingStdIds() {
		return this.notGoingStdIds;
	}

	public void setNotGoingStdIds(String notGoingStdIds) {
		this.notGoingStdIds = notGoingStdIds;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getVerificationComments() {
		return this.verificationComments;
	}

	public void setVerificationComments(String verificationComments) {
		this.verificationComments = verificationComments;
	}

	public List<AmsEventAlbum> getAmsEventAlbums() {
		return this.amsEventAlbums;
	}

	public void setAmsEventAlbums(List<AmsEventAlbum> amsEventAlbums) {
		this.amsEventAlbums = amsEventAlbums;
	}

	public AmsEventAlbum addAmsEventAlbum(AmsEventAlbum amsEventAlbum) {
		getAmsEventAlbums().add(amsEventAlbum);
		amsEventAlbum.setAmsEvent(this);

		return amsEventAlbum;
	}

	public AmsEventAlbum removeAmsEventAlbum(AmsEventAlbum amsEventAlbum) {
		getAmsEventAlbums().remove(amsEventAlbum);
		amsEventAlbum.setAmsEvent(null);

		return amsEventAlbum;
	}

	public List<AmsEventComment> getAmsEventComments() {
		return this.amsEventComments;
	}

	public void setAmsEventComments(List<AmsEventComment> amsEventComments) {
		this.amsEventComments = amsEventComments;
	}

	public AmsEventComment addAmsEventComment(AmsEventComment amsEventComment) {
		getAmsEventComments().add(amsEventComment);
		amsEventComment.setAmsEvent(this);

		return amsEventComment;
	}

	public AmsEventComment removeAmsEventComment(AmsEventComment amsEventComment) {
		getAmsEventComments().remove(amsEventComment);
		amsEventComment.setAmsEvent(null);

		return amsEventComment;
	}

}