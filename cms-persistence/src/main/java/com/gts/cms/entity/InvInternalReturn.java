package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_inv_internal_returns database table.
 * 
 */
@Entity
@Table(name = "t_inv_internal_returns")
@NamedQuery(name = "InvInternalReturn.findAll", query = "SELECT i FROM InvInternalReturn i")
public class InvInternalReturn implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_inter_return_id")
	private Long interReturnId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "acknowledged_date")
	private Date acknowledgedDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_acknowledged_emp_id")
	private EmployeeDetail acknowledgedEmp;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_from_dept_id")
	private Department fromDept;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_from_emp_id")
	private EmployeeDetail fromEmp;

	// bi-directional many-to-one association to Room
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_from_room_id")
	private Room fromRoomId;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_inv_transtype_catdet_id")
	private GeneralDetail generalDetail;

	@Column(name = "internal_return_no")
	private String internalReturnNo;

	@Column(name = "is_acknowledged")
	private Boolean isAcknowledged;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "return_date")
	private Date returnDate;

	@Column(name = "return_purpose")
	private String returnPurpose;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;
	
	//bi-directional many-to-one association to InvInternalReturnItem
	@OneToMany(mappedBy="invInternalReturn",fetch=FetchType.LAZY,cascade=CascadeType.ALL) 
	private List<InvInternalReturnItem>  invInternalReturnItems;
	 

	// bi-directional many-to-one association to InvInternalIssue
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_internal_issue_id")
	private InvInternalIssue invInternalIssue;

	// bi-directional many-to-one association to InvStoresmaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_to_store_id")
	private InvStoresmaster invStoresmaster;

	public InvInternalReturn() {
	}

	public Long getInterReturnId() {
		return this.interReturnId;
	}

	public void setInterReturnId(Long interReturnId) {
		this.interReturnId = interReturnId;
	}

	public Date getAcknowledgedDate() {
		return this.acknowledgedDate;
	}

	public void setAcknowledgedDate(Date acknowledgedDate) {
		this.acknowledgedDate = acknowledgedDate;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getInternalReturnNo() {
		return this.internalReturnNo;
	}

	public void setInternalReturnNo(String internalReturnNo) {
		this.internalReturnNo = internalReturnNo;
	}

	public Boolean getIsAcknowledged() {
		return this.isAcknowledged;
	}

	public void setIsAcknowledged(Boolean isAcknowledged) {
		this.isAcknowledged = isAcknowledged;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getReturnDate() {
		return this.returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public String getReturnPurpose() {
		return this.returnPurpose;
	}

	public void setReturnPurpose(String returnPurpose) {
		this.returnPurpose = returnPurpose;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getAcknowledgedEmp() {
		return acknowledgedEmp;
	}

	public void setAcknowledgedEmp(EmployeeDetail acknowledgedEmp) {
		this.acknowledgedEmp = acknowledgedEmp;
	}

	public Department getFromDept() {
		return fromDept;
	}

	public void setFromDept(Department fromDept) {
		this.fromDept = fromDept;
	}

	public EmployeeDetail getFromEmp() {
		return fromEmp;
	}

	public void setFromEmp(EmployeeDetail fromEmp) {
		this.fromEmp = fromEmp;
	}

	public Room getFromRoomId() {
		return fromRoomId;
	}

	public void setFromRoomId(Room fromRoomId) {
		this.fromRoomId = fromRoomId;
	}

	public GeneralDetail getGeneralDetail() {
		return generalDetail;
	}

	public void setGeneralDetail(GeneralDetail generalDetail) {
		this.generalDetail = generalDetail;
	}

	public InvInternalIssue getInvInternalIssue() {
		return invInternalIssue;
	}

	public void setInvInternalIssue(InvInternalIssue invInternalIssue) {
		this.invInternalIssue = invInternalIssue;
	}

	public InvStoresmaster getInvStoresmaster() {
		return invStoresmaster;
	}

	public void setInvStoresmaster(InvStoresmaster invStoresmaster) {
		this.invStoresmaster = invStoresmaster;
	}

	public List<InvInternalReturnItem> getInvInternalReturnItems() {
		return invInternalReturnItems;
	}

	public void setInvInternalReturnItems(List<InvInternalReturnItem> invInternalReturnItems) {
		this.invInternalReturnItems = invInternalReturnItems;
	}
	
}