package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_ams_group_members database table.
 * 
 */
@Entity
@Table(name="t_ams_group_members")
@NamedQuery(name="AmsGroupMember.findAll", query="SELECT a FROM AmsGroupMember a")
public class AmsGroupMember implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	//bi-directional many-to-one association to Organization
			@ManyToOne(fetch = FetchType.LAZY)
			@JoinColumn(name="fk_org_id")
			private Organization organization;

			//bi-directional many-to-one association to EmployeeDetail
			@ManyToOne(fetch = FetchType.LAZY)
			@JoinColumn(name="fk_verifiedby_emp_id")
			private EmployeeDetail verifiedbyEmpId;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_verifiedby_incharge")
	private Boolean isVerifiedbyIncharge;

	@Temporal(TemporalType.DATE)
	@Column(name="member_join_date")
	private Date memberJoinDate;

	@Temporal(TemporalType.DATE)
	@Column(name="member_left_date")
	private Date memberLeftDate;

	@Id
	@Column(name="pk_ams_group_member_id")
	private Long amsGroupMemberId;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	@Column(name="verification_comments")
	private String verificationComments;

	//bi-directional many-to-one association to AmsProfileDetail
	@ManyToOne
	@JoinColumn(name="fk_ams_profile_id")
	private AmsProfileDetail amsProfileDetail;

	public AmsGroupMember() {
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public EmployeeDetail getVerifiedbyEmpId() {
		return verifiedbyEmpId;
	}

	public void setVerifiedbyEmpId(EmployeeDetail verifiedbyEmpId) {
		this.verifiedbyEmpId = verifiedbyEmpId;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsVerifiedbyIncharge() {
		return this.isVerifiedbyIncharge;
	}

	public void setIsVerifiedbyIncharge(Boolean isVerifiedbyIncharge) {
		this.isVerifiedbyIncharge = isVerifiedbyIncharge;
	}

	public Date getMemberJoinDate() {
		return this.memberJoinDate;
	}

	public void setMemberJoinDate(Date memberJoinDate) {
		this.memberJoinDate = memberJoinDate;
	}

	public Date getMemberLeftDate() {
		return this.memberLeftDate;
	}

	public void setMemberLeftDate(Date memberLeftDate) {
		this.memberLeftDate = memberLeftDate;
	}

	public Long getAmsGroupMemberId() {
		return this.amsGroupMemberId;
	}

	public void setAmsGroupMemberId(Long amsGroupMemberId) {
		this.amsGroupMemberId = amsGroupMemberId;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getVerificationComments() {
		return this.verificationComments;
	}

	public void setVerificationComments(String verificationComments) {
		this.verificationComments = verificationComments;
	}

	public AmsProfileDetail getAmsProfileDetail() {
		return this.amsProfileDetail;
	}

	public void setAmsProfileDetail(AmsProfileDetail amsProfileDetail) {
		this.amsProfileDetail = amsProfileDetail;
	}

}