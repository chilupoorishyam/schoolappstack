package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_cm_assignment_workflow database table.
 * 
 */
@Entity
@Table(name = "t_cm_assignment_workflow")
@NamedQuery(name = "AssignmentWorkflow.findAll", query = "SELECT a FROM AssignmentWorkflow a")
public class AssignmentWorkflow implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_assignment_wf_id")
	private Long assignmentWfId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Column(name = "status_comments")
	private String statusComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "status_updated_on")
	private Date statusUpdatedOn;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to StudentAssignment
	@ManyToOne
	@JoinColumn(name = "fk_student_assignment_id")
	private StudentAssignment studentAssignment;

	// bi-directional many-to-one association to TEmpDetail
	@ManyToOne
	@JoinColumn(name = "fk_status_updated_emp_id")
	private EmployeeDetail empDetail;

	// bi-directional many-to-one association to TMSchool
	@ManyToOne
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to TMWorkflowStage
	@ManyToOne
	@JoinColumn(name = "fk_wf_assignment_status_id")
	private WorkflowStage workflowStage;

	// bi-directional many-to-one association to TStdStudentDetail
	@ManyToOne
	@JoinColumn(name = "fk_status_updated_student_id")
	private StudentDetail studentDetail;

	public AssignmentWorkflow() {
	}

	public Long getAssignmentWfId() {
		return this.assignmentWfId;
	}

	public void setAssignmentWfId(Long assignmentWfId) {
		this.assignmentWfId = assignmentWfId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStatusComments() {
		return this.statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public Date getStatusUpdatedOn() {
		return this.statusUpdatedOn;
	}

	public void setStatusUpdatedOn(Date statusUpdatedOn) {
		this.statusUpdatedOn = statusUpdatedOn;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public StudentAssignment getStudentAssignment() {
		return this.studentAssignment;
	}

	public void setStudentAssignment(StudentAssignment studentAssignment) {
		this.studentAssignment = studentAssignment;
	}

	public EmployeeDetail getEmpDetail() {
		return empDetail;
	}

	public void setEmpDetail(EmployeeDetail empDetail) {
		this.empDetail = empDetail;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public WorkflowStage getWorkflowStage() {
		return workflowStage;
	}

	public void setWorkflowStage(WorkflowStage workflowStage) {
		this.workflowStage = workflowStage;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

}