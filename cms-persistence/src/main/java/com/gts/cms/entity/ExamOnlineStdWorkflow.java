package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_exam_online_std_workflow database table.
 */
@Data
@Entity
@Table(name = "t_exam_online_std_workflow")
@NamedQuery(name = "ExamOnlineStdWorkflow.findAll", query = "SELECT e FROM ExamOnlineStdWorkflow e")
public class ExamOnlineStdWorkflow implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_exam_onlinestd_wf_id")
    private Long examOnlinestdWfId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    // bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_school_id")
    private School school;

    // bi-directional many-to-one association to Course
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_exam_online_std_paper_id")
    private ExamOnlineStdPaper fkExamOnlineStdPaperId;

    // bi-directional many-to-one association to Course
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_status_updated_emp_id")
    private EmployeeDetail statusUpdatedEmpId;

    // bi-directional many-to-one association to Course
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_status_updated_student_id   ")
    private StudentDetail statusUpdatedStudentId;

    // bi-directional many-to-one association to Course
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_wf_exampapersubmission_status_id")
    private WorkflowStage wfExampapersubmissionStatus;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "status_updated_on")
    private Date statusUpdatedOn;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "status_comments")
    private String statusComments;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;
}