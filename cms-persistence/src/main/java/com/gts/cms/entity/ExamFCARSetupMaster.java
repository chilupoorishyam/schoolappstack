package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_exam_fcar_setup_master database table.
 * 
 */
@Entity
@Table(name = "t_exam_fcar_setup_master")
@NamedQuery(name = "ExamFCARSetupMaster.findAll", query = "SELECT e FROM ExamFCARSetupMaster e")
public class ExamFCARSetupMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_fcar_set_master_id")
	private Long examFCARSetMasterId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne
	@JoinColumn(name = "fk_school_id")
	private School school;

	@ManyToOne
	@JoinColumn(name = "fk_course_id")
	private Course course;

	@Column(name = "fk_regulation_ids")
	private String regulationIds;

	@ManyToOne
	@JoinColumn(name = "fk_resultvalidation_catdet_id")
	private GeneralDetail resultvalidationCat;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_havingoptions")
	private Boolean isHavingoptions;

	@Column(name = "mark_setup_name")
	private String markSetupName;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to ExamFCARSetupDetail
	@OneToMany(mappedBy = "examFcarSetupMaster")
	private List<ExamFCARSetupDetail> examFcarSetupDetails;

	public ExamFCARSetupMaster() {
	}

	public Long getExamFCARSetMasterId() {
		return this.examFCARSetMasterId;
	}

	public void setExamFCARSetMasterId(Long examFCARSetMasterId) {
		this.examFCARSetMasterId = examFCARSetMasterId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public String getRegulationIds() {
		return regulationIds;
	}

	public void setRegulationIds(String regulationIds) {
		this.regulationIds = regulationIds;
	}

	public GeneralDetail getResultvalidationCat() {
		return resultvalidationCat;
	}

	public void setResultvalidationCat(GeneralDetail resultvalidationCat) {
		this.resultvalidationCat = resultvalidationCat;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsHavingoptions() {
		return isHavingoptions;
	}

	public void setIsHavingoptions(Boolean isHavingoptions) {
		this.isHavingoptions = isHavingoptions;
	}

	public String getMarkSetupName() {
		return markSetupName;
	}

	public void setMarkSetupName(String markSetupName) {
		this.markSetupName = markSetupName;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<ExamFCARSetupDetail> getExamFcarSetupDetails() {
		return examFcarSetupDetails;
	}

	public void setExamFcarSetupDetails(List<ExamFCARSetupDetail> examFcarSetupDetails) {
		this.examFcarSetupDetails = examFcarSetupDetails;
	}
}