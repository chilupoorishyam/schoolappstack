package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_pa_sub_categories database table.
 * 
 */
@Entity
@Table(name = "t_pa_sub_categories")
@NamedQuery(name = "SubCategory.findAll", query = "SELECT s FROM SubCategory s")
public class SubCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_pa_subcategory_id")
	private Long subcategoryId;

	// bi-directional many-to-one association to Organization
	@ManyToOne
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	// bi-directional many-to-one association to Category
	@ManyToOne
	@JoinColumn(name = "fk_pa_category_id")
	private Category category;

	@Column(name = "achievement_subcategory")
	private String achievementSubcategory;

	@Column(name = "achievement_subcategory_code")
	private String achievementSubcategoryCode;

	@Column(name = "subcategory_logo_path")
	private String subcategoryLogoPath;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to Achievement
	@OneToMany(mappedBy = "subCategory")
	private List<Achievement> achievements;

	public SubCategory() {
	}

	public Long getSubcategoryId() {
		return this.subcategoryId;
	}

	public void setSubcategoryId(Long subcategoryId) {
		this.subcategoryId = subcategoryId;
	}

	public String getAchievementSubcategory() {
		return this.achievementSubcategory;
	}

	public void setAchievementSubcategory(String achievementSubcategory) {
		this.achievementSubcategory = achievementSubcategory;
	}

	public String getAchievementSubcategoryCode() {
		return this.achievementSubcategoryCode;
	}

	public void setAchievementSubcategoryCode(String achievementSubcategoryCode) {
		this.achievementSubcategoryCode = achievementSubcategoryCode;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSubcategoryLogoPath() {
		return this.subcategoryLogoPath;
	}

	public void setSubcategoryLogoPath(String subcategoryLogoPath) {
		this.subcategoryLogoPath = subcategoryLogoPath;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<Achievement> getAchievements() {
		return achievements;
	}

	public void setAchievements(List<Achievement> achievements) {
		this.achievements = achievements;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

}