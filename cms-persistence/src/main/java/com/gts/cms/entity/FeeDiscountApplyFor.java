package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_fee_discount_apply_for database table.
 * 
 */
@Entity
@Table(name="t_fee_discount_apply_for")
@NamedQuery(name="FeeDiscountApplyFor.findAll", query="SELECT f FROM FeeDiscountApplyFor f")
public class FeeDiscountApplyFor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_discount_applyfor_id")
	private Long feeDiscountApplyforId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="discount_for")
	private String discountFor;

	@Column(name="fk_discount_for_values_id")
	private Long discountForValuesId;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FeeDiscount
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_discount_id")
	private FeeDiscount feeDiscount;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	public FeeDiscountApplyFor() {
	}

	public Long getFeeDiscountApplyforId() {
		return this.feeDiscountApplyforId;
	}

	public void setFeeDiscountApplyforId(Long feeDiscountApplyforId) {
		this.feeDiscountApplyforId = feeDiscountApplyforId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDiscountFor() {
		return this.discountFor;
	}

	public void setDiscountFor(String discountFor) {
		this.discountFor = discountFor;
	}

	public Long getDiscountForValuesId() {
		return this.discountForValuesId;
	}

	public void setDiscountForValuesId(Long discountForValuesId) {
		this.discountForValuesId = discountForValuesId;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public FeeDiscount getFeeDiscount() {
		return this.feeDiscount;
	}

	public void setFeeDiscount(FeeDiscount feeDiscount) {
		this.feeDiscount = feeDiscount;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

}