package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the t_fee_student_wise_particulars database table.
 * 
 */
@Entity
@Table(name="t_fee_student_wise_particulars")
@NamedQuery(name="FeeStudentWiseParticular.findAll", query="SELECT f FROM FeeStudentWiseParticular f")
public class FeeStudentWiseParticular implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_std_particular_id")
	private Long feeStdParticularId;

	private BigDecimal amount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String description;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FeeParticularwisePayment
	@OneToMany(mappedBy="feeStudentWiseParticular",fetch =FetchType.LAZY)
	private List<FeeParticularwisePayment> feeParticularwisePayments;

	//bi-directional many-to-one association to FeeCategory
	@ManyToOne(fetch =FetchType.LAZY)
	@JoinColumn(name="fk_fee_category_id")
	private FeeCategory feeCategory;

	//bi-directional many-to-one association to FeeParticular
	@ManyToOne(fetch =FetchType.LAZY)
	@JoinColumn(name="fk_fee_particulars_id")
	private FeeParticular feeParticular;

	//bi-directional many-to-one association to FeeStructure
	@ManyToOne(fetch =FetchType.LAZY)
	@JoinColumn(name="fk_fee_structure_id")
	private FeeStructure feeStructure;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch =FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch =FetchType.LAZY)
	@JoinColumn(name="fk_student_id")
	private StudentDetail studentDetail;

	//bi-directional many-to-one association to FeeTransactionDetail
	@OneToMany(mappedBy="feeStudentWiseParticular",fetch =FetchType.LAZY)
	private List<FeeTransactionDetail> feeTransactionDetails;

	public FeeStudentWiseParticular() {
	}

	public Long getFeeStdParticularId() {
		return this.feeStdParticularId;
	}

	public void setFeeStdParticularId(Long feeStdParticularId) {
		this.feeStdParticularId = feeStdParticularId;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<FeeParticularwisePayment> getFeeParticularwisePayments() {
		return this.feeParticularwisePayments;
	}

	public void setFeeParticularwisePayments(List<FeeParticularwisePayment> feeParticularwisePayments) {
		this.feeParticularwisePayments = feeParticularwisePayments;
	}

	public FeeParticularwisePayment addFeeParticularwisePayment(FeeParticularwisePayment feeParticularwisePayment) {
		getFeeParticularwisePayments().add(feeParticularwisePayment);
		feeParticularwisePayment.setFeeStudentWiseParticular(this);

		return feeParticularwisePayment;
	}

	public FeeParticularwisePayment removeFeeParticularwisePayment(FeeParticularwisePayment feeParticularwisePayment) {
		getFeeParticularwisePayments().remove(feeParticularwisePayment);
		feeParticularwisePayment.setFeeStudentWiseParticular(null);

		return feeParticularwisePayment;
	}

	public FeeCategory getFeeCategory() {
		return this.feeCategory;
	}

	public void setFeeCategory(FeeCategory feeCategory) {
		this.feeCategory = feeCategory;
	}

	public FeeParticular getFeeParticular() {
		return this.feeParticular;
	}

	public void setFeeParticular(FeeParticular feeParticular) {
		this.feeParticular = feeParticular;
	}

	public FeeStructure getFeeStructure() {
		return this.feeStructure;
	}

	public void setFeeStructure(FeeStructure feeStructure) {
		this.feeStructure = feeStructure;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public List<FeeTransactionDetail> getFeeTransactionDetails() {
		return this.feeTransactionDetails;
	}

	public void setFeeTransactionDetails(List<FeeTransactionDetail> feeTransactionDetails) {
		this.feeTransactionDetails = feeTransactionDetails;
	}

	public FeeTransactionDetail addFeeTransactionDetail(FeeTransactionDetail feeTransactionDetail) {
		getFeeTransactionDetails().add(feeTransactionDetail);
		feeTransactionDetail.setFeeStudentWiseParticular(this);

		return feeTransactionDetail;
	}

	public FeeTransactionDetail removeFeeTransactionDetail(FeeTransactionDetail feeTransactionDetail) {
		getFeeTransactionDetails().remove(feeTransactionDetail);
		feeTransactionDetail.setFeeStudentWiseParticular(null);

		return feeTransactionDetail;
	}

}