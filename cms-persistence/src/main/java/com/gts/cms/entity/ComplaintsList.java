package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_grv_complaints_list database table.
 * 
 */
@Entity
@Table(name = "t_grv_complaints_list")
@NamedQuery(name = "ComplaintsList.findAll", query = "SELECT c FROM ComplaintsList c")
public class ComplaintsList implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_complaint_list_id")
	private Long complaintListId;

	@Column(name = "complaint_desc")
	private String complaintDesc;

	@Column(name = "complaint_short_desc")
	private String complaintShortDesc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "instructions_notes")
	private String instructionsNotes;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to Complaint
	@OneToMany(mappedBy = "complaintsList")
	private List<Complaint> complaints;

	// bi-directional many-to-one association to GrievanceCategory
	@ManyToOne
	@JoinColumn(name = "fk_grv_category_id")
	private GrievanceCategory grievanceCategory;

	public ComplaintsList() {
	}

	public Long getComplaintListId() {
		return this.complaintListId;
	}

	public void setComplaintListId(Long complaintListId) {
		this.complaintListId = complaintListId;
	}

	public String getComplaintDesc() {
		return this.complaintDesc;
	}

	public void setComplaintDesc(String complaintDesc) {
		this.complaintDesc = complaintDesc;
	}

	public String getComplaintShortDesc() {
		return this.complaintShortDesc;
	}

	public void setComplaintShortDesc(String complaintShortDesc) {
		this.complaintShortDesc = complaintShortDesc;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getInstructionsNotes() {
		return this.instructionsNotes;
	}

	public void setInstructionsNotes(String instructionsNotes) {
		this.instructionsNotes = instructionsNotes;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<Complaint> getComplaints() {
		return complaints;
	}

	public void setComplaints(List<Complaint> complaints) {
		this.complaints = complaints;
	}

	public GrievanceCategory getGrievanceCategory() {
		return grievanceCategory;
	}

	public void setGrievanceCategory(GrievanceCategory grievanceCategory) {
		this.grievanceCategory = grievanceCategory;
	}

}