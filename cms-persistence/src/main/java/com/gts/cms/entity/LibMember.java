package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_lib_members database table.
 * 
 */
@Entity
@Table(name = "t_lib_members")
@NamedQuery(name = "LibMember.findAll", query = "SELECT l FROM LibMember l")
public class LibMember implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_lib_member_id")
	private Long libMemberId;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_feepaid")
	private Boolean isFeepaid;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "member_from_dt")
	private Date memberFromDt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "member_to_dt")
	private Date memberToDt;

	private String membertype;

	@Column(name = "no_of_borrowed_books")
	private Long noOfBorrowedBooks;

	@Column(name = "no_of_max_books")
	private Long noOfMaxBooks;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to LibraryDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_library_id")
	private LibraryDetail library;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_member_emp_id")
	private EmployeeDetail memberEmp;

	// bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_member_student_id")
	private StudentDetail memberStudent;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;
	
	@Column(name = "member_code")
	private String memberCode;

	public LibMember() {
	}

	public Long getLibMemberId() {
		return libMemberId;
	}

	public void setLibMemberId(Long libMemberId) {
		this.libMemberId = libMemberId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public LibraryDetail getLibrary() {
		return library;
	}

	public void setLibrary(LibraryDetail library) {
		this.library = library;
	}

	public EmployeeDetail getMemberEmp() {
		return memberEmp;
	}

	public void setMemberEmp(EmployeeDetail memberEmp) {
		this.memberEmp = memberEmp;
	}

	public StudentDetail getMemberStudent() {
		return memberStudent;
	}

	public void setMemberStudent(StudentDetail memberStudent) {
		this.memberStudent = memberStudent;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsFeepaid() {
		return isFeepaid;
	}

	public void setIsFeepaid(Boolean isFeepaid) {
		this.isFeepaid = isFeepaid;
	}

	public Date getMemberFromDt() {
		return memberFromDt;
	}

	public void setMemberFromDt(Date memberFromDt) {
		this.memberFromDt = memberFromDt;
	}

	public Date getMemberToDt() {
		return memberToDt;
	}

	public void setMemberToDt(Date memberToDt) {
		this.memberToDt = memberToDt;
	}

	public String getMembertype() {
		return membertype;
	}

	public void setMembertype(String membertype) {
		this.membertype = membertype;
	}

	public Long getNoOfBorrowedBooks() {
		return noOfBorrowedBooks;
	}

	public void setNoOfBorrowedBooks(Long noOfBorrowedBooks) {
		this.noOfBorrowedBooks = noOfBorrowedBooks;
	}

	public Long getNoOfMaxBooks() {
		return noOfMaxBooks;
	}

	public void setNoOfMaxBooks(Long noOfMaxBooks) {
		this.noOfMaxBooks = noOfMaxBooks;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}
	
	

}