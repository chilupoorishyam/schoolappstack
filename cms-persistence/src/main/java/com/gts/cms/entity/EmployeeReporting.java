package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_emp_employee_reporting database table.
 * 
 */
@Entity
@Table(name="t_emp_employee_reporting")
@NamedQuery(name="EmployeeReporting.findAll", query="SELECT e FROM EmployeeReporting e")
public class EmployeeReporting implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_emp_reporting_id")
	private Long employeeReportingId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="from_date")
	private Date fromDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="to_date")
	private Date toDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;
	
	@Column(name="is_active")
	private Boolean isActive;
	
	private String reason;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_id")
	private EmployeeDetail employeeDetail;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_manager_emp_id")
	private EmployeeDetail managerEmployeeDetail;

	//bi-directional many-to-one association to Designation
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_designation_id")
	private Designation designation;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	public EmployeeReporting() {
	}

	public Long getEmployeeReportingId() {
		return this.employeeReportingId;
	}

	public void setEmployeeReportingId(Long employeeReportingId) {
		this.employeeReportingId = employeeReportingId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getEmployeeDetail() {
		return this.employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public EmployeeDetail getManagerEmployeeDetail() {
		return this.managerEmployeeDetail;
	}

	public void setManagerEmployeeDetail(EmployeeDetail managerEmployeeDetail) {
		this.managerEmployeeDetail = managerEmployeeDetail;
	}

	public Designation getDesignation() {
		return this.designation;
	}

	public void setDesignation(Designation designation) {
		this.designation = designation;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}