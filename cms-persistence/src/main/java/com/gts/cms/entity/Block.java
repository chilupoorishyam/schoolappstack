package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_blocks database table.
 * 
 */
@Entity
@Table(name="t_m_blocks")
@NamedQuery(name="Block.findAll", query="SELECT b FROM Block b")
public class Block implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_block_id")
	private Long blockId;

	@Column(name="block_code")
	private String blockCode;

	@Column(name="block_name")
	private String blockName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="no_of_floors")
	private Integer noOfFloors;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to Building
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_building_id")
	private Building building;

	//bi-directional many-to-one association to Floor
	@OneToMany(mappedBy="block",fetch = FetchType.LAZY)
	private List<Floor> floors;

	//bi-directional many-to-one association to Room
	@OneToMany(mappedBy="block",fetch = FetchType.LAZY)
	private List<Room> rooms;

	public Block() {
	}

	public Long getBlockId() {
		return this.blockId;
	}

	public void setBlockId(Long blockId) {
		this.blockId = blockId;
	}

	public String getBlockCode() {
		return this.blockCode;
	}

	public void setBlockCode(String blockCode) {
		this.blockCode = blockCode;
	}

	public String getBlockName() {
		return this.blockName;
	}

	public void setBlockName(String blockName) {
		this.blockName = blockName;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getNoOfFloors() {
		return this.noOfFloors;
	}

	public void setNoOfFloors(Integer noOfFloors) {
		this.noOfFloors = noOfFloors;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Building getBuilding() {
		return this.building;
	}

	public void setBuilding(Building building) {
		this.building = building;
	}

	public List<Floor> getFloors() {
		return this.floors;
	}

	public void setFloors(List<Floor> floors) {
		this.floors = floors;
	}

	public Floor addFloor(Floor floor) {
		getFloors().add(floor);
		floor.setBlock(this);

		return floor;
	}

	public Floor removeFloor(Floor floor) {
		getFloors().remove(floor);
		floor.setBlock(null);

		return floor;
	}

	public List<Room> getRooms() {
		return this.rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}

	public Room addRoom(Room room) {
		getRooms().add(room);
		room.setBlock(this);

		return room;
	}

	public Room removeRoom(Room room) {
		getRooms().remove(room);
		room.setBlock(null);

		return room;
	}

}