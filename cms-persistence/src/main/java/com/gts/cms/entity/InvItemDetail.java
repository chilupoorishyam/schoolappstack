package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_inv_item_details database table.
 * 
 */
@Entity
@Table(name = "t_inv_item_details")
@NamedQuery(name = "InvItemDetail.findAll", query = "SELECT i FROM InvItemDetail i")
public class InvItemDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_item_det_id")
	private Long itemDetId;

	@Column(name = "batch_no")
	private String batchNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	// bi-directional many-to-one association to Department
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_current_dept_id")
	private Department department;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_current_emp_id")
	private EmployeeDetail employeeDetail;

	// bi-directional many-to-one association to Room
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_current_room_id")
	private Room currentRoom;
	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_itemcondition_catdet_id")
	private GeneralDetail itemconditionCatdet;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_in_store")
	private Boolean isInStore;

	@Column(name = "item_comments")
	private String itemComments;

	@Column(name = "item_serial_no")
	private String itemSerialNo;

	@Column(name = "item_track_no")
	private String itemTrackNo;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "item_barcode")
	private String itemBarCode;

	/*
	 * // bi-directional many-to-one association to InvInternalIssueItem
	 * 
	 * @OneToMany(mappedBy = "InvItemDetail") private List<InvInternalIssueItem>
	 * invInternalIssueItems;
	 * 
	 * // bi-directional many-to-one association to InvInternalReturnItem
	 * 
	 * @OneToMany(mappedBy = "InvItemDetail") private List<InvInternalReturnItem>
	 * invInternalReturnItems;
	 */

	// bi-directional many-to-one association to InvStoresmaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_store_id")
	private InvStoresmaster invStoresmaster;

	// bi-directional many-to-one association to InvItemmaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_item_id")
	private InvItemmaster invItemmaster;

	public InvItemDetail() {
	}

	public Long getItemDetId() {
		return this.itemDetId;
	}

	public void setItemDetId(Long itemDetId) {
		this.itemDetId = itemDetId;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsInStore() {
		return this.isInStore;
	}

	public void setIsInStore(Boolean isInStore) {
		this.isInStore = isInStore;
	}

	public String getItemComments() {
		return this.itemComments;
	}

	public void setItemComments(String itemComments) {
		this.itemComments = itemComments;
	}

	public String getItemSerialNo() {
		return this.itemSerialNo;
	}

	public void setItemSerialNo(String itemSerialNo) {
		this.itemSerialNo = itemSerialNo;
	}

	public String getItemTrackNo() {
		return this.itemTrackNo;
	}

	public void setItemTrackNo(String itemTrackNo) {
		this.itemTrackNo = itemTrackNo;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public EmployeeDetail getEmployeeDetail() {
		return employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public Room getCurrentRoom() {
		return currentRoom;
	}

	public void setCurrentRoom(Room currentRoom) {
		this.currentRoom = currentRoom;
	}

	public GeneralDetail getItemconditionCatdet() {
		return itemconditionCatdet;
	}

	public void setItemconditionCatdet(GeneralDetail itemconditionCatdet) {
		this.itemconditionCatdet = itemconditionCatdet;
	}

	/*
	 * public List<InvInternalIssueItem> getInvInternalIssueItems() { return
	 * invInternalIssueItems; }
	 * 
	 * public void setInvInternalIssueItems(List<InvInternalIssueItem>
	 * invInternalIssueItems) { this.invInternalIssueItems = invInternalIssueItems;
	 * }
	 * 
	 * public List<InvInternalReturnItem> getInvInternalReturnItems() { return
	 * invInternalReturnItems; }
	 * 
	 * public void setInvInternalReturnItems(List<InvInternalReturnItem>
	 * invInternalReturnItems) { this.invInternalReturnItems =
	 * invInternalReturnItems; }
	 */
	public InvStoresmaster getInvStoresmaster() {
		return invStoresmaster;
	}

	public void setInvStoresmaster(InvStoresmaster invStoresmaster) {
		this.invStoresmaster = invStoresmaster;
	}

	public InvItemmaster getInvItemmaster() {
		return invItemmaster;
	}

	public void setInvItemmaster(InvItemmaster invItemmaster) {
		this.invItemmaster = invItemmaster;
	}

	public String getItemBarCode() {
		return itemBarCode;
	}

	public void setItemBarCode(String itemBarCode) {
		this.itemBarCode = itemBarCode;
	}

}