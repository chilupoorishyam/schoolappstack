package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_tt_schedules database table.
 * 
 */
@Entity
@Table(name="t_tt_schedules")
@NamedQuery(name="Schedule.findAll", query="SELECT s FROM Schedule s")
public class Schedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_timetable_schedule_id")
	private Long timetableScheduleId;

	@Column(name="color_code")
	private String colorCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to ActualClassesSchedule
	@OneToMany(mappedBy="schedule",fetch = FetchType.LAZY)
	private List<ActualClassesSchedule> actualClassesSchedules;

	//bi-directional many-to-one association to Lessonstatus
	@OneToMany(mappedBy="schedule",fetch = FetchType.LAZY)
	private List<Lessonstatus> lessonstatuses;

	//bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;
	
	@Column(name="cell_group_id")
	private String cellGroupId;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to GroupSection
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_group_section_id")
	private GroupSection groupSection;

	//bi-directional many-to-one association to Weekday
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_weekday_id")
	private Weekday weekday;

	//bi-directional many-to-one association to ClassTiming
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_class_timing_id")
	private ClassTiming classTiming;

	//bi-directional many-to-one association to Timetable
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_timetable_id")
	private Timetable timetable;

	//bi-directional many-to-one association to StaffProxy
	@OneToMany(mappedBy="schedule",fetch = FetchType.LAZY)
	private List<StaffProxy> staffProxies;

	//bi-directional many-to-one association to SubjectResource
	@OneToMany(mappedBy="schedule",fetch = FetchType.LAZY)
	private List<SubjectResource> subjectResources;

	public Schedule() {
	}

	public Long getTimetableScheduleId() {
		return this.timetableScheduleId;
	}

	public void setTimetableScheduleId(Long timetableScheduleId) {
		this.timetableScheduleId = timetableScheduleId;
	}

	public String getColorCode() {
		return this.colorCode;
	}

	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<ActualClassesSchedule> getActualClassesSchedules() {
		return this.actualClassesSchedules;
	}

	public void setActualClassesSchedules(List<ActualClassesSchedule> actualClassesSchedules) {
		this.actualClassesSchedules = actualClassesSchedules;
	}

	public ActualClassesSchedule addActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		getActualClassesSchedules().add(actualClassesSchedule);
		actualClassesSchedule.setSchedule(this);

		return actualClassesSchedule;
	}

	public ActualClassesSchedule removeActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		getActualClassesSchedules().remove(actualClassesSchedule);
		actualClassesSchedule.setSchedule(null);

		return actualClassesSchedule;
	}

	public List<Lessonstatus> getLessonstatuses() {
		return this.lessonstatuses;
	}

	public void setLessonstatuses(List<Lessonstatus> lessonstatuses) {
		this.lessonstatuses = lessonstatuses;
	}

	public Lessonstatus addLessonstatus(Lessonstatus lessonstatus) {
		getLessonstatuses().add(lessonstatus);
		lessonstatus.setSchedule(this);

		return lessonstatus;
	}

	public Lessonstatus removeLessonstatus(Lessonstatus lessonstatus) {
		getLessonstatuses().remove(lessonstatus);
		lessonstatus.setSchedule(null);

		return lessonstatus;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GroupSection getGroupSection() {
		return this.groupSection;
	}

	public void setGroupSection(GroupSection groupSection) {
		this.groupSection = groupSection;
	}

	public Weekday getWeekday() {
		return this.weekday;
	}

	public void setWeekday(Weekday weekday) {
		this.weekday = weekday;
	}

	public ClassTiming getClassTiming() {
		return this.classTiming;
	}

	public void setClassTiming(ClassTiming classTiming) {
		this.classTiming = classTiming;
	}

	public Timetable getTimetable() {
		return this.timetable;
	}

	public void setTimetable(Timetable timetable) {
		this.timetable = timetable;
	}

	public List<StaffProxy> getStaffProxies() {
		return this.staffProxies;
	}

	public void setStaffProxies(List<StaffProxy> staffProxies) {
		this.staffProxies = staffProxies;
	}

	public StaffProxy addStaffProxy(StaffProxy staffProxy) {
		getStaffProxies().add(staffProxy);
		staffProxy.setSchedule(this);

		return staffProxy;
	}

	public StaffProxy removeStaffProxy(StaffProxy staffProxy) {
		getStaffProxies().remove(staffProxy);
		staffProxy.setSchedule(null);

		return staffProxy;
	}

	public List<SubjectResource> getSubjectResources() {
		return this.subjectResources;
	}

	public void setSubjectResources(List<SubjectResource> subjectResources) {
		this.subjectResources = subjectResources;
	}

	public SubjectResource addSubjectResource(SubjectResource subjectResource) {
		getSubjectResources().add(subjectResource);
		subjectResource.setSchedule(this);

		return subjectResource;
	}

	public SubjectResource removeSubjectResource(SubjectResource subjectResource) {
		getSubjectResources().remove(subjectResource);
		subjectResource.setSchedule(null);

		return subjectResource;
	}

	public String getCellGroupId() {
		return cellGroupId;
	}

	public void setCellGroupId(String cellGroupId) {
		this.cellGroupId = cellGroupId;
	}

}