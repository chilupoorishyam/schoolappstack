package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_fee_structure database table.
 * 
 */
@Entity
@Table(name="t_fee_structure")
@NamedQuery(name="FeeStructure.findAll", query="SELECT f FROM FeeStructure f")
public class FeeStructure implements Serializable {
	public GeneralDetail getQuota() {
		return quota;
	}

	public void setQuota(GeneralDetail quota) {
		this.quota = quota;
	}

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_structure_id")
	private Long feeStructureId;

	@Temporal(TemporalType.DATE)
	private Date activefromdate;

	@Temporal(TemporalType.DATE)
	private Date activetodate;

	@Column(name="structure_name")
	private String classGroupName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String description;

	@Temporal(TemporalType.DATE)
	private Date duedate;

	@Column(name="fee_due_before_ndays")
	private Integer feeDueBeforeNdays;

	@Column(name="fee_due_before_nweeks")
	private Integer feeDueBeforeNweeks;

	@Lob
	@Column(name="fee_due_mail_notes")
	private String feeDueMailNotes;

	@Column(name="fee_due_remainder_on")
	private Boolean feeDueRemainderOn;

	@Lob
	@Column(name="fee_due_sms_notes")
	private String feeDueSmsNotes;

	@Column(name="is_active")
	private Boolean isActive;
	
	@Column(name="is_editable")
	private Boolean isEditable;
	
	@Column(name="is_deactivate")
	private Boolean isDeactivate;

	private String reason;

	@Column(name="schedule_before_ndays")
	private Integer scheduleBeforeNdays;

	@Column(name="schedule_before_nweeks")
	private Integer scheduleBeforeNweeks;

	@Lob
	@Column(name="schedule_mail_notes")
	private String scheduleMailNotes;

	@Column(name="schedule_remainder_on")
	private Boolean scheduleRemainderOn;

	@Lob
	@Column(name="schedule_sms_notes")
	private String scheduleSmsNotes;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FeeDiscount
	@OneToMany(mappedBy="feeStructure",fetch = FetchType.LAZY)
	private List<FeeDiscount> feeDiscounts;

	//bi-directional many-to-one association to FeeParticularwisePayment
	@OneToMany(mappedBy="feeStructure",fetch = FetchType.LAZY)
	private List<FeeParticularwisePayment> feeParticularwisePayments;

	//bi-directional many-to-one association to FeeReceipt
	@OneToMany(mappedBy="feeStructure",fetch = FetchType.LAZY)
	private List<FeeReceipt> feeReceipts;

	//bi-directional many-to-one association to FeeRefundRule
	@OneToMany(mappedBy="feeStructure",fetch = FetchType.LAZY)
	private List<FeeRefundRule> feeRefundRules;

	//bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_quota_catdet_id")
	private GeneralDetail quota;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_id")
	private Course course;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to FeeStructureCourseyr
	@OneToMany(mappedBy="feeStructure",cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	private List<FeeStructureCourseyr> feeStructureCourseyrs;

	//bi-directional many-to-one association to FeeStructureParticular
	@OneToMany(mappedBy="feeStructure",cascade=CascadeType.ALL,fetch = FetchType.EAGER)
	private List<FeeStructureParticular> feeStructureParticulars;

	//bi-directional many-to-one association to FeeStudentData
	@OneToMany(mappedBy="feeStructure",fetch = FetchType.LAZY)
	private List<FeeStudentData> feeStudentData;

	//bi-directional many-to-one association to FeeStudentRefund
	@OneToMany(mappedBy="feeStructure",fetch = FetchType.LAZY)
	private List<FeeStudentRefund> feeStudentRefunds;

	//bi-directional many-to-one association to FeeStudentWiseParticular
	@OneToMany(mappedBy="feeStructure",fetch = FetchType.LAZY)
	private List<FeeStudentWiseParticular> feeStudentWiseParticulars;

	//bi-directional many-to-one association to FeeStudentwiseDiscount
	@OneToMany(mappedBy="feeStructure",fetch = FetchType.LAZY)
	private List<FeeStudentwiseDiscount> feeStudentwiseDiscounts;

	//bi-directional many-to-one association to FeeStudentwiseFine
	@OneToMany(mappedBy="feeStructure",fetch = FetchType.LAZY)
	private List<FeeStudentwiseFine> feeStudentwiseFines;

	//bi-directional many-to-one association to FeeTransactionMaster
	@OneToMany(mappedBy="feeStructure",fetch = FetchType.LAZY)
	private List<FeeTransactionMaster> feeTransactionMasters;

	public FeeStructure() {
	}

	public Long getFeeStructureId() {
		return this.feeStructureId;
	}

	public void setFeeStructureId(Long feeStructureId) {
		this.feeStructureId = feeStructureId;
	}

	public Date getActivefromdate() {
		return this.activefromdate;
	}

	public void setActivefromdate(Date activefromdate) {
		this.activefromdate = activefromdate;
	}

	public Date getActivetodate() {
		return this.activetodate;
	}

	public void setActivetodate(Date activetodate) {
		this.activetodate = activetodate;
	}

	public String getClassGroupName() {
		return this.classGroupName;
	}

	public void setClassGroupName(String classGroupName) {
		this.classGroupName = classGroupName;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDuedate() {
		return this.duedate;
	}

	public void setDuedate(Date duedate) {
		this.duedate = duedate;
	}

	public Integer getFeeDueBeforeNdays() {
		return this.feeDueBeforeNdays;
	}

	public void setFeeDueBeforeNdays(Integer feeDueBeforeNdays) {
		this.feeDueBeforeNdays = feeDueBeforeNdays;
	}

	public Integer getFeeDueBeforeNweeks() {
		return this.feeDueBeforeNweeks;
	}

	public void setFeeDueBeforeNweeks(Integer feeDueBeforeNweeks) {
		this.feeDueBeforeNweeks = feeDueBeforeNweeks;
	}

	public String getFeeDueMailNotes() {
		return this.feeDueMailNotes;
	}

	public void setFeeDueMailNotes(String feeDueMailNotes) {
		this.feeDueMailNotes = feeDueMailNotes;
	}

	public Boolean getFeeDueRemainderOn() {
		return this.feeDueRemainderOn;
	}

	public void setFeeDueRemainderOn(Boolean feeDueRemainderOn) {
		this.feeDueRemainderOn = feeDueRemainderOn;
	}

	public String getFeeDueSmsNotes() {
		return this.feeDueSmsNotes;
	}

	public void setFeeDueSmsNotes(String feeDueSmsNotes) {
		this.feeDueSmsNotes = feeDueSmsNotes;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getScheduleBeforeNdays() {
		return this.scheduleBeforeNdays;
	}

	public void setScheduleBeforeNdays(Integer scheduleBeforeNdays) {
		this.scheduleBeforeNdays = scheduleBeforeNdays;
	}

	public Integer getScheduleBeforeNweeks() {
		return this.scheduleBeforeNweeks;
	}

	public void setScheduleBeforeNweeks(Integer scheduleBeforeNweeks) {
		this.scheduleBeforeNweeks = scheduleBeforeNweeks;
	}

	public String getScheduleMailNotes() {
		return this.scheduleMailNotes;
	}

	public void setScheduleMailNotes(String scheduleMailNotes) {
		this.scheduleMailNotes = scheduleMailNotes;
	}

	public Boolean getScheduleRemainderOn() {
		return this.scheduleRemainderOn;
	}

	public void setScheduleRemainderOn(Boolean scheduleRemainderOn) {
		this.scheduleRemainderOn = scheduleRemainderOn;
	}

	public String getScheduleSmsNotes() {
		return this.scheduleSmsNotes;
	}

	public void setScheduleSmsNotes(String scheduleSmsNotes) {
		this.scheduleSmsNotes = scheduleSmsNotes;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<FeeDiscount> getFeeDiscounts() {
		return this.feeDiscounts;
	}

	public void setFeeDiscounts(List<FeeDiscount> feeDiscounts) {
		this.feeDiscounts = feeDiscounts;
	}

	public FeeDiscount addFeeDiscount(FeeDiscount feeDiscount) {
		getFeeDiscounts().add(feeDiscount);
		feeDiscount.setFeeStructure(this);

		return feeDiscount;
	}

	public FeeDiscount removeFeeDiscount(FeeDiscount feeDiscount) {
		getFeeDiscounts().remove(feeDiscount);
		feeDiscount.setFeeStructure(null);

		return feeDiscount;
	}

	public List<FeeParticularwisePayment> getFeeParticularwisePayments() {
		return this.feeParticularwisePayments;
	}

	public void setFeeParticularwisePayments(List<FeeParticularwisePayment> feeParticularwisePayments) {
		this.feeParticularwisePayments = feeParticularwisePayments;
	}

	public FeeParticularwisePayment addFeeParticularwisePayment(FeeParticularwisePayment feeParticularwisePayment) {
		getFeeParticularwisePayments().add(feeParticularwisePayment);
		feeParticularwisePayment.setFeeStructure(this);

		return feeParticularwisePayment;
	}

	public FeeParticularwisePayment removeFeeParticularwisePayment(FeeParticularwisePayment feeParticularwisePayment) {
		getFeeParticularwisePayments().remove(feeParticularwisePayment);
		feeParticularwisePayment.setFeeStructure(null);

		return feeParticularwisePayment;
	}

	public List<FeeReceipt> getFeeReceipts() {
		return this.feeReceipts;
	}

	public void setFeeReceipts(List<FeeReceipt> feeReceipts) {
		this.feeReceipts = feeReceipts;
	}

	public FeeReceipt addFeeReceipt(FeeReceipt feeReceipt) {
		getFeeReceipts().add(feeReceipt);
		feeReceipt.setFeeStructure(this);

		return feeReceipt;
	}

	public FeeReceipt removeFeeReceipt(FeeReceipt feeReceipt) {
		getFeeReceipts().remove(feeReceipt);
		feeReceipt.setFeeStructure(null);

		return feeReceipt;
	}

	public List<FeeRefundRule> getFeeRefundRules() {
		return this.feeRefundRules;
	}

	public void setFeeRefundRules(List<FeeRefundRule> feeRefundRules) {
		this.feeRefundRules = feeRefundRules;
	}

	public FeeRefundRule addFeeRefundRule(FeeRefundRule feeRefundRule) {
		getFeeRefundRules().add(feeRefundRule);
		feeRefundRule.setFeeStructure(this);

		return feeRefundRule;
	}

	public FeeRefundRule removeFeeRefundRule(FeeRefundRule feeRefundRule) {
		getFeeRefundRules().remove(feeRefundRule);
		feeRefundRule.setFeeStructure(null);

		return feeRefundRule;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<FeeStructureCourseyr> getFeeStructureCourseyrs() {
		return this.feeStructureCourseyrs;
	}

	public void setFeeStructureCourseyrs(List<FeeStructureCourseyr> feeStructureCourseyrs) {
		this.feeStructureCourseyrs = feeStructureCourseyrs;
	}

	public FeeStructureCourseyr addFeeStructureCourseyr(FeeStructureCourseyr feeStructureCourseyr) {
		getFeeStructureCourseyrs().add(feeStructureCourseyr);
		feeStructureCourseyr.setFeeStructure(this);

		return feeStructureCourseyr;
	}

	public FeeStructureCourseyr removeFeeStructureCourseyr(FeeStructureCourseyr feeStructureCourseyr) {
		getFeeStructureCourseyrs().remove(feeStructureCourseyr);
		feeStructureCourseyr.setFeeStructure(null);

		return feeStructureCourseyr;
	}

	public List<FeeStructureParticular> getFeeStructureParticulars() {
		return this.feeStructureParticulars;
	}

	public void setFeeStructureParticulars(List<FeeStructureParticular> feeStructureParticulars) {
		this.feeStructureParticulars = feeStructureParticulars;
	}

	public FeeStructureParticular addFeeStructureParticular(FeeStructureParticular feeStructureParticular) {
		getFeeStructureParticulars().add(feeStructureParticular);
		feeStructureParticular.setFeeStructure(this);

		return feeStructureParticular;
	}

	public FeeStructureParticular removeFeeStructureParticular(FeeStructureParticular feeStructureParticular) {
		getFeeStructureParticulars().remove(feeStructureParticular);
		feeStructureParticular.setFeeStructure(null);

		return feeStructureParticular;
	}

	public List<FeeStudentData> getFeeStudentData() {
		return this.feeStudentData;
	}

	public void setFeeStudentData(List<FeeStudentData> feeStudentData) {
		this.feeStudentData = feeStudentData;
	}

	public FeeStudentData addFeeStudentData(FeeStudentData feeStudentData) {
		getFeeStudentData().add(feeStudentData);
		feeStudentData.setFeeStructure(this);

		return feeStudentData;
	}

	public FeeStudentData removeFeeStudentData(FeeStudentData feeStudentData) {
		getFeeStudentData().remove(feeStudentData);
		feeStudentData.setFeeStructure(null);

		return feeStudentData;
	}

	public List<FeeStudentRefund> getFeeStudentRefunds() {
		return this.feeStudentRefunds;
	}

	public void setFeeStudentRefunds(List<FeeStudentRefund> feeStudentRefunds) {
		this.feeStudentRefunds = feeStudentRefunds;
	}

	public FeeStudentRefund addFeeStudentRefund(FeeStudentRefund feeStudentRefund) {
		getFeeStudentRefunds().add(feeStudentRefund);
		feeStudentRefund.setFeeStructure(this);

		return feeStudentRefund;
	}

	public FeeStudentRefund removeFeeStudentRefund(FeeStudentRefund feeStudentRefund) {
		getFeeStudentRefunds().remove(feeStudentRefund);
		feeStudentRefund.setFeeStructure(null);

		return feeStudentRefund;
	}

	public List<FeeStudentWiseParticular> getFeeStudentWiseParticulars() {
		return this.feeStudentWiseParticulars;
	}

	public void setFeeStudentWiseParticulars(List<FeeStudentWiseParticular> feeStudentWiseParticulars) {
		this.feeStudentWiseParticulars = feeStudentWiseParticulars;
	}

	public FeeStudentWiseParticular addFeeStudentWiseParticular(FeeStudentWiseParticular feeStudentWiseParticular) {
		getFeeStudentWiseParticulars().add(feeStudentWiseParticular);
		feeStudentWiseParticular.setFeeStructure(this);

		return feeStudentWiseParticular;
	}

	public FeeStudentWiseParticular removeFeeStudentWiseParticular(FeeStudentWiseParticular feeStudentWiseParticular) {
		getFeeStudentWiseParticulars().remove(feeStudentWiseParticular);
		feeStudentWiseParticular.setFeeStructure(null);

		return feeStudentWiseParticular;
	}

	public List<FeeStudentwiseDiscount> getFeeStudentwiseDiscounts() {
		return this.feeStudentwiseDiscounts;
	}

	public void setFeeStudentwiseDiscounts(List<FeeStudentwiseDiscount> feeStudentwiseDiscounts) {
		this.feeStudentwiseDiscounts = feeStudentwiseDiscounts;
	}

	public FeeStudentwiseDiscount addFeeStudentwiseDiscount(FeeStudentwiseDiscount feeStudentwiseDiscount) {
		getFeeStudentwiseDiscounts().add(feeStudentwiseDiscount);
		feeStudentwiseDiscount.setFeeStructure(this);

		return feeStudentwiseDiscount;
	}

	public FeeStudentwiseDiscount removeFeeStudentwiseDiscount(FeeStudentwiseDiscount feeStudentwiseDiscount) {
		getFeeStudentwiseDiscounts().remove(feeStudentwiseDiscount);
		feeStudentwiseDiscount.setFeeStructure(null);

		return feeStudentwiseDiscount;
	}

	public List<FeeStudentwiseFine> getFeeStudentwiseFines() {
		return this.feeStudentwiseFines;
	}

	public void setFeeStudentwiseFines(List<FeeStudentwiseFine> feeStudentwiseFines) {
		this.feeStudentwiseFines = feeStudentwiseFines;
	}

	public FeeStudentwiseFine addFeeStudentwiseFine(FeeStudentwiseFine feeStudentwiseFine) {
		getFeeStudentwiseFines().add(feeStudentwiseFine);
		feeStudentwiseFine.setFeeStructure(this);

		return feeStudentwiseFine;
	}

	public FeeStudentwiseFine removeFeeStudentwiseFine(FeeStudentwiseFine feeStudentwiseFine) {
		getFeeStudentwiseFines().remove(feeStudentwiseFine);
		feeStudentwiseFine.setFeeStructure(null);

		return feeStudentwiseFine;
	}

	public List<FeeTransactionMaster> getFeeTransactionMasters() {
		return this.feeTransactionMasters;
	}

	public void setFeeTransactionMasters(List<FeeTransactionMaster> feeTransactionMasters) {
		this.feeTransactionMasters = feeTransactionMasters;
	}

	public FeeTransactionMaster addFeeTransactionMaster(FeeTransactionMaster feeTransactionMaster) {
		getFeeTransactionMasters().add(feeTransactionMaster);
		feeTransactionMaster.setFeeStructure(this);

		return feeTransactionMaster;
	}

	public FeeTransactionMaster removeFeeTransactionMaster(FeeTransactionMaster feeTransactionMaster) {
		getFeeTransactionMasters().remove(feeTransactionMaster);
		feeTransactionMaster.setFeeStructure(null);

		return feeTransactionMaster;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Boolean getIsEditable() {
		return isEditable;
	}

	public void setIsEditable(Boolean isEditable) {
		this.isEditable = isEditable;
	}

	public Boolean getIsDeactivate() {
		return isDeactivate;
	}

	public void setIsDeactivate(Boolean isDeactivate) {
		this.isDeactivate = isDeactivate;
	}

}