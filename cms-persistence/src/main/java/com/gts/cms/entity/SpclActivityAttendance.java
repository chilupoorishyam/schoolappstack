package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the t_tt_spcl_activity_attendance database table.
 * 
 */
@Entity
@Table(name = "t_tt_spcl_activity_attendance")
@NamedQuery(name = "SpclActivityAttendance.findAll", query = "SELECT s FROM SpclActivityAttendance s")
public class SpclActivityAttendance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_spcl_activity_attendance_id")
	private Long spclActivityAttendanceId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_collectedby_emp_id")
	private EmployeeDetail employeeDetail;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_payment_mode_catdet_id")
	private GeneralDetail paymentModeCatdet;

	// bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	@Column(name = "is_absent")
	private Boolean isAbsent;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_presented")
	private Boolean isPresented;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne
	@JoinColumn(name = "fk_reg_status_emp_id")
	private EmployeeDetail statusEmpId;

	@Column(name = "paid_amount")
	private BigDecimal paidAmount;

	private String reason;

	@Column(name = "receipt_no")
	private String receiptNo;
	
	@Column(name = "is_all_present")
	private String isAllPresent;

	@Column(name = "is_reg_accepted")
	private String isRegAccepted;


	@Column(name = "reference_number")
	private String referenceNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "reg_status_date")
	private Date regStatusDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "registration_date")
	private Date registrationDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to Specialactivity
	@ManyToOne
	@JoinColumn(name = "fk_spcl_activity_id")
	private SpecialActivity specialActivity;

	public SpclActivityAttendance() {
	}

	public Long getSpclActivityAttendanceId() {
		return this.spclActivityAttendanceId;
	}

	public void setSpclActivityAttendanceId(Long spclActivityAttendanceId) {
		this.spclActivityAttendanceId = spclActivityAttendanceId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public EmployeeDetail getEmployeeDetail() {
		return employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public Boolean getIsAbsent() {
		return this.isAbsent;
	}

	public void setIsAbsent(Boolean isAbsent) {
		this.isAbsent = isAbsent;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsPresented() {
		return this.isPresented;
	}

	public void setIsPresented(Boolean isPresented) {
		this.isPresented = isPresented;
	}

	

	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReceiptNo() {
		return this.receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public String getReferenceNumber() {
		return this.referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	

	public Date getRegistrationDate() {
		return this.registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getPaymentModeCatdet() {
		return paymentModeCatdet;
	}

	public void setPaymentModeCatdet(GeneralDetail paymentModeCatdet) {
		this.paymentModeCatdet = paymentModeCatdet;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public SpecialActivity getSpecialActivity() {
		return specialActivity;
	}

	public void setSpecialActivity(SpecialActivity specialActivity) {
		this.specialActivity = specialActivity;
	}

	public EmployeeDetail getStatusEmpId() {
		return statusEmpId;
	}

	public void setStatusEmpId(EmployeeDetail statusEmpId) {
		this.statusEmpId = statusEmpId;
	}

	public String getIsAllPresent() {
		return isAllPresent;
	}

	public void setIsAllPresent(String isAllPresent) {
		this.isAllPresent = isAllPresent;
	}

	public String getIsRegAccepted() {
		return isRegAccepted;
	}

	public void setIsRegAccepted(String isRegAccepted) {
		this.isRegAccepted = isRegAccepted;
	}

	public Date getRegStatusDate() {
		return regStatusDate;
	}

	public void setRegStatusDate(Date regStatusDate) {
		this.regStatusDate = regStatusDate;
	}
	

}