package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_grv_complaint_details database table.
 * 
 */
@Entity
@Table(name = "t_grv_complaint_details")
@NamedQuery(name = "ComplaintDetail.findAll", query = "SELECT c FROM ComplaintDetail c")
public class ComplaintDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_grv_complaint_det_id")
	private Long complaintDetId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	private String message;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "message_date")
	private Date messageDate;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to Complaint
	@ManyToOne
	@JoinColumn(name = "fk_grv_complaint_id")
	private Complaint complaint;

	// bi-directional many-to-one association to ComplaintTask
	@OneToMany(mappedBy = "complaintDetail")
	private List<ComplaintTask> complaintTasks;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne
	@JoinColumn(name = "fk_emp_id")
	private EmployeeDetail employeeDetail;

	// bi-directional many-to-one association to StudentDetail
	@ManyToOne
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	// bi-directional many-to-one association to WorkflowStage
	@ManyToOne
	@JoinColumn(name = "fk_wf_stage_id")
	private WorkflowStage workflowStage;

	public ComplaintDetail() {
	}

	public Long getComplaintDetId() {
		return this.complaintDetId;
	}

	public void setComplaintDetId(Long complaintDetId) {
		this.complaintDetId = complaintDetId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getMessageDate() {
		return this.messageDate;
	}

	public void setMessageDate(Date messageDate) {
		this.messageDate = messageDate;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Complaint getComplaint() {
		return complaint;
	}

	public void setComplaint(Complaint complaint) {
		this.complaint = complaint;
	}

	public EmployeeDetail getEmployeeDetail() {
		return employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public WorkflowStage getWorkflowStage() {
		return workflowStage;
	}

	public void setWorkflowStage(WorkflowStage workflowStage) {
		this.workflowStage = workflowStage;
	}

	public List<ComplaintTask> getComplaintTasks() {
		return complaintTasks;
	}

	public void setComplaintTasks(List<ComplaintTask> complaintTasks) {
		this.complaintTasks = complaintTasks;
	}

}