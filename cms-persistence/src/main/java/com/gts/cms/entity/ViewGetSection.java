package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
@Entity
@Table(name = "v_get_sections")
@NamedQuery(name = "ViewGetSection.findAll", query = "SELECT v FROM ViewGetSection v")
public class ViewGetSection implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "pk_org_id")
	private Long orgId;
	 
	@Column(name = "org_name")
	private String orgName;
	
	@Column(name = "org_code")
	private String orgCode;
	
	@Column(name = "pk_school_id")
	private Long schoolId;
	
	@Column(name = "school_name")
	private String schoolName;
	
	@Column(name = "pk_course_id")
	private Long courseId;
	
	@Column(name = "course_name")
	private String courseName;
	
	@Column(name = "course_code")
	private String courseCode;

	
	@Column(name = "group_code")
	private String groupCode;
	
	@Column(name = "group_name")
	private String groupName;
	
	@Column(name = "pk_course_year_id")
	private Long courseYearId;
	
	@Column(name = "course_year_name")
	private String courseYearName;
	
	@Column(name = "course_year_code")
	private String courseYearCode;
	
	@Column(name = "year_name")
	private String yearName;
	
	@Column(name = "year_no")
	private Integer yearNo;
	

	@Column(name = "pk_group_section_id")
	private Long groupSectionId;
	
	@Column(name = "section")
	private String section;
	
	@Column(name = "pk_academic_year_id")
	private Long academicYearId;
	
	@Column(name = "academic_year")
	private String academicYear;
	
	@Column(name = "ay_from_date")
	private Date fromDate;
	
	@Column(name = "ay_to_date")
	private Date toDate;
	
	@Column(name = "Display_Name")
	private String displayName;
	
	@Column(name = "Display_full_Name")
	private String displayFullName;
	
	@Column(name = "year_sort_order")
	private Integer yearSortOrder;
	
	@Column(name = "sec_sort_order")
	private Integer secSortOrder;
	

    public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}


	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Long getCourseYearId() {
		return courseYearId;
	}

	public void setCourseYearId(Long courseYearId) {
		this.courseYearId = courseYearId;
	}

	public String getCourseYearName() {
		return courseYearName;
	}

	public void setCourseYearName(String courseYearName) {
		this.courseYearName = courseYearName;
	}

	public String getCourseYearCode() {
		return courseYearCode;
	}

	public void setCourseYearCode(String courseYearCode) {
		this.courseYearCode = courseYearCode;
	}

	public String getYearName() {
		return yearName;
	}

	public void setYearName(String yearName) {
		this.yearName = yearName;
	}

	public Integer getYearNo() {
		return yearNo;
	}

	public void setYearNo(Integer yearNo) {
		this.yearNo = yearNo;
	}

	public Long getGroupSectionId() {
		return groupSectionId;
	}

	public void setGroupSectionId(Long groupSectionId) {
		this.groupSectionId = groupSectionId;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public Long getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}

	public String getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayFullName() {
		return displayFullName;
	}

	public void setDisplayFullName(String displayFullName) {
		this.displayFullName = displayFullName;
	}

	public Integer getYearSortOrder() {
		return yearSortOrder;
	}

	public void setYearSortOrder(Integer yearSortOrder) {
		this.yearSortOrder = yearSortOrder;
	}

	public Integer getSecSortOrder() {
		return secSortOrder;
	}

	public void setSecSortOrder(Integer secSortOrder) {
		this.secSortOrder = secSortOrder;
	}

		
    
}
