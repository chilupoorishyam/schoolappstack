package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_departments database table.
 * 
 */
@Entity
@Table(name="t_m_departments")
@NamedQuery(name="Department.findAll", query="SELECT d FROM Department d")
public class Department implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_dept_id")
	private Long departmentId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="dept_code")
	private String deptCode;

	@Column(name="dept_name")
	private String deptName;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Column(name="sort_order")
	private Integer sortOrder;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="employeeDepartment",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails1;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="employeeWorkingDepartment",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails2;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	public Department() {
	}

	public Long getDepartmentId() {
		return this.departmentId;
	}

	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDeptCode() {
		return this.deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public String getDeptName() {
		return this.deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<EmployeeDetail> getEmpDetails1() {
		return this.empDetails1;
	}

	public void setEmpDetails1(List<EmployeeDetail> empDetails1) {
		this.empDetails1 = empDetails1;
	}

	public EmployeeDetail addEmpDetails1(EmployeeDetail empDetails1) {
		getEmpDetails1().add(empDetails1);
		empDetails1.setEmployeeDepartment(this);

		return empDetails1;
	}

	public EmployeeDetail removeEmpDetails1(EmployeeDetail empDetails1) {
		getEmpDetails1().remove(empDetails1);
		empDetails1.setEmployeeDepartment(null);

		return empDetails1;
	}

	public List<EmployeeDetail> getEmpDetails2() {
		return this.empDetails2;
	}

	public void setEmpDetails2(List<EmployeeDetail> empDetails2) {
		this.empDetails2 = empDetails2;
	}

	public EmployeeDetail addEmpDetails2(EmployeeDetail empDetails2) {
		getEmpDetails2().add(empDetails2);
		empDetails2.setEmployeeWorkingDepartment(this);

		return empDetails2;
	}

	public EmployeeDetail removeEmpDetails2(EmployeeDetail empDetails2) {
		getEmpDetails2().remove(empDetails2);
		empDetails2.setEmployeeWorkingDepartment(null);

		return empDetails2;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

}