package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_tt_actual_classes_schedule_workflow database table.
 * 
 */
@Entity
@Table(name="t_tt_actual_classes_schedule_workflow")
@NamedQuery(name="ActualClassesScheduleWorkflow.findAll", query="SELECT a FROM ActualClassesScheduleWorkflow a")
public class ActualClassesScheduleWorkflow implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_actual_cls_schedule_wf_id")
	private Long actualClsScheduleWfId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_actual_cls_schedule_id")
	private ActualClassesSchedule actualClassesSchedule;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_from_attendance_status_id")
	private GeneralDetail fromAttendanceStatus;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_from_emp_id")
	private EmployeeDetail fromEmp;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_to_attendance_status_id")
	private GeneralDetail toAttendanceStatus;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_to_emp_id")
	private EmployeeDetail toEmp;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Column(name="status_comments")
	private String statusComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="status_date")
	private Date statusDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	public ActualClassesScheduleWorkflow() {
	}

	public Long getActualClsScheduleWfId() {
		return actualClsScheduleWfId;
	}

	public void setActualClsScheduleWfId(Long actualClsScheduleWfId) {
		this.actualClsScheduleWfId = actualClsScheduleWfId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public ActualClassesSchedule getActualClassesSchedule() {
		return actualClassesSchedule;
	}

	public void setActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		this.actualClassesSchedule = actualClassesSchedule;
	}

	public GeneralDetail getFromAttendanceStatus() {
		return fromAttendanceStatus;
	}

	public void setFromAttendanceStatus(GeneralDetail fromAttendanceStatus) {
		this.fromAttendanceStatus = fromAttendanceStatus;
	}

	public EmployeeDetail getFromEmp() {
		return fromEmp;
	}

	public void setFromEmp(EmployeeDetail fromEmp) {
		this.fromEmp = fromEmp;
	}

	public GeneralDetail getToAttendanceStatus() {
		return toAttendanceStatus;
	}

	public void setToAttendanceStatus(GeneralDetail toAttendanceStatus) {
		this.toAttendanceStatus = toAttendanceStatus;
	}

	public EmployeeDetail getToEmp() {
		return toEmp;
	}

	public void setToEmp(EmployeeDetail toEmp) {
		this.toEmp = toEmp;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStatusComments() {
		return statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public Date getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}
}