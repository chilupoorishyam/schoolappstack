package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_cm_std_sub_reg_details database table.
 * 
 */
@Entity
@Table(name = "t_cm_std_sub_reg_details")
@NamedQuery(name = "StdSubRegDetail.findAll", query = "SELECT s FROM StdSubRegDetail s")
public class StdSubRegDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_std_sub_reg_detail_id")
	private Long stdSubRegDetailId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne
	@JoinColumn(name = "fk_course_year_id")
	private CourseYear courseYear;

	@ManyToOne
	@JoinColumn(name = "fk_status_emp_id")
	private EmployeeDetail statusEmp;

	@ManyToOne
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	@ManyToOne
	@JoinColumn(name = "fk_subject_id")
	private Subject subject;

	@ManyToOne
	@JoinColumn(name = "fk_subjectcategory_catdet_id")
	private GeneralDetail subjectcategoryCat;

	@ManyToOne
	@JoinColumn(name = "fk_subjecttype_catdet_id")
	private GeneralDetail subjecttypeCat;

	@ManyToOne
	@JoinColumn(name = "fk_subreg_status_catdet_id")
	private GeneralDetail sbregStatusCat;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_prerequisite")
	private Boolean isPrerequisite;

	private String reason;

	@Column(name = "status_comments")
	private String statusComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "status_date")
	private Date statusDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to StdSubRegistration
	@ManyToOne
	@JoinColumn(name = "fk_std_sub_reg_id")
	private StdSubRegistration stdSubRegistration;

	public Long getStdSubRegDetailId() {
		return stdSubRegDetailId;
	}

	public void setStdSubRegDetailId(Long stdSubRegDetailId) {
		this.stdSubRegDetailId = stdSubRegDetailId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public CourseYear getCourseYear() {
		return courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public EmployeeDetail getStatusEmp() {
		return statusEmp;
	}

	public void setStatusEmp(EmployeeDetail statusEmp) {
		this.statusEmp = statusEmp;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public GeneralDetail getSubjectcategoryCat() {
		return subjectcategoryCat;
	}

	public void setSubjectcategoryCat(GeneralDetail subjectcategoryCat) {
		this.subjectcategoryCat = subjectcategoryCat;
	}

	public GeneralDetail getSubjecttypeCat() {
		return subjecttypeCat;
	}

	public void setSubjecttypeCat(GeneralDetail subjecttypeCat) {
		this.subjecttypeCat = subjecttypeCat;
	}

	public GeneralDetail getSbregStatusCat() {
		return sbregStatusCat;
	}

	public void setSbregStatusCat(GeneralDetail sbregStatusCat) {
		this.sbregStatusCat = sbregStatusCat;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsPrerequisite() {
		return isPrerequisite;
	}

	public void setIsPrerequisite(Boolean isPrerequisite) {
		this.isPrerequisite = isPrerequisite;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStatusComments() {
		return statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public Date getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public StdSubRegistration getStdSubRegistration() {
		return stdSubRegistration;
	}

	public void setStdSubRegistration(StdSubRegistration stdSubRegistration) {
		this.stdSubRegistration = stdSubRegistration;
	}

}
