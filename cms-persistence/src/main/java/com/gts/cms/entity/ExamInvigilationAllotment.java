package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_exam_invigilation_allotment database table.
 * 
 */
@Entity
@Table(name = "t_exam_invigilation_allotment")
@NamedQuery(name = "ExamInvigilationAllotment.findAll", query = "SELECT e FROM ExamInvigilationAllotment e")
public class ExamInvigilationAllotment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_invg_allotment_id")
	private Long examInvgAllotmentId;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "fk_remuneration_pay_id")
	private Long remunerationPay;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_presented")
	private Boolean isPresented;

	@Column(name = "is_remuneration_settled")
	private Boolean isRemunerationSettled;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to Room
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_room_id")
	private Room examRoom;

	// bi-directional many-to-one association to ExamTimetable
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_timetable_id")
	private ExamTimetable examTimetable;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_invgdesignation_catdet_id")
	private GeneralDetail invgdesignationCat;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_invgilator_emp_id")
	private EmployeeDetail invgilatorEmp;

	public ExamInvigilationAllotment() {
	}

	public Long getExamInvgAllotmentId() {
		return this.examInvgAllotmentId;
	}

	public void setExamInvgAllotmentId(Long examInvgAllotmentId) {
		this.examInvgAllotmentId = examInvgAllotmentId;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Room getExamRoom() {
		return examRoom;
	}

	public void setExamRoom(Room examRoom) {
		this.examRoom = examRoom;
	}

	

	

	public ExamTimetable getExamTimetable() {
		return examTimetable;
	}

	public void setExamTimetable(ExamTimetable examTimetable) {
		this.examTimetable = examTimetable;
	}

	public GeneralDetail getInvgdesignationCat() {
		return invgdesignationCat;
	}

	public void setInvgdesignationCat(GeneralDetail invgdesignationCat) {
		this.invgdesignationCat = invgdesignationCat;
	}

	public EmployeeDetail getInvgilatorEmp() {
		return invgilatorEmp;
	}

	public void setInvgilatorEmp(EmployeeDetail invgilatorEmp) {
		this.invgilatorEmp = invgilatorEmp;
	}

	public Long getRemunerationPay() {
		return remunerationPay;
	}

	public void setRemunerationPay(Long remunerationPay) {
		this.remunerationPay = remunerationPay;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsPresented() {
		return isPresented;
	}

	public void setIsPresented(Boolean isPresented) {
		this.isPresented = isPresented;
	}

	public Boolean getIsRemunerationSettled() {
		return isRemunerationSettled;
	}

	public void setIsRemunerationSettled(Boolean isRemunerationSettled) {
		this.isRemunerationSettled = isRemunerationSettled;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

}