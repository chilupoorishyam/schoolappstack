package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
@Entity
@Table(name = "t_hr_emp_perf_assessment_qutn_opts")
@NamedQuery(name="EmpPerfAssessmentQutnOpts.findAll", query="SELECT e FROM EmpPerfAssessmentQutnOpts e")
public class EmpPerfAssessmentQutnOpts implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_assessment_option_id", nullable = false)
    private Long assessmentOptionId;

    @Column(name = "option_name")
    private String optionName;

    @Column(name = "option_code")
    private String optionCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_assessment_question_id")
    private EmpPerfAssessmentQuestions assessmentQuestionId;

    @Column(name = "is_active", nullable = false)
    private Boolean active;

    @Column(name = "reason")
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt", nullable = false)
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    public Long getAssessmentOptionId() {
        return assessmentOptionId;
    }

    public void setAssessmentOptionId(Long assessmentOptionId) {
        this.assessmentOptionId = assessmentOptionId;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public String getOptionCode() {
        return optionCode;
    }

    public void setOptionCode(String optionCode) {
        this.optionCode = optionCode;
    }

    public EmpPerfAssessmentQuestions getAssessmentQuestionId() {
        return assessmentQuestionId;
    }

    public void setAssessmentQuestionId(EmpPerfAssessmentQuestions assessmentQuestionId) {
        this.assessmentQuestionId = assessmentQuestionId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    public Long getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(Long createdUser) {
        this.createdUser = createdUser;
    }

    public Date getUpdatedDt() {
        return updatedDt;
    }

    public void setUpdatedDt(Date updatedDt) {
        this.updatedDt = updatedDt;
    }

    public Long getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(Long updatedUser) {
        this.updatedUser = updatedUser;
    }
}
