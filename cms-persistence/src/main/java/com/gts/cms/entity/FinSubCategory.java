package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_fin_sub_category database table.
 * 
 */
@Entity
@Table(name="t_fin_sub_category")
@NamedQuery(name="FinSubCategory.findAll", query="SELECT f FROM FinSubCategory f")
public class FinSubCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fin_sub_category_id")
	private Long finSubCategoryId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_under_income")
	private Boolean isUnderIncome;

	private String reason;

	@Column(name="sub_category_description")
	private String subCategoryDescription;

	@Column(name="sub_category_name")
	private String subCategoryName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FinCategory
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fin_category_id")
	private FinCategory finCategory;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to FinTransaction
	@OneToMany(mappedBy="finSubCategory",fetch = FetchType.LAZY)
	private List<FinTransaction> finTransactions;

	//bi-directional many-to-one association to FinTransactionHistory
	@OneToMany(mappedBy="finSubCategory",fetch = FetchType.LAZY)
	private List<FinTransactionHistory> finTransactionHistories;

	public FinSubCategory() {
	}

	public Long getFinSubCategoryId() {
		return this.finSubCategoryId;
	}

	public void setFinSubCategoryId(Long finSubCategoryId) {
		this.finSubCategoryId = finSubCategoryId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsUnderIncome() {
		return this.isUnderIncome;
	}

	public void setIsUnderIncome(Boolean isUnderIncome) {
		this.isUnderIncome = isUnderIncome;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSubCategoryDescription() {
		return this.subCategoryDescription;
	}

	public void setSubCategoryDescription(String subCategoryDescription) {
		this.subCategoryDescription = subCategoryDescription;
	}

	public String getSubCategoryName() {
		return this.subCategoryName;
	}

	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public FinCategory getFinCategory() {
		return this.finCategory;
	}

	public void setFinCategory(FinCategory finCategory) {
		this.finCategory = finCategory;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<FinTransaction> getFinTransactions() {
		return this.finTransactions;
	}

	public void setFinTransactions(List<FinTransaction> finTransactions) {
		this.finTransactions = finTransactions;
	}

	public FinTransaction addFinTransaction(FinTransaction finTransaction) {
		getFinTransactions().add(finTransaction);
		finTransaction.setFinSubCategory(this);

		return finTransaction;
	}

	public FinTransaction removeFinTransaction(FinTransaction finTransaction) {
		getFinTransactions().remove(finTransaction);
		finTransaction.setFinSubCategory(null);

		return finTransaction;
	}

	public List<FinTransactionHistory> getFinTransactionHistories() {
		return this.finTransactionHistories;
	}

	public void setFinTransactionHistories(List<FinTransactionHistory> finTransactionHistories) {
		this.finTransactionHistories = finTransactionHistories;
	}

	public FinTransactionHistory addFinTransactionHistory(FinTransactionHistory finTransactionHistory) {
		getFinTransactionHistories().add(finTransactionHistory);
		finTransactionHistory.setFinSubCategory(this);

		return finTransactionHistory;
	}

	public FinTransactionHistory removeFinTransactionHistory(FinTransactionHistory finTransactionHistory) {
		getFinTransactionHistories().remove(finTransactionHistory);
		finTransactionHistory.setFinSubCategory(null);

		return finTransactionHistory;
	}

}