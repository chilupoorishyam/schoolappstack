package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_fee_instant_particular database table.
 * 
 */
@Entity
@Table(name="t_fee_instant_particular")
@NamedQuery(name="FeeInstantParticular.findAll", query="SELECT f FROM FeeInstantParticular f")
public class FeeInstantParticular implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_instant_particular_id")
	private Long feeInstantParticularId;

	private Integer amount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String description;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="particular_name")
	private String particularName;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FeeInstantCategory
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_instant_category_id")
	private FeeInstantCategory feeInstantCategory;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to FeeInstantPaymentParticular
	@OneToMany(mappedBy="feeInstantParticular",fetch = FetchType.LAZY)
	private List<FeeInstantPaymentParticular> feeInstantPaymentParticulars;

	public FeeInstantParticular() {
	}

	public Long getFeeInstantParticularId() {
		return this.feeInstantParticularId;
	}

	public void setFeeInstantParticularId(Long feeInstantParticularId) {
		this.feeInstantParticularId = feeInstantParticularId;
	}

	public Integer getAmount() {
		return this.amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getParticularName() {
		return this.particularName;
	}

	public void setParticularName(String particularName) {
		this.particularName = particularName;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public FeeInstantCategory getFeeInstantCategory() {
		return this.feeInstantCategory;
	}

	public void setFeeInstantCategory(FeeInstantCategory feeInstantCategory) {
		this.feeInstantCategory = feeInstantCategory;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<FeeInstantPaymentParticular> getFeeInstantPaymentParticulars() {
		return this.feeInstantPaymentParticulars;
	}

	public void setFeeInstantPaymentParticulars(List<FeeInstantPaymentParticular> feeInstantPaymentParticulars) {
		this.feeInstantPaymentParticulars = feeInstantPaymentParticulars;
	}

	public FeeInstantPaymentParticular addFeeInstantPaymentParticular(FeeInstantPaymentParticular feeInstantPaymentParticular) {
		getFeeInstantPaymentParticulars().add(feeInstantPaymentParticular);
		feeInstantPaymentParticular.setFeeInstantParticular(this);

		return feeInstantPaymentParticular;
	}

	public FeeInstantPaymentParticular removeFeeInstantPaymentParticular(FeeInstantPaymentParticular feeInstantPaymentParticular) {
		getFeeInstantPaymentParticulars().remove(feeInstantPaymentParticular);
		feeInstantPaymentParticular.setFeeInstantParticular(null);

		return feeInstantPaymentParticular;
	}

}