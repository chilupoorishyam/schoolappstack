package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_pa_m_companies database table.
 * 
 */
@Entity
@Table(name = "t_pa_m_companies")
@NamedQuery(name = "Company.findAll", query = "SELECT c FROM Company c")
public class Company implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_company_id")
	private Long companyId;

	private String companydescription;

	private String companyname;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	@Temporal(TemporalType.DATE)
	@Column(name = "last_participated_date")
	private Date lastParticipatedDate;

	private String linkedin;

	private String location;

	@Column(name = "phonenumber")
	private String phoneNumber;

	@Column(name = "primary_contact_details")
	private String primaryContactDetails;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	private String website;

	// bi-directional many-to-one association to PlacementCompany
	@OneToMany(mappedBy = "company", fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<PlacementCompany> placementCompanies;

	@OneToMany(mappedBy = "company", fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<CompanyContact> companyContacts;
	
	public Company() {
	}

	public Long getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getCompanydescription() {
		return this.companydescription;
	}

	public void setCompanydescription(String companydescription) {
		this.companydescription = companydescription;
	}

	public String getCompanyname() {
		return this.companyname;
	}

	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Date getLastParticipatedDate() {
		return this.lastParticipatedDate;
	}

	public void setLastParticipatedDate(Date lastParticipatedDate) {
		this.lastParticipatedDate = lastParticipatedDate;
	}

	public String getLinkedin() {
		return this.linkedin;
	}

	public void setLinkedin(String linkedin) {
		this.linkedin = linkedin;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPrimaryContactDetails() {
		return this.primaryContactDetails;
	}

	public void setPrimaryContactDetails(String primaryContactDetails) {
		this.primaryContactDetails = primaryContactDetails;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public List<PlacementCompany> getPlacementCompanies() {
		return placementCompanies;
	}

	public void setPlacementCompanies(List<PlacementCompany> placementCompanies) {
		this.placementCompanies = placementCompanies;
	}

	public PlacementCompany addPlacementCompany(PlacementCompany PlacementCompany) {
		getPlacementCompanies().add(PlacementCompany);
		PlacementCompany.setCompany(this);

		return PlacementCompany;
	}

	public PlacementCompany removePlacementCompany(PlacementCompany PlacementCompany) {
		getPlacementCompanies().remove(PlacementCompany);
		PlacementCompany.setCompany(null);

		return PlacementCompany;
	}

	public List<CompanyContact> getCompanyContacts() {
		return companyContacts;
	}

	public void setCompanyContacts(List<CompanyContact> companyContacts) {
		this.companyContacts = companyContacts;
	}
	

}