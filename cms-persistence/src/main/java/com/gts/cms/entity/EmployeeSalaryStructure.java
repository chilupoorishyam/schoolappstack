package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;


/**
 * The persistent class for the t_hr_employee_salary_structure database table.
 * 
 */
@Entity
@Table(name="t_hr_employee_salary_structure")
@NamedQuery(name="EmployeeSalaryStructure.findAll", query="SELECT e FROM EmployeeSalaryStructure e")
public class EmployeeSalaryStructure implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_emp_salary_structure_id")
	private Long empSalaryStructureId;

	private BigDecimal amount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="payment_frequency")
	private BigInteger paymentFrequency;

	@Column(name="payslip_generation_day")
	private Integer payslipGenerationDay;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to PayrollCategory
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_payroll_category_id")
	private PayrollCategory payrollCategory;

	//bi-directional many-to-one association to PayrollCategoryGroup
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_payroll_category_group_id")
	private PayrollCategoryGroup payrollCategoryGroup;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_payroll_group_id")
	private EmployeePayrollGroup employeePayrollGroup;

	public EmployeeSalaryStructure() {
	}

	public Long getEmpSalaryStructureId() {
		return this.empSalaryStructureId;
	}

	public void setEmpSalaryStructureId(Long empSalaryStructureId) {
		this.empSalaryStructureId = empSalaryStructureId;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public BigInteger getPaymentFrequency() {
		return this.paymentFrequency;
	}

	public void setPaymentFrequency(BigInteger paymentFrequency) {
		this.paymentFrequency = paymentFrequency;
	}

	public Integer getPayslipGenerationDay() {
		return this.payslipGenerationDay;
	}

	public void setPayslipGenerationDay(Integer payslipGenerationDay) {
		this.payslipGenerationDay = payslipGenerationDay;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public PayrollCategory getPayrollCategory() {
		return this.payrollCategory;
	}

	public void setPayrollCategory(PayrollCategory payrollCategory) {
		this.payrollCategory = payrollCategory;
	}

	public PayrollCategoryGroup getPayrollCategoryGroup() {
		return this.payrollCategoryGroup;
	}

	public void setPayrollCategoryGroup(PayrollCategoryGroup payrollCategoryGroup) {
		this.payrollCategoryGroup = payrollCategoryGroup;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public EmployeePayrollGroup getEmployeePayrollGroup() {
		return employeePayrollGroup;
	}

	public void setEmployeePayrollGroup(EmployeePayrollGroup employeePayrollGroup) {
		this.employeePayrollGroup = employeePayrollGroup;
	}

}