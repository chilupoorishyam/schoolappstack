package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_ams_event_album database table.
 * 
 */
@Entity
@Table(name = "t_ams_event_album")
@NamedQuery(name = "AmsEventAlbum.findAll", query = "SELECT a FROM AmsEventAlbum a")
public class AmsEventAlbum implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_ams_event_album_id")
	private Long amsEventAlbumId;

	@Column(name = "attachment_comments")
	private String attachmentComments;
	
	@Column(name="album_name")
	private String albumName;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "attachment_date")
	private Date attachmentDate;

	@Column(name = "attachment_path")
	private String attachmentPath;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_verifiedby_emp_id")
	private EmployeeDetail verifiedbyEmp;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_verifiedby_incharge")
	private Boolean isVerifiedbyIncharge;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "verification_comments")
	private String verificationComments;

	// bi-directional many-to-one association to AmsEvent
	@ManyToOne
	@JoinColumn(name = "fk_event_id")
	private AmsEvent amsEvent;

	public AmsEventAlbum() {
	}

	public Long getAmsEventAlbumId() {
		return this.amsEventAlbumId;
	}

	public void setAmsEventAlbumId(Long amsEventAlbumId) {
		this.amsEventAlbumId = amsEventAlbumId;
	}

	public String getAttachmentComments() {
		return this.attachmentComments;
	}

	public void setAttachmentComments(String attachmentComments) {
		this.attachmentComments = attachmentComments;
	}

	public Date getAttachmentDate() {
		return this.attachmentDate;
	}

	public void setAttachmentDate(Date attachmentDate) {
		this.attachmentDate = attachmentDate;
	}

	public String getAttachmentPath() {
		return this.attachmentPath;
	}

	public void setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public EmployeeDetail getVerifiedbyEmp() {
		return verifiedbyEmp;
	}

	public void setVerifiedbyEmp(EmployeeDetail verifiedbyEmp) {
		this.verifiedbyEmp = verifiedbyEmp;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsVerifiedbyIncharge() {
		return this.isVerifiedbyIncharge;
	}

	public void setIsVerifiedbyIncharge(Boolean isVerifiedbyIncharge) {
		this.isVerifiedbyIncharge = isVerifiedbyIncharge;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getVerificationComments() {
		return this.verificationComments;
	}

	public void setVerificationComments(String verificationComments) {
		this.verificationComments = verificationComments;
	}

	public AmsEvent getAmsEvent() {
		return this.amsEvent;
	}

	public void setAmsEvent(AmsEvent amsEvent) {
		this.amsEvent = amsEvent;
	}

	public String getAlbumName() {
		return albumName;
	}

	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}
}