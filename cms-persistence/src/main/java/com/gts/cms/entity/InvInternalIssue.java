package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_inv_internal_issue database table.
 * 
 */
@Entity
@Table(name = "t_inv_internal_issue")
@NamedQuery(name = "InvInternalIssue.findAll", query = "SELECT i FROM InvInternalIssue i")
public class InvInternalIssue implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_inter_issue_id")
	private Long interIssueId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "acknowledged_date")
	private Date acknowledgedDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_acknowledged_emp_id")
	private EmployeeDetail acknowledgedEmpId;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_inv_transtype_catdet_id")
	private GeneralDetail invTranstypeCatdet;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_issue_status_catdet_id")
	private GeneralDetail issueStatusCatdet;

	// bi-directional many-to-one association to Department
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_to_dept_id")
	private Department toDeptId;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_to_emp_id")
	private EmployeeDetail employeeDetail;

	// bi-directional many-to-one association to Room
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_to_room_id")
	private Room toRoom;

	@Column(name = "internal_issue_no")
	private String internalIssueNo;

	@Column(name = "is_acknowledged")
	private Boolean isAcknowledged;

	@Column(name = "is_active")
	private Boolean isActive;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "issue_date")
	private Date issueDate;

	private String reason;

	@Column(name = "status_comments")
	private String statusComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to InvInternalIndent
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_internal_ind_id")
	private InvInternalIndent invInternalIndent;

	// bi-directional many-to-one association to InvStoresmaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_from_store_id")
	private InvStoresmaster invStoresmaster;

	// bi-directional many-to-one association to InvInternalIssueItem
	@OneToMany(mappedBy = "invInternalIssue",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<InvInternalIssueItem> invInternalIssueItems;

	/*// bi-directional many-to-one association to InvInternalReturn
	@OneToMany(mappedBy = "InvInternalIssue")
	private List<InvInternalReturn> invInternalReturns;*/

	public InvInternalIssue() {
	}

	public Long getInterIssueId() {
		return this.interIssueId;
	}

	public void setInterIssueId(Long interIssueId) {
		this.interIssueId = interIssueId;
	}

	public Date getAcknowledgedDate() {
		return this.acknowledgedDate;
	}

	public void setAcknowledgedDate(Date acknowledgedDate) {
		this.acknowledgedDate = acknowledgedDate;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getInternalIssueNo() {
		return this.internalIssueNo;
	}

	public void setInternalIssueNo(String internalIssueNo) {
		this.internalIssueNo = internalIssueNo;
	}

	public Boolean getIsAcknowledged() {
		return this.isAcknowledged;
	}

	public void setIsAcknowledged(Boolean isAcknowledged) {
		this.isAcknowledged = isAcknowledged;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Date getIssueDate() {
		return this.issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStatusComments() {
		return this.statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getAcknowledgedEmpId() {
		return acknowledgedEmpId;
	}

	public void setAcknowledgedEmpId(EmployeeDetail acknowledgedEmpId) {
		this.acknowledgedEmpId = acknowledgedEmpId;
	}

	public GeneralDetail getInvTranstypeCatdet() {
		return invTranstypeCatdet;
	}

	public void setInvTranstypeCatdet(GeneralDetail invTranstypeCatdet) {
		this.invTranstypeCatdet = invTranstypeCatdet;
	}

	public GeneralDetail getIssueStatusCatdet() {
		return issueStatusCatdet;
	}

	public void setIssueStatusCatdet(GeneralDetail issueStatusCatdet) {
		this.issueStatusCatdet = issueStatusCatdet;
	}

	public Department getToDeptId() {
		return toDeptId;
	}

	public void setToDeptId(Department toDeptId) {
		this.toDeptId = toDeptId;
	}

	public EmployeeDetail getEmployeeDetail() {
		return employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public Room getToRoom() {
		return toRoom;
	}

	public void setToRoom(Room toRoom) {
		this.toRoom = toRoom;
	}

	public InvInternalIndent getInvInternalIndent() {
		return invInternalIndent;
	}

	public void setInvInternalIndent(InvInternalIndent invInternalIndent) {
		this.invInternalIndent = invInternalIndent;
	}

	public InvStoresmaster getInvStoresmaster() {
		return invStoresmaster;
	}

	public void setInvStoresmaster(InvStoresmaster invStoresmaster) {
		this.invStoresmaster = invStoresmaster;
	}

	public List<InvInternalIssueItem> getInvInternalIssueItems() {
		return invInternalIssueItems;
	}

	public void setInvInternalIssueItems(List<InvInternalIssueItem> invInternalIssueItems) {
		this.invInternalIssueItems = invInternalIssueItems;
	}
	
/*
	public InvInternalIndent getInvInternalIndent() {
		return invInternalIndent;
	}

	public void setInvInternalIndent(InvInternalIndent invInternalIndent) {
		this.invInternalIndent = invInternalIndent;
	}

	public InvStoresmaster getInvStoresmaster() {
		return invStoresmaster;
	}

	public void setInvStoresmaster(InvStoresmaster invStoresmaster) {
		this.invStoresmaster = invStoresmaster;
	}

	public List<InvInternalIssueItem> getInvInternalIssueItems() {
		return invInternalIssueItems;
	}

	public void setInvInternalIssueItems(List<InvInternalIssueItem> invInternalIssueItems) {
		this.invInternalIssueItems = invInternalIssueItems;
	}

	public List<InvInternalReturn> getInvInternalReturns() {
		return invInternalReturns;
	}

	public void setInvInternalReturns(List<InvInternalReturn> invInternalReturns) {
		this.invInternalReturns = invInternalReturns;
	}*/
}