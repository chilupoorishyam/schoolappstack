package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_fee_certificate_issue database table.
 * 
 */
@Entity
@Table(name = "t_fee_certificate_issue")
@NamedQuery(name = "FeeCertificateIssue.findAll", query = "SELECT f FROM FeeCertificateIssue f")
public class FeeCertificateIssue implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_fee_certificate_issue_id")
	private Long feeCertificateIssueId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "applied_on")
	private Date appliedOn;

	@Column(name = "collected_amount")
	private BigDecimal collectedAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_approved")
	private Boolean isApproved;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "issued_on")
	private Date issuedOn;

	private String reason;
	
	@Column(name = "application_comments")
	private String applicationComments;

	@Column(name = "ref_document_path")
	private String refDocumentPath;

	@Column(name = "certificate_number")
	private String certificateNumber;
	
	@Column(name = "certificate_for")
	private String certificateFor;
	
	@Column(name = "certificate_for_value")
	private String certificateForValue;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_academic_year_id")
	private AcademicYear academicYear;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_financial_year_id")
	private FinancialYear financialYear;

	// bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_application_status_catdet_id")
	private GeneralDetail applicationStatus;

	// bi-directional many-to-one association to FeeReceipt
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_fee_receipts_id")
	private FeeReceipt feeReceipt;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_issued_emp_id")
	private EmployeeDetail issuedEmpDetail;

	// bi-directional many-to-one association to SchoolCertificate
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_clg_certificate_id")
	private SchoolCertificate schoolCertificate;

	// bi-directional many-to-one association to FeeCertificateWorkflow
	@OneToMany(mappedBy = "feeCertificateIssue", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<FeeCertificateWorkflow> feeCertificateWorkflows;

	private String conduct;

	private String remarks;

	public FeeCertificateIssue() {
	}

	public Long getFeeCertificateIssueId() {
		return this.feeCertificateIssueId;
	}

	public void setFeeCertificateIssueId(Long feeCertificateIssueId) {
		this.feeCertificateIssueId = feeCertificateIssueId;
	}

	public Date getAppliedOn() {
		return this.appliedOn;
	}

	public void setAppliedOn(Date appliedOn) {
		this.appliedOn = appliedOn;
	}

	public BigDecimal getCollectedAmount() {
		return this.collectedAmount;
	}

	public void setCollectedAmount(BigDecimal collectedAmount) {
		this.collectedAmount = collectedAmount;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsApproved() {
		return this.isApproved;
	}

	public void setIsApproved(Boolean isApproved) {
		this.isApproved = isApproved;
	}

	public Date getIssuedOn() {
		return this.issuedOn;
	}

	public void setIssuedOn(Date issuedOn) {
		this.issuedOn = issuedOn;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRefDocumentPath() {
		return this.refDocumentPath;
	}

	public void setRefDocumentPath(String refDocumentPath) {
		this.refDocumentPath = refDocumentPath;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public GeneralDetail getApplicationStatus() {
		return this.applicationStatus;
	}

	public void setApplicationStatus(GeneralDetail applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public FeeReceipt getFeeReceipt() {
		return this.feeReceipt;
	}

	public void setFeeReceipt(FeeReceipt feeReceipt) {
		this.feeReceipt = feeReceipt;
	}

	public EmployeeDetail getIssuedEmpDetail() {
		return this.issuedEmpDetail;
	}

	public void setIssuedEmpDetail(EmployeeDetail issuedEmpDetail) {
		this.issuedEmpDetail = issuedEmpDetail;
	}

	public SchoolCertificate getSchoolCertificate() {
		return this.schoolCertificate;
	}

	public void setSchoolCertificate(SchoolCertificate schoolCertificate) {
		this.schoolCertificate = schoolCertificate;
	}

	public List<FeeCertificateWorkflow> getFeeCertificateWorkflows() {
		return this.feeCertificateWorkflows;
	}

	public void setFeeCertificateWorkflows(List<FeeCertificateWorkflow> feeCertificateWorkflows) {
		this.feeCertificateWorkflows = feeCertificateWorkflows;
	}

	public FeeCertificateWorkflow addFeeCertificateWorkflow(FeeCertificateWorkflow feeCertificateWorkflow) {
		getFeeCertificateWorkflows().add(feeCertificateWorkflow);
		feeCertificateWorkflow.setFeeCertificateIssue(this);

		return feeCertificateWorkflow;
	}

	public FeeCertificateWorkflow removeFeeCertificateWorkflow(FeeCertificateWorkflow feeCertificateWorkflow) {
		getFeeCertificateWorkflows().remove(feeCertificateWorkflow);
		feeCertificateWorkflow.setFeeCertificateIssue(null);

		return feeCertificateWorkflow;
	}

	public String getConduct() {
		return conduct;
	}

	public void setConduct(String conduct) {
		this.conduct = conduct;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCertificateNumber() {
		return certificateNumber;
	}

	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}


	public String getApplicationComments() {
		return applicationComments;
	}

	public void setApplicationComments(String applicationComments) {
		this.applicationComments = applicationComments;
	}

	public String getCertificateFor() {
		return certificateFor;
	}

	public void setCertificateFor(String certificateFor) {
		this.certificateFor = certificateFor;
	}

	public String getCertificateForValue() {
		return certificateForValue;
	}

	public void setCertificateForValue(String certificateForValue) {
		this.certificateForValue = certificateForValue;
	}

	public FinancialYear getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}
}