package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_inv_itemsubcategory database table.
 * 
 */
@Entity
@Table(name = "t_inv_itemsubcategory")
@NamedQuery(name = "InvItemsubcategory.findAll", query = "SELECT i FROM InvItemsubcategory i")
public class InvItemsubcategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_item_subcategory_id")
	private Long itemSubcategoryId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Column(name = "subcategory_code")
	private String subcategoryCode;

	@Column(name = "subcategory_name")
	private String subcategoryName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

/*	// bi-directional many-to-one association to InvItemmaster
	@OneToMany(mappedBy = "InvItemsubcategory")
	private List<InvItemmaster> invItemmasters;*/

	// bi-directional many-to-one association to InvItemcategory
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_item_category_id")
	private InvItemcategory invItemcategory;

	public InvItemsubcategory() {
	}

	public Long getItemSubcategoryId() {
		return this.itemSubcategoryId;
	}

	public void setItemSubcategoryId(Long itemSubcategoryId) {
		this.itemSubcategoryId = itemSubcategoryId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

/*	public List<InvItemmaster> getInvItemmasters() {
		return invItemmasters;
	}

	public void setInvItemmasters(List<InvItemmaster> invItemmasters) {
		this.invItemmasters = invItemmasters;
	}*/

	public InvItemcategory getInvItemcategory() {
		return invItemcategory;
	}

	public void setInvItemcategory(InvItemcategory invItemcategory) {
		this.invItemcategory = invItemcategory;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSubcategoryCode() {
		return this.subcategoryCode;
	}

	public void setSubcategoryCode(String subcategoryCode) {
		this.subcategoryCode = subcategoryCode;
	}

	public String getSubcategoryName() {
		return this.subcategoryName;
	}

	public void setSubcategoryName(String subcategoryName) {
		this.subcategoryName = subcategoryName;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

}