package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the t_fee_managment_std_details database table.
 * 
 */
@Entity
@Table(name="t_fee_managment_std_details")
@NamedQuery(name="FeeManagmentStdDetail.findAll", query="SELECT f FROM FeeManagmentStdDetail f")
public class FeeManagmentStdDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_mgmt_std_id")
	private Long feeManagementStdDetailsId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="allotement_date")
	private Date allotementDate;

	@Column(name="course_amount")
	private BigDecimal courseAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="due_amount")
	private BigDecimal dueAmount;

	@Column(name="father_name")
	private String fatherName;

	@Column(name="gross_amount")
	private BigDecimal grossAmount;

	private String instructions;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_processed")
	private Boolean isProcessed;

	@Column(name="mobile_no")
	private String mobileNo;

	@Column(name="paid_amount")
	private BigDecimal paidAmount;

	private String reason;

	@Column(name="student_name")
	private String studentName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to FeeParticular
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_particulars_id")
	private FeeParticular feeParticular;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_alloted_emp_id")
	private EmployeeDetail allotedEmpDetail;

	//bi-directional many-to-one association to FeeStudentDataParticular
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_std_data_particulars_id")
	private FeeStudentDataParticular feeStudentDataParticular;

	//bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;



	//bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_year_id")
	private CourseYear courseYear;

	//bi-directional many-to-one association to StudentEnquiry
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_std_enquiry_id")
	private StudentEnquiry studentEnquiry;

	//bi-directional many-to-one association to StudentApplication
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_app_id")
	private StudentApplication studentApplication;

	//bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_student_id")
	private StudentDetail studentDetail;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_pay_schedule_catdet_id")
	private GeneralDetail paySchedule;

	//bi-directional many-to-one association to FeeCategory
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_category_id")
	private FeeCategory feeCategory;

	public FeeManagmentStdDetail() {
	}

	public Long getFeeManagementStdDetailsId() {
		return this.feeManagementStdDetailsId;
	}

	public void setFeeManagementStdDetailsId(Long feeManagementStdDetailsId) {
		this.feeManagementStdDetailsId = feeManagementStdDetailsId;
	}

	public Date getAllotementDate() {
		return this.allotementDate;
	}

	public void setAllotementDate(Date allotementDate) {
		this.allotementDate = allotementDate;
	}

	public BigDecimal getCourseAmount() {
		return this.courseAmount;
	}

	public void setCourseAmount(BigDecimal courseAmount) {
		this.courseAmount = courseAmount;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public BigDecimal getDueAmount() {
		return this.dueAmount;
	}

	public void setDueAmount(BigDecimal dueAmount) {
		this.dueAmount = dueAmount;
	}

	public String getFatherName() {
		return this.fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public BigDecimal getGrossAmount() {
		return this.grossAmount;
	}

	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}

	public String getInstructions() {
		return this.instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsProcessed() {
		return this.isProcessed;
	}

	public void setIsProcessed(Boolean isProcessed) {
		this.isProcessed = isProcessed;
	}

	public String getMobileNo() {
		return this.mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStudentName() {
		return this.studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public FeeParticular getFeeParticular() {
		return this.feeParticular;
	}

	public void setFeeParticular(FeeParticular feeParticular) {
		this.feeParticular = feeParticular;
	}

	public EmployeeDetail getAllotedEmpDetail() {
		return this.allotedEmpDetail;
	}

	public void setAllotedEmpDetail(EmployeeDetail allotedEmpDetail) {
		this.allotedEmpDetail = allotedEmpDetail;
	}

	public FeeStudentDataParticular getFeeStudentDataParticular() {
		return this.feeStudentDataParticular;
	}

	public void setFeeStudentDataParticular(FeeStudentDataParticular feeStudentDataParticular) {
		this.feeStudentDataParticular = feeStudentDataParticular;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}



	public CourseYear getCourseYear() {
		return this.courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public StudentEnquiry getStudentEnquiry() {
		return this.studentEnquiry;
	}

	public void setStudentEnquiry(StudentEnquiry studentEnquiry) {
		this.studentEnquiry = studentEnquiry;
	}

	public StudentApplication getStudentApplication() {
		return this.studentApplication;
	}

	public void setStudentApplication(StudentApplication studentApplication) {
		this.studentApplication = studentApplication;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public GeneralDetail getPaySchedule() {
		return this.paySchedule;
	}

	public void setPaySchedule(GeneralDetail paySchedule) {
		this.paySchedule = paySchedule;
	}

	public FeeCategory getFeeCategory() {
		return this.feeCategory;
	}

	public void setFeeCategory(FeeCategory feeCategory) {
		this.feeCategory = feeCategory;
	}

}