package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_ams_profile_details database table.
 * 
 */
@Entity
@Table(name = "t_ams_profile_details")
@NamedQuery(name = "AmsProfileDetail.findAll", query = "SELECT a FROM AmsProfileDetail a")
public class AmsProfileDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_ams_profile_id")
	private Long amsProfileId;

	@Temporal(TemporalType.DATE)
	private Date birthday;

	@Column(name = "block_reason")
	private String blockReason;

	private String branch;

	@Column(name = "can_post")
	private Boolean canPost;

	@Column(name = "city_name")
	private String cityName;

	@Column(name = "company_name")
	private String companyName;

	private String country;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "current_location")
	private String currentLocation;

	private String designation;

	@Column(name = "facebook_profile")
	private String facebookProfile;;

	@Column(name = "first_name")
	private String amsFirstName;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	// bi-directional many-to-one association to Course
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_id")
	private Course course;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_emp_id")
	private EmployeeDetail employeeDetail;

	// bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	// bi-directional many-to-one association to user
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_user_id")
	private User user;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_verifiedby_emp_id")
	private EmployeeDetail verifiedbyemp;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	@Column(name = "hallticket_number")
	private String hallticketNumber;

	@Column(name = "heigher_study_details")
	private String heigherStudyDetails;

	@Column(name = "instagram_profile")
	private String instagramProfile;

	@Column(name = "interested_posts")
	private String interestedPosts;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_blocked")
	private Boolean isBlocked;

	@Column(name = "is_employee")
	private Boolean isEmployee;

	@Column(name = "is_incharge")
	private Boolean isIncharge;

	@Column(name = "is_profile_verified")
	private Boolean isProfileVerified;

	@Column(name = "is_student")
	private Boolean isStudent;

	@Column(name = "is_verifiedby_incharge")
	private Boolean isVerifiedbyIncharge;

	private BigDecimal langitude;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_login")
	private Date lastLogin;

	@Column(name = "last_name")
	private String lastName;

	private BigDecimal latitude;

	@Column(name = "linkedin_profile")
	private String linkedinProfile;

	@Temporal(TemporalType.DATE)
	@Column(name = "marriage_day")
	private Date marriageDay;

	@Column(name = "official_email")
	private String officialEmail;

	@Column(name = "passed_out_year")
	private String passedOutYear;

	@Column(name = "personal_email")
	private String personalEmail;

	@Column(name = "personal_website")
	private String personalWebsite;

	@Column(name = "phone_number")
	private String phoneNumber;

	private String pincode;

	@Column(name = "postal_address")
	private String postalAddress;

	@Column(name = "profile_image_url")
	private String profileImageUrl;

	@Column(name = "is_wishto_donate")
	private Boolean isWishtoDonate;

	@Column(name = "`donation_available_from` ")
	private Date donationAvailableFrom;

	private String reason;

	private String skillset;

	private String state;

	private String summary;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "verification_comments")
	private String verificationComments;

	// bi-directional many-to-one association to AmsEventComment
	@OneToMany(mappedBy = "amsProfileDetail")
	private List<AmsEventComment> amsEventComments;

	// bi-directional many-to-one association to AmsGroupMember
	@OneToMany(mappedBy = "amsProfileDetail")
	private List<AmsGroupMember> amsGroupMembers;

	// bi-directional many-to-one association to AmsPostComment
	@OneToMany(mappedBy = "amsProfileDetail")
	private List<AmsPostComment> amsPostComments;

	// bi-directional many-to-one association to AmsPost
	@OneToMany(mappedBy = "amsProfileDetail")
	private List<AmsPost> amsPosts;

	// bi-directional many-to-one association to AmsIndustry
	@ManyToOne
	@JoinColumn(name = "fk_industry_id")
	private AmsIndustry amsIndustry;

	@ManyToOne
	@JoinColumn(name = "fk_batch_id")
	private Batch batch;


	@ManyToOne
	@JoinColumn(name = "fk_bloodgroup_catdet_id")
	private GeneralDetail bloodGroupCatDet;

	// bi-directional many-to-one association to AmsStudentEducation
	@OneToMany(mappedBy = "amsProfileDetail", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<AmsStudentEducation> amsStudentEducations;

	// bi-directional many-to-one association to AmsStudentWorkEmp
	@OneToMany(mappedBy = "amsProfileDetail", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<AmsStudentWorkEmp> amsStudentWorkEmps;

	@Column(name = "twitter_profile")
	private String twitterProfile;
	@Column(name = "github_profile")
	private String githubProfile;
	@Column(name = "behance_profile")
	private String behanceProfile;

	public AmsProfileDetail() {
	}

	public Long getAmsProfileId() {
		return this.amsProfileId;
	}

	public void setAmsProfileId(Long amsProfileId) {
		this.amsProfileId = amsProfileId;
	}

	public Date getBirthday() {
		return this.birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getBlockReason() {
		return this.blockReason;
	}

	public void setBlockReason(String blockReason) {
		this.blockReason = blockReason;
	}

	public String getBranch() {
		return this.branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public Boolean getCanPost() {
		return canPost;
	}

	public void setCanPost(Boolean canPost) {
		this.canPost = canPost;
	}

	public String getCityName() {
		return this.cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getCurrentLocation() {
		return this.currentLocation;
	}

	public void setCurrentLocation(String currentLocation) {
		this.currentLocation = currentLocation;
	}

	public String getDesignation() {
		return this.designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getAmsFirstName() {
		return amsFirstName;
	}

	public void setAmsFirstName(String amsFirstName) {
		this.amsFirstName = amsFirstName;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public EmployeeDetail getEmployeeDetail() {
		return employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public EmployeeDetail getVerifiedbyemp() {
		return verifiedbyemp;
	}

	public void setVerifiedbyemp(EmployeeDetail verifiedbyemp) {
		this.verifiedbyemp = verifiedbyemp;
	}

	public String getHallticketNumber() {
		return this.hallticketNumber;
	}

	public void setHallticketNumber(String hallticketNumber) {
		this.hallticketNumber = hallticketNumber;
	}

	public String getHeigherStudyDetails() {
		return this.heigherStudyDetails;
	}

	public void setHeigherStudyDetails(String heigherStudyDetails) {
		this.heigherStudyDetails = heigherStudyDetails;
	}

	public String getInterestedPosts() {
		return this.interestedPosts;
	}

	public void setInterestedPosts(String interestedPosts) {
		this.interestedPosts = interestedPosts;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsBlocked() {
		return this.isBlocked;
	}

	public void setIsBlocked(Boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	public Boolean getIsEmployee() {
		return this.isEmployee;
	}

	public void setIsEmployee(Boolean isEmployee) {
		this.isEmployee = isEmployee;
	}

	public Boolean getIsIncharge() {
		return this.isIncharge;
	}

	public void setIsIncharge(Boolean isIncharge) {
		this.isIncharge = isIncharge;
	}

	public Boolean getIsProfileVerified() {
		return this.isProfileVerified;
	}

	public void setIsProfileVerified(Boolean isProfileVerified) {
		this.isProfileVerified = isProfileVerified;
	}

	public Boolean getIsStudent() {
		return this.isStudent;
	}

	public void setIsStudent(Boolean isStudent) {
		this.isStudent = isStudent;
	}

	public Boolean getIsVerifiedbyIncharge() {
		return this.isVerifiedbyIncharge;
	}

	public void setIsVerifiedbyIncharge(Boolean isVerifiedbyIncharge) {
		this.isVerifiedbyIncharge = isVerifiedbyIncharge;
	}

	public BigDecimal getLangitude() {
		return this.langitude;
	}

	public void setLangitude(BigDecimal langitude) {
		this.langitude = langitude;
	}

	public Date getLastLogin() {
		return this.lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public BigDecimal getLatitude() {
		return this.latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public Date getMarriageDay() {
		return this.marriageDay;
	}

	public void setMarriageDay(Date marriageDay) {
		this.marriageDay = marriageDay;
	}

	public String getOfficialEmail() {
		return this.officialEmail;
	}

	public void setOfficialEmail(String officialEmail) {
		this.officialEmail = officialEmail;
	}

	public String getPassedOutYear() {
		return this.passedOutYear;
	}

	public void setPassedOutYear(String passedOutYear) {
		this.passedOutYear = passedOutYear;
	}

	public String getPersonalEmail() {
		return this.personalEmail;
	}

	public void setPersonalEmail(String personalEmail) {
		this.personalEmail = personalEmail;
	}

	public String getPersonalWebsite() {
		return this.personalWebsite;
	}

	public void setPersonalWebsite(String personalWebsite) {
		this.personalWebsite = personalWebsite;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPincode() {
		return this.pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getPostalAddress() {
		return this.postalAddress;
	}

	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	public String getProfileImageUrl() {
		return this.profileImageUrl;
	}

	public void setProfileImageUrl(String profileImageUrl) {
		this.profileImageUrl = profileImageUrl;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSkillset() {
		return this.skillset;
	}

	public void setSkillset(String skillset) {
		this.skillset = skillset;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getSummary() {
		return this.summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getVerificationComments() {
		return this.verificationComments;
	}

	public void setVerificationComments(String verificationComments) {
		this.verificationComments = verificationComments;
	}

	public List<AmsEventComment> getAmsEventComments() {
		return this.amsEventComments;
	}

	public void setAmsEventComments(List<AmsEventComment> amsEventComments) {
		this.amsEventComments = amsEventComments;
	}

	public AmsEventComment addAmsEventComment(AmsEventComment amsEventComment) {
		getAmsEventComments().add(amsEventComment);
		amsEventComment.setAmsProfileDetail(this);

		return amsEventComment;
	}

	public AmsEventComment removeAmsEventComment(AmsEventComment amsEventComment) {
		getAmsEventComments().remove(amsEventComment);
		amsEventComment.setAmsProfileDetail(null);

		return amsEventComment;
	}

	public List<AmsGroupMember> getAmsGroupMembers() {
		return this.amsGroupMembers;
	}

	public void setAmsGroupMembers(List<AmsGroupMember> amsGroupMembers) {
		this.amsGroupMembers = amsGroupMembers;
	}

	public AmsGroupMember addAmsGroupMember(AmsGroupMember amsGroupMember) {
		getAmsGroupMembers().add(amsGroupMember);
		amsGroupMember.setAmsProfileDetail(this);

		return amsGroupMember;
	}

	public AmsGroupMember removeAmsGroupMember(AmsGroupMember amsGroupMember) {
		getAmsGroupMembers().remove(amsGroupMember);
		amsGroupMember.setAmsProfileDetail(null);

		return amsGroupMember;
	}

	public List<AmsPostComment> getAmsPostComments() {
		return this.amsPostComments;
	}

	public void setAmsPostComments(List<AmsPostComment> amsPostComments) {
		this.amsPostComments = amsPostComments;
	}

	public AmsPostComment addAmsPostComment(AmsPostComment amsPostComment) {
		getAmsPostComments().add(amsPostComment);
		amsPostComment.setAmsProfileDetail(this);

		return amsPostComment;
	}

	public AmsPostComment removeAmsPostComment(AmsPostComment amsPostComment) {
		getAmsPostComments().remove(amsPostComment);
		amsPostComment.setAmsProfileDetail(null);

		return amsPostComment;
	}

	public List<AmsPost> getAmsPosts() {
		return this.amsPosts;
	}

	public void setAmsPosts(List<AmsPost> amsPosts) {
		this.amsPosts = amsPosts;
	}

	public AmsPost addAmsPost(AmsPost amsPost) {
		getAmsPosts().add(amsPost);
		amsPost.setAmsProfileDetail(this);

		return amsPost;
	}

	public AmsPost removeAmsPost(AmsPost amsPost) {
		getAmsPosts().remove(amsPost);
		amsPost.setAmsProfileDetail(null);

		return amsPost;
	}

	public AmsIndustry getAmsIndustry() {
		return this.amsIndustry;
	}

	public void setAmsIndustry(AmsIndustry amsIndustry) {
		this.amsIndustry = amsIndustry;
	}

	public List<AmsStudentEducation> getAmsStudentEducations() {
		return this.amsStudentEducations;
	}

	public void setAmsStudentEducations(List<AmsStudentEducation> amsStudentEducations) {
		this.amsStudentEducations = amsStudentEducations;
	}

	public AmsStudentEducation addAmsStudentEducation(AmsStudentEducation amsStudentEducation) {
		getAmsStudentEducations().add(amsStudentEducation);
		amsStudentEducation.setAmsProfileDetail(this);

		return amsStudentEducation;
	}

	public AmsStudentEducation removeAmsStudentEducation(AmsStudentEducation amsStudentEducation) {
		getAmsStudentEducations().remove(amsStudentEducation);
		amsStudentEducation.setAmsProfileDetail(null);

		return amsStudentEducation;
	}

	public List<AmsStudentWorkEmp> getAmsStudentWorkEmps() {
		return this.amsStudentWorkEmps;
	}

	public void setAmsStudentWorkEmps(List<AmsStudentWorkEmp> amsStudentWorkEmps) {
		this.amsStudentWorkEmps = amsStudentWorkEmps;
	}

	public AmsStudentWorkEmp addAmsStudentWorkEmp(AmsStudentWorkEmp amsStudentWorkEmp) {
		getAmsStudentWorkEmps().add(amsStudentWorkEmp);
		amsStudentWorkEmp.setAmsProfileDetail(this);

		return amsStudentWorkEmp;
	}

	public AmsStudentWorkEmp removeAmsStudentWorkEmp(AmsStudentWorkEmp amsStudentWorkEmp) {
		getAmsStudentWorkEmps().remove(amsStudentWorkEmp);
		amsStudentWorkEmp.setAmsProfileDetail(null);

		return amsStudentWorkEmp;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Batch getBatch() {
		return batch;
	}

	public void setBatch(Batch batch) {
		this.batch = batch;
	}



	public Boolean getIsWishtoDonate() {
		return isWishtoDonate;
	}

	public void setIsWishtoDonate(Boolean isWishtoDonate) {
		this.isWishtoDonate = isWishtoDonate;
	}

	public Date getDonationAvailableFrom() {
		return donationAvailableFrom;
	}

	public void setDonationAvailableFrom(Date donationAvailableFrom) {
		this.donationAvailableFrom = donationAvailableFrom;
	}

	public GeneralDetail getBloodGroupCatDet() {
		return bloodGroupCatDet;
	}

	public void setBloodGroupCatDet(GeneralDetail bloodGroupCatDet) {
		this.bloodGroupCatDet = bloodGroupCatDet;
	}

	public String getTwitterProfile() {
		return twitterProfile;
	}

	public void setTwitterProfile(String twitterProfile) {
		this.twitterProfile = twitterProfile;
	}

	public String getGithubProfile() {
		return githubProfile;
	}

	public void setGithubProfile(String githubProfile) {
		this.githubProfile = githubProfile;
	}

	public String getBehanceProfile() {
		return behanceProfile;
	}

	public void setBehanceProfile(String behanceProfile) {
		this.behanceProfile = behanceProfile;
	}

	public String getFacebookProfile() {
		return facebookProfile;
	}

	public void setFacebookProfile(String facebookProfile) {
		this.facebookProfile = facebookProfile;
	}

	public String getInstagramProfile() {
		return instagramProfile;
	}

	public void setInstagramProfile(String instagramProfile) {
		this.instagramProfile = instagramProfile;
	}

	public String getLinkedinProfile() {
		return linkedinProfile;
	}

	public void setLinkedinProfile(String linkedinProfile) {
		this.linkedinProfile = linkedinProfile;
	}
}