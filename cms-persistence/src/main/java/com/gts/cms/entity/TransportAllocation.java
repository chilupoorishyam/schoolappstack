package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_tm_transport_allocation database table.
 * 
 */
@Entity
@Table(name="t_tm_transport_allocation")
@NamedQuery(name="TransportAllocation.findAll", query="SELECT t FROM TransportAllocation t")
public class TransportAllocation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_transport_allocation_id")
	private Long transportAllocationId;

	@Column(name="allocation_for")
	private String allocationFor;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="from_date")
	private Date fromDate;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="to_date")
	private Date toDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FeePayment
	@OneToMany(mappedBy="transportAllocation",fetch = FetchType.LAZY)
	private List<TransportFeePayment> feePayments;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_id")
	private EmployeeDetail employeeDetail;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_student_id")
	private StudentDetail studentDetail;
	
	//bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;

	//bi-directional many-to-one association to RouteStop
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_pickup_route_stop_id")
	private RouteStop pickupRouteStop;

	//bi-directional many-to-one association to RouteStop
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_drop_route_stop_id")
	private RouteStop dropRouteStop;

	//bi-directional many-to-one association to TransportDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_transport_detail_id")
	private TransportDetail transportDetail;
	
	/*@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_vechicle_route_id")
	private VechicleRoute vechicleRoute;*/

	public TransportAllocation() {
	}

	public Long getTransportAllocationId() {
		return this.transportAllocationId;
	}

	public void setTransportAllocationId(Long transportAllocationId) {
		this.transportAllocationId = transportAllocationId;
	}

	public String getAllocationFor() {
		return this.allocationFor;
	}

	public void setAllocationFor(String allocationFor) {
		this.allocationFor = allocationFor;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<TransportFeePayment> getFeePayments() {
		return this.feePayments;
	}

	public void setFeePayments(List<TransportFeePayment> feePayments) {
		this.feePayments = feePayments;
	}

	public TransportFeePayment addFeePayment(TransportFeePayment feePayment) {
		getFeePayments().add(feePayment);
		feePayment.setTransportAllocation(this);

		return feePayment;
	}

	public TransportFeePayment removeFeePayment(TransportFeePayment feePayment) {
		getFeePayments().remove(feePayment);
		feePayment.setTransportAllocation(null);

		return feePayment;
	}

	public EmployeeDetail getEmployeeDetail() {
		return this.employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public RouteStop getPickupRouteStop() {
		return this.pickupRouteStop;
	}

	public void setPickupRouteStop(RouteStop pickupRouteStop) {
		this.pickupRouteStop = pickupRouteStop;
	}

	public RouteStop getDropRouteStop() {
		return this.dropRouteStop;
	}

	public void setDropRouteStop(RouteStop dropRouteStop) {
		this.dropRouteStop = dropRouteStop;
	}

	public TransportDetail getTransportDetail() {
		return this.transportDetail;
	}

	public void setTransportDetail(TransportDetail transportDetail) {
		this.transportDetail = transportDetail;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}
	

	/*public VechicleRoute getVechicleRoute() {
		return vechicleRoute;
	}

	public void setVechicleRoute(VechicleRoute vechicleRoute) {
		this.vechicleRoute = vechicleRoute;
	}*/

	
	
}