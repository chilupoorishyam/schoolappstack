package com.gts.cms.entity;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="t_pg_transactions")
@NamedQuery(name="PgTransaction.findAll", query="SELECT p FROM PgTransaction p")
public class PgTransaction implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="pk_transaction_id")
    private Long transactionId;

    private BigDecimal amount;

    @Column(name="bank_ref_no")
    private String bankRefNo;

    @Column(name="billing_address")
    private String billingAddress;

    @Column(name="billing_city")
    private String billingCity;

    @Column(name="billing_country")
    private String billingCountry;

    @Column(name="billing_email")
    private String billingEmail;

    @Column(name="billing_name")
    private String billingName;

    @Column(name="billing_notes")
    private String billingNotes;

    @Column(name="billing_state")
    private String billingState;

    @Column(name="billing_tel")
    private String billingTel;

    @Column(name="billing_zip")
    private String billingZip;

    @Column(name="card_name")
    private String cardName;

    private String currency;

    @Column(name="customer_card_id")
    private String customerCardId;

    @Column(name="customer_identifier")
    private String customerIdentifier;

    @Column(name="discount_value")
    private BigDecimal discountValue;

    @Column(name="failure_message")
    private String failureMessage;

    @Column(name="fk_student_id")
    private Long fkStudentId;

    @Column(name="ip_address")
    private String ipAddress;

    @Column(name="mer_amount")
    private BigDecimal merAmount;

    @Column(name="merchant_param1")
    private String merchantParam1;

    @Column(name="merchant_param2")
    private String merchantParam2;

    @Column(name="merchant_param3")
    private String merchantParam3;

    @Column(name="offer_code")
    private String offerCode;

    @Column(name="offer_type")
    private String offerType;

    @Column(name="order_id")
    private String orderId;

    @Column(name="order_status")
    private String orderStatus;

    @Column(name="payment_mode")
    private String paymentMode;

    @Column(name="response_code")
    private String responseCode;

    private String retry;

    @Column(name="service_tax")
    private BigDecimal serviceTax;

    @Column(name="status_code")
    private String statusCode;

    @Column(name="status_message")
    private String statusMessage;

    private String tid;

    @Column(name="tracking_id")
    private String trackingId;

    @Column(name="trans_fee")
    private BigDecimal transFee;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="tx_completed_time")
    private Date txCompletedTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="tx_start_time")
    private Date txStartTime;

    @Column(name="user_agent")
    private String userAgent;

    private String vault;

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getBankRefNo() {
		return bankRefNo;
	}

	public void setBankRefNo(String bankRefNo) {
		this.bankRefNo = bankRefNo;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getBillingCity() {
		return billingCity;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	public String getBillingCountry() {
		return billingCountry;
	}

	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}

	public String getBillingEmail() {
		return billingEmail;
	}

	public void setBillingEmail(String billingEmail) {
		this.billingEmail = billingEmail;
	}

	public String getBillingName() {
		return billingName;
	}

	public void setBillingName(String billingName) {
		this.billingName = billingName;
	}

	public String getBillingNotes() {
		return billingNotes;
	}

	public void setBillingNotes(String billingNotes) {
		this.billingNotes = billingNotes;
	}

	public String getBillingState() {
		return billingState;
	}

	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}

	public String getBillingTel() {
		return billingTel;
	}

	public void setBillingTel(String billingTel) {
		this.billingTel = billingTel;
	}

	public String getBillingZip() {
		return billingZip;
	}

	public void setBillingZip(String billingZip) {
		this.billingZip = billingZip;
	}

	public String getCardName() {
		return cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCustomerCardId() {
		return customerCardId;
	}

	public void setCustomerCardId(String customerCardId) {
		this.customerCardId = customerCardId;
	}

	public String getCustomerIdentifier() {
		return customerIdentifier;
	}

	public void setCustomerIdentifier(String customerIdentifier) {
		this.customerIdentifier = customerIdentifier;
	}

	public BigDecimal getDiscountValue() {
		return discountValue;
	}

	public void setDiscountValue(BigDecimal discountValue) {
		this.discountValue = discountValue;
	}

	public String getFailureMessage() {
		return failureMessage;
	}

	public void setFailureMessage(String failureMessage) {
		this.failureMessage = failureMessage;
	}

	public Long getFkStudentId() {
		return fkStudentId;
	}

	public void setFkStudentId(Long fkStudentId) {
		this.fkStudentId = fkStudentId;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public BigDecimal getMerAmount() {
		return merAmount;
	}

	public void setMerAmount(BigDecimal merAmount) {
		this.merAmount = merAmount;
	}

	public String getMerchantParam1() {
		return merchantParam1;
	}

	public void setMerchantParam1(String merchantParam1) {
		this.merchantParam1 = merchantParam1;
	}

	public String getMerchantParam2() {
		return merchantParam2;
	}

	public void setMerchantParam2(String merchantParam2) {
		this.merchantParam2 = merchantParam2;
	}

	public String getMerchantParam3() {
		return merchantParam3;
	}

	public void setMerchantParam3(String merchantParam3) {
		this.merchantParam3 = merchantParam3;
	}

	public String getOfferCode() {
		return offerCode;
	}

	public void setOfferCode(String offerCode) {
		this.offerCode = offerCode;
	}

	public String getOfferType() {
		return offerType;
	}

	public void setOfferType(String offerType) {
		this.offerType = offerType;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getRetry() {
		return retry;
	}

	public void setRetry(String retry) {
		this.retry = retry;
	}

	public BigDecimal getServiceTax() {
		return serviceTax;
	}

	public void setServiceTax(BigDecimal serviceTax) {
		this.serviceTax = serviceTax;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getTrackingId() {
		return trackingId;
	}

	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}

	public BigDecimal getTransFee() {
		return transFee;
	}

	public void setTransFee(BigDecimal transFee) {
		this.transFee = transFee;
	}

	public Date getTxCompletedTime() {
		return txCompletedTime;
	}

	public void setTxCompletedTime(Date txCompletedTime) {
		this.txCompletedTime = txCompletedTime;
	}

	public Date getTxStartTime() {
		return txStartTime;
	}

	public void setTxStartTime(Date txStartTime) {
		this.txStartTime = txStartTime;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getVault() {
		return vault;
	}

	public void setVault(String vault) {
		this.vault = vault;
	}
    
    
}