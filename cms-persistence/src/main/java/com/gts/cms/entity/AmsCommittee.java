package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_ams_committee database table.
 * 
 */
@Entity
@Table(name="t_ams_committee")
@NamedQuery(name="AmsCommittee.findAll", query="SELECT a FROM AmsCommittee a")
public class AmsCommittee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_committee_member_id")
	private Long committeeMemberId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	//bi-directional many-to-one association to Designation
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_designation_id")
	private Designation designation;
	
	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_id")
	private EmployeeDetail employeedetail;
	
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="fk_committee_role_catdet_id")
//	private GeneralDetail committeeRoleCatDet;
	
	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;
	
	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="from_date")
	private Date fromDate;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="to_date")
	private Date toDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	public AmsCommittee() {
	}

	public Long getCommitteeMemberId() {
		return this.committeeMemberId;
	}

	public void setCommitteeMemberId(Long committeeMemberId) {
		this.committeeMemberId = committeeMemberId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Designation getDesignation() {
		return designation;
	}

	public void setDesignation(Designation designation) {
		this.designation = designation;
	}

	public EmployeeDetail getEmployeedetail() {
		return employeedetail;
	}

	public void setEmployeedetail(EmployeeDetail employeedetail) {
		this.employeedetail = employeedetail;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}
	
//	public GeneralDetail getCommitteeRoleCatDet() {
//		return committeeRoleCatDet;
//	}
//
//	public void setCommitteeRoleCatDet(GeneralDetail committeeRoleCatDet) {
//		this.committeeRoleCatDet = committeeRoleCatDet;
//	}
	

}