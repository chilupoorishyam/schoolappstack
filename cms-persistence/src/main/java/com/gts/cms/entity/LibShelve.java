package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_lib_shelves database table.
 * 
 */
@Entity
@Table(name="t_lib_shelves")
@NamedQuery(name="LibShelve.findAll", query="SELECT l FROM LibShelve l")
public class LibShelve implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_lib_shelve_id")
	private Long shelveId;

	@Column(name="block_capacity")
	private Integer blockCapacity;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String location;

	@Column(name="no_of_columns")
	private Integer noOfColumns;

	@Column(name="no_of_rows")
	private Integer noOfRows;

	private String reason;

	@Column(name="shelve_code")
	private String shelveCode;

	@Column(name="shelve_name")
	private String shelveName;

	@Column(name="total_capacity")
	private Integer totalCapacity;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to PeriodicalsDetail
	@OneToMany(mappedBy="libShelve")
	private List<PeriodicalsDetail> periodicalsDetails;

	//bi-directional many-to-one association to Organization
	@ManyToOne
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to LibraryDetail
	@ManyToOne
	@JoinColumn(name="fk_library_id")
	private LibraryDetail libraryDetail;

	public LibShelve() {
	}

	public Long getShelveId() {
		return this.shelveId;
	}

	public void setShelveId(Long shelveId) {
		this.shelveId = shelveId;
	}

	public Integer getBlockCapacity() {
		return this.blockCapacity;
	}

	public void setBlockCapacity(Integer blockCapacity) {
		this.blockCapacity = blockCapacity;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getNoOfColumns() {
		return this.noOfColumns;
	}

	public void setNoOfColumns(Integer noOfColumns) {
		this.noOfColumns = noOfColumns;
	}

	public Integer getNoOfRows() {
		return this.noOfRows;
	}

	public void setNoOfRows(Integer noOfRows) {
		this.noOfRows = noOfRows;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getShelveCode() {
		return this.shelveCode;
	}

	public void setShelveCode(String shelveCode) {
		this.shelveCode = shelveCode;
	}

	public String getShelveName() {
		return this.shelveName;
	}

	public void setShelveName(String shelveName) {
		this.shelveName = shelveName;
	}

	public Integer getTotalCapacity() {
		return this.totalCapacity;
	}

	public void setTotalCapacity(Integer totalCapacity) {
		this.totalCapacity = totalCapacity;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<PeriodicalsDetail> getPeriodicalsDetails() {
		return this.periodicalsDetails;
	}

	public void setPeriodicalsDetails(List<PeriodicalsDetail> periodicalsDetails) {
		this.periodicalsDetails = periodicalsDetails;
	}

	public PeriodicalsDetail addPeriodicalsDetail(PeriodicalsDetail periodicalsDetail) {
		getPeriodicalsDetails().add(periodicalsDetail);
		periodicalsDetail.setLibShelve(this);

		return periodicalsDetail;
	}

	public PeriodicalsDetail removePeriodicalsDetail(PeriodicalsDetail periodicalsDetail) {
		getPeriodicalsDetails().remove(periodicalsDetail);
		periodicalsDetail.setLibShelve(null);

		return periodicalsDetail;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public LibraryDetail getLibraryDetail() {
		return this.libraryDetail;
	}

	public void setLibraryDetail(LibraryDetail libraryDetail) {
		this.libraryDetail = libraryDetail;
	}

}