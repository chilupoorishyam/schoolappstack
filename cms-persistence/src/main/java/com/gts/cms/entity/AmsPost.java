package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_ams_posts database table.
 * 
 */
@Entity
@Table(name = "t_ams_posts")
@NamedQuery(name = "AmsPost.findAll", query = "SELECT a FROM AmsPost a")
public class AmsPost implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_ams_post_id")
	private Long amsPostId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_ams_emp_id")
	private EmployeeDetail amsEmpId;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_posttype_catdet_id")
	private GeneralDetail posttypeCatdetId;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_verifiedby_emp_id")
	private EmployeeDetail verifiedbyEmpId;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_published")
	private Boolean isPublished;

	@Column(name = "job_email_id")
	private String jobEmailId;

	@Column(name = "is_verifiedby_incharge")
	private Boolean isVerifiedbyIncharge;

	@Column(name = "job_company_name")
	private String jobCompanyName;

	private String location;

	@Column(name = "other_posttype")
	private String otherPosttype;

	@Column(name = "post_attachement_path")
	private String postAttachementPath;

	@Column(name = "post_attachement_path1")
	private String postAttachementPath1;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "post_date")
	private Date postDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "post_last_date")
	private Date postLastDate;

	private String postdetails;

	private String postheading;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "published_on")
	private Date publishedOn;

	private String reason;

	@Column(name = "reference_details")
	private String referenceDetails;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "verification_comments")
	private String verificationComments;

	// bi-directional many-to-one association to AmsPostComment
	@OneToMany(mappedBy = "amsPost", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<AmsPostComment> amsPostComments;

	// bi-directional many-to-one association to AmsProfileDetail
	@ManyToOne
	@JoinColumn(name = "fk_ams_profile_id")
	private AmsProfileDetail amsProfileDetail;

	@Column(name = "no_of_positions")
	private Integer noOfPositions;

	@Column(name = "minexp")
	private Integer minExp;

	@Column(name = "maxexp")
	private Integer maxExp;

	@Column(name = "minctc")
	private BigDecimal minCTC;

	@Column(name = "maxctc")
	private BigDecimal maxCTC;

	@Column(name = "receiveapplicationfromothgrp")
	private Boolean receiveAppfromothgrp;

	public AmsPost() {
	}

	public Long getAmsPostId() {
		return this.amsPostId;
	}

	public void setAmsPostId(Long amsPostId) {
		this.amsPostId = amsPostId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public EmployeeDetail getAmsEmpId() {
		return amsEmpId;
	}

	public void setAmsEmpId(EmployeeDetail amsEmpId) {
		this.amsEmpId = amsEmpId;
	}

	public GeneralDetail getPosttypeCatdetId() {
		return posttypeCatdetId;
	}

	public void setPosttypeCatdetId(GeneralDetail posttypeCatdetId) {
		this.posttypeCatdetId = posttypeCatdetId;
	}

	public EmployeeDetail getVerifiedbyEmpId() {
		return verifiedbyEmpId;
	}

	public void setVerifiedbyEmpId(EmployeeDetail verifiedbyEmpId) {
		this.verifiedbyEmpId = verifiedbyEmpId;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsPublished() {
		return this.isPublished;
	}

	public void setIsPublished(Boolean isPublished) {
		this.isPublished = isPublished;
	}

	public Boolean getIsVerifiedbyIncharge() {
		return this.isVerifiedbyIncharge;
	}

	public void setIsVerifiedbyIncharge(Boolean isVerifiedbyIncharge) {
		this.isVerifiedbyIncharge = isVerifiedbyIncharge;
	}

	public String getJobCompanyName() {
		return this.jobCompanyName;
	}

	public void setJobCompanyName(String jobCompanyName) {
		this.jobCompanyName = jobCompanyName;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getOtherPosttype() {
		return this.otherPosttype;
	}

	public void setOtherPosttype(String otherPosttype) {
		this.otherPosttype = otherPosttype;
	}

	public String getPostAttachementPath() {
		return this.postAttachementPath;
	}

	public void setPostAttachementPath(String postAttachementPath) {
		this.postAttachementPath = postAttachementPath;
	}

	public String getPostAttachementPath1() {
		return this.postAttachementPath1;
	}

	public void setPostAttachementPath1(String postAttachementPath1) {
		this.postAttachementPath1 = postAttachementPath1;
	}

	public Date getPostDate() {
		return this.postDate;
	}

	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}

	public Date getPostLastDate() {
		return this.postLastDate;
	}

	public void setPostLastDate(Date postLastDate) {
		this.postLastDate = postLastDate;
	}

	public String getPostdetails() {
		return this.postdetails;
	}

	public void setPostdetails(String postdetails) {
		this.postdetails = postdetails;
	}

	public String getPostheading() {
		return this.postheading;
	}

	public void setPostheading(String postheading) {
		this.postheading = postheading;
	}

	public Date getPublishedOn() {
		return this.publishedOn;
	}

	public void setPublishedOn(Date publishedOn) {
		this.publishedOn = publishedOn;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReferenceDetails() {
		return this.referenceDetails;
	}

	public void setReferenceDetails(String referenceDetails) {
		this.referenceDetails = referenceDetails;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getVerificationComments() {
		return this.verificationComments;
	}

	public void setVerificationComments(String verificationComments) {
		this.verificationComments = verificationComments;
	}

	public List<AmsPostComment> getAmsPostComments() {
		return this.amsPostComments;
	}

	public void setAmsPostComments(List<AmsPostComment> amsPostComments) {
		this.amsPostComments = amsPostComments;
	}

	public AmsPostComment addAmsPostComment(AmsPostComment amsPostComment) {
		getAmsPostComments().add(amsPostComment);
		amsPostComment.setAmsPost(this);

		return amsPostComment;
	}

	public AmsPostComment removeAmsPostComment(AmsPostComment amsPostComment) {
		getAmsPostComments().remove(amsPostComment);
		amsPostComment.setAmsPost(null);

		return amsPostComment;
	}

	public AmsProfileDetail getAmsProfileDetail() {
		return this.amsProfileDetail;
	}

	public void setAmsProfileDetail(AmsProfileDetail amsProfileDetail) {
		this.amsProfileDetail = amsProfileDetail;
	}

	public String getJobEmailId() {
		return jobEmailId;
	}

	public void setJobEmailId(String jobEmailId) {
		this.jobEmailId = jobEmailId;
	}

	public Integer getNoOfPositions() {
		return noOfPositions;
	}

	public void setNoOfPositions(Integer noOfPositions) {
		this.noOfPositions = noOfPositions;
	}

	public Integer getMinExp() {
		return minExp;
	}

	public void setMinExp(Integer minExp) {
		this.minExp = minExp;
	}

	public Integer getMaxExp() {
		return maxExp;
	}

	public void setMaxExp(Integer maxExp) {
		this.maxExp = maxExp;
	}

	public BigDecimal getMinCTC() {
		return minCTC;
	}

	public void setMinCTC(BigDecimal minCTC) {
		this.minCTC = minCTC;
	}

	public BigDecimal getMaxCTC() {
		return maxCTC;
	}

	public void setMaxCTC(BigDecimal maxCTC) {
		this.maxCTC = maxCTC;
	}

	public Boolean getReceiveAppfromothgrp() {
		return receiveAppfromothgrp;
	}

	public void setReceiveAppfromothgrp(Boolean receiveAppfromothgrp) {
		this.receiveAppfromothgrp = receiveAppfromothgrp;
	}

}