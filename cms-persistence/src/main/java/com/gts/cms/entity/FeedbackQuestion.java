package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_hr_feedback_questions database table.
 * 
 */
@Entity
@Table(name="t_hr_feedback_questions")
@NamedQuery(name="FeedbackQuestion.findAll", query="SELECT f FROM FeedbackQuestion f")
public class FeedbackQuestion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fb_question_id")
	private Long fbQuestionId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="fb_discription")
	private String fbDiscription;

	@Column(name="fb_question")
	private String fbQuestion;

	@Column(name="fk_dependent_question_id")
	private Long dependentQuestionId;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_allow_multiple_answers")
	private Boolean isAllowMultipleAnswers;

	@Column(name="is_answerrequired")
	private Boolean isAnswerrequired;

	@Column(name="is_questionrequired")
	private Boolean isQuestionrequired;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FbOptionGroup
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fb_optiongroup_id")
	private FbOptionGroup fbOptionGroup;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fbinputtype_catdet_id")
	private GeneralDetail fbinputtype;

	//bi-directional many-to-one association to SurveyDetail
	@OneToMany(mappedBy="feedbackQuestion",fetch = FetchType.LAZY)
	private List<SurveyDetail> surveyDetails;

	public FeedbackQuestion() {
	}

	public Long getFbQuestionId() {
		return this.fbQuestionId;
	}

	public void setFbQuestionId(Long fbQuestionId) {
		this.fbQuestionId = fbQuestionId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getFbDiscription() {
		return this.fbDiscription;
	}

	public void setFbDiscription(String fbDiscription) {
		this.fbDiscription = fbDiscription;
	}

	public String getFbQuestion() {
		return this.fbQuestion;
	}

	public void setFbQuestion(String fbQuestion) {
		this.fbQuestion = fbQuestion;
	}

	public Long getDependentQuestionId() {
		return this.dependentQuestionId;
	}

	public void setDependentQuestionId(Long dependentQuestionId) {
		this.dependentQuestionId = dependentQuestionId;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsAllowMultipleAnswers() {
		return this.isAllowMultipleAnswers;
	}

	public void setIsAllowMultipleAnswers(Boolean isAllowMultipleAnswers) {
		this.isAllowMultipleAnswers = isAllowMultipleAnswers;
	}

	public Boolean getIsAnswerrequired() {
		return this.isAnswerrequired;
	}

	public void setIsAnswerrequired(Boolean isAnswerrequired) {
		this.isAnswerrequired = isAnswerrequired;
	}

	public Boolean getIsQuestionrequired() {
		return this.isQuestionrequired;
	}

	public void setIsQuestionrequired(Boolean isQuestionrequired) {
		this.isQuestionrequired = isQuestionrequired;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public FbOptionGroup getFbOptionGroup() {
		return this.fbOptionGroup;
	}

	public void setFbOptionGroup(FbOptionGroup fbOptionGroup) {
		this.fbOptionGroup = fbOptionGroup;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getFbinputtype() {
		return this.fbinputtype;
	}

	public void setFbinputtype(GeneralDetail fbinputtype) {
		this.fbinputtype = fbinputtype;
	}

	public List<SurveyDetail> getSurveyDetails() {
		return this.surveyDetails;
	}

	public void setSurveyDetails(List<SurveyDetail> surveyDetails) {
		this.surveyDetails = surveyDetails;
	}

	public SurveyDetail addSurveyDetail(SurveyDetail surveyDetail) {
		getSurveyDetails().add(surveyDetail);
		surveyDetail.setFeedbackQuestion(this);

		return surveyDetail;
	}

	public SurveyDetail removeSurveyDetail(SurveyDetail surveyDetail) {
		getSurveyDetails().remove(surveyDetail);
		surveyDetail.setFeedbackQuestion(null);

		return surveyDetail;
	}

}