package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_cm_std_elective_subject_request database
 * table.
 * 
 */
@Entity
@Table(name = "t_cm_std_elective_subject_request")
@NamedQuery(name = "ElectiveSubjectRequest.findAll", query = "SELECT e FROM ElectiveSubjectRequest e")
public class ElectiveSubjectRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_std_elective_std_req_id")
	private Long electiveSubReqId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	private Boolean isfuturesubject;

	private Boolean ismapped;

	private Integer priority;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "request_date")
	private Date requestDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to Subject
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_subject_id")
	private Subject subject;

	// bi-directional many-to-one association to Subjectregulation
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_actual_subreg_id")
	private Subjectregulation subjectregulation;



	// bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_academic_year_id")
	private AcademicYear academicYear;

	// bi-directional many-to-one association to Batch
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_batch_id")
	private Batch batch;

	// bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_year_id")
	private CourseYear courseYear;

	// bi-directional many-to-one association to GroupSection
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_group_section_id")
	private GroupSection groupSection;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_subjecttype_catdet_id")
	private GeneralDetail subjecttype;

	// bi-directional many-to-one association to Studentbatch
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_stdbatch_id")
	private Studentbatch studentbatch;

	// bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	public ElectiveSubjectRequest() {
	}

	public Long getElectiveSubReqId() {
		return this.electiveSubReqId;
	}

	public void setElectiveSubReqId(Long electiveSubReqId) {
		this.electiveSubReqId = electiveSubReqId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsfuturesubject() {
		return this.isfuturesubject;
	}

	public void setIsfuturesubject(Boolean isfuturesubject) {
		this.isfuturesubject = isfuturesubject;
	}

	public Boolean getIsmapped() {
		return this.ismapped;
	}

	public void setIsmapped(Boolean ismapped) {
		this.ismapped = ismapped;
	}

	public Integer getPriority() {
		return this.priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getRequestDate() {
		return this.requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Subject getSubject() {
		return this.subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Subjectregulation getSubjectregulation() {
		return this.subjectregulation;
	}

	public void setSubjectregulation(Subjectregulation subjectregulation) {
		this.subjectregulation = subjectregulation;
	}



	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public Batch getBatch() {
		return this.batch;
	}

	public void setBatch(Batch batch) {
		this.batch = batch;
	}

	public CourseYear getCourseYear() {
		return this.courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public GroupSection getGroupSection() {
		return this.groupSection;
	}

	public void setGroupSection(GroupSection groupSection) {
		this.groupSection = groupSection;
	}

	public GeneralDetail getSubjecttype() {
		return this.subjecttype;
	}

	public void setSubjecttype(GeneralDetail subjecttype) {
		this.subjecttype = subjecttype;
	}

	public Studentbatch getStudentbatch() {
		return this.studentbatch;
	}

	public void setStudentbatch(Studentbatch studentbatch) {
		this.studentbatch = studentbatch;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

}