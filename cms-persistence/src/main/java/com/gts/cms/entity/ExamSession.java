package com.gts.cms.entity;

import java.io.Serializable;
import java.sql.Time;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_exam_sessions database table.
 * 
 */
@Entity
@Table(name = "t_exam_sessions")
@NamedQuery(name = "ExamSession.findAll", query = "SELECT e FROM ExamSession e")
public class ExamSession implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_session_id")
	private Long examSessionId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "exam_session_name")
	private String examSessionName;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Column(name = "session_end_time")
	private Time sessionEndTime;

	@Column(name = "session_start_time")
	private Time sessionStartTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getExamsessioninCat() {
		return examsessioninCat;
	}

	public void setExamsessioninCat(GeneralDetail examsessioninCat) {
		this.examsessioninCat = examsessioninCat;
	}

	public List<ExamTimetable> getExamTimetables() {
		return examTimetables;
	}

	public void setExamTimetables(List<ExamTimetable> examTimetables) {
		this.examTimetables = examTimetables;
	}

	// bi-directional many-to-one association to
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_examsessionin_catdet_id")
	private GeneralDetail examsessioninCat;

	// bi-directional many-to-one association to ExamTimetable
	@OneToMany(mappedBy = "examSession")
	private List<ExamTimetable> examTimetables;

	public ExamSession() {
	}

	public Long getExamSessionId() {
		return this.examSessionId;
	}

	public void setExamSessionId(Long examSessionId) {
		this.examSessionId = examSessionId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getExamSessionName() {
		return this.examSessionName;
	}

	public void setExamSessionName(String examSessionName) {
		this.examSessionName = examSessionName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
	public Time getSessionEndTime() {
		return sessionEndTime;
	}

	public void setSessionEndTime(Time sessionEndTime) {
		this.sessionEndTime = sessionEndTime;
	}

	public Time getSessionStartTime() {
		return sessionStartTime;
	}

	public void setSessionStartTime(Time sessionStartTime) {
		this.sessionStartTime = sessionStartTime;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}
}