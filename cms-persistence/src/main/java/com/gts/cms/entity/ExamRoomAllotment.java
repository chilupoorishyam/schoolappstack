package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_exam_room_allotment database table.
 * 
 */
@Entity
@Table(name = "t_exam_room_allotment")
@NamedQuery(name = "ExamRoomAllotment.findAll", query = "SELECT e FROM ExamRoomAllotment e")
public class ExamRoomAllotment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_room_allotment_id")
	private Long examRoomAllotmentId;

	@Column(name = "available_seats")
	private Integer availableSeats;

	@Column(name = "blocked_seats")
	private Integer blockedSeats;

	@Column(name = "booked_seats")
	private Integer bookedSeats;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	@Column(name = "exam_date")
	private Date examDate;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to ExamMaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_id")
	private ExamMaster examMaster;

	// bi-directional many-to-one association to ExamTimetable
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_timetable_id")
	private ExamTimetable examTimetable;

	@Column(name = "fk_invigilation_emp_ids")
	private String invigilationEmpIds;

	// bi-directional many-to-one association to Room
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_room_id")
	private Room room;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Column(name = "room_strength")
	private Integer roomStrength;

	@Column(name = "total_columns")
	private Integer totalColumns;

	@Column(name = "total_rows")
	private Integer totalRows;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to ExamRoomStudentsAllot
	@OneToMany(mappedBy = "examRoomAllotment",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<ExamRoomStudentsAllot> examRoomStudentsAllots;

	public ExamRoomAllotment() {
	}

	public Long getExamRoomAllotmentId() {
		return this.examRoomAllotmentId;
	}

	public void setExamRoomAllotmentId(Long examRoomAllotmentId) {
		this.examRoomAllotmentId = examRoomAllotmentId;
	}

	public Integer getAvailableSeats() {
		return this.availableSeats;
	}

	public void setAvailableSeats(Integer availableSeats) {
		this.availableSeats = availableSeats;
	}

	public Integer getBlockedSeats() {
		return this.blockedSeats;
	}

	public void setBlockedSeats(Integer blockedSeats) {
		this.blockedSeats = blockedSeats;
	}

	public Integer getBookedSeats() {
		return this.bookedSeats;
	}

	public void setBookedSeats(Integer bookedSeats) {
		this.bookedSeats = bookedSeats;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getExamDate() {
		return this.examDate;
	}

	public void setExamDate(Date examDate) {
		this.examDate = examDate;
	}

	public String getInvigilationEmpIds() {
		return this.invigilationEmpIds;
	}

	public void setInvigilationEmpIds(String invigilationEmpIds) {
		this.invigilationEmpIds = invigilationEmpIds;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getRoomStrength() {
		return this.roomStrength;
	}

	public void setRoomStrength(Integer roomStrength) {
		this.roomStrength = roomStrength;
	}

	public Integer getTotalColumns() {
		return this.totalColumns;
	}

	public void setTotalColumns(Integer totalColumns) {
		this.totalColumns = totalColumns;
	}

	public Integer getTotalRows() {
		return this.totalRows;
	}

	public void setTotalRows(Integer totalRows) {
		this.totalRows = totalRows;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public ExamMaster getExamMaster() {
		return examMaster;
	}

	public void setExamMaster(ExamMaster examMaster) {
		this.examMaster = examMaster;
	}

	public ExamTimetable getExamTimetable() {
		return examTimetable;
	}

	public void setExamTimetable(ExamTimetable examTimetable) {
		this.examTimetable = examTimetable;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public List<ExamRoomStudentsAllot> getExamRoomStudentsAllots() {
		return examRoomStudentsAllots;
	}

	public void setExamRoomStudentsAllots(List<ExamRoomStudentsAllot> examRoomStudentsAllots) {
		this.examRoomStudentsAllots = examRoomStudentsAllots;
	}
}