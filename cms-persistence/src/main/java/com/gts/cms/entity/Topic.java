package com.gts.cms.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Entity
public class Topic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @NotNull
    private String name;

    private String fileUri;

    private String meetingId;

    @Enumerated(EnumType.STRING)
    private TopicType type;

    @ManyToOne
    @JoinColumn(name = "unit_id")
    private Unit unit;
}
