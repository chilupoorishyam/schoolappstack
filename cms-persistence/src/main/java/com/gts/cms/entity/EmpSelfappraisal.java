package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_emp_selfappraisal database table.
 */
@Data
@Entity
@Table(name = "t_emp_selfappraisal")
@NamedQuery(name = "EmpSelfappraisal.findAll", query = "SELECT c FROM EmpSelfappraisal c")
public class EmpSelfappraisal implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_emp_selfappraisal_id")
    private Long empSelfappraisalId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_date")
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_date")
    private Date endDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "appraisal_submission_date")
    private Date appraisalSubmissionDate;

    @Column(name = "emp_rating")
    private Double empRating;

    @Column(name = "manager_rating")
    private Double managerRating;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "manager_review_date")
    private Date managerReviewDate;

    @Column(name = "management_rating")
    private Double managementRating;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "management_review_date")
    private Date managementReviewDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "status_updated_on")
    private Date statusUpdatedOn;

    @ManyToOne
    @JoinColumn(name = "fk_school_id")
    private School school;

    @ManyToOne
    @JoinColumn(name = "fk_emp_id")
    private EmployeeDetail employeeDetail;

    @ManyToOne
    @JoinColumn(name = "fk_manager_emp_id")
    private EmployeeDetail managerEmpDetail;

    @ManyToOne
    @JoinColumn(name = "fk_management_emp_id")
    private EmployeeDetail managementEmployeeDetail;

    @ManyToOne
    @JoinColumn(name = "fk_academic_year_id")
    private AcademicYear academicYear;

    @ManyToOne
    @JoinColumn(name = "fk_selfappraisal_form_id")
    private EmpSelfappraisalForm selfappraisalFormId;

    @ManyToOne
    @JoinColumn(name = "fk_appraisal_status_wf_id")
    private GeneralDetail appraisalStatusWfId;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    @OneToMany(mappedBy = "empSelfappraisalId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<EmpSelfappraisalDetail> empSelfappraisalDetails;
}