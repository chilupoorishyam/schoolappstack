package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_fee_category database table.
 * 
 */
@Entity
@Table(name="t_fee_category")
@NamedQuery(name="FeeCategory.findAll", query="SELECT f FROM FeeCategory f")
public class FeeCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_category_id")
	private Long feeCategoryId;

	@Column(name="fee_category_name")
	private String categoryName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String description;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_master")
	private Boolean isMaster;

	private String reason;

	@Column(name="sort_order")
	private Integer sortOrder;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to FeeDiscountsParticular
	@OneToMany(mappedBy="feeCategory",fetch = FetchType.LAZY)
	private List<FeeDiscountsParticular> feeDiscountsParticulars;

	//bi-directional many-to-one association to FeeFine
	@OneToMany(mappedBy="feeCategory",fetch = FetchType.LAZY)
	private List<FeeFine> feeFines;

	//bi-directional many-to-one association to FeeStructureParticular
	@OneToMany(mappedBy="feeCategory" ,fetch = FetchType.LAZY)
	private List<FeeStructureParticular> feeStructureParticulars;

	//bi-directional many-to-one association to FeeStudentWiseParticular
	@OneToMany(mappedBy="feeCategory",fetch = FetchType.LAZY)
	private List<FeeStudentWiseParticular> feeStudentWiseParticulars;

	//bi-directional many-to-one association to FeeStudentwiseDiscount
	@OneToMany(mappedBy="feeCategory",fetch = FetchType.LAZY)
	private List<FeeStudentwiseDiscount> feeStudentwiseDiscounts;

	//bi-directional many-to-one association to FeeStudentwiseFine
	@OneToMany(mappedBy="feeCategory",fetch = FetchType.LAZY)
	private List<FeeStudentwiseFine> feeStudentwiseFines;
	
	//Added by Naveen on 04-12-2018
	
	@Column(name="fee_category_code")
	private String feeCategoryCode;
	
	@Column(name=" is_library")
	private Boolean  isLibrary;
	
	@Column(name="is_hostel")
	private Boolean isHostel;
	
	@Column(name="is_transport")
	private Boolean isTransport;
	
	@Column(name="include_in_ledger")
	private Boolean includeInLedger;
	

	public FeeCategory() {
	}

	public Long getFeeCategoryId() {
		return this.feeCategoryId;
	}

	public void setFeeCategoryId(Long feeCategoryId) {
		this.feeCategoryId = feeCategoryId;
	}

	public String getCategoryName() {
		return this.categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsMaster() {
		return this.isMaster;
	}

	public void setIsMaster(Boolean isMaster) {
		this.isMaster = isMaster;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<FeeDiscountsParticular> getFeeDiscountsParticulars() {
		return this.feeDiscountsParticulars;
	}

	public void setFeeDiscountsParticulars(List<FeeDiscountsParticular> feeDiscountsParticulars) {
		this.feeDiscountsParticulars = feeDiscountsParticulars;
	}

	public FeeDiscountsParticular addFeeDiscountsParticular(FeeDiscountsParticular feeDiscountsParticular) {
		getFeeDiscountsParticulars().add(feeDiscountsParticular);
		feeDiscountsParticular.setFeeCategory(this);

		return feeDiscountsParticular;
	}

	public FeeDiscountsParticular removeFeeDiscountsParticular(FeeDiscountsParticular feeDiscountsParticular) {
		getFeeDiscountsParticulars().remove(feeDiscountsParticular);
		feeDiscountsParticular.setFeeCategory(null);

		return feeDiscountsParticular;
	}

	public List<FeeFine> getFeeFines() {
		return this.feeFines;
	}

	public void setFeeFines(List<FeeFine> feeFines) {
		this.feeFines = feeFines;
	}

	public FeeFine addFeeFine(FeeFine feeFine) {
		getFeeFines().add(feeFine);
		feeFine.setFeeCategory(this);

		return feeFine;
	}

	public FeeFine removeFeeFine(FeeFine feeFine) {
		getFeeFines().remove(feeFine);
		feeFine.setFeeCategory(null);

		return feeFine;
	}

	public List<FeeStructureParticular> getFeeStructureParticulars() {
		return this.feeStructureParticulars;
	}

	public void setFeeStructureParticulars(List<FeeStructureParticular> feeStructureParticulars) {
		this.feeStructureParticulars = feeStructureParticulars;
	}

	public FeeStructureParticular addFeeStructureParticular(FeeStructureParticular feeStructureParticular) {
		getFeeStructureParticulars().add(feeStructureParticular);
		feeStructureParticular.setFeeCategory(this);

		return feeStructureParticular;
	}

	public FeeStructureParticular removeFeeStructureParticular(FeeStructureParticular feeStructureParticular) {
		getFeeStructureParticulars().remove(feeStructureParticular);
		feeStructureParticular.setFeeCategory(null);

		return feeStructureParticular;
	}

	public List<FeeStudentWiseParticular> getFeeStudentWiseParticulars() {
		return this.feeStudentWiseParticulars;
	}

	public void setFeeStudentWiseParticulars(List<FeeStudentWiseParticular> feeStudentWiseParticulars) {
		this.feeStudentWiseParticulars = feeStudentWiseParticulars;
	}

	public FeeStudentWiseParticular addFeeStudentWiseParticular(FeeStudentWiseParticular feeStudentWiseParticular) {
		getFeeStudentWiseParticulars().add(feeStudentWiseParticular);
		feeStudentWiseParticular.setFeeCategory(this);

		return feeStudentWiseParticular;
	}

	public FeeStudentWiseParticular removeFeeStudentWiseParticular(FeeStudentWiseParticular feeStudentWiseParticular) {
		getFeeStudentWiseParticulars().remove(feeStudentWiseParticular);
		feeStudentWiseParticular.setFeeCategory(null);

		return feeStudentWiseParticular;
	}

	public List<FeeStudentwiseDiscount> getFeeStudentwiseDiscounts() {
		return this.feeStudentwiseDiscounts;
	}

	public void setFeeStudentwiseDiscounts(List<FeeStudentwiseDiscount> feeStudentwiseDiscounts) {
		this.feeStudentwiseDiscounts = feeStudentwiseDiscounts;
	}

	public FeeStudentwiseDiscount addFeeStudentwiseDiscount(FeeStudentwiseDiscount feeStudentwiseDiscount) {
		getFeeStudentwiseDiscounts().add(feeStudentwiseDiscount);
		feeStudentwiseDiscount.setFeeCategory(this);

		return feeStudentwiseDiscount;
	}

	public FeeStudentwiseDiscount removeFeeStudentwiseDiscount(FeeStudentwiseDiscount feeStudentwiseDiscount) {
		getFeeStudentwiseDiscounts().remove(feeStudentwiseDiscount);
		feeStudentwiseDiscount.setFeeCategory(null);

		return feeStudentwiseDiscount;
	}

	public List<FeeStudentwiseFine> getFeeStudentwiseFines() {
		return this.feeStudentwiseFines;
	}

	public void setFeeStudentwiseFines(List<FeeStudentwiseFine> feeStudentwiseFines) {
		this.feeStudentwiseFines = feeStudentwiseFines;
	}

	public FeeStudentwiseFine addFeeStudentwiseFine(FeeStudentwiseFine feeStudentwiseFine) {
		getFeeStudentwiseFines().add(feeStudentwiseFine);
		feeStudentwiseFine.setFeeCategory(this);

		return feeStudentwiseFine;
	}

	public FeeStudentwiseFine removeFeeStudentwiseFine(FeeStudentwiseFine feeStudentwiseFine) {
		getFeeStudentwiseFines().remove(feeStudentwiseFine);
		feeStudentwiseFine.setFeeCategory(null);

		return feeStudentwiseFine;
	}

	public String getFeeCategoryCode() {
		return feeCategoryCode;
	}

	public void setFeeCategoryCode(String feeCategoryCode) {
		this.feeCategoryCode = feeCategoryCode;
	}


	public Boolean getIsLibrary() {
		return isLibrary;
	}

	public void setIsLibrary(Boolean isLibrary) {
		this.isLibrary = isLibrary;
	}

	public Boolean getIsHostel() {
		return isHostel;
	}

	public void setIsHostel(Boolean isHostel) {
		this.isHostel = isHostel;
	}

	public Boolean getIsTransport() {
		return isTransport;
	}

	public void setIsTransport(Boolean isTransport) {
		this.isTransport = isTransport;
	}

	public Boolean getIncludeInLedger() {
		return includeInLedger;
	}

	public void setIncludeInLedger(Boolean includeInLedger) {
		this.includeInLedger = includeInLedger;
	}
	
	

}