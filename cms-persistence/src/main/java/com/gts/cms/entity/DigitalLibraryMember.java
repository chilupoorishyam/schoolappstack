package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "t_dl_members")
@NamedQuery(name = "DigitalLibraryMember.findAll", query = "SELECT m FROM DigitalLibraryMember m")
public class DigitalLibraryMember implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_dl_membership_id")
	private Long membershipId;

	@Column(name = "academic_qualification")
	private String academicQualification;

	@Column(name = "behance_profile")
	private String behanceProfile;

	private String company;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	private Date dateofbirth;

	private String designation;

	@Column(name = "email_id")
	private String emailId;

	@Column(name = "facebook_profile")
	private String facebookProfile;

	private String firstname;

	@ManyToOne
	@JoinColumn(name = "fk_country_id")
	private Country country;

	@ManyToOne
	@JoinColumn(name = "fk_dl_fundedby_catdet_id")
	private GeneralDetail fundedbyCat;

	@ManyToOne
	@JoinColumn(name = "fk_emp_id")
	private EmployeeDetail employeeDetail;

	@ManyToOne
	@JoinColumn(name = "fk_gender_catdet_id")
	private GeneralDetail genderCat;

	@ManyToOne
	@JoinColumn(name = "fk_objectiveoftraining_catdet_id")
	private GeneralDetail objectiveoftrainingCat;

	@ManyToOne
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	@ManyToOne
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	@ManyToOne
	@JoinColumn(name = "fk_user_id")
	private User user;

	@Temporal(TemporalType.DATE)
	@Column(name = "from_date")
	private Date fromDate;

	@Column(name = "funtion_or_dept")
	private String funtionOrDept;

	@Column(name = "github_profile")
	private String githubProfile;

	private String industry;

	@Column(name = "instagram_profile")
	private String instagramProfile;

	private String institutename;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_currently_studying")
	private Boolean isCurrentlyStudying;

	@Column(name = "is_currently_working")
	private Boolean isCurrentlyWorking;

	@Column(name = "is_learner")
	private Boolean isLearner;

	@Column(name = "is_peer")
	private Boolean isPeer;

	private String lastname;

	@Column(name = "linkedin_profile")
	private String linkedinProfile;

	private String location;

	private String middlename;

	private String mobile;

	@Column(name = "personal_website")
	private String personalWebsite;

	private String reason;

	private String specialization;

	@Temporal(TemporalType.DATE)
	@Column(name = "to_date")
	private Date toDate;

	@Column(name = "twitter_profile")
	private String twitterProfile;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to CourseDiscussion

	@OneToMany(mappedBy = "member", cascade = CascadeType.ALL)
	private List<CourseDiscussion> courseDiscussions;

	// bi-directional many-to-one association to CourseMemberTopicLog

	@OneToMany(mappedBy = "member", cascade = CascadeType.ALL)
	private List<CourseMemberTopicLog> courseMemberTopicLogs;

	// bi-directional many-to-one association to CourseMember

	@OneToMany(mappedBy = "member", cascade = CascadeType.ALL)
	private List<CourseMember> courseMembers;

	// bi-directional many-to-one association to CourseReview

	@OneToMany(mappedBy = "member", cascade = CascadeType.ALL)
	private List<CourseReview> courseReviews;

	// bi-directional many-to-one association to OnlineCourses

	@OneToMany(mappedBy = "member", cascade = CascadeType.ALL)
	private List<OnlineCourses> onlineCourses;

	// bi-directional many-to-one association to TDlMemberLearningNote

	@OneToMany(mappedBy = "member", cascade = CascadeType.ALL)
	private List<MemberLearningNote> memberLearningNotes;

	public DigitalLibraryMember() {
	}

	public Long getMembershipId() {
		return membershipId;
	}

	public void setMembershipId(Long membershipId) {
		this.membershipId = membershipId;
	}

	public String getAcademicQualification() {
		return academicQualification;
	}

	public void setAcademicQualification(String academicQualification) {
		this.academicQualification = academicQualification;
	}

	public String getBehanceProfile() {
		return behanceProfile;
	}

	public void setBehanceProfile(String behanceProfile) {
		this.behanceProfile = behanceProfile;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getDateofbirth() {
		return dateofbirth;
	}

	public void setDateofbirth(Date dateofbirth) {
		this.dateofbirth = dateofbirth;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFacebookProfile() {
		return facebookProfile;
	}

	public void setFacebookProfile(String facebookProfile) {
		this.facebookProfile = facebookProfile;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public GeneralDetail getFundedbyCat() {
		return fundedbyCat;
	}

	public void setFundedbyCat(GeneralDetail fundedbyCat) {
		this.fundedbyCat = fundedbyCat;
	}

	public EmployeeDetail getEmployeeDetail() {
		return employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public GeneralDetail getGenderCat() {
		return genderCat;
	}

	public void setGenderCat(GeneralDetail genderCat) {
		this.genderCat = genderCat;
	}

	public GeneralDetail getObjectiveoftrainingCat() {
		return objectiveoftrainingCat;
	}

	public void setObjectiveoftrainingCat(GeneralDetail objectiveoftrainingCat) {
		this.objectiveoftrainingCat = objectiveoftrainingCat;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public String getFuntionOrDept() {
		return funtionOrDept;
	}

	public void setFuntionOrDept(String funtionOrDept) {
		this.funtionOrDept = funtionOrDept;
	}

	public String getGithubProfile() {
		return githubProfile;
	}

	public void setGithubProfile(String githubProfile) {
		this.githubProfile = githubProfile;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getInstagramProfile() {
		return instagramProfile;
	}

	public void setInstagramProfile(String instagramProfile) {
		this.instagramProfile = instagramProfile;
	}

	public String getInstitutename() {
		return institutename;
	}

	public void setInstitutename(String institutename) {
		this.institutename = institutename;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsCurrentlyStudying() {
		return isCurrentlyStudying;
	}

	public void setIsCurrentlyStudying(Boolean isCurrentlyStudying) {
		this.isCurrentlyStudying = isCurrentlyStudying;
	}

	public Boolean getIsCurrentlyWorking() {
		return isCurrentlyWorking;
	}

	public void setIsCurrentlyWorking(Boolean isCurrentlyWorking) {
		this.isCurrentlyWorking = isCurrentlyWorking;
	}

	public Boolean getIsLearner() {
		return isLearner;
	}

	public void setIsLearner(Boolean isLearner) {
		this.isLearner = isLearner;
	}

	public Boolean getIsPeer() {
		return isPeer;
	}

	public void setIsPeer(Boolean isPeer) {
		this.isPeer = isPeer;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getLinkedinProfile() {
		return linkedinProfile;
	}

	public void setLinkedinProfile(String linkedinProfile) {
		this.linkedinProfile = linkedinProfile;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPersonalWebsite() {
		return personalWebsite;
	}

	public void setPersonalWebsite(String personalWebsite) {
		this.personalWebsite = personalWebsite;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSpecialization() {
		return specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getTwitterProfile() {
		return twitterProfile;
	}

	public void setTwitterProfile(String twitterProfile) {
		this.twitterProfile = twitterProfile;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<CourseDiscussion> getCourseDiscussions() {
		return courseDiscussions;
	}

	public void setCourseDiscussions(List<CourseDiscussion> courseDiscussions) {
		this.courseDiscussions = courseDiscussions;
	}

	public List<CourseMemberTopicLog> getCourseMemberTopicLogs() {
		return courseMemberTopicLogs;
	}

	public void setCourseMemberTopicLogs(List<CourseMemberTopicLog> courseMemberTopicLogs) {
		this.courseMemberTopicLogs = courseMemberTopicLogs;
	}

	public List<CourseMember> getCourseMembers() {
		return courseMembers;
	}

	public void setCourseMembers(List<CourseMember> courseMembers) {
		this.courseMembers = courseMembers;
	}

	public List<CourseReview> getCourseReviews() {
		return courseReviews;
	}

	public void setCourseReviews(List<CourseReview> courseReviews) {
		this.courseReviews = courseReviews;
	}

	public List<OnlineCourses> getOnlineCourses() {
		return onlineCourses;
	}

	public void setOnlineCourses(List<OnlineCourses> onlineCourses) {
		this.onlineCourses = onlineCourses;
	}

	public List<MemberLearningNote> getMemberLearningNotes() {
		return memberLearningNotes;
	}

	public void setMemberLearningNotes(List<MemberLearningNote> memberLearningNotes) {
		this.memberLearningNotes = memberLearningNotes;
	}

}
