package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_cm_std_counselor_activity database table.
 * 
 */
@Entity
@Table(name="t_cm_std_counselor_activity")
@NamedQuery(name="CounselorActivity.findAll", query="SELECT c FROM CounselorActivity c")
public class CounselorActivity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_std_counselor_activity_id")
	private Long counselorActivityId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="activity_date")
	private Date activityDate;

	@Column(name="attendees_name")
	private String attendeesName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="discussion_points")
	private String discussionPoints;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="next_schedule_purpose")
	private String nextSchedulePurpose;

	@Temporal(TemporalType.DATE)
	@Column(name="next_scheduled_activity_date")
	private Date nextScheduledActivityDate;

	@Column(name="output_from_meeting")
	private String outputFromMeeting;

	private String purpose;

	private String reason;

	private String relationship;

	private String summary;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to CounselorMapping
	@ManyToOne
	@JoinColumn(name="fk_std_counselor_id")
	private CounselorMapping counselorMapping;

	//bi-directional many-to-one association to School
	@ManyToOne
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to CounselorActivityType
	@ManyToOne
	@JoinColumn(name="fk_counselor_activity_type_id")
	private CounselorActivityType counselorActivityType;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne
	@JoinColumn(name="fk_activity_status_catdet_id")
	private GeneralDetail activityStatus;

	public CounselorActivity() {
	}

	public Long getCounselorActivityId() {
		return this.counselorActivityId;
	}

	public void setCounselorActivityId(Long counselorActivityId) {
		this.counselorActivityId = counselorActivityId;
	}

	public Date getActivityDate() {
		return this.activityDate;
	}

	public void setActivityDate(Date activityDate) {
		this.activityDate = activityDate;
	}

	public String getAttendeesName() {
		return this.attendeesName;
	}

	public void setAttendeesName(String attendeesName) {
		this.attendeesName = attendeesName;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDiscussionPoints() {
		return this.discussionPoints;
	}

	public void setDiscussionPoints(String discussionPoints) {
		this.discussionPoints = discussionPoints;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getNextSchedulePurpose() {
		return this.nextSchedulePurpose;
	}

	public void setNextSchedulePurpose(String nextSchedulePurpose) {
		this.nextSchedulePurpose = nextSchedulePurpose;
	}

	public Date getNextScheduledActivityDate() {
		return this.nextScheduledActivityDate;
	}

	public void setNextScheduledActivityDate(Date nextScheduledActivityDate) {
		this.nextScheduledActivityDate = nextScheduledActivityDate;
	}

	public String getOutputFromMeeting() {
		return this.outputFromMeeting;
	}

	public void setOutputFromMeeting(String outputFromMeeting) {
		this.outputFromMeeting = outputFromMeeting;
	}

	public String getPurpose() {
		return this.purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRelationship() {
		return this.relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public String getSummary() {
		return this.summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public CounselorMapping getCounselorMapping() {
		return this.counselorMapping;
	}

	public void setCounselorMapping(CounselorMapping counselorMapping) {
		this.counselorMapping = counselorMapping;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public CounselorActivityType getCounselorActivityType() {
		return this.counselorActivityType;
	}

	public void setCounselorActivityType(CounselorActivityType counselorActivityType) {
		this.counselorActivityType = counselorActivityType;
	}

	public GeneralDetail getActivityStatus() {
		return this.activityStatus;
	}

	public void setActivityStatus(GeneralDetail activityStatus) {
		this.activityStatus = activityStatus;
	}

}