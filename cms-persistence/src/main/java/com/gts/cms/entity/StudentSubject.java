package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_std_student_subjects database table.
 * 
 */
@Entity
@Table(name="t_std_student_subjects")
@NamedQuery(name="StudentSubject.findAll", query="SELECT s FROM StudentSubject s")
public class StudentSubject implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_student_subject_id")
	private Long studentSubjectId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to Course
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_id")
	private Course course;

	//bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_year_id")
	private CourseYear courseYear;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subjecttype_catdet_id")
	private GeneralDetail subjectType;

	/*
	 * //bi-directional many-to-one association to GroupSection
	 * 
	 * @ManyToOne(fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name="fk_group_section_id") private GroupSection groupSection;
	 */


	/*
	 * //bi-directional many-to-one association to Studentbatch
	 * 
	 * @ManyToOne(fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name="fk_stdbatch_id") private Studentbatch studentbatch;
	 */

	//bi-directional many-to-one association to Subject
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subject_id")
	private Subject subject;

	//bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_student_id")
	private StudentDetail studentDetail;

	public StudentSubject() {
	}

	public Long getStudentSubjectId() {
		return this.studentSubjectId;
	}

	public void setStudentSubjectId(Long studentSubjectId) {
		this.studentSubjectId = studentSubjectId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Course getCourse() {
		return this.course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public CourseYear getCourseYear() {
		return this.courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public GeneralDetail getSubjectType() {
		return this.subjectType;
	}

	public void setSubjectType(GeneralDetail subjectType) {
		this.subjectType = subjectType;
	}



	/*
	 * public Studentbatch getStudentbatch() { return this.studentbatch; }
	 * 
	 * public void setStudentbatch(Studentbatch studentbatch) { this.studentbatch =
	 * studentbatch; }
	 */

	public Subject getSubject() {
		return this.subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

}