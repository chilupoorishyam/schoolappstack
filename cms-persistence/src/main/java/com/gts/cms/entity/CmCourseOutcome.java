package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_cm_course_outcomes database
 * table.
 */
@Data
@Entity
@Table(name = "t_cm_course_outcomes")
@NamedQuery(name = "CMCourseOutcome.findAll", query = "SELECT e FROM CmCourseOutcome e")
public class CmCourseOutcome implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_course_outcome_id")
    private Long courseOutcomeId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    private String code;

    private String description;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "inserted_dt")
    private Date insertedDt;

    @Column(name = "inserted_user")
    private Long insertedUser;

    // bi-directional many-to-one association to TMSchool
    @ManyToOne
    @JoinColumn(name = "fk_school_id")
    private School school;

    @ManyToOne
    @JoinColumn(name = "fk_academic_year_id")
    private AcademicYear academicYear;

    // bi-directional many-to-one association to TMCourseYear
    @ManyToOne
    @JoinColumn(name = "fk_course_year_id")
    private CourseYear courseYear;



    // bi-directional many-to-one association to TMSubject
    @ManyToOne
    @JoinColumn(name = "fk_subject_id")
    private Subject subject;

    // bi-directional many-to-one association to TMSubject
    @ManyToOne
    @JoinColumn(name = "fk_emp_id")
    private EmployeeDetail employeeDetail;

    // bi-directional many-to-one association to TMSubject
    @ManyToOne
    @JoinColumn(name = "fk_courseoutcome_catdet_id")
    private GeneralDetail courseoutcomeCatdetId;

    @ManyToOne
    @JoinColumn(name = "fk_taxonomy_level_catdet_id")
    private GeneralDetail taxonomyLevelCatdetId;

}