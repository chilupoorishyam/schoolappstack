package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_exam_std_courseyr_subjects database table.
 * 
 */
@Entity
@Table(name = "t_exam_std_courseyr_subjects")
@NamedQuery(name = "ExamStdCourseyrSubject.findAll", query = "SELECT e FROM ExamStdCourseyrSubject e")
public class ExamStdCourseyrSubject implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_std_couryear_sub_id")
	private Long examStdCouryearSubId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "credits")
	private Integer credits;
	
	
	@Column(name = "grade")
	private String grade;
	

	@Column(name = "grade_points")
	private BigDecimal gradePoints;

	@Column(name = "external_marks")
	private Integer externalMarks;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	/*// bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_year_id")
	private CourseYear courseYear;
*/
	// bi-directional many-to-one association to
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_examresult_catdet_id")
	private GeneralDetail examresultCat;

	/*// bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;
	*/
	
	// bi-directional many-to-one association to Subject
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_subject_id")
	private Subject subject;

	@Column(name = "internal_marks")
	private Integer internalMarks;

	@Column(name = "is_active")
	private Boolean isActive;
	
	@Column(name = "is_pass")
	private Boolean isPass;
	
	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	/*// bi-directional many-to-one association to ExamMaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_id")
	private ExamMaster examMaster;
	*/
	
	@Column(name = "fk_exam_ids")
	private String examIds;
	
	// bi-directional many-to-one association to ExamMaster
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_exam_std_couryear_id")
	private ExamStudentCourseYear examStudentCourseYear;
	
	public ExamStdCourseyrSubject() {
	}

	public Long getExamStdCouryearSubId() {
		return this.examStdCouryearSubId;
	}

	public void setExamStdCouryearSubId(Long examStdCouryearSubId) {
		this.examStdCouryearSubId = examStdCouryearSubId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Integer getCredits() {
		return credits;
	}

	public void setCredits(Integer credits) {
		this.credits = credits;
	}


	public Integer getExternalMarks() {
		return this.externalMarks;
	}

	public void setExternalMarks(Integer externalMarks) {
		this.externalMarks = externalMarks;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	/*public CourseYear getCourseYear() {
		return courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}
*/
	public GeneralDetail getExamresultCat() {
		return examresultCat;
	}

	public void setExamresultCat(GeneralDetail examresultCat) {
		this.examresultCat = examresultCat;
	}

	/*public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}
*/
	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Integer getInternalMarks() {
		return internalMarks;
	}

	public void setInternalMarks(Integer internalMarks) {
		this.internalMarks = internalMarks;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	/*public ExamMaster getExamMaster() {
		return examMaster;
	}

	public void setExamMaster(ExamMaster examMaster) {
		this.examMaster = examMaster;
	}*/


	public String getExamIds() {
		return examIds;
	}

	public Boolean getIsPass() {
		return isPass;
	}

	public void setIsPass(Boolean isPass) {
		this.isPass = isPass;
	}

	public void setExamIds(String examIds) {
		this.examIds = examIds;
	}

	public ExamStudentCourseYear getExamStudentCourseYear() {
		return examStudentCourseYear;
	}

	public void setExamStudentCourseYear(ExamStudentCourseYear examStudentCourseYear) {
		this.examStudentCourseYear = examStudentCourseYear;
	}
	
	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public BigDecimal getGradePoints() {
		return gradePoints;
	}

	public void setGradePoints(BigDecimal gradePoints) {
		this.gradePoints = gradePoints;
	}

}