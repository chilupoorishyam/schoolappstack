package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_emp_employee_bank_details database table.
 * 
 */
@Entity
@Table(name="t_emp_employee_bank_details")
@NamedQuery(name="EmployeeBankDetail.findAll", query="SELECT e FROM EmployeeBankDetail e")
public class EmployeeBankDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_emp_bank_detail_id")
	private Long employeeBankDetailId;

	@Column(name="account_number")
	private String accountNumber;

	@Column(name="bank_address")
	private String bankAddress;

	@Column(name="bank_name")
	private String bankName;

	@Column(name="branch_name")
	private String branchName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="dd_payable_address")
	private String ddPayableAddress;

	@Column(name="ifsc_code")
	private String ifscCode;

	@Column(name="is_active")
	private Boolean isActive;

	private String phone;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_id")
	private EmployeeDetail employeeDetail;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to EmployeePayslipGeneration
	@OneToMany(mappedBy="employeeBankDetail",fetch = FetchType.LAZY)
	private List<EmployeePayslipGeneration> employeePayslipGenerations;

	public EmployeeBankDetail() {
	}

	public Long getEmployeeBankDetailId() {
		return this.employeeBankDetailId;
	}

	public void setEmployeeBankDetailId(Long employeeBankDetailId) {
		this.employeeBankDetailId = employeeBankDetailId;
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getBankAddress() {
		return this.bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDdPayableAddress() {
		return this.ddPayableAddress;
	}

	public void setDdPayableAddress(String ddPayableAddress) {
		this.ddPayableAddress = ddPayableAddress;
	}

	public String getIfscCode() {
		return this.ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getEmployeeDetail() {
		return this.employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public List<EmployeePayslipGeneration> getEmployeePayslipGenerations() {
		return this.employeePayslipGenerations;
	}

	public void setEmployeePayslipGenerations(List<EmployeePayslipGeneration> employeePayslipGenerations) {
		this.employeePayslipGenerations = employeePayslipGenerations;
	}

	public EmployeePayslipGeneration addEmployeePayslipGeneration(EmployeePayslipGeneration employeePayslipGeneration) {
		getEmployeePayslipGenerations().add(employeePayslipGeneration);
		employeePayslipGeneration.setEmployeeBankDetail(this);

		return employeePayslipGeneration;
	}

	public EmployeePayslipGeneration removeEmployeePayslipGeneration(EmployeePayslipGeneration employeePayslipGeneration) {
		getEmployeePayslipGenerations().remove(employeePayslipGeneration);
		employeePayslipGeneration.setEmployeeBankDetail(null);

		return employeePayslipGeneration;
	}

}