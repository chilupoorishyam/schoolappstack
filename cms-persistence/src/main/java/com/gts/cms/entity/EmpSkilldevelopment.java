package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_emp_skilldevelopment database table.
 */
@Data
@Entity
@Table(name = "t_emp_skilldevelopment")
@NamedQuery(name = "EmpSkilldevelopment.findAll", query = "SELECT c FROM EmpSkilldevelopment c")
public class EmpSkilldevelopment implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_emp_skilldevelopment_id")
    private Long empskilldevelopmentId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "title")
    private String title;

    @Column(name = "title_of_conference")
    private String titleOfConference;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "from_date")
    private Date fromDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "to_date")
    private Date toDate;

    @Column(name = "location")
    private Integer location;

    @Column(name = "organized_by")
    private Boolean organizedBy;

    @Column(name = "is_online")
    private Boolean isOnline;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "reviwed_on")
    private Date reviwedOn;

    @ManyToOne
    @JoinColumn(name = "fk_emp_id")
    private EmployeeDetail employeeDetail;

    @ManyToOne
    @JoinColumn(name = "fk_skilldevelopment_catdet_id")
    private GeneralDetail skilldevelopmentCatdetId;

    @ManyToOne
    @JoinColumn(name = "fk_skilldevelopment_level_catdet_id")
    private GeneralDetail skilldevelopmentLevelCatdetId;

    @ManyToOne
    @JoinColumn(name = "fk_reviwed_emp_id")
    private EmployeeDetail reviwedEmp;

    @ManyToOne
    @JoinColumn(name = "fk_approval_status_catdet_id")
    private GeneralDetail approvalStatusCatdet;

    @ManyToOne
    @JoinColumn(name = "fk_contribution_status_catdet_id")
    private GeneralDetail contributionStatusCatdet;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;
}