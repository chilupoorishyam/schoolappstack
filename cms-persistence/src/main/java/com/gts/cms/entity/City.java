package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_cities database table.
 * 
 */
@Entity
@Table(name="t_m_cities")
@NamedQuery(name="City.findAll", query="SELECT c FROM City c")
public class City implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_city_id")
	private Long cityId;

	@Column(name="city_code")
	private String cityCode;

	@Column(name="city_name")
	private String cityName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="presentCity",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails1;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="permanentCity",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails2;

	//bi-directional many-to-one association to District
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_district_id")
	private District district;

	//bi-directional many-to-one association to School
	@OneToMany(mappedBy="city",fetch = FetchType.LAZY)
	private List<School> schools;

	//bi-directional many-to-one association to Organization
	@OneToMany(mappedBy="city",fetch = FetchType.LAZY)
	private List<Organization> organizations;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="presentCity",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications1;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="permenentCity",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications2;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="presentCity",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails1;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="premenentCity",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails2;

	public City() {
	}

	public Long getCityId() {
		return this.cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public String getCityCode() {
		return this.cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return this.cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<EmployeeDetail> getEmpDetails1() {
		return this.empDetails1;
	}

	public void setEmpDetails1(List<EmployeeDetail> empDetails1) {
		this.empDetails1 = empDetails1;
	}

	public EmployeeDetail addEmpDetails1(EmployeeDetail empDetails1) {
		getEmpDetails1().add(empDetails1);
		empDetails1.setPresentCity(this);

		return empDetails1;
	}

	public EmployeeDetail removeEmpDetails1(EmployeeDetail empDetails1) {
		getEmpDetails1().remove(empDetails1);
		empDetails1.setPresentCity(null);

		return empDetails1;
	}

	public List<EmployeeDetail> getEmpDetails2() {
		return this.empDetails2;
	}

	public void setEmpDetails2(List<EmployeeDetail> empDetails2) {
		this.empDetails2 = empDetails2;
	}

	public EmployeeDetail addEmpDetails2(EmployeeDetail empDetails2) {
		getEmpDetails2().add(empDetails2);
		empDetails2.setPermanentCity(this);

		return empDetails2;
	}

	public EmployeeDetail removeEmpDetails2(EmployeeDetail empDetails2) {
		getEmpDetails2().remove(empDetails2);
		empDetails2.setPermanentCity(null);

		return empDetails2;
	}

	public District getDistrict() {
		return this.district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public List<School> getSchools() {
		return this.schools;
	}

	public void setSchools(List<School> schools) {
		this.schools = schools;
	}

	public School addSchool(School school) {
		getSchools().add(school);
		school.setCity(this);

		return school;
	}

	public School removeSchool(School school) {
		getSchools().remove(school);
		school.setCity(null);

		return school;
	}

	public List<Organization> getOrganizations() {
		return this.organizations;
	}

	public void setOrganizations(List<Organization> organizations) {
		this.organizations = organizations;
	}

	public Organization addOrganization(Organization organization) {
		getOrganizations().add(organization);
		organization.setCity(this);

		return organization;
	}

	public Organization removeOrganization(Organization organization) {
		getOrganizations().remove(organization);
		organization.setCity(null);

		return organization;
	}

	public List<StudentApplication> getStdApplications1() {
		return this.stdApplications1;
	}

	public void setStdApplications1(List<StudentApplication> stdApplications1) {
		this.stdApplications1 = stdApplications1;
	}

	public StudentApplication addStdApplications1(StudentApplication stdApplications1) {
		getStdApplications1().add(stdApplications1);
		stdApplications1.setPresentCity(this);

		return stdApplications1;
	}

	public StudentApplication removeStdApplications1(StudentApplication stdApplications1) {
		getStdApplications1().remove(stdApplications1);
		stdApplications1.setPresentCity(null);

		return stdApplications1;
	}

	public List<StudentApplication> getStdApplications2() {
		return this.stdApplications2;
	}

	public void setStdApplications2(List<StudentApplication> stdApplications2) {
		this.stdApplications2 = stdApplications2;
	}

	public StudentApplication addStdApplications2(StudentApplication stdApplications2) {
		getStdApplications2().add(stdApplications2);
		stdApplications2.setPermenentCity(this);

		return stdApplications2;
	}

	public StudentApplication removeStdApplications2(StudentApplication stdApplications2) {
		getStdApplications2().remove(stdApplications2);
		stdApplications2.setPermenentCity(null);

		return stdApplications2;
	}

	public List<StudentDetail> getStdStudentDetails1() {
		return this.stdStudentDetails1;
	}

	public void setStdStudentDetails1(List<StudentDetail> stdStudentDetails1) {
		this.stdStudentDetails1 = stdStudentDetails1;
	}

	public StudentDetail addStdStudentDetails1(StudentDetail stdStudentDetails1) {
		getStdStudentDetails1().add(stdStudentDetails1);
		stdStudentDetails1.setPresentCity(this);

		return stdStudentDetails1;
	}

	public StudentDetail removeStdStudentDetails1(StudentDetail stdStudentDetails1) {
		getStdStudentDetails1().remove(stdStudentDetails1);
		stdStudentDetails1.setPresentCity(null);

		return stdStudentDetails1;
	}

	public List<StudentDetail> getStdStudentDetails2() {
		return this.stdStudentDetails2;
	}

	public void setStdStudentDetails2(List<StudentDetail> stdStudentDetails2) {
		this.stdStudentDetails2 = stdStudentDetails2;
	}

	public StudentDetail addStdStudentDetails2(StudentDetail stdStudentDetails2) {
		getStdStudentDetails2().add(stdStudentDetails2);
		stdStudentDetails2.setPremenentCity(this);

		return stdStudentDetails2;
	}

	public StudentDetail removeStdStudentDetails2(StudentDetail stdStudentDetails2) {
		getStdStudentDetails2().remove(stdStudentDetails2);
		stdStudentDetails2.setPremenentCity(null);

		return stdStudentDetails2;
	}

}