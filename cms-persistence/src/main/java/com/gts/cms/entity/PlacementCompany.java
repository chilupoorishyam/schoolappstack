package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_pa_placement_companies database table.
 * 
 */
@Entity
@Table(name = "t_pa_placement_companies")
@NamedQuery(name = "PlacementCompany.findAll", query = "SELECT p FROM PlacementCompany p")
public class PlacementCompany implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_placement_company_id")
	private Long placementCompanyId;

	

	@Column(name = "company_name")
	private String companyName;

	@Column(name = "contact_details")
	private String contactDetails;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;
	
	@Column(name = "company_requirements")
	private String comapanyRequirements;
	
	@Column(name = "ssc_grade")
	private Integer sscGrade;
	
	@Column (name = "ssc_percentage")
	private Integer sscPercentage;
	
	@Column (name = "inter_grade")
	private Integer interGrade;
	
	@Column ( name = "inter_percentage")
	private Integer interPercentage;
	
	@Column (name = "diploma_grade")
	private Integer diplomaGrade;
	
	@Column(name= "diploma_percentage")
	private Integer diplomaPercentage;
	
	@Column(name = "ug_grade")
	private Integer ugGrade;
	
	@Column (name="ug_percentage")
	private Integer ugPercentage;
	
	@Column(name= "pg_grade")
	private Integer pgGrade;
	
	@Column(name = "pg_percentage")
	private Integer pgPercentage;
	
	@Column(name = "fk_skillset_ids")
	private String skillSetIds;
	
	@Column(name = "is_backlogs_allowed")
	private Boolean isBackLogAllowed;

	@Column(name = "fk_attended_student_ids")
	private String attendedStudentIds;

	@Column(name = "fk_placed_student_ids")
	private String placedStudentIds;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "no_of_students_attended")
	private Integer noOfStudentsAttended;

	@Column(name = "no_of_students_placed")
	private Integer noOfStudentsPlaced;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to Placement
	@ManyToOne
	@JoinColumn(name = "fk_pa_placement_id")
	private Placement placement;

	// bi-directional many-to-one association to Company
	@ManyToOne
	@JoinColumn(name = "fk_company_id")
	private Company company;

	// bi-directional many-to-one association to companyContact
	@ManyToOne
	@JoinColumn(name = "fk_company_contact_id")
	private CompanyContact companyContact;

	public PlacementCompany() {
	}

	public Long getPlacementCompanyId() {
		return this.placementCompanyId;
	}

	public void setPlacementCompanyId(Long placementCompanyId) {
		this.placementCompanyId = placementCompanyId;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getContactDetails() {
		return this.contactDetails;
	}

	public void setContactDetails(String contactDetails) {
		this.contactDetails = contactDetails;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getAttendedStudentIds() {
		return this.attendedStudentIds;
	}

	public void setAttendedStudentIds(String attendedStudentIds) {
		this.attendedStudentIds = attendedStudentIds;
	}

	public String getPlacedStudentIds() {
		return this.placedStudentIds;
	}

	public void setPlacedStudentIds(String placedStudentIds) {
		this.placedStudentIds = placedStudentIds;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getNoOfStudentsAttended() {
		return this.noOfStudentsAttended;
	}

	public void setNoOfStudentsAttended(Integer noOfStudentsAttended) {
		this.noOfStudentsAttended = noOfStudentsAttended;
	}

	public Integer getNoOfStudentsPlaced() {
		return this.noOfStudentsPlaced;
	}

	public void setNoOfStudentsPlaced(Integer noOfStudentsPlaced) {
		this.noOfStudentsPlaced = noOfStudentsPlaced;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Placement getPlacement() {
		return placement;
	}

	public void setPlacement(Placement placement) {
		this.placement = placement;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public CompanyContact getCompanyContact() {
		return companyContact;
	}

	public void setCompanyContact(CompanyContact companyContact) {
		this.companyContact = companyContact;
	}
	
	public String getComapanyRequirements() {
		return comapanyRequirements;
	}

	public void setComapanyRequirements(String comapanyRequirements) {
		this.comapanyRequirements = comapanyRequirements;
	}

	public Integer getSscGrade() {
		return sscGrade;
	}

	public void setSscGrade(Integer sscGrade) {
		this.sscGrade = sscGrade;
	}

	public Integer getSscPercentage() {
		return sscPercentage;
	}

	public void setSscPercentage(Integer sscPercentage) {
		this.sscPercentage = sscPercentage;
	}

	public Integer getInterGrade() {
		return interGrade;
	}

	public void setInterGrade(Integer interGrade) {
		this.interGrade = interGrade;
	}

	public Integer getInterPercentage() {
		return interPercentage;
	}

	public void setInterPercentage(Integer interPercentage) {
		this.interPercentage = interPercentage;
	}

	public Integer getDiplomaGrade() {
		return diplomaGrade;
	}

	public void setDiplomaGrade(Integer diplomaGrade) {
		this.diplomaGrade = diplomaGrade;
	}

	public Integer getDiplomaPercentage() {
		return diplomaPercentage;
	}

	public void setDiplomaPercentage(Integer diplomaPercentage) {
		this.diplomaPercentage = diplomaPercentage;
	}

	public Integer getUgGrade() {
		return ugGrade;
	}

	public void setUgGrade(Integer ugGrade) {
		this.ugGrade = ugGrade;
	}

	public Integer getUgPercentage() {
		return ugPercentage;
	}

	public void setUgPercentage(Integer ugPercentage) {
		this.ugPercentage = ugPercentage;
	}

	public Integer getPgGrade() {
		return pgGrade;
	}

	public void setPgGrade(Integer pgGrade) {
		this.pgGrade = pgGrade;
	}

	public Integer getPgPercentage() {
		return pgPercentage;
	}

	public void setPgPercentage(Integer pgPercentage) {
		this.pgPercentage = pgPercentage;
	}

	public String getSkillSetIds() {
		return skillSetIds;
	}

	public void setSkillSetIds(String skillSetIds) {
		this.skillSetIds = skillSetIds;
	}

	public Boolean getIsBackLogAllowed() {
		return isBackLogAllowed;
	}

	public void setIsBackLogAllowed(Boolean isBackLogAllowed) {
		this.isBackLogAllowed = isBackLogAllowed;
	}

}