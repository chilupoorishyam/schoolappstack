package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_cm_subject_books database table.
 * 
 */
@Entity
@Table(name="t_cm_subject_books")
@NamedQuery(name="SubjectBook.findAll", query="SELECT s FROM SubjectBook s")
public class SubjectBook implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_sub_book_id")
	private Long subBookId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_onlinecourse")
	private Boolean isOnlinecourse;

	@Column(name="is_reference")
	private Boolean isReference;

	@Column(name="is_textbook")
	private Boolean isTextbook;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to Subjectregulation
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subreg_id")
	private Subjectregulation subjectregulation;

	//bi-directional many-to-one association to Book
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_lib_book_id")
	private Book book;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to SubjectUnitTopic
	@OneToMany(mappedBy="subjectBook",fetch = FetchType.LAZY)
	private List<SubjectUnitTopic> subjectUnitTopics;

	public SubjectBook() {
	}

	public Long getSubBookId() {
		return this.subBookId;
	}

	public void setSubBookId(Long subBookId) {
		this.subBookId = subBookId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsOnlinecourse() {
		return this.isOnlinecourse;
	}

	public void setIsOnlinecourse(Boolean isOnlinecourse) {
		this.isOnlinecourse = isOnlinecourse;
	}

	public Boolean getIsReference() {
		return this.isReference;
	}

	public void setIsReference(Boolean isReference) {
		this.isReference = isReference;
	}

	public Boolean getIsTextbook() {
		return this.isTextbook;
	}

	public void setIsTextbook(Boolean isTextbook) {
		this.isTextbook = isTextbook;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Subjectregulation getSubjectregulation() {
		return this.subjectregulation;
	}

	public void setSubjectregulation(Subjectregulation subjectregulation) {
		this.subjectregulation = subjectregulation;
	}

	public Book getBook() {
		return this.book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<SubjectUnitTopic> getSubjectUnitTopics() {
		return this.subjectUnitTopics;
	}

	public void setSubjectUnitTopics(List<SubjectUnitTopic> subjectUnitTopics) {
		this.subjectUnitTopics = subjectUnitTopics;
	}

	public SubjectUnitTopic addSubjectUnitTopic(SubjectUnitTopic subjectUnitTopic) {
		getSubjectUnitTopics().add(subjectUnitTopic);
		subjectUnitTopic.setSubjectBook(this);

		return subjectUnitTopic;
	}

	public SubjectUnitTopic removeSubjectUnitTopic(SubjectUnitTopic subjectUnitTopic) {
		getSubjectUnitTopics().remove(subjectUnitTopic);
		subjectUnitTopic.setSubjectBook(null);

		return subjectUnitTopic;
	}

}