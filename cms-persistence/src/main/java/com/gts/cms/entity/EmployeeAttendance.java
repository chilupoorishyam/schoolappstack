package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_emp_attendance database table.
 * 
 */
@Entity
@Table(name="t_emp_attendance")
@NamedQuery(name="EmployeeAttendance.findAll", query="SELECT e FROM EmployeeAttendance e")
public class EmployeeAttendance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_emp_attendance_id")
	private Long employeeAttendanceId;

	@Temporal(TemporalType.DATE)
	@Column(name="attendance_date")
	private Date attendanceDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_present")
	private Boolean isPresent;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_editable")
	private Boolean isEditable;

	@Column(name="is_freeze")
	private Boolean isFreeze;

	@Column(name="is_halfday")
	private Boolean isHalfday;

	@Column(name="is_manual")
	private Boolean isManual;

	@Column(name="is_verified")
	private Boolean isVerified;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="login_date_time")
	private Date loginDateTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="logout_date_time")
	private Date logoutDateTime;

	private String reason;
	
	@Column(name="early_by")
	private Integer earlyBy;
	
	@Column(name="is_missedinpunch")
	private Boolean isMissedInPunch;
	
	@Column(name="is_missedoutpunch")
	private Boolean isMissedOutPunch;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	@Column(name="verfied_reason")
	private String verfiedReason;
	
	@Column(name="comments")
	private String comments;
	
	@Column(name="punch_records")
	private String punchRecords;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_id")
	private EmployeeDetail employeeDetail;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_verifiedby_emp_id")
	private EmployeeDetail verifiedbyEmployeeDetail;

	//bi-directional many-to-one association to LeaveType
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_leavetype_id")
	private LeaveType leaveType;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	public EmployeeAttendance() {
	}

	public Long getEmployeeAttendanceId() {
		return this.employeeAttendanceId;
	}

	public void setEmployeeAttendanceId(Long employeeAttendanceId) {
		this.employeeAttendanceId = employeeAttendanceId;
	}

	public Date getAttendanceDate() {
		return this.attendanceDate;
	}

	public void setAttendanceDate(Date attendanceDate) {
		this.attendanceDate = attendanceDate;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsPresent() {
		return isPresent;
	}

	public void setIsPresent(Boolean isPresent) {
		this.isPresent = isPresent;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsEditable() {
		return this.isEditable;
	}

	public void setIsEditable(Boolean isEditable) {
		this.isEditable = isEditable;
	}

	public Boolean getIsFreeze() {
		return this.isFreeze;
	}

	public void setIsFreeze(Boolean isFreeze) {
		this.isFreeze = isFreeze;
	}

	public Boolean getIsHalfday() {
		return this.isHalfday;
	}

	public void setIsHalfday(Boolean isHalfday) {
		this.isHalfday = isHalfday;
	}

	public Boolean getIsManual() {
		return this.isManual;
	}

	public void setIsManual(Boolean isManual) {
		this.isManual = isManual;
	}

	public Boolean getIsVerified() {
		return this.isVerified;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	public Date getLoginDateTime() {
		return this.loginDateTime;
	}

	public void setLoginDateTime(Date loginDateTime) {
		this.loginDateTime = loginDateTime;
	}

	public Date getLogoutDateTime() {
		return this.logoutDateTime;
	}

	public void setLogoutDateTime(Date logoutDateTime) {
		this.logoutDateTime = logoutDateTime;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getVerfiedReason() {
		return this.verfiedReason;
	}

	public void setVerfiedReason(String verfiedReason) {
		this.verfiedReason = verfiedReason;
	}

	public EmployeeDetail getEmployeeDetail() {
		return this.employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public EmployeeDetail getVerifiedbyEmployeeDetail() {
		return this.verifiedbyEmployeeDetail;
	}

	public void setVerifiedbyEmployeeDetail(EmployeeDetail verifiedbyEmployeeDetail) {
		this.verifiedbyEmployeeDetail = verifiedbyEmployeeDetail;
	}

	public LeaveType getLeaveType() {
		return this.leaveType;
	}

	public void setLeaveType(LeaveType leaveType) {
		this.leaveType = leaveType;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getPunchRecords() {
		return punchRecords;
	}

	public void setPunchRecords(String punchRecords) {
		this.punchRecords = punchRecords;
	}

	public Integer getEarlyBy() {
		return earlyBy;
	}

	public void setEarlyBy(Integer earlyBy) {
		this.earlyBy = earlyBy;
	}

	public Boolean getIsMissedInPunch() {
		return isMissedInPunch;
	}

	public void setIsMissedInPunch(Boolean isMissedInPunch) {
		this.isMissedInPunch = isMissedInPunch;
	}

	public Boolean getIsMissedOutPunch() {
		return isMissedOutPunch;
	}

	public void setIsMissedOutPunch(Boolean isMissedOutPunch) {
		this.isMissedOutPunch = isMissedOutPunch;
	}
}