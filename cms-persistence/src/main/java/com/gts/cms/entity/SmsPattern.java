package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_sms_patterns database table.
 * 
 */
@Entity
@Table(name="t_sms_patterns")
@NamedQuery(name="SmsPattern.findAll", query="SELECT s FROM SmsPattern s")
public class SmsPattern implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_message_pattern_id")
	private Long messagePatternId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_email_alert")
	private Boolean isEmailAlert;

	@Column(name="is_sms_alert")
	private Boolean isSmsAlert;

	@Column(name="message_content")
	private String messageContent;

	@Lob
	@Column(name="message_content_html")
	private String messageContentHtml;

	@Column(name="message_signature")
	private String messageSignature;

	@Column(name="message_subject")
	private String messageSubject;

	private String messagepatternfor;

	private String reason;

	@Column(name="sender_code")
	private String senderCode;

	@Column(name="sms_text_format")
	private String smsTextFormat;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to Messaging
	@OneToMany(mappedBy="smsPattern")
	private List<Messaging> messagings;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	public SmsPattern() {
	}

	public Long getMessagePatternId() {
		return this.messagePatternId;
	}

	public void setMessagePatternId(Long messagePatternId) {
		this.messagePatternId = messagePatternId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsEmailAlert() {
		return this.isEmailAlert;
	}

	public void setIsEmailAlert(Boolean isEmailAlert) {
		this.isEmailAlert = isEmailAlert;
	}

	public Boolean getIsSmsAlert() {
		return this.isSmsAlert;
	}

	public void setIsSmsAlert(Boolean isSmsAlert) {
		this.isSmsAlert = isSmsAlert;
	}

	public String getMessageContent() {
		return this.messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public String getMessageContentHtml() {
		return this.messageContentHtml;
	}

	public void setMessageContentHtml(String messageContentHtml) {
		this.messageContentHtml = messageContentHtml;
	}

	public String getMessageSignature() {
		return this.messageSignature;
	}

	public void setMessageSignature(String messageSignature) {
		this.messageSignature = messageSignature;
	}

	public String getMessageSubject() {
		return this.messageSubject;
	}

	public void setMessageSubject(String messageSubject) {
		this.messageSubject = messageSubject;
	}

	public String getMessagepatternfor() {
		return this.messagepatternfor;
	}

	public void setMessagepatternfor(String messagepatternfor) {
		this.messagepatternfor = messagepatternfor;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSenderCode() {
		return this.senderCode;
	}

	public void setSenderCode(String senderCode) {
		this.senderCode = senderCode;
	}

	public String getSmsTextFormat() {
		return this.smsTextFormat;
	}

	public void setSmsTextFormat(String smsTextFormat) {
		this.smsTextFormat = smsTextFormat;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<Messaging> getMessagings() {
		return this.messagings;
	}

	public void setMessagings(List<Messaging> messagings) {
		this.messagings = messagings;
	}

	public Messaging addMessaging(Messaging messaging) {
		getMessagings().add(messaging);
		messaging.setSmsPattern(this);

		return messaging;
	}

	public Messaging removeMessaging(Messaging messaging) {
		getMessagings().remove(messaging);
		messaging.setSmsPattern(null);

		return messaging;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

}