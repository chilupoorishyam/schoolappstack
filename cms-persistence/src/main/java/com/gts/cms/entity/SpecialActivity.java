package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_tt_specialactivities database table.
 * 
 */
@Entity
@Table(name = "t_tt_specialactivities")
@NamedQuery(name = "SpecialActivity.findAll", query = "SELECT s FROM SpecialActivity s")
public class SpecialActivity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_spcl_activity_id")
	private Long spclActivityId;

	@Column(name = "closing_comments")
	private String closingComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "facilitator_company_name")
	private String facilitatorCompanyName;

	@Column(name = "facilitator_details")
	private String facilitatorDetails;

	@Column(name = "facilitator_names")
	private String facilitatorNames;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_facilitator_emp_id")
	private EmployeeDetail employeeDetail;

	// bi-directional many-to-one association to Company
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_facilitator_pa_company_id")
	private Company facilitatorPaCompany;

	// bi-directional many-to-one association to Room
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_room_id")
	private Room room;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_schedulestatus_catdet_id")
	private GeneralDetail schedulestatusCatdet;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_spclactity_catdet_id")
	private GeneralDetail spclactityCatdet;

	// bi-directional many-to-one association to Subject
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_subject_id")
	private Subject subject;

	@Temporal(TemporalType.DATE)
	@Column(name = "from_date")
	private Date fromDate;

	@Column(name = "from_time")
	private Time fromTime;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_registration_required")
	private Boolean isRegistrationRequired;

	private String reason;

	@Temporal(TemporalType.DATE)
	@Column(name = "registration_end_date")
	private Date registrationEndDate;

	@Column(name = "registration_fee")
	private BigDecimal registrationFee;

	@Temporal(TemporalType.DATE)
	@Column(name = "registration_start_date")
	private Date registrationStartDate;

	@Column(name = "special_activity_description")
	private String specialActivityDescription;

	@Column(name = "special_activity_name")
	private String specialActivityName;

	@Temporal(TemporalType.DATE)
	@Column(name = "to_date")
	private Date toDate;

	@Column(name = "to_time")
	private Time toTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to SpclActivityAttendance
	@OneToMany(mappedBy = "specialActivity",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<SpclActivityAttendance> spclActivityAttendances;

	// bi-directional many-to-one association to SpclActivityAttendee
	@OneToMany(mappedBy = "specialActivity",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<SpclActivityAttendee> spclActivityAttendees;

	public SpecialActivity() {
	}

	public Long getSpclActivityId() {
		return this.spclActivityId;
	}

	public void setSpclActivityId(Long spclActivityId) {
		this.spclActivityId = spclActivityId;
	}

	public String getClosingComments() {
		return this.closingComments;
	}

	public void setClosingComments(String closingComments) {
		this.closingComments = closingComments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getFacilitatorCompanyName() {
		return this.facilitatorCompanyName;
	}

	public void setFacilitatorCompanyName(String facilitatorCompanyName) {
		this.facilitatorCompanyName = facilitatorCompanyName;
	}

	public String getFacilitatorDetails() {
		return this.facilitatorDetails;
	}

	public void setFacilitatorDetails(String facilitatorDetails) {
		this.facilitatorDetails = facilitatorDetails;
	}

	public String getFacilitatorNames() {
		return this.facilitatorNames;
	}

	public void setFacilitatorNames(String facilitatorNames) {
		this.facilitatorNames = facilitatorNames;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public EmployeeDetail getEmployeeDetail() {
		return employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public Company getFacilitatorPaCompany() {
		return facilitatorPaCompany;
	}

	public void setFacilitatorPaCompany(Company facilitatorPaCompany) {
		this.facilitatorPaCompany = facilitatorPaCompany;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public GeneralDetail getSchedulestatusCatdet() {
		return schedulestatusCatdet;
	}

	public void setSchedulestatusCatdet(GeneralDetail schedulestatusCatdet) {
		this.schedulestatusCatdet = schedulestatusCatdet;
	}

	public GeneralDetail getSpclactityCatdet() {
		return spclactityCatdet;
	}

	public void setSpclactityCatdet(GeneralDetail spclactityCatdet) {
		this.spclactityCatdet = spclactityCatdet;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Time getFromTime() {
		return fromTime;
	}

	public void setFromTime(Time fromTime) {
		this.fromTime = fromTime;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsRegistrationRequired() {
		return isRegistrationRequired;
	}

	public void setIsRegistrationRequired(Boolean isRegistrationRequired) {
		this.isRegistrationRequired = isRegistrationRequired;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getRegistrationEndDate() {
		return registrationEndDate;
	}

	public void setRegistrationEndDate(Date registrationEndDate) {
		this.registrationEndDate = registrationEndDate;
	}

	public BigDecimal getRegistrationFee() {
		return registrationFee;
	}

	public void setRegistrationFee(BigDecimal registrationFee) {
		this.registrationFee = registrationFee;
	}

	public Date getRegistrationStartDate() {
		return registrationStartDate;
	}

	public void setRegistrationStartDate(Date registrationStartDate) {
		this.registrationStartDate = registrationStartDate;
	}

	public String getSpecialActivityDescription() {
		return specialActivityDescription;
	}

	public void setSpecialActivityDescription(String specialActivityDescription) {
		this.specialActivityDescription = specialActivityDescription;
	}

	public String getSpecialActivityName() {
		return specialActivityName;
	}

	public void setSpecialActivityName(String specialActivityName) {
		this.specialActivityName = specialActivityName;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Time getToTime() {
		return toTime;
	}

	public void setToTime(Time toTime) {
		this.toTime = toTime;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<SpclActivityAttendance> getSpclActivityAttendances() {
		return spclActivityAttendances;
	}

	public void setSpclActivityAttendances(List<SpclActivityAttendance> spclActivityAttendances) {
		this.spclActivityAttendances = spclActivityAttendances;
	}

	public List<SpclActivityAttendee> getSpclActivityAttendees() {
		return spclActivityAttendees;
	}

	public void setSpclActivityAttendees(List<SpclActivityAttendee> spclActivityAttendees) {
		this.spclActivityAttendees = spclActivityAttendees;
	}

}