package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_exam_online_question_options database table.
 */
@Data
@Entity
@Table(name = "t_exam_online_question_options")
@NamedQuery(name = "ExamOnlineQuestionOption.findAll", query = "SELECT c FROM ExamOnlineQuestionOption c")
public class ExamOnlineQuestionOption implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_exam_online_question_option_id")
    private Long examOnlineQuestionOptionId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "is_correct_answer")
    private Boolean isCorrectAnswer;

    private String options;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    // bi-directional many-to-one association to CourseQuestion
    @ManyToOne
    @JoinColumn(name = "fk_exam_online_question_id")
    private ExamOnlineQuestion examOnlineQuestion;
}
