package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;


/**
 * The persistent class for the t_emp_stg_dailylog database table.
 * 
 */
@Entity
@Table(name="t_emp_stg_dailylog")
@NamedQuery(name="EmployeeStgDailylog.findAll", query="SELECT e FROM EmployeeStgDailylog e")
public class EmployeeStgDailylog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_stg_dailylog_id")
	private Long stgDailylogId;

	@Column(name="biometric_code")
	private String biometricCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	@Column(name="dateofduty_raw")
	private Date dateofdutyRaw;

	@Column(name="event_date")
	private Integer eventDate;

	@Column(name="event_id")
	private Integer eventId;

	@Column(name="event_time")
	private Time eventTime;

	@Temporal(TemporalType.TIMESTAMP)
	private Date eventdate;

	@Column(name="fk_biometric_id")
	private Long biometricId;

	private String location;

	private String rfid;

	@Column(name="seq_no")
	private Integer seqNo;

	@Column(name="servicetag_id")
	private String servicetagId;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	public EmployeeStgDailylog() {
	}

	public Long getStgDailylogId() {
		return this.stgDailylogId;
	}

	public void setStgDailylogId(Long stgDailylogId) {
		this.stgDailylogId = stgDailylogId;
	}

	public String getBiometricCode() {
		return this.biometricCode;
	}

	public void setBiometricCode(String biometricCode) {
		this.biometricCode = biometricCode;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getDateofdutyRaw() {
		return this.dateofdutyRaw;
	}

	public void setDateofdutyRaw(Date dateofdutyRaw) {
		this.dateofdutyRaw = dateofdutyRaw;
	}

	public Integer getEventDate() {
		return this.eventDate;
	}

	public void setEventDate(Integer eventDate) {
		this.eventDate = eventDate;
	}

	public Integer getEventId() {
		return this.eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public Time getEventTime() {
		return this.eventTime;
	}

	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}

	public Date getEventdate() {
		return this.eventdate;
	}

	public void setEventdate(Date eventdate) {
		this.eventdate = eventdate;
	}

	public Long getBiometricId() {
		return this.biometricId;
	}

	public void setBiometricId(Long biometricId) {
		this.biometricId = biometricId;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getRfid() {
		return this.rfid;
	}

	public void setRfid(String rfid) {
		this.rfid = rfid;
	}

	public Integer getSeqNo() {
		return this.seqNo;
	}

	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}

	public String getServicetagId() {
		return this.servicetagId;
	}

	public void setServicetagId(String servicetagId) {
		this.servicetagId = servicetagId;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

}