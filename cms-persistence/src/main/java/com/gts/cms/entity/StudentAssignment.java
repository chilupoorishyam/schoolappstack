package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_cm_student_assignments database table.
 * 
 */
@Entity
@Table(name = "t_cm_student_assignments")
@NamedQuery(name = "StudentAssignment.findAll", query = "SELECT s FROM StudentAssignment s")
public class StudentAssignment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_student_assignment_id")
	private Long studentAssignmentId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "assignment_submitted_on")
	private Date assignmentSubmittedOn;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_review_completed")
	private Boolean isReviewCompleted;

	@Column(name = "marks_secured")
	private Integer marksSecured;

	private String reason;

	@Column(name = "review_comments")
	private String reviewComments;

	@Column(name = "status_comments")
	private String statusComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "status_updated_on")
	private Date statusUpdatedOn;

	@Column(name = "student_description")
	private String studentDescription;

	@Column(name = "student_summary")
	private String studentSummary;

	@Column(name = "submssion_file")
	private String submssionFile;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to AssignmentWorkflow
	@OneToMany(mappedBy = "studentAssignment",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	private List<AssignmentWorkflow> assignmentWorkflows;

	// bi-directional many-to-one association to Assignment
	@ManyToOne
	@JoinColumn(name = "fk_assignment_id")
	private Assignment assignment;

	// bi-directional many-to-one association to TMSchool
	@ManyToOne
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to TMWorkflowStage
	@ManyToOne
	@JoinColumn(name = "fk_wf_assignment_status_id")
	private WorkflowStage workflowStage;

	// bi-directional many-to-one association to TStdStudentDetail
	@ManyToOne
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	public StudentAssignment() {
	}

	public Long getStudentAssignmentId() {
		return this.studentAssignmentId;
	}

	public void setStudentAssignmentId(Long studentAssignmentId) {
		this.studentAssignmentId = studentAssignmentId;
	}

	public Date getAssignmentSubmittedOn() {
		return this.assignmentSubmittedOn;
	}

	public void setAssignmentSubmittedOn(Date assignmentSubmittedOn) {
		this.assignmentSubmittedOn = assignmentSubmittedOn;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsReviewCompleted() {
		return this.isReviewCompleted;
	}

	public void setIsReviewCompleted(Boolean isReviewCompleted) {
		this.isReviewCompleted = isReviewCompleted;
	}

	public Integer getMarksSecured() {
		return this.marksSecured;
	}

	public void setMarksSecured(Integer marksSecured) {
		this.marksSecured = marksSecured;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReviewComments() {
		return this.reviewComments;
	}

	public void setReviewComments(String reviewComments) {
		this.reviewComments = reviewComments;
	}

	public String getStatusComments() {
		return this.statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public Date getStatusUpdatedOn() {
		return this.statusUpdatedOn;
	}

	public void setStatusUpdatedOn(Date statusUpdatedOn) {
		this.statusUpdatedOn = statusUpdatedOn;
	}

	public String getStudentDescription() {
		return this.studentDescription;
	}

	public void setStudentDescription(String studentDescription) {
		this.studentDescription = studentDescription;
	}

	public String getStudentSummary() {
		return this.studentSummary;
	}

	public void setStudentSummary(String studentSummary) {
		this.studentSummary = studentSummary;
	}

	public String getSubmssionFile() {
		return this.submssionFile;
	}

	public void setSubmssionFile(String submssionFile) {
		this.submssionFile = submssionFile;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<AssignmentWorkflow> getAssignmentWorkflows() {
		return this.assignmentWorkflows;
	}

	public void setAssignmentWorkflows(List<AssignmentWorkflow> assignmentWorkflows) {
		this.assignmentWorkflows = assignmentWorkflows;
	}

	public AssignmentWorkflow addAssignmentWorkflow(AssignmentWorkflow assignmentWorkflow) {
		getAssignmentWorkflows().add(assignmentWorkflow);
		assignmentWorkflow.setStudentAssignment(this);

		return assignmentWorkflow;
	}

	public AssignmentWorkflow removeAssignmentWorkflow(AssignmentWorkflow assignmentWorkflow) {
		getAssignmentWorkflows().remove(assignmentWorkflow);
		assignmentWorkflow.setStudentAssignment(null);

		return assignmentWorkflow;
	}

	public Assignment getAssignment() {
		return this.assignment;
	}

	public void setAssignment(Assignment assignment) {
		this.assignment = assignment;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public WorkflowStage getWorkflowStage() {
		return workflowStage;
	}

	public void setWorkflowStage(WorkflowStage workflowStage) {
		this.workflowStage = workflowStage;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

}