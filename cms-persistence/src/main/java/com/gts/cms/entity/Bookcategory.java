package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_lib_bookcategory database table.
 * 
 */
@Entity
@Table(name="t_lib_bookcategory")
@NamedQuery(name="Bookcategory.findAll", query="SELECT b FROM Bookcategory b")
public class Bookcategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_lib_bookcat_id")
	private Long bookcatId;

	@Column(name="book_category_code")
	private String bookCategoryCode;

	@Column(name="book_category_name")
	private String bookCategoryName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to Organization
	@ManyToOne
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to LibraryDetail
	@ManyToOne
	@JoinColumn(name="fk_library_id")
	private LibraryDetail libraryDetail;

	//bi-directional many-to-one association to Periodical
	@OneToMany(mappedBy="bookcategory")
	private List<Periodical> periodicals;

	public Bookcategory() {
	}

	public Long getBookcatId() {
		return this.bookcatId;
	}

	public void setBookcatId(Long bookcatId) {
		this.bookcatId = bookcatId;
	}

	public String getBookCategoryCode() {
		return this.bookCategoryCode;
	}

	public void setBookCategoryCode(String bookCategoryCode) {
		this.bookCategoryCode = bookCategoryCode;
	}

	public String getBookCategoryName() {
		return this.bookCategoryName;
	}

	public void setBookCategoryName(String bookCategoryName) {
		this.bookCategoryName = bookCategoryName;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public LibraryDetail getLibraryDetail() {
		return this.libraryDetail;
	}

	public void setLibraryDetail(LibraryDetail libraryDetail) {
		this.libraryDetail = libraryDetail;
	}

	public List<Periodical> getPeriodicals() {
		return this.periodicals;
	}

	public void setPeriodicals(List<Periodical> periodicals) {
		this.periodicals = periodicals;
	}

	public Periodical addPeriodical(Periodical periodical) {
		getPeriodicals().add(periodical);
		periodical.setBookcategory(this);

		return periodical;
	}

	public Periodical removePeriodical(Periodical periodical) {
		getPeriodicals().remove(periodical);
		periodical.setBookcategory(null);

		return periodical;
	}

}