package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_ams_fundraise_event database table.
 * 
 */
@Entity
@Table(name = "t_ams_fundraise_event")
@NamedQuery(name = "AmsFundraiseEvent.findAll", query = "SELECT f FROM AmsFundraiseEvent f")
public class AmsFundraiseEvent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_fundraise_event_id")
	private Long fundraiseEventId;

	@Column(name = "audit_comments")
	private String auditComments;

	@Column(name = "audit_rating_percentage")
	private Integer auditRatingPercentage;

	@Column(name = "balance_sheet_url")
	private String balanceSheetUrl;

	@Column(name = "conclusion_message")
	private String conclusionMessage;

	@Column(name = "cover_photo")
	private String coverPhoto;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "expected_expenditure")
	private BigDecimal expectedExpenditure;

	@Column(name = "fk_audit_emp_ids")
	private String auditEmpIds;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_fundraise_purpose_catdet_id")
	private GeneralDetail fundraisePurposeCat;

	@Lob
	@Column(name = "fundraise_body")
	private String fundraiseBody;

	@Column(name = "fundraise_signature")
	private String fundraiseSignature;

	@Column(name = "fundraise_title")
	private String fundraiseTitle;

	@Column(name = "fundraising_for")
	private String fundraisingFor;

	@Column(name = "fundraising_for_details")
	private String fundraisingForDetails;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_audit_completed")
	private Boolean isAuditCompleted;

	@Column(name = "is_fundriase_closed")
	private Boolean isFundriaseClosed;

	private String reason;

	@Column(name = "ref_attachement1")
	private String refAttachement1;

	@Column(name = "ref_attachement2")
	private String refAttachement2;

	@Column(name = "ref_attachement3")
	private String refAttachement3;

	@Temporal(TemporalType.DATE)
	@Column(name = "start_date")
	private Date startDate;

	@Column(name = "total_balance")
	private BigDecimal totalBalance;

	@Column(name = "total_fund_expended")
	private BigDecimal totalFundExpended;

	@Column(name = "total_fund_raised")
	private BigDecimal totalFundRaised;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to AmsFundraiseDetail
	@OneToMany(mappedBy = "fundraiseEvent")
	private List<AmsFundraiseDetail> fundraiseDetails;

	// bi-directional many-to-one association to AmsFundraiseAccount
	@ManyToOne
	@JoinColumn(name = "fk_fundraise_account_id")
	private AmsFundraiseAccount fundraiseAccount;

	// bi-directional many-to-one association to AmsFundraiseExpenditure
	@OneToMany(mappedBy = "fundraiseEvent")
	private List<AmsFundraiseExpenditure> fundraiseExpenditures;

	public AmsFundraiseEvent() {
	}

	public Long getFundraiseEventId() {
		return this.fundraiseEventId;
	}

	public void setFundraiseEventId(Long fundraiseEventId) {
		this.fundraiseEventId = fundraiseEventId;
	}

	public String getAuditComments() {
		return this.auditComments;
	}

	public void setAuditComments(String auditComments) {
		this.auditComments = auditComments;
	}

	public Integer getAuditRatingPercentage() {
		return this.auditRatingPercentage;
	}

	public void setAuditRatingPercentage(Integer auditRatingPercentage) {
		this.auditRatingPercentage = auditRatingPercentage;
	}

	public String getBalanceSheetUrl() {
		return this.balanceSheetUrl;
	}

	public void setBalanceSheetUrl(String balanceSheetUrl) {
		this.balanceSheetUrl = balanceSheetUrl;
	}

	public String getConclusionMessage() {
		return this.conclusionMessage;
	}

	public void setConclusionMessage(String conclusionMessage) {
		this.conclusionMessage = conclusionMessage;
	}

	public String getCoverPhoto() {
		return this.coverPhoto;
	}

	public void setCoverPhoto(String coverPhoto) {
		this.coverPhoto = coverPhoto;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public BigDecimal getExpectedExpenditure() {
		return this.expectedExpenditure;
	}

	public void setExpectedExpenditure(BigDecimal expectedExpenditure) {
		this.expectedExpenditure = expectedExpenditure;
	}

	public String getAuditEmpIds() {
		return this.auditEmpIds;
	}

	public void setAuditEmpIds(String auditEmpIds) {
		this.auditEmpIds = auditEmpIds;
	}

	public String getFundraiseBody() {
		return this.fundraiseBody;
	}

	public void setFundraiseBody(String fundraiseBody) {
		this.fundraiseBody = fundraiseBody;
	}

	public String getFundraiseSignature() {
		return this.fundraiseSignature;
	}

	public void setFundraiseSignature(String fundraiseSignature) {
		this.fundraiseSignature = fundraiseSignature;
	}

	public String getFundraiseTitle() {
		return this.fundraiseTitle;
	}

	public void setFundraiseTitle(String fundraiseTitle) {
		this.fundraiseTitle = fundraiseTitle;
	}

	public String getFundraisingFor() {
		return this.fundraisingFor;
	}

	public void setFundraisingFor(String fundraisingFor) {
		this.fundraisingFor = fundraisingFor;
	}

	public String getFundraisingForDetails() {
		return this.fundraisingForDetails;
	}

	public void setFundraisingForDetails(String fundraisingForDetails) {
		this.fundraisingForDetails = fundraisingForDetails;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsAuditCompleted() {
		return this.isAuditCompleted;
	}

	public void setIsAuditCompleted(Boolean isAuditCompleted) {
		this.isAuditCompleted = isAuditCompleted;
	}

	public Boolean getIsFundriaseClosed() {
		return this.isFundriaseClosed;
	}

	public void setIsFundriaseClosed(Boolean isFundriaseClosed) {
		this.isFundriaseClosed = isFundriaseClosed;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRefAttachement1() {
		return this.refAttachement1;
	}

	public void setRefAttachement1(String refAttachement1) {
		this.refAttachement1 = refAttachement1;
	}

	public String getRefAttachement2() {
		return this.refAttachement2;
	}

	public void setRefAttachement2(String refAttachement2) {
		this.refAttachement2 = refAttachement2;
	}

	public String getRefAttachement3() {
		return this.refAttachement3;
	}

	public void setRefAttachement3(String refAttachement3) {
		this.refAttachement3 = refAttachement3;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public BigDecimal getTotalBalance() {
		return this.totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public BigDecimal getTotalFundExpended() {
		return this.totalFundExpended;
	}

	public void setTotalFundExpended(BigDecimal totalFundExpended) {
		this.totalFundExpended = totalFundExpended;
	}

	public BigDecimal getTotalFundRaised() {
		return this.totalFundRaised;
	}

	public void setTotalFundRaised(BigDecimal totalFundRaised) {
		this.totalFundRaised = totalFundRaised;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public GeneralDetail getFundraisePurposeCat() {
		return fundraisePurposeCat;
	}

	public void setFundraisePurposeCat(GeneralDetail fundraisePurposeCat) {
		this.fundraisePurposeCat = fundraisePurposeCat;
	}

	public List<AmsFundraiseDetail> getFundraiseDetails() {
		return fundraiseDetails;
	}

	public void setFundraiseDetails(List<AmsFundraiseDetail> fundraiseDetails) {
		this.fundraiseDetails = fundraiseDetails;
	}

	public AmsFundraiseAccount getFundraiseAccount() {
		return fundraiseAccount;
	}

	public void setFundraiseAccount(AmsFundraiseAccount fundraiseAccount) {
		this.fundraiseAccount = fundraiseAccount;
	}

	public List<AmsFundraiseExpenditure> getFundraiseExpenditures() {
		return fundraiseExpenditures;
	}

	public void setFundraiseExpenditures(List<AmsFundraiseExpenditure> fundraiseExpenditures) {
		this.fundraiseExpenditures = fundraiseExpenditures;
	}

}