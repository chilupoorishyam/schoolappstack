package com.gts.cms.entity;

import com.fasterxml.jackson.annotation.JsonValue;

public enum RecordingFileStatus {
    INIT("INIT"),
    NO_RECORDING("NO_RECORDING"),
    UPLOAD_SUCCESS("UPLOAD SUCCESS"),
    DOWNLOAD_SUCCESS("DOWNLOAD SUCCESS");

    @JsonValue
    String recordingFileStatus;

    RecordingFileStatus(String recordingFileStatus) {
        this.recordingFileStatus = recordingFileStatus;
    }

    public String getRecordingFileStatus() {
        return recordingFileStatus;
    }
}
