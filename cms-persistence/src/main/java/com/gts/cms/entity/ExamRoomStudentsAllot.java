package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_exam_room_students_allot database table.
 * 
 */
@Entity
@Table(name = "t_exam_room_students_allot")
@NamedQuery(name = "ExamRoomStudentsAllot.findAll", query = "SELECT e FROM ExamRoomStudentsAllot e")
public class ExamRoomStudentsAllot implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_room_std_allot_id")
	private Long examRoomStdAllotId;

	@Column(name = "column_no")
	private Integer columnNo;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to ExamMaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_id")
	private ExamMaster examMaster;

	// bi-directional many-to-one association to ExamTimetable
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_timetable_id")
	private ExamTimetable examTimetable;
	
	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_examseatstatus_catdet_id")
	private GeneralDetail examseatstatusCat;

	// bi-directional many-to-one association to Room
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_room_id")
	private Room room;

	// bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_subject_id")
	private Subject subject;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_presented")
	private Boolean isPresented;

	private String reason;

	@Column(name = "row_no")
	private Integer rowNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to ExamRoomAllotment
	@ManyToOne
	@JoinColumn(name = "fk_exam_room_allotment_id")
	private ExamRoomAllotment examRoomAllotment;

	public ExamRoomStudentsAllot() {
	}

	public Long getExamRoomStdAllotId() {
		return this.examRoomStdAllotId;
	}

	public void setExamRoomStdAllotId(Long examRoomStdAllotId) {
		this.examRoomStdAllotId = examRoomStdAllotId;
	}

	public Integer getColumnNo() {
		return this.columnNo;
	}

	public void setColumnNo(Integer columnNo) {
		this.columnNo = columnNo;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsPresented() {
		return this.isPresented;
	}

	public void setIsPresented(Boolean isPresented) {
		this.isPresented = isPresented;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getRowNo() {
		return this.rowNo;
	}

	public void setRowNo(Integer rowNo) {
		this.rowNo = rowNo;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public ExamMaster getExamMaster() {
		return examMaster;
	}

	public void setExamMaster(ExamMaster examMaster) {
		this.examMaster = examMaster;
	}

	public ExamTimetable getExamTimetable() {
		return examTimetable;
	}

	public void setExamTimetable(ExamTimetable examTimetable) {
		this.examTimetable = examTimetable;
	}

	public GeneralDetail getExamseatstatusCat() {
		return examseatstatusCat;
	}

	public void setExamseatstatusCat(GeneralDetail examseatstatusCat) {
		this.examseatstatusCat = examseatstatusCat;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public ExamRoomAllotment getExamRoomAllotment() {
		return examRoomAllotment;
	}

	public void setExamRoomAllotment(ExamRoomAllotment examRoomAllotment) {
		this.examRoomAllotment = examRoomAllotment;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}