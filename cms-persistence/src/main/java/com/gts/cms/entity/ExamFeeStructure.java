package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_exam_fee_structure database table.
 * 
 */
@Entity
@Table(name = "t_exam_fee_structure")
@NamedQuery(name = "ExamFeeStructure.findAll", query = "SELECT e FROM ExamFeeStructure e")
public class ExamFeeStructure implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_fee_structure_id")
	private Long examFeeStructureId;

	@Temporal(TemporalType.DATE)
	@Column(name = "collection_end_date")
	private Date collectionEndDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "collection_start_date")
	private Date collectionStartDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	@Column(name = "due_date_wo_fine")
	private Date dueDateWoFine;

	@Column(name = "exam_fee_structure_name")
	private String examFeeStructureName;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_additional_fee")
	private Boolean isAdditionalFee;

	private String reason;

	@Column(name = "reg_fee")
	private BigDecimal regFee;

	@Column(name = "subject1_fee")
	private BigDecimal subject1Fee;

	@Column(name = "subject2_fee")
	private BigDecimal subject2Fee;

	@Column(name = "subject3_fee")
	private BigDecimal subject3Fee;

	@Column(name = "subject4_fee")
	private BigDecimal subject4Fee;

	@Column(name = "subject5_fee")
	private BigDecimal subject5Fee;

	@Column(name = "subject6_fee")
	private BigDecimal subject6Fee;

	@Column(name = "subject7_fee")
	private BigDecimal subject7Fee;

	@Column(name = "supply_fee")
	private BigDecimal supplyFee;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to ExamFeeAdditionalStructure
	@OneToMany(mappedBy = "examFeeStructure",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<ExamFeeAdditionalStructure> examFeeAdditionalStructures;

	// bi-directional many-to-one association to ExamFeeFine
	@OneToMany(mappedBy = "examFeeStructure",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<ExamFeeFine> examFeeFines;

	// bi-directional many-to-one association to ExamMaster
	@ManyToOne
	@JoinColumn(name = "fk_exam_id")
	private ExamMaster examMaster;
	
//	// bi-directional many-to-one association to GeneralDetail
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="fk_adt_examfeetype_catdet_id")
//	private GeneralDetail addtExamFeeTypeCat;
//		

	

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to ExamFeeStructureCourseyr
	@OneToMany(mappedBy = "examFeeStructure",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<ExamFeeStructureCourseyr> examFeeStructureCourseyrs;

	public ExamFeeStructure() {
	}

	public Long getExamFeeStructureId() {
		return this.examFeeStructureId;
	}

	public void setExamFeeStructureId(Long examFeeStructureId) {
		this.examFeeStructureId = examFeeStructureId;
	}

	public Date getCollectionEndDate() {
		return this.collectionEndDate;
	}

	public void setCollectionEndDate(Date collectionEndDate) {
		this.collectionEndDate = collectionEndDate;
	}

	public Date getCollectionStartDate() {
		return this.collectionStartDate;
	}

	public void setCollectionStartDate(Date collectionStartDate) {
		this.collectionStartDate = collectionStartDate;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getDueDateWoFine() {
		return this.dueDateWoFine;
	}

	public void setDueDateWoFine(Date dueDateWoFine) {
		this.dueDateWoFine = dueDateWoFine;
	}

	public String getExamFeeStructureName() {
		return this.examFeeStructureName;
	}

	public void setExamFeeStructureName(String examFeeStructureName) {
		this.examFeeStructureName = examFeeStructureName;
	}
	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsAdditionalFee() {
		return this.isAdditionalFee;
	}

	public void setIsAdditionalFee(Boolean isAdditionalFee) {
		this.isAdditionalFee = isAdditionalFee;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getRegFee() {
		return this.regFee;
	}

	public void setRegFee(BigDecimal regFee) {
		this.regFee = regFee;
	}

	public BigDecimal getSubject1Fee() {
		return this.subject1Fee;
	}

	public void setSubject1Fee(BigDecimal subject1Fee) {
		this.subject1Fee = subject1Fee;
	}

	public BigDecimal getSubject2Fee() {
		return this.subject2Fee;
	}

	public void setSubject2Fee(BigDecimal subject2Fee) {
		this.subject2Fee = subject2Fee;
	}

	public BigDecimal getSubject3Fee() {
		return this.subject3Fee;
	}

	public void setSubject3Fee(BigDecimal subject3Fee) {
		this.subject3Fee = subject3Fee;
	}

	public BigDecimal getSubject4Fee() {
		return this.subject4Fee;
	}

	public void setSubject4Fee(BigDecimal subject4Fee) {
		this.subject4Fee = subject4Fee;
	}

	public BigDecimal getSubject5Fee() {
		return this.subject5Fee;
	}

	public void setSubject5Fee(BigDecimal subject5Fee) {
		this.subject5Fee = subject5Fee;
	}

	public BigDecimal getSubject6Fee() {
		return this.subject6Fee;
	}

	public void setSubject6Fee(BigDecimal subject6Fee) {
		this.subject6Fee = subject6Fee;
	}

	public BigDecimal getSubject7Fee() {
		return this.subject7Fee;
	}

	public void setSubject7Fee(BigDecimal subject7Fee) {
		this.subject7Fee = subject7Fee;
	}

	public BigDecimal getSupplyFee() {
		return this.supplyFee;
	}

	public void setSupplyFee(BigDecimal supplyFee) {
		this.supplyFee = supplyFee;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<ExamFeeAdditionalStructure> getExamFeeAdditionalStructures() {
		return examFeeAdditionalStructures;
	}

	public void setExamFeeAdditionalStructures(List<ExamFeeAdditionalStructure> examFeeAdditionalStructures) {
		this.examFeeAdditionalStructures = examFeeAdditionalStructures;
	}

	public List<ExamFeeFine> getExamFeeFines() {
		return examFeeFines;
	}

	public void setExamFeeFines(List<ExamFeeFine> examFeeFines) {
		this.examFeeFines = examFeeFines;
	}

	public ExamMaster getExamMaster() {
		return examMaster;
	}

	public void setExamMaster(ExamMaster examMaster) {
		this.examMaster = examMaster;
	}

	public List<ExamFeeStructureCourseyr> getExamFeeStructureCourseyrs() {
		return examFeeStructureCourseyrs;
	}

	public void setExamFeeStructureCourseyrs(List<ExamFeeStructureCourseyr> examFeeStructureCourseyrs) {
		this.examFeeStructureCourseyrs = examFeeStructureCourseyrs;
	}
	
//	public GeneralDetail getAddtExamFeeTypeCat() {
//		return addtExamFeeTypeCat;
//	}
//
//	public void setAddtExamFeeTypeCat(GeneralDetail addtExamFeeTypeCat) {
//		this.addtExamFeeTypeCat = addtExamFeeTypeCat;
//	}

}