package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the t_fee_instant_payment_particulars database table.
 * 
 */
@Entity
@Table(name="t_fee_instant_payment_particulars")
@NamedQuery(name="FeeInstantPaymentParticular.findAll", query="SELECT f FROM FeeInstantPaymentParticular f")
public class FeeInstantPaymentParticular implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_instant_payment_particular_id")
	private Long feeInstantPaymentParticularId;

	private BigDecimal amount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private BigDecimal discount;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="new_particular_name")
	private String newParticularName;

	private BigDecimal paid;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FeeInstantCategory
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_instant_category_id")
	private FeeInstantCategory feeInstantCategory;

	//bi-directional many-to-one association to FeeInstantParticular
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_instant_particular_id")
	private FeeInstantParticular feeInstantParticular;

	//bi-directional many-to-one association to FeeInstantPayment
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_instant_payment_id")
	private FeeInstantPayment feeInstantPayment;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	public FeeInstantPaymentParticular() {
	}

	public Long getFeeInstantPaymentParticularId() {
		return this.feeInstantPaymentParticularId;
	}

	public void setFeeInstantPaymentParticularId(Long feeInstantPaymentParticularId) {
		this.feeInstantPaymentParticularId = feeInstantPaymentParticularId;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public BigDecimal getDiscount() {
		return this.discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getNewParticularName() {
		return this.newParticularName;
	}

	public void setNewParticularName(String newParticularName) {
		this.newParticularName = newParticularName;
	}

	public BigDecimal getPaid() {
		return this.paid;
	}

	public void setPaid(BigDecimal paid) {
		this.paid = paid;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public FeeInstantCategory getFeeInstantCategory() {
		return this.feeInstantCategory;
	}

	public void setFeeInstantCategory(FeeInstantCategory feeInstantCategory) {
		this.feeInstantCategory = feeInstantCategory;
	}

	public FeeInstantParticular getFeeInstantParticular() {
		return this.feeInstantParticular;
	}

	public void setFeeInstantParticular(FeeInstantParticular feeInstantParticular) {
		this.feeInstantParticular = feeInstantParticular;
	}

	public FeeInstantPayment getFeeInstantPayment() {
		return this.feeInstantPayment;
	}

	public void setFeeInstantPayment(FeeInstantPayment feeInstantPayment) {
		this.feeInstantPayment = feeInstantPayment;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

}