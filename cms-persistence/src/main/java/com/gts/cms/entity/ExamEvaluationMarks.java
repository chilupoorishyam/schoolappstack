package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_exam_evaluation_marks database table.
 */
@Data
@Entity
@Table(name = "t_exam_evaluation_marks")
@NamedQuery(name = "ExamEvaluationMarks.findAll", query = "SELECT e FROM ExamEvaluationMarks e")
public class ExamEvaluationMarks implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_exam_evaluation_marks_id")
    private Long examEvaluationMarksId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "marks")
    private Integer marks;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "exam_date")
    private Date examDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "reviewed_on")
    private Date reviewedOn;

    @Column(name = "review_comments")
    private String reviewComments;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "published_on")
    private Date publishedOn;

    @Column(name = "is_published")
    private Boolean isPublished;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_school_id")
    private School school;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_subject_id")
    private Subject subject;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_exam_type_id")
    private ExamTypes examTypes;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_exam_id")
    private ExamMaster examMaster;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_course_year_id")
    private CourseYear courseYear;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_student_id")
    private StudentDetail studentDetail;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_reviewer_emp_id")
    private EmployeeDetail employeeDetail;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_exam_sub_evaluation_id")
    private ExamSubjectwiseEvaluation examSubjectwiseEvaluation;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;
}