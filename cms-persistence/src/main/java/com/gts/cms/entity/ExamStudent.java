package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_exam_students database table.
 * 
 */
@Entity
@Table(name = "t_exam_students")
@NamedQuery(name = "ExamStudent.findAll", query = "SELECT e FROM ExamStudent e")
public class ExamStudent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_std_id")
	private Long examStdId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "fee_comments")
	private String feeComments;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_year_id")
	private CourseYear courseYear;
	
	// bi-directional many-to-one association to FeeReceipt
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_fee_receipt_id")
	private ExamFeeReceipt examFeeReceipt;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_examtype_catdet_id")
	private GeneralDetail examtypeCat;

	// bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	@Temporal(TemporalType.DATE)
	@Column(name = "hallticket_issued_on")
	private Date hallticketIssuedOn;

	@Column(name = "hallticket_no")
	private String hallticketNo;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_fee_paid")
	private Boolean isFeePaid;

	@Column(name = "is_hallticket_issued")
	private Boolean isHallticketIssued;

	@Temporal(TemporalType.DATE)
	@Column(name = "memo_date")
	private Date memoDate;

	@Column(name = "memo_no")
	private String memoNo;

	@Column(name = "memo_serial_no")
	private String memoSerialNo;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "registration_date")
	private Date registrationDate;

	@Column(name = "sub_appeared")
	private Integer subAppeared;

	@Column(name = "sub_passed")
	private Integer subPassed;

	@Column(name = "sub_registered")
	private Integer subRegistered;

	@Column(name = "total_credits")
	private Integer totalCredits;

	@Column(name = "total_external")
	private Integer totalExternal;

	@Column(name = "total_internal")
	private Integer totalInternal;

	@Column(name = "total_marks")
	private Integer totalMarks;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to ExamStudentDetail
	@OneToMany(mappedBy = "examStudent",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	private List<ExamStudentDetail> examStudentDetails;

	/*// bi-directional many-to-one association to ExamStudentMemoSubject
	@OneToMany(mappedBy = "examStudent")
	private List<ExamStudentMemoSubject> examStudentMemoSubjects;
*/
	// bi-directional many-to-one association to ExamMaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_id")
	private ExamMaster examMaster;
	


	public ExamStudent() {
	}

	public Long getExamStdId() {
		return this.examStdId;
	}

	public void setExamStdId(Long examStdId) {
		this.examStdId = examStdId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getFeeComments() {
		return this.feeComments;
	}

	public void setFeeComments(String feeComments) {
		this.feeComments = feeComments;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public CourseYear getCourseYear() {
		return courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public ExamFeeReceipt getExamFeeReceipt() {
		return examFeeReceipt;
	}

	public void setExamFeeReceipt(ExamFeeReceipt examFeeReceipt) {
		this.examFeeReceipt = examFeeReceipt;
	}

	public GeneralDetail getExamtypeCat() {
		return examtypeCat;
	}

	public void setExamtypeCat(GeneralDetail examtypeCat) {
		this.examtypeCat = examtypeCat;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public Date getHallticketIssuedOn() {
		return hallticketIssuedOn;
	}

	public void setHallticketIssuedOn(Date hallticketIssuedOn) {
		this.hallticketIssuedOn = hallticketIssuedOn;
	}

	public String getHallticketNo() {
		return hallticketNo;
	}

	public void setHallticketNo(String hallticketNo) {
		this.hallticketNo = hallticketNo;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsFeePaid() {
		return isFeePaid;
	}

	public void setIsFeePaid(Boolean isFeePaid) {
		this.isFeePaid = isFeePaid;
	}

	public Boolean getIsHallticketIssued() {
		return isHallticketIssued;
	}

	public void setIsHallticketIssued(Boolean isHallticketIssued) {
		this.isHallticketIssued = isHallticketIssued;
	}

	public Date getMemoDate() {
		return memoDate;
	}

	public void setMemoDate(Date memoDate) {
		this.memoDate = memoDate;
	}

	public String getMemoNo() {
		return memoNo;
	}

	public void setMemoNo(String memoNo) {
		this.memoNo = memoNo;
	}

	public String getMemoSerialNo() {
		return memoSerialNo;
	}

	public void setMemoSerialNo(String memoSerialNo) {
		this.memoSerialNo = memoSerialNo;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Integer getSubAppeared() {
		return subAppeared;
	}

	public void setSubAppeared(Integer subAppeared) {
		this.subAppeared = subAppeared;
	}

	public Integer getSubPassed() {
		return subPassed;
	}

	public void setSubPassed(Integer subPassed) {
		this.subPassed = subPassed;
	}

	public Integer getSubRegistered() {
		return subRegistered;
	}

	public void setSubRegistered(Integer subRegistered) {
		this.subRegistered = subRegistered;
	}

	public Integer getTotalCredits() {
		return totalCredits;
	}

	public void setTotalCredits(Integer totalCredits) {
		this.totalCredits = totalCredits;
	}

	public Integer getTotalExternal() {
		return totalExternal;
	}

	public void setTotalExternal(Integer totalExternal) {
		this.totalExternal = totalExternal;
	}

	public Integer getTotalInternal() {
		return totalInternal;
	}

	public void setTotalInternal(Integer totalInternal) {
		this.totalInternal = totalInternal;
	}

	public Integer getTotalMarks() {
		return totalMarks;
	}

	public void setTotalMarks(Integer totalMarks) {
		this.totalMarks = totalMarks;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<ExamStudentDetail> getExamStudentDetails() {
		return examStudentDetails;
	}

	public void setExamStudentDetails(List<ExamStudentDetail> examStudentDetails) {
		this.examStudentDetails = examStudentDetails;
	}

	/*public List<ExamStudentMemoSubject> getExamStudentMemoSubjects() {
		return examStudentMemoSubjects;
	}

	public void setExamStudentMemoSubjects(List<ExamStudentMemoSubject> examStudentMemoSubjects) {
		this.examStudentMemoSubjects = examStudentMemoSubjects;
	}
*/
	public ExamMaster getExamMaster() {
		return examMaster;
	}

	public void setExamMaster(ExamMaster examMaster) {
		this.examMaster = examMaster;
	}


}