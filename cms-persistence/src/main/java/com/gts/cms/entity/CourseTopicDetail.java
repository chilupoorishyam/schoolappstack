package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_dl_course_topic_details database table.
 * 
 */
@Entity
@Table(name = "t_dl_course_topic_details")
@NamedQuery(name = "CourseTopicDetail.findAll", query = "SELECT c FROM CourseTopicDetail c")
public class CourseTopicDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_dl_course_topic_det_id")
	private Long courseTopicDetId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "lesson_duration_minutes")
	private Integer lessonDurationMinutes;

	private String reason;

	@Column(name = "starts_at")
	private Time startsAt;

	@Column(name = "topic_detail_no")
	private Integer topicDetailNo;

	@Column(name = "topic_details")
	private String topicDetails;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to CourseMemberTopicLog
	@OneToMany(mappedBy = "courseTopicDetail", cascade = CascadeType.ALL)
	private List<CourseMemberTopicLog> courseMemberTopicLogs;

	// bi-directional many-to-one association to CourseLessonsTopic
	@ManyToOne
	@JoinColumn(name = "fk_dl_course_lesson_topic_id")
	private CourseLessonsTopic courseLessonsTopic;

	// bi-directional many-to-one association to TDlMemberLearningNote
	@OneToMany(mappedBy = "courseTopicDetail", cascade = CascadeType.ALL)
	private List<MemberLearningNote> memberLearningNotes;

	public CourseTopicDetail() {
	}

	public Long getCourseTopicDetId() {
		return this.courseTopicDetId;
	}

	public void setCourseTopicDetId(Long courseTopicDetId) {
		this.courseTopicDetId = courseTopicDetId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getLessonDurationMinutes() {
		return this.lessonDurationMinutes;
	}

	public void setLessonDurationMinutes(Integer lessonDurationMinutes) {
		this.lessonDurationMinutes = lessonDurationMinutes;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Time getStartsAt() {
		return this.startsAt;
	}

	public void setStartsAt(Time startsAt) {
		this.startsAt = startsAt;
	}

	public Integer getTopicDetailNo() {
		return this.topicDetailNo;
	}

	public void setTopicDetailNo(Integer topicDetailNo) {
		this.topicDetailNo = topicDetailNo;
	}

	public String getTopicDetails() {
		return this.topicDetails;
	}

	public void setTopicDetails(String topicDetails) {
		this.topicDetails = topicDetails;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<CourseMemberTopicLog> getCourseMemberTopicLogs() {
		return courseMemberTopicLogs;
	}

	public void setCourseMemberTopicLogs(List<CourseMemberTopicLog> courseMemberTopicLogs) {
		this.courseMemberTopicLogs = courseMemberTopicLogs;
	}

	public CourseLessonsTopic getCourseLessonsTopic() {
		return courseLessonsTopic;
	}

	public void setCourseLessonsTopic(CourseLessonsTopic courseLessonsTopic) {
		this.courseLessonsTopic = courseLessonsTopic;
	}

	public List<MemberLearningNote> getMemberLearningNotes() {
		return memberLearningNotes;
	}

	public void setMemberLearningNotes(List<MemberLearningNote> memberLearningNotes) {
		this.memberLearningNotes = memberLearningNotes;
	}

}