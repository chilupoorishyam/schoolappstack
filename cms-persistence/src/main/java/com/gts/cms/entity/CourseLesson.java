package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_dl_course_lessons database table.
 * 
 */
@Entity
@Table(name = "t_dl_course_lessons")
@NamedQuery(name = "CourseLesson.findAll", query = "SELECT c FROM CourseLesson c")
public class CourseLesson implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_dl_course_lesson_id")
	private Long courseLessonId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne
	@JoinColumn(name = "fk_sub_units_id")
	private SubjectUnit subjectUnit;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "lesson_code")
	private String lessonCode;

	@Column(name = "lesson_description")
	private String lessonDescription;

	@Column(name = "lesson_duration_minutes")
	private Integer lessonDurationMinutes;

	@Column(name = "lesson_name")
	private String lessonName;

	@Column(name = "lesson_no")
	private Integer lessonNo;

	@Column(name = "no_of_videos")
	private Integer noOfVideos;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to OnlineCourses
	@ManyToOne
	@JoinColumn(name = "fk_dl_onlinecourse_id")
	private OnlineCourses onlineCourses;

	// bi-directional many-to-one association to CourseLessonsTopic
	@OneToMany(mappedBy = "courseLesson",cascade = CascadeType.ALL)
	private List<CourseLessonsTopic> courseLessonsTopics;

	public CourseLesson() {
	}

	public Long getCourseLessonId() {
		return courseLessonId;
	}

	public void setCourseLessonId(Long courseLessonId) {
		this.courseLessonId = courseLessonId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public SubjectUnit getSubjectUnit() {
		return subjectUnit;
	}

	public void setSubjectUnit(SubjectUnit subjectUnit) {
		this.subjectUnit = subjectUnit;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getLessonCode() {
		return lessonCode;
	}

	public void setLessonCode(String lessonCode) {
		this.lessonCode = lessonCode;
	}

	public String getLessonDescription() {
		return lessonDescription;
	}

	public void setLessonDescription(String lessonDescription) {
		this.lessonDescription = lessonDescription;
	}

	public Integer getLessonDurationMinutes() {
		return lessonDurationMinutes;
	}

	public void setLessonDurationMinutes(Integer lessonDurationMinutes) {
		this.lessonDurationMinutes = lessonDurationMinutes;
	}

	public String getLessonName() {
		return lessonName;
	}

	public void setLessonName(String lessonName) {
		this.lessonName = lessonName;
	}

	public Integer getLessonNo() {
		return lessonNo;
	}

	public void setLessonNo(Integer lessonNo) {
		this.lessonNo = lessonNo;
	}

	public Integer getNoOfVideos() {
		return noOfVideos;
	}

	public void setNoOfVideos(Integer noOfVideos) {
		this.noOfVideos = noOfVideos;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public OnlineCourses getOnlineCourses() {
		return onlineCourses;
	}

	public void setOnlineCourses(OnlineCourses onlineCourses) {
		this.onlineCourses = onlineCourses;
	}

	public List<CourseLessonsTopic> getCourseLessonsTopics() {
		return courseLessonsTopics;
	}

	public void setCourseLessonsTopics(List<CourseLessonsTopic> courseLessonsTopics) {
		this.courseLessonsTopics = courseLessonsTopics;
	}

}