package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_tt_lessonstatus database table.
 * 
 */
@Entity
@Table(name="t_tt_lessonstatus")
@NamedQuery(name="Lessonstatus.findAll", query="SELECT l FROM Lessonstatus l")
public class Lessonstatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_leassonstatus_id")
	private Long leassonstatusId;

	@Temporal(TemporalType.DATE)
	@Column(name="class_date")
	private Date classDate;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private Integer percentage;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_sub_units_id")
	private SubjectUnit subjectUnit;
	
	//bi-directional many-to-one association to SubjectUnitTopic
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_sub_unit_topic_id")
	private SubjectUnitTopic subjectUnitTopic;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_lessonstatus_catdet_id")
	private GeneralDetail lessonstatus;

	//bi-directional many-to-one association to GroupSection
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_group_section_id")
	private GroupSection groupSection;

	//bi-directional many-to-one association to ActualClassesSchedule
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_actual_cls_schedule_id")
	private ActualClassesSchedule actualClassesSchedule;

	//bi-directional many-to-one association to Schedule
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_timetable_schedule_id")
	private Schedule schedule;

	//bi-directional many-to-one association to SubjectResource
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subject_resource_id")
	private SubjectResource subjectResource;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_teaching_method_catdet_id")
	private GeneralDetail teachingMethodCatdetId;

	public Lessonstatus() {
	}

	public Long getLeassonstatusId() {
		return this.leassonstatusId;
	}

	public void setLeassonstatusId(Long leassonstatusId) {
		this.leassonstatusId = leassonstatusId;
	}

	public Date getClassDate() {
		return this.classDate;
	}

	public void setClassDate(Date classDate) {
		this.classDate = classDate;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getPercentage() {
		return this.percentage;
	}

	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public SubjectUnit getSubjectUnit() {
		return subjectUnit;
	}

	public void setSubjectUnit(SubjectUnit subjectUnit) {
		this.subjectUnit = subjectUnit;
	}

	public SubjectUnitTopic getSubjectUnitTopic() {
		return this.subjectUnitTopic;
	}

	public void setSubjectUnitTopic(SubjectUnitTopic subjectUnitTopic) {
		this.subjectUnitTopic = subjectUnitTopic;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getLessonstatus() {
		return this.lessonstatus;
	}

	public void setLessonstatus(GeneralDetail lessonstatus) {
		this.lessonstatus = lessonstatus;
	}

	public GroupSection getGroupSection() {
		return this.groupSection;
	}

	public void setGroupSection(GroupSection groupSection) {
		this.groupSection = groupSection;
	}

	public ActualClassesSchedule getActualClassesSchedule() {
		return this.actualClassesSchedule;
	}

	public void setActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		this.actualClassesSchedule = actualClassesSchedule;
	}

	public Schedule getSchedule() {
		return this.schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	public SubjectResource getSubjectResource() {
		return this.subjectResource;
	}

	public void setSubjectResource(SubjectResource subjectResource) {
		this.subjectResource = subjectResource;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public GeneralDetail getTeachingMethodCatdetId() {
		return teachingMethodCatdetId;
	}

	public void setTeachingMethodCatdetId(GeneralDetail teachingMethodCatdetId) {
		this.teachingMethodCatdetId = teachingMethodCatdetId;
	}
}