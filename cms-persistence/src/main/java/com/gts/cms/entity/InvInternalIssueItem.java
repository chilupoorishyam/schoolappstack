package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the t_inv_internal_issue_items database table.
 * 
 */
@Entity
@Table(name = "t_inv_internal_issue_items")
@NamedQuery(name = "InvInternalIssueItem.findAll", query = "SELECT i FROM InvInternalIssueItem i")
public class InvInternalIssueItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_inter_issue_item_id")
	private Long interIssueItemId;

	@Column(name = "batch_no")
	private String batchNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "issued_date")
	private Date issuedDate;

	@Column(name = "issued_qty")
	private BigDecimal issuedQty;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "requested_date")
	private Date requestedDate;

	@Column(name = "requested_qty")
	private BigDecimal requestedQty;

	@Column(name = "returned_qty")
	private BigDecimal returnedQty;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to InvInternalIssue
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_inter_issue_id")
	private InvInternalIssue invInternalIssue;

	// bi-directional many-to-one association to InvInternalIndentitem
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_inter_ind_item_id")
	private InvInternalIndentitem invInternalIndentitem;

	// bi-directional many-to-one association to InvItemmaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_item_id")
	private InvItemmaster invItemmaster;

	// bi-directional many-to-one association to InvItemDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_item_det_id")
	private InvItemDetail invItemDetail;
/*
	// bi-directional many-to-one association to InvInternalReturnItem
	@OneToMany(mappedBy = "InvInternalIssueItem")
	private List<InvInternalReturnItem> invInternalReturnItems;*/
	
	@Transient
	private Long trakingItemDetailId;
	@Transient
	private Long trakingDeptId;
	@Transient
	private Long trakingEmployeeId;
	@Transient
	private Long trakingRoomId;

	public InvInternalIssueItem() {
	}

	public Long getInterIssueItemId() {
		return this.interIssueItemId;
	}

	public void setInterIssueItemId(Long interIssueItemId) {
		this.interIssueItemId = interIssueItemId;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Date getIssuedDate() {
		return this.issuedDate;
	}

	public void setIssuedDate(Date issuedDate) {
		this.issuedDate = issuedDate;
	}

	public BigDecimal getIssuedQty() {
		return this.issuedQty;
	}

	public void setIssuedQty(BigDecimal issuedQty) {
		this.issuedQty = issuedQty;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getRequestedDate() {
		return this.requestedDate;
	}

	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}

	public BigDecimal getRequestedQty() {
		return this.requestedQty;
	}

	public void setRequestedQty(BigDecimal requestedQty) {
		this.requestedQty = requestedQty;
	}

	public BigDecimal getReturnedQty() {
		return this.returnedQty;
	}

	public void setReturnedQty(BigDecimal returnedQty) {
		this.returnedQty = returnedQty;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public InvInternalIssue getInvInternalIssue() {
		return invInternalIssue;
	}

	public void setInvInternalIssue(InvInternalIssue invInternalIssue) {
		this.invInternalIssue = invInternalIssue;
	}

	public InvInternalIndentitem getInvInternalIndentitem() {
		return invInternalIndentitem;
	}

	public void setInvInternalIndentitem(InvInternalIndentitem invInternalIndentitem) {
		this.invInternalIndentitem = invInternalIndentitem;
	}

	public InvItemmaster getInvItemmaster() {
		return invItemmaster;
	}

	public void setInvItemmaster(InvItemmaster invItemmaster) {
		this.invItemmaster = invItemmaster;
	}

	public InvItemDetail getInvItemDetail() {
		return invItemDetail;
	}

	public void setInvItemDetail(InvItemDetail invItemDetail) {
		this.invItemDetail = invItemDetail;
	}

	public Long getTrakingItemDetailId() {
		return trakingItemDetailId;
	}

	public void setTrakingItemDetailId(Long trakingItemDetailId) {
		this.trakingItemDetailId = trakingItemDetailId;
	}

	public Long getTrakingDeptId() {
		return trakingDeptId;
	}

	public void setTrakingDeptId(Long trakingDeptId) {
		this.trakingDeptId = trakingDeptId;
	}

	public Long getTrakingEmployeeId() {
		return trakingEmployeeId;
	}

	public void setTrakingEmployeeId(Long trakingEmployeeId) {
		this.trakingEmployeeId = trakingEmployeeId;
	}

	public Long getTrakingRoomId() {
		return trakingRoomId;
	}

	public void setTrakingRoomId(Long trakingRoomId) {
		this.trakingRoomId = trakingRoomId;
	}

	/*public List<InvInternalReturnItem> getInvInternalReturnItems() {
		return invInternalReturnItems;
	}

	public void setInvInternalReturnItems(List<InvInternalReturnItem> invInternalReturnItems) {
		this.invInternalReturnItems = invInternalReturnItems;
	}*/

	
}