package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_std_enquiry database table.
 * 
 */
@Entity
@Table(name="t_std_enquiry")
@NamedQuery(name="StudentEnquiry.findAll", query="SELECT s FROM StudentEnquiry s")
public class StudentEnquiry implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_std_enquiry_id")
	private Long enquiryId;

	private String address;

	@Column(name="counseled_by")
	private String counseledBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String emailid;

	private String emcetrank;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="enquiry_date")
	private Date enquiryDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_closedenquiry")
	private Boolean isClosedenquiry;

	@Column(name="mobile_number")
	private String mobileNumber;

	@Column(name="mobile_number1")
	private String mobileNumber1;

	@Column(name="mobile_number2")
	private String mobileNumber2;
	
	@Column(name="enquiry_no")
	private String enquiryNo;

	private String parentmobile;

	private String parentname;

	private String percentage;

	private String reason;

	private String remarks;

	private String resultstatus;

	@Temporal(TemporalType.DATE)
	@Column(name="return_date")
	private Date returnDate;

	private String sourceofenquiry;

	@Column(name="student_name")
	private String studentName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to Course
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_id")
	private Course course;

	//bi-directional many-to-one association to District
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_district_id")
	private District district;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_enquirystatus_catdet_id")
	private GeneralDetail enquirystatus;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_modeofenquiry_catdet_id")
	private GeneralDetail modeofenquiry;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_knowaboutus_catdet_id")
	private GeneralDetail knowaboutus;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_gender_catdet_id")
	private GeneralDetail gender;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to QualificationGroup
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_qualif_group_id")
	private QualificationGroup qualificationGroup;

	//bi-directional many-to-one association to Qualification
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_qualification_id")
	private Qualification qualification;

	public StudentEnquiry() {
	}

	public Long getEnquiryId() {
		return this.enquiryId;
	}

	public void setEnquiryId(Long enquiryId) {
		this.enquiryId = enquiryId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCounseledBy() {
		return this.counseledBy;
	}

	public void setCounseledBy(String counseledBy) {
		this.counseledBy = counseledBy;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getEmailid() {
		return this.emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getEmcetrank() {
		return this.emcetrank;
	}

	public void setEmcetrank(String emcetrank) {
		this.emcetrank = emcetrank;
	}

	public Date getEnquiryDate() {
		return this.enquiryDate;
	}

	public void setEnquiryDate(Date enquiryDate) {
		this.enquiryDate = enquiryDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsClosedenquiry() {
		return this.isClosedenquiry;
	}

	public void setIsClosedenquiry(Boolean isClosedenquiry) {
		this.isClosedenquiry = isClosedenquiry;
	}

	public String getMobileNumber() {
		return this.mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getMobileNumber1() {
		return this.mobileNumber1;
	}

	public void setMobileNumber1(String mobileNumber1) {
		this.mobileNumber1 = mobileNumber1;
	}

	public String getMobileNumber2() {
		return this.mobileNumber2;
	}

	public void setMobileNumber2(String mobileNumber2) {
		this.mobileNumber2 = mobileNumber2;
	}

	public String getParentmobile() {
		return this.parentmobile;
	}

	public void setParentmobile(String parentmobile) {
		this.parentmobile = parentmobile;
	}

	public String getParentname() {
		return this.parentname;
	}

	public void setParentname(String parentname) {
		this.parentname = parentname;
	}

	public String getPercentage() {
		return this.percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getResultstatus() {
		return this.resultstatus;
	}

	public void setResultstatus(String resultstatus) {
		this.resultstatus = resultstatus;
	}

	public Date getReturnDate() {
		return this.returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public String getSourceofenquiry() {
		return this.sourceofenquiry;
	}

	public void setSourceofenquiry(String sourceofenquiry) {
		this.sourceofenquiry = sourceofenquiry;
	}

	public String getStudentName() {
		return this.studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Course getCourse() {
		return this.course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public District getDistrict() {
		return this.district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public GeneralDetail getEnquirystatus() {
		return this.enquirystatus;
	}

	public void setEnquirystatus(GeneralDetail enquirystatus) {
		this.enquirystatus = enquirystatus;
	}

	public GeneralDetail getModeofenquiry() {
		return this.modeofenquiry;
	}

	public void setModeofenquiry(GeneralDetail modeofenquiry) {
		this.modeofenquiry = modeofenquiry;
	}

	public GeneralDetail getKnowaboutus() {
		return this.knowaboutus;
	}

	public void setKnowaboutus(GeneralDetail knowaboutus) {
		this.knowaboutus = knowaboutus;
	}

	public GeneralDetail getGender() {
		return this.gender;
	}

	public void setGender(GeneralDetail gender) {
		this.gender = gender;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public QualificationGroup getQualificationGroup() {
		return this.qualificationGroup;
	}

	public void setQualificationGroup(QualificationGroup qualificationGroup) {
		this.qualificationGroup = qualificationGroup;
	}

	public Qualification getQualification() {
		return this.qualification;
	}

	public void setQualification(Qualification qualification) {
		this.qualification = qualification;
	}

	public String getEnquiryNo() {
		return enquiryNo;
	}

	public void setEnquiryNo(String enquiryNo) {
		this.enquiryNo = enquiryNo;
	}

}