package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_hr_employee_payslip_generation_status database table.
 * 
 */
@Entity
@Table(name="t_hr_employee_payslip_generation_status")
@NamedQuery(name="EmployeePayslipGenerationStatus.findAll", query="SELECT e FROM EmployeePayslipGenerationStatus e")
public class EmployeePayslipGenerationStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_payslip_gen_status_id")
	private Long payslipGenStatusId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="from_status")
	private String fromStatus;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	private String remarks;

	/*@Column(name="to_status")
	private String toStatus;*/

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeePayslipGeneration
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_payslip_generation_id")
	private EmployeePayslipGeneration employeePayslipGeneration;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	/*@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_payslip_status_from_catdet_id")
	private GeneralDetail payslipStatusFromCatdet;*/
	
	/*@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_payslip_status_to_catdet_id")
	private GeneralDetail payslipStatusToCatdet;*/
	
	public EmployeePayslipGenerationStatus() {
	}

	public Long getPayslipGenStatusId() {
		return this.payslipGenStatusId;
	}

	public void setPayslipGenStatusId(Long payslipGenStatusId) {
		this.payslipGenStatusId = payslipGenStatusId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	/*public String getFromStatus() {
		return this.fromStatus;
	}

	public void setFromStatus(String fromStatus) {
		this.fromStatus = fromStatus;
	}*/

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/*public String getToStatus() {
		return this.toStatus;
	}

	public void setToStatus(String toStatus) {
		this.toStatus = toStatus;
	}
*/
	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeePayslipGeneration getEmployeePayslipGeneration() {
		return this.employeePayslipGeneration;
	}

	public void setEmployeePayslipGeneration(EmployeePayslipGeneration employeePayslipGeneration) {
		this.employeePayslipGeneration = employeePayslipGeneration;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	/*public GeneralDetail getPayslipStatusFromCatdet() {
		return payslipStatusFromCatdet;
	}*/

	/*public GeneralDetail getPayslipStatusToCatdet() {
		return payslipStatusToCatdet;
	}*/
/*
	public void setPayslipStatusFromCatdet(GeneralDetail payslipStatusFromCatdet) {
		this.payslipStatusFromCatdet = payslipStatusFromCatdet;
	}*/

	/*public void setPayslipStatusToCatdet(GeneralDetail payslipStatusToCatdet) {
		this.payslipStatusToCatdet = payslipStatusToCatdet;
	}*/
	

}