package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the t_lib_books database table.
 * 
 */
@Entity
@Table(name = "t_stg_lib_book")
@NamedQuery(name = "StgBook.findAll", query = "SELECT b FROM StgBook b")
public class StgBook implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_stg_lib_book_td")
	private Long stgBookId;

	@Column(name = "barcode")
	private String barcode;

	@Column(name = "library_ref_prefix")
	private String libraryRefPrefix;
	
	@Column(name = "booknumber")
	private String booknumber;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "bookcode")
	private String bookcode;
	
	@Column(name = "tags")
	private String tags;
	
	@Column(name = "custom_tags")
	private String customTags;
	
	@Column(name = "noofcopies")
	private String noofcopies;
	
	@Column(name = "available_copies")
	private String availableCopies;
	
	@Column(name = "issued_copies")
	private String issuedCopies;
	
	@Column(name = "vol")
	private String vol;
	
	@Column(name = "noofpages")
	private String noofpages;
	
	@Column(name = "year")
	private String year;
	
	@Column(name = "isbn")
	private String isbn;
	
	@Column(name = "edition")
	private String edition;
	
	@Column(name = "publisher")
	private String publisher;
	
	@Column(name = "language")
	private String language;
	
	@Column(name = "book_image1")
	private String bookImage1;
	
	@Column(name = "book_image2")
	private String bookImage2;
	
	@Column(name = "book_image3")
	private String bookImage3;
	
	@Column(name = "library_ref_number")
	private String libraryRefNumber;
	
	@Column(name = "accessionno")
	private String accessionno;

	@Column(name = "book_position")
	private String bookPosition;
	
	@Column(name = "availability_status")
	private String availabilityStatus;
	
	@Column(name = "status_comments")
	private String statusComments;

	public Long getStgBookId() {
		return stgBookId;
	}

	public void setStgBookId(Long stgBookId) {
		this.stgBookId = stgBookId;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getLibraryRefPrefix() {
		return libraryRefPrefix;
	}

	public void setLibraryRefPrefix(String libraryRefPrefix) {
		this.libraryRefPrefix = libraryRefPrefix;
	}

	public String getBooknumber() {
		return booknumber;
	}

	public void setBooknumber(String booknumber) {
		this.booknumber = booknumber;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBookcode() {
		return bookcode;
	}

	public void setBookcode(String bookcode) {
		this.bookcode = bookcode;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getCustomTags() {
		return customTags;
	}

	public void setCustomTags(String customTags) {
		this.customTags = customTags;
	}

	public String getNoofcopies() {
		return noofcopies;
	}

	public void setNoofcopies(String noofcopies) {
		this.noofcopies = noofcopies;
	}

	public String getAvailableCopies() {
		return availableCopies;
	}

	public void setAvailableCopies(String availableCopies) {
		this.availableCopies = availableCopies;
	}

	public String getIssuedCopies() {
		return issuedCopies;
	}

	public void setIssuedCopies(String issuedCopies) {
		this.issuedCopies = issuedCopies;
	}

	public String getVol() {
		return vol;
	}

	public void setVol(String vol) {
		this.vol = vol;
	}

	public String getNoofpages() {
		return noofpages;
	}

	public void setNoofpages(String noofpages) {
		this.noofpages = noofpages;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getBookImage1() {
		return bookImage1;
	}

	public void setBookImage1(String bookImage1) {
		this.bookImage1 = bookImage1;
	}

	public String getBookImage2() {
		return bookImage2;
	}

	public void setBookImage2(String bookImage2) {
		this.bookImage2 = bookImage2;
	}

	public String getBookImage3() {
		return bookImage3;
	}

	public void setBookImage3(String bookImage3) {
		this.bookImage3 = bookImage3;
	}

	public String getLibraryRefNumber() {
		return libraryRefNumber;
	}

	public void setLibraryRefNumber(String libraryRefNumber) {
		this.libraryRefNumber = libraryRefNumber;
	}

	public String getAccessionno() {
		return accessionno;
	}

	public void setAccessionno(String accessionno) {
		this.accessionno = accessionno;
	}

	public String getBookPosition() {
		return bookPosition;
	}

	public void setBookPosition(String bookPosition) {
		this.bookPosition = bookPosition;
	}

	public String getAvailabilityStatus() {
		return availabilityStatus;
	}

	public void setAvailabilityStatus(String availabilityStatus) {
		this.availabilityStatus = availabilityStatus;
	}

	public String getStatusComments() {
		return statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	



}