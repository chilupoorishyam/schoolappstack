package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_dl_course_discussions database table.
 * 
 */
@Entity
@Table(name = "t_dl_course_discussions")
@NamedQuery(name = "CourseDiscussion.findAll", query = "SELECT c FROM CourseDiscussion c")
public class CourseDiscussion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_dl_member_topic_discussion_id")
	private Long memberTopicDiscussionId;

	private String comment;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_question")
	private Boolean isQuestion;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to CourseMember
	@ManyToOne
	@JoinColumn(name = "fk_dl_course_member_id")
	private CourseMember courseMember;

	// bi-directional many-to-one association to OnlineCourses
	@ManyToOne
	@JoinColumn(name = "fk_dl_onlinecourse_id")
	private OnlineCourses onlineCourses;

	// bi-directional many-to-one association to CourseLessonsTopic
	@ManyToOne
	@JoinColumn(name = "fk_dl_course_lesson_topic_id")
	private CourseLessonsTopic courseLessonsTopic;

	// bi-directional many-to-one association to TDlMember
	@ManyToOne
	@JoinColumn(name = "fk_dl_membership_id")
	private DigitalLibraryMember member;

	// bi-directional many-to-one association to CourseDiscussion
	@ManyToOne
	@JoinColumn(name = "fk_answer_for_topic_discussion_id")
	private CourseDiscussion courseDiscussion;

	// bi-directional many-to-one association to CourseDiscussion
	@OneToMany(mappedBy = "courseDiscussion" , cascade = CascadeType.ALL)
	private List<CourseDiscussion> courseDiscussions;

	public CourseDiscussion() {
	}

	public Long getMemberTopicDiscussionId() {
		return this.memberTopicDiscussionId;
	}

	public void setMemberTopicDiscussionId(Long memberTopicDiscussionId) {
		this.memberTopicDiscussionId = memberTopicDiscussionId;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsQuestion() {
		return this.isQuestion;
	}

	public void setIsQuestion(Boolean isQuestion) {
		this.isQuestion = isQuestion;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public CourseMember getCourseMember() {
		return courseMember;
	}

	public void setCourseMember(CourseMember courseMember) {
		this.courseMember = courseMember;
	}

	public OnlineCourses getOnlineCourses() {
		return onlineCourses;
	}

	public void setOnlineCourses(OnlineCourses onlineCourses) {
		this.onlineCourses = onlineCourses;
	}

	public CourseLessonsTopic getCourseLessonsTopic() {
		return courseLessonsTopic;
	}

	public void setCourseLessonsTopic(CourseLessonsTopic courseLessonsTopic) {
		this.courseLessonsTopic = courseLessonsTopic;
	}

	public DigitalLibraryMember getMember() {
		return member;
	}

	public void setMember(DigitalLibraryMember member) {
		this.member = member;
	}

	public CourseDiscussion getCourseDiscussion() {
		return courseDiscussion;
	}

	public void setCourseDiscussion(CourseDiscussion courseDiscussion) {
		this.courseDiscussion = courseDiscussion;
	}

	public List<CourseDiscussion> getCourseDiscussions() {
		return courseDiscussions;
	}

	public void setCourseDiscussions(List<CourseDiscussion> courseDiscussions) {
		this.courseDiscussions = courseDiscussions;
	}
}