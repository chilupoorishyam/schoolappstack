package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_exam_grouping database table.
 */
@Data
@Entity
@Table(name = "t_exam_grouping")
@NamedQuery(name = "ExamGrouping.findAll", query = "SELECT e FROM ExamGrouping e")
public class ExamGrouping implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_examgroup_id")
    private Long examgroupId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "groupname")
    private String groupname;

    @Column(name = "fk_examtype_ids")
    private String examtypeIds;

    @Column(name = "fk_exam_ids")
    private String examIds;

    @Column(name = "max_marks")
    private Integer maxMarks;

    @Column(name = "pass_marks")
    private Integer passMarks;

    @Column(name = "pass_percentage")
    private Integer passPercentage;

    @Column(name = "is_display_all_exams")
    private Boolean isDisplayAllExams;

    @Column(name = "examtypemarksf1")
    private String examtypemarksf1;

    @Column(name = "examtypemarksf2")
    private String examtypemarksf2;

    @Column(name = "examtypemarksf3")
    private String examtypemarksf3;

    @Column(name = "examtypemarksf4")
    private String examtypemarksf4;

    @Column(name = "examtypemarksf5")
    private String examtypemarksf5;

    @Column(name = "total_marks_formula")
    private String totalMarksFormula;

    // bi-directional many-to-one association to AcademicYear
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_school_id")
    private School school;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;
}