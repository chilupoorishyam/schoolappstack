package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the t_tm_distance_fee database table.
 * 
 */
@Entity
@Table(name="t_tm_distance_fee")
@NamedQuery(name="DistanceFee.findAll", query="SELECT d FROM DistanceFee d")
public class DistanceFee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_distance_fee_id")
	private Long distanceFeeId;

	private BigDecimal amount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="from_date")
	private Date fromDate;

	@Column(name="from_km")
	private Integer fromKm;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="to_date")
	private Date toDate;

	@Column(name="to_km")
	private Integer toKm;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_frequency_catdet_id")
	private GeneralDetail feeFrequency;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to TransportDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_transport_detail_id")
	private TransportDetail transportDetail;

	public DistanceFee() {
	}

	public Long getDistanceFeeId() {
		return this.distanceFeeId;
	}

	public void setDistanceFeeId(Long distanceFeeId) {
		this.distanceFeeId = distanceFeeId;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Integer getFromKm() {
		return this.fromKm;
	}

	public void setFromKm(Integer fromKm) {
		this.fromKm = fromKm;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Integer getToKm() {
		return this.toKm;
	}

	public void setToKm(Integer toKm) {
		this.toKm = toKm;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public GeneralDetail getFeeFrequency() {
		return this.feeFrequency;
	}

	public void setFeeFrequency(GeneralDetail feeFrequency) {
		this.feeFrequency = feeFrequency;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public TransportDetail getTransportDetail() {
		return this.transportDetail;
	}

	public void setTransportDetail(TransportDetail transportDetail) {
		this.transportDetail = transportDetail;
	}

}