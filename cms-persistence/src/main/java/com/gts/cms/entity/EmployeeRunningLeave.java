package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the t_hr_emp_running_leaves database table.
 * 
 */
@Entity
@Table(name = "t_hr_emp_running_leaves")
@NamedQuery(name = "EmployeeRunningLeave.findAll", query = "SELECT e FROM EmployeeRunningLeave e")
public class EmployeeRunningLeave implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_emp_running_leaves_id")
	private Long empRunningLeavesId;

	@Column(name = "balance_leaves")
	private BigDecimal balanceLeaves;

	@Column(name = "consumed_leaves")
	private BigDecimal consumedLeaves;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "fk_acquired_entitlement_id")
	private String acquiredEntitlementId;

	@Column(name = "fk_consumed_leave_application_ids")
	private String consumedLeaveApplicationIds;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Column(name = "total_leaves")
	private BigDecimal totalLeaves;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "expired_leaves")
	private BigDecimal expiredLeaves;

	@Column(name = "encash_Leaves")
	private BigDecimal encashLeaves;

	@Column(name = "comments")
	private String comments;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne
	@JoinColumn(name = "fk_emp_id")
	private EmployeeDetail employeeDetail;

	// bi-directional many-to-one association to LeaveType
	@ManyToOne
	@JoinColumn(name = "fk_leave_type_id")
	private LeaveType leaveType;

	// bi-directional many-to-one association to AcademicYear
	@ManyToOne
	@JoinColumn(name = "fk_academic_year_id")
	private AcademicYear academicYear;

	// bi-directional many-to-one association to School
	@ManyToOne
	@JoinColumn(name = "fk_school_id")
	private School school;
	
	@Column(name="leave_year")
	private Integer leaveYear;

	public Integer getLeaveYear() {
		return leaveYear;
	}

	public void setLeaveYear(Integer leaveYear) {
		this.leaveYear = leaveYear;
	}

	public EmployeeRunningLeave() {
	}

	public Long getEmpRunningLeavesId() {
		return this.empRunningLeavesId;
	}

	public void setEmpRunningLeavesId(Long empRunningLeavesId) {
		this.empRunningLeavesId = empRunningLeavesId;
	}

	
	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getAcquiredEntitlementId() {
		return this.acquiredEntitlementId;
	}

	public void setAcquiredEntitlementId(String acquiredEntitlementId) {
		this.acquiredEntitlementId = acquiredEntitlementId;
	}

	public String getConsumedLeaveApplicationIds() {
		return this.consumedLeaveApplicationIds;
	}

	public void setConsumedLeaveApplicationIds(String consumedLeaveApplicationIds) {
		this.consumedLeaveApplicationIds = consumedLeaveApplicationIds;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getEmployeeDetail() {
		return this.employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public LeaveType getLeaveType() {
		return this.leaveType;
	}

	public void setLeaveType(LeaveType leaveType) {
		this.leaveType = leaveType;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}


	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public BigDecimal getBalanceLeaves() {
		return balanceLeaves;
	}

	public void setBalanceLeaves(BigDecimal balanceLeaves) {
		this.balanceLeaves = balanceLeaves;
	}

	public BigDecimal getConsumedLeaves() {
		return consumedLeaves;
	}

	public void setConsumedLeaves(BigDecimal consumedLeaves) {
		this.consumedLeaves = consumedLeaves;
	}

	public BigDecimal getTotalLeaves() {
		return totalLeaves;
	}

	public void setTotalLeaves(BigDecimal totalLeaves) {
		this.totalLeaves = totalLeaves;
	}

	public BigDecimal getExpiredLeaves() {
		return expiredLeaves;
	}

	public void setExpiredLeaves(BigDecimal expiredLeaves) {
		this.expiredLeaves = expiredLeaves;
	}

	public BigDecimal getEncashLeaves() {
		return encashLeaves;
	}

	public void setEncashLeaves(BigDecimal encashLeaves) {
		this.encashLeaves = encashLeaves;
	}

}