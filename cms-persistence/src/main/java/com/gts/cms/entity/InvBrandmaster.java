package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_inv_brandmaster database table.
 * 
 */
@Entity
@Table(name = "t_inv_brandmaster")
@NamedQuery(name = "InvBrandmaster.findAll", query = "SELECT i FROM InvBrandmaster i")
public class InvBrandmaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_brandmaster_id")
	private Long brandmasterId;

	@Column(name = "brand_code")
	private String brandCode;

	@Column(name = "brand_name")
	private String brandName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

/*	// bi-directional many-to-one association to InvItemmaster
	@OneToMany(mappedBy = "InvBrandmaster")
	private List<InvItemmaster> invItemmasters;*/

	public InvBrandmaster() {
	}

	public Long getBrandmasterId() {
		return this.brandmasterId;
	}

	public void setBrandmasterId(Long brandmasterId) {
		this.brandmasterId = brandmasterId;
	}

	public String getBrandCode() {
		return this.brandCode;
	}

	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}

	public String getBrandName() {
		return this.brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

/*	public List<InvItemmaster> getInvItemmasters() {
		return invItemmasters;
	}

	public void setInvItemmasters(List<InvItemmaster> invItemmasters) {
		this.invItemmasters = invItemmasters;
	}*/

}