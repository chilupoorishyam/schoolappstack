package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_exam_fcar_student_sub_marks database table.
 */
@Entity
@Table(name = "t_exam_fcar_student_sub_marks")
@NamedQuery(name = "ExamFCARStudentSubMark.findAll", query = "SELECT e FROM ExamFCARStudentSubMark e")
public class ExamFCARStudentSubMark implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_exam_std_sub_mark_id")
    private Long examStdSubMarkId;

    @ManyToOne
    @JoinColumn(name = "fk_exam_fcar_sub_syllabus_id")
    private ExamFCARSubjectSyllabus examFCARSubjectSyllabus;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Temporal(TemporalType.DATE)
    @Column(name = "exam_date")
    private Date examDate;

    @ManyToOne
    @JoinColumn(name = "fk_school_id")
    private School school;

    @ManyToOne
    @JoinColumn(name = "fk_course_year_id")
    private CourseYear courseYear;

    @ManyToOne
    @JoinColumn(name = "fk_exam_id")
    private ExamMaster examMaster;

    @ManyToOne
    @JoinColumn(name = "fk_exam_timetable_det_id")
    private ExamTimetableDetail examTimetableDet;

    @ManyToOne
    @JoinColumn(name = "fk_exam_timetable_id")
    private ExamTimetable examTimetable;

    @ManyToOne
    @JoinColumn(name = "fk_reviewer_emp_id")
    private EmployeeDetail reviewerEmp;

    @ManyToOne
    @JoinColumn(name = "fk_student_id")
    private StudentDetail student;

    @ManyToOne
    @JoinColumn(name = "fk_subject_id")
    private Subject subject;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "is_present")
    private Boolean isPresent;

    @Column(name = "is_attempted")
    private Boolean isAttempted;

    private Integer marks;

    private String reason;

    @Column(name = "review_comments")
    private String reviewComments;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "reviewed_on")
    private Date reviewedOn;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    // bi-directional many-to-one association to ExamFCARSetupDetail
    @ManyToOne
    @JoinColumn(name = "fk_exam_fcar_set_det_id")
    private ExamFCARSetupDetail examFcarSetupDetail;

    public ExamFCARStudentSubMark() {
    }

    public Long getExamStdSubMarkId() {
        return this.examStdSubMarkId;
    }

    public void setExamStdSubMarkId(Long examStdSubMarkId) {
        this.examStdSubMarkId = examStdSubMarkId;
    }

    public ExamFCARSubjectSyllabus getExamFCARSubjectSyllabus() {
        return examFCARSubjectSyllabus;
    }

    public void setExamFCARSubjectSyllabus(ExamFCARSubjectSyllabus examFCARSubjectSyllabus) {
        this.examFCARSubjectSyllabus = examFCARSubjectSyllabus;
    }

    public Date getCreatedDt() {
        return this.createdDt;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    public Long getCreatedUser() {
        return this.createdUser;
    }

    public void setCreatedUser(Long createdUser) {
        this.createdUser = createdUser;
    }

    public Date getExamDate() {
        return this.examDate;
    }

    public void setExamDate(Date examDate) {
        this.examDate = examDate;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public CourseYear getCourseYear() {
        return courseYear;
    }

    public void setCourseYear(CourseYear courseYear) {
        this.courseYear = courseYear;
    }

    public ExamMaster getExamMaster() {
        return examMaster;
    }

    public void setExamMaster(ExamMaster examMaster) {
        this.examMaster = examMaster;
    }

    public ExamTimetableDetail getExamTimetableDet() {
        return examTimetableDet;
    }

    public void setExamTimetableDet(ExamTimetableDetail examTimetableDet) {
        this.examTimetableDet = examTimetableDet;
    }

    public ExamTimetable getExamTimetable() {
        return examTimetable;
    }

    public void setExamTimetable(ExamTimetable examTimetable) {
        this.examTimetable = examTimetable;
    }

    public EmployeeDetail getReviewerEmp() {
        return reviewerEmp;
    }

    public void setReviewerEmp(EmployeeDetail reviewerEmp) {
        this.reviewerEmp = reviewerEmp;
    }

    public StudentDetail getStudent() {
        return student;
    }

    public void setStudent(StudentDetail student) {
        this.student = student;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }


    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsPresent() {
        return this.isPresent;
    }

    public void setIsPresent(Boolean isPresent) {
        this.isPresent = isPresent;
    }

    public Integer getMarks() {
        return this.marks;
    }

    public void setMarks(Integer marks) {
        this.marks = marks;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReviewComments() {
        return this.reviewComments;
    }

    public void setReviewComments(String reviewComments) {
        this.reviewComments = reviewComments;
    }

    public Date getReviewedOn() {
        return this.reviewedOn;
    }

    public void setReviewedOn(Date reviewedOn) {
        this.reviewedOn = reviewedOn;
    }

    public Date getUpdatedDt() {
        return this.updatedDt;
    }

    public void setUpdatedDt(Date updatedDt) {
        this.updatedDt = updatedDt;
    }

    public Long getUpdatedUser() {
        return this.updatedUser;
    }

    public void setUpdatedUser(Long updatedUser) {
        this.updatedUser = updatedUser;
    }

    public ExamFCARSetupDetail getExamFcarSetupDetail() {
        return examFcarSetupDetail;
    }

    public void setExamFcarSetupDetail(ExamFCARSetupDetail examFcarSetupDetail) {
        this.examFcarSetupDetail = examFcarSetupDetail;
    }

    public Boolean getAttempted() {
        return isAttempted;
    }

    public void setAttempted(Boolean attempted) {
        isAttempted = attempted;
    }
}