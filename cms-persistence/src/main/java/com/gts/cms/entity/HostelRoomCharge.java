package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_hstl_room_charges database table.
 * 
 */
@Entity
@Table(name = "t_hstl_room_charges")
@NamedQuery(name = "HostelRoomCharge.findAll", query = "SELECT h FROM HostelRoomCharge h")
public class HostelRoomCharge implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_hstl_room_charges_id")
	private Long hstlRoomChargesId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "from_date")
	private Date fromDate;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "to_date")
	private Date toDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_payment_frequency_catdet_id")
	private GeneralDetail paymentFrequencyCatdet;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_room_type_catdet_id")
	private GeneralDetail roomTypeCatdet;

	// bi-directional many-to-one association to HostelDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_hstl_hostel_id")
	private HostelDetail hostelDetail;
	
	private BigDecimal amount;

	public HostelRoomCharge() {
	}

	public Long getHstlRoomChargesId() {
		return hstlRoomChargesId;
	}

	public void setHstlRoomChargesId(Long hstlRoomChargesId) {
		this.hstlRoomChargesId = hstlRoomChargesId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public GeneralDetail getPaymentFrequencyCatdet() {
		return paymentFrequencyCatdet;
	}

	public void setPaymentFrequencyCatdet(GeneralDetail paymentFrequencyCatdet) {
		this.paymentFrequencyCatdet = paymentFrequencyCatdet;
	}

	public GeneralDetail getRoomTypeCatdet() {
		return roomTypeCatdet;
	}

	public void setRoomTypeCatdet(GeneralDetail roomTypeCatdet) {
		this.roomTypeCatdet = roomTypeCatdet;
	}

	public HostelDetail getHostelDetail() {
		return hostelDetail;
	}

	public void setHostelDetail(HostelDetail hostelDetail) {
		this.hostelDetail = hostelDetail;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	
	
}