package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_fcm_user_group database table.
 * 
 */
@Entity
@Table(name = "t_fcm_user_group")
@NamedQuery(name = "FCMUserGroup.findAll", query = "SELECT f FROM FCMUserGroup f")
public class FCMUserGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_fcm_user_group_id")
	private String fcmUserGroupId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_academic_year_id")
	private AcademicYear academicYear;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_fcm_group_id")
	private FCMGroup fcmGroup;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_fcm_user_token_id")
	private FCMUserToken fcmUserToken;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private long updatedUser;

	public FCMUserGroup() {
	}

	public String getFcmUserGroupId() {
		return fcmUserGroupId;
	}

	public void setFcmUserGroupId(String fcmUserGroupId) {
		this.fcmUserGroupId = fcmUserGroupId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public FCMGroup getFcmGroup() {
		return fcmGroup;
	}

	public void setFcmGroup(FCMGroup fcmGroup) {
		this.fcmGroup = fcmGroup;
	}

	public FCMUserToken getFcmUserToken() {
		return fcmUserToken;
	}

	public void setFcmUserToken(FCMUserToken fcmUserToken) {
		this.fcmUserToken = fcmUserToken;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(long updatedUser) {
		this.updatedUser = updatedUser;
	}
}