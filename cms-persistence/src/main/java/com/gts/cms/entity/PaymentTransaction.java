package com.gts.cms.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_payment_transaction database table.
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "t_payment_transaction")
public class PaymentTransaction implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "t_payment_transaction_id")
    private Long paymentTransactionId;

    @Column(name = "payment_gateway_type")
    private String paymentGatewayType;

    @Column(name = "payment_status")
    private String paymentStatus;

    @Column(name = "tracking_id")
    private String trackingId;

    @Column(name = "bank_ref_no")
    private String bankRefNo;

    @Column(name = "order_status")
    private String orderStatus;

    @Column(name = "payment_mode")
    private String paymentMode;

    @Column(name = "card_name")
    private String cardName;

    @Column(name = "status_code")
    private String statusCode;

    @Column(name = "status_message")
    private String statusMessage;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "retry")
    private String retry;

    @Column(name = "response_code")
    private String responseCode;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trans_date")
    private Date transDate;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.DATE)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    //bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_school_id")
    private School school;
}