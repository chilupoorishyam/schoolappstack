package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_tm_helpers database table.
 * 
 */
@Entity
@Table(name="t_tm_helpers")
@NamedQuery(name="Helper.findAll", query="SELECT h FROM Helper h")
public class Helper implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_helper_id")
	private Long helperId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="date_of_birth")
	private Date dateOfBirth;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="date_of_joining")
	private Date dateOfJoining;

	@Column(name="email_id")
	private String emailId;

	private BigDecimal experience;

	@Column(name="helper_name")
	private String helperName;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="mobile_number")
	private String mobileNumber;

	@Column(name="permanent_address")
	private String permanentAddress;

	private String phone;

	@Column(name="present_address")
	private String presentAddress;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_gender_catdet_id")
	private GeneralDetail gender;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_marital_status_catdet_id")
	private GeneralDetail maritalStatus;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_bloodgroup_catdet_id")
	private GeneralDetail bloodgroup;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to TransportDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_transport_detail_id")
	private TransportDetail transportDetail;

	//bi-directional many-to-one association to VechicleRoute
	@OneToMany(mappedBy="helper",fetch = FetchType.LAZY)
	private List<VechicleRoute> vechicleRoutes;

	//bi-directional many-to-one association to VehicleDriver
	@OneToMany(mappedBy="helper",fetch = FetchType.LAZY)
	private List<VehicleDriver> vehicleDrivers;

	public Helper() {
	}

	public Long getHelperId() {
		return this.helperId;
	}

	public void setHelperId(Long helperId) {
		this.helperId = helperId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Date getDateOfJoining() {
		return this.dateOfJoining;
	}

	public void setDateOfJoining(Date dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}

	public String getEmailId() {
		return this.emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public BigDecimal getExperience() {
		return this.experience;
	}

	public void setExperience(BigDecimal experience) {
		this.experience = experience;
	}

	public String getHelperName() {
		return this.helperName;
	}

	public void setHelperName(String helperName) {
		this.helperName = helperName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getMobileNumber() {
		return this.mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPermanentAddress() {
		return this.permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPresentAddress() {
		return this.presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public GeneralDetail getGender() {
		return this.gender;
	}

	public void setGender(GeneralDetail gender) {
		this.gender = gender;
	}

	public GeneralDetail getMaritalStatus() {
		return this.maritalStatus;
	}

	public void setMaritalStatus(GeneralDetail maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public GeneralDetail getBloodgroup() {
		return this.bloodgroup;
	}

	public void setBloodgroup(GeneralDetail bloodgroup) {
		this.bloodgroup = bloodgroup;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public TransportDetail getTransportDetail() {
		return this.transportDetail;
	}

	public void setTransportDetail(TransportDetail transportDetail) {
		this.transportDetail = transportDetail;
	}

	public List<VechicleRoute> getVechicleRoutes() {
		return this.vechicleRoutes;
	}

	public void setVechicleRoutes(List<VechicleRoute> vechicleRoutes) {
		this.vechicleRoutes = vechicleRoutes;
	}

	public VechicleRoute addVechicleRoute(VechicleRoute vechicleRoute) {
		getVechicleRoutes().add(vechicleRoute);
		vechicleRoute.setHelper(this);

		return vechicleRoute;
	}

	public VechicleRoute removeVechicleRoute(VechicleRoute vechicleRoute) {
		getVechicleRoutes().remove(vechicleRoute);
		vechicleRoute.setHelper(null);

		return vechicleRoute;
	}

	public List<VehicleDriver> getVehicleDrivers() {
		return this.vehicleDrivers;
	}

	public void setVehicleDrivers(List<VehicleDriver> vehicleDrivers) {
		this.vehicleDrivers = vehicleDrivers;
	}

	public VehicleDriver addVehicleDriver(VehicleDriver vehicleDriver) {
		getVehicleDrivers().add(vehicleDriver);
		vehicleDriver.setHelper(this);

		return vehicleDriver;
	}

	public VehicleDriver removeVehicleDriver(VehicleDriver vehicleDriver) {
		getVehicleDrivers().remove(vehicleDriver);
		vehicleDriver.setHelper(null);

		return vehicleDriver;
	}

}