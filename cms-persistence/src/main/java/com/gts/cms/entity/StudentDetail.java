package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_std_student_details database table.
 * 
 */
@Entity
@Table(name = "t_std_student_details")
@NamedQuery(name = "StudentDetail.findAll", query = "SELECT s FROM StudentDetail s")
public class StudentDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_student_id")
	private Long studentId;

	@Column(name = "aadhar_card_no")
	private String aadharCardNo;

	@Column(name = "aadhar_file_path")
	private String aadharFilePath;

	@Temporal(TemporalType.DATE)
	@Column(name = "adminssion_date")
	private Date adminssionDate;

	@Column(name = "admission_number")
	private String admissionNumber;

	@Column(name = "application_no")
	private String applicationNo;

	@Column(name = "biometric_code")
	private String biometricCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	@Column(name = "date_of_birth")
	private Date dateOfBirth;

	@Temporal(TemporalType.DATE)
	@Column(name = "date_of_expiry")
	private Date dateOfExpiry;

	@Temporal(TemporalType.DATE)
	@Column(name = "date_of_issue")
	private Date dateOfIssue;

	@Temporal(TemporalType.DATE)
	@Column(name = "date_of_registration")
	private Date dateOfRegistration;

	@Column(name = "eamcet_rank")
	private String eamcetRank;

	@Column(name = "entrance_ht_no")
	private String entranceHtNo;

	@Column(name = "father_address")
	private String fatherAddress;

	@Column(name = "father_email_id")
	private String fatherEmailId;

	@Column(name = "father_mobile_no")
	private String fatherMobileNo;

	@Column(name = "father_name")
	private String fatherName;

	@Column(name = "father_occupation")
	private String fatherOccupation;

	@Column(name = "father_photo_path")
	private String fatherPhotoPath;

	@Column(name = "father_qualification")
	private String fatherQualification;

	@Column(name = "fathers_income_pa")
	private String fathersIncomePa;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "folder_path")
	private String folderPath;

	@Column(name = "guardian_address")
	private String guardianAddress;

	@Column(name = "guardian_email_id")
	private String guardianEmailId;

	@Column(name = "guardian_income_pa")
	private String guardianIncomePa;

	@Column(name = "guardian_mobile_no")
	private String guardianMobileNo;

	@Column(name = "guardian_name")
	private String guardianName;

	@Column(name = "hallticket_number")
	private String hallticketNumber;

	private String hobbies;

	@Column(name = "identification_marks")
	private String identificationMarks;

	private String interests;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_current_year")
	private Boolean isCurrentYear;

	@Column(name = "is_lateral")
	private Boolean isLateral;

	@Column(name = "is_minority")
	private Boolean isMinority;

	@Column(name = "is_local")
	private Boolean isLocal;

	@Column(name = "is_scholorship")
	private Boolean isScholorship;

	@Column(name = "isgovtemp_father")
	private Boolean isgovtempFather;

	@Column(name = "isgovtemp_mother")
	private Boolean isgovtempMother;

	@Column(name = "isgovtemp_spouse")
	private Boolean isgovtempSpouse;

	@Column(name = "lang_status1")
	private String langStatus1;

	@Column(name = "lang_status2")
	private String langStatus2;

	@Column(name = "lang_status3")
	private String langStatus3;

	@Column(name = "lang_status4")
	private String langStatus4;

	@Column(name = "lang_status5")
	private String langStatus5;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "middle_name")
	private String middleName;

	private String mobile;

	@Column(name = "mother_address")
	private String motherAddress;

	@Column(name = "mother_email_id")
	private String motherEmailId;

	@Column(name = "mother_income_pa")
	private String motherIncomePa;

	@Column(name = "mother_mobile_no")
	private String motherMobileNo;

	@Column(name = "mother_name")
	private String motherName;

	@Column(name = "mother_occupation")
	private String motherOccupation;

	@Column(name = "mother_photo_path")
	private String motherPhotoPath;

	@Column(name = "mother_qualification")
	private String motherQualification;

	@Column(name = "pancard_file_path")
	private String pancardFilePath;

	@Column(name = "pancard_no")
	private String pancardNo;

	@Column(name = "passport_no")
	private String passportNo;

	@Column(name = "permanent_address")
	private String permanentAddress;

	@Column(name = "permanent_pincode")
	private String permanentPincode;

	@Column(name = "permanent_street")
	private String permanentStreet;

	@Column(name = "premanent_mandal")
	private String premanentMandal;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_title_catdet_id")
	private GeneralDetail title;

	@Column(name = "present_address")
	private String presentAddress;

	@Column(name = "present_mandal")
	private String presentMandal;

	@Column(name = "present_pincode")
	private String presentPincode;

	@Column(name = "present_street")
	private String presentStreet;

	@Column(name = "primary_contact")
	private String primaryContact;

	private String reason;

	@Column(name = "receipt_no")
	private String receiptNo;

	@Column(name = "ref_application_no")
	private String refApplicationNo;

	@Column(name = "residence_phone")
	private String residencePhone;

	private String rfid;

	@Column(name = "roll_number")
	private String rollNumber;

	@Column(name = "spouse_address")
	private String spouseAddress;

	@Column(name = "spouse_email_id")
	private String spouseEmailId;

	@Column(name = "spouse_income_pa")
	private String spouseIncomePa;

	@Column(name = "spouse_mobile_no")
	private String spouseMobileNo;

	@Column(name = "spouse_name")
	private String spouseName;

	@Column(name = "spouse_occupation")
	private String spouseOccupation;

	@Column(name = "spouse_photo_path")
	private String spousePhotoPath;

	@Column(name = "spouse_qualification")
	private String spouseQualification;

	@Column(name = "ssc_no")
	private String sscNo;

	@Column(name = "std_email_id")
	private String stdEmailId;

	@Column(name = "student_photo_path")
	private String studentPhotoPath;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Temporal(TemporalType.DATE)
	@Column(name = "wedding_date")
	private Date weddingDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_group_section_id")
	private GroupSection groupSection;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_qualifying_catdet_id")
	private GeneralDetail qualifying;

	// bi-directional many-to-one association to BatchwiseStudent
	@OneToMany(mappedBy = "studentDetail", fetch = FetchType.LAZY)
	private List<BatchwiseStudent> batchwiseStudents;

	// bi-directional many-to-one association to FeeInstantPayment
	@OneToMany(mappedBy = "studentDetail", fetch = FetchType.LAZY)
	private List<FeeInstantPayment> feeInstantPayments;

	// bi-directional many-to-one association to FeeParticularwisePayment
	@OneToMany(mappedBy = "studentDetail", fetch = FetchType.LAZY)
	private List<FeeParticularwisePayment> feeParticularwisePayments;

	// bi-directional many-to-one association to FeeReceipt
	@OneToMany(mappedBy = "studentDetail", fetch = FetchType.LAZY)
	private List<FeeReceipt> feeReceipts;

	// bi-directional many-to-one association to FeeStudentData
	@OneToMany(mappedBy = "studentDetail", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<FeeStudentData> feeStudentData;

	// bi-directional many-to-one association to FeeStudentRefund
	@OneToMany(mappedBy = "studentDetail", fetch = FetchType.LAZY)
	private List<FeeStudentRefund> feeStudentRefunds;

	// bi-directional many-to-one association to FeeStudentWiseParticular
	@OneToMany(mappedBy = "studentDetail", fetch = FetchType.LAZY)
	private List<FeeStudentWiseParticular> feeStudentWiseParticulars;

	// bi-directional many-to-one association to FeeStudentwiseDiscount
	@OneToMany(mappedBy = "studentDetail", fetch = FetchType.LAZY)
	private List<FeeStudentwiseDiscount> feeStudentwiseDiscounts;

	// bi-directional many-to-one association to FeeStudentwiseFine
	@OneToMany(mappedBy = "studentDetail", fetch = FetchType.LAZY)
	private List<FeeStudentwiseFine> feeStudentwiseFines;

	// bi-directional many-to-one association to FeeTransactionMaster
	@OneToMany(mappedBy = "studentDetail", fetch = FetchType.LAZY)
	private List<FeeTransactionMaster> feeTransactionMasters;

	// bi-directional many-to-one association to SurveyFeedback
	@OneToMany(mappedBy = "fromStudentDetail", fetch = FetchType.LAZY)
	private List<SurveyFeedback> surveyFeedbacks1;

	// bi-directional many-to-one association to SurveyFeedback
	@OneToMany(mappedBy = "forStudentDetail", fetch = FetchType.LAZY)
	private List<SurveyFeedback> surveyFeedbacks2;

	// bi-directional many-to-one association to MessagingRecipient
	@OneToMany(mappedBy = "studentDetail", fetch = FetchType.LAZY)
	private List<MessagingRecipient> messagingRecipients;

	// bi-directional many-to-one association to StudentActivitiesDetail
	@OneToMany(mappedBy = "studentDetail", cascade = CascadeType.ALL)
	private List<StudentActivitiesDetail> activitiesDetails;

	// bi-directional many-to-one association to StudentDailylog
	@OneToMany(mappedBy = "studentDetail", fetch = FetchType.LAZY)
	private List<StudentDailylog> stdDailylogs;

	// bi-directional many-to-one association to StudentDocumentCollection
	@OneToMany(mappedBy = "studentDetail", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<StudentDocumentCollection> stdDocumentCollections;

	// bi-directional many-to-one association to StudentEducationDetail
	@OneToMany(mappedBy = "studentDetail", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<StudentEducationDetail> stdEducationDetails;

	// bi-directional many-to-one association to StudentAcademicbatch
	@OneToMany(mappedBy = "studentDetail", fetch = FetchType.LAZY)
	private List<StudentAcademicbatch> studentAcademicbatches;

	// bi-directional many-to-one association to Batch
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_batch_id")
	private Batch batch;

	// bi-directional many-to-one association to Caste
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_caste_id")
	private Caste caste;

	// bi-directional many-to-one association to City
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_city_present_id")
	private City presentCity;

	// bi-directional many-to-one association to City
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_city_premenent_id")
	private City premenentCity;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to Course
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_id")
	private Course course;



	// bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_year_id")
	private CourseYear courseYear;

	// bi-directional many-to-one association to District
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_district_present_id")
	private District presentDistrict;

	// bi-directional many-to-one association to District
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_district_permanent_id")
	private District permanentDistrict;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_disability_catdet_id")
	private GeneralDetail disability;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_nationality_catdet_id")
	private GeneralDetail nationality;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_religion_catdet_id")
	private GeneralDetail religion;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_bloodgroup_catdet_id")
	private GeneralDetail bloodgroup;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_maritalstatus_catdet_id")
	private GeneralDetail maritalstatus;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_language1_catdet_id")
	private GeneralDetail language1;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_language2_catdet_id")
	private GeneralDetail language2;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_language3_catdet_id")
	private GeneralDetail language3;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_language4_catdet_id")
	private GeneralDetail language4;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_language5_catdet_id")
	private GeneralDetail language5;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_quota_catdet_id")
	private GeneralDetail quota;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_stdtype_catdet_id")
	private GeneralDetail studentType;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_gender_catdet_id")
	private GeneralDetail gender;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_status_catdet_id")
	private GeneralDetail studentStatus;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;


	// bi-directional many-to-one association to SubCaste
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_sub_caste_id")
	private SubCaste subCaste;

	// bi-directional many-to-one association to User
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_parent_user_id")
	private User parentUser;

	// bi-directional many-to-one association to User
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_user_id")
	private User user;

	// bi-directional many-to-one association to StudentApplication
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_app_id")
	private StudentApplication studentApplication;

	// bi-directional many-to-one association to StudentSubject
	@OneToMany(mappedBy = "studentDetail", fetch = FetchType.LAZY)
	private List<StudentSubject> studentSubjects;

	// bi-directional many-to-one association to FeePayment
	@OneToMany(mappedBy = "studentDetail", fetch = FetchType.LAZY)
	private List<TransportFeePayment> feePayments;

	// bi-directional many-to-one association to TransportAllocation
	@OneToMany(mappedBy = "studentDetail", fetch = FetchType.LAZY)
	private List<TransportAllocation> transportAllocations;

	// bi-directional many-to-one association to StudentAttendance
	@OneToMany(mappedBy = "studentDetail", fetch = FetchType.LAZY)
	private List<StudentAttendance> stdAttendances;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_academic_year_id")
	private AcademicYear academicYear;

	public GeneralDetail getTitle() {
		return title;
	}

	public void setTitle(GeneralDetail title) {
		this.title = title;
	}

	public StudentDetail() {
	}

	public Long getStudentId() {
		return this.studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getAadharCardNo() {
		return this.aadharCardNo;
	}

	public void setAadharCardNo(String aadharCardNo) {
		this.aadharCardNo = aadharCardNo;
	}

	public String getAadharFilePath() {
		return this.aadharFilePath;
	}

	public void setAadharFilePath(String aadharFilePath) {
		this.aadharFilePath = aadharFilePath;
	}

	public Date getAdminssionDate() {
		return this.adminssionDate;
	}

	public void setAdminssionDate(Date adminssionDate) {
		this.adminssionDate = adminssionDate;
	}

	public String getAdmissionNumber() {
		return this.admissionNumber;
	}

	public void setAdmissionNumber(String admissionNumber) {
		this.admissionNumber = admissionNumber;
	}

	public String getApplicationNo() {
		return this.applicationNo;
	}

	public void setApplicationNo(String applicationNo) {
		this.applicationNo = applicationNo;
	}

	public String getBiometricCode() {
		return this.biometricCode;
	}

	public void setBiometricCode(String biometricCode) {
		this.biometricCode = biometricCode;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Date getDateOfExpiry() {
		return this.dateOfExpiry;
	}

	public void setDateOfExpiry(Date dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public Date getDateOfIssue() {
		return this.dateOfIssue;
	}

	public void setDateOfIssue(Date dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public Date getDateOfRegistration() {
		return this.dateOfRegistration;
	}

	public void setDateOfRegistration(Date dateOfRegistration) {
		this.dateOfRegistration = dateOfRegistration;
	}

	public String getEamcetRank() {
		return this.eamcetRank;
	}

	public void setEamcetRank(String eamcetRank) {
		this.eamcetRank = eamcetRank;
	}

	public String getEntranceHtNo() {
		return this.entranceHtNo;
	}

	public void setEntranceHtNo(String entranceHtNo) {
		this.entranceHtNo = entranceHtNo;
	}

	public String getFatherAddress() {
		return this.fatherAddress;
	}

	public void setFatherAddress(String fatherAddress) {
		this.fatherAddress = fatherAddress;
	}

	public String getFatherEmailId() {
		return this.fatherEmailId;
	}

	public void setFatherEmailId(String fatherEmailId) {
		this.fatherEmailId = fatherEmailId;
	}

	public String getFatherMobileNo() {
		return this.fatherMobileNo;
	}

	public void setFatherMobileNo(String fatherMobileNo) {
		this.fatherMobileNo = fatherMobileNo;
	}

	public String getFatherName() {
		return this.fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getFatherOccupation() {
		return this.fatherOccupation;
	}

	public void setFatherOccupation(String fatherOccupation) {
		this.fatherOccupation = fatherOccupation;
	}

	public String getFatherPhotoPath() {
		return this.fatherPhotoPath;
	}

	public void setFatherPhotoPath(String fatherPhotoPath) {
		this.fatherPhotoPath = fatherPhotoPath;
	}

	public String getFatherQualification() {
		return this.fatherQualification;
	}

	public void setFatherQualification(String fatherQualification) {
		this.fatherQualification = fatherQualification;
	}

	public String getFathersIncomePa() {
		return this.fathersIncomePa;
	}

	public void setFathersIncomePa(String fathersIncomePa) {
		this.fathersIncomePa = fathersIncomePa;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFolderPath() {
		return this.folderPath;
	}

	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}

	public String getGuardianAddress() {
		return this.guardianAddress;
	}

	public void setGuardianAddress(String guardianAddress) {
		this.guardianAddress = guardianAddress;
	}

	public String getGuardianEmailId() {
		return this.guardianEmailId;
	}

	public void setGuardianEmailId(String guardianEmailId) {
		this.guardianEmailId = guardianEmailId;
	}

	public String getGuardianIncomePa() {
		return this.guardianIncomePa;
	}

	public void setGuardianIncomePa(String guardianIncomePa) {
		this.guardianIncomePa = guardianIncomePa;
	}

	public String getGuardianMobileNo() {
		return this.guardianMobileNo;
	}

	public void setGuardianMobileNo(String guardianMobileNo) {
		this.guardianMobileNo = guardianMobileNo;
	}

	public String getGuardianName() {
		return this.guardianName;
	}

	public void setGuardianName(String guardianName) {
		this.guardianName = guardianName;
	}

	public String getHallticketNumber() {
		return this.hallticketNumber;
	}

	public void setHallticketNumber(String hallticketNumber) {
		this.hallticketNumber = hallticketNumber;
	}

	public String getHobbies() {
		return this.hobbies;
	}

	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}

	public String getIdentificationMarks() {
		return this.identificationMarks;
	}

	public void setIdentificationMarks(String identificationMarks) {
		this.identificationMarks = identificationMarks;
	}

	public String getInterests() {
		return this.interests;
	}

	public void setInterests(String interests) {
		this.interests = interests;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsCurrentYear() {
		return this.isCurrentYear;
	}

	public void setIsCurrentYear(Boolean isCurrentYear) {
		this.isCurrentYear = isCurrentYear;
	}

	public Boolean getIsLateral() {
		return this.isLateral;
	}

	public void setIsLateral(Boolean isLateral) {
		this.isLateral = isLateral;
	}

	public Boolean getIsLocal() {
		return this.isLocal;
	}

	public void setIsLocal(Boolean isLocal) {
		this.isLocal = isLocal;
	}

	public Boolean getIsScholorship() {
		return this.isScholorship;
	}

	public void setIsScholorship(Boolean isScholorship) {
		this.isScholorship = isScholorship;
	}

	public Boolean getIsgovtempFather() {
		return this.isgovtempFather;
	}

	public void setIsgovtempFather(Boolean isgovtempFather) {
		this.isgovtempFather = isgovtempFather;
	}

	public Boolean getIsgovtempMother() {
		return this.isgovtempMother;
	}

	public void setIsgovtempMother(Boolean isgovtempMother) {
		this.isgovtempMother = isgovtempMother;
	}

	public Boolean getIsgovtempSpouse() {
		return this.isgovtempSpouse;
	}

	public void setIsgovtempSpouse(Boolean isgovtempSpouse) {
		this.isgovtempSpouse = isgovtempSpouse;
	}

	public String getLangStatus1() {
		return this.langStatus1;
	}

	public void setLangStatus1(String langStatus1) {
		this.langStatus1 = langStatus1;
	}

	public String getLangStatus2() {
		return this.langStatus2;
	}

	public void setLangStatus2(String langStatus2) {
		this.langStatus2 = langStatus2;
	}

	public String getLangStatus3() {
		return this.langStatus3;
	}

	public void setLangStatus3(String langStatus3) {
		this.langStatus3 = langStatus3;
	}

	public String getLangStatus4() {
		return this.langStatus4;
	}

	public void setLangStatus4(String langStatus4) {
		this.langStatus4 = langStatus4;
	}

	public String getLangStatus5() {
		return this.langStatus5;
	}

	public void setLangStatus5(String langStatus5) {
		this.langStatus5 = langStatus5;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMotherAddress() {
		return this.motherAddress;
	}

	public void setMotherAddress(String motherAddress) {
		this.motherAddress = motherAddress;
	}

	public String getMotherEmailId() {
		return this.motherEmailId;
	}

	public void setMotherEmailId(String motherEmailId) {
		this.motherEmailId = motherEmailId;
	}

	public String getMotherIncomePa() {
		return this.motherIncomePa;
	}

	public void setMotherIncomePa(String motherIncomePa) {
		this.motherIncomePa = motherIncomePa;
	}

	public String getMotherMobileNo() {
		return this.motherMobileNo;
	}

	public void setMotherMobileNo(String motherMobileNo) {
		this.motherMobileNo = motherMobileNo;
	}

	public String getMotherName() {
		return this.motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getMotherOccupation() {
		return this.motherOccupation;
	}

	public void setMotherOccupation(String motherOccupation) {
		this.motherOccupation = motherOccupation;
	}

	public String getMotherPhotoPath() {
		return this.motherPhotoPath;
	}

	public void setMotherPhotoPath(String motherPhotoPath) {
		this.motherPhotoPath = motherPhotoPath;
	}

	public String getMotherQualification() {
		return this.motherQualification;
	}

	public void setMotherQualification(String motherQualification) {
		this.motherQualification = motherQualification;
	}

	public String getPancardFilePath() {
		return this.pancardFilePath;
	}

	public void setPancardFilePath(String pancardFilePath) {
		this.pancardFilePath = pancardFilePath;
	}

	public String getPancardNo() {
		return this.pancardNo;
	}

	public void setPancardNo(String pancardNo) {
		this.pancardNo = pancardNo;
	}

	public String getPassportNo() {
		return this.passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public String getPermanentAddress() {
		return this.permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getPermanentPincode() {
		return this.permanentPincode;
	}

	public void setPermanentPincode(String permanentPincode) {
		this.permanentPincode = permanentPincode;
	}

	public String getPermanentStreet() {
		return this.permanentStreet;
	}

	public void setPermanentStreet(String permanentStreet) {
		this.permanentStreet = permanentStreet;
	}

	public String getPremanentMandal() {
		return this.premanentMandal;
	}

	public void setPremanentMandal(String premanentMandal) {
		this.premanentMandal = premanentMandal;
	}

	public String getPresentAddress() {
		return this.presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getPresentMandal() {
		return this.presentMandal;
	}

	public void setPresentMandal(String presentMandal) {
		this.presentMandal = presentMandal;
	}

	public String getPresentPincode() {
		return this.presentPincode;
	}

	public void setPresentPincode(String presentPincode) {
		this.presentPincode = presentPincode;
	}

	public String getPresentStreet() {
		return this.presentStreet;
	}

	public void setPresentStreet(String presentStreet) {
		this.presentStreet = presentStreet;
	}

	public String getPrimaryContact() {
		return this.primaryContact;
	}

	public void setPrimaryContact(String primaryContact) {
		this.primaryContact = primaryContact;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReceiptNo() {
		return this.receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public String getRefApplicationNo() {
		return this.refApplicationNo;
	}

	public void setRefApplicationNo(String refApplicationNo) {
		this.refApplicationNo = refApplicationNo;
	}

	public String getResidencePhone() {
		return this.residencePhone;
	}

	public void setResidencePhone(String residencePhone) {
		this.residencePhone = residencePhone;
	}

	public String getRfid() {
		return this.rfid;
	}

	public void setRfid(String rfid) {
		this.rfid = rfid;
	}

	public String getRollNumber() {
		return this.rollNumber;
	}

	public void setRollNumber(String rollNumber) {
		this.rollNumber = rollNumber;
	}

	public String getSpouseAddress() {
		return this.spouseAddress;
	}

	public void setSpouseAddress(String spouseAddress) {
		this.spouseAddress = spouseAddress;
	}

	public String getSpouseEmailId() {
		return this.spouseEmailId;
	}

	public void setSpouseEmailId(String spouseEmailId) {
		this.spouseEmailId = spouseEmailId;
	}

	public String getSpouseIncomePa() {
		return this.spouseIncomePa;
	}

	public void setSpouseIncomePa(String spouseIncomePa) {
		this.spouseIncomePa = spouseIncomePa;
	}

	public String getSpouseMobileNo() {
		return this.spouseMobileNo;
	}

	public void setSpouseMobileNo(String spouseMobileNo) {
		this.spouseMobileNo = spouseMobileNo;
	}

	public String getSpouseName() {
		return this.spouseName;
	}

	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}

	public String getSpouseOccupation() {
		return this.spouseOccupation;
	}

	public void setSpouseOccupation(String spouseOccupation) {
		this.spouseOccupation = spouseOccupation;
	}

	public String getSpousePhotoPath() {
		return this.spousePhotoPath;
	}

	public void setSpousePhotoPath(String spousePhotoPath) {
		this.spousePhotoPath = spousePhotoPath;
	}

	public String getSpouseQualification() {
		return this.spouseQualification;
	}

	public void setSpouseQualification(String spouseQualification) {
		this.spouseQualification = spouseQualification;
	}

	public String getSscNo() {
		return this.sscNo;
	}

	public void setSscNo(String sscNo) {
		this.sscNo = sscNo;
	}

	public String getStdEmailId() {
		return this.stdEmailId;
	}

	public void setStdEmailId(String stdEmailId) {
		this.stdEmailId = stdEmailId;
	}

	public String getStudentPhotoPath() {
		return this.studentPhotoPath;
	}

	public void setStudentPhotoPath(String studentPhotoPath) {
		this.studentPhotoPath = studentPhotoPath;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Date getWeddingDate() {
		return this.weddingDate;
	}

	public void setWeddingDate(Date weddingDate) {
		this.weddingDate = weddingDate;
	}

	public List<BatchwiseStudent> getBatchwiseStudents() {
		return this.batchwiseStudents;
	}

	public void setBatchwiseStudents(List<BatchwiseStudent> batchwiseStudents) {
		this.batchwiseStudents = batchwiseStudents;
	}

	public BatchwiseStudent addBatchwiseStudent(BatchwiseStudent batchwiseStudent) {
		getBatchwiseStudents().add(batchwiseStudent);
		batchwiseStudent.setStudentDetail(this);

		return batchwiseStudent;
	}

	public BatchwiseStudent removeBatchwiseStudent(BatchwiseStudent batchwiseStudent) {
		getBatchwiseStudents().remove(batchwiseStudent);
		batchwiseStudent.setStudentDetail(null);

		return batchwiseStudent;
	}

	public List<FeeInstantPayment> getFeeInstantPayments() {
		return this.feeInstantPayments;
	}

	public void setFeeInstantPayments(List<FeeInstantPayment> feeInstantPayments) {
		this.feeInstantPayments = feeInstantPayments;
	}

	public FeeInstantPayment addFeeInstantPayment(FeeInstantPayment feeInstantPayment) {
		getFeeInstantPayments().add(feeInstantPayment);
		feeInstantPayment.setStudentDetail(this);

		return feeInstantPayment;
	}

	public FeeInstantPayment removeFeeInstantPayment(FeeInstantPayment feeInstantPayment) {
		getFeeInstantPayments().remove(feeInstantPayment);
		feeInstantPayment.setStudentDetail(null);

		return feeInstantPayment;
	}

	public List<FeeParticularwisePayment> getFeeParticularwisePayments() {
		return this.feeParticularwisePayments;
	}

	public void setFeeParticularwisePayments(List<FeeParticularwisePayment> feeParticularwisePayments) {
		this.feeParticularwisePayments = feeParticularwisePayments;
	}

	public FeeParticularwisePayment addFeeParticularwisePayment(FeeParticularwisePayment feeParticularwisePayment) {
		getFeeParticularwisePayments().add(feeParticularwisePayment);
		feeParticularwisePayment.setStudentDetail(this);

		return feeParticularwisePayment;
	}

	public FeeParticularwisePayment removeFeeParticularwisePayment(FeeParticularwisePayment feeParticularwisePayment) {
		getFeeParticularwisePayments().remove(feeParticularwisePayment);
		feeParticularwisePayment.setStudentDetail(null);

		return feeParticularwisePayment;
	}

	public List<FeeReceipt> getFeeReceipts() {
		return this.feeReceipts;
	}

	public void setFeeReceipts(List<FeeReceipt> feeReceipts) {
		this.feeReceipts = feeReceipts;
	}

	public FeeReceipt addFeeReceipt(FeeReceipt feeReceipt) {
		getFeeReceipts().add(feeReceipt);
		feeReceipt.setStudentDetail(this);

		return feeReceipt;
	}

	public FeeReceipt removeFeeReceipt(FeeReceipt feeReceipt) {
		getFeeReceipts().remove(feeReceipt);
		feeReceipt.setStudentDetail(null);

		return feeReceipt;
	}

	public List<FeeStudentData> getFeeStudentData() {
		return this.feeStudentData;
	}

	public void setFeeStudentData(List<FeeStudentData> feeStudentData) {
		this.feeStudentData = feeStudentData;
	}

	public FeeStudentData addFeeStudentData(FeeStudentData feeStudentData) {
		getFeeStudentData().add(feeStudentData);
		feeStudentData.setStudentDetail(this);

		return feeStudentData;
	}

	public FeeStudentData removeFeeStudentData(FeeStudentData feeStudentData) {
		getFeeStudentData().remove(feeStudentData);
		feeStudentData.setStudentDetail(null);

		return feeStudentData;
	}

	public List<FeeStudentRefund> getFeeStudentRefunds() {
		return this.feeStudentRefunds;
	}

	public void setFeeStudentRefunds(List<FeeStudentRefund> feeStudentRefunds) {
		this.feeStudentRefunds = feeStudentRefunds;
	}

	public FeeStudentRefund addFeeStudentRefund(FeeStudentRefund feeStudentRefund) {
		getFeeStudentRefunds().add(feeStudentRefund);
		feeStudentRefund.setStudentDetail(this);

		return feeStudentRefund;
	}

	public FeeStudentRefund removeFeeStudentRefund(FeeStudentRefund feeStudentRefund) {
		getFeeStudentRefunds().remove(feeStudentRefund);
		feeStudentRefund.setStudentDetail(null);

		return feeStudentRefund;
	}

	public List<FeeStudentWiseParticular> getFeeStudentWiseParticulars() {
		return this.feeStudentWiseParticulars;
	}

	public void setFeeStudentWiseParticulars(List<FeeStudentWiseParticular> feeStudentWiseParticulars) {
		this.feeStudentWiseParticulars = feeStudentWiseParticulars;
	}

	public FeeStudentWiseParticular addFeeStudentWiseParticular(FeeStudentWiseParticular feeStudentWiseParticular) {
		getFeeStudentWiseParticulars().add(feeStudentWiseParticular);
		feeStudentWiseParticular.setStudentDetail(this);

		return feeStudentWiseParticular;
	}

	public FeeStudentWiseParticular removeFeeStudentWiseParticular(FeeStudentWiseParticular feeStudentWiseParticular) {
		getFeeStudentWiseParticulars().remove(feeStudentWiseParticular);
		feeStudentWiseParticular.setStudentDetail(null);

		return feeStudentWiseParticular;
	}

	public List<FeeStudentwiseDiscount> getFeeStudentwiseDiscounts() {
		return this.feeStudentwiseDiscounts;
	}

	public void setFeeStudentwiseDiscounts(List<FeeStudentwiseDiscount> feeStudentwiseDiscounts) {
		this.feeStudentwiseDiscounts = feeStudentwiseDiscounts;
	}

	public FeeStudentwiseDiscount addFeeStudentwiseDiscount(FeeStudentwiseDiscount feeStudentwiseDiscount) {
		getFeeStudentwiseDiscounts().add(feeStudentwiseDiscount);
		feeStudentwiseDiscount.setStudentDetail(this);

		return feeStudentwiseDiscount;
	}

	public FeeStudentwiseDiscount removeFeeStudentwiseDiscount(FeeStudentwiseDiscount feeStudentwiseDiscount) {
		getFeeStudentwiseDiscounts().remove(feeStudentwiseDiscount);
		feeStudentwiseDiscount.setStudentDetail(null);

		return feeStudentwiseDiscount;
	}

	public List<FeeStudentwiseFine> getFeeStudentwiseFines() {
		return this.feeStudentwiseFines;
	}

	public void setFeeStudentwiseFines(List<FeeStudentwiseFine> feeStudentwiseFines) {
		this.feeStudentwiseFines = feeStudentwiseFines;
	}

	public FeeStudentwiseFine addFeeStudentwiseFine(FeeStudentwiseFine feeStudentwiseFine) {
		getFeeStudentwiseFines().add(feeStudentwiseFine);
		feeStudentwiseFine.setStudentDetail(this);

		return feeStudentwiseFine;
	}

	public FeeStudentwiseFine removeFeeStudentwiseFine(FeeStudentwiseFine feeStudentwiseFine) {
		getFeeStudentwiseFines().remove(feeStudentwiseFine);
		feeStudentwiseFine.setStudentDetail(null);

		return feeStudentwiseFine;
	}

	public List<FeeTransactionMaster> getFeeTransactionMasters() {
		return this.feeTransactionMasters;
	}

	public void setFeeTransactionMasters(List<FeeTransactionMaster> feeTransactionMasters) {
		this.feeTransactionMasters = feeTransactionMasters;
	}

	public FeeTransactionMaster addFeeTransactionMaster(FeeTransactionMaster feeTransactionMaster) {
		getFeeTransactionMasters().add(feeTransactionMaster);
		feeTransactionMaster.setStudentDetail(this);

		return feeTransactionMaster;
	}

	public FeeTransactionMaster removeFeeTransactionMaster(FeeTransactionMaster feeTransactionMaster) {
		getFeeTransactionMasters().remove(feeTransactionMaster);
		feeTransactionMaster.setStudentDetail(null);

		return feeTransactionMaster;
	}

	public List<SurveyFeedback> getSurveyFeedbacks1() {
		return this.surveyFeedbacks1;
	}

	public void setSurveyFeedbacks1(List<SurveyFeedback> surveyFeedbacks1) {
		this.surveyFeedbacks1 = surveyFeedbacks1;
	}

	public SurveyFeedback addSurveyFeedbacks1(SurveyFeedback surveyFeedbacks1) {
		getSurveyFeedbacks1().add(surveyFeedbacks1);
		surveyFeedbacks1.setFromStudentDetail(this);

		return surveyFeedbacks1;
	}

	public SurveyFeedback removeSurveyFeedbacks1(SurveyFeedback surveyFeedbacks1) {
		getSurveyFeedbacks1().remove(surveyFeedbacks1);
		surveyFeedbacks1.setFromStudentDetail(null);

		return surveyFeedbacks1;
	}

	public List<SurveyFeedback> getSurveyFeedbacks2() {
		return this.surveyFeedbacks2;
	}

	public void setSurveyFeedbacks2(List<SurveyFeedback> surveyFeedbacks2) {
		this.surveyFeedbacks2 = surveyFeedbacks2;
	}

	public SurveyFeedback addSurveyFeedbacks2(SurveyFeedback surveyFeedbacks2) {
		getSurveyFeedbacks2().add(surveyFeedbacks2);
		surveyFeedbacks2.setForStudentDetail(this);

		return surveyFeedbacks2;
	}

	public SurveyFeedback removeSurveyFeedbacks2(SurveyFeedback surveyFeedbacks2) {
		getSurveyFeedbacks2().remove(surveyFeedbacks2);
		surveyFeedbacks2.setForStudentDetail(null);

		return surveyFeedbacks2;
	}

	public List<MessagingRecipient> getMessagingRecipients() {
		return this.messagingRecipients;
	}

	public void setMessagingRecipients(List<MessagingRecipient> messagingRecipients) {
		this.messagingRecipients = messagingRecipients;
	}

	public MessagingRecipient addMessagingRecipient(MessagingRecipient messagingRecipient) {
		getMessagingRecipients().add(messagingRecipient);
		messagingRecipient.setStudentDetail(this);

		return messagingRecipient;
	}

	public MessagingRecipient removeMessagingRecipient(MessagingRecipient messagingRecipient) {
		getMessagingRecipients().remove(messagingRecipient);
		messagingRecipient.setStudentDetail(null);

		return messagingRecipient;
	}

	public List<StudentActivitiesDetail> getActivitiesDetails() {
		return this.activitiesDetails;
	}

	public void setActivitiesDetails(List<StudentActivitiesDetail> activitiesDetails) {
		this.activitiesDetails = activitiesDetails;
	}

	public StudentActivitiesDetail addActivitiesDetail(StudentActivitiesDetail activitiesDetail) {
		getActivitiesDetails().add(activitiesDetail);
		activitiesDetail.setStudentDetail(this);

		return activitiesDetail;
	}

	public StudentActivitiesDetail removeActivitiesDetail(StudentActivitiesDetail activitiesDetail) {
		getActivitiesDetails().remove(activitiesDetail);
		activitiesDetail.setStudentDetail(null);

		return activitiesDetail;
	}

	public List<StudentDailylog> getStdDailylogs() {
		return this.stdDailylogs;
	}

	public void setStdDailylogs(List<StudentDailylog> stdDailylogs) {
		this.stdDailylogs = stdDailylogs;
	}

	public StudentDailylog addStdDailylog(StudentDailylog stdDailylog) {
		getStdDailylogs().add(stdDailylog);
		stdDailylog.setStudentDetail(this);

		return stdDailylog;
	}

	public StudentDailylog removeStdDailylog(StudentDailylog stdDailylog) {
		getStdDailylogs().remove(stdDailylog);
		stdDailylog.setStudentDetail(null);

		return stdDailylog;
	}

	public List<StudentDocumentCollection> getStdDocumentCollections() {
		return this.stdDocumentCollections;
	}

	public void setStdDocumentCollections(List<StudentDocumentCollection> stdDocumentCollections) {
		this.stdDocumentCollections = stdDocumentCollections;
	}

	public StudentDocumentCollection addStdDocumentCollection(StudentDocumentCollection stdDocumentCollection) {
		getStdDocumentCollections().add(stdDocumentCollection);
		stdDocumentCollection.setStudentDetail(this);

		return stdDocumentCollection;
	}

	public StudentDocumentCollection removeStdDocumentCollection(StudentDocumentCollection stdDocumentCollection) {
		getStdDocumentCollections().remove(stdDocumentCollection);
		stdDocumentCollection.setStudentDetail(null);

		return stdDocumentCollection;
	}

	public List<StudentEducationDetail> getStdEducationDetails() {
		return this.stdEducationDetails;
	}

	public void setStdEducationDetails(List<StudentEducationDetail> stdEducationDetails) {
		this.stdEducationDetails = stdEducationDetails;
	}

	public StudentEducationDetail addStdEducationDetail(StudentEducationDetail stdEducationDetail) {
		getStdEducationDetails().add(stdEducationDetail);
		stdEducationDetail.setStudentDetail(this);

		return stdEducationDetail;
	}

	public StudentEducationDetail removeStdEducationDetail(StudentEducationDetail stdEducationDetail) {
		getStdEducationDetails().remove(stdEducationDetail);
		stdEducationDetail.setStudentDetail(null);

		return stdEducationDetail;
	}

	public List<StudentAcademicbatch> getStudentAcademicbatches() {
		return this.studentAcademicbatches;
	}

	public void setStudentAcademicbatches(List<StudentAcademicbatch> studentAcademicbatches) {
		this.studentAcademicbatches = studentAcademicbatches;
	}

	public StudentAcademicbatch addStudentAcademicbatch(StudentAcademicbatch studentAcademicbatch) {
		getStudentAcademicbatches().add(studentAcademicbatch);
		studentAcademicbatch.setStudentDetail(this);

		return studentAcademicbatch;
	}

	public StudentAcademicbatch removeStudentAcademicbatch(StudentAcademicbatch studentAcademicbatch) {
		getStudentAcademicbatches().remove(studentAcademicbatch);
		studentAcademicbatch.setStudentDetail(null);

		return studentAcademicbatch;
	}

	public Batch getBatch() {
		return this.batch;
	}

	public void setBatch(Batch batch) {
		this.batch = batch;
	}

	public Caste getCaste() {
		return this.caste;
	}

	public void setCaste(Caste caste) {
		this.caste = caste;
	}

	public City getPresentCity() {
		return this.presentCity;
	}

	public void setPresentCity(City presentCity) {
		this.presentCity = presentCity;
	}

	public City getPremenentCity() {
		return this.premenentCity;
	}

	public void setPremenentCity(City premenentCity) {
		this.premenentCity = premenentCity;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Course getCourse() {
		return this.course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}



	public CourseYear getCourseYear() {
		return this.courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public District getPresentDistrict() {
		return this.presentDistrict;
	}

	public void setPresentDistrict(District presentDistrict) {
		this.presentDistrict = presentDistrict;
	}

	public District getPermanentDistrict() {
		return this.permanentDistrict;
	}

	public void setPermanentDistrict(District permanentDistrict) {
		this.permanentDistrict = permanentDistrict;
	}

	public GeneralDetail getDisability() {
		return this.disability;
	}

	public void setDisability(GeneralDetail disability) {
		this.disability = disability;
	}

	public GeneralDetail getNationality() {
		return this.nationality;
	}

	public void setNationality(GeneralDetail nationality) {
		this.nationality = nationality;
	}

	public GeneralDetail getReligion() {
		return this.religion;
	}

	public void setReligion(GeneralDetail religion) {
		this.religion = religion;
	}

	public GeneralDetail getBloodgroup() {
		return this.bloodgroup;
	}

	public void setBloodgroup(GeneralDetail bloodgroup) {
		this.bloodgroup = bloodgroup;
	}

	public GeneralDetail getMaritalstatus() {
		return this.maritalstatus;
	}

	public void setMaritalstatus(GeneralDetail maritalstatus) {
		this.maritalstatus = maritalstatus;
	}

	public GeneralDetail getLanguage1() {
		return this.language1;
	}

	public void setLanguage1(GeneralDetail language1) {
		this.language1 = language1;
	}

	public GeneralDetail getLanguage2() {
		return this.language2;
	}

	public void setLanguage2(GeneralDetail language2) {
		this.language2 = language2;
	}

	public GeneralDetail getLanguage3() {
		return this.language3;
	}

	public void setLanguage3(GeneralDetail language3) {
		this.language3 = language3;
	}

	public GeneralDetail getLanguage4() {
		return this.language4;
	}

	public void setLanguage4(GeneralDetail language4) {
		this.language4 = language4;
	}

	public GeneralDetail getLanguage5() {
		return this.language5;
	}

	public void setLanguage5(GeneralDetail language5) {
		this.language5 = language5;
	}

	public GeneralDetail getQuota() {
		return this.quota;
	}

	public void setQuota(GeneralDetail quota) {
		this.quota = quota;
	}

	public GeneralDetail getStudentType() {
		return this.studentType;
	}

	public void setStudentType(GeneralDetail studentType) {
		this.studentType = studentType;
	}

	public GeneralDetail getGender() {
		return this.gender;
	}

	public void setGender(GeneralDetail gender) {
		this.gender = gender;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public SubCaste getSubCaste() {
		return this.subCaste;
	}

	public void setSubCaste(SubCaste subCaste) {
		this.subCaste = subCaste;
	}

	public User getParentUser() {
		return this.parentUser;
	}

	public void setParentUser(User parentUser) {
		this.parentUser = parentUser;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public StudentApplication getStudentApplication() {
		return this.studentApplication;
	}

	public void setStudentApplication(StudentApplication studentApplication) {
		this.studentApplication = studentApplication;
	}

	public List<StudentSubject> getStudentSubjects() {
		return this.studentSubjects;
	}

	public void setStudentSubjects(List<StudentSubject> studentSubjects) {
		this.studentSubjects = studentSubjects;
	}

	public StudentSubject addStudentSubject(StudentSubject studentSubject) {
		getStudentSubjects().add(studentSubject);
		studentSubject.setStudentDetail(this);

		return studentSubject;
	}

	public StudentSubject removeStudentSubject(StudentSubject studentSubject) {
		getStudentSubjects().remove(studentSubject);
		studentSubject.setStudentDetail(null);

		return studentSubject;
	}

	public List<TransportFeePayment> getFeePayments() {
		return this.feePayments;
	}

	public void setFeePayments(List<TransportFeePayment> feePayments) {
		this.feePayments = feePayments;
	}

	public TransportFeePayment addFeePayment(TransportFeePayment feePayment) {
		getFeePayments().add(feePayment);
		feePayment.setStudentDetail(this);

		return feePayment;
	}

	public TransportFeePayment removeFeePayment(TransportFeePayment feePayment) {
		getFeePayments().remove(feePayment);
		feePayment.setStudentDetail(null);

		return feePayment;
	}

	public List<TransportAllocation> getTransportAllocations() {
		return this.transportAllocations;
	}

	public void setTransportAllocations(List<TransportAllocation> transportAllocations) {
		this.transportAllocations = transportAllocations;
	}

	public TransportAllocation addTransportAllocation(TransportAllocation transportAllocation) {
		getTransportAllocations().add(transportAllocation);
		transportAllocation.setStudentDetail(this);

		return transportAllocation;
	}

	public TransportAllocation removeTransportAllocation(TransportAllocation transportAllocation) {
		getTransportAllocations().remove(transportAllocation);
		transportAllocation.setStudentDetail(null);

		return transportAllocation;
	}

	public List<StudentAttendance> getStdAttendances() {
		return this.stdAttendances;
	}

	public void setStdAttendances(List<StudentAttendance> stdAttendances) {
		this.stdAttendances = stdAttendances;
	}

	public StudentAttendance addStdAttendance(StudentAttendance stdAttendance) {
		getStdAttendances().add(stdAttendance);
		stdAttendance.setStudentDetail(this);

		return stdAttendance;
	}

	public StudentAttendance removeStdAttendance(StudentAttendance stdAttendance) {
		getStdAttendances().remove(stdAttendance);
		stdAttendance.setStudentDetail(null);

		return stdAttendance;
	}

	public GroupSection getGroupSection() {
		return groupSection;
	}

	public void setGroupSection(GroupSection groupSection) {
		this.groupSection = groupSection;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public GeneralDetail getStudentStatus() {
		return studentStatus;
	}

	public void setStudentStatus(GeneralDetail studentStatus) {
		this.studentStatus = studentStatus;
	}

	public GeneralDetail getQualifying() {
		return qualifying;
	}

	public void setQualifying(GeneralDetail qualifying) {
		this.qualifying = qualifying;
	}

	public Boolean getIsMinority() {
		return isMinority;
	}

	public void setIsMinority(Boolean isMinority) {
		this.isMinority = isMinority;
	}
}