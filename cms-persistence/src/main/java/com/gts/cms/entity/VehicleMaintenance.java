package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the t_tm_vehicle_maintenance database table.
 * 
 */
@Entity
@Table(name="t_tm_vehicle_maintenance")
@NamedQuery(name="VehicleMaintenance.findAll", query="SELECT v FROM VehicleMaintenance v")
public class VehicleMaintenance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_vehicle_maintenance_id")
	private Long vehicleMaintenanceId;

	@Column(name="aproved_by")
	private String aprovedBy;

	@Column(name="balance_engoil_in_tank")
	private BigDecimal balanceEngineoilInTank;

	@Column(name="balance_fuel_in_tank")
	private BigDecimal balanceFuelInTank;

	@Column(name="closing_reading")
	private String closingReading;

	@Column(name="consumed_engoil")
	private BigDecimal consumedEngineoil;

	@Column(name="consumed_fuel")
	private BigDecimal consumedFuel;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="engoil_cost")
	private BigDecimal engoilCost;

	@Column(name="fuel_cost")
	private BigDecimal fuelCost;

	@Column(name="is_active")
	private Boolean isActive;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="maintenance_date")
	private Date maintenanceDate;

	@Temporal(TemporalType.DATE)
	private Date monthyr;

	@Column(name="opening_reading")
	private String openingReading;

	@Column(name="purchased_engoil_ltr")
	private BigDecimal purchasedEngoilLtr;

	@Column(name="purchased_fuel_ltr")
	private BigDecimal purchasedFuelLtr;

	private String reason;

	@Column(name="receipt_path1")
	private String receiptPath1;

	@Column(name="receipt_path2")
	private String receiptPath2;

	@Column(name="receipt_path3")
	private String receiptPath3;

	@Column(name="repairs_cost")
	private BigDecimal repairsCost;

	@Column(name="repairs_description")
	private String repairsDescription;

	@Column(name="total_maintenance_cost")
	private BigDecimal totalMaintenanceCost;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_aproved_emp_id")
	private EmployeeDetail approvedEmployeeDetail;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to TransportDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_transport_detail_id")
	private TransportDetail transportDetail;

	//bi-directional many-to-one association to VehicleDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_vehicle_detail_id")
	private VehicleDetail vehicleDetail;

	public VehicleMaintenance() {
	}

	public Long getVehicleMaintenanceId() {
		return this.vehicleMaintenanceId;
	}

	public void setVehicleMaintenanceId(Long vehicleMaintenanceId) {
		this.vehicleMaintenanceId = vehicleMaintenanceId;
	}

	public String getAprovedBy() {
		return this.aprovedBy;
	}

	public void setAprovedBy(String aprovedBy) {
		this.aprovedBy = aprovedBy;
	}

	public BigDecimal getBalanceEngineoilInTank() {
		return this.balanceEngineoilInTank;
	}

	public void setBalanceEngineoilInTank(BigDecimal balanceEngineoilInTank) {
		this.balanceEngineoilInTank = balanceEngineoilInTank;
	}

	public BigDecimal getBalanceFuelInTank() {
		return this.balanceFuelInTank;
	}

	public void setBalanceFuelInTank(BigDecimal balanceFuelInTank) {
		this.balanceFuelInTank = balanceFuelInTank;
	}

	public String getClosingReading() {
		return this.closingReading;
	}

	public void setClosingReading(String closingReading) {
		this.closingReading = closingReading;
	}

	public BigDecimal getConsumedEngineoil() {
		return this.consumedEngineoil;
	}

	public void setConsumedEngineoil(BigDecimal consumedEngineoil) {
		this.consumedEngineoil = consumedEngineoil;
	}

	public BigDecimal getConsumedFuel() {
		return this.consumedFuel;
	}

	public void setConsumedFuel(BigDecimal consumedFuel) {
		this.consumedFuel = consumedFuel;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public BigDecimal getEngoilCost() {
		return this.engoilCost;
	}

	public void setEngoilCost(BigDecimal engoilCost) {
		this.engoilCost = engoilCost;
	}

	public BigDecimal getFuelCost() {
		return this.fuelCost;
	}

	public void setFuelCost(BigDecimal fuelCost) {
		this.fuelCost = fuelCost;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Date getMaintenanceDate() {
		return this.maintenanceDate;
	}

	public void setMaintenanceDate(Date maintenanceDate) {
		this.maintenanceDate = maintenanceDate;
	}

	public Date getMonthyr() {
		return this.monthyr;
	}

	public void setMonthyr(Date monthyr) {
		this.monthyr = monthyr;
	}

	public String getOpeningReading() {
		return this.openingReading;
	}

	public void setOpeningReading(String openingReading) {
		this.openingReading = openingReading;
	}

	public BigDecimal getPurchasedEngoilLtr() {
		return this.purchasedEngoilLtr;
	}

	public void setPurchasedEngoilLtr(BigDecimal purchasedEngoilLtr) {
		this.purchasedEngoilLtr = purchasedEngoilLtr;
	}

	public BigDecimal getPurchasedFuelLtr() {
		return this.purchasedFuelLtr;
	}

	public void setPurchasedFuelLtr(BigDecimal purchasedFuelLtr) {
		this.purchasedFuelLtr = purchasedFuelLtr;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReceiptPath1() {
		return this.receiptPath1;
	}

	public void setReceiptPath1(String receiptPath1) {
		this.receiptPath1 = receiptPath1;
	}

	public String getReceiptPath2() {
		return this.receiptPath2;
	}

	public void setReceiptPath2(String receiptPath2) {
		this.receiptPath2 = receiptPath2;
	}

	public String getReceiptPath3() {
		return this.receiptPath3;
	}

	public void setReceiptPath3(String receiptPath3) {
		this.receiptPath3 = receiptPath3;
	}

	public BigDecimal getRepairsCost() {
		return this.repairsCost;
	}

	public void setRepairsCost(BigDecimal repairsCost) {
		this.repairsCost = repairsCost;
	}

	public String getRepairsDescription() {
		return this.repairsDescription;
	}

	public void setRepairsDescription(String repairsDescription) {
		this.repairsDescription = repairsDescription;
	}

	public BigDecimal getTotalMaintenanceCost() {
		return this.totalMaintenanceCost;
	}

	public void setTotalMaintenanceCost(BigDecimal totalMaintenanceCost) {
		this.totalMaintenanceCost = totalMaintenanceCost;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getApprovedEmployeeDetail() {
		return this.approvedEmployeeDetail;
	}

	public void setApprovedEmployeeDetail(EmployeeDetail approvedEmployeeDetail) {
		this.approvedEmployeeDetail = approvedEmployeeDetail;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public TransportDetail getTransportDetail() {
		return this.transportDetail;
	}

	public void setTransportDetail(TransportDetail transportDetail) {
		this.transportDetail = transportDetail;
	}

	public VehicleDetail getVehicleDetail() {
		return this.vehicleDetail;
	}

	public void setVehicleDetail(VehicleDetail vehicleDetail) {
		this.vehicleDetail = vehicleDetail;
	}

}