package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_exam_revision_subjects database table.
 * 
 */
@Entity
@Table(name="t_exam_revision_subjects")
@NamedQuery(name="ExamRevisionSubject.findAll", query="SELECT e FROM ExamRevisionSubject e")
public class ExamRevisionSubject implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_exam_revision_sub_id")
	private Long examRevisionSubId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_year_id")
	private CourseYear courseYear;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_exam_addt_fee_receipt_id")
	private ExamAdditionalFeeReceipt examAdditionalFeeReceipt;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_exam_id")
	private ExamMaster examMaster;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_exam_std_det_id")
	private ExamStudentDetail examStudentDetail;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_examrevisiontype_catdet_id")
	private GeneralDetail examRevisionTypeCat;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_revisedby_emp_id")
	private EmployeeDetail revisedByEmp;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_student_id")
	private StudentDetail studentDetail;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subject_id")
	private Subject subject;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_published")
	private Boolean isPublished;

	@Column(name="previous_marks")
	private Integer previousMarks;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="revised_date")
	private Date revisedDate;

	@Column(name="revised_marks")
	private Integer revisedMarks;

	@Column(name="revisedby_name")
	private String revisedbyName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	public ExamRevisionSubject() {
	}

	public Long getExamRevisionSubId() {
		return examRevisionSubId;
	}

	public void setExamRevisionSubId(Long examRevisionSubId) {
		this.examRevisionSubId = examRevisionSubId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public CourseYear getCourseYear() {
		return courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public ExamAdditionalFeeReceipt getExamAdditionalFeeReceipt() {
		return examAdditionalFeeReceipt;
	}

	public void setExamAdditionalFeeReceipt(ExamAdditionalFeeReceipt examAdditionalFeeReceipt) {
		this.examAdditionalFeeReceipt = examAdditionalFeeReceipt;
	}

	public ExamMaster getExamMaster() {
		return examMaster;
	}

	public void setExamMaster(ExamMaster examMaster) {
		this.examMaster = examMaster;
	}

	public ExamStudentDetail getExamStudentDetail() {
		return examStudentDetail;
	}

	public void setExamStudentDetail(ExamStudentDetail examStudentDetail) {
		this.examStudentDetail = examStudentDetail;
	}

	public GeneralDetail getExamRevisionTypeCat() {
		return examRevisionTypeCat;
	}

	public void setExamRevisionTypeCat(GeneralDetail examRevisionTypeCat) {
		this.examRevisionTypeCat = examRevisionTypeCat;
	}

	public EmployeeDetail getRevisedByEmp() {
		return revisedByEmp;
	}

	public void setRevisedByEmp(EmployeeDetail revisedByEmp) {
		this.revisedByEmp = revisedByEmp;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsPublished() {
		return isPublished;
	}

	public void setIsPublished(Boolean isPublished) {
		this.isPublished = isPublished;
	}

	public Integer getPreviousMarks() {
		return previousMarks;
	}

	public void setPreviousMarks(Integer previousMarks) {
		this.previousMarks = previousMarks;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getRevisedDate() {
		return revisedDate;
	}

	public void setRevisedDate(Date revisedDate) {
		this.revisedDate = revisedDate;
	}

	public Integer getRevisedMarks() {
		return revisedMarks;
	}

	public void setRevisedMarks(Integer revisedMarks) {
		this.revisedMarks = revisedMarks;
	}

	public String getRevisedbyName() {
		return revisedbyName;
	}

	public void setRevisedbyName(String revisedbyName) {
		this.revisedbyName = revisedbyName;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}
}