package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_std_activities_details database table.
 * 
 */
@Entity
@Table(name="t_std_activities_details")
@NamedQuery(name="StudentActivitiesDetail.findAll", query="SELECT s FROM StudentActivitiesDetail s")
public class StudentActivitiesDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_std_activity_id")
	private Long studentActivityId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String level;

	private String particulars;

	@Column(name="sponsored_by")
	private String sponsoredBy;
	
	@Column(name="is_active")
	private Boolean isActive;
	
	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to StudentAppActivity
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_app_activity_id")
	private StudentAppActivity appActivity;

	//bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_student_id")
	private StudentDetail studentDetail;

	public StudentActivitiesDetail() {
	}

	public Long getStudentActivityId() {
		return this.studentActivityId;
	}

	public void setStudentActivityId(Long studentActivityId) {
		this.studentActivityId = studentActivityId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getLevel() {
		return this.level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getParticulars() {
		return this.particulars;
	}

	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}

	public String getSponsoredBy() {
		return this.sponsoredBy;
	}

	public void setSponsoredBy(String sponsoredBy) {
		this.sponsoredBy = sponsoredBy;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public StudentAppActivity getAppActivity() {
		return this.appActivity;
	}

	public void setAppActivity(StudentAppActivity appActivity) {
		this.appActivity = appActivity;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}