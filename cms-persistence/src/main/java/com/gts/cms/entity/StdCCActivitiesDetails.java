package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_std_ccactivities_details database table.
 */
@Data
@Entity
@Table(name = "t_std_ccactivities_details")
@NamedQuery(name = "StdCCActivitiesDetails.findAll", query = "SELECT c FROM StdCCActivitiesDetails c")
public class StdCCActivitiesDetails implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_std_ccactivity_id")
    private Long stdCcactivityId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @ManyToOne
    @JoinColumn(name = "fk_school_id")
    private School school;

    @ManyToOne
    @JoinColumn(name = "fk_student_id")
    private StudentDetail studentDetail;

    @ManyToOne
    @JoinColumn(name = "fk_course_year_id")
    private CourseYear courseYear;

    @ManyToOne
    @JoinColumn(name = "fk_ccactivity_catdet_id")
    private GeneralDetail ccactivityCatdetId;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;


}