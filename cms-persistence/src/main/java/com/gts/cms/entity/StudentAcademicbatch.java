package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_std_student_academicbatches database table.
 * 
 */
@Entity
@Table(name="t_std_student_academicbatches")
@NamedQuery(name="StudentAcademicbatch.findAll", query="SELECT s FROM StudentAcademicbatch s")
public class StudentAcademicbatch implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_std_student_academicbatch_id")
	private Long studentAcademicbatchId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="from_date")
	private Date fromDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_promoted")
	private Boolean isPromoted;
	
	@Column(name="is_alumni")
	private Boolean isAlmuni;
	
	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="to_date")
	private Date toDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;

	//bi-directional many-to-one association to Batch
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_from_batch_id")
	private Batch fromBatch;

	//bi-directional many-to-one association to Batch
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_to_batch_id")
	private Batch toBatch;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to Course
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_id")
	private Course course;

	//bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_from_course_year_id")
	private CourseYear fromCourseYear;

	//bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_to_course_year_id")
	private CourseYear toCourseYear;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_student_status_catdet_id")
	private GeneralDetail studentStatus;

	//bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_student_id")
	private StudentDetail studentDetail;
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_from_group_section_id")
	private GroupSection fromGroupSection;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_to_group_section_id")
	private GroupSection toGroupSection;
	

	
	public StudentAcademicbatch() {
	}

	public Long getStudentAcademicbatchId() {
		return this.studentAcademicbatchId;
	}

	public void setStudentAcademicbatchId(Long studentAcademicbatchId) {
		this.studentAcademicbatchId = studentAcademicbatchId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsPromoted() {
		return this.isPromoted;
	}

	public void setIsPromoted(Boolean isPromoted) {
		this.isPromoted = isPromoted;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public Batch getFromBatch() {
		return this.fromBatch;
	}

	public void setFromBatch(Batch fromBatch) {
		this.fromBatch = fromBatch;
	}

	public Batch getToBatch() {
		return this.toBatch;
	}

	public void setToBatch(Batch toBatch) {
		this.toBatch = toBatch;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Course getCourse() {
		return this.course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public CourseYear getFromCourseYear() {
		return this.fromCourseYear;
	}

	public void setFromCourseYear(CourseYear fromCourseYear) {
		this.fromCourseYear = fromCourseYear;
	}

	public CourseYear getToCourseYear() {
		return this.toCourseYear;
	}

	public void setToCourseYear(CourseYear toCourseYear) {
		this.toCourseYear = toCourseYear;
	}

	public GeneralDetail getStudentStatus() {
		return this.studentStatus;
	}

	public void setStudentStatus(GeneralDetail studentStatus) {
		this.studentStatus = studentStatus;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public GroupSection getFromGroupSection() {
		return fromGroupSection;
	}

	public void setFromGroupSection(GroupSection fromGroupSection) {
		this.fromGroupSection = fromGroupSection;
	}

	public GroupSection getToGroupSection() {
		return toGroupSection;
	}

	public void setToGroupSection(GroupSection toGroupSection) {
		this.toGroupSection = toGroupSection;
	}

	public Boolean getIsAlmuni() {
		return isAlmuni;
	}

	public void setIsAlmuni(Boolean isAlmuni) {
		this.isAlmuni = isAlmuni;
	}


}