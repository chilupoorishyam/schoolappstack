package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_tt_subject_resources database table.
 * 
 */
@Entity
@Table(name="t_tt_subject_resources")
@NamedQuery(name="SubjectResource.findAll", query="SELECT s FROM SubjectResource s")
public class SubjectResource implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_subject_resource_id")
	private Long subjectResourceId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="from_date")
	private Date fromDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_tutorial")
	private Boolean isTutorial;
	
	@Column(name="cell_group_id")
	private String cellGroupId;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="to_date")
	private Date toDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;
	
	@Column(name="color_code")
	private String colorCode;

	//bi-directional many-to-one association to ActualClassesSchedule
	/*@OneToMany(mappedBy="subjectResource")
	private List<ActualClassesSchedule> actualClassesSchedules;*/

	//bi-directional many-to-one association to Lessonstatus
	@OneToMany(mappedBy="subjectResource",fetch = FetchType.LAZY)
	private List<Lessonstatus> lessonstatuses;

	//bi-directional many-to-one association to StaffProxy
	/*@OneToMany(mappedBy="subjectResource",fetch = FetchType.LAZY)
	private List<StaffProxy> staffProxies;*/

	//bi-directional many-to-one association to StudentAttendance
	/*@OneToMany(mappedBy="subjectResource")
	private List<StudentAttendance> stdAttendances;*/

	//bi-directional many-to-one association to StaffCourseyrSubject
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subcourseyr_staff_id")
	private StaffCourseyrSubject staffCourseyrSubject;

	//bi-directional many-to-one association to SubjectCourseyear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_sub_courseyear_id")
	private SubjectCourseyear subjectCourseyear;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subjecttype_catdet_id")
	private GeneralDetail subjecttype;

	//bi-directional many-to-one association to Room
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_room_id")
	private Room room;

	//bi-directional many-to-one association to Studentbatch
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_stdbatch_id")
	private Studentbatch studentbatch;

	//bi-directional many-to-one association to Schedule
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_timetable_schedule_id")
	private Schedule schedule;

	public SubjectResource() {
	}

	public Long getSubjectResourceId() {
		return this.subjectResourceId;
	}

	public void setSubjectResourceId(Long subjectResourceId) {
		this.subjectResourceId = subjectResourceId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsTutorial() {
		return this.isTutorial;
	}

	public void setIsTutorial(Boolean isTutorial) {
		this.isTutorial = isTutorial;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	/*public List<ActualClassesSchedule> getActualClassesSchedules() {
		return this.actualClassesSchedules;
	}

	public void setActualClassesSchedules(List<ActualClassesSchedule> actualClassesSchedules) {
		this.actualClassesSchedules = actualClassesSchedules;
	}*/

	/*public ActualClassesSchedule addActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		getActualClassesSchedules().add(actualClassesSchedule);
		actualClassesSchedule.setSubjectResource(this);

		return actualClassesSchedule;
	}

	public ActualClassesSchedule removeActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		getActualClassesSchedules().remove(actualClassesSchedule);
		actualClassesSchedule.setSubjectResource(null);

		return actualClassesSchedule;
	}*/

	public List<Lessonstatus> getLessonstatuses() {
		return this.lessonstatuses;
	}

	public void setLessonstatuses(List<Lessonstatus> lessonstatuses) {
		this.lessonstatuses = lessonstatuses;
	}

	public Lessonstatus addLessonstatus(Lessonstatus lessonstatus) {
		getLessonstatuses().add(lessonstatus);
		lessonstatus.setSubjectResource(this);

		return lessonstatus;
	}

	public Lessonstatus removeLessonstatus(Lessonstatus lessonstatus) {
		getLessonstatuses().remove(lessonstatus);
		lessonstatus.setSubjectResource(null);

		return lessonstatus;
	}

	/*public List<StaffProxy> getStaffProxies() {
		return this.staffProxies;
	}

	public void setStaffProxies(List<StaffProxy> staffProxies) {
		this.staffProxies = staffProxies;
	}

	public StaffProxy addStaffProxy(StaffProxy staffProxy) {
		getStaffProxies().add(staffProxy);
		staffProxy.setSubjectResource(this);

		return staffProxy;
	}

	public StaffProxy removeStaffProxy(StaffProxy staffProxy) {
		getStaffProxies().remove(staffProxy);
		staffProxy.setSubjectResource(null);

		return staffProxy;
	}*/

	/*public List<StudentAttendance> getStdAttendances() {
		return this.stdAttendances;
	}

	public void setStdAttendances(List<StudentAttendance> stdAttendances) {
		this.stdAttendances = stdAttendances;
	}*/

/*	public StudentAttendance addStdAttendance(StudentAttendance stdAttendance) {
		getStdAttendances().add(stdAttendance);
		stdAttendance.setSubjectResource(this);

		return stdAttendance;
	}

	public StudentAttendance removeStdAttendance(StudentAttendance stdAttendance) {
		getStdAttendances().remove(stdAttendance);
		stdAttendance.setSubjectResource(null);

		return stdAttendance;
	}*/

	public StaffCourseyrSubject getStaffCourseyrSubject() {
		return this.staffCourseyrSubject;
	}

	public void setStaffCourseyrSubject(StaffCourseyrSubject staffCourseyrSubject) {
		this.staffCourseyrSubject = staffCourseyrSubject;
	}

	public SubjectCourseyear getSubjectCourseyear() {
		return this.subjectCourseyear;
	}

	public void setSubjectCourseyear(SubjectCourseyear subjectCourseyear) {
		this.subjectCourseyear = subjectCourseyear;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getSubjecttype() {
		return this.subjecttype;
	}

	public void setSubjecttype(GeneralDetail subjecttype) {
		this.subjecttype = subjecttype;
	}

	public Room getRoom() {
		return this.room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public Studentbatch getStudentbatch() {
		return this.studentbatch;
	}

	public void setStudentbatch(Studentbatch studentbatch) {
		this.studentbatch = studentbatch;
	}

	public Schedule getSchedule() {
		return this.schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	public String getColorCode() {
		return colorCode;
	}

	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}

	public String getCellGroupId() {
		return cellGroupId;
	}

	public void setCellGroupId(String cellGroupId) {
		this.cellGroupId = cellGroupId;
	}

}