package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_dl_course_members database table.
 * 
 */
@Entity
@Table(name = "t_dl_course_members")
@NamedQuery(name = "CourseMember.findAll", query = "SELECT c FROM CourseMember c")
public class CourseMember implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_dl_course_member_id")
	private Long courseMemberId;

	@Column(name = "completed_percentage")
	private Integer completedPercentage;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	@Column(name = "end_date")
	private Date endDate;

	@ManyToOne
	@JoinColumn(name = "fk_course_year_id")
	private CourseYear courseYear;

	@ManyToOne
	@JoinColumn(name = "fk_coursesub_catdet_id")
	private GeneralDetail coursesubCat;

	@ManyToOne
	@JoinColumn(name = "fk_subject_id")
	private Subject subject;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_lifetime")
	private Boolean isLifetime;

	private String reason;

	@Temporal(TemporalType.DATE)
	@Column(name = "start_date")
	private Date startDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "watch_time")
	private Integer watchTime;

	// bi-directional many-to-one association to CourseDiscussion
	@OneToMany(mappedBy = "courseMember",cascade = CascadeType.ALL)
	private List<CourseDiscussion> courseDiscussions;

	// bi-directional many-to-one association to CourseMemberTopicLog
	@OneToMany(mappedBy = "courseMember",cascade = CascadeType.ALL)
	private List<CourseMemberTopicLog> courseMemberTopicLogs;

	// bi-directional many-to-one association to OnlineCourses
	@ManyToOne
	@JoinColumn(name = "fk_dl_onlinecourse_id")
	private OnlineCourses onlineCourses;

	// bi-directional many-to-one association to TDlMember
	@ManyToOne
	@JoinColumn(name = "fk_subscription_membership_id")
	private DigitalLibraryMember member;

	// bi-directional many-to-one association to TDlMemberLearningNote
	@OneToMany(mappedBy = "courseMember" ,cascade = CascadeType.ALL)
	private List<MemberLearningNote> memberLearningNotes;

	public CourseMember() {
	}

	public Long getCourseMemberId() {
		return this.courseMemberId;
	}

	public void setCourseMemberId(Long courseMemberId) {
		this.courseMemberId = courseMemberId;
	}

	public Integer getCompletedPercentage() {
		return this.completedPercentage;
	}

	public void setCompletedPercentage(Integer completedPercentage) {
		this.completedPercentage = completedPercentage;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsLifetime() {
		return this.isLifetime;
	}

	public void setIsLifetime(Boolean isLifetime) {
		this.isLifetime = isLifetime;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Integer getWatchTime() {
		return this.watchTime;
	}

	public void setWatchTime(Integer watchTime) {
		this.watchTime = watchTime;
	}

	public CourseYear getCourseYear() {
		return courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public GeneralDetail getCoursesubCat() {
		return coursesubCat;
	}

	public void setCoursesubCat(GeneralDetail coursesubCat) {
		this.coursesubCat = coursesubCat;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public List<CourseDiscussion> getCourseDiscussions() {
		return courseDiscussions;
	}

	public void setCourseDiscussions(List<CourseDiscussion> courseDiscussions) {
		this.courseDiscussions = courseDiscussions;
	}

	public List<CourseMemberTopicLog> getCourseMemberTopicLogs() {
		return courseMemberTopicLogs;
	}

	public void setCourseMemberTopicLogs(List<CourseMemberTopicLog> courseMemberTopicLogs) {
		this.courseMemberTopicLogs = courseMemberTopicLogs;
	}

	public OnlineCourses getOnlineCourses() {
		return onlineCourses;
	}

	public void setOnlineCourses(OnlineCourses onlineCourses) {
		this.onlineCourses = onlineCourses;
	}

	public DigitalLibraryMember getMember() {
		return member;
	}

	public void setMember(DigitalLibraryMember member) {
		this.member = member;
	}

	public List<MemberLearningNote> getMemberLearningNotes() {
		return memberLearningNotes;
	}

	public void setMemberLearningNotes(List<MemberLearningNote> memberLearningNotes) {
		this.memberLearningNotes = memberLearningNotes;
	}

}