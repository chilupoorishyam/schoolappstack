package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_pa_company_contacts database table.
 * 
 */
@Entity
@Table(name = "t_pa_company_contacts")
@NamedQuery(name = "companyContact.findAll", query = "SELECT c FROM CompanyContact c")
public class CompanyContact implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_company_contact_id")
	private Long companyContactId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	private String designation;

	private String details;

	private String emailid;

	@Column(name = "is_active")
	private Boolean isActive;

	private String landline;

	@Temporal(TemporalType.DATE)
	@Column(name = "last_contacted_on")
	private Date lastContactedOn;

	private String mobile;

	@Column(name = "person_name")
	private String personName;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to District
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_company_id")
	private Company company;

	// bi-directional many-to-one association to PlacementCompany
	@OneToMany(mappedBy = "companyContact")
	private List<PlacementCompany> placementCompanies;

	public Long getCompanyContactId() {
		return this.companyContactId;
	}

	public void setCompanyContactId(Long companyContactId) {
		this.companyContactId = companyContactId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDesignation() {
		return this.designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDetails() {
		return this.details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getEmailid() {
		return this.emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getLandline() {
		return this.landline;
	}

	public void setLandline(String landline) {
		this.landline = landline;
	}

	public Date getLastContactedOn() {
		return this.lastContactedOn;
	}

	public void setLastContactedOn(Date lastContactedOn) {
		this.lastContactedOn = lastContactedOn;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPersonName() {
		return this.personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<PlacementCompany> getPlacementCompanies() {
		return placementCompanies;
	}

	public void setPlacementCompanies(List<PlacementCompany> placementCompanies) {
		this.placementCompanies = placementCompanies;
	}

}