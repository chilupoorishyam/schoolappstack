package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_hr_leave_entitlement database table.
 * 
 */
@Entity
@Table(name="t_hr_leave_entitlement")
@NamedQuery(name="LeaveEntitlement.findAll", query="SELECT l FROM LeaveEntitlement l")
public class LeaveEntitlement implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_leave_entitlement_id")
	private Long leaveEntitlementId;

	@Column(name="allocated_leaves")
	private BigDecimal allocatedLeaves;

	@Column(name="approval_comments")
	private String approvalComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="approval_date")
	private Date approvalDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;
	
	@Column(name="leave_year")
	private Integer leaveYear;

    @Column(name="is_carry_forward")
	private Boolean isCarryForward;

	private Boolean isapproved;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="valid_from")
	private Date validFrom;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="valid_to")
	private Date validTo;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_id")
	private EmployeeDetail employeeDetail;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_approved_emp_id")
	private EmployeeDetail approvedEmployeeDetail;

	//bi-directional many-to-one association to LeaveType
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_leave_type_id")
	private LeaveType leaveType;

	//bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	public LeaveEntitlement() {
	}

	public Long getLeaveEntitlementId() {
		return this.leaveEntitlementId;
	}

	public void setLeaveEntitlementId(Long leaveEntitlementId) {
		this.leaveEntitlementId = leaveEntitlementId;
	}


	public BigDecimal getAllocatedLeaves() {
		return allocatedLeaves;
	}

	public void setAllocatedLeaves(BigDecimal allocatedLeaves) {
		this.allocatedLeaves = allocatedLeaves;
	}

	public String getApprovalComments() {
		return this.approvalComments;
	}

	public void setApprovalComments(String approvalComments) {
		this.approvalComments = approvalComments;
	}

	public Date getApprovalDate() {
		return this.approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsCarryForward() {
		return this.isCarryForward;
	}

	public void setIsCarryForward(Boolean isCarryForward) {
		this.isCarryForward = isCarryForward;
	}

	public Boolean getIsapproved() {
		return this.isapproved;
	}

	public void setIsapproved(Boolean isapproved) {
		this.isapproved = isapproved;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public EmployeeDetail getEmployeeDetail() {
		return this.employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public EmployeeDetail getApprovedEmployeeDetail() {
		return this.approvedEmployeeDetail;
	}

	public void setApprovedEmployeeDetail(EmployeeDetail approvedEmployeeDetail) {
		this.approvedEmployeeDetail = approvedEmployeeDetail;
	}

	public LeaveType getLeaveType() {
		return this.leaveType;
	}

	public void setLeaveType(LeaveType leaveType) {
		this.leaveType = leaveType;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Integer getLeaveYear() {
		return leaveYear;
	}

	public void setLeaveYear(Integer leaveYear) {
		this.leaveYear = leaveYear;
	}
	

}