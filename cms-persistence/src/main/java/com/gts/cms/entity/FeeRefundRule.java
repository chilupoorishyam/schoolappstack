package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_fee_refund_rule database table.
 * 
 */
@Entity
@Table(name="t_fee_refund_rule")
@NamedQuery(name="FeeRefundRule.findAll", query="SELECT f FROM FeeRefundRule f")
public class FeeRefundRule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_refund_rule_id")
	private Long feeRefundRuleId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Column(name="refund_name")
	private String refundName;

	@Column(name="refund_type")
	private String refundType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="refund_validity")
	private Date refundValidity;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	private Integer value;

	//bi-directional many-to-one association to FeeStructure
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_structure_id")
	private FeeStructure feeStructure;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	public FeeRefundRule() {
	}

	public Long getFeeRefundRuleId() {
		return feeRefundRuleId;
	}

	public void setFeeRefundRuleId(Long feeRefundRuleId) {
		this.feeRefundRuleId = feeRefundRuleId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRefundName() {
		return this.refundName;
	}

	public void setRefundName(String refundName) {
		this.refundName = refundName;
	}

	public String getRefundType() {
		return this.refundType;
	}

	public void setRefundType(String refundType) {
		this.refundType = refundType;
	}

	public Date getRefundValidity() {
		return this.refundValidity;
	}

	public void setRefundValidity(Date refundValidity) {
		this.refundValidity = refundValidity;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Integer getValue() {
		return this.value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public FeeStructure getFeeStructure() {
		return this.feeStructure;
	}

	public void setFeeStructure(FeeStructure feeStructure) {
		this.feeStructure = feeStructure;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

}