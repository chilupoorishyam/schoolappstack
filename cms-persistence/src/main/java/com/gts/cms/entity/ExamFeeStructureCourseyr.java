package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_exam_fee_structure_courseyrs database table.
 * 
 */
@Entity
@Table(name = "t_exam_fee_structure_courseyrs")
@NamedQuery(name = "ExamFeeStructureCourseyr.findAll", query = "SELECT e FROM ExamFeeStructureCourseyr e")
public class ExamFeeStructureCourseyr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_fee_courseyr_id")
	private Long examFeeCourseyrId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to ExamFeeStructure
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_fee_structure_id")
	private ExamFeeStructure examFeeStructure;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;


	// bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_year_id")
	private CourseYear courseYear;

	public ExamFeeStructureCourseyr() {
	}

	public Long getExamFeeCourseyrId() {
		return examFeeCourseyrId;
	}

	public void setExamFeeCourseyrId(Long examFeeCourseyrId) {
		this.examFeeCourseyrId = examFeeCourseyrId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}



	public CourseYear getCourseYear() {
		return courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public ExamFeeStructure getExamFeeStructure() {
		return examFeeStructure;
	}

	public void setExamFeeStructure(ExamFeeStructure examFeeStructure) {
		this.examFeeStructure = examFeeStructure;
	}

}