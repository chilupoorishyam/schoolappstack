package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the t_inv_internal_return_items database table.
 * 
 */
@Entity
@Table(name="t_inv_internal_return_items")
@NamedQuery(name="InvInternalReturnItem.findAll", query="SELECT i FROM InvInternalReturnItem i")
public class InvInternalReturnItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_inter_return_item_id")
	private Long interReturnItemId;

	@Column(name="batch_no")
	private String batchNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="return_date")
	private Date returnDate;

	@Column(name="return_qty")
	private BigDecimal returnQty;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to InvInternalReturn
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_inter_return_id")
	private InvInternalReturn invInternalReturn;

	//bi-directional many-to-one association to InvInternalIssueItem
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_inter_issue_item_id")
	private InvInternalIssueItem invInternalIssueItem;

	//bi-directional many-to-one association to InvItemmaster
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_item_id")
	private InvItemmaster invItemmaster;

	//bi-directional many-to-one association to InvItemDetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_item_det_id")
	private InvItemDetail invItemDetail;
	
	@Transient
	private Long trakingItemDetailId;
	@Transient
	private Long trakingDeptId;
	@Transient
	private Long trakingEmployeeId;
	@Transient
	private Long trakingRoomId;

	public InvInternalReturnItem() {
	}

	public Long getInterReturnItemId() {
		return this.interReturnItemId;
	}

	public void setInterReturnItemId(Long interReturnItemId) {
		this.interReturnItemId = interReturnItemId;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getReturnDate() {
		return this.returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public BigDecimal getReturnQty() {
		return this.returnQty;
	}

	public void setReturnQty(BigDecimal returnQty) {
		this.returnQty = returnQty;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public InvInternalReturn getInvInternalReturn() {
		return invInternalReturn;
	}

	public void setInvInternalReturn(InvInternalReturn invInternalReturn) {
		this.invInternalReturn = invInternalReturn;
	}

	public InvInternalIssueItem getInvInternalIssueItem() {
		return invInternalIssueItem;
	}

	public void setInvInternalIssueItem(InvInternalIssueItem invInternalIssueItem) {
		this.invInternalIssueItem = invInternalIssueItem;
	}

	public InvItemmaster getInvItemmaster() {
		return invItemmaster;
	}

	public void setInvItemmaster(InvItemmaster invItemmaster) {
		this.invItemmaster = invItemmaster;
	}

	public InvItemDetail getInvItemDetail() {
		return invItemDetail;
	}

	public void setInvItemDetail(InvItemDetail invItemDetail) {
		this.invItemDetail = invItemDetail;
	}

	public Long getTrakingItemDetailId() {
		return trakingItemDetailId;
	}

	public void setTrakingItemDetailId(Long trakingItemDetailId) {
		this.trakingItemDetailId = trakingItemDetailId;
	}

	public Long getTrakingDeptId() {
		return trakingDeptId;
	}

	public void setTrakingDeptId(Long trakingDeptId) {
		this.trakingDeptId = trakingDeptId;
	}

	public Long getTrakingEmployeeId() {
		return trakingEmployeeId;
	}

	public void setTrakingEmployeeId(Long trakingEmployeeId) {
		this.trakingEmployeeId = trakingEmployeeId;
	}

	public Long getTrakingRoomId() {
		return trakingRoomId;
	}

	public void setTrakingRoomId(Long trakingRoomId) {
		this.trakingRoomId = trakingRoomId;
	}
}