package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_stg_m_subjects database table.
 * 
 */
@Entity
@Table(name="t_stg_m_subjects")
@NamedQuery(name="StgSubject.findAll", query="SELECT s FROM StgSubject s")
public class StgSubject implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_stg_subject_id")
	private Long stgSubjectId;

	@Column(name="campus_name")
	private String campusName;

	private String category;

	private String course;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="credit_hrs")
	private String creditHrs;

	private String credits;

	@Column(name="error_message")
	private String errorMessage;

	private Boolean iserrored;

	private Boolean islanguage;

	@Column(name="organization_name")
	private String organizationName;

	@Column(name="subject_code")
	private String subjectCode;

	@Column(name="subject_name")
	private String subjectName;

	@Column(name="subject_type")
	private String subjectType;

	//bi-directional many-to-one association to Bulkdataprocess
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_bd_process_id")
	private Bulkdataprocess bulkdataprocess;

	public StgSubject() {
	}

	public Long getStgSubjectId() {
		return this.stgSubjectId;
	}

	public void setStgSubjectId(Long stgSubjectId) {
		this.stgSubjectId = stgSubjectId;
	}

	public String getCampusName() {
		return this.campusName;
	}

	public void setCampusName(String campusName) {
		this.campusName = campusName;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCourse() {
		return this.course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getCreditHrs() {
		return this.creditHrs;
	}

	public void setCreditHrs(String creditHrs) {
		this.creditHrs = creditHrs;
	}

	public String getCredits() {
		return this.credits;
	}

	public void setCredits(String credits) {
		this.credits = credits;
	}

	public String getErrorMessage() {
		return this.errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Boolean getIserrored() {
		return this.iserrored;
	}

	public void setIserrored(Boolean iserrored) {
		this.iserrored = iserrored;
	}

	public Boolean getIslanguage() {
		return this.islanguage;
	}

	public void setIslanguage(Boolean islanguage) {
		this.islanguage = islanguage;
	}

	public String getOrganizationName() {
		return this.organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getSubjectCode() {
		return this.subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	public String getSubjectName() {
		return this.subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getSubjectType() {
		return this.subjectType;
	}

	public void setSubjectType(String subjectType) {
		this.subjectType = subjectType;
	}

	public Bulkdataprocess getBulkdataprocess() {
		return this.bulkdataprocess;
	}

	public void setBulkdataprocess(Bulkdataprocess bulkdataprocess) {
		this.bulkdataprocess = bulkdataprocess;
	}

}