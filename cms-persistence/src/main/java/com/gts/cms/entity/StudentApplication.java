package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_std_applications database table.
 * 
 */
@Entity
@Table(name="t_std_applications")
@NamedQuery(name="StudentApplication.findAll", query="SELECT s FROM StudentApplication s")
public class StudentApplication implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_app_id")
	private Long studentAppId;

	@Column(name="aadhar_card_no")
	private String aadharCardNo;

	@Column(name="aadhar_file_path")
	private String aadharFilePath;

	@Temporal(TemporalType.DATE)
	@Column(name="adminssion_date")
	private Date adminssionDate;

	@Column(name="admission_number")
	private String admissionNumber;

	@Column(name="application_no")
	private String applicationNo;

	@Column(name="biometric_no")
	private String biometricNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	@Column(name="date_of_birth")
	private Date dateOfBirth;

	@Temporal(TemporalType.DATE)
	@Column(name="date_of_expiry")
	private Date dateOfExpiry;

	@Temporal(TemporalType.DATE)
	@Column(name="date_of_issue")
	private Date dateOfIssue;

	@Temporal(TemporalType.DATE)
	@Column(name="date_of_registration")
	private Date dateOfRegistration;

	@Column(name="eamcet_rank")
	private String eamcetRank;

	@Column(name="entrance_ht_no")
	private String entranceHtNo;

	@Column(name="father_address")
	private String fatherAddress;

	@Column(name="father_email_id")
	private String fatherEmailId;

	@Column(name="father_mobile_no")
	private String fatherMobileNo;

	@Column(name="father_name")
	private String fatherName;

	@Column(name="father_occupation")
	private String fatherOccupation;

	@Column(name="father_photo_path")
	private String fatherPhotoPath;

	@Column(name="father_qualification")
	private String fatherQualification;

	@Column(name="fathers_income_pa")
	private String fathersIncomePa;

	@Column(name="first_name")
	private String firstName;

	@Column(name="folder_path")
	private String folderPath;

	@Column(name="guardian_address")
	private String guardianAddress;

	@Column(name="guardian_email_id")
	private String guardianEmailId;

	@Column(name="guardian_income_pa")
	private String guardianIncomePa;

	@Column(name="guardian_mobile_no")
	private String guardianMobileNo;

	@Column(name="guardian_name")
	private String guardianName;

	@Column(name="hallticket_number")
	private String hallticketNumber;

	private String hobbies;

	@Column(name="identification_marks")
	private String identificationMarks;

	private String interests;

	@Column(name="is_active")
	private Boolean isActive;
	
	@Column(name="is_minority")
	private Boolean isMinority;

	@Column(name="is_current_year")
	private Boolean isCurrentYear;

	@Column(name="is_lateral")
	private Boolean isLateral;

	@Column(name="is_local")
	private Boolean isLocal;

	@Column(name="is_scholorship")
	private Boolean isScholorship;

	@Column(name="isgovtemp_father")
	private Boolean isgovtempFather;

	@Column(name="isgovtemp_mother")
	private Boolean isgovtempMother;

	@Column(name="lang_status1")
	private String langStatus1;

	@Column(name="lang_status2")
	private String langStatus2;

	@Column(name="lang_status3")
	private String langStatus3;

	@Column(name="lang_status4")
	private String langStatus4;

	@Column(name="lang_status5")
	private String langStatus5;

	@Column(name="last_name")
	private String lastName;

	@Column(name="middle_name")
	private String middleName;

	private String mobile;

	@Column(name="mother_address")
	private String motherAddress;

	@Column(name="mother_email_id")
	private String motherEmailId;

	@Column(name="mother_income_pa")
	private String motherIncomePa;

	@Column(name="mother_mobile_no")
	private String motherMobileNo;

	@Column(name="mother_name")
	private String motherName;

	@Column(name="mother_occupation")
	private String motherOccupation;

	@Column(name="mother_photo_path")
	private String motherPhotoPath;

	@Column(name="mother_qualification")
	private String motherQualification;

	@Column(name="pancard_file_path")
	private String pancardFilePath;

	@Column(name="pancard_no")
	private String pancardNo;

	@Column(name="passport_no")
	private String passportNo;

	@Column(name="permanent_address")
	private String permanentAddress;

	@Column(name="permanent_mandal")
	private String permanentMandal;

	@Column(name="permanent_pincode")
	private String permanentPincode;

	@Column(name="permanent_street")
	private String permanentStreet;

	@Column(name="present_address")
	private String presentAddress;

	@Column(name="present_mandal")
	private String presentMandal;

	@Column(name="present_pincode")
	private String presentPincode;

	@Column(name="present_street_name")
	private String presentStreetName;

	@Column(name="primary_contact")
	private String primaryContact;

	private String reason;

	@Column(name="receipt_no")
	private String receiptNo;

	@Column(name="ref_application_no")
	private String refApplicationNo;

	@Column(name="residence_phone")
	private String residencePhone;

	@Column(name="roll_number")
	private String rollNumber;

	@Column(name="ssc_no")
	private String sscNo;

	@Column(name="status_comments")
	private String statusComments;

	@Column(name="std_email_id")
	private String studentEmailId;

	@Column(name="student_photo_path")
	private String studentPhotoPath;
	
	@Column(name="wedding_date")
	private Date weddingDate;
	
	
	
	public Date getWeddingDate() {
		return weddingDate;
	}

	public void setWeddingDate(Date weddingDate) {
		this.weddingDate = weddingDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to StudentAppActivity
	  @OneToMany(mappedBy="studentApplication",cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	  private List<StudentAppActivity> stdAppActivities;
	 
	  //bi-directional many-to-one association to StudentAppDocCollection
	  @OneToMany(mappedBy="studentApplication",cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	  private List<StudentAppDocCollection> stdAppDocCollections;
	 
	  //bi-directional many-to-one association to StudentAppEducation
	  @OneToMany(mappedBy="studentApplication",cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	  private List<StudentAppEducation> stdAppEducations;
	 
	  //bi-directional many-to-one association to StudentAppWorkflow
	  @OneToMany(mappedBy="studentApplication",cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	  private List<StudentAppWorkflow> stdAppWorkflows;

	//bi-directional many-to-one association to Batch
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_batch_id")
	private Batch batch;

	//bi-directional many-to-one association to Caste
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_caste_id")
	private Caste caste;

	//bi-directional many-to-one association to City
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_city_present_id")
	private City presentCity;

	//bi-directional many-to-one association to City
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_city_permenent_id")
	private City permenentCity;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to Course
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_id")
	private Course course;


	//bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_year_id")
	private CourseYear courseYear;

	//bi-directional many-to-one association to District
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_district_present_id")
	private District presentDistrict;

	//bi-directional many-to-one association to District
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_district_permanent_id")
	private District permanentDistrict;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_disability_catdet_id")
	private GeneralDetail disability;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_nationality_catdet_id")
	private GeneralDetail nationality;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_religion_catdet_id")
	private GeneralDetail religion;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_bloodgroup_catdet_id")
	private GeneralDetail bloodGroup;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_language1_catdet_id")
	private GeneralDetail language1;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_language2_catdet_id")
	private GeneralDetail language2;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_language3_catdet_id")
	private GeneralDetail language3;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_language4_catdet_id")
	private GeneralDetail language4;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_language5_catdet_id")
	private GeneralDetail language5;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_quota_catdet_id")
	private GeneralDetail quota;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_stdtype_catdet_id")
	private GeneralDetail studenttype;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_title_catdet_id")
	private GeneralDetail title;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_gender_catdet_id")
	private GeneralDetail gender;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to SubCaste
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_sub_caste_id")
	private SubCaste subCaste;

	//bi-directional many-to-one association to WorkflowStage
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_application_status_id")
	private WorkflowStage workflowStage;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_qualifying_catdet_id")
	private GeneralDetail qualifyingCat;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;
	
	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="studentApplication",fetch = FetchType.LAZY)
	private List<StudentDetail> studentDetails;

	public StudentApplication() {
	}

	public Long getStudentAppId() {
		return this.studentAppId;
	}

	public void setStudentAppId(Long studentAppId) {
		this.studentAppId = studentAppId;
	}

	public String getAadharCardNo() {
		return this.aadharCardNo;
	}

	public void setAadharCardNo(String aadharCardNo) {
		this.aadharCardNo = aadharCardNo;
	}

	public String getAadharFilePath() {
		return this.aadharFilePath;
	}

	public void setAadharFilePath(String aadharFilePath) {
		this.aadharFilePath = aadharFilePath;
	}

	public Date getAdminssionDate() {
		return this.adminssionDate;
	}

	public void setAdminssionDate(Date adminssionDate) {
		this.adminssionDate = adminssionDate;
	}

	public String getAdmissionNumber() {
		return this.admissionNumber;
	}

	public void setAdmissionNumber(String admissionNumber) {
		this.admissionNumber = admissionNumber;
	}

	public String getApplicationNo() {
		return this.applicationNo;
	}

	public void setApplicationNo(String applicationNo) {
		this.applicationNo = applicationNo;
	}

	public String getBiometricNo() {
		return this.biometricNo;
	}

	public void setBiometricNo(String biometricNo) {
		this.biometricNo = biometricNo;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Date getDateOfExpiry() {
		return this.dateOfExpiry;
	}

	public void setDateOfExpiry(Date dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public Date getDateOfIssue() {
		return this.dateOfIssue;
	}

	public void setDateOfIssue(Date dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public Date getDateOfRegistration() {
		return this.dateOfRegistration;
	}

	public void setDateOfRegistration(Date dateOfRegistration) {
		this.dateOfRegistration = dateOfRegistration;
	}

	public String getEamcetRank() {
		return this.eamcetRank;
	}

	public void setEamcetRank(String eamcetRank) {
		this.eamcetRank = eamcetRank;
	}

	public String getEntranceHtNo() {
		return this.entranceHtNo;
	}

	public void setEntranceHtNo(String entranceHtNo) {
		this.entranceHtNo = entranceHtNo;
	}

	public String getFatherAddress() {
		return this.fatherAddress;
	}

	public void setFatherAddress(String fatherAddress) {
		this.fatherAddress = fatherAddress;
	}

	public String getFatherEmailId() {
		return this.fatherEmailId;
	}

	public void setFatherEmailId(String fatherEmailId) {
		this.fatherEmailId = fatherEmailId;
	}

	public String getFatherMobileNo() {
		return this.fatherMobileNo;
	}

	public void setFatherMobileNo(String fatherMobileNo) {
		this.fatherMobileNo = fatherMobileNo;
	}

	public String getFatherName() {
		return this.fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getFatherOccupation() {
		return this.fatherOccupation;
	}

	public void setFatherOccupation(String fatherOccupation) {
		this.fatherOccupation = fatherOccupation;
	}

	public String getFatherPhotoPath() {
		return this.fatherPhotoPath;
	}

	public void setFatherPhotoPath(String fatherPhotoPath) {
		this.fatherPhotoPath = fatherPhotoPath;
	}

	public String getFatherQualification() {
		return this.fatherQualification;
	}

	public void setFatherQualification(String fatherQualification) {
		this.fatherQualification = fatherQualification;
	}

	public String getFathersIncomePa() {
		return this.fathersIncomePa;
	}

	public void setFathersIncomePa(String fathersIncomePa) {
		this.fathersIncomePa = fathersIncomePa;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFolderPath() {
		return this.folderPath;
	}

	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}

	public String getGuardianAddress() {
		return this.guardianAddress;
	}

	public void setGuardianAddress(String guardianAddress) {
		this.guardianAddress = guardianAddress;
	}

	public String getGuardianEmailId() {
		return this.guardianEmailId;
	}

	public void setGuardianEmailId(String guardianEmailId) {
		this.guardianEmailId = guardianEmailId;
	}

	public String getGuardianIncomePa() {
		return this.guardianIncomePa;
	}

	public void setGuardianIncomePa(String guardianIncomePa) {
		this.guardianIncomePa = guardianIncomePa;
	}

	public String getGuardianMobileNo() {
		return this.guardianMobileNo;
	}

	public void setGuardianMobileNo(String guardianMobileNo) {
		this.guardianMobileNo = guardianMobileNo;
	}

	public String getGuardianName() {
		return this.guardianName;
	}

	public void setGuardianName(String guardianName) {
		this.guardianName = guardianName;
	}

	public String getHallticketNumber() {
		return this.hallticketNumber;
	}

	public void setHallticketNumber(String hallticketNumber) {
		this.hallticketNumber = hallticketNumber;
	}

	public String getHobbies() {
		return this.hobbies;
	}

	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}

	public String getIdentificationMarks() {
		return this.identificationMarks;
	}

	public void setIdentificationMarks(String identificationMarks) {
		this.identificationMarks = identificationMarks;
	}

	public String getInterests() {
		return this.interests;
	}

	public void setInterests(String interests) {
		this.interests = interests;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsCurrentYear() {
		return this.isCurrentYear;
	}

	public void setIsCurrentYear(Boolean isCurrentYear) {
		this.isCurrentYear = isCurrentYear;
	}

	public Boolean getIsLateral() {
		return this.isLateral;
	}

	public void setIsLateral(Boolean isLateral) {
		this.isLateral = isLateral;
	}

	public Boolean getIsLocal() {
		return this.isLocal;
	}

	public void setIsLocal(Boolean isLocal) {
		this.isLocal = isLocal;
	}

	public Boolean getIsScholorship() {
		return this.isScholorship;
	}

	public void setIsScholorship(Boolean isScholorship) {
		this.isScholorship = isScholorship;
	}

	public Boolean getIsgovtempFather() {
		return this.isgovtempFather;
	}

	public void setIsgovtempFather(Boolean isgovtempFather) {
		this.isgovtempFather = isgovtempFather;
	}

	public Boolean getIsgovtempMother() {
		return this.isgovtempMother;
	}

	public void setIsgovtempMother(Boolean isgovtempMother) {
		this.isgovtempMother = isgovtempMother;
	}

	public String getLangStatus1() {
		return this.langStatus1;
	}

	public void setLangStatus1(String langStatus1) {
		this.langStatus1 = langStatus1;
	}

	public String getLangStatus2() {
		return this.langStatus2;
	}

	public void setLangStatus2(String langStatus2) {
		this.langStatus2 = langStatus2;
	}

	public String getLangStatus3() {
		return this.langStatus3;
	}

	public void setLangStatus3(String langStatus3) {
		this.langStatus3 = langStatus3;
	}

	public String getLangStatus4() {
		return this.langStatus4;
	}

	public void setLangStatus4(String langStatus4) {
		this.langStatus4 = langStatus4;
	}

	public String getLangStatus5() {
		return this.langStatus5;
	}

	public void setLangStatus5(String langStatus5) {
		this.langStatus5 = langStatus5;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMotherAddress() {
		return this.motherAddress;
	}

	public void setMotherAddress(String motherAddress) {
		this.motherAddress = motherAddress;
	}

	public String getMotherEmailId() {
		return this.motherEmailId;
	}

	public void setMotherEmailId(String motherEmailId) {
		this.motherEmailId = motherEmailId;
	}

	public String getMotherIncomePa() {
		return this.motherIncomePa;
	}

	public void setMotherIncomePa(String motherIncomePa) {
		this.motherIncomePa = motherIncomePa;
	}

	public String getMotherMobileNo() {
		return this.motherMobileNo;
	}

	public void setMotherMobileNo(String motherMobileNo) {
		this.motherMobileNo = motherMobileNo;
	}

	public String getMotherName() {
		return this.motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getMotherOccupation() {
		return this.motherOccupation;
	}

	public void setMotherOccupation(String motherOccupation) {
		this.motherOccupation = motherOccupation;
	}

	public String getMotherPhotoPath() {
		return this.motherPhotoPath;
	}

	public void setMotherPhotoPath(String motherPhotoPath) {
		this.motherPhotoPath = motherPhotoPath;
	}

	public String getMotherQualification() {
		return this.motherQualification;
	}

	public void setMotherQualification(String motherQualification) {
		this.motherQualification = motherQualification;
	}

	public String getPancardFilePath() {
		return this.pancardFilePath;
	}

	public void setPancardFilePath(String pancardFilePath) {
		this.pancardFilePath = pancardFilePath;
	}

	public String getPancardNo() {
		return this.pancardNo;
	}

	public void setPancardNo(String pancardNo) {
		this.pancardNo = pancardNo;
	}

	public String getPassportNo() {
		return this.passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public String getPermanentAddress() {
		return this.permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getPermanentMandal() {
		return this.permanentMandal;
	}

	public void setPermanentMandal(String permanentMandal) {
		this.permanentMandal = permanentMandal;
	}

	public String getPermanentPincode() {
		return this.permanentPincode;
	}

	public void setPermanentPincode(String permanentPincode) {
		this.permanentPincode = permanentPincode;
	}

	public String getPermanentStreet() {
		return this.permanentStreet;
	}

	public void setPermanentStreet(String permanentStreet) {
		this.permanentStreet = permanentStreet;
	}

	public String getPresentAddress() {
		return this.presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getPresentMandal() {
		return this.presentMandal;
	}

	public void setPresentMandal(String presentMandal) {
		this.presentMandal = presentMandal;
	}

	public String getPresentPincode() {
		return this.presentPincode;
	}

	public void setPresentPincode(String presentPincode) {
		this.presentPincode = presentPincode;
	}

	public String getPresentStreetName() {
		return this.presentStreetName;
	}

	public void setPresentStreetName(String presentStreetName) {
		this.presentStreetName = presentStreetName;
	}

	public String getPrimaryContact() {
		return this.primaryContact;
	}

	public void setPrimaryContact(String primaryContact) {
		this.primaryContact = primaryContact;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReceiptNo() {
		return this.receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public String getRefApplicationNo() {
		return this.refApplicationNo;
	}

	public void setRefApplicationNo(String refApplicationNo) {
		this.refApplicationNo = refApplicationNo;
	}

	public String getResidencePhone() {
		return this.residencePhone;
	}

	public void setResidencePhone(String residencePhone) {
		this.residencePhone = residencePhone;
	}

	public String getRollNumber() {
		return this.rollNumber;
	}

	public void setRollNumber(String rollNumber) {
		this.rollNumber = rollNumber;
	}

	public String getSscNo() {
		return this.sscNo;
	}

	public void setSscNo(String sscNo) {
		this.sscNo = sscNo;
	}

	public String getStatusComments() {
		return this.statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public String getStudentEmailId() {
		return this.studentEmailId;
	}

	public void setStudentEmailId(String studentEmailId) {
		this.studentEmailId = studentEmailId;
	}

	public String getStudentPhotoPath() {
		return this.studentPhotoPath;
	}

	public void setStudentPhotoPath(String studentPhotoPath) {
		this.studentPhotoPath = studentPhotoPath;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<StudentAppActivity> getStdAppActivities() {
		return this.stdAppActivities;
	}

	public void setStdAppActivities(List<StudentAppActivity> stdAppActivities) {
		this.stdAppActivities = stdAppActivities;
	}

	public StudentAppActivity addStdAppActivity(StudentAppActivity stdAppActivity) {
		getStdAppActivities().add(stdAppActivity);
		stdAppActivity.setStudentApplication(this);

		return stdAppActivity;
	}

	public StudentAppActivity removeStdAppActivity(StudentAppActivity stdAppActivity) {
		getStdAppActivities().remove(stdAppActivity);
		stdAppActivity.setStudentApplication(null);

		return stdAppActivity;
	}

	public List<StudentAppDocCollection> getStdAppDocCollections() {
		return this.stdAppDocCollections;
	}

	public void setStdAppDocCollections(List<StudentAppDocCollection> stdAppDocCollections) {
		this.stdAppDocCollections = stdAppDocCollections;
	}

	public StudentAppDocCollection addStdAppDocCollection(StudentAppDocCollection stdAppDocCollection) {
		getStdAppDocCollections().add(stdAppDocCollection);
		stdAppDocCollection.setStudentApplication(this);

		return stdAppDocCollection;
	}

	public StudentAppDocCollection removeStdAppDocCollection(StudentAppDocCollection stdAppDocCollection) {
		getStdAppDocCollections().remove(stdAppDocCollection);
		stdAppDocCollection.setStudentApplication(null);

		return stdAppDocCollection;
	}

	public List<StudentAppEducation> getStdAppEducations() {
		return this.stdAppEducations;
	}

	public void setStdAppEducations(List<StudentAppEducation> stdAppEducations) {
		this.stdAppEducations = stdAppEducations;
	}

	public StudentAppEducation addStdAppEducation(StudentAppEducation stdAppEducation) {
		getStdAppEducations().add(stdAppEducation);
		stdAppEducation.setStudentApplication(this);

		return stdAppEducation;
	}

	public StudentAppEducation removeStdAppEducation(StudentAppEducation stdAppEducation) {
		getStdAppEducations().remove(stdAppEducation);
		stdAppEducation.setStudentApplication(null);

		return stdAppEducation;
	}

	public List<StudentAppWorkflow> getStdAppWorkflows() {
		return this.stdAppWorkflows;
	}

	public void setStdAppWorkflows(List<StudentAppWorkflow> stdAppWorkflows) {
		this.stdAppWorkflows = stdAppWorkflows;
	}

	public StudentAppWorkflow addStdAppWorkflow(StudentAppWorkflow stdAppWorkflow) {
		getStdAppWorkflows().add(stdAppWorkflow);
		stdAppWorkflow.setStudentApplication(this);

		return stdAppWorkflow;
	}

	public StudentAppWorkflow removeStdAppWorkflow(StudentAppWorkflow stdAppWorkflow) {
		getStdAppWorkflows().remove(stdAppWorkflow);
		stdAppWorkflow.setStudentApplication(null);

		return stdAppWorkflow;
	}

	public Batch getBatch() {
		return this.batch;
	}

	public void setBatch(Batch batch) {
		this.batch = batch;
	}

	public Caste getCaste() {
		return this.caste;
	}

	public void setCaste(Caste caste) {
		this.caste = caste;
	}

	public City getPresentCity() {
		return this.presentCity;
	}

	public void setPresentCity(City presentCity) {
		this.presentCity = presentCity;
	}

	public City getPermenentCity() {
		return this.permenentCity;
	}

	public void setPermenentCity(City permenentCity) {
		this.permenentCity = permenentCity;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Course getCourse() {
		return this.course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}



	public CourseYear getCourseYear() {
		return this.courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public District getPresentDistrict() {
		return this.presentDistrict;
	}

	public void setPresentDistrict(District presentDistrict) {
		this.presentDistrict = presentDistrict;
	}

	public District getPermanentDistrict() {
		return this.permanentDistrict;
	}

	public void setPermanentDistrict(District permanentDistrict) {
		this.permanentDistrict = permanentDistrict;
	}

	public GeneralDetail getDisability() {
		return this.disability;
	}

	public void setDisability(GeneralDetail disability) {
		this.disability = disability;
	}

	public GeneralDetail getNationality() {
		return this.nationality;
	}

	public void setNationality(GeneralDetail nationality) {
		this.nationality = nationality;
	}

	public GeneralDetail getReligion() {
		return this.religion;
	}

	public void setReligion(GeneralDetail religion) {
		this.religion = religion;
	}

	public GeneralDetail getBloodGroup() {
		return this.bloodGroup;
	}

	public void setBloodGroup(GeneralDetail bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public GeneralDetail getLanguage1() {
		return this.language1;
	}

	public void setLanguage1(GeneralDetail language1) {
		this.language1 = language1;
	}

	public GeneralDetail getLanguage2() {
		return this.language2;
	}

	public void setLanguage2(GeneralDetail language2) {
		this.language2 = language2;
	}

	public GeneralDetail getLanguage3() {
		return this.language3;
	}

	public void setLanguage3(GeneralDetail language3) {
		this.language3 = language3;
	}

	public GeneralDetail getLanguage4() {
		return this.language4;
	}

	public void setLanguage4(GeneralDetail language4) {
		this.language4 = language4;
	}

	public GeneralDetail getLanguage5() {
		return this.language5;
	}

	public void setLanguage5(GeneralDetail language5) {
		this.language5 = language5;
	}

	public GeneralDetail getQuota() {
		return this.quota;
	}

	public void setQuota(GeneralDetail quota) {
		this.quota = quota;
	}

	public GeneralDetail getStudenttype() {
		return this.studenttype;
	}

	public void setStudenttype(GeneralDetail studenttype) {
		this.studenttype = studenttype;
	}

	public GeneralDetail getTitle() {
		return this.title;
	}

	public void setTitle(GeneralDetail title) {
		this.title = title;
	}

	public GeneralDetail getGender() {
		return this.gender;
	}

	public void setGender(GeneralDetail gender) {
		this.gender = gender;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}


	public SubCaste getSubCaste() {
		return this.subCaste;
	}

	public void setSubCaste(SubCaste subCaste) {
		this.subCaste = subCaste;
	}

	public WorkflowStage getWorkflowStage() {
		return this.workflowStage;
	}

	public void setWorkflowStage(WorkflowStage workflowStage) {
		this.workflowStage = workflowStage;
	}

	public List<StudentDetail> getStudentDetails() {
		return this.studentDetails;
	}

	public void setStudentDetails(List<StudentDetail> studentDetails) {
		this.studentDetails = studentDetails;
	}

	public StudentDetail addStudentDetail(StudentDetail studentDetail) {
		getStudentDetails().add(studentDetail);
		studentDetail.setStudentApplication(this);

		return studentDetail;
	}

	public StudentDetail removeStudentDetail(StudentDetail studentDetail) {
		getStudentDetails().remove(studentDetail);
		studentDetail.setStudentApplication(null);

		return studentDetail;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public GeneralDetail getQualifyingCat() {
		return qualifyingCat;
	}

	public void setQualifyingCat(GeneralDetail qualifyingCat) {
		this.qualifyingCat = qualifyingCat;
	}

	public Boolean getIsMinority() {
		return isMinority;
	}

	public void setIsMinority(Boolean isMinority) {
		this.isMinority = isMinority;
	}
}