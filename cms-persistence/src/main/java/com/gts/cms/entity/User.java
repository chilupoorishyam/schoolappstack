package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_users database table.
 *
 */
@Entity
@Table(name="t_m_users")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_user_id")
	private Long userId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String email;

	@Column(name="first_name")
	private String firstName;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_user_type_id")
	private Usertype userType;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_editable")
	private Boolean isEditable;

	@Column(name="is_reset")
	private Boolean isReset;

	@Column(name="last_name")
	private String lastName;

	@Column(name="mobile_number")
	private String mobileNumber;

	private String password;

	@Column(name="password_attempts")
	private Integer passwordAttempts;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="password_exp_date")
	private Date passwordExpDate;

	private String reason;

	@Column(name="reset_password_code")
	private String resetPasswordCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	@Column(name="user_name")
	private String userName;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="user",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to Authorization
	@OneToMany(mappedBy="user",fetch = FetchType.LAZY)
	private List<Authorization> authorizations;

	//bi-directional many-to-one association to UserSchoolsGroup
	@OneToMany(mappedBy="user",fetch = FetchType.LAZY)
	private List<UserSchoolsGroup> userSchoolsGroups;

	//bi-directional many-to-one association to UserRole
	@OneToMany(mappedBy="user",fetch = FetchType.LAZY)
	private List<UserRole> userRoles;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="parentUser",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails1;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="user",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails2;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="mobile_app_last_login")
	private Date mobileAppLastLogin;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_login")
	private Date lastLogin;

	@Column(name="mobile_app_version")
	private String mobileAppVersion;

	public User() {
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Usertype getUserType() {
		return userType;
	}

	public void setUserType(Usertype userType) {
		this.userType = userType;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsEditable() {
		return this.isEditable;
	}

	public void setIsEditable(Boolean isEditable) {
		this.isEditable = isEditable;
	}

	public Boolean getIsReset() {
		return this.isReset;
	}

	public void setIsReset(Boolean isReset) {
		this.isReset = isReset;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobileNumber() {
		return this.mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getPasswordAttempts() {
		return this.passwordAttempts;
	}

	public void setPasswordAttempts(Integer passwordAttempts) {
		this.passwordAttempts = passwordAttempts;
	}

	public Date getPasswordExpDate() {
		return this.passwordExpDate;
	}

	public void setPasswordExpDate(Date passwordExpDate) {
		this.passwordExpDate = passwordExpDate;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getResetPasswordCode() {
		return this.resetPasswordCode;
	}

	public void setResetPasswordCode(String resetPasswordCode) {
		this.resetPasswordCode = resetPasswordCode;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<EmployeeDetail> getEmpDetails() {
		return this.empDetails;
	}

	public void setEmpDetails(List<EmployeeDetail> empDetails) {
		this.empDetails = empDetails;
	}

	public EmployeeDetail addEmpDetail(EmployeeDetail empDetail) {
		getEmpDetails().add(empDetail);
		empDetail.setUser(this);

		return empDetail;
	}

	public EmployeeDetail removeEmpDetail(EmployeeDetail empDetail) {
		getEmpDetails().remove(empDetail);
		empDetail.setUser(null);

		return empDetail;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public List<Authorization> getAuthorizations() {
		return this.authorizations;
	}

	public void setAuthorizations(List<Authorization> authorizations) {
		this.authorizations = authorizations;
	}

	public Authorization addAuthorization(Authorization authorization) {
		getAuthorizations().add(authorization);
		authorization.setUser(this);

		return authorization;
	}

	public Authorization removeAuthorization(Authorization authorization) {
		getAuthorizations().remove(authorization);
		authorization.setUser(null);

		return authorization;
	}

	public List<UserSchoolsGroup> getUserSchoolsGroups() {
		return this.userSchoolsGroups;
	}

	public void setUserSchoolsGroups(List<UserSchoolsGroup> userSchoolsGroups) {
		this.userSchoolsGroups = userSchoolsGroups;
	}

	public UserSchoolsGroup addUserSchoolsGroup(UserSchoolsGroup userSchoolsGroup) {
		getUserSchoolsGroups().add(userSchoolsGroup);
		userSchoolsGroup.setUser(this);

		return userSchoolsGroup;
	}

	public UserSchoolsGroup removeUserSchoolsGroup(UserSchoolsGroup userSchoolsGroup) {
		getUserSchoolsGroups().remove(userSchoolsGroup);
		userSchoolsGroup.setUser(null);

		return userSchoolsGroup;
	}

	public List<UserRole> getUserRoles() {
		return this.userRoles;
	}

	public void setUserRoles(List<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	public UserRole addUserRole(UserRole userRole) {
		getUserRoles().add(userRole);
		userRole.setUser(this);

		return userRole;
	}

	public UserRole removeUserRole(UserRole userRole) {
		getUserRoles().remove(userRole);
		userRole.setUser(null);

		return userRole;
	}

	public List<StudentDetail> getStdStudentDetails1() {
		return this.stdStudentDetails1;
	}

	public void setStdStudentDetails1(List<StudentDetail> stdStudentDetails1) {
		this.stdStudentDetails1 = stdStudentDetails1;
	}

	public StudentDetail addStdStudentDetails1(StudentDetail stdStudentDetails1) {
		getStdStudentDetails1().add(stdStudentDetails1);
		stdStudentDetails1.setParentUser(this);

		return stdStudentDetails1;
	}

	public StudentDetail removeStdStudentDetails1(StudentDetail stdStudentDetails1) {
		getStdStudentDetails1().remove(stdStudentDetails1);
		stdStudentDetails1.setParentUser(null);

		return stdStudentDetails1;
	}

	public List<StudentDetail> getStdStudentDetails2() {
		return this.stdStudentDetails2;
	}

	public void setStdStudentDetails2(List<StudentDetail> stdStudentDetails2) {
		this.stdStudentDetails2 = stdStudentDetails2;
	}

	public StudentDetail addStdStudentDetails2(StudentDetail stdStudentDetails2) {
		getStdStudentDetails2().add(stdStudentDetails2);
		stdStudentDetails2.setUser(this);

		return stdStudentDetails2;
	}

	public StudentDetail removeStdStudentDetails2(StudentDetail stdStudentDetails2) {
		getStdStudentDetails2().remove(stdStudentDetails2);
		stdStudentDetails2.setUser(null);

		return stdStudentDetails2;
	}

	public Date getMobileAppLastLogin() {
		return mobileAppLastLogin;
	}

	public void setMobileAppLastLogin(Date mobileAppLastLogin) {
		this.mobileAppLastLogin = mobileAppLastLogin;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getMobileAppVersion() {
		return mobileAppVersion;
	}

	public void setMobileAppVersion(String mobileAppVersion) {
		this.mobileAppVersion = mobileAppVersion;
	}


}