package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_tt_std_attendance database table.
 * 
 */
@Entity
@Table(name="t_tt_std_attendance")
@NamedQuery(name="StudentAttendance.findAll", query="SELECT s FROM StudentAttendance s")
public class StudentAttendance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_std_attendance_id")
	private Long stdAttendanceId;

	@Column(name="absent_reason")
	private String absentReason;

	@Temporal(TemporalType.DATE)
	@Column(name="attendance_date")
	private Date attendanceDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_absent")
	private Boolean isAbsent;

	@Column(name="is_absent_verified")
	private Boolean isAbsentVerified;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	@Column(name="verified_comments")
	private String verifiedComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="verified_date")
	private Date verifiedDate;

	/*//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_takenby_emp_id")
	private EmployeeDetail takenbyEmployeeDetail;*/

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_verifiedby_emp_id")
	private EmployeeDetail verifiedbyEmployeeDetail;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_student_id")
	private StudentDetail studentDetail;

	//bi-directional many-to-one association to ActualClassesSchedule
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_actual_cls_schedule_id")
	private ActualClassesSchedule actualClassesSchedule;

	/*//bi-directional many-to-one association to SubjectResource
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_subject_resource_id")
	private SubjectResource subjectResource;*/
	
	
	//Added by  Naveen on 16-11-2018
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_timetable_schedule_id")
	private Schedule schedule;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subject_id")
	private Subject subject;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_cls_takenby_emp_id")
	private EmployeeDetail classTakenByEmployee;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_attendance_takenby_emp_id")
	private EmployeeDetail attendanceTakenByEmployee;
	
	@Column(name="is_all_present")
	private Boolean isAllPresent;
	
	@Column(name="is_freezed")
	private Boolean isFreezed;

	public StudentAttendance() {
	}

	public Long getStdAttendanceId() {
		return this.stdAttendanceId;
	}

	public void setStdAttendanceId(Long stdAttendanceId) {
		this.stdAttendanceId = stdAttendanceId;
	}

	public String getAbsentReason() {
		return this.absentReason;
	}

	public void setAbsentReason(String absentReason) {
		this.absentReason = absentReason;
	}

	public Date getAttendanceDate() {
		return this.attendanceDate;
	}

	public void setAttendanceDate(Date attendanceDate) {
		this.attendanceDate = attendanceDate;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsAbsent() {
		return this.isAbsent;
	}

	public void setIsAbsent(Boolean isAbsent) {
		this.isAbsent = isAbsent;
	}

	public Boolean getIsAbsentVerified() {
		return this.isAbsentVerified;
	}

	public void setIsAbsentVerified(Boolean isAbsentVerified) {
		this.isAbsentVerified = isAbsentVerified;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getVerifiedComments() {
		return this.verifiedComments;
	}

	public void setVerifiedComments(String verifiedComments) {
		this.verifiedComments = verifiedComments;
	}

	public Date getVerifiedDate() {
		return this.verifiedDate;
	}

	public void setVerifiedDate(Date verifiedDate) {
		this.verifiedDate = verifiedDate;
	}

	/*public EmployeeDetail getTakenbyEmployeeDetail() {
		return this.takenbyEmployeeDetail;
	}

	public void setTakenbyEmployeeDetail(EmployeeDetail takenbyEmployeeDetail) {
		this.takenbyEmployeeDetail = takenbyEmployeeDetail;
	}*/

	public EmployeeDetail getVerifiedbyEmployeeDetail() {
		return this.verifiedbyEmployeeDetail;
	}

	public void setVerifiedbyEmployeeDetail(EmployeeDetail verifiedbyEmployeeDetail) {
		this.verifiedbyEmployeeDetail = verifiedbyEmployeeDetail;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public ActualClassesSchedule getActualClassesSchedule() {
		return this.actualClassesSchedule;
	}

	public void setActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		this.actualClassesSchedule = actualClassesSchedule;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public EmployeeDetail getClassTakenByEmployee() {
		return classTakenByEmployee;
	}

	public void setClassTakenByEmployee(EmployeeDetail classTakenByEmployee) {
		this.classTakenByEmployee = classTakenByEmployee;
	}

	public EmployeeDetail getAttendanceTakenByEmployee() {
		return attendanceTakenByEmployee;
	}

	public void setAttendanceTakenByEmployee(EmployeeDetail attendanceTakenByEmployee) {
		this.attendanceTakenByEmployee = attendanceTakenByEmployee;
	}

	public Boolean getIsAllPresent() {
		return isAllPresent;
	}

	public void setIsAllPresent(Boolean isAllPresent) {
		this.isAllPresent = isAllPresent;
	}

	public Boolean getIsFreezed() {
		return isFreezed;
	}

	public void setIsFreezed(Boolean isFreezed) {
		this.isFreezed = isFreezed;
	}
	
	

/*	public SubjectResource getSubjectResource() {
		return this.subjectResource;
	}

	public void setSubjectResource(SubjectResource subjectResource) {
		this.subjectResource = subjectResource;
	}
*/
	
	
}