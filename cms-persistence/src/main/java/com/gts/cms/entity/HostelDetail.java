package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_hstl_hostel_details database table.
 * 
 */
@Entity
@Table(name = "t_hstl_hostel_details")
@NamedQuery(name = "HostelDetail.findAll", query = "SELECT h FROM HostelDetail h")
public class HostelDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_hstl_hostel_id")
	private Long hostelId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "hostel_address")
	private String hostelAddress;

	@Column(name = "hostel_code")
	private String hostelCode;

	@Column(name = "hostel_name")
	private String hostelName;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "no_of_floors")
	private Long noOfFloors;

	@Column(name = "other_info")
	private String otherInfo;

	@Column(name = "phone_number")
	private String phoneNumber;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_hstl_for_catdet_id")
	private GeneralDetail hstlForCatdet;

	// bi-directional many-to-one association to HstlDiscount
	@OneToMany(mappedBy = "hostelDetail")
	private List<HostelDiscount> hstlDiscounts;

	// bi-directional many-to-one association to HstlFeePayment
	@OneToMany(mappedBy = "hostelDetail")
	private List<HostelFeePayment> hstlFeePayments;

	// bi-directional many-to-one association to HostelType
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_hstl_hostel_type_id")
	private HostelType hostelType;

	// bi-directional many-to-one association to HstlRegister
	@OneToMany(mappedBy = "hostelDetail")
	private List<HostelRegister> hostelRegisters;

	// bi-directional many-to-one association to HstlRoomAllocation
	@OneToMany(mappedBy = "hstlHostelDetail")
	private List<HostelRoomAllocation> hstlRoomAllocations;

	// bi-directional many-to-one association to HstlRoomCharge
	@OneToMany(mappedBy = "hostelDetail")
	private List<HostelRoomCharge> hstlRoomCharges;

	// bi-directional many-to-one association to HstlRoom
	@OneToMany(mappedBy = "hostelDetail")
	private List<HostelRoom> hstlRooms;

	// bi-directional many-to-one association to HstlVisitor
	@OneToMany(mappedBy = "hostelDetail")
	private List<HostelVisitor> hstlVisitors;

	public HostelDetail() {
	}

	public Long getHostelId() {
		return hostelId;
	}

	public void setHostelId(Long hostelId) {
		this.hostelId = hostelId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getHostelAddress() {
		return hostelAddress;
	}

	public void setHostelAddress(String hostelAddress) {
		this.hostelAddress = hostelAddress;
	}

	public String getHostelCode() {
		return hostelCode;
	}

	public void setHostelCode(String hostelCode) {
		this.hostelCode = hostelCode;
	}

	public String getHostelName() {
		return hostelName;
	}

	public void setHostelName(String hostelName) {
		this.hostelName = hostelName;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Long getNoOfFloors() {
		return noOfFloors;
	}

	public void setNoOfFloors(Long noOfFloors) {
		this.noOfFloors = noOfFloors;
	}

	public String getOtherInfo() {
		return otherInfo;
	}

	public void setOtherInfo(String otherInfo) {
		this.otherInfo = otherInfo;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public GeneralDetail getHstlForCatdet() {
		return hstlForCatdet;
	}

	public void setHstlForCatdet(GeneralDetail hstlForCatdet) {
		this.hstlForCatdet = hstlForCatdet;
	}

	public List<HostelDiscount> getHstlDiscounts() {
		return hstlDiscounts;
	}

	public void setHstlDiscounts(List<HostelDiscount> hstlDiscounts) {
		this.hstlDiscounts = hstlDiscounts;
	}

	public List<HostelFeePayment> getHstlFeePayments() {
		return hstlFeePayments;
	}

	public void setHstlFeePayments(List<HostelFeePayment> hstlFeePayments) {
		this.hstlFeePayments = hstlFeePayments;
	}

	public HostelType getHostelType() {
		return hostelType;
	}

	public void setHostelType(HostelType hostelType) {
		this.hostelType = hostelType;
	}

	public List<HostelRegister> getHostelRegisters() {
		return hostelRegisters;
	}

	public void setHostelRegisters(List<HostelRegister> hostelRegisters) {
		this.hostelRegisters = hostelRegisters;
	}

	public List<HostelRoomAllocation> getHstlRoomAllocations() {
		return hstlRoomAllocations;
	}

	public void setHstlRoomAllocations(List<HostelRoomAllocation> hstlRoomAllocations) {
		this.hstlRoomAllocations = hstlRoomAllocations;
	}

	public List<HostelRoomCharge> getHstlRoomCharges() {
		return hstlRoomCharges;
	}

	public void setHstlRoomCharges(List<HostelRoomCharge> hstlRoomCharges) {
		this.hstlRoomCharges = hstlRoomCharges;
	}

	public List<HostelRoom> getHstlRooms() {
		return hstlRooms;
	}

	public void setHstlRooms(List<HostelRoom> hstlRooms) {
		this.hstlRooms = hstlRooms;
	}

	public List<HostelVisitor> getHstlVisitors() {
		return hstlVisitors;
	}

	public void setHstlVisitors(List<HostelVisitor> hstlVisitors) {
		this.hstlVisitors = hstlVisitors;
	}
}