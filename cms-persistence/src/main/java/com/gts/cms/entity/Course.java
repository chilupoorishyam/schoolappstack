package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * The persistent class for the t_m_course database table.
 * 
 */
@Entity
@Table(name="t_m_course")
@NamedQuery(name="Course.findAll", query="SELECT c FROM Course c")
public class Course implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_course_id")
	private Long courseId;

	@Column(name="course_code")
	private String courseCode;

	@Column(name="course_name")
	private String courseName;

	@Column(name="course_shortname")
	private String courseShortname;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private Integer duration;

	private Integer intake;

	@Column(name="is_active")
	private Boolean isActive;

	private String prefix;

	private String reason;

	@Column(name="starting_no")
	private Integer startingNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to Batch
	@OneToMany(mappedBy="course",fetch = FetchType.LAZY)
	private List<Batch> batches;

	//bi-directional many-to-one association to ConfigAutonumber
	@OneToMany(mappedBy="course",fetch = FetchType.LAZY)
	private List<ConfigAutonumber> configAutonumbers;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;



	//bi-directional many-to-one association to CourseYear
	@OneToMany(mappedBy="course",fetch = FetchType.LAZY)
	@OrderBy("sortOrder ASC")
	private Set<CourseYear> courseYears;

	//bi-directional many-to-one association to DocumentRepository
	@OneToMany(mappedBy="course",fetch = FetchType.LAZY)
	private List<DocumentRepository> documentRepositories;

	//bi-directional many-to-one association to Electivetype
	@OneToMany(mappedBy="course",fetch = FetchType.LAZY)
	private List<Electivetype> electivetypes;

	//bi-directional many-to-one association to EventAudience
	@OneToMany(mappedBy="course",fetch = FetchType.LAZY)
	private List<EventAudience> eventAudiences;



	//bi-directional many-to-one association to Studentbatch
	@OneToMany(mappedBy="course",fetch = FetchType.LAZY)
	private List<Studentbatch> studentbatches;

	//bi-directional many-to-one association to Subject
	@OneToMany(mappedBy="course",fetch = FetchType.LAZY)
	private List<Subject> subjects;

	//bi-directional many-to-one association to UserSchoolsGroup
	@OneToMany(mappedBy="course",fetch = FetchType.LAZY)
	private List<UserSchoolsGroup> userSchoolsGroups;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="course",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications;

	//bi-directional many-to-one association to StudentEnquiry
	@OneToMany(mappedBy="course",fetch = FetchType.LAZY)
	private List<StudentEnquiry> stdEnquiries;

	//bi-directional many-to-one association to StudentAcademicbatch
	@OneToMany(mappedBy="course",fetch = FetchType.LAZY)
	private List<StudentAcademicbatch> studentAcademicbatches;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="course",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails;

	//bi-directional many-to-one association to StudentSubject
	@OneToMany(mappedBy="course",fetch = FetchType.LAZY)
	private List<StudentSubject> studentSubjects;

	public Course() {
	}

	public Long getCourseId() {
		return this.courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getCourseCode() {
		return this.courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public String getCourseName() {
		return this.courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseShortname() {
		return this.courseShortname;
	}

	public void setCourseShortname(String courseShortname) {
		this.courseShortname = courseShortname;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Integer getDuration() {
		return this.duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Integer getIntake() {
		return this.intake;
	}

	public void setIntake(Integer intake) {
		this.intake = intake;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getPrefix() {
		return this.prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getStartingNo() {
		return this.startingNo;
	}

	public void setStartingNo(Integer startingNo) {
		this.startingNo = startingNo;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<Batch> getBatches() {
		return this.batches;
	}

	public void setBatches(List<Batch> batches) {
		this.batches = batches;
	}

	public Batch addBatch(Batch batch) {
		getBatches().add(batch);
		batch.setCourse(this);

		return batch;
	}

	public Batch removeBatch(Batch batch) {
		getBatches().remove(batch);
		batch.setCourse(null);

		return batch;
	}

	public List<ConfigAutonumber> getConfigAutonumbers() {
		return this.configAutonumbers;
	}

	public void setConfigAutonumbers(List<ConfigAutonumber> configAutonumbers) {
		this.configAutonumbers = configAutonumbers;
	}

	public ConfigAutonumber addConfigAutonumber(ConfigAutonumber configAutonumber) {
		getConfigAutonumbers().add(configAutonumber);
		configAutonumber.setCourse(this);

		return configAutonumber;
	}

	public ConfigAutonumber removeConfigAutonumber(ConfigAutonumber configAutonumber) {
		getConfigAutonumbers().remove(configAutonumber);
		configAutonumber.setCourse(null);

		return configAutonumber;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	/*public CourseYear addCourseYear(CourseYear courseYear) {
		getCourseYears().add(courseYear);
		courseYear.setCourse(this);

		return courseYear;
	}

	public CourseYear removeCourseYear(CourseYear courseYear) {
		getCourseYears().remove(courseYear);
		courseYear.setCourse(null);

		return courseYear;
	}*/

	public Set<CourseYear> getCourseYears() {
		return courseYears;
	}

	public void setCourseYears(Set<CourseYear> courseYears) {
		this.courseYears = courseYears;
	}

	public List<DocumentRepository> getDocumentRepositories() {
		return this.documentRepositories;
	}

	public void setDocumentRepositories(List<DocumentRepository> documentRepositories) {
		this.documentRepositories = documentRepositories;
	}

	public DocumentRepository addDocumentRepository(DocumentRepository documentRepository) {
		getDocumentRepositories().add(documentRepository);
		documentRepository.setCourse(this);

		return documentRepository;
	}

	public DocumentRepository removeDocumentRepository(DocumentRepository documentRepository) {
		getDocumentRepositories().remove(documentRepository);
		documentRepository.setCourse(null);

		return documentRepository;
	}

	public List<Electivetype> getElectivetypes() {
		return this.electivetypes;
	}

	public void setElectivetypes(List<Electivetype> electivetypes) {
		this.electivetypes = electivetypes;
	}

	public Electivetype addElectivetype(Electivetype electivetype) {
		getElectivetypes().add(electivetype);
		electivetype.setCourse(this);

		return electivetype;
	}

	public Electivetype removeElectivetype(Electivetype electivetype) {
		getElectivetypes().remove(electivetype);
		electivetype.setCourse(null);

		return electivetype;
	}

	public List<EventAudience> getEventAudiences() {
		return this.eventAudiences;
	}

	public void setEventAudiences(List<EventAudience> eventAudiences) {
		this.eventAudiences = eventAudiences;
	}

	public EventAudience addEventAudience(EventAudience eventAudience) {
		getEventAudiences().add(eventAudience);
		eventAudience.setCourse(this);

		return eventAudience;
	}

	public EventAudience removeEventAudience(EventAudience eventAudience) {
		getEventAudiences().remove(eventAudience);
		eventAudience.setCourse(null);

		return eventAudience;
	}



	public List<Studentbatch> getStudentbatches() {
		return this.studentbatches;
	}

	public void setStudentbatches(List<Studentbatch> studentbatches) {
		this.studentbatches = studentbatches;
	}

	public Studentbatch addStudentbatch(Studentbatch studentbatch) {
		getStudentbatches().add(studentbatch);
		studentbatch.setCourse(this);

		return studentbatch;
	}

	public Studentbatch removeStudentbatch(Studentbatch studentbatch) {
		getStudentbatches().remove(studentbatch);
		studentbatch.setCourse(null);

		return studentbatch;
	}

	public List<Subject> getSubjects() {
		return this.subjects;
	}

	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}

	public Subject addSubject(Subject subject) {
		getSubjects().add(subject);
		subject.setCourse(this);

		return subject;
	}

	public Subject removeSubject(Subject subject) {
		getSubjects().remove(subject);
		subject.setCourse(null);

		return subject;
	}

	public List<UserSchoolsGroup> getUserSchoolsGroups() {
		return this.userSchoolsGroups;
	}

	public void setUserSchoolsGroups(List<UserSchoolsGroup> userSchoolsGroups) {
		this.userSchoolsGroups = userSchoolsGroups;
	}

	public UserSchoolsGroup addUserSchoolsGroup(UserSchoolsGroup userSchoolsGroup) {
		getUserSchoolsGroups().add(userSchoolsGroup);
		userSchoolsGroup.setCourse(this);

		return userSchoolsGroup;
	}

	public UserSchoolsGroup removeUserSchoolsGroup(UserSchoolsGroup userSchoolsGroup) {
		getUserSchoolsGroups().remove(userSchoolsGroup);
		userSchoolsGroup.setCourse(null);

		return userSchoolsGroup;
	}

	public List<StudentApplication> getStdApplications() {
		return this.stdApplications;
	}

	public void setStdApplications(List<StudentApplication> stdApplications) {
		this.stdApplications = stdApplications;
	}

	public StudentApplication addStdApplication(StudentApplication stdApplication) {
		getStdApplications().add(stdApplication);
		stdApplication.setCourse(this);

		return stdApplication;
	}

	public StudentApplication removeStdApplication(StudentApplication stdApplication) {
		getStdApplications().remove(stdApplication);
		stdApplication.setCourse(null);

		return stdApplication;
	}

	public List<StudentEnquiry> getStdEnquiries() {
		return this.stdEnquiries;
	}

	public void setStdEnquiries(List<StudentEnquiry> stdEnquiries) {
		this.stdEnquiries = stdEnquiries;
	}

	public StudentEnquiry addStdEnquiry(StudentEnquiry stdEnquiry) {
		getStdEnquiries().add(stdEnquiry);
		stdEnquiry.setCourse(this);

		return stdEnquiry;
	}

	public StudentEnquiry removeStdEnquiry(StudentEnquiry stdEnquiry) {
		getStdEnquiries().remove(stdEnquiry);
		stdEnquiry.setCourse(null);

		return stdEnquiry;
	}

	public List<StudentAcademicbatch> getStudentAcademicbatches() {
		return this.studentAcademicbatches;
	}

	public void setStudentAcademicbatches(List<StudentAcademicbatch> studentAcademicbatches) {
		this.studentAcademicbatches = studentAcademicbatches;
	}

	public StudentAcademicbatch addStudentAcademicbatch(StudentAcademicbatch studentAcademicbatch) {
		getStudentAcademicbatches().add(studentAcademicbatch);
		studentAcademicbatch.setCourse(this);

		return studentAcademicbatch;
	}

	public StudentAcademicbatch removeStudentAcademicbatch(StudentAcademicbatch studentAcademicbatch) {
		getStudentAcademicbatches().remove(studentAcademicbatch);
		studentAcademicbatch.setCourse(null);

		return studentAcademicbatch;
	}

	public List<StudentDetail> getStdStudentDetails() {
		return this.stdStudentDetails;
	}

	public void setStdStudentDetails(List<StudentDetail> stdStudentDetails) {
		this.stdStudentDetails = stdStudentDetails;
	}

	public StudentDetail addStdStudentDetail(StudentDetail stdStudentDetail) {
		getStdStudentDetails().add(stdStudentDetail);
		stdStudentDetail.setCourse(this);

		return stdStudentDetail;
	}

	public StudentDetail removeStdStudentDetail(StudentDetail stdStudentDetail) {
		getStdStudentDetails().remove(stdStudentDetail);
		stdStudentDetail.setCourse(null);

		return stdStudentDetail;
	}

	public List<StudentSubject> getStudentSubjects() {
		return this.studentSubjects;
	}

	public void setStudentSubjects(List<StudentSubject> studentSubjects) {
		this.studentSubjects = studentSubjects;
	}

	public StudentSubject addStudentSubject(StudentSubject studentSubject) {
		getStudentSubjects().add(studentSubject);
		studentSubject.setCourse(this);

		return studentSubject;
	}

	public StudentSubject removeStudentSubject(StudentSubject studentSubject) {
		getStudentSubjects().remove(studentSubject);
		studentSubject.setCourse(null);

		return studentSubject;
	}

}