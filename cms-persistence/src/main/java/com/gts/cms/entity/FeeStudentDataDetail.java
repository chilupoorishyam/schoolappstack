package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the t_fee_student_data_details database table.
 * 
 */
@Entity
@Table(name="t_fee_student_data_details")
@NamedQuery(name="FeeStudentDataDetail.findAll", query="SELECT f FROM FeeStudentDataDetail f")
public class FeeStudentDataDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_std_data_details_id")
	private Long feeStdDataDetailsId;

	private BigDecimal amount;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="fk_feedata_type_value_id")
	private Long feedataTypeValueId;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FeeStudentData
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_std_data_id")
	private FeeStudentData feeStudentData;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_feedata_type_catdet_id")
	private GeneralDetail feedataType;

	public FeeStudentDataDetail() {
	}

	public Long getFeeStdDataDetailsId() {
		return this.feeStdDataDetailsId;
	}

	public void setFeeStdDataDetailsId(Long feeStdDataDetailsId) {
		this.feeStdDataDetailsId = feeStdDataDetailsId;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Long getFeedataTypeValueId() {
		return this.feedataTypeValueId;
	}

	public void setFeedataTypeValueId(Long feedataTypeValueId) {
		this.feedataTypeValueId = feedataTypeValueId;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public FeeStudentData getFeeStudentData() {
		return this.feeStudentData;
	}

	public void setFeeStudentData(FeeStudentData feeStudentData) {
		this.feeStudentData = feeStudentData;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getFeedataType() {
		return this.feedataType;
	}

	public void setFeedataType(GeneralDetail feedataType) {
		this.feedataType = feedataType;
	}

}