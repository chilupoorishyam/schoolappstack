package com.gts.cms.entity;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TopicType {
    VIDEO("VIDEO"),
    ZOOM("MEETING");

    @JsonValue
    String topicType;
}
