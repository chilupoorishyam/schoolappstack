package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_hr_survey_feedback database table.
 * 
 */
@Entity
@Table(name="t_hr_survey_feedback")
@NamedQuery(name="SurveyFeedback.findAll", query="SELECT s FROM SurveyFeedback s")
public class SurveyFeedback implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_survery_fb_id")
	private Long surveryFbId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="feedback_date")
	private Date feedbackDate;

	@Column(name="is_active")
	private Boolean isActive;

	private Boolean iscompleted;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fb_from_emp_id")
	private EmployeeDetail fromEmployeeDetail;
	
	// bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_academic_year_id")
	private AcademicYear academicYear;
	
	// bi-directional many-to-one association to Subject
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_subject_id")
	private Subject subject;
	
	// bi-directional many-to-one association to GroupSection
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_group_section_id")
	private GroupSection groupSection;
	
	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fb_for_emp_id")
	private EmployeeDetail forEmployeeDetail;

	//bi-directional many-to-one association to SurveyForm
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_survey_form_id")
	private SurveyForm surveyForm;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fb_from_student_id")
	private StudentDetail fromStudentDetail;

	//bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fb_for_student_id")
	private StudentDetail forStudentDetail;

	//bi-directional many-to-one association to SurveyFeedbackDetail
	@OneToMany(mappedBy="surveyFeedback",fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	private List<SurveyFeedbackDetail> surveyFeedbackDetails;

	public SurveyFeedback() {
	}

	public Long getSurveryFbId() {
		return this.surveryFbId;
	}

	public void setSurveryFbId(Long surveryFbId) {
		this.surveryFbId = surveryFbId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getFeedbackDate() {
		return this.feedbackDate;
	}

	public void setFeedbackDate(Date feedbackDate) {
		this.feedbackDate = feedbackDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIscompleted() {
		return this.iscompleted;
	}

	public void setIscompleted(Boolean iscompleted) {
		this.iscompleted = iscompleted;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getFromEmployeeDetail() {
		return this.fromEmployeeDetail;
	}

	public void setFromEmployeeDetail(EmployeeDetail fromEmployeeDetail) {
		this.fromEmployeeDetail = fromEmployeeDetail;
	}

	public EmployeeDetail getForEmployeeDetail() {
		return this.forEmployeeDetail;
	}

	public void setForEmployeeDetail(EmployeeDetail forEmployeeDetail) {
		this.forEmployeeDetail = forEmployeeDetail;
	}

	public SurveyForm getSurveyForm() {
		return this.surveyForm;
	}

	public void setSurveyForm(SurveyForm surveyForm) {
		this.surveyForm = surveyForm;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public StudentDetail getFromStudentDetail() {
		return this.fromStudentDetail;
	}

	public void setFromStudentDetail(StudentDetail fromStudentDetail) {
		this.fromStudentDetail = fromStudentDetail;
	}

	public StudentDetail getForStudentDetail() {
		return this.forStudentDetail;
	}

	public void setForStudentDetail(StudentDetail forStudentDetail) {
		this.forStudentDetail = forStudentDetail;
	}

	public List<SurveyFeedbackDetail> getSurveyFeedbackDetails() {
		return this.surveyFeedbackDetails;
	}

	public void setSurveyFeedbackDetails(List<SurveyFeedbackDetail> surveyFeedbackDetails) {
		this.surveyFeedbackDetails = surveyFeedbackDetails;
	}

	public SurveyFeedbackDetail addSurveyFeedbackDetail(SurveyFeedbackDetail surveyFeedbackDetail) {
		getSurveyFeedbackDetails().add(surveyFeedbackDetail);
		surveyFeedbackDetail.setSurveyFeedback(this);

		return surveyFeedbackDetail;
	}

	public SurveyFeedbackDetail removeSurveyFeedbackDetail(SurveyFeedbackDetail surveyFeedbackDetail) {
		getSurveyFeedbackDetails().remove(surveyFeedbackDetail);
		surveyFeedbackDetail.setSurveyFeedback(null);

		return surveyFeedbackDetail;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public GroupSection getGroupSection() {
		return groupSection;
	}

	public void setGroupSection(GroupSection groupSection) {
		this.groupSection = groupSection;
	}
}