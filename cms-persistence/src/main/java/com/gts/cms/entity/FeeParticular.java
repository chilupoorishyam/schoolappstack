package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the t_fee_particulars database table.
 * 
 */
@Entity
@Table(name="t_fee_particulars")
@NamedQuery(name="FeeParticular.findAll", query="SELECT f FROM FeeParticular f")
public class FeeParticular implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_particulars_id")
	private Long feeParticularsId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String description;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="particulars_name")
	private String particularsName;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FeeDiscountsParticular
	@OneToMany(mappedBy="feeParticular",fetch = FetchType.LAZY)
	private List<FeeDiscountsParticular> feeDiscountsParticulars;

	//bi-directional many-to-one association to FeeFine
	@OneToMany(mappedBy="feeParticular",fetch = FetchType.LAZY)
	private List<FeeFine> feeFines;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to FeeParticularwisePayment
	@OneToMany(mappedBy="feeParticular",fetch = FetchType.LAZY)
	private List<FeeParticularwisePayment> feeParticularwisePayments;

	//bi-directional many-to-one association to FeeStructureParticular
	@OneToMany(mappedBy="feeParticular",fetch = FetchType.LAZY)
	private List<FeeStructureParticular> feeStructureParticulars;

	//bi-directional many-to-one association to FeeStudentWiseParticular
	@OneToMany(mappedBy="feeParticular",fetch = FetchType.LAZY)
	private List<FeeStudentWiseParticular> feeStudentWiseParticulars;

	//bi-directional many-to-one association to FeeStudentwiseDiscount
	@OneToMany(mappedBy="feeParticular",fetch = FetchType.LAZY)
	private List<FeeStudentwiseDiscount> feeStudentwiseDiscounts;

	//bi-directional many-to-one association to FeeStudentwiseFine
	@OneToMany(mappedBy="feeParticular",fetch = FetchType.LAZY)
	private List<FeeStudentwiseFine> feeStudentwiseFines;

	//bi-directional many-to-one association to FeeTransactionDetail
	@OneToMany(mappedBy="feeParticular",fetch = FetchType.LAZY)
	private List<FeeTransactionDetail> feeTransactionDetails;
	
	/*@Column(name="is_scholarship")
	private Boolean isScholarship;*/
	
	@Column(name=" is_hostel")
	private Boolean  isHostel;
	
	@Column(name=" is_transport")
	private Boolean  isTransport;
	
	@Column(name=" is_library")
	private Boolean  isLibrary;
	
	//Added by Naveen on .4-12-2018
	
	@Column(name="particulars_code")
	private String particularsCode;

	public FeeParticular() {
	}

	public Long getFeeParticularsId() {
		return this.feeParticularsId;
	}

	public void setFeeParticularsId(Long feeParticularsId) {
		this.feeParticularsId = feeParticularsId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getParticularsName() {
		return this.particularsName;
	}

	public void setParticularsName(String particularsName) {
		this.particularsName = particularsName;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<FeeDiscountsParticular> getFeeDiscountsParticulars() {
		return this.feeDiscountsParticulars;
	}

	public void setFeeDiscountsParticulars(List<FeeDiscountsParticular> feeDiscountsParticulars) {
		this.feeDiscountsParticulars = feeDiscountsParticulars;
	}

	public FeeDiscountsParticular addFeeDiscountsParticular(FeeDiscountsParticular feeDiscountsParticular) {
		getFeeDiscountsParticulars().add(feeDiscountsParticular);
		feeDiscountsParticular.setFeeParticular(this);

		return feeDiscountsParticular;
	}

	public FeeDiscountsParticular removeFeeDiscountsParticular(FeeDiscountsParticular feeDiscountsParticular) {
		getFeeDiscountsParticulars().remove(feeDiscountsParticular);
		feeDiscountsParticular.setFeeParticular(null);

		return feeDiscountsParticular;
	}

	public List<FeeFine> getFeeFines() {
		return this.feeFines;
	}

	public void setFeeFines(List<FeeFine> feeFines) {
		this.feeFines = feeFines;
	}

	public FeeFine addFeeFine(FeeFine feeFine) {
		getFeeFines().add(feeFine);
		feeFine.setFeeParticular(this);

		return feeFine;
	}

	public FeeFine removeFeeFine(FeeFine feeFine) {
		getFeeFines().remove(feeFine);
		feeFine.setFeeParticular(null);

		return feeFine;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<FeeParticularwisePayment> getFeeParticularwisePayments() {
		return this.feeParticularwisePayments;
	}

	public void setFeeParticularwisePayments(List<FeeParticularwisePayment> feeParticularwisePayments) {
		this.feeParticularwisePayments = feeParticularwisePayments;
	}

	public FeeParticularwisePayment addFeeParticularwisePayment(FeeParticularwisePayment feeParticularwisePayment) {
		getFeeParticularwisePayments().add(feeParticularwisePayment);
		feeParticularwisePayment.setFeeParticular(this);

		return feeParticularwisePayment;
	}

	public FeeParticularwisePayment removeFeeParticularwisePayment(FeeParticularwisePayment feeParticularwisePayment) {
		getFeeParticularwisePayments().remove(feeParticularwisePayment);
		feeParticularwisePayment.setFeeParticular(null);

		return feeParticularwisePayment;
	}

	public List<FeeStructureParticular> getFeeStructureParticulars() {
		return this.feeStructureParticulars;
	}

	public void setFeeStructureParticulars(List<FeeStructureParticular> feeStructureParticulars) {
		this.feeStructureParticulars = feeStructureParticulars;
	}

	public FeeStructureParticular addFeeStructureParticular(FeeStructureParticular feeStructureParticular) {
		getFeeStructureParticulars().add(feeStructureParticular);
		feeStructureParticular.setFeeParticular(this);

		return feeStructureParticular;
	}

	public FeeStructureParticular removeFeeStructureParticular(FeeStructureParticular feeStructureParticular) {
		getFeeStructureParticulars().remove(feeStructureParticular);
		feeStructureParticular.setFeeParticular(null);

		return feeStructureParticular;
	}

	public List<FeeStudentWiseParticular> getFeeStudentWiseParticulars() {
		return this.feeStudentWiseParticulars;
	}

	public void setFeeStudentWiseParticulars(List<FeeStudentWiseParticular> feeStudentWiseParticulars) {
		this.feeStudentWiseParticulars = feeStudentWiseParticulars;
	}

	public FeeStudentWiseParticular addFeeStudentWiseParticular(FeeStudentWiseParticular feeStudentWiseParticular) {
		getFeeStudentWiseParticulars().add(feeStudentWiseParticular);
		feeStudentWiseParticular.setFeeParticular(this);

		return feeStudentWiseParticular;
	}

	public FeeStudentWiseParticular removeFeeStudentWiseParticular(FeeStudentWiseParticular feeStudentWiseParticular) {
		getFeeStudentWiseParticulars().remove(feeStudentWiseParticular);
		feeStudentWiseParticular.setFeeParticular(null);

		return feeStudentWiseParticular;
	}

	public List<FeeStudentwiseDiscount> getFeeStudentwiseDiscounts() {
		return this.feeStudentwiseDiscounts;
	}

	public void setFeeStudentwiseDiscounts(List<FeeStudentwiseDiscount> feeStudentwiseDiscounts) {
		this.feeStudentwiseDiscounts = feeStudentwiseDiscounts;
	}

	public FeeStudentwiseDiscount addFeeStudentwiseDiscount(FeeStudentwiseDiscount feeStudentwiseDiscount) {
		getFeeStudentwiseDiscounts().add(feeStudentwiseDiscount);
		feeStudentwiseDiscount.setFeeParticular(this);

		return feeStudentwiseDiscount;
	}

	public FeeStudentwiseDiscount removeFeeStudentwiseDiscount(FeeStudentwiseDiscount feeStudentwiseDiscount) {
		getFeeStudentwiseDiscounts().remove(feeStudentwiseDiscount);
		feeStudentwiseDiscount.setFeeParticular(null);

		return feeStudentwiseDiscount;
	}

	public List<FeeStudentwiseFine> getFeeStudentwiseFines() {
		return this.feeStudentwiseFines;
	}

	public void setFeeStudentwiseFines(List<FeeStudentwiseFine> feeStudentwiseFines) {
		this.feeStudentwiseFines = feeStudentwiseFines;
	}

	public FeeStudentwiseFine addFeeStudentwiseFine(FeeStudentwiseFine feeStudentwiseFine) {
		getFeeStudentwiseFines().add(feeStudentwiseFine);
		feeStudentwiseFine.setFeeParticular(this);

		return feeStudentwiseFine;
	}

	public FeeStudentwiseFine removeFeeStudentwiseFine(FeeStudentwiseFine feeStudentwiseFine) {
		getFeeStudentwiseFines().remove(feeStudentwiseFine);
		feeStudentwiseFine.setFeeParticular(null);

		return feeStudentwiseFine;
	}

	public List<FeeTransactionDetail> getFeeTransactionDetails() {
		return this.feeTransactionDetails;
	}

	public void setFeeTransactionDetails(List<FeeTransactionDetail> feeTransactionDetails) {
		this.feeTransactionDetails = feeTransactionDetails;
	}

	public FeeTransactionDetail addFeeTransactionDetail(FeeTransactionDetail feeTransactionDetail) {
		getFeeTransactionDetails().add(feeTransactionDetail);
		feeTransactionDetail.setFeeParticular(this);

		return feeTransactionDetail;
	}

	public FeeTransactionDetail removeFeeTransactionDetail(FeeTransactionDetail feeTransactionDetail) {
		getFeeTransactionDetails().remove(feeTransactionDetail);
		feeTransactionDetail.setFeeParticular(null);

		return feeTransactionDetail;
	}

	public String getParticularsCode() {
		return particularsCode;
	}

	public void setParticularsCode(String particularsCode) {
		this.particularsCode = particularsCode;
	}

	/*public Boolean getIsScholarship() {
		return isScholarship;
	}

	public void setIsScholarship(Boolean isScholarship) {
		this.isScholarship = isScholarship;
	}*/

	public Boolean getIsHostel() {
		return isHostel;
	}

	public void setIsHostel(Boolean isHostel) {
		this.isHostel = isHostel;
	}

	public Boolean getIsTransport() {
		return isTransport;
	}

	public void setIsTransport(Boolean isTransport) {
		this.isTransport = isTransport;
	}

	public Boolean getIsLibrary() {
		return isLibrary;
	}

	public void setIsLibrary(Boolean isLibrary) {
		this.isLibrary = isLibrary;
	}

	
}