package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_ams_membership_invitations database table.
 * 
 */
@Entity
@Table(name = "t_ams_membership_invitations")
@NamedQuery(name = "MembershipInvitation.findAll", query = "SELECT m FROM MembershipInvitation m")
public class MembershipInvitation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_ams_member_inv_id")
	private Long memberInvId;

	@ManyToOne
	@JoinColumn(name = "fk_ams_group_id")
	private AmsGroup amsGroup;

	@ManyToOne
	@JoinColumn(name = "fk_invitationby_profile_id")
	private AmsProfileDetail invitationByProfile;

	@ManyToOne
	@JoinColumn(name = "fk_invitationto_profile_id")
	private AmsProfileDetail invitationToProfile;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "invitation_on")
	private Date invitationOn;

	@Column(name = "is_joined")
	private Boolean isJoined;

	@Column(name = "is_rejected")
	private Boolean isRejected;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "response_date")
	private Date responseDate;

	// bi-directional many-to-one association to Network
	@ManyToOne
	@JoinColumn(name = "fk_ams_network_id")
	private Network network;

	public MembershipInvitation() {
	}

	public Long getMemberInvId() {
		return this.memberInvId;
	}

	public void setMemberInvId(Long memberInvId) {
		this.memberInvId = memberInvId;
	}

	public Date getInvitationOn() {
		return this.invitationOn;
	}

	public void setInvitationOn(Date invitationOn) {
		this.invitationOn = invitationOn;
	}

	public Boolean getIsJoined() {
		return this.isJoined;
	}

	public void setIsJoined(Boolean isJoined) {
		this.isJoined = isJoined;
	}

	public Boolean getIsRejected() {
		return this.isRejected;
	}

	public void setIsRejected(Boolean isRejected) {
		this.isRejected = isRejected;
	}

	public Date getResponseDate() {
		return this.responseDate;
	}

	public void setResponseDate(Date responseDate) {
		this.responseDate = responseDate;
	}

	public AmsGroup getAmsGroup() {
		return amsGroup;
	}

	public void setAmsGroup(AmsGroup amsGroup) {
		this.amsGroup = amsGroup;
	}

	public AmsProfileDetail getInvitationByProfile() {
		return invitationByProfile;
	}

	public void setInvitationByProfile(AmsProfileDetail invitationByProfile) {
		this.invitationByProfile = invitationByProfile;
	}

	public AmsProfileDetail getInvitationToProfile() {
		return invitationToProfile;
	}

	public void setInvitationToProfile(AmsProfileDetail invitationToProfile) {
		this.invitationToProfile = invitationToProfile;
	}

	public Network getNetwork() {
		return network;
	}

	public void setNetwork(Network network) {
		this.network = network;
	}

}