package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_designations database table.
 * 
 */
@Entity
@Table(name="t_m_designations")
@NamedQuery(name="Designation.findAll", query="SELECT d FROM Designation d")
public class Designation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_designation_id")
	private Long designationId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="designation_name")
	private String designationName;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="designation",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails1;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="workingDesignation",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails2;

	//bi-directional many-to-one association to EmployeeReporting
	@OneToMany(mappedBy="designation",fetch = FetchType.LAZY)
	private List<EmployeeReporting> empEmployeeReportings;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	public Designation() {
	}

	public Long getDesignationId() {
		return this.designationId;
	}

	public void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDesignationName() {
		return this.designationName;
	}

	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<EmployeeDetail> getEmpDetails1() {
		return this.empDetails1;
	}

	public void setEmpDetails1(List<EmployeeDetail> empDetails1) {
		this.empDetails1 = empDetails1;
	}

	public EmployeeDetail addEmpDetails1(EmployeeDetail empDetails1) {
		getEmpDetails1().add(empDetails1);
		empDetails1.setDesignation(this);

		return empDetails1;
	}

	public EmployeeDetail removeEmpDetails1(EmployeeDetail empDetails1) {
		getEmpDetails1().remove(empDetails1);
		empDetails1.setDesignation(null);

		return empDetails1;
	}

	public List<EmployeeDetail> getEmpDetails2() {
		return this.empDetails2;
	}

	public void setEmpDetails2(List<EmployeeDetail> empDetails2) {
		this.empDetails2 = empDetails2;
	}

	public EmployeeDetail addEmpDetails2(EmployeeDetail empDetails2) {
		getEmpDetails2().add(empDetails2);
		empDetails2.setWorkingDesignation(this);

		return empDetails2;
	}

	public EmployeeDetail removeEmpDetails2(EmployeeDetail empDetails2) {
		getEmpDetails2().remove(empDetails2);
		empDetails2.setWorkingDesignation(null);

		return empDetails2;
	}

	public List<EmployeeReporting> getEmpEmployeeReportings() {
		return this.empEmployeeReportings;
	}

	public void setEmpEmployeeReportings(List<EmployeeReporting> empEmployeeReportings) {
		this.empEmployeeReportings = empEmployeeReportings;
	}

	public EmployeeReporting addEmpEmployeeReporting(EmployeeReporting empEmployeeReporting) {
		getEmpEmployeeReportings().add(empEmployeeReporting);
		empEmployeeReporting.setDesignation(this);

		return empEmployeeReporting;
	}

	public EmployeeReporting removeEmpEmployeeReporting(EmployeeReporting empEmployeeReporting) {
		getEmpEmployeeReportings().remove(empEmployeeReporting);
		empEmployeeReporting.setDesignation(null);

		return empEmployeeReporting;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

}