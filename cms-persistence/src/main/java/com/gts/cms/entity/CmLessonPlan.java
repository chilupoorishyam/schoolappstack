package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_cm_lesson_plan database
 * table.
 */
@Data
@Entity
@Table(name = "t_cm_lesson_plan")
@NamedQuery(name = "CMLessonPlan.findAll", query = "SELECT e FROM CmLessonPlan e")
public class CmLessonPlan implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_lesson_plan_id")
    private Long lessonPlanId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "unit")
    private String unit;

    @Column(name = "sort_order")
    private Integer sortOrder;

    @Column(name = "topic_concept")
    private String topicConcept;

    @Column(name = "fk_teaching_method_catdet_id")
    private String teachingMethodCatdetId;

    @Column(name = "no_of_periods")
    private Integer noOfPeriods;

    @Column(name = "notes")
    private String notes;

    @Column(name = "quiz")
    private String quiz;

    @Column(name = "is_active")
    private Boolean isActive;
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "inserted_dt")
    private Date insertedDt;

    @Column(name = "inserted_user")
    private Long insertedUser;

    // bi-directional many-to-one association to TMSchool
    @ManyToOne
    @JoinColumn(name = "fk_school_id")
    private School school;

    @ManyToOne
    @JoinColumn(name = "fk_academic_year_id")
    private AcademicYear academicYear;

    // bi-directional many-to-one association to TMCourseYear
    @ManyToOne
    @JoinColumn(name = "fk_course_year_id")
    private CourseYear courseYear;

    // bi-directional many-to-one association to TMSubject
    @ManyToOne
    @JoinColumn(name = "fk_subject_id")
    private Subject subject;

    // bi-directional many-to-one association to TMSubject
    @ManyToOne
    @JoinColumn(name = "fk_emp_id")
    private EmployeeDetail employeeDetail;


}