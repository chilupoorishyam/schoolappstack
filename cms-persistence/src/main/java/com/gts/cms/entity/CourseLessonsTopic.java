package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_dl_course_lessons_topics database table.
 * 
 */
@Entity
@Table(name = "t_dl_course_lessons_topics")
@NamedQuery(name = "CourseLessonsTopic.findAll", query = "SELECT c FROM CourseLessonsTopic c")
public class CourseLessonsTopic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_dl_course_lesson_topic_id")
	private Long courseLessonTopicId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne
	@JoinColumn(name = "fk_sub_unit_topic_id")
	private SubjectUnitTopic subUnitTopic;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "lesson_duration_minutes")
	private Integer lessonDurationMinutes;

	private String notes;

	private String reason;

	@Column(name = "ref_doc_url")
	private String refDocUrl;

	@Column(name = "topic_name")
	private String topicName;

	@Column(name = "topic_no")
	private Integer topicNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "video_url")
	private String videoUrl;

	// bi-directional many-to-one association to CourseDiscussion
	@OneToMany(mappedBy = "courseLessonsTopic",cascade = CascadeType.ALL)
	private List<CourseDiscussion> courseDiscussions;

	// bi-directional many-to-one association to CourseLesson
	@ManyToOne
	@JoinColumn(name = "fk_dl_course_lesson_id")
	private CourseLesson courseLesson;

	// bi-directional many-to-one association to CourseTopicDetail
	@OneToMany(mappedBy = "courseLessonsTopic",cascade = CascadeType.ALL)
	private List<CourseTopicDetail> courseTopicDetails;

	// bi-directional many-to-one association to TDlMemberLearningNote
	@OneToMany(mappedBy = "courseLessonsTopic",cascade = CascadeType.ALL)
	private List<MemberLearningNote> memberLearningNotes;

	public CourseLessonsTopic() {
	}

	public Long getCourseLessonTopicId() {
		return this.courseLessonTopicId;
	}

	public void setCourseLessonTopicId(Long courseLessonTopicId) {
		this.courseLessonTopicId = courseLessonTopicId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getLessonDurationMinutes() {
		return this.lessonDurationMinutes;
	}

	public void setLessonDurationMinutes(Integer lessonDurationMinutes) {
		this.lessonDurationMinutes = lessonDurationMinutes;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRefDocUrl() {
		return this.refDocUrl;
	}

	public void setRefDocUrl(String refDocUrl) {
		this.refDocUrl = refDocUrl;
	}

	public String getTopicName() {
		return this.topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public Integer getTopicNo() {
		return this.topicNo;
	}

	public void setTopicNo(Integer topicNo) {
		this.topicNo = topicNo;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getVideoUrl() {
		return this.videoUrl;
	}

	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}

	public SubjectUnitTopic getSubUnitTopic() {
		return subUnitTopic;
	}

	public void setSubUnitTopic(SubjectUnitTopic subUnitTopic) {
		this.subUnitTopic = subUnitTopic;
	}

	public List<CourseDiscussion> getCourseDiscussions() {
		return courseDiscussions;
	}

	public void setCourseDiscussions(List<CourseDiscussion> courseDiscussions) {
		this.courseDiscussions = courseDiscussions;
	}

	public CourseLesson getCourseLesson() {
		return courseLesson;
	}

	public void setCourseLesson(CourseLesson courseLesson) {
		this.courseLesson = courseLesson;
	}

	public List<CourseTopicDetail> getCourseTopicDetails() {
		return courseTopicDetails;
	}

	public void setCourseTopicDetails(List<CourseTopicDetail> courseTopicDetails) {
		this.courseTopicDetails = courseTopicDetails;
	}

	public List<MemberLearningNote> getMemberLearningNotes() {
		return memberLearningNotes;
	}

	public void setMemberLearningNotes(List<MemberLearningNote> memberLearningNotes) {
		this.memberLearningNotes = memberLearningNotes;
	}

}