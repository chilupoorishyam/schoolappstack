package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "t_hr_emp_perf_subj_passpercentage")
@NamedQuery(name="EmpPerfSubjPasspercentage.findAll", query="SELECT e FROM EmpPerfSubjPasspercentage e")
public class EmpPerfSubjPasspercentage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_emp_perf_subj_passpercent_id", nullable = false)
    private Long empPerfSubjPasspercentId;

    @ManyToOne
    @JoinColumn(name = "fk_assessment_feedback_id", nullable = false)
    private EmpPerfAssessmentFeedback fkAssessmentFeedbackId;

    @ManyToOne
    @JoinColumn(name = "fk_emp_id", nullable = false)
    private EmployeeDetail empId;

    @ManyToOne
    @JoinColumn(name = "fk_subject_id", nullable = false)
    private Subject subjectId;

    @ManyToOne
    @JoinColumn(name = "fk_subjecttype_catdet_id", nullable = false)
    private GeneralDetail subjectTypeCatdetId;

    @Column(name = "student_appeared")
    private Integer studentAppeared;

    @Column(name = "student_passed")
    private Integer studentPassed;

    @Column(name = "pass_percentage")
    private Integer passPercentage;

    @Column(name = "division1")
    private String division1;

    @Column(name = "division2")
    private String division2;

    @Column(name = "division3")
    private String division3;

    @Column(name = "is_active", nullable = false)
    private Boolean active;

    @Column(name = "reason")
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt", nullable = false)
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    public Long getEmpPerfSubjPasspercentId() {
        return empPerfSubjPasspercentId;
    }

    public void setEmpPerfSubjPasspercentId(Long empPerfSubjPasspercentId) {
        this.empPerfSubjPasspercentId = empPerfSubjPasspercentId;
    }

    public EmpPerfAssessmentFeedback getFkAssessmentFeedbackId() {
        return fkAssessmentFeedbackId;
    }

    public void setFkAssessmentFeedbackId(EmpPerfAssessmentFeedback fkAssessmentFeedbackId) {
        this.fkAssessmentFeedbackId = fkAssessmentFeedbackId;
    }

    public EmployeeDetail getEmpId() {
        return empId;
    }

    public void setEmpId(EmployeeDetail empId) {
        this.empId = empId;
    }

    public Subject getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Subject subjectId) {
        this.subjectId = subjectId;
    }

    public GeneralDetail getSubjectTypeCatdetId() {
        return subjectTypeCatdetId;
    }

    public void setSubjectTypeCatdetId(GeneralDetail subjectTypeCatdetId) {
        this.subjectTypeCatdetId = subjectTypeCatdetId;
    }

    public Integer getStudentAppeared() {
        return studentAppeared;
    }

    public void setStudentAppeared(Integer studentAppeared) {
        this.studentAppeared = studentAppeared;
    }

    public Integer getStudentPassed() {
        return studentPassed;
    }

    public void setStudentPassed(Integer studentPassed) {
        this.studentPassed = studentPassed;
    }

    public Integer getPassPercentage() {
        return passPercentage;
    }

    public void setPassPercentage(Integer passPercentage) {
        this.passPercentage = passPercentage;
    }

    public String getDivision1() {
        return division1;
    }

    public void setDivision1(String division1) {
        this.division1 = division1;
    }

    public String getDivision2() {
        return division2;
    }

    public void setDivision2(String division2) {
        this.division2 = division2;
    }

    public String getDivision3() {
        return division3;
    }

    public void setDivision3(String division3) {
        this.division3 = division3;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    public Long getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(Long createdUser) {
        this.createdUser = createdUser;
    }

    public Date getUpdatedDt() {
        return updatedDt;
    }

    public void setUpdatedDt(Date updatedDt) {
        this.updatedDt = updatedDt;
    }

    public Long getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(Long updatedUser) {
        this.updatedUser = updatedUser;
    }
}
