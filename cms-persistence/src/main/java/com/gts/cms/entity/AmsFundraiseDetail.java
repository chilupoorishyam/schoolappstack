package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the t_ams_fundraise_details database table.
 * 
 */
@Entity
@Table(name = "t_ams_fundraise_details")
@NamedQuery(name = "AmsFundraiseDetail.findAll", query = "SELECT f FROM AmsFundraiseDetail f")
public class AmsFundraiseDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_fundraise_detail_id")
	private Long fundraiseDetailId;

	@Temporal(TemporalType.DATE)
	@Column(name = "audit_on")
	private Date auditOn;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne
	@JoinColumn(name = "fk_ams_profile_id")
	private AmsProfileDetail profileDetail;

	@ManyToOne
	@JoinColumn(name = "fk_audit_emp_id")
	private EmployeeDetail auditEmp;

	@ManyToOne
	@JoinColumn(name = "fk_emp_id")
	private EmployeeDetail employeeDetail;

	@ManyToOne
	@JoinColumn(name = "fk_paymentmode_catdet_id")
	private GeneralDetail paymentmodeCat;

	@ManyToOne
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	@Column(name = "fundraise_amount")
	private BigDecimal fundraiseAmount;

	@Temporal(TemporalType.DATE)
	@Column(name = "fundraised_on")
	private Date fundraisedOn;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_audit_completed")
	private Boolean isAuditCompleted;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to AmsFundraiseEvent
	@ManyToOne
	@JoinColumn(name = "fk_fundraise_event_id")
	private AmsFundraiseEvent fundraiseEvent;

	public AmsFundraiseDetail() {
	}

	public Long getFundraiseDetailId() {
		return this.fundraiseDetailId;
	}

	public void setFundraiseDetailId(Long fundraiseDetailId) {
		this.fundraiseDetailId = fundraiseDetailId;
	}

	public Date getAuditOn() {
		return this.auditOn;
	}

	public void setAuditOn(Date auditOn) {
		this.auditOn = auditOn;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public BigDecimal getFundraiseAmount() {
		return this.fundraiseAmount;
	}

	public void setFundraiseAmount(BigDecimal fundraiseAmount) {
		this.fundraiseAmount = fundraiseAmount;
	}

	public Date getFundraisedOn() {
		return this.fundraisedOn;
	}

	public void setFundraisedOn(Date fundraisedOn) {
		this.fundraisedOn = fundraisedOn;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsAuditCompleted() {
		return this.isAuditCompleted;
	}

	public void setIsAuditCompleted(Boolean isAuditCompleted) {
		this.isAuditCompleted = isAuditCompleted;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public AmsProfileDetail getProfileDetail() {
		return profileDetail;
	}

	public void setProfileDetail(AmsProfileDetail profileDetail) {
		this.profileDetail = profileDetail;
	}

	public EmployeeDetail getAuditEmp() {
		return auditEmp;
	}

	public void setAuditEmp(EmployeeDetail auditEmp) {
		this.auditEmp = auditEmp;
	}

	public EmployeeDetail getEmployeeDetail() {
		return employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public GeneralDetail getPaymentmodeCat() {
		return paymentmodeCat;
	}

	public void setPaymentmodeCat(GeneralDetail paymentmodeCat) {
		this.paymentmodeCat = paymentmodeCat;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public AmsFundraiseEvent getFundraiseEvent() {
		return fundraiseEvent;
	}

	public void setFundraiseEvent(AmsFundraiseEvent fundraiseEvent) {
		this.fundraiseEvent = fundraiseEvent;
	}

}