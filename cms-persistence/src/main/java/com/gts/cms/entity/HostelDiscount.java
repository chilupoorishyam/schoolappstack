package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_hstl_discounts database table.
 * 
 */
@Entity
@Table(name = "t_hstl_discounts")
@NamedQuery(name = "HostelDiscount.findAll", query = "SELECT h FROM HostelDiscount h")
public class HostelDiscount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_hstl_discount_id")
	private Long hstlDiscountId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "discount_type")
	private String discountType;

	@Column(name = "discount_value")
	private BigDecimal discountValue;

	@Column(name = "hstl_discount_name")
	private String hstlDiscountName;

	@Column(name = "is_active")
	private Boolean isActive;

	private Long noofmonths;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "valid_from")
	private Date validFrom;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "valid_to")
	private Date validTo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	// bi-directional many-to-one association to HostelRoom
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_hstl_room_id")
	private HostelRoom hstlRoom;

	// bi-directional many-to-one association to HostelDetail
	//@ManyToOne(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_hstl_hostel_id")
	private HostelDetail hostelDetail;

	public HostelDiscount() {
	}

	public Long getHstlDiscountId() {
		return this.hstlDiscountId;
	}

	public void setHstlDiscountId(Long hstlDiscountId) {
		this.hstlDiscountId = hstlDiscountId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDiscountType() {
		return this.discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public BigDecimal getDiscountValue() {
		return this.discountValue;
	}

	public void setDiscountValue(BigDecimal discountValue) {
		this.discountValue = discountValue;
	}

	public HostelRoom getHstlRoom() {
		return hstlRoom;
	}

	public void setHstlRoom(HostelRoom hstlRoom) {
		this.hstlRoom = hstlRoom;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public String getHstlDiscountName() {
		return this.hstlDiscountName;
	}

	public void setHstlDiscountName(String hstlDiscountName) {
		this.hstlDiscountName = hstlDiscountName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Long getNoofmonths() {
		return this.noofmonths;
	}

	public void setNoofmonths(Long noofmonths) {
		this.noofmonths = noofmonths;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public HostelDetail getHostelDetail() {
		return this.hostelDetail;
	}

	public void setHostelDetail(HostelDetail hostelDetail) {
		this.hostelDetail = hostelDetail;
	}

}