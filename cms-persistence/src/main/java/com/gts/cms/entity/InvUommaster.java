package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_inv_uommaster database table.
 * 
 */
@Entity
@Table(name = "t_inv_uommaster")
@NamedQuery(name = "InvUommaster.findAll", query = "SELECT i FROM InvUommaster i")
public class InvUommaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_uom_id")
	private Long uomId;

	private Integer conversionqty;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Column(name = "uom_code")
	private String uomCode;

	@Column(name = "uom_name")
	private String uomName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;
/*
	// bi-directional many-to-one association to InvPoItem
	@OneToMany(mappedBy = "TInvUommaster")
	private List<InvPoItem> invPoItems;

	// bi-directional many-to-one association to InvSrvItem
	@OneToMany(mappedBy = "TInvUommaster")
	private List<InvSrvItem> invSrvItems;

	// bi-directional many-to-one association to InvStoreItem
	@OneToMany(mappedBy = "TInvUommaster")
	private List<InvStoreItem> invStoreItems;*/

	// bi-directional many-to-one association to InvUommaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_parent_uom_id")
	private InvUommaster invUommaster;

	// bi-directional many-to-one association to InvUommaster
	@OneToMany(mappedBy = "invUommaster")
	private List<InvUommaster> invUommasters;

	public InvUommaster() {
	}

	public Long getUomId() {
		return uomId;
	}

	public void setUomId(Long uomId) {
		this.uomId = uomId;
	}

	
	public Integer getConversionqty() {
		return conversionqty;
	}

	public void setConversionqty(Integer conversionqty) {
		this.conversionqty = conversionqty;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getUomCode() {
		return uomCode;
	}

	public void setUomCode(String uomCode) {
		this.uomCode = uomCode;
	}

	public String getUomName() {
		return uomName;
	}

	public void setUomName(String uomName) {
		this.uomName = uomName;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}
/*
	public List<InvPoItem> getInvPoItems() {
		return invPoItems;
	}

	public void setInvPoItems(List<InvPoItem> invPoItems) {
		this.invPoItems = invPoItems;
	}

	public List<InvSrvItem> getInvSrvItems() {
		return invSrvItems;
	}

	public void setInvSrvItems(List<InvSrvItem> invSrvItems) {
		this.invSrvItems = invSrvItems;
	}

	public List<InvStoreItem> getInvStoreItems() {
		return invStoreItems;
	}

	public void setInvStoreItems(List<InvStoreItem> invStoreItems) {
		this.invStoreItems = invStoreItems;
	}*/

	public InvUommaster getInvUommaster() {
		return invUommaster;
	}

	public void setInvUommaster(InvUommaster invUommaster) {
		this.invUommaster = invUommaster;
	}

	/*public List<InvUommaster> getInvUommasters() {
		return invUommasters;
	}

	public void setInvUommasters(List<InvUommaster> invUommasters) {
		this.invUommasters = invUommasters;
	}*/

}