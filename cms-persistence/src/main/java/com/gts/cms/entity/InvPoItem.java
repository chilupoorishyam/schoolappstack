package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_inv_po_items database table.
 * 
 */
@Entity
@Table(name = "t_inv_po_items")
@NamedQuery(name = "InvPoItem.findAll", query = "SELECT i FROM InvPoItem i")
public class InvPoItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_po_item_id")
	private Long poItemId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_free")
	private Boolean isFree;

	@Column(name = "item_discount_percentage")
	private BigDecimal itemDiscountPercentage;

	@Column(name = "item_total_actual_amount")
	private BigDecimal itemTotalActualAmount;

	@Column(name = "item_total_cost")
	private BigDecimal itemTotalCost;

	@Column(name = "item_total_discount_amount")
	private BigDecimal itemTotalDiscountAmount;

	@Column(name = "item_total_tax_amount")
	private BigDecimal itemTotalTaxAmount;

	@Column(name = "order_quantity")
	private BigDecimal orderQuantity;

	private String reason;

	@Column(name = "received_qty")
	private BigDecimal receivedQty;

	@Column(name = "unit_price")
	private BigDecimal unitPrice;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "batch_no")
	private String batchNo;

	// bi-directional many-to-one association to InvPurchaseOrder
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_po_id")
	private InvPurchaseOrder invPurchaseOrder;

	// bi-directional many-to-one association to InvItemmaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_item_id")
	private InvItemmaster invItemMaster;

	// bi-directional many-to-one association to InvUommaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_uom_id")
	private InvUommaster invUommaster;

	// bi-directional many-to-one association to InvItemmaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_freefor_item_id")
	private InvItemmaster freeForItemMaster;

	/*
	 * // bi-directional many-to-one association to InvSrvItem
	 * 
	 * @OneToMany(mappedBy = "InvPoItem") private List<InvSrvItem> invSrvItems;
	 */

	public InvPoItem() {
	}

	public Long getPoItemId() {
		return this.poItemId;
	}

	public void setPoItemId(Long poItemId) {
		this.poItemId = poItemId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsFree() {
		return this.isFree;
	}

	public void setIsFree(Boolean isFree) {
		this.isFree = isFree;
	}

	public BigDecimal getItemDiscountPercentage() {
		return this.itemDiscountPercentage;
	}

	public void setItemDiscountPercentage(BigDecimal itemDiscountPercentage) {
		this.itemDiscountPercentage = itemDiscountPercentage;
	}

	public BigDecimal getItemTotalActualAmount() {
		return this.itemTotalActualAmount;
	}

	public void setItemTotalActualAmount(BigDecimal itemTotalActualAmount) {
		this.itemTotalActualAmount = itemTotalActualAmount;
	}

	public BigDecimal getItemTotalCost() {
		return this.itemTotalCost;
	}

	public void setItemTotalCost(BigDecimal itemTotalCost) {
		this.itemTotalCost = itemTotalCost;
	}

	public BigDecimal getItemTotalDiscountAmount() {
		return this.itemTotalDiscountAmount;
	}

	public void setItemTotalDiscountAmount(BigDecimal itemTotalDiscountAmount) {
		this.itemTotalDiscountAmount = itemTotalDiscountAmount;
	}

	public BigDecimal getItemTotalTaxAmount() {
		return this.itemTotalTaxAmount;
	}

	public void setItemTotalTaxAmount(BigDecimal itemTotalTaxAmount) {
		this.itemTotalTaxAmount = itemTotalTaxAmount;
	}

	public BigDecimal getOrderQuantity() {
		return this.orderQuantity;
	}

	public void setOrderQuantity(BigDecimal orderQuantity) {
		this.orderQuantity = orderQuantity;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getReceivedQty() {
		return this.receivedQty;
	}

	public void setReceivedQty(BigDecimal receivedQty) {
		this.receivedQty = receivedQty;
	}

	public BigDecimal getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public InvPurchaseOrder getInvPurchaseOrder() {
		return invPurchaseOrder;
	}

	public void setInvPurchaseOrder(InvPurchaseOrder invPurchaseOrder) {
		this.invPurchaseOrder = invPurchaseOrder;
	}

	public InvUommaster getInvUommaster() {
		return invUommaster;
	}

	public void setInvUommaster(InvUommaster invUommaster) {
		this.invUommaster = invUommaster;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public InvItemmaster getInvItemMaster() {
		return invItemMaster;
	}

	public void setInvItemMaster(InvItemmaster invItemMaster) {
		this.invItemMaster = invItemMaster;
	}

	public InvItemmaster getFreeForItemMaster() {
		return freeForItemMaster;
	}

	public void setFreeForItemMaster(InvItemmaster freeForItemMaster) {
		this.freeForItemMaster = freeForItemMaster;
	}

	public String getBatchNo() {
		return batchNo;
	}

}