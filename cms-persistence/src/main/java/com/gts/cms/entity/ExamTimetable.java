package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_exam_timetable database table.
 * 
 */
@Entity
@Table(name = "t_exam_timetable")
@NamedQuery(name = "ExamTimetable.findAll", query = "SELECT e FROM ExamTimetable e")
public class ExamTimetable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_timetable_id")
	private Long examTimetableId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	@Column(name = "exam_date")
	private Date examDate;

	// bi-directional many-to-one association to ExamFeeStructure
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;


	// bi-directional many-to-one association to Course
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_id")
	private Course course;

	@Column(name = "fk_course_year_ids")
	private String courseYearIds;

	@Column(name = "fk_subject_ids")
	private String subjectIds;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to ExamMaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_id")
	private ExamMaster examMaster;

	// bi-directional many-to-one association to ExamSession
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_session_id")
	private ExamSession examSession;

	// bi-directional many-to-one association to ExamTimetableDetail
	@OneToMany(mappedBy = "examTimetable",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<ExamTimetableDetail> examTimetableDetails;

	public ExamTimetable() {
	}

	public Long getExamTimetableId() {
		return this.examTimetableId;
	}

	public void setExamTimetableId(Long examTimetableId) {
		this.examTimetableId = examTimetableId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getExamDate() {
		return this.examDate;
	}

	public void setExamDate(Date examDate) {
		this.examDate = examDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}


	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public String getCourseYearIds() {
		return courseYearIds;
	}

	public void setCourseYearIds(String courseYearIds) {
		this.courseYearIds = courseYearIds;
	}

	public String getSubjectIds() {
		return subjectIds;
	}

	public void setSubjectIds(String subjectIds) {
		this.subjectIds = subjectIds;
	}

	public ExamMaster getExamMaster() {
		return examMaster;
	}

	public void setExamMaster(ExamMaster examMaster) {
		this.examMaster = examMaster;
	}

	public ExamSession getExamSession() {
		return examSession;
	}

	public void setExamSession(ExamSession examSession) {
		this.examSession = examSession;
	}

	public List<ExamTimetableDetail> getExamTimetableDetails() {
		return examTimetableDetails;
	}

	public void setExamTimetableDetails(List<ExamTimetableDetail> examTimetableDetails) {
		this.examTimetableDetails = examTimetableDetails;
	}

}