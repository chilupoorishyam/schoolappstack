package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_sch_std_application database table.
 * 
 */
@Entity
@Table(name = "t_sch_std_application")
@NamedQuery(name = "SchStdApplication.findAll", query = "SELECT s FROM SchStdApplication s")
public class SchStdApplication implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_sch_std_application_id")
	private Long schStdApplicationId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "applied_on")
	private Date appliedOn;

	@Column(name = "clg_approval_status_ar")
	private String clgApprovalStatusAr;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "doc_colelcted_on")
	private Date docColelctedOn;

	@Column(name = "doc_collection_comments")
	private String docCollectionComments;

	@Column(name = "govt_approval_status_ar")
	private String govtApprovalStatusAr;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "govt_approved_on")
	private Date govtApprovedOn;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_all_docs_collected")
	private Boolean isAllDocsCollected;

	@Column(name = "is_completed")
	private Boolean isCompleted;

	@Column(name = "is_renewaled")
	private Boolean isRenewaled;

	@Column(name = "is_submitted_to_govt")
	private Boolean isSubmittedToGovt;

	private String reason;

	@Column(name = "sch_application_no")
	private String schApplicationNo;

	@Column(name = "scholarship_amount")
	private BigDecimal scholarshipAmount;

	@Column(name = "status_comments")
	private String statusComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "submitted_to_govt_on")
	private Date submittedToGovtOn;

	@Column(name = "total_amount_received")
	private BigDecimal totalAmountReceived;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "Applicant_Name")
	private String applicantName;

	// bi-directional many-to-one association to Course
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_id")
	private Course course;

	// bi-directional many-to-one association to GroupSection
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_group_section_id")
	private GroupSection groupSection;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_doc_collected_emp_id")
	private EmployeeDetail docCollectedEmpDetail;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_status_emp_id")
	private EmployeeDetail statusEmpDetail;

	// bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_academic_year_id")
	private AcademicYear academicYear;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_year_id")
	private CourseYear courseYear;

	// bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	// bi-directional many-to-one association to SchStdPreceeding
	@OneToMany(mappedBy = "stdApplication", fetch = FetchType.LAZY)
	private List<SchStdPreceeding> stdPreceedings;

	@Column(name="balance_amount")
	private BigDecimal balanceAmount;
	
	public SchStdApplication() {
	}

	public Long getSchStdApplicationId() {
		return this.schStdApplicationId;
	}

	public void setSchStdApplicationId(Long schStdApplicationId) {
		this.schStdApplicationId = schStdApplicationId;
	}

	public Date getAppliedOn() {
		return this.appliedOn;
	}

	public void setAppliedOn(Date appliedOn) {
		this.appliedOn = appliedOn;
	}

	public String getClgApprovalStatusAr() {
		return this.clgApprovalStatusAr;
	}

	public void setClgApprovalStatusAr(String clgApprovalStatusAr) {
		this.clgApprovalStatusAr = clgApprovalStatusAr;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getDocColelctedOn() {
		return this.docColelctedOn;
	}

	public void setDocColelctedOn(Date docColelctedOn) {
		this.docColelctedOn = docColelctedOn;
	}

	public String getDocCollectionComments() {
		return this.docCollectionComments;
	}

	public void setDocCollectionComments(String docCollectionComments) {
		this.docCollectionComments = docCollectionComments;
	}

	public String getGovtApprovalStatusAr() {
		return this.govtApprovalStatusAr;
	}

	public void setGovtApprovalStatusAr(String govtApprovalStatusAr) {
		this.govtApprovalStatusAr = govtApprovalStatusAr;
	}

	public Date getGovtApprovedOn() {
		return this.govtApprovedOn;
	}

	public void setGovtApprovedOn(Date govtApprovedOn) {
		this.govtApprovedOn = govtApprovedOn;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsAllDocsCollected() {
		return this.isAllDocsCollected;
	}

	public void setIsAllDocsCollected(Boolean isAllDocsCollected) {
		this.isAllDocsCollected = isAllDocsCollected;
	}

	public Boolean getIsCompleted() {
		return this.isCompleted;
	}

	public void setIsCompleted(Boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	public Boolean getIsRenewaled() {
		return this.isRenewaled;
	}

	public void setIsRenewaled(Boolean isRenewaled) {
		this.isRenewaled = isRenewaled;
	}

	public Boolean getIsSubmittedToGovt() {
		return this.isSubmittedToGovt;
	}

	public void setIsSubmittedToGovt(Boolean isSubmittedToGovt) {
		this.isSubmittedToGovt = isSubmittedToGovt;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSchApplicationNo() {
		return this.schApplicationNo;
	}

	public void setSchApplicationNo(String schApplicationNo) {
		this.schApplicationNo = schApplicationNo;
	}

	public BigDecimal getScholarshipAmount() {
		return this.scholarshipAmount;
	}

	public void setScholarshipAmount(BigDecimal scholarshipAmount) {
		this.scholarshipAmount = scholarshipAmount;
	}

	public String getStatusComments() {
		return this.statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public Date getSubmittedToGovtOn() {
		return this.submittedToGovtOn;
	}

	public void setSubmittedToGovtOn(Date submittedToGovtOn) {
		this.submittedToGovtOn = submittedToGovtOn;
	}

	public BigDecimal getTotalAmountReceived() {
		return this.totalAmountReceived;
	}

	public void setTotalAmountReceived(BigDecimal totalAmountReceived) {
		this.totalAmountReceived = totalAmountReceived;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getDocCollectedEmpDetail() {
		return this.docCollectedEmpDetail;
	}

	public void setDocCollectedEmpDetail(EmployeeDetail docCollectedEmpDetail) {
		this.docCollectedEmpDetail = docCollectedEmpDetail;
	}

	public EmployeeDetail getStatusEmpDetail() {
		return this.statusEmpDetail;
	}

	public void setStatusEmpDetail(EmployeeDetail statusEmpDetail) {
		this.statusEmpDetail = statusEmpDetail;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public CourseYear getCourseYear() {
		return this.courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public List<SchStdPreceeding> getStdPreceedings() {
		return this.stdPreceedings;
	}

	public void setStdPreceedings(List<SchStdPreceeding> stdPreceedings) {
		this.stdPreceedings = stdPreceedings;
	}

	public SchStdPreceeding addStdPreceeding(SchStdPreceeding stdPreceeding) {
		getStdPreceedings().add(stdPreceeding);
		stdPreceeding.setStdApplication(this);

		return stdPreceeding;
	}

	public SchStdPreceeding removeStdPreceeding(SchStdPreceeding stdPreceeding) {
		getStdPreceedings().remove(stdPreceeding);
		stdPreceeding.setStdApplication(null);

		return stdPreceeding;
	}

	public String getApplicantName() {
		return applicantName;
	}

	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}



	public GroupSection getGroupSection() {
		return groupSection;
	}

	public void setGroupSection(GroupSection groupSection) {
		this.groupSection = groupSection;
	}

	public BigDecimal getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(BigDecimal balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	
}