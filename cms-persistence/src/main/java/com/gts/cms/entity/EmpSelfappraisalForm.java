package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_emp_selfappraisal_form database table.
 */
@Data
@Entity
@Table(name = "t_emp_selfappraisal_form")
@NamedQuery(name = "EmpSelfappraisalForm.findAll", query = "SELECT c FROM EmpSelfappraisalForm c")
public class EmpSelfappraisalForm implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_selfappraisal_form_id")
    private Long selfAppraisalFormId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "title")
    private String title;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_date")
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_date")
    private Date endDate;

    @ManyToOne
    @JoinColumn(name = "fk_school_id")
    private School school;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    @OneToMany(mappedBy="selfappraisalFormId",fetch = FetchType.LAZY,cascade=CascadeType.ALL)
    private List<EmpSelfappraisalFormDetail> empSelfappraisalFormDetails;
}