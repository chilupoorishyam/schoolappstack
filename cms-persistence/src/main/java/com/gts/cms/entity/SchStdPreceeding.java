package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_sch_std_preceedings database table.
 * 
 */
@Entity
@Table(name = "t_sch_std_preceedings")
@NamedQuery(name = "SchStdPreceeding.findAll", query = "SELECT s FROM SchStdPreceeding s")
public class SchStdPreceeding implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_sch_std_preceeding_id")
	private Long schStdPreceedingId;

	@Column(name = "tution_fee")
	private BigDecimal tutionFee;

	@Column(name = "spl_fee")
	private BigDecimal splFee;

	@Column(name = "other_fee")
	private BigDecimal otherFee;

	@Column(name = "rtf_amount")
	private BigDecimal rtfAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "inst_no")
	private Integer instNo;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_amount_settled")
	private Boolean isAmountSettled;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "released_from_dt")
	private Date releaseFromDt;

	@Column(name = "released_to_dt")
	private Date releaseToDt;

	// bi-directional many-to-one association to FeeReceipt
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_fee_receipts_id")
	private FeeReceipt feeReceipt;

	// bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_academic_year_id")
	private AcademicYear academicYear;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_year_id")
	private CourseYear courseYear;

	// bi-directional many-to-one association to SchPreceeding
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_sch_preceeding_id")
	private SchPreceeding preceeding;

	// bi-directional many-to-one association to SchStdApplication
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_sch_std_application_id")
	private SchStdApplication stdApplication;

	// bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	public SchStdPreceeding() {
	}

	public Long getSchStdPreceedingId() {
		return this.schStdPreceedingId;
	}

	public void setSchStdPreceedingId(Long schStdPreceedingId) {
		this.schStdPreceedingId = schStdPreceedingId;
	}

	public BigDecimal getTutionFee() {
		return tutionFee;
	}

	public void setTutionFee(BigDecimal tutionFee) {
		this.tutionFee = tutionFee;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Integer getInstNo() {
		return this.instNo;
	}

	public void setInstNo(Integer instNo) {
		this.instNo = instNo;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsAmountSettled() {
		return this.isAmountSettled;
	}

	public void setIsAmountSettled(Boolean isAmountSettled) {
		this.isAmountSettled = isAmountSettled;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public FeeReceipt getFeeReceipt() {
		return this.feeReceipt;
	}

	public void setFeeReceipt(FeeReceipt feeReceipt) {
		this.feeReceipt = feeReceipt;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public CourseYear getCourseYear() {
		return this.courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public SchPreceeding getPreceeding() {
		return this.preceeding;
	}

	public void setPreceeding(SchPreceeding preceeding) {
		this.preceeding = preceeding;
	}

	public SchStdApplication getStdApplication() {
		return this.stdApplication;
	}

	public void setStdApplication(SchStdApplication stdApplication) {
		this.stdApplication = stdApplication;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public BigDecimal getSplFee() {
		return splFee;
	}

	public void setSplFee(BigDecimal splFee) {
		this.splFee = splFee;
	}

	public BigDecimal getOtherFee() {
		return otherFee;
	}

	public void setOtherFee(BigDecimal otherFee) {
		this.otherFee = otherFee;
	}

	public BigDecimal getRtfAmount() {
		return rtfAmount;
	}

	public void setRtfAmount(BigDecimal rtfAmount) {
		this.rtfAmount = rtfAmount;
	}

	public Date getReleaseFromDt() {
		return releaseFromDt;
	}

	public void setReleaseFromDt(Date releaseFromDt) {
		this.releaseFromDt = releaseFromDt;
	}

	public Date getReleaseToDt() {
		return releaseToDt;
	}

	public void setReleaseToDt(Date releaseToDt) {
		this.releaseToDt = releaseToDt;
	}

}