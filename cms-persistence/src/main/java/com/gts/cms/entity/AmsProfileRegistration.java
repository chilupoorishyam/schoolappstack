package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_ams_profile_registration database table.
 * 
 */
@Entity
@Table(name = "t_ams_profile_registration")
@NamedQuery(name = "AmsProfileRegistration.findAll", query = "SELECT p FROM AmsProfileRegistration p")
public class AmsProfileRegistration implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_ams_profile_reg_id")
	private Long profileRegId;

	@Column(name = "batch")
	private String baatch;

	
	private String branch;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "first_name")
	private String firstName;

	@ManyToOne
	@JoinColumn(name = "fk_batch_id")
	private Batch batch;

	@ManyToOne
	@JoinColumn(name = "fk_school_id")
	private School school;


	@ManyToOne
	@JoinColumn(name = "fk_course_id")
	private Course course;

	@ManyToOne
	@JoinColumn(name = "fk_emp_id")
	private EmployeeDetail employeeDetail;

	@ManyToOne
	@JoinColumn(name = "fk_org_id")
	private Organization fkOrgId;

	@Column(name = "fk_profile_id")
	private Long profileId;

	@ManyToOne
	@JoinColumn(name = "fk_profile_verifiedby_emp_id")
	private EmployeeDetail profileVerifiedbyEmp;

	@ManyToOne
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	@ManyToOne
	@JoinColumn(name = "fk_user_id")
	private User user;

	@Column(name = "hallticket_number")
	private String hallticketNumber;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_profile_created")
	private Boolean isProfileCreated;

	@Column(name = "is_rejected")
	private Boolean isRejected;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "passed_out_year")
	private String passedOutYear;

	@Column(name = "personal_email")
	private String personalEmail;

	@Column(name = "phone_number")
	private String phoneNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "profile_created_on")
	private Date profileCreatedOn;

	@Column(name = "profile_image_url")
	private String profileImageUrl;

	private String reason;

	@Column(name = "rejected_reason")
	private String rejectedReason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "is_email_verified")
	private Boolean isEmailVerified;
	
	@Column(name = "email_verification_code")
	private String emailVerificationCode;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "email_code_exp_date")
	private Date emailCodeExpDate;
	
	@Column(name = "is_mobile_verified")
	private Boolean isMobileVerified;
	
	@Column(name = "mobile_verification_code")
	private String mobileVerificationCode;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "mobile_code_exp_date")
	private Date mobileCodeExpDate;

	public Long getProfileRegId() {
		return profileRegId;
	}

	public void setProfileRegId(Long profileRegId) {
		this.profileRegId = profileRegId;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}


	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public EmployeeDetail getEmployeeDetail() {
		return employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public Organization getFkOrgId() {
		return fkOrgId;
	}

	public void setFkOrgId(Organization fkOrgId) {
		this.fkOrgId = fkOrgId;
	}

	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public EmployeeDetail getProfileVerifiedbyEmp() {
		return profileVerifiedbyEmp;
	}

	public void setProfileVerifiedbyEmp(EmployeeDetail profileVerifiedbyEmp) {
		this.profileVerifiedbyEmp = profileVerifiedbyEmp;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getHallticketNumber() {
		return hallticketNumber;
	}

	public void setHallticketNumber(String hallticketNumber) {
		this.hallticketNumber = hallticketNumber;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsProfileCreated() {
		return isProfileCreated;
	}

	public void setIsProfileCreated(Boolean isProfileCreated) {
		this.isProfileCreated = isProfileCreated;
	}

	public Boolean getIsRejected() {
		return isRejected;
	}

	public void setIsRejected(Boolean isRejected) {
		this.isRejected = isRejected;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassedOutYear() {
		return passedOutYear;
	}

	public void setPassedOutYear(String passedOutYear) {
		this.passedOutYear = passedOutYear;
	}

	public String getPersonalEmail() {
		return personalEmail;
	}

	public void setPersonalEmail(String personalEmail) {
		this.personalEmail = personalEmail;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Date getProfileCreatedOn() {
		return profileCreatedOn;
	}

	public void setProfileCreatedOn(Date profileCreatedOn) {
		this.profileCreatedOn = profileCreatedOn;
	}

	public String getProfileImageUrl() {
		return profileImageUrl;
	}

	public void setProfileImageUrl(String profileImageUrl) {
		this.profileImageUrl = profileImageUrl;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRejectedReason() {
		return rejectedReason;
	}

	public void setRejectedReason(String rejectedReason) {
		this.rejectedReason = rejectedReason;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Batch getBatch() {
		return batch;
	}

	public void setBatch(Batch batch) {
		this.batch = batch;
	}

	public String getBaatch() {
		return baatch;
	}

	public void setBaatch(String baatch) {
		this.baatch = baatch;
	}

	public Boolean getIsEmailVerified() {
		return isEmailVerified;
	}

	public void setIsEmailVerified(Boolean isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}

	public String getEmailVerificationCode() {
		return emailVerificationCode;
	}

	public void setEmailVerificationCode(String emailVerificationCode) {
		this.emailVerificationCode = emailVerificationCode;
	}

	public Date getEmailCodeExpDate() {
		return emailCodeExpDate;
	}

	public void setEmailCodeExpDate(Date emailCodeExpDate) {
		this.emailCodeExpDate = emailCodeExpDate;
	}

	public Boolean getIsMobileVerified() {
		return isMobileVerified;
	}

	public void setIsMobileVerified(Boolean isMobileVerified) {
		this.isMobileVerified = isMobileVerified;
	}

	public String getMobileVerificationCode() {
		return mobileVerificationCode;
	}

	public void setMobileVerificationCode(String mobileVerificationCode) {
		this.mobileVerificationCode = mobileVerificationCode;
	}

	public Date getMobileCodeExpDate() {
		return mobileCodeExpDate;
	}

	public void setMobileCodeExpDate(Date mobileCodeExpDate) {
		this.mobileCodeExpDate = mobileCodeExpDate;
	}
}