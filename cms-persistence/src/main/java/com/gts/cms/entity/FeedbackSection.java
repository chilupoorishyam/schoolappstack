package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_hr_feedback_sections database table.
 * 
 */
@Entity
@Table(name="t_hr_feedback_sections")
@NamedQuery(name="FeedbackSection.findAll", query="SELECT f FROM FeedbackSection f")
public class FeedbackSection implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fb_section_id")
	private Long fbSectionId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="fb_section_code")
	private String fbSectionCode;

	@Column(name="fb_section_name")
	private String fbSectionName;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FbOptionGroup
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fb_optiongroup_id")
	private FbOptionGroup fbOptionGroup;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fbinputtype_catdet_id")
	private GeneralDetail fbinputtype;

	//bi-directional many-to-one association to SurveyDetail
	@OneToMany(mappedBy="feedbackSection",fetch = FetchType.LAZY)
	private List<SurveyDetail> surveyDetails;

	public FeedbackSection() {
	}

	public Long getFbSectionId() {
		return this.fbSectionId;
	}

	public void setFbSectionId(Long fbSectionId) {
		this.fbSectionId = fbSectionId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getFbSectionCode() {
		return this.fbSectionCode;
	}

	public void setFbSectionCode(String fbSectionCode) {
		this.fbSectionCode = fbSectionCode;
	}

	public String getFbSectionName() {
		return this.fbSectionName;
	}

	public void setFbSectionName(String fbSectionName) {
		this.fbSectionName = fbSectionName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public FbOptionGroup getFbOptionGroup() {
		return this.fbOptionGroup;
	}

	public void setFbOptionGroup(FbOptionGroup fbOptionGroup) {
		this.fbOptionGroup = fbOptionGroup;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getFbinputtype() {
		return this.fbinputtype;
	}

	public void setFbinputtype(GeneralDetail fbinputtype) {
		this.fbinputtype = fbinputtype;
	}

	public List<SurveyDetail> getSurveyDetails() {
		return this.surveyDetails;
	}

	public void setSurveyDetails(List<SurveyDetail> surveyDetails) {
		this.surveyDetails = surveyDetails;
	}

	public SurveyDetail addSurveyDetail(SurveyDetail surveyDetail) {
		getSurveyDetails().add(surveyDetail);
		surveyDetail.setFeedbackSection(this);

		return surveyDetail;
	}

	public SurveyDetail removeSurveyDetail(SurveyDetail surveyDetail) {
		getSurveyDetails().remove(surveyDetail);
		surveyDetail.setFeedbackSection(null);

		return surveyDetail;
	}

}