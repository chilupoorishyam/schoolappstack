package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_sub_caste database table.
 * 
 */
@Entity
@Table(name="t_m_sub_caste")
@NamedQuery(name="SubCaste.findAll", query="SELECT s FROM SubCaste s")
public class SubCaste implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_sub_caste_id")
	private Long subCasteId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_eligible_for_reservation")
	private Boolean isEligibleForReservation;

	@Column(name="sort_order")
	private Integer sortOrder;

	@Column(name="sub_caste")
	private String subCaste;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="subCaste",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails;

	//bi-directional many-to-one association to Caste
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_caste_id")
	private Caste caste;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="subCaste",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="subCaste",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails;

	public SubCaste() {
	}

	public Long getSubCasteId() {
		return this.subCasteId;
	}

	public void setSubCasteId(Long subCasteId) {
		this.subCasteId = subCasteId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsEligibleForReservation() {
		return this.isEligibleForReservation;
	}

	public void setIsEligibleForReservation(Boolean isEligibleForReservation) {
		this.isEligibleForReservation = isEligibleForReservation;
	}

	public Integer getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSubCaste() {
		return this.subCaste;
	}

	public void setSubCaste(String subCaste) {
		this.subCaste = subCaste;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<EmployeeDetail> getEmpDetails() {
		return this.empDetails;
	}

	public void setEmpDetails(List<EmployeeDetail> empDetails) {
		this.empDetails = empDetails;
	}

	public EmployeeDetail addEmpDetail(EmployeeDetail empDetail) {
		getEmpDetails().add(empDetail);
		empDetail.setSubCaste(this);

		return empDetail;
	}

	public EmployeeDetail removeEmpDetail(EmployeeDetail empDetail) {
		getEmpDetails().remove(empDetail);
		empDetail.setSubCaste(null);

		return empDetail;
	}

	public Caste getCaste() {
		return this.caste;
	}

	public void setCaste(Caste caste) {
		this.caste = caste;
	}

	public List<StudentApplication> getStdApplications() {
		return this.stdApplications;
	}

	public void setStdApplications(List<StudentApplication> stdApplications) {
		this.stdApplications = stdApplications;
	}

	public StudentApplication addStdApplication(StudentApplication stdApplication) {
		getStdApplications().add(stdApplication);
		stdApplication.setSubCaste(this);

		return stdApplication;
	}

	public StudentApplication removeStdApplication(StudentApplication stdApplication) {
		getStdApplications().remove(stdApplication);
		stdApplication.setSubCaste(null);

		return stdApplication;
	}

	public List<StudentDetail> getStdStudentDetails() {
		return this.stdStudentDetails;
	}

	public void setStdStudentDetails(List<StudentDetail> stdStudentDetails) {
		this.stdStudentDetails = stdStudentDetails;
	}

	public StudentDetail addStdStudentDetail(StudentDetail stdStudentDetail) {
		getStdStudentDetails().add(stdStudentDetail);
		stdStudentDetail.setSubCaste(this);

		return stdStudentDetail;
	}

	public StudentDetail removeStdStudentDetail(StudentDetail stdStudentDetail) {
		getStdStudentDetails().remove(stdStudentDetail);
		stdStudentDetail.setSubCaste(null);

		return stdStudentDetail;
	}

}