package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "t_hr_emp_perf_level_works")
@NamedQuery(name="EmpPerfLevelWorks.findAll", query="SELECT e FROM EmpPerfLevelWorks e")
public class EmpPerfLevelWorks implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_emp_perf_level_works_id", nullable = false)
    private Long empPerfLevelWorksId;

    @ManyToOne
    @JoinColumn(name = "fk_assessment_feedback_id", nullable = false)
    private EmpPerfAssessmentFeedback assessmentFeedbackId;

    @ManyToOne
    @JoinColumn(name = "fk_emp_id", nullable = false)
    private EmployeeDetail empId;

    @ManyToOne
    @JoinColumn(name = "fk_levelworks_catdet_id", nullable = false)
    private GeneralDetail levelworksCatdetId;


    @ManyToOne
    @JoinColumn(name = "fk_authority_catdet_id", nullable = false)
    private GeneralDetail authorityCatdetId;

    @ManyToOne
    @JoinColumn(name = "fk_authority_emp_id", nullable = false)
    private EmployeeDetail authorityEmpId;

    @Column(name = "nature_of_work")
    private String natureOfWork;

    @ManyToOne
    @JoinColumn(name = "fk_rating_by_authority_catdet_id", nullable = false)
    private GeneralDetail ratingByAuthorityCatdetId;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "is_active", nullable = false)
    private Boolean active;

    @Column(name = "reason")
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt", nullable = false)
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    public Long getEmpPerfLevelWorksId() {
        return empPerfLevelWorksId;
    }

    public void setEmpPerfLevelWorksId(Long empPerfLevelWorksId) {
        this.empPerfLevelWorksId = empPerfLevelWorksId;
    }

    public EmpPerfAssessmentFeedback getAssessmentFeedbackId() {
        return assessmentFeedbackId;
    }

    public void setAssessmentFeedbackId(EmpPerfAssessmentFeedback assessmentFeedbackId) {
        this.assessmentFeedbackId = assessmentFeedbackId;
    }

    public EmployeeDetail getEmpId() {
        return empId;
    }

    public void setEmpId(EmployeeDetail empId) {
        this.empId = empId;
    }

    public GeneralDetail getLevelworksCatdetId() {
        return levelworksCatdetId;
    }

    public void setLevelworksCatdetId(GeneralDetail levelworksCatdetId) {
        this.levelworksCatdetId = levelworksCatdetId;
    }

    public GeneralDetail getAuthorityCatdetId() {
        return authorityCatdetId;
    }

    public void setAuthorityCatdetId(GeneralDetail authorityCatdetId) {
        this.authorityCatdetId = authorityCatdetId;
    }

    public EmployeeDetail getAuthorityEmpId() {
        return authorityEmpId;
    }

    public void setAuthorityEmpId(EmployeeDetail authorityEmpId) {
        this.authorityEmpId = authorityEmpId;
    }

    public String getNatureOfWork() {
        return natureOfWork;
    }

    public void setNatureOfWork(String natureOfWork) {
        this.natureOfWork = natureOfWork;
    }

    public GeneralDetail getRatingByAuthorityCatdetId() {
        return ratingByAuthorityCatdetId;
    }

    public void setRatingByAuthorityCatdetId(GeneralDetail ratingByAuthorityCatdetId) {
        this.ratingByAuthorityCatdetId = ratingByAuthorityCatdetId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    public Long getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(Long createdUser) {
        this.createdUser = createdUser;
    }

    public Date getUpdatedDt() {
        return updatedDt;
    }

    public void setUpdatedDt(Date updatedDt) {
        this.updatedDt = updatedDt;
    }

    public Long getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(Long updatedUser) {
        this.updatedUser = updatedUser;
    }
}
