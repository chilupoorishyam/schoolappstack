package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_ams_profilevisits database table.
 * 
 */
@Entity
@Table(name = "t_ams_profilevisits")
@NamedQuery(name = "ProfileVisit.findAll", query = "SELECT p FROM ProfileVisit p")
public class ProfileVisit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_ams_profile_visit_id")
	private Long profileVisitId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne
	@JoinColumn(name = "fk_ams_profile_id")
	private AmsProfileDetail profile;

	@ManyToOne
	@JoinColumn(name = "fk_ams_visitorprofile_id")
	private AmsProfileDetail visitorProfile;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "visited_time")
	private Date visitedTime;

	public ProfileVisit() {
	}

	public Long getProfileVisitId() {
		return this.profileVisitId;
	}

	public void setProfileVisitId(Long profileVisitId) {
		this.profileVisitId = profileVisitId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Date getVisitedTime() {
		return this.visitedTime;
	}

	public void setVisitedTime(Date visitedTime) {
		this.visitedTime = visitedTime;
	}

	public AmsProfileDetail getProfile() {
		return profile;
	}

	public void setProfile(AmsProfileDetail profile) {
		this.profile = profile;
	}

	public AmsProfileDetail getVisitorProfile() {
		return visitorProfile;
	}

	public void setVisitorProfile(AmsProfileDetail visitorProfile) {
		this.visitorProfile = visitorProfile;
	}
}