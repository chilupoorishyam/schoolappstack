package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_fee_structure_particulars database table.
 * 
 */
@Entity
@Table(name="t_fee_structure_particulars")
@NamedQuery(name="FeeStructureParticular.findAll", query="SELECT f FROM FeeStructureParticular f")
public class FeeStructureParticular implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_structure_particular_id")
	private Long feeStructureParticularId;

	@Column(name="concession_cutoff_per")
	private Integer concessionCutoffPer;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="fee_amount")
	private BigDecimal feeAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="from_date")
	private Date fromDate;

	@Column(name="is_active")
	private Boolean isActive;

	private Integer noofinstallments;

	private Integer priority;

	private String reason;

	@Column(name="sort_order")
	private Integer sortOrder;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="to_date")
	private Date toDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FeeParticularwisePayment
	@OneToMany(mappedBy="feeStructureParticular",fetch = FetchType.LAZY)
	private List<FeeParticularwisePayment> feeParticularwisePayments;

	//bi-directional many-to-one association to FeeCategory
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_category_id")
	private FeeCategory feeCategory;

	//bi-directional many-to-one association to FeeParticular
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_particulars_id")
	private FeeParticular feeParticular;

	//bi-directional many-to-one association to FeeStructure
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_structure_id")
	private FeeStructure feeStructure;

	//bi-directional many-to-one association to FinAccountType
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_mappingacctype_id")
	private FinAccountType mappingAccountType;

	//bi-directional many-to-one association to FinAccountType
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_cashacctype_id")
	private FinAccountType cashAccountType;

	//bi-directional many-to-one association to FinAccountType
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_bankacctype_id")
	private FinAccountType bankAccountType;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to FeeTransactionDetail
	@OneToMany(mappedBy="feeStructureParticular",fetch = FetchType.LAZY)
	private List<FeeTransactionDetail> feeTransactionDetails;
	
	//Added by Naveen on 04-12-2018
	@Column(name="lateral_fee_amount")
	private BigDecimal lateralFeeAmount;
	

	public FeeStructureParticular() {
	}

	public Long getFeeStructureParticularId() {
		return this.feeStructureParticularId;
	}

	public void setFeeStructureParticularId(Long feeStructureParticularId) {
		this.feeStructureParticularId = feeStructureParticularId;
	}

	public Integer getConcessionCutoffPer() {
		return this.concessionCutoffPer;
	}

	public void setConcessionCutoffPer(Integer concessionCutoffPer) {
		this.concessionCutoffPer = concessionCutoffPer;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public BigDecimal getFeeAmount() {
		return this.feeAmount;
	}

	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getNoofinstallments() {
		return this.noofinstallments;
	}

	public void setNoofinstallments(Integer noofinstallments) {
		this.noofinstallments = noofinstallments;
	}

	public Integer getPriority() {
		return this.priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<FeeParticularwisePayment> getFeeParticularwisePayments() {
		return this.feeParticularwisePayments;
	}

	public void setFeeParticularwisePayments(List<FeeParticularwisePayment> feeParticularwisePayments) {
		this.feeParticularwisePayments = feeParticularwisePayments;
	}

	public FeeParticularwisePayment addFeeParticularwisePayment(FeeParticularwisePayment feeParticularwisePayment) {
		getFeeParticularwisePayments().add(feeParticularwisePayment);
		feeParticularwisePayment.setFeeStructureParticular(this);

		return feeParticularwisePayment;
	}

	public FeeParticularwisePayment removeFeeParticularwisePayment(FeeParticularwisePayment feeParticularwisePayment) {
		getFeeParticularwisePayments().remove(feeParticularwisePayment);
		feeParticularwisePayment.setFeeStructureParticular(null);

		return feeParticularwisePayment;
	}

	public FeeCategory getFeeCategory() {
		return this.feeCategory;
	}

	public void setFeeCategory(FeeCategory feeCategory) {
		this.feeCategory = feeCategory;
	}

	public FeeParticular getFeeParticular() {
		return this.feeParticular;
	}

	public void setFeeParticular(FeeParticular feeParticular) {
		this.feeParticular = feeParticular;
	}

	public FeeStructure getFeeStructure() {
		return this.feeStructure;
	}

	public void setFeeStructure(FeeStructure feeStructure) {
		this.feeStructure = feeStructure;
	}

	public FinAccountType getMappingAccountType() {
		return this.mappingAccountType;
	}

	public void setMappingAccountType(FinAccountType mappingAccountType) {
		this.mappingAccountType = mappingAccountType;
	}

	public FinAccountType getCashAccountType() {
		return this.cashAccountType;
	}

	public void setCashAccountType(FinAccountType cashAccountType) {
		this.cashAccountType = cashAccountType;
	}

	public FinAccountType getBankAccountType() {
		return this.bankAccountType;
	}

	public void setBankAccountType(FinAccountType bankAccountType) {
		this.bankAccountType = bankAccountType;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<FeeTransactionDetail> getFeeTransactionDetails() {
		return this.feeTransactionDetails;
	}

	public void setFeeTransactionDetails(List<FeeTransactionDetail> feeTransactionDetails) {
		this.feeTransactionDetails = feeTransactionDetails;
	}

	public FeeTransactionDetail addFeeTransactionDetail(FeeTransactionDetail feeTransactionDetail) {
		getFeeTransactionDetails().add(feeTransactionDetail);
		feeTransactionDetail.setFeeStructureParticular(this);

		return feeTransactionDetail;
	}

	public FeeTransactionDetail removeFeeTransactionDetail(FeeTransactionDetail feeTransactionDetail) {
		getFeeTransactionDetails().remove(feeTransactionDetail);
		feeTransactionDetail.setFeeStructureParticular(null);

		return feeTransactionDetail;
	}

	public BigDecimal getLateralFeeAmount() {
		return lateralFeeAmount;
	}

	public void setLateralFeeAmount(BigDecimal lateralFeeAmount) {
		this.lateralFeeAmount = lateralFeeAmount;
	}

}