package com.gts.cms.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_std_student_profile database table.
 */
@Entity
@Setter
@Getter
@Table(name = "t_std_student_profile")
@NamedQuery(name = "StudentProfile.findAll", query = "SELECT a FROM StudentProfile a")
public class StudentProfile implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_student_profile_id")
    private Long studentProfileId;

    @ManyToOne
    @JoinColumn(name = "fk_student_id")
    private StudentDetail studentDetail;

    @ManyToOne
    @JoinColumn(name = "fk_event_master_catdet_id")
    private GeneralMaster eventMasterCatdetId;

    @ManyToOne
    @JoinColumn(name = "fk_event_title_catdet_id")
    private GeneralDetail eventTitleCatdetId;

    @ManyToOne
    @JoinColumn(name = "fk_course_year_id")
    private CourseYear courseYear;

    @Column(name = "certificate")
    private Boolean certificate;

    @Column(name = "videos_photos")
    private Integer videosPhotos;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

}