package com.gts.cms.entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_pa_training_sessions database table.
 * 
 */
@Entity
@Table(name = "t_pa_training_sessions")
@NamedQuery(name = "TrainingSession.findAll", query = "SELECT t FROM TrainingSession t")
public class TrainingSession implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_pa_training_session_id")
	private Long trainingSessionId;

	// bi-directional many-to-one association to TrainingDetail
	@ManyToOne
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to TrainingDetail
	@ManyToOne
	@JoinColumn(name = "fk_pa_traning_det_id")
	private TrainingDetail trainingDetail;

	@Temporal(TemporalType.DATE)
	@Column(name = "session_date")
	private Date sessionDate;

	@Column(name = "from_time")
	private Time fromTime;

	@Column(name = "to_time")
	private Time toTime;

	@Column(name = "no_of_attendees")
	private Integer noOfAttendees;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne
	@JoinColumn(name = "fk_session_incharge_emp_id")
	private EmployeeDetail employeeDetail;

	@Column(name = "session_taken_by")
	private String sessionTakenBy;

	@Column(name = "session_topics_covered")
	private String sessionTopicsCovered;

	@Column(name = "is_session_cancelled")
	private Boolean isSessionCancelled;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "attendance_captured_on")
	private Date attendanceCapturedOn;

	@Column(name = "is_attendance_freezed")
	private Boolean isAttendanceFreezed;

	@Column(name = "session_cancel_reason")
	private String sessionCancelReason;

	@Column(name = "reference_documents")
	private String referrenceDocuments;

	@Column(name = "reference_images")
	private String referenceImages;

	@Column(name = "reference_details")
	private String referenceDetails;

	@Column(name = "assignment_document")
	private String assignmentDocument;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	public TrainingSession() {
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	
	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsSessionCancelled() {
		return this.isSessionCancelled;
	}

	public void setIsSessionCancelled(Boolean isSessionCancelled) {
		this.isSessionCancelled = isSessionCancelled;
	}

	public Integer getNoOfAttendees() {
		return this.noOfAttendees;
	}

	public void setNoOfAttendees(Integer noOfAttendees) {
		this.noOfAttendees = noOfAttendees;
	}

	public Long getTrainingSessionId() {
		return this.trainingSessionId;
	}

	public void setTrainingSessionId(Long trainingSessionId) {
		this.trainingSessionId = trainingSessionId;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSessionCancelReason() {
		return this.sessionCancelReason;
	}

	public void setSessionCancelReason(String sessionCancelReason) {
		this.sessionCancelReason = sessionCancelReason;
	}

	public Date getSessionDate() {
		return this.sessionDate;
	}

	public void setSessionDate(Date sessionDate) {
		this.sessionDate = sessionDate;
	}

	public String getSessionTakenBy() {
		return this.sessionTakenBy;
	}

	public void setSessionTakenBy(String sessionTakenBy) {
		this.sessionTakenBy = sessionTakenBy;
	}

	public String getSessionTopicsCovered() {
		return this.sessionTopicsCovered;
	}

	public void setSessionTopicsCovered(String sessionTopicsCovered) {
		this.sessionTopicsCovered = sessionTopicsCovered;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public TrainingDetail getTrainingDetail() {
		return trainingDetail;
	}

	public void setTrainingDetail(TrainingDetail trainingDetail) {
		this.trainingDetail = trainingDetail;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public EmployeeDetail getEmployeeDetail() {
		return employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public Date getAttendanceCapturedOn() {
		return attendanceCapturedOn;
	}

	public void setAttendanceCapturedOn(Date attendanceCapturedOn) {
		this.attendanceCapturedOn = attendanceCapturedOn;
	}

	public Boolean getIsAttendanceFreezed() {
		return isAttendanceFreezed;
	}

	public void setIsAttendanceFreezed(Boolean isAttendanceFreezed) {
		this.isAttendanceFreezed = isAttendanceFreezed;
	}

	public String getReferrenceDocuments() {
		return referrenceDocuments;
	}

	public void setReferrenceDocuments(String referrenceDocuments) {
		this.referrenceDocuments = referrenceDocuments;
	}

	public String getReferenceImages() {
		return referenceImages;
	}

	public void setReferenceImages(String referenceImages) {
		this.referenceImages = referenceImages;
	}

	public String getReferenceDetails() {
		return referenceDetails;
	}

	public void setReferenceDetails(String referenceDetails) {
		this.referenceDetails = referenceDetails;
	}

	public String getAssignmentDocument() {
		return assignmentDocument;
	}

	public void setAssignmentDocument(String assignmentDocument) {
		this.assignmentDocument = assignmentDocument;
	}

	public Time getFromTime() {
		return fromTime;
	}

	public void setFromTime(Time fromTime) {
		this.fromTime = fromTime;
	}

	public Time getToTime() {
		return toTime;
	}

	public void setToTime(Time toTime) {
		this.toTime = toTime;
	}
	
}