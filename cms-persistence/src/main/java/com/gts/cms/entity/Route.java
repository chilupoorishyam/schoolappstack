package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_tm_route database table.
 * 
 */
@Entity
@Table(name="t_tm_route")
@NamedQuery(name="Route.findAll", query="SELECT r FROM Route r")
public class Route implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_route_id")
	private Long routeId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Column(name="route_code")
	private String routeCode;

	@Column(name="route_drop_place")
	private String routeDropPlace;

	@Column(name="route_pickup_place")
	private String routePickupPlace;

	@Column(name="service_number")
	private String serviceNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to TransportDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_transport_detail_id")
	private TransportDetail transportDetail;

	//bi-directional many-to-one association to RouteStop
	@OneToMany(mappedBy="route",fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	private List<RouteStop> routeStops;

	//bi-directional many-to-one association to VechicleRoute
	@OneToMany(mappedBy="route",fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	private List<VechicleRoute> vechicleRoutes;

	public Route() {
	}

	public Long getRouteId() {
		return this.routeId;
	}

	public void setRouteId(Long routeId) {
		this.routeId = routeId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRouteCode() {
		return this.routeCode;
	}

	public void setRouteCode(String routeCode) {
		this.routeCode = routeCode;
	}

	public String getRouteDropPlace() {
		return this.routeDropPlace;
	}

	public void setRouteDropPlace(String routeDropPlace) {
		this.routeDropPlace = routeDropPlace;
	}

	public String getRoutePickupPlace() {
		return this.routePickupPlace;
	}

	public void setRoutePickupPlace(String routePickupPlace) {
		this.routePickupPlace = routePickupPlace;
	}

	public String getServiceNumber() {
		return this.serviceNumber;
	}

	public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public TransportDetail getTransportDetail() {
		return this.transportDetail;
	}

	public void setTransportDetail(TransportDetail transportDetail) {
		this.transportDetail = transportDetail;
	}

	public List<RouteStop> getRouteStops() {
		return this.routeStops;
	}

	public void setRouteStops(List<RouteStop> routeStops) {
		this.routeStops = routeStops;
	}

	public RouteStop addRouteStop(RouteStop routeStop) {
		getRouteStops().add(routeStop);
		routeStop.setRoute(this);

		return routeStop;
	}

	public RouteStop removeRouteStop(RouteStop routeStop) {
		getRouteStops().remove(routeStop);
		routeStop.setRoute(null);

		return routeStop;
	}

	public List<VechicleRoute> getVechicleRoutes() {
		return this.vechicleRoutes;
	}

	public void setVechicleRoutes(List<VechicleRoute> vechicleRoutes) {
		this.vechicleRoutes = vechicleRoutes;
	}

	public VechicleRoute addVechicleRoute(VechicleRoute vechicleRoute) {
		getVechicleRoutes().add(vechicleRoute);
		vechicleRoute.setRoute(this);

		return vechicleRoute;
	}

	public VechicleRoute removeVechicleRoute(VechicleRoute vechicleRoute) {
		getVechicleRoutes().remove(vechicleRoute);
		vechicleRoute.setRoute(null);

		return vechicleRoute;
	}

}