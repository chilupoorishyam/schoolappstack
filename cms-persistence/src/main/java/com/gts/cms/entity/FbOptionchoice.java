package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_hr_fb_optionchoices database table.
 * 
 */
@Entity
@Table(name="t_hr_fb_optionchoices")
@NamedQuery(name="FbOptionchoice.findAll", query="SELECT f FROM FbOptionchoice f")
public class FbOptionchoice implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fb_optionchoice_id")
	private Long fbOptionchoiceId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String optionchoice;

	@Column(name="optionchoice_rating")
	private Integer optionchoiceRating;

	private String reason;

	@Column(name="sort_order")
	private Integer sortOrder;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FbOptionGroup
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fb_option_group_id")
	private FbOptionGroup fbOptionGroup;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to SurveyFeedbackDetail
	@OneToMany(mappedBy="fbOptionchoice",fetch = FetchType.LAZY)
	private List<SurveyFeedbackDetail> surveyFeedbackDetails;

	public FbOptionchoice() {
	}

	public Long getFbOptionchoiceId() {
		return this.fbOptionchoiceId;
	}

	public void setFbOptionchoiceId(Long fbOptionchoiceId) {
		this.fbOptionchoiceId = fbOptionchoiceId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getOptionchoice() {
		return this.optionchoice;
	}

	public void setOptionchoice(String optionchoice) {
		this.optionchoice = optionchoice;
	}

	public Integer getOptionchoiceRating() {
		return this.optionchoiceRating;
	}

	public void setOptionchoiceRating(Integer optionchoiceRating) {
		this.optionchoiceRating = optionchoiceRating;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public FbOptionGroup getFbOptionGroup() {
		return this.fbOptionGroup;
	}

	public void setFbOptionGroup(FbOptionGroup fbOptionGroup) {
		this.fbOptionGroup = fbOptionGroup;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<SurveyFeedbackDetail> getSurveyFeedbackDetails() {
		return this.surveyFeedbackDetails;
	}

	public void setSurveyFeedbackDetails(List<SurveyFeedbackDetail> surveyFeedbackDetails) {
		this.surveyFeedbackDetails = surveyFeedbackDetails;
	}

	public SurveyFeedbackDetail addSurveyFeedbackDetail(SurveyFeedbackDetail surveyFeedbackDetail) {
		getSurveyFeedbackDetails().add(surveyFeedbackDetail);
		surveyFeedbackDetail.setFbOptionchoice(this);

		return surveyFeedbackDetail;
	}

	public SurveyFeedbackDetail removeSurveyFeedbackDetail(SurveyFeedbackDetail surveyFeedbackDetail) {
		getSurveyFeedbackDetails().remove(surveyFeedbackDetail);
		surveyFeedbackDetail.setFbOptionchoice(null);

		return surveyFeedbackDetail;
	}

}