package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the t_hstl_fee_payment database table.
 * 
 */
@Entity
@Table(name = "t_hstl_fee_payment")
@NamedQuery(name = "HostelFeePayment.findAll", query = "SELECT h FROM HostelFeePayment h")
public class HostelFeePayment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_hstl_fee_payment_id")
	private Long hstlFeePaymentId;

	@Column(name = "applied_discounts")
	private String appliedDiscounts;

	@Column(name = "applied_fines")
	private String appliedFines;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "discount_amount")
	private BigDecimal discountAmount;

	@Column(name = "discount_file_path")
	private String discountFilePath;

	@Column(name = "discount_reason")
	private String discountReason;

	@Column(name = "due_amount")
	private BigDecimal dueAmount;

	@Column(name = "fine_amount")
	private BigDecimal fineAmount;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "from_pay_date")
	private Date fromPayDate;

	@Column(name = "gross_amount")
	private BigDecimal grossAmount;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "net_amount")
	private BigDecimal netAmount;

	private String reason;

	@Column(name = "receipt_no")
	private String receiptNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "to_pay_date")
	private Date toPayDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_academic_year_id")
	private AcademicYear academicYear;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_emp_id")
	private EmployeeDetail employeeDetail;

	// bi-directional many-to-one association to HostelDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_hstl_hostel_id")
	private HostelDetail hostelDetail;
	
	// bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	// bi-directional many-to-one association to HstlRoomAllocation
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_hstl_room_allot_id")
	private HostelRoomAllocation hstlRoomAllocation;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	public HostelFeePayment() {
	}

	public Long getHstlFeePaymentId() {
		return this.hstlFeePaymentId;
	}

	public void setHstlFeePaymentId(Long hstlFeePaymentId) {
		this.hstlFeePaymentId = hstlFeePaymentId;
	}

	public String getAppliedDiscounts() {
		return this.appliedDiscounts;
	}

	public void setAppliedDiscounts(String appliedDiscounts) {
		this.appliedDiscounts = appliedDiscounts;
	}

	public String getAppliedFines() {
		return this.appliedFines;
	}

	public void setAppliedFines(String appliedFines) {
		this.appliedFines = appliedFines;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public BigDecimal getDiscountAmount() {
		return this.discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public String getDiscountFilePath() {
		return this.discountFilePath;
	}

	public void setDiscountFilePath(String discountFilePath) {
		this.discountFilePath = discountFilePath;
	}

	public String getDiscountReason() {
		return this.discountReason;
	}

	public void setDiscountReason(String discountReason) {
		this.discountReason = discountReason;
	}

	public BigDecimal getDueAmount() {
		return this.dueAmount;
	}

	public void setDueAmount(BigDecimal dueAmount) {
		this.dueAmount = dueAmount;
	}

	public BigDecimal getFineAmount() {
		return this.fineAmount;
	}

	public void setFineAmount(BigDecimal fineAmount) {
		this.fineAmount = fineAmount;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public EmployeeDetail getEmployeeDetail() {
		return employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public Date getFromPayDate() {
		return this.fromPayDate;
	}

	public void setFromPayDate(Date fromPayDate) {
		this.fromPayDate = fromPayDate;
	}

	public BigDecimal getGrossAmount() {
		return this.grossAmount;
	}

	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public BigDecimal getNetAmount() {
		return this.netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReceiptNo() {
		return this.receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public Date getToPayDate() {
		return this.toPayDate;
	}

	public void setToPayDate(Date toPayDate) {
		this.toPayDate = toPayDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public HostelDetail getHostelDetail() {
		return this.hostelDetail;
	}

	public void setHostelDetail(HostelDetail hostelDetail) {
		this.hostelDetail = hostelDetail;
	}

	public HostelRoomAllocation getHstlRoomAllocation() {
		return this.hstlRoomAllocation;
	}

	public void setHstlRoomAllocation(HostelRoomAllocation hstlRoomAllocation) {
		this.hstlRoomAllocation = hstlRoomAllocation;
	}

}