package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_lib_book_details database table.
 * 
 */
@Entity
@Table(name = "t_lib_book_details")
@NamedQuery(name = "BookDetail.findAll", query = "SELECT b FROM BookDetail b")
public class BookDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_lib_book_details_id")
	private Long bookDetailsId;

	@Column(name = "availability_status")
	private String availabilityStatus;

	@Column(name = "book_position")
	private String bookPosition;

	@Column(name = "accessionno")
	private String accessionno;

	@Column(name = "book_barcode")
	private String bookbarCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "library_ref_number")
	private Integer libraryRefNumber;

	private String reason;
	
	@Column(name = "book_amount")
	private BigDecimal bookAmount;

	@Column(name = "status_comments")
	private String statusComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to Book
	//Mandatory to eager loading - ssr
	@ManyToOne
	@JoinColumn(name = "fk_lib_book_id")
	private Book book;

	// bi-directional many-to-one association to LibraryDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_library_id")
	private LibraryDetail libraryDetail;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_bookcondition_catdet_id")
	private GeneralDetail bookcondition;

	// bi-directional many-to-one association to BookIssuedetail
	@OneToMany(mappedBy = "bookDetail", fetch = FetchType.LAZY)
	private List<BookIssuedetail> bookIssuedetails;

	// bi-directional many-to-one association to BookIssuedetailsHistory
	@OneToMany(mappedBy = "bookDetail", fetch = FetchType.LAZY)
	private List<BookIssuedetailsHistory> bookIssuedetailsHistories;

	// bi-directional many-to-one association to LibShelve
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_lib_shelve_id")
	private LibShelve libShelve;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_bookregtype_catdet_id")
	private GeneralDetail bookregType;

	// bi-directional many-to-one association to LibMember
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_lib_member_id")
	private LibMember libMember;

	public BookDetail() {
	}

	public Long getBookDetailsId() {
		return this.bookDetailsId;
	}

	public void setBookDetailsId(Long bookDetailsId) {
		this.bookDetailsId = bookDetailsId;
	}

	public String getAvailabilityStatus() {
		return this.availabilityStatus;
	}

	public void setAvailabilityStatus(String availabilityStatus) {
		this.availabilityStatus = availabilityStatus;
	}

	public String getAccessionno() {
		return accessionno;
	}

	public void setAccessionno(String accessionno) {
		this.accessionno = accessionno;
	}

	public String getBookbarCode() {
		return bookbarCode;
	}

	public void setBookbarCode(String bookbarCode) {
		this.bookbarCode = bookbarCode;
	}

	public String getBookPosition() {
		return this.bookPosition;
	}

	public void setBookPosition(String bookPosition) {
		this.bookPosition = bookPosition;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getLibraryRefNumber() {
		return this.libraryRefNumber;
	}

	public void setLibraryRefNumber(Integer libraryRefNumber) {
		this.libraryRefNumber = libraryRefNumber;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public LibShelve getLibShelve() {
		return libShelve;
	}

	public void setLibShelve(LibShelve libShelve) {
		this.libShelve = libShelve;
	}

	public String getStatusComments() {
		return this.statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Book getBook() {
		return this.book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public LibraryDetail getLibraryDetail() {
		return this.libraryDetail;
	}

	public void setLibraryDetail(LibraryDetail libraryDetail) {
		this.libraryDetail = libraryDetail;
	}

	public GeneralDetail getBookcondition() {
		return this.bookcondition;
	}

	public void setBookcondition(GeneralDetail bookcondition) {
		this.bookcondition = bookcondition;
	}

	public List<BookIssuedetail> getBookIssuedetails() {
		return this.bookIssuedetails;
	}

	public void setBookIssuedetails(List<BookIssuedetail> bookIssuedetails) {
		this.bookIssuedetails = bookIssuedetails;
	}

	public BookIssuedetail addBookIssuedetail(BookIssuedetail bookIssuedetail) {
		getBookIssuedetails().add(bookIssuedetail);
		bookIssuedetail.setBookDetail(this);

		return bookIssuedetail;
	}

	public BookIssuedetail removeBookIssuedetail(BookIssuedetail bookIssuedetail) {
		getBookIssuedetails().remove(bookIssuedetail);
		bookIssuedetail.setBookDetail(null);

		return bookIssuedetail;
	}

	public List<BookIssuedetailsHistory> getBookIssuedetailsHistories() {
		return this.bookIssuedetailsHistories;
	}

	public void setBookIssuedetailsHistories(List<BookIssuedetailsHistory> bookIssuedetailsHistories) {
		this.bookIssuedetailsHistories = bookIssuedetailsHistories;
	}

	public BookIssuedetailsHistory addBookIssuedetailsHistory(BookIssuedetailsHistory bookIssuedetailsHistory) {
		getBookIssuedetailsHistories().add(bookIssuedetailsHistory);
		bookIssuedetailsHistory.setBookDetail(this);

		return bookIssuedetailsHistory;
	}

	public BookIssuedetailsHistory removeBookIssuedetailsHistory(BookIssuedetailsHistory bookIssuedetailsHistory) {
		getBookIssuedetailsHistories().remove(bookIssuedetailsHistory);
		bookIssuedetailsHistory.setBookDetail(null);

		return bookIssuedetailsHistory;
	}

	public GeneralDetail getBookregType() {
		return bookregType;
	}

	public void setBookregType(GeneralDetail bookregType) {
		this.bookregType = bookregType;
	}

	public LibMember getLibMember() {
		return libMember;
	}

	public void setLibMember(LibMember libMember) {
		this.libMember = libMember;
	}

	public BigDecimal getBookAmount() {
		return bookAmount;
	}

	public void setBookAmount(BigDecimal bookAmount) {
		this.bookAmount = bookAmount;
	}
	
}