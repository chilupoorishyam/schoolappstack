package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_qualifications database table.
 * 
 */
@Entity
@Table(name="t_m_qualifications")
@NamedQuery(name="Qualification.findAll", query="SELECT q FROM Qualification q")
public class Qualification implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_qualification_id")
	private Long qualificationId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="qualification_code")
	private String qualificationCode;

	@Column(name="qualification_name")
	private String qualificationName;

	private String reason;

	@Column(name="sort_order")
	private Integer sortOrder;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeeDetail
	@OneToMany(mappedBy="qualification",fetch = FetchType.LAZY)
	private List<EmployeeDetail> empDetails;

	//bi-directional many-to-one association to QualificationGroup
	@OneToMany(mappedBy="qualification",fetch = FetchType.LAZY)
	private List<QualificationGroup> qualificationGroups;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to StudentEnquiry
	@OneToMany(mappedBy="qualification",fetch = FetchType.LAZY)
	private List<StudentEnquiry> enquiries;

	public Qualification() {
	}

	public Long getQualificationId() {
		return this.qualificationId;
	}

	public void setQualificationId(Long qualificationId) {
		this.qualificationId = qualificationId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getQualificationCode() {
		return this.qualificationCode;
	}

	public void setQualificationCode(String qualificationCode) {
		this.qualificationCode = qualificationCode;
	}

	public String getQualificationName() {
		return this.qualificationName;
	}

	public void setQualificationName(String qualificationName) {
		this.qualificationName = qualificationName;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<EmployeeDetail> getEmpDetails() {
		return this.empDetails;
	}

	public void setEmpDetails(List<EmployeeDetail> empDetails) {
		this.empDetails = empDetails;
	}

	public EmployeeDetail addEmpDetail(EmployeeDetail empDetail) {
		getEmpDetails().add(empDetail);
		empDetail.setQualification(this);

		return empDetail;
	}

	public EmployeeDetail removeEmpDetail(EmployeeDetail empDetail) {
		getEmpDetails().remove(empDetail);
		empDetail.setQualification(null);

		return empDetail;
	}

	public List<QualificationGroup> getQualificationGroups() {
		return this.qualificationGroups;
	}

	public void setQualificationGroups(List<QualificationGroup> qualificationGroups) {
		this.qualificationGroups = qualificationGroups;
	}

	public QualificationGroup addQualificationGroup(QualificationGroup qualificationGroup) {
		getQualificationGroups().add(qualificationGroup);
		qualificationGroup.setQualification(this);

		return qualificationGroup;
	}

	public QualificationGroup removeQualificationGroup(QualificationGroup qualificationGroup) {
		getQualificationGroups().remove(qualificationGroup);
		qualificationGroup.setQualification(null);

		return qualificationGroup;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<StudentEnquiry> getEnquiries() {
		return this.enquiries;
	}

	public void setEnquiries(List<StudentEnquiry> enquiries) {
		this.enquiries = enquiries;
	}

	public StudentEnquiry addEnquiry(StudentEnquiry enquiry) {
		getEnquiries().add(enquiry);
		enquiry.setQualification(this);

		return enquiry;
	}

	public StudentEnquiry removeEnquiry(StudentEnquiry enquiry) {
		getEnquiries().remove(enquiry);
		enquiry.setQualification(null);

		return enquiry;
	}

}