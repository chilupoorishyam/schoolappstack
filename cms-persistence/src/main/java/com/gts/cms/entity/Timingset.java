package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the t_tt_timingsets database table.
 * 
 */
@Entity
@Table(name="t_tt_timingsets")
@NamedQuery(name="Timingset.findAll", query="SELECT t FROM Timingset t")
public class Timingset implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_timingsets_id")
	private Long timingsetId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_default")
	private Boolean isDefault;

	private String reason;

	@Column(name="timingset_name")
	private String timingsetName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to ClassTiming
	@OrderBy("sortOrder ASC")
	@OneToMany(mappedBy="timingset",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private Set <ClassTiming> classTimings;

	//bi-directional many-to-one association to ClassWeekday
	@OrderBy("weekday.weekdayId ASC")
	@OneToMany(mappedBy="timingset",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private Set <ClassWeekday> classWeekdays;
	
	@OneToMany(mappedBy="timingset",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private Set <Timetable> timetable;

	//bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;
	
	//Added by Naveen on 04-12-2018
	@Column(name="timingset_code")
	private String timingsetCode;

	public Timingset() {
	}

	public Long getTimingsetId() {
		return this.timingsetId;
	}

	public void setTimingsetId(Long timingsetId) {
		this.timingsetId = timingsetId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDefault() {
		return this.isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getTimingsetName() {
		return this.timingsetName;
	}

	public void setTimingsetName(String timingsetName) {
		this.timingsetName = timingsetName;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Set <ClassTiming> getClassTimings() {
		return this.classTimings;
	}

	public void setClassTimings(Set <ClassTiming> classTimings) {
		this.classTimings = classTimings;
	}

	public ClassTiming addClassTiming(ClassTiming classTiming) {
		getClassTimings().add(classTiming);
		classTiming.setTimingset(this);

		return classTiming;
	}

	public ClassTiming removeClassTiming(ClassTiming classTiming) {
		getClassTimings().remove(classTiming);
		classTiming.setTimingset(null);

		return classTiming;
	}

	public Set <ClassWeekday> getClassWeekdays() {
		return this.classWeekdays;
	}

	public void setClassWeekdays(Set <ClassWeekday> classWeekdays) {
		this.classWeekdays = classWeekdays;
	}

	public ClassWeekday addClassWeekday(ClassWeekday classWeekday) {
		getClassWeekdays().add(classWeekday);
		classWeekday.setTimingset(this);

		return classWeekday;
	}

	public ClassWeekday removeClassWeekday(ClassWeekday classWeekday) {
		getClassWeekdays().remove(classWeekday);
		classWeekday.setTimingset(null);

		return classWeekday;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Set<Timetable> getTimetable() {
		return timetable;
	}

	public void setTimetable(Set<Timetable> timetable) {
		this.timetable = timetable;
	}

	public String getTimingsetCode() {
		return timingsetCode;
	}

	public void setTimingsetCode(String timingsetCode) {
		this.timingsetCode = timingsetCode;
	}
	
	

}