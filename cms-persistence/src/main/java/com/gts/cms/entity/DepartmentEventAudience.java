package com.gts.cms.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_de_dept_event_audience database table.
 */
@Entity
@Setter
@Getter
@Table(name = "t_de_dept_event_audience")
@NamedQuery(name = "DepartmentEventAudience.findAll", query = "SELECT d FROM DepartmentEventAudience d")
public class DepartmentEventAudience implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_dept_event_audience_id")
    private Long deptEventAudienceId;

    //bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_dept_event_id")
    private DepartmentEvent deptEventId;

    //bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_student_id")
    private StudentDetail studentDetailId;

    //bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_emp_id")
    private EmployeeDetail employeeDetailId;

    //bi-directional many-to-one association to School
    @JoinColumn(name = "is_coordinator")
    private Boolean isCoordinator;

    @Column(name = "fee_collected")
    private Double feeCollected;

    //bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_fee_collectedby_emp_id")
    private EmployeeDetail feeCollectedbyEmpId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cancelled_dt")
    private Date cancelledDt;

    //bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_cancelledby_emp_id")
    private EmployeeDetail cancelledbyEmpId;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "reason")
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

}