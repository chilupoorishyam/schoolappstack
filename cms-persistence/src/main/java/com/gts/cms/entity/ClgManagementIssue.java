package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_inv_clg_management_issue database table.
 * 
 */
@Entity
@Table(name = "t_inv_clg_management_issue")
@NamedQuery(name = "ClgManagementIssue.findAll", query = "SELECT c FROM ClgManagementIssue c")
public class ClgManagementIssue implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_clg_management_issue_id")
	private Long managementIssueId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "acknowledged_date")
	private Date acknowledgedDate;

	@Column(name = "acknowledgement_notes")
	private String acknowledgementNotes;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "actual_resolved_on")
	private Date actualResolvedOn;

	@Column(name = "closing_comments")
	private String closingComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;
	
	@Column(name = "after_complaint_picture")
	private String afterComplaintPicture;
	
	@Column(name = "before_complaint_picture")
	private String beforeComplaintPicture;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "expected_resolved_on")
	private Date expectedResolvedOn;

	private String feedback;
	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_aprvrejstatus_catdet_id")
	private GeneralDetail aprvrejstatusCat;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_closed_emp_id")
	private EmployeeDetail closedEmp;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_incharge_emp_id")
	private EmployeeDetail inchargeEmp;

	// bi-directional many-to-one association to Department
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_issue_in_dept_id")
	private Department issueInDept;

	// bi-directional many-to-one association to Room
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_issue_in_room_id")
	private Room issueInRoom;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_issuepriority_catdet_id")
	private GeneralDetail issuepriorityCat;

	// bi-directional many-to-one association to InvItemDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_item_det_id")
	private InvItemDetail itemDet;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_raiseby_emp_id")
	private EmployeeDetail raisebyEmp;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_status_emp_id")
	private EmployeeDetail statusEmp;

	// bi-directional many-to-one association to WorkflowStage
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_wf_status_id")
	private WorkflowStage wfStatus;
	//fk_issuecategory_catdet_id
	
	// bi-directional many-to-one association to WorkflowStage
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_issuecategory_catdet_id")
	private GeneralDetail issueCategoryCat;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_closed")
	private Boolean isClosed;

	@Column(name = "is_mgmt_approval_req")
	private Boolean isMgmtApprovalReq;

	@Column(name = "issue_description")
	private String issueDescription;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "issue_log_date")
	private Date issueLogDate;

	@Column(name = "issue_title")
	private String issueTitle;

	private String location;

	private Integer rating;

	private String reason;

	@Column(name = "status_comments")
	private String statusComments;

	@Column(name = "status_ref_path")
	private String statusRefPath;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "wf_status_comments")
	private String wfStatusComments;

	
	// bi-directional many-to-one association to ComplaintsWf

	@OneToMany(mappedBy = "clgManagementIssue")
	private List<InvComplaintsWf> complaintsWfs;

	public ClgManagementIssue() {
	}

	public Long getManagementIssueId() {
		return this.managementIssueId;
	}

	public void setManagementIssueId(Long managementIssueId) {
		this.managementIssueId = managementIssueId;
	}

	public Date getAcknowledgedDate() {
		return this.acknowledgedDate;
	}

	public void setAcknowledgedDate(Date acknowledgedDate) {
		this.acknowledgedDate = acknowledgedDate;
	}

	public String getAcknowledgementNotes() {
		return this.acknowledgementNotes;
	}

	public void setAcknowledgementNotes(String acknowledgementNotes) {
		this.acknowledgementNotes = acknowledgementNotes;
	}

	public Date getActualResolvedOn() {
		return this.actualResolvedOn;
	}

	public void setActualResolvedOn(Date actualResolvedOn) {
		this.actualResolvedOn = actualResolvedOn;
	}

	public String getClosingComments() {
		return this.closingComments;
	}

	public void setClosingComments(String closingComments) {
		this.closingComments = closingComments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getExpectedResolvedOn() {
		return this.expectedResolvedOn;
	}

	public void setExpectedResolvedOn(Date expectedResolvedOn) {
		this.expectedResolvedOn = expectedResolvedOn;
	}

	public String getFeedback() {
		return this.feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public GeneralDetail getAprvrejstatusCat() {
		return aprvrejstatusCat;
	}

	public void setAprvrejstatusCat(GeneralDetail aprvrejstatusCat) {
		this.aprvrejstatusCat = aprvrejstatusCat;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public EmployeeDetail getInchargeEmp() {
		return inchargeEmp;
	}

	public void setInchargeEmp(EmployeeDetail inchargeEmp) {
		this.inchargeEmp = inchargeEmp;
	}

	public Department getIssueInDept() {
		return issueInDept;
	}

	public void setIssueInDept(Department issueInDept) {
		this.issueInDept = issueInDept;
	}

	public Room getIssueInRoom() {
		return issueInRoom;
	}

	public void setIssueInRoom(Room issueInRoom) {
		this.issueInRoom = issueInRoom;
	}

	public GeneralDetail getIssuepriorityCat() {
		return issuepriorityCat;
	}

	public void setIssuepriorityCat(GeneralDetail issuepriorityCat) {
		this.issuepriorityCat = issuepriorityCat;
	}

	public InvItemDetail getItemDet() {
		return itemDet;
	}

	public void setItemDet(InvItemDetail itemDet) {
		this.itemDet = itemDet;
	}

	public EmployeeDetail getRaisebyEmp() {
		return raisebyEmp;
	}

	public void setRaisebyEmp(EmployeeDetail raisebyEmp) {
		this.raisebyEmp = raisebyEmp;
	}

	public EmployeeDetail getClosedEmp() {
		return closedEmp;
	}

	public void setClosedEmp(EmployeeDetail closedEmp) {
		this.closedEmp = closedEmp;
	}

	public EmployeeDetail getStatusEmp() {
		return statusEmp;
	}

	public void setStatusEmp(EmployeeDetail statusEmp) {
		this.statusEmp = statusEmp;
	}

	public WorkflowStage getWfStatus() {
		return wfStatus;
	}

	public void setWfStatus(WorkflowStage wfStatus) {
		this.wfStatus = wfStatus;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsClosed() {
		return isClosed;
	}

	public void setIsClosed(Boolean isClosed) {
		this.isClosed = isClosed;
	}

	public Boolean getIsMgmtApprovalReq() {
		return isMgmtApprovalReq;
	}

	public void setIsMgmtApprovalReq(Boolean isMgmtApprovalReq) {
		this.isMgmtApprovalReq = isMgmtApprovalReq;
	}

	public String getIssueDescription() {
		return issueDescription;
	}

	public void setIssueDescription(String issueDescription) {
		this.issueDescription = issueDescription;
	}

	public Date getIssueLogDate() {
		return issueLogDate;
	}

	public void setIssueLogDate(Date issueLogDate) {
		this.issueLogDate = issueLogDate;
	}

	public String getIssueTitle() {
		return issueTitle;
	}

	public void setIssueTitle(String issueTitle) {
		this.issueTitle = issueTitle;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStatusComments() {
		return statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public String getStatusRefPath() {
		return statusRefPath;
	}

	public void setStatusRefPath(String statusRefPath) {
		this.statusRefPath = statusRefPath;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getWfStatusComments() {
		return wfStatusComments;
	}

	public void setWfStatusComments(String wfStatusComments) {
		this.wfStatusComments = wfStatusComments;
	}

	public List<InvComplaintsWf> getComplaintsWfs() {
		return complaintsWfs;
	}

	public void setComplaintsWfs(List<InvComplaintsWf> complaintsWfs) {
		this.complaintsWfs = complaintsWfs;
	}

	public String getAfterComplaintPicture() {
		return afterComplaintPicture;
	}

	public void setAfterComplaintPicture(String afterComplaintPicture) {
		this.afterComplaintPicture = afterComplaintPicture;
	}

	public String getBeforeComplaintPicture() {
		return beforeComplaintPicture;
	}

	public void setBeforeComplaintPicture(String beforeComplaintPicture) {
		this.beforeComplaintPicture = beforeComplaintPicture;
	}

	public GeneralDetail getIssueCategoryCat() {
		return issueCategoryCat;
	}

	public void setIssueCategoryCat(GeneralDetail issueCategoryCat) {
		this.issueCategoryCat = issueCategoryCat;
	}
}