package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_emp_daily_details database table.
 */
@Entity
@Data
@Table(name = "t_emp_daily_details")
@NamedQuery(name = "EmployeeDailyDetail.findAll", query = "SELECT e FROM EmployeeDailyDetail e")
public class EmployeeDailyDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_emp_daily_details_id")
    private Long employeeDailyDetailsId;

    @Column(name = "period_no")
    private Integer periodNo;

    @Column(name = "no_of_students_attended")
    private Integer noOfStudentsAttended;

    @Column(name = "absentees_rollno")
    private String absenteesRollno;

    @Column(name = "other_activites")
    private String otherActivites;

    @Column(name = "is_attendance_updated")
    private Boolean isAttendanceUpdated;

    @Column(name = "is_ment_report_updated")
    private Boolean isMentReportUpdated;

    @Column(name = "is_facultyportfolio_updated")
    private Boolean isFacultyportfolioUpdated;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "inserted_date")
    private Date insertedDate;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "reason")
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    //bi-directional many-to-one association to Caste
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_emp_id")
    private EmployeeDetail employeeDetail;

    //bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_school_id")
    private School school;

    //bi-directional many-to-one association to Department
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_dept_id")
    private Department department;

    //bi-directional many-to-one association to Designation
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_designation_id")
    private Designation designation;

    //bi-directional many-to-one association to Organization
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_group_section_id")
    private GroupSection groupSection;

    //bi-directional many-to-one association to Organization
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_theory_lab_catdet_id")
    private GeneralDetail theoryLabCatdetId;

    //bi-directional many-to-one association to Organization
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_sub_unit_topic_id")
    private SubjectUnitTopic subUnitTopicId;
}