package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * The persistent class for the t_m_course_year database table.
 * 
 */
@Entity
@Table(name="t_m_course_year")
@NamedQuery(name="CourseYear.findAll", query="SELECT c FROM CourseYear c")
public class CourseYear implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_course_year_id")
	private Long courseYearId;

	@Column(name="course_year_code")
	private String courseYearCode;

	@Column(name="course_year_name")
	private String courseYearName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Column(name="sort_order")
	private Integer sortOrder;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;
	
	@Column(name="year_name")
	private String yearName;
	
	@Column(name="year_no")
	private Integer yearNo;
	
	@Column(name="sem_no")
	private Integer semNo;

	//bi-directional many-to-one association to BatchwiseStudent
	@OneToMany(mappedBy="courseYear",fetch = FetchType.LAZY)
	private List<BatchwiseStudent> batchwiseStudents;

	//bi-directional many-to-one association to GroupyrRegulationDetail
	@OneToMany(mappedBy="courseYear",fetch = FetchType.LAZY)
	private List<GroupyrRegulationDetail> groupyrRegulationDetails;

	//bi-directional many-to-one association to SubjectBook
	/*@OneToMany(mappedBy="courseYear")
	private List<SubjectBook> subjectBooks;

	//bi-directional many-to-one association to SubjectUnitTopic
	@OneToMany(mappedBy="courseYear")
	private List<SubjectUnitTopic> subjectUnitTopics;

	//bi-directional many-to-one association to SubjectUnit
	@OneToMany(mappedBy="courseYear")
	private List<SubjectUnit> subjectUnits;
*/
	//bi-directional many-to-one association to Subjectregulation
	@OneToMany(mappedBy="courseYear",fetch = FetchType.EAGER)
	private List<Subjectregulation> subjectregulations;

	//bi-directional many-to-one association to FeeStructureCourseyr
	@OneToMany(mappedBy="courseYear",fetch = FetchType.LAZY)
	private List<FeeStructureCourseyr> feeStructureCourseyrs;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to Course
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_id")
	private Course course;

	//bi-directional many-to-one association to GroupSection
	@OneToMany(mappedBy="courseYear",fetch = FetchType.LAZY)
	@OrderBy("sortOrder ASC")
	private Set<GroupSection> groupSections;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="courseYear",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications;

	//bi-directional many-to-one association to StudentAcademicbatch
	@OneToMany(mappedBy="fromCourseYear",fetch = FetchType.LAZY)
	private List<StudentAcademicbatch> studentAcademicbatches1;

	//bi-directional many-to-one association to StudentAcademicbatch
	@OneToMany(mappedBy="toCourseYear",fetch = FetchType.LAZY)
	private List<StudentAcademicbatch> studentAcademicbatches2;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="courseYear",fetch = FetchType.LAZY)
	private List<StudentDetail> stdStudentDetails;

	//bi-directional many-to-one association to StudentSubject
	@OneToMany(mappedBy="courseYear",fetch = FetchType.LAZY)
	private List<StudentSubject> studentSubjects;

	public CourseYear() {
	}

	public Long getCourseYearId() {
		return this.courseYearId;
	}

	public void setCourseYearId(Long courseYearId) {
		this.courseYearId = courseYearId;
	}

	public String getCourseYearCode() {
		return this.courseYearCode;
	}

	public void setCourseYearCode(String courseYearCode) {
		this.courseYearCode = courseYearCode;
	}

	public String getCourseYearName() {
		return this.courseYearName;
	}

	public void setCourseYearName(String courseYearName) {
		this.courseYearName = courseYearName;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<BatchwiseStudent> getBatchwiseStudents() {
		return this.batchwiseStudents;
	}

	public void setBatchwiseStudents(List<BatchwiseStudent> batchwiseStudents) {
		this.batchwiseStudents = batchwiseStudents;
	}

	public BatchwiseStudent addBatchwiseStudent(BatchwiseStudent batchwiseStudent) {
		getBatchwiseStudents().add(batchwiseStudent);
		batchwiseStudent.setCourseYear(this);

		return batchwiseStudent;
	}

	public BatchwiseStudent removeBatchwiseStudent(BatchwiseStudent batchwiseStudent) {
		getBatchwiseStudents().remove(batchwiseStudent);
		batchwiseStudent.setCourseYear(null);

		return batchwiseStudent;
	}

	public List<GroupyrRegulationDetail> getGroupyrRegulationDetails() {
		return this.groupyrRegulationDetails;
	}

	public void setGroupyrRegulationDetails(List<GroupyrRegulationDetail> groupyrRegulationDetails) {
		this.groupyrRegulationDetails = groupyrRegulationDetails;
	}

	public GroupyrRegulationDetail addGroupyrRegulationDetail(GroupyrRegulationDetail groupyrRegulationDetail) {
		getGroupyrRegulationDetails().add(groupyrRegulationDetail);
		groupyrRegulationDetail.setCourseYear(this);

		return groupyrRegulationDetail;
	}

	public GroupyrRegulationDetail removeGroupyrRegulationDetail(GroupyrRegulationDetail groupyrRegulationDetail) {
		getGroupyrRegulationDetails().remove(groupyrRegulationDetail);
		groupyrRegulationDetail.setCourseYear(null);

		return groupyrRegulationDetail;
	}

/*	public List<SubjectBook> getSubjectBooks() {
		return this.subjectBooks;
	}

	public void setSubjectBooks(List<SubjectBook> subjectBooks) {
		this.subjectBooks = subjectBooks;
	}



	public List<SubjectUnitTopic> getSubjectUnitTopics() {
		return this.subjectUnitTopics;
	}

	public void setSubjectUnitTopics(List<SubjectUnitTopic> subjectUnitTopics) {
		this.subjectUnitTopics = subjectUnitTopics;
	}



	public List<SubjectUnit> getSubjectUnits() {
		return this.subjectUnits;
	}

	public void setSubjectUnits(List<SubjectUnit> subjectUnits) {
		this.subjectUnits = subjectUnits;
	}*/


	public List<Subjectregulation> getSubjectregulations() {
		return this.subjectregulations;
	}

	public void setSubjectregulations(List<Subjectregulation> subjectregulations) {
		this.subjectregulations = subjectregulations;
	}

	public Subjectregulation addSubjectregulation(Subjectregulation subjectregulation) {
		getSubjectregulations().add(subjectregulation);
		subjectregulation.setCourseYear(this);

		return subjectregulation;
	}

	public Subjectregulation removeSubjectregulation(Subjectregulation subjectregulation) {
		getSubjectregulations().remove(subjectregulation);
		subjectregulation.setCourseYear(null);

		return subjectregulation;
	}

	public List<FeeStructureCourseyr> getFeeStructureCourseyrs() {
		return this.feeStructureCourseyrs;
	}

	public void setFeeStructureCourseyrs(List<FeeStructureCourseyr> feeStructureCourseyrs) {
		this.feeStructureCourseyrs = feeStructureCourseyrs;
	}

	public FeeStructureCourseyr addFeeStructureCourseyr(FeeStructureCourseyr feeStructureCourseyr) {
		getFeeStructureCourseyrs().add(feeStructureCourseyr);
		feeStructureCourseyr.setCourseYear(this);

		return feeStructureCourseyr;
	}

	public FeeStructureCourseyr removeFeeStructureCourseyr(FeeStructureCourseyr feeStructureCourseyr) {
		getFeeStructureCourseyrs().remove(feeStructureCourseyr);
		feeStructureCourseyr.setCourseYear(null);

		return feeStructureCourseyr;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Course getCourse() {
		return this.course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Set<GroupSection> getGroupSections() {
		return this.groupSections;
	}

	public void setGroupSections(Set<GroupSection> groupSections) {
		this.groupSections = groupSections;
	}

	public GroupSection addGroupSection(GroupSection groupSection) {
		getGroupSections().add(groupSection);
		groupSection.setCourseYear(this);

		return groupSection;
	}

	public GroupSection removeGroupSection(GroupSection groupSection) {
		getGroupSections().remove(groupSection);
		groupSection.setCourseYear(null);

		return groupSection;
	}

	public List<StudentApplication> getStdApplications() {
		return this.stdApplications;
	}

	public void setStdApplications(List<StudentApplication> stdApplications) {
		this.stdApplications = stdApplications;
	}

	public StudentApplication addStdApplication(StudentApplication stdApplication) {
		getStdApplications().add(stdApplication);
		stdApplication.setCourseYear(this);

		return stdApplication;
	}

	public StudentApplication removeStdApplication(StudentApplication stdApplication) {
		getStdApplications().remove(stdApplication);
		stdApplication.setCourseYear(null);

		return stdApplication;
	}

	public List<StudentAcademicbatch> getStudentAcademicbatches1() {
		return this.studentAcademicbatches1;
	}

	public void setStudentAcademicbatches1(List<StudentAcademicbatch> studentAcademicbatches1) {
		this.studentAcademicbatches1 = studentAcademicbatches1;
	}

	public StudentAcademicbatch addStudentAcademicbatches1(StudentAcademicbatch studentAcademicbatches1) {
		getStudentAcademicbatches1().add(studentAcademicbatches1);
		studentAcademicbatches1.setFromCourseYear(this);

		return studentAcademicbatches1;
	}

	public StudentAcademicbatch removeStudentAcademicbatches1(StudentAcademicbatch studentAcademicbatches1) {
		getStudentAcademicbatches1().remove(studentAcademicbatches1);
		studentAcademicbatches1.setFromCourseYear(null);

		return studentAcademicbatches1;
	}

	public List<StudentAcademicbatch> getStudentAcademicbatches2() {
		return this.studentAcademicbatches2;
	}

	public void setStudentAcademicbatches2(List<StudentAcademicbatch> studentAcademicbatches2) {
		this.studentAcademicbatches2 = studentAcademicbatches2;
	}

	public StudentAcademicbatch addStudentAcademicbatches2(StudentAcademicbatch studentAcademicbatches2) {
		getStudentAcademicbatches2().add(studentAcademicbatches2);
		studentAcademicbatches2.setToCourseYear(this);

		return studentAcademicbatches2;
	}

	public StudentAcademicbatch removeStudentAcademicbatches2(StudentAcademicbatch studentAcademicbatches2) {
		getStudentAcademicbatches2().remove(studentAcademicbatches2);
		studentAcademicbatches2.setToCourseYear(null);

		return studentAcademicbatches2;
	}

	public List<StudentDetail> getStdStudentDetails() {
		return this.stdStudentDetails;
	}

	public void setStdStudentDetails(List<StudentDetail> stdStudentDetails) {
		this.stdStudentDetails = stdStudentDetails;
	}

	public StudentDetail addStdStudentDetail(StudentDetail stdStudentDetail) {
		getStdStudentDetails().add(stdStudentDetail);
		stdStudentDetail.setCourseYear(this);

		return stdStudentDetail;
	}

	public StudentDetail removeStdStudentDetail(StudentDetail stdStudentDetail) {
		getStdStudentDetails().remove(stdStudentDetail);
		stdStudentDetail.setCourseYear(null);

		return stdStudentDetail;
	}

	public List<StudentSubject> getStudentSubjects() {
		return this.studentSubjects;
	}

	public void setStudentSubjects(List<StudentSubject> studentSubjects) {
		this.studentSubjects = studentSubjects;
	}

	public StudentSubject addStudentSubject(StudentSubject studentSubject) {
		getStudentSubjects().add(studentSubject);
		studentSubject.setCourseYear(this);

		return studentSubject;
	}

	public StudentSubject removeStudentSubject(StudentSubject studentSubject) {
		getStudentSubjects().remove(studentSubject);
		studentSubject.setCourseYear(null);

		return studentSubject;
	}

	public String getYearName() {
		return yearName;
	}

	public void setYearName(String yearName) {
		this.yearName = yearName;
	}

	public Integer getYearNo() {
		return yearNo;
	}

	public void setYearNo(Integer yearNo) {
		this.yearNo = yearNo;
	}

	public Integer getSemNo() {
		return semNo;
	}

	public void setSemNo(Integer semNo) {
		this.semNo = semNo;
	}
	
	

}