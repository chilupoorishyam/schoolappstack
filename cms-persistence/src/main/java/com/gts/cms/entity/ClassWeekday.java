package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_tt_class_weekdays database table.
 * 
 */
@Entity
@Table(name="t_tt_class_weekdays")
@NamedQuery(name="ClassWeekday.findAll", query="SELECT c FROM ClassWeekday c")
public class ClassWeekday implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_class_weekday_id")
	private Long classWeekdayId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to Weekday
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_weekday_id")
	private Weekday weekday;

	//bi-directional many-to-one association to Timingset
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_timingsets_id")
	private Timingset timingset;

	public ClassWeekday() {
	}

	public Long getClassWeekdayId() {
		return this.classWeekdayId;
	}

	public void setClassWeekdayId(Long classWeekdayId) {
		this.classWeekdayId = classWeekdayId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Weekday getWeekday() {
		return this.weekday;
	}

	public void setWeekday(Weekday weekday) {
		this.weekday = weekday;
	}

	public Timingset getTimingset() {
		return this.timingset;
	}

	public void setTimingset(Timingset timingset) {
		this.timingset = timingset;
	}

}