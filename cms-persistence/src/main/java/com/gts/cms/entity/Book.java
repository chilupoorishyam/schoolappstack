package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the t_lib_books database table.
 * 
 */
@Entity
@Table(name = "t_lib_books")
@NamedQuery(name = "Book.findAll", query = "SELECT b FROM Book b")
public class Book implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_lib_book_id")
	private Long bookId;

	private String barcode;

	private String booknumber;
	@Column(name = "vol")
	private String vol;

	@Column(name = "noofpages")
	private Long noOfPages;

	@Column(name = "year")
	private String year;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "custom_tags")
	private String customTags;

	private String edition;

	@Column(name = "is_active")
	private Boolean isActive;

	private String isbn;

	@Column(name = "bookcode")
	private String bookcode;

	@Column(name = "library_ref_prefix")
	private String libraryRefPrefix;

	private Integer noofcopies;
	
	@Column(name = "available_copies")
	private Integer availableCopies;
	
	@Column(name = "issued_copies")
	private Integer issuedCopies;

	// private String publisher;

	private String reason;

	private String tags;

	private String title;

	@Column(name = "book_image1")
	private String bookImage1;

	@Column(name = "book_image2")
	private String bookImage2;

	@Column(name = "book_image3")
	private String bookImage3;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;
	
	@Column(name="call_number")
	private String callNumber;
	
	@Column(name="subject_headings")
	private String subjectHeadings;
	
	@Column(name="fk_author_ids")
	private String authorIds;
	
	@Column(name="fk_publisher_ids")
	private String publisherIds;

	// bi-directional many-to-one association to SubjectBook
	@OneToMany(mappedBy = "book", fetch = FetchType.LAZY)
	private List<SubjectBook> subjectBooks;

	// bi-directional many-to-one association to BookDetail
	@OneToMany(mappedBy = "book", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	private List<BookDetail> bookDetails;

	// bi-directional many-to-one association to BookPurchaseDetail
	@OneToMany(mappedBy = "book", fetch = FetchType.LAZY)
	private List<BookPurchaseDetail> bookPurchaseDetails;

	// bi-directional many-to-one association to Author
	/*@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_author_id")
	private Author author;*/

	// bi-directional many-to-one association to BookPurchaseDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_book_purchase_details")
	private BookPurchaseDetail bookPurchaseDetail;

	// bi-directional many-to-one association to LibraryDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_library_id")
	private LibraryDetail libraryDetail;

	// bi-directional many-to-one association to Publisher
	/*@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_publisher_id")
	private Publisher publisher;*/

	// bi-directional many-to-one association to Bookcategory
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_lib_bookcat_id")
	private Bookcategory bookCategory;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_language_catdet_id")
	private GeneralDetail language;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_bindingtype_catdet_id")
	private GeneralDetail bindingType;

	public Book() {
	}

	public Long getBookId() {
		return this.bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public String getBarcode() {
		return this.barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getBooknumber() {
		return this.booknumber;
	}

	public void setBooknumber(String booknumber) {
		this.booknumber = booknumber;
	}

	public String getVol() {
		return vol;
	}

	public void setVol(String vol) {
		this.vol = vol;
	}

	public Long getNoOfPages() {
		return noOfPages;
	}

	public void setNoOfPages(Long noOfPages) {
		this.noOfPages = noOfPages;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getCustomTags() {
		return this.customTags;
	}

	public void setCustomTags(String customTags) {
		this.customTags = customTags;
	}

	public String getEdition() {
		return this.edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getIsbn() {
		return this.isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getBookcode() {
		return bookcode;
	}

	public void setBookcode(String bookcode) {
		this.bookcode = bookcode;
	}

	public String getLibraryRefPrefix() {
		return this.libraryRefPrefix;
	}

	public void setLibraryRefPrefix(String libraryRefPrefix) {
		this.libraryRefPrefix = libraryRefPrefix;
	}

	public Integer getNoofcopies() {
		return this.noofcopies;
	}

	public void setNoofcopies(Integer noofcopies) {
		this.noofcopies = noofcopies;
	}

//	public String getPublisher() {
//		return this.publisher;
//	}
//
//	public void setPublisher(String publisher) {
//		this.publisher = publisher;
//	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getTags() {
		return this.tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<SubjectBook> getSubjectBooks() {
		return this.subjectBooks;
	}

	public void setSubjectBooks(List<SubjectBook> subjectBooks) {
		this.subjectBooks = subjectBooks;
	}

	public SubjectBook addSubjectBook(SubjectBook subjectBook) {
		getSubjectBooks().add(subjectBook);
		subjectBook.setBook(this);

		return subjectBook;
	}

	public SubjectBook removeSubjectBook(SubjectBook subjectBook) {
		getSubjectBooks().remove(subjectBook);
		subjectBook.setBook(null);

		return subjectBook;
	}

	public List<BookDetail> getBookDetails() {
		return this.bookDetails;
	}

	public void setBookDetails(List<BookDetail> bookDetails) {
		this.bookDetails = bookDetails;
	}

	public BookDetail addBookDetail(BookDetail bookDetail) {
		getBookDetails().add(bookDetail);
		bookDetail.setBook(this);

		return bookDetail;
	}

	public BookDetail removeBookDetail(BookDetail bookDetail) {
		getBookDetails().remove(bookDetail);
		bookDetail.setBook(null);

		return bookDetail;
	}

	public List<BookPurchaseDetail> getBookPurchaseDetails() {
		return this.bookPurchaseDetails;
	}

	public void setBookPurchaseDetails(List<BookPurchaseDetail> bookPurchaseDetails) {
		this.bookPurchaseDetails = bookPurchaseDetails;
	}

	public BookPurchaseDetail addBookPurchaseDetail(BookPurchaseDetail bookPurchaseDetail) {
		getBookPurchaseDetails().add(bookPurchaseDetail);
		bookPurchaseDetail.setBook(this);

		return bookPurchaseDetail;
	}

	public BookPurchaseDetail removeBookPurchaseDetail(BookPurchaseDetail bookPurchaseDetail) {
		getBookPurchaseDetails().remove(bookPurchaseDetail);
		bookPurchaseDetail.setBook(null);

		return bookPurchaseDetail;
	}

	public BookPurchaseDetail getBookPurchaseDetail() {
		return this.bookPurchaseDetail;
	}

	public void setBookPurchaseDetail(BookPurchaseDetail bookPurchaseDetail) {
		this.bookPurchaseDetail = bookPurchaseDetail;
	}

	public LibraryDetail getLibraryDetail() {
		return this.libraryDetail;
	}

	public void setLibraryDetail(LibraryDetail libraryDetail) {
		this.libraryDetail = libraryDetail;
	}

	public String getBookImage1() {
		return bookImage1;
	}

	public void setBookImage1(String bookImage1) {
		this.bookImage1 = bookImage1;
	}

	public String getBookImage2() {
		return bookImage2;
	}

	public void setBookImage2(String bookImage2) {
		this.bookImage2 = bookImage2;
	}

	public String getBookImage3() {
		return bookImage3;
	}

	public void setBookImage3(String bookImage3) {
		this.bookImage3 = bookImage3;
	}

	public Bookcategory getBookCategory() {
		return bookCategory;
	}

	public void setBookCategory(Bookcategory bookCategory) {
		this.bookCategory = bookCategory;
	}

	public GeneralDetail getBindingType() {
		return bindingType;
	}

	public void setBindingType(GeneralDetail bindingType) {
		this.bindingType = bindingType;
	}

	public GeneralDetail getLanguage() {
		return language;
	}

	public void setLanguage(GeneralDetail language) {
		this.language = language;
	}

	public Integer getAvailableCopies() {
		return availableCopies;
	}

	public Integer getIssuedCopies() {
		return issuedCopies;
	}

	public void setAvailableCopies(Integer availableCopies) {
		this.availableCopies = availableCopies;
	}

	public void setIssuedCopies(Integer issuedCopies) {
		this.issuedCopies = issuedCopies;
	}

	public String getCallNumber() {
		return callNumber;
	}

	public void setCallNumber(String callNumber) {
		this.callNumber = callNumber;
	}

	public String getSubjectHeadings() {
		return subjectHeadings;
	}

	public void setSubjectHeadings(String subjectHeadings) {
		this.subjectHeadings = subjectHeadings;
	}

	public String getAuthorIds() {
		return authorIds;
	}

	public String getPublisherIds() {
		return publisherIds;
	}

	public void setAuthorIds(String authorIds) {
		this.authorIds = authorIds;
	}

	public void setPublisherIds(String publisherIds) {
		this.publisherIds = publisherIds;
	}
	
}