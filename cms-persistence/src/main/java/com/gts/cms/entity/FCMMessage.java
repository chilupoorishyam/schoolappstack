package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_fcm_messages database table.
 * 
 */
@Entity
@Table(name = "t_fcm_messages")
@NamedQuery(name = "FCMMessage.findAll", query = "SELECT f FROM FCMMessage f")
public class FCMMessage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_fcm_message_id")
	private Long fcmMessageId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_academic_year_id")
	private AcademicYear academicYear;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_fcm_group_id")
	private FCMGroup fcmGroup;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_from_user_id")
	private User fromUser;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_user_id")
	private User user;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Boolean updatedUser;

	public FCMMessage() {
	}

	public Long getFcmMessageId() {
		return fcmMessageId;
	}

	public void setFcmMessageId(Long fcmMessageId) {
		this.fcmMessageId = fcmMessageId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public FCMGroup getFcmGroup() {
		return fcmGroup;
	}

	public void setFcmGroup(FCMGroup fcmGroup) {
		this.fcmGroup = fcmGroup;
	}

	public User getFromUser() {
		return fromUser;
	}

	public void setFromUser(User fromUser) {
		this.fromUser = fromUser;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Boolean getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Boolean updatedUser) {
		this.updatedUser = updatedUser;
	}
}