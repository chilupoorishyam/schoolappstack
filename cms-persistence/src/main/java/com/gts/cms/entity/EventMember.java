package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_ams_event_members database table.
 * 
 */
@Entity
@Table(name = "t_ams_event_members")
@NamedQuery(name = "EventMember.findAll", query = "SELECT e FROM EventMember e")
public class EventMember implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_ams_event_member_id")
	private Long eventMemberId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne
	@JoinColumn(name = "fk_ams_event_id")
	private AmsEvent event;

	@ManyToOne
	@JoinColumn(name = "fk_ams_profile_id")
	private AmsProfileDetail profileDetail;

	@Column(name = "is_active")
	private Boolean isActive;

	private Boolean isattended;

	private Boolean isgoing;

	private Boolean isnotgoing;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	public EventMember() {
	}

	public Long getEventMemberId() {
		return this.eventMemberId;
	}

	public void setEventMemberId(Long eventMemberId) {
		this.eventMemberId = eventMemberId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsattended() {
		return this.isattended;
	}

	public void setIsattended(Boolean isattended) {
		this.isattended = isattended;
	}

	public Boolean getIsgoing() {
		return this.isgoing;
	}

	public void setIsgoing(Boolean isgoing) {
		this.isgoing = isgoing;
	}

	public Boolean getIsnotgoing() {
		return this.isnotgoing;
	}

	public void setIsnotgoing(Boolean isnotgoing) {
		this.isnotgoing = isnotgoing;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public AmsEvent getEvent() {
		return event;
	}

	public void setEvent(AmsEvent event) {
		this.event = event;
	}

	public AmsProfileDetail getProfileDetail() {
		return profileDetail;
	}

	public void setProfileDetail(AmsProfileDetail profileDetail) {
		this.profileDetail = profileDetail;
	}

}