package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_workflow_stages database table.
 * 
 */
@Entity
@Table(name="t_m_workflow_stages")
@NamedQuery(name="WorkflowStage.findAll", query="SELECT w FROM WorkflowStage w")
public class WorkflowStage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_wf_stage_id")
	private Long workflowStageId;

	@Column(name="available_for")
	private Long availableFor;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="go_back_point")
	private Long goBackPoint;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_self_available")
	private Boolean isSelfAvailable;

	private String reason;

	@Column(name="sort_order")
	private Integer sortOrder;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	@Column(name="wf_for")
	private String wfFor;

	@Column(name="wf_for_code")
	private String wfForCode;

	@Column(name="wf_name")
	private String wfName;

	@Column(name="wf_stage")
	private Integer wfStage;

	@Column(name="wf_status")
	private String wfStatus;
	
	@Column(name="wf_code")
	private String wfCode;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to StudentAppWorkflow
	@OneToMany(mappedBy="fromAppStatus",fetch = FetchType.LAZY)
	private List<StudentAppWorkflow> stdAppWorkflows1;

	//bi-directional many-to-one association to StudentAppWorkflow
	@OneToMany(mappedBy="toAppStatus",fetch = FetchType.LAZY)
	private List<StudentAppWorkflow> stdAppWorkflows2;

	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="workflowStage",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications;

	public WorkflowStage() {
	}

	public Long getWorkflowStageId() {
		return this.workflowStageId;
	}

	public void setWorkflowStageId(Long workflowStageId) {
		this.workflowStageId = workflowStageId;
	}

	public Long getAvailableFor() {
		return this.availableFor;
	}

	public void setAvailableFor(Long availableFor) {
		this.availableFor = availableFor;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Long getGoBackPoint() {
		return this.goBackPoint;
	}

	public void setGoBackPoint(Long goBackPoint) {
		this.goBackPoint = goBackPoint;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsSelfAvailable() {
		return this.isSelfAvailable;
	}

	public void setIsSelfAvailable(Boolean isSelfAvailable) {
		this.isSelfAvailable = isSelfAvailable;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getWfFor() {
		return this.wfFor;
	}

	public void setWfFor(String wfFor) {
		this.wfFor = wfFor;
	}

	public String getWfForCode() {
		return this.wfForCode;
	}

	public void setWfForCode(String wfForCode) {
		this.wfForCode = wfForCode;
	}

	public String getWfName() {
		return this.wfName;
	}

	public void setWfName(String wfName) {
		this.wfName = wfName;
	}

	public Integer getWfStage() {
		return this.wfStage;
	}

	public void setWfStage(Integer wfStage) {
		this.wfStage = wfStage;
	}

	public String getWfStatus() {
		return this.wfStatus;
	}

	public void setWfStatus(String wfStatus) {
		this.wfStatus = wfStatus;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public List<StudentAppWorkflow> getStdAppWorkflows1() {
		return this.stdAppWorkflows1;
	}

	public void setStdAppWorkflows1(List<StudentAppWorkflow> stdAppWorkflows1) {
		this.stdAppWorkflows1 = stdAppWorkflows1;
	}

	public StudentAppWorkflow addStdAppWorkflows1(StudentAppWorkflow stdAppWorkflows1) {
		getStdAppWorkflows1().add(stdAppWorkflows1);
		stdAppWorkflows1.setFromAppStatus(this);

		return stdAppWorkflows1;
	}

	public StudentAppWorkflow removeStdAppWorkflows1(StudentAppWorkflow stdAppWorkflows1) {
		getStdAppWorkflows1().remove(stdAppWorkflows1);
		stdAppWorkflows1.setFromAppStatus(null);

		return stdAppWorkflows1;
	}

	public List<StudentAppWorkflow> getStdAppWorkflows2() {
		return this.stdAppWorkflows2;
	}

	public void setStdAppWorkflows2(List<StudentAppWorkflow> stdAppWorkflows2) {
		this.stdAppWorkflows2 = stdAppWorkflows2;
	}

	public StudentAppWorkflow addStdAppWorkflows2(StudentAppWorkflow stdAppWorkflows2) {
		getStdAppWorkflows2().add(stdAppWorkflows2);
		stdAppWorkflows2.setToAppStatus(this);

		return stdAppWorkflows2;
	}

	public StudentAppWorkflow removeStdAppWorkflows2(StudentAppWorkflow stdAppWorkflows2) {
		getStdAppWorkflows2().remove(stdAppWorkflows2);
		stdAppWorkflows2.setToAppStatus(null);

		return stdAppWorkflows2;
	}

	public List<StudentApplication> getStdApplications() {
		return this.stdApplications;
	}

	public void setStdApplications(List<StudentApplication> stdApplications) {
		this.stdApplications = stdApplications;
	}

	public StudentApplication addStdApplication(StudentApplication stdApplication) {
		getStdApplications().add(stdApplication);
		stdApplication.setWorkflowStage(this);

		return stdApplication;
	}

	public StudentApplication removeStdApplication(StudentApplication stdApplication) {
		getStdApplications().remove(stdApplication);
		stdApplication.setWorkflowStage(null);

		return stdApplication;
	}

	public String getWfCode() {
		return wfCode;
	}

	public void setWfCode(String wfCode) {
		this.wfCode = wfCode;
	}
	
	

}