package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_grv_complaint_tasks database table.
 * 
 */
@Entity
@Table(name = "t_grv_complaint_tasks")
@NamedQuery(name = "ComplaintTask.findAll", query = "SELECT c FROM ComplaintTask c")
public class ComplaintTask implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_complaint_task_id")
	private Long complaintTaskId;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "followup_date")
	private Date followupDate;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_task_completed")
	private Boolean isTaskCompleted;

	private String reason;

	@Column(name = "task_description")
	private String taskDescription;

	@Column(name = "task_name")
	private String taskName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to Complaint
	@ManyToOne
	@JoinColumn(name = "fk_grv_complaint_id")
	private Complaint complaint;

	// bi-directional many-to-one association to ComplaintDetail
	@ManyToOne
	@JoinColumn(name = "fk_grv_complaint_det_id")
	private ComplaintDetail complaintDetail;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne
	@JoinColumn(name = "fk_emp_id")
	private EmployeeDetail employeeDetail;

	// bi-directional many-to-one association to StudentDetail
	@ManyToOne
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne
	@JoinColumn(name = "fk_dependent_emp_id")
	private EmployeeDetail dependentEmp;

	public ComplaintTask() {
	}

	public Long getComplaintTaskId() {
		return this.complaintTaskId;
	}

	public void setComplaintTaskId(Long complaintTaskId) {
		this.complaintTaskId = complaintTaskId;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public EmployeeDetail getDependentEmp() {
		return dependentEmp;
	}

	public void setDependentEmp(EmployeeDetail dependentEmp) {
		this.dependentEmp = dependentEmp;
	}

	public EmployeeDetail getEmployeeDetail() {
		return employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public Date getFollowupDate() {
		return this.followupDate;
	}

	public void setFollowupDate(Date followupDate) {
		this.followupDate = followupDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsTaskCompleted() {
		return this.isTaskCompleted;
	}

	public void setIsTaskCompleted(Boolean isTaskCompleted) {
		this.isTaskCompleted = isTaskCompleted;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getTaskDescription() {
		return this.taskDescription;
	}

	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	public String getTaskName() {
		return this.taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Complaint getComplaint() {
		return complaint;
	}

	public void setComplaint(Complaint complaint) {
		this.complaint = complaint;
	}

	public ComplaintDetail getComplaintDetail() {
		return complaintDetail;
	}

	public void setComplaintDetail(ComplaintDetail complaintDetail) {
		this.complaintDetail = complaintDetail;
	}

}