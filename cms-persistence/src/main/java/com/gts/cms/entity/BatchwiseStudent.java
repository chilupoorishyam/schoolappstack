package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_cm_batchwise_students database table.
 * 
 */
@Entity
@Table(name = "t_cm_batchwise_students")
@NamedQuery(name = "BatchwiseStudent.findAll", query = "SELECT b FROM BatchwiseStudent b")
public class BatchwiseStudent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_batchwise_student_id")
	private Long batchwiseStudentId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;
	
	@Column(name = "electivegroupcode")
	private String electiveGroupCode;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="from_date")
	private Date fromDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="to_date")
	private Date toDate;

	// bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_academic_year_id")
	private AcademicYear academicYear;

	// bi-directional many-to-one association to Batch
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_batch_id")
	private Batch batch;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_year_id")
	private CourseYear courseYear;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_subjecttype_catdet_id")
	private GeneralDetail subjectType;

	// bi-directional many-to-one association to GroupSection
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_group_section_id")
	private GroupSection groupSection;


	// bi-directional many-to-one association to Studentbatch
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_stdbatch_id")
	private Studentbatch studentbatch;

	// bi-directional many-to-one association to Subject
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_subject_id")
	private Subject subject;

	// bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	// bi-directional many-to-one association to ElectiveGroupyrMapping
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_elective_groupyr_mapping_id")
	private ElectiveGroupyrMapping electiveGroupyrMapping;

	public BatchwiseStudent() {
	}

	public Long getBatchwiseStudentId() {
		return this.batchwiseStudentId;
	}

	public void setBatchwiseStudentId(Long batchwiseStudentId) {
		this.batchwiseStudentId = batchwiseStudentId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public Batch getBatch() {
		return this.batch;
	}

	public void setBatch(Batch batch) {
		this.batch = batch;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public CourseYear getCourseYear() {
		return this.courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public GeneralDetail getSubjectType() {
		return this.subjectType;
	}

	public void setSubjectType(GeneralDetail subjectType) {
		this.subjectType = subjectType;
	}

	public GroupSection getGroupSection() {
		return this.groupSection;
	}

	public void setGroupSection(GroupSection groupSection) {
		this.groupSection = groupSection;
	}



	public Studentbatch getStudentbatch() {
		return this.studentbatch;
	}

	public void setStudentbatch(Studentbatch studentbatch) {
		this.studentbatch = studentbatch;
	}

	public Subject getSubject() {
		return this.subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public ElectiveGroupyrMapping getElectiveGroupyrMapping() {
		return electiveGroupyrMapping;
	}

	public void setElectiveGroupyrMapping(ElectiveGroupyrMapping electiveGroupyrMapping) {
		this.electiveGroupyrMapping = electiveGroupyrMapping;
	}

	public String getElectiveGroupCode() {
		return electiveGroupCode;
	}

	public void setElectiveGroupCode(String electiveGroupCode) {
		this.electiveGroupCode = electiveGroupCode;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
}