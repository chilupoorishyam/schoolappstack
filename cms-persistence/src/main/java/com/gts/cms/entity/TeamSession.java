package com.gts.cms.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Positive;
import java.time.LocalTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Entity
public class TeamSession {

    @Id
    @Positive
    private Long id;

    private LocalTime startTime;

    private LocalTime endTime;

}
