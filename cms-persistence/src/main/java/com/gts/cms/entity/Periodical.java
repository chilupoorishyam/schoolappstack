package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_lib_periodicals database table.
 * 
 */
@Entity
@Table(name="t_lib_periodicals")
@NamedQuery(name="Periodical.findAll", query="SELECT p FROM Periodical p")
public class Periodical implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_lib_periodical_id")
	private Long periodicalId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="followup_comments")
	private String followupComments;

	@Column(name="followup_to")
	private String followupTo;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_series_completed")
	private Boolean isSeriesCompleted;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_received_date")
	private Date lastReceivedDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="next_schedule_date")
	private Date nextScheduleDate;

	@Column(name="periodical_code")
	private String periodicalCode;

	@Column(name="periodical_description")
	private String periodicalDescription;

	@Column(name="periodical_name")
	private String periodicalName;

	@Column(name="periodical_time_value")
	private String periodicalTimeValue;

	private BigDecimal price;

	private String reason;

	@Column(name="series_count")
	private String seriesCount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="subscription_enddate")
	private Date subscriptionEnddate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	private String url;
	
	/*@Column(name="bill_no")
	private String billNo;
	
	@Column(name="bill_date")
	private Date billDate;*/
	
	@Column(name="issue_no")
	private String issueNo;
	
	@Column(name="issn_no")
	private String issnNo;
	

	@Column(name="fk_lib_supplier_ids")
	private String supplierIds;
	
	@Column(name="fk_publisher_ids")
	private String publisherIds;
	
	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_bindingtype_catdet_id")
	private GeneralDetail bindingType;
	
	//bi-directional many-to-one association to Organization
	@ManyToOne
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to LibraryDetail
	@ManyToOne
	@JoinColumn(name="fk_library_id")
	private LibraryDetail libraryDetail;

	//bi-directional many-to-one association to Bookcategory
	@ManyToOne
	@JoinColumn(name="fk_lib_bookcat_id")
	private Bookcategory bookcategory;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne
	@JoinColumn(name="fk_language_catdet_id")
	private GeneralDetail language;

	//bi-directional many-to-one association to LibSupplierDetail
	/*@ManyToOne
	@JoinColumn(name="fk_lib_supplier_id")
	private LibSupplierDetail libSupplierDetail;

	//bi-directional many-to-one association to Publisher
	@ManyToOne
	@JoinColumn(name="fk_publisher_id")
	private Publisher publisher;*/

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne
	@JoinColumn(name="fk_periodicaltype_catdet_id")
	private GeneralDetail periodicalType;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne
	@JoinColumn(name="fk_periodicalfrequency_catdet_id")
	private GeneralDetail periodicalFrequency;
	
	@ManyToOne
	@JoinColumn(name="fk_paymentfrequency_catdet_id")
	private GeneralDetail paymentFrequency;

	//bi-directional many-to-one association to PeriodicalsDetail
	@OneToMany(mappedBy="periodical",cascade = CascadeType.ALL)
	private List<PeriodicalsDetail> periodicalsDetails;
	
	@OneToMany(mappedBy="periodical",cascade = CascadeType.ALL)
	private List<LibPeriodicalsPurchaseDetails> periodicalsPurchaseDetails;

	public Periodical() {
	}

	public Long getPeriodicalId() {
		return this.periodicalId;
	}

	public void setPeriodicalId(Long periodicalId) {
		this.periodicalId = periodicalId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getFollowupComments() {
		return this.followupComments;
	}

	public void setFollowupComments(String followupComments) {
		this.followupComments = followupComments;
	}

	public String getFollowupTo() {
		return this.followupTo;
	}

	public void setFollowupTo(String followupTo) {
		this.followupTo = followupTo;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsSeriesCompleted() {
		return this.isSeriesCompleted;
	}

	public void setIsSeriesCompleted(Boolean isSeriesCompleted) {
		this.isSeriesCompleted = isSeriesCompleted;
	}

	public Date getLastReceivedDate() {
		return this.lastReceivedDate;
	}

	public void setLastReceivedDate(Date lastReceivedDate) {
		this.lastReceivedDate = lastReceivedDate;
	}

	public Date getNextScheduleDate() {
		return this.nextScheduleDate;
	}

	public void setNextScheduleDate(Date nextScheduleDate) {
		this.nextScheduleDate = nextScheduleDate;
	}

	public String getPeriodicalCode() {
		return this.periodicalCode;
	}

	public void setPeriodicalCode(String periodicalCode) {
		this.periodicalCode = periodicalCode;
	}

	public String getPeriodicalDescription() {
		return this.periodicalDescription;
	}

	public void setPeriodicalDescription(String periodicalDescription) {
		this.periodicalDescription = periodicalDescription;
	}

	public String getPeriodicalName() {
		return this.periodicalName;
	}

	public void setPeriodicalName(String periodicalName) {
		this.periodicalName = periodicalName;
	}

	public String getPeriodicalTimeValue() {
		return this.periodicalTimeValue;
	}

	public void setPeriodicalTimeValue(String periodicalTimeValue) {
		this.periodicalTimeValue = periodicalTimeValue;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSeriesCount() {
		return this.seriesCount;
	}

	public void setSeriesCount(String seriesCount) {
		this.seriesCount = seriesCount;
	}

	public Date getSubscriptionEnddate() {
		return this.subscriptionEnddate;
	}

	public void setSubscriptionEnddate(Date subscriptionEnddate) {
		this.subscriptionEnddate = subscriptionEnddate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public LibraryDetail getLibraryDetail() {
		return this.libraryDetail;
	}

	public void setLibraryDetail(LibraryDetail libraryDetail) {
		this.libraryDetail = libraryDetail;
	}

	public Bookcategory getBookcategory() {
		return this.bookcategory;
	}

	public void setBookcategory(Bookcategory bookcategory) {
		this.bookcategory = bookcategory;
	}

	public GeneralDetail getLanguage() {
		return this.language;
	}

	public void setLanguage(GeneralDetail language) {
		this.language = language;
	}

	public GeneralDetail getPeriodicalType() {
		return this.periodicalType;
	}

	public void setPeriodicalType(GeneralDetail periodicalType) {
		this.periodicalType = periodicalType;
	}

	public GeneralDetail getPeriodicalFrequency() {
		return this.periodicalFrequency;
	}

	public void setPeriodicalFrequency(GeneralDetail periodicalFrequency) {
		this.periodicalFrequency = periodicalFrequency;
	}

	public List<PeriodicalsDetail> getPeriodicalsDetails() {
		return this.periodicalsDetails;
	}

	public void setPeriodicalsDetails(List<PeriodicalsDetail> periodicalsDetails) {
		this.periodicalsDetails = periodicalsDetails;
	}

	public PeriodicalsDetail addPeriodicalsDetail(PeriodicalsDetail periodicalsDetail) {
		getPeriodicalsDetails().add(periodicalsDetail);
		periodicalsDetail.setPeriodical(this);

		return periodicalsDetail;
	}

	public PeriodicalsDetail removePeriodicalsDetail(PeriodicalsDetail periodicalsDetail) {
		getPeriodicalsDetails().remove(periodicalsDetail);
		periodicalsDetail.setPeriodical(null);

		return periodicalsDetail;
	}

	public String getIssueNo() {
		return issueNo;
	}

	public void setIssueNo(String issueNo) {
		this.issueNo = issueNo;
	}

	public String getIssnNo() {
		return issnNo;
	}

	public void setIssnNo(String issnNo) {
		this.issnNo = issnNo;
	}

	public GeneralDetail getBindingType() {
		return bindingType;
	}

	public void setBindingType(GeneralDetail bindingType) {
		this.bindingType = bindingType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getSupplierIds() {
		return supplierIds;
	}

	public String getPublisherIds() {
		return publisherIds;
	}

	public void setSupplierIds(String supplierIds) {
		this.supplierIds = supplierIds;
	}

	public void setPublisherIds(String publisherIds) {
		this.publisherIds = publisherIds;
	}

	public GeneralDetail getPaymentFrequency() {
		return paymentFrequency;
	}

	public void setPaymentFrequency(GeneralDetail paymentFrequency) {
		this.paymentFrequency = paymentFrequency;
	}

	public List<LibPeriodicalsPurchaseDetails> getPeriodicalsPurchaseDetails() {
		return periodicalsPurchaseDetails;
	}

	public void setPeriodicalsPurchaseDetails(List<LibPeriodicalsPurchaseDetails> periodicalsPurchaseDetails) {
		this.periodicalsPurchaseDetails = periodicalsPurchaseDetails;
	}
	
}