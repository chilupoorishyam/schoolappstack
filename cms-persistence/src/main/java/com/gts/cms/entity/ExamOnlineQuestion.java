package com.gts.cms.entity;

import lombok.Data;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_exam_online_questions database table.
 */
@Data
@Entity
@Table(name = "t_exam_online_questions")
@NamedQuery(name = "ExamOnlineQuestion.findAll", query = "SELECT c FROM ExamOnlineQuestion c")
public class ExamOnlineQuestion implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_exam_online_question_id")
    private Long examOnlineQuestionId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "question")
    private String question;

    @ManyToOne
    @JoinColumn(name = "fk_exam_online_paper_id")
    private ExamOnlinePaper examOnlinePaperId;

    @ManyToOne
    @JoinColumn(name = "fk_sub_unit_topic_id")
    private SubjectUnitTopic subjectUnitTopic;

    @ManyToOne
    @JoinColumn(name = "fk_sub_units_id")
    private SubjectUnit subjectUnit;

    @ManyToOne
    @JoinColumn(name = "fk_fbinputtype_catdet_id")
    private GeneralDetail fbInputTypeCat;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "is_allow_multiple_answers")
    private Boolean isAllowMultipleAnswers;

    private Integer marks;

    @Column(name = "no_of_correct_answers")
    private Integer noOfCorrectAnswers;

    @Column(name = "no_of_options")
    private Integer noOfOptions;

    @Column(name = "question_level")
    private Integer questionLevel;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    @OneToMany(mappedBy = "examOnlineQuestion", cascade = CascadeType.MERGE)
    private List<ExamOnlineQuestionOption> examOnlineQuestionOptions;

    @OneToMany(mappedBy = "examOnlineQuestion", cascade = CascadeType.MERGE)
    private List<ExamOnlineQuestionPaper> examOnlineQuestionPapers;

    @OneToMany(mappedBy = "examOnlineQuestion", cascade = CascadeType.MERGE)
    private List<ExamOnlineStudentAnswers> examOnlineStudentAnswers;
}