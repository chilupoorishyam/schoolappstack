package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_std_detained_students database table.
 * 
 */
@Entity
@Table(name = "t_std_detained_students")
@NamedQuery(name = "StdDetainedStudent.findAll", query = "SELECT s FROM StdDetainedStudent s")
public class StdDetainedStudent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_detained_student_id")
	private Long detainedStudentId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "detained_on")
	private Date detainedOn;

	@Column(name = "detention_reason")
	private String detentionReason;

	// bi-directional many-to-one association to AcademicYear
	@ManyToOne
	@JoinColumn(name = "fk_academic_year_id")
	private AcademicYear academicYear;

	// bi-directional many-to-one association to StudentAcademicbatch
	@ManyToOne
	@JoinColumn(name = "fk_std_student_academicbatch_id")
	private StudentAcademicbatch studentAcademicbatch;

	@Column(name = "is_academicbatch_updated")
	private Boolean isAcademicbatchUpdated;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Column(name = "status_comments")
	private String statusComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "status_updated_on")
	private Date statusUpdatedOn;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to TEmpDetail
	@ManyToOne
	@JoinColumn(name = "fk_status_emp_id")
	private EmployeeDetail empDetail;

	// bi-directional many-to-one association to TMSchool
	@ManyToOne
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to TMCourse
	@ManyToOne
	@JoinColumn(name = "fk_course_id")
	private Course course;

	// bi-directional many-to-one association to TMCourseYear
	@ManyToOne
	@JoinColumn(name = "fk_from_course_year_id")
	private CourseYear courseYear;

	// bi-directional many-to-one association to TMGeneralDetail
	@ManyToOne
	@JoinColumn(name = "fk_student_status_catdet_id")
	private GeneralDetail studentStatusCat;

	// bi-directional many-to-one association to TMGeneralDetail
	@ManyToOne
	@JoinColumn(name = "fk_approval_status__catdet_id")
	private GeneralDetail approvalStatusCat;

	// bi-directional many-to-one association to TMGroupSection
	@ManyToOne
	@JoinColumn(name = "fk_from_group_section_id")
	private GroupSection groupSection;

	// bi-directional many-to-one association to TStdStudentDetail
	@ManyToOne
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	public StdDetainedStudent() {
	}

	public Long getDetainedStudentId() {
		return this.detainedStudentId;
	}

	public void setDetainedStudentId(Long detainedStudentId) {
		this.detainedStudentId = detainedStudentId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getDetainedOn() {
		return this.detainedOn;
	}

	public void setDetainedOn(Date detainedOn) {
		this.detainedOn = detainedOn;
	}

	public String getDetentionReason() {
		return this.detentionReason;
	}

	public void setDetentionReason(String detentionReason) {
		this.detentionReason = detentionReason;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public StudentAcademicbatch getStudentAcademicbatch() {
		return studentAcademicbatch;
	}

	public void setStudentAcademicbatch(StudentAcademicbatch studentAcademicbatch) {
		this.studentAcademicbatch = studentAcademicbatch;
	}

	public Boolean getIsAcademicbatchUpdated() {
		return this.isAcademicbatchUpdated;
	}

	public void setIsAcademicbatchUpdated(Boolean isAcademicbatchUpdated) {
		this.isAcademicbatchUpdated = isAcademicbatchUpdated;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStatusComments() {
		return this.statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public Date getStatusUpdatedOn() {
		return this.statusUpdatedOn;
	}

	public void setStatusUpdatedOn(Date statusUpdatedOn) {
		this.statusUpdatedOn = statusUpdatedOn;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getEmpDetail() {
		return empDetail;
	}

	public void setEmpDetail(EmployeeDetail empDetail) {
		this.empDetail = empDetail;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public CourseYear getCourseYear() {
		return courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public GeneralDetail getStudentStatusCat() {
		return studentStatusCat;
	}

	public void setStudentStatusCat(GeneralDetail studentStatusCat) {
		this.studentStatusCat = studentStatusCat;
	}

	public GeneralDetail getApprovalStatusCat() {
		return approvalStatusCat;
	}

	public void setApprovalStatusCat(GeneralDetail approvalStatusCat) {
		this.approvalStatusCat = approvalStatusCat;
	}

	public GroupSection getGroupSection() {
		return groupSection;
	}

	public void setGroupSection(GroupSection groupSection) {
		this.groupSection = groupSection;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

}