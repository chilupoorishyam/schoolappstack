package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_qualification_groups database table.
 * 
 */
@Entity
@Table(name="t_m_qualification_groups")
@NamedQuery(name="QualificationGroup.findAll", query="SELECT q FROM QualificationGroup q")
public class QualificationGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_qualif_group_id")
	private Long qualificationGroupId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="qualification_group_code")
	private String qualificationGroupCode;

	@Column(name="qualification_group_name")
	private String qualificationGroupName;

	private String reason;

	@Column(name="sort_order")
	private Integer sortOrder;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to Qualification
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_qualification_id")
	private Qualification qualification;

	//bi-directional many-to-one association to StudentEnquiry
	@OneToMany(mappedBy="qualificationGroup",fetch = FetchType.LAZY)
	private List<StudentEnquiry> enquiries;

	public QualificationGroup() {
	}

	public Long getQualificationGroupId() {
		return this.qualificationGroupId;
	}

	public void setQualificationGroupId(Long qualificationGroupId) {
		this.qualificationGroupId = qualificationGroupId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getQualificationGroupCode() {
		return this.qualificationGroupCode;
	}

	public void setQualificationGroupCode(String qualificationGroupCode) {
		this.qualificationGroupCode = qualificationGroupCode;
	}

	public String getQualificationGroupName() {
		return this.qualificationGroupName;
	}

	public void setQualificationGroupName(String qualificationGroupName) {
		this.qualificationGroupName = qualificationGroupName;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Qualification getQualification() {
		return this.qualification;
	}

	public void setQualification(Qualification qualification) {
		this.qualification = qualification;
	}

	public List<StudentEnquiry> getEnquiries() {
		return this.enquiries;
	}

	public void setEnquiries(List<StudentEnquiry> enquiries) {
		this.enquiries = enquiries;
	}

	public StudentEnquiry addEnquiry(StudentEnquiry enquiry) {
		getEnquiries().add(enquiry);
		enquiry.setQualificationGroup(this);

		return enquiry;
	}

	public StudentEnquiry removeEnquiry(StudentEnquiry enquiry) {
		getEnquiries().remove(enquiry);
		enquiry.setQualificationGroup(null);

		return enquiry;
	}

}