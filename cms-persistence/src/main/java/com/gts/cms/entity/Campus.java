package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_campus database table.
 * 
 */
@Entity
@Table(name="t_m_campus")
@NamedQuery(name="Campus.findAll", query="SELECT c FROM Campus c")
public class Campus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_campus_id")
	private Long campusId;

	@Column(name="campus_code")
	private String campusCode;

	@Column(name="campus_name")
	private String campusName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to LibraryDetail
	@OneToMany(mappedBy="campus",fetch = FetchType.LAZY)
	private List<LibraryDetail> libraryDetails;

	//bi-directional many-to-one association to Building
	@OneToMany(mappedBy="campus",fetch = FetchType.LAZY)
	private List<Building> buildings;

	//bi-directional many-to-one association to District
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_district_id")
	private District district;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to School
	@OneToMany(mappedBy="campus",fetch = FetchType.LAZY)
	private List<School> schools;

	//bi-directional many-to-one association to TransportDetail
	@OneToMany(mappedBy="campus",fetch = FetchType.LAZY)
	private List<TransportDetail> transportDetails;

	//bi-directional many-to-one association to StudentAttendanceTimeSetting
	@OneToMany(mappedBy="campus",fetch = FetchType.LAZY)
	private List<StudentAttendanceTimeSetting> stdAttendanceTimeSettings;

	public Campus() {
	}

	public Long getCampusId() {
		return this.campusId;
	}

	public void setCampusId(Long campusId) {
		this.campusId = campusId;
	}

	public String getCampusCode() {
		return this.campusCode;
	}

	public void setCampusCode(String campusCode) {
		this.campusCode = campusCode;
	}

	public String getCampusName() {
		return this.campusName;
	}

	public void setCampusName(String campusName) {
		this.campusName = campusName;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<LibraryDetail> getLibraryDetails() {
		return this.libraryDetails;
	}

	public void setLibraryDetails(List<LibraryDetail> libraryDetails) {
		this.libraryDetails = libraryDetails;
	}

	public LibraryDetail addLibraryDetail(LibraryDetail libraryDetail) {
		getLibraryDetails().add(libraryDetail);
		libraryDetail.setCampus(this);

		return libraryDetail;
	}

	public LibraryDetail removeLibraryDetail(LibraryDetail libraryDetail) {
		getLibraryDetails().remove(libraryDetail);
		libraryDetail.setCampus(null);

		return libraryDetail;
	}

	public List<Building> getBuildings() {
		return this.buildings;
	}

	public void setBuildings(List<Building> buildings) {
		this.buildings = buildings;
	}

	public Building addBuilding(Building building) {
		getBuildings().add(building);
		building.setCampus(this);

		return building;
	}

	public Building removeBuilding(Building building) {
		getBuildings().remove(building);
		building.setCampus(null);

		return building;
	}

	public District getDistrict() {
		return this.district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public List<School> getSchools() {
		return this.schools;
	}

	public void setSchools(List<School> schools) {
		this.schools = schools;
	}

	public School addSchool(School school) {
		getSchools().add(school);
		school.setCampus(this);

		return school;
	}

	public School removeSchool(School school) {
		getSchools().remove(school);
		school.setCampus(null);

		return school;
	}

	public List<TransportDetail> getTransportDetails() {
		return this.transportDetails;
	}

	public void setTransportDetails(List<TransportDetail> transportDetails) {
		this.transportDetails = transportDetails;
	}

	public TransportDetail addTransportDetail(TransportDetail transportDetail) {
		getTransportDetails().add(transportDetail);
		transportDetail.setCampus(this);

		return transportDetail;
	}

	public TransportDetail removeTransportDetail(TransportDetail transportDetail) {
		getTransportDetails().remove(transportDetail);
		transportDetail.setCampus(null);

		return transportDetail;
	}

	public List<StudentAttendanceTimeSetting> getStdAttendanceTimeSettings() {
		return this.stdAttendanceTimeSettings;
	}

	public void setStdAttendanceTimeSettings(List<StudentAttendanceTimeSetting> stdAttendanceTimeSettings) {
		this.stdAttendanceTimeSettings = stdAttendanceTimeSettings;
	}

	public StudentAttendanceTimeSetting addStdAttendanceTimeSetting(StudentAttendanceTimeSetting stdAttendanceTimeSetting) {
		getStdAttendanceTimeSettings().add(stdAttendanceTimeSetting);
		stdAttendanceTimeSetting.setCampus(this);

		return stdAttendanceTimeSetting;
	}

	public StudentAttendanceTimeSetting removeStdAttendanceTimeSetting(StudentAttendanceTimeSetting stdAttendanceTimeSetting) {
		getStdAttendanceTimeSettings().remove(stdAttendanceTimeSetting);
		stdAttendanceTimeSetting.setCampus(null);

		return stdAttendanceTimeSetting;
	}

}