package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_lib_book_purchase_details database table.
 * 
 */
@Entity
@Table(name = "t_lib_book_purchase_details")
@NamedQuery(name = "BookPurchaseDetail.findAll", query = "SELECT b FROM BookPurchaseDetail b")
public class BookPurchaseDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_book_purchase_detail_id")
	private Long bookPurchaseDetailId;
	
	@Column(name = "amount")
	private BigDecimal amount;
	
	@Column(name = "book_amount")
	private BigDecimal bookAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_of_purchase")
	private Date dateOfPurchase;

	@Column(name = "purchase_source")
	private String purchaseSource;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "no_of_books")
	private Integer noOfBooks;

	@Column(name = "purchase_receipt_no")
	private String purchaseReceiptNo;

	@Column(name = "purchase_receipt_path")
	private String purchaseReceiptPath;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to Book
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_lib_book_id")
	private Book book;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_currency_catdet_id")
	private GeneralDetail currency;

	// bi-directional many-to-one association to Book
	@OneToMany(mappedBy = "bookPurchaseDetail", fetch = FetchType.LAZY)
	private List<Book> books;

	public BookPurchaseDetail() {
	}

	public Long getBookPurchaseDetailId() {
		return this.bookPurchaseDetailId;
	}

	public void setBookPurchaseDetailId(Long bookPurchaseDetailId) {
		this.bookPurchaseDetailId = bookPurchaseDetailId;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getDateOfPurchase() {
		return this.dateOfPurchase;
	}

	public void setDateOfPurchase(Date dateOfPurchase) {
		this.dateOfPurchase = dateOfPurchase;
	}

	public String getPurchaseSource() {
		return purchaseSource;
	}

	public void setPurchaseSource(String purchaseSource) {
		this.purchaseSource = purchaseSource;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getNoOfBooks() {
		return this.noOfBooks;
	}

	public void setNoOfBooks(Integer noOfBooks) {
		this.noOfBooks = noOfBooks;
	}

	public String getPurchaseReceiptNo() {
		return this.purchaseReceiptNo;
	}

	public void setPurchaseReceiptNo(String purchaseReceiptNo) {
		this.purchaseReceiptNo = purchaseReceiptNo;
	}

	public String getPurchaseReceiptPath() {
		return this.purchaseReceiptPath;
	}

	public void setPurchaseReceiptPath(String purchaseReceiptPath) {
		this.purchaseReceiptPath = purchaseReceiptPath;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Book getBook() {
		return this.book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public List<Book> getBooks() {
		return this.books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public Book addBook(Book book) {
		getBooks().add(book);
		book.setBookPurchaseDetail(this);

		return book;
	}

	public Book removeBook(Book book) {
		getBooks().remove(book);
		book.setBookPurchaseDetail(null);

		return book;
	}

	public GeneralDetail getCurrency() {
		return currency;
	}

	public void setCurrency(GeneralDetail currency) {
		this.currency = currency;
	}

	public BigDecimal getBookAmount() {
		return bookAmount;
	}

	public void setBookAmount(BigDecimal bookAmount) {
		this.bookAmount = bookAmount;
	}
	
}