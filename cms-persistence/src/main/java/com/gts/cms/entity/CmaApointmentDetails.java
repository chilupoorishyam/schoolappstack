package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

/**
 * The persistent class for the t_cm_appointment_details database table.
 */
@Data
@Entity
@Table(name = "t_cm_appointment_details")
@NamedQuery(name = "CMAppointmentDetails.findAll", query = "SELECT c FROM CmaApointmentDetails c")
public class CmaApointmentDetails implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_appointment_details_id")
    private Long appointmentDetailsId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @ManyToOne
    @JoinColumn(name = "fk_school_id")
    private School school;

    @ManyToOne
    @JoinColumn(name = "fk_student_id")
    private StudentDetail studentDetail;

    @ManyToOne
    @JoinColumn(name = "fk_std_relation_catdet_id")
    private GeneralDetail stdRelationCatdetId;

    @ManyToOne
    @JoinColumn(name = "fk_app_with_emp_id")
    private EmployeeDetail employeeDetail;

    @ManyToOne
    @JoinColumn(name = "fk_emp_dept_id")
    private Department department;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "appointment_date")
    private Date appointmentDate;

    @Column(name = "start_time")
    private Time startTime;

    @Column(name = "end_time")
    private Time endTime;

    @Column(name = "meeting_notes")
    private String meetingNotes;
}