package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_exam_addt_fee_receipts database table.
 * 
 */
@Entity
@Table(name="t_exam_addt_fee_receipts")
@NamedQuery(name="ExamAdditionalFeeReceipt.findAll", query="SELECT efr FROM ExamAdditionalFeeReceipt efr")
public class ExamAdditionalFeeReceipt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_exam_addt_fee_receipt_id")
	private Long examAddtFeeReceiptId;
	
	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;
	
	// bi-directional many-to-one association to ExamFeeReceipt
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_fee_receipt_id")
	private ExamFeeReceipt examFeeReceipt;
	
	// bi-directional many-to-one association to ExamFeeAdditionalStructure
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_fee_addt_id")
	private ExamFeeAdditionalStructure examFeeAdditionalStructure;
	
	@Column(name="addt_fee_amount")
	private BigDecimal addtFeeAmount;
	
	@Column(name="addt_receipt_no")
	private String addtReceiptNo;
	
	@Temporal(TemporalType.DATE)
	@Column(name="addt_receipt_date")
	private Date addtReceiptDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_collected_emp_id")
	private EmployeeDetail collectedEmp;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_adt_examfeetype_catdet_id")
	private GeneralDetail addtExamFeeTypeCat;
	
	@Column(name="is_active")
	private Boolean isActive;
	
	private String reason;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;
	
	@Column(name ="is_Refund")
	private Boolean isRefund;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_refund_emp_id")
	private EmployeeDetail refundEmp;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "refund_date")
	private Date refundDate;
	
	@Column(name = "refund_Reason")
	private String refundReason;
	
	// bi-directional many-to-one association to ExamStudentDetail
	@OneToMany(mappedBy = "examAdditionalFeeReceipt", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ExamRevisionSubject> examRevisionSubjects;

	public Long getExamAddtFeeReceiptId() {
		return examAddtFeeReceiptId;
	}

	public void setExamAddtFeeReceiptId(Long examAddtFeeReceiptId) {
		this.examAddtFeeReceiptId = examAddtFeeReceiptId;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public ExamFeeReceipt getExamFeeReceipt() {
		return examFeeReceipt;
	}

	public void setExamFeeReceipt(ExamFeeReceipt examFeeReceipt) {
		this.examFeeReceipt = examFeeReceipt;
	}

	public ExamFeeAdditionalStructure getExamFeeAdditionalStructure() {
		return examFeeAdditionalStructure;
	}

	public void setExamFeeAdditionalStructure(ExamFeeAdditionalStructure examFeeAdditionalStructure) {
		this.examFeeAdditionalStructure = examFeeAdditionalStructure;
	}

	public BigDecimal getAddtFeeAmount() {
		return addtFeeAmount;
	}

	public void setAddtFeeAmount(BigDecimal addtFeeAmount) {
		this.addtFeeAmount = addtFeeAmount;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getAddtReceiptNo() {
		return addtReceiptNo;
	}

	public void setAddtReceiptNo(String addtReceiptNo) {
		this.addtReceiptNo = addtReceiptNo;
	}

	public Date getAddtReceiptDate() {
		return addtReceiptDate;
	}

	public void setAddtReceiptDate(Date addtReceiptDate) {
		this.addtReceiptDate = addtReceiptDate;
	}

	public EmployeeDetail getCollectedEmp() {
		return collectedEmp;
	}

	public void setCollectedEmp(EmployeeDetail collectedEmp) {
		this.collectedEmp = collectedEmp;
	}

	public GeneralDetail getAddtExamFeeTypeCat() {
		return addtExamFeeTypeCat;
	}

	public void setAddtExamFeeTypeCat(GeneralDetail addtExamFeeTypeCat) {
		this.addtExamFeeTypeCat = addtExamFeeTypeCat;
	}

	public List<ExamRevisionSubject> getExamRevisionSubjects() {
		return examRevisionSubjects;
	}

	public void setExamRevisionSubjects(List<ExamRevisionSubject> examRevisionSubjects) {
		this.examRevisionSubjects = examRevisionSubjects;
	}

	public Boolean getIsRefund() {
		return isRefund;
	}

	public void setIsRefund(Boolean isRefund) {
		this.isRefund = isRefund;
	}

	public EmployeeDetail getRefundEmp() {
		return refundEmp;
	}

	public void setRefundEmp(EmployeeDetail refundEmp) {
		this.refundEmp = refundEmp;
	}

	public Date getRefundDate() {
		return refundDate;
	}

	public void setRefundDate(Date refundDate) {
		this.refundDate = refundDate;
	}

	public String getRefundReason() {
		return refundReason;
	}

	public void setRefundReason(String refundReason) {
		this.refundReason = refundReason;
	}
}