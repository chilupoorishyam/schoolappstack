package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="t_m_time_dimension")
@NamedQuery(name="TimeDimension.findAll", query="SELECT t FROM TimeDimension t")
public class TimeDimension implements Serializable {
	private static final long serialVersionUID = 1L;
	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_time_dim_id")
	private Long timedimensionId;
	
    @Column(name="date")
	private Date date;
    

    @Column(name="date_num")
	private Long dateNum;
    
    @Column(name="day_num")
  	private Long dayNum;
    
    @Column(name="day_of_year")
  	private Long dayOfYear;
    
    @Column(name="day_of_week")
  	private Long dayOfWeek;
    
    @Column(name="day_of_week_name")
  	private String dayOfWeekName;
    
    @Column(name="week_num")
  	private Integer weekNum;
    
    @Column(name="week_begin_date")
  	private Date weekBegainDate;
    
    @Column(name="week_end_date")
  	private Date weekEndDate;
    
    
    @Column(name="last_week_begin_date")
  	private Date lastWeekBeginDate;
    
    @Column(name="last_week_end_date")
  	private Date lastWeekEndDate;
    
    
    @Column(name="last_2_week_begin_date")
  	private Date lastIIWeekBeginDate;
    
    @Column(name="last_2_week_end_date")
  	private Date lastIIWeekEndDate;
    
    @Column(name="month_num")
  	private Integer monthNum;
    
    @Column(name="month_name")
  	private String monthName;
    
    @Column(name="YEARMONTH_NUM")
  	private Long yearMonthNum;
    
    @Column(name="last_month_num")
  	private Integer lastMonthNum;
    
    @Column(name="last_month_name")
  	private String lastMonthName;
    
    @Column(name="last_month_year")
  	private Long lastMonthYear;
    
    @Column(name="last_yearmonth_num")
  	private Long lastYearMonthNum;
    
    @Column(name="quarter_num")
  	private Integer quarterNum;
    
    @Column(name="year_num")
  	private Long yearNum;
    
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDt;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_date")
	private Date updatedDt;
    
    
	public Long getTimedimensionId() {
		return timedimensionId;
	}

	public void setTimedimensionId(Long timedimensionId) {
		this.timedimensionId = timedimensionId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Long getDateNum() {
		return dateNum;
	}

	public void setDateNum(Long dateNum) {
		this.dateNum = dateNum;
	}

	public Long getDayNum() {
		return dayNum;
	}

	public void setDayNum(Long dayNum) {
		this.dayNum = dayNum;
	}

	public Long getDayOfYear() {
		return dayOfYear;
	}

	public void setDayOfYear(Long dayOfYear) {
		this.dayOfYear = dayOfYear;
	}

	public Long getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(Long dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public String getDayOfWeekName() {
		return dayOfWeekName;
	}

	public void setDayOfWeekName(String dayOfWeekName) {
		this.dayOfWeekName = dayOfWeekName;
	}

	public Integer getWeekNum() {
		return weekNum;
	}

	public void setWeekNum(Integer weekNum) {
		this.weekNum = weekNum;
	}

	public Date getWeekBegainDate() {
		return weekBegainDate;
	}

	public void setWeekBegainDate(Date weekBegainDate) {
		this.weekBegainDate = weekBegainDate;
	}

	public Date getWeekEndDate() {
		return weekEndDate;
	}

	public void setWeekEndDate(Date weekEndDate) {
		this.weekEndDate = weekEndDate;
	}

	public Date getLastWeekBeginDate() {
		return lastWeekBeginDate;
	}

	public void setLastWeekBeginDate(Date lastWeekBeginDate) {
		this.lastWeekBeginDate = lastWeekBeginDate;
	}

	public Date getLastWeekEndDate() {
		return lastWeekEndDate;
	}

	public void setLastWeekEndDate(Date lastWeekEndDate) {
		this.lastWeekEndDate = lastWeekEndDate;
	}

	public Date getLastIIWeekBeginDate() {
		return lastIIWeekBeginDate;
	}

	public void setLastIIWeekBeginDate(Date lastIIWeekBeginDate) {
		this.lastIIWeekBeginDate = lastIIWeekBeginDate;
	}

	public Date getLastIIWeekEndDate() {
		return lastIIWeekEndDate;
	}

	public void setLastIIWeekEndDate(Date lastIIWeekEndDate) {
		this.lastIIWeekEndDate = lastIIWeekEndDate;
	}

	public Integer getMonthNum() {
		return monthNum;
	}

	public void setMonthNum(Integer monthNum) {
		this.monthNum = monthNum;
	}

	public String getMonthName() {
		return monthName;
	}

	public void setMonthName(String monthName) {
		this.monthName = monthName;
	}

	public Long getYearMonthNum() {
		return yearMonthNum;
	}

	public void setYearMonthNum(Long yearMonthNum) {
		this.yearMonthNum = yearMonthNum;
	}

	public Integer getLastMonthNum() {
		return lastMonthNum;
	}

	public void setLastMonthNum(Integer lastMonthNum) {
		this.lastMonthNum = lastMonthNum;
	}

	public String getLastMonthName() {
		return lastMonthName;
	}

	public void setLastMonthName(String lastMonthName) {
		this.lastMonthName = lastMonthName;
	}

	public Long getLastMonthYear() {
		return lastMonthYear;
	}

	public void setLastMonthYear(Long lastMonthYear) {
		this.lastMonthYear = lastMonthYear;
	}

	public Long getLastYearMonthNum() {
		return lastYearMonthNum;
	}

	public void setLastYearMonthNum(Long lastYearMonthNum) {
		this.lastYearMonthNum = lastYearMonthNum;
	}

	public Integer getQuarterNum() {
		return quarterNum;
	}

	public void setQuarterNum(Integer quarterNum) {
		this.quarterNum = quarterNum;
	}

	public Long getYearNum() {
		return yearNum;
	}

	public void setYearNum(Long yearNum) {
		this.yearNum = yearNum;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

    
}
