package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_cm_std_sub_registration database table.
 * 
 */
@Entity
@Table(name = "t_cm_std_sub_registration")
@NamedQuery(name = "StdSubRegistration.findAll", query = "SELECT s FROM StdSubRegistration s")
public class StdSubRegistration implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_std_sub_reg_id")
	private Long stdSubRegId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne
	@JoinColumn(name = "fk_academic_year_id")
	private AcademicYear academicYear;

	@ManyToOne
	@JoinColumn(name = "fk_school_id")
	private School school;



	@ManyToOne
	@JoinColumn(name = "fk_course_year_id")
	private CourseYear courseYear;



	@ManyToOne
	@JoinColumn(name = "fk_status_emp_id")
	private EmployeeDetail statusEmp;

	@ManyToOne
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	@ManyToOne
	@JoinColumn(name = "fk_subreg_status_catdet_id")
	private GeneralDetail subregStatusCat;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "registered_date")
	private Date registeredDate;

	@Column(name = "status_comments")
	private String statusComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "status_date")
	private Date statusDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to StdSubRegDetail
	@OneToMany(mappedBy = "stdSubRegistration" , cascade = CascadeType.ALL)
	private List<StdSubRegDetail> stdSubRegDetails;

	// bi-directional many-to-one association to StdSubRegWorkflow
	@OneToMany(mappedBy = "stdSubRegistration" , cascade = CascadeType.ALL)
	private List<StdSubRegWorkflow> stdSubRegWorkflows;

	public Long getStdSubRegId() {
		return stdSubRegId;
	}

	public void setStdSubRegId(Long stdSubRegId) {
		this.stdSubRegId = stdSubRegId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}



	public CourseYear getCourseYear() {
		return courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}


	public EmployeeDetail getStatusEmp() {
		return statusEmp;
	}

	public void setStatusEmp(EmployeeDetail statusEmp) {
		this.statusEmp = statusEmp;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public GeneralDetail getSubregStatusCat() {
		return subregStatusCat;
	}

	public void setSubregStatusCat(GeneralDetail subregStatusCat) {
		this.subregStatusCat = subregStatusCat;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getRegisteredDate() {
		return registeredDate;
	}

	public void setRegisteredDate(Date registeredDate) {
		this.registeredDate = registeredDate;
	}

	public String getStatusComments() {
		return statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public Date getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<StdSubRegDetail> getStdSubRegDetails() {
		return stdSubRegDetails;
	}

	public void setStdSubRegDetails(List<StdSubRegDetail> stdSubRegDetails) {
		this.stdSubRegDetails = stdSubRegDetails;
	}

	public List<StdSubRegWorkflow> getStdSubRegWorkflows() {
		return stdSubRegWorkflows;
	}

	public void setStdSubRegWorkflows(List<StdSubRegWorkflow> stdSubRegWorkflows) {
		this.stdSubRegWorkflows = stdSubRegWorkflows;
	}

}