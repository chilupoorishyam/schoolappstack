package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_lib_book_issuedetails_history database table.
 * 
 */
@Entity
@Table(name = "t_lib_book_issuedetails_history")
@NamedQuery(name = "BookIssuedetailsHistory.findAll", query = "SELECT b FROM BookIssuedetailsHistory b")
public class BookIssuedetailsHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_lib_book_issuedetailshistory_id")
	private Long bookIssuedetailshistoryId;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	/*@Column(name = "fine_collected_amount")
	private BigDecimal fineCollectedAmount;*/

	@Column(name = "is_active")
	private Boolean isActive;

	private Boolean isrenewaled;

	private Boolean isreturned;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "issue_duedate")
	private Date issueDuedate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "issue_fromdate")
	private Date issueFromdate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "issue_todate")
	private Date issueTodate;

	private String reason;

	@Column(name = "issued_days")
	private Long issuedDays;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to BookDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_lib_book_details_id")
	private BookDetail bookDetail;

	// bi-directional many-to-one association to BookIssuedetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_lib_book_issuedetails_id")
	private BookIssuedetail bookIssuedetail;

	// bi-directional many-to-one association to LibraryDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_library_id")
	private LibraryDetail libraryDetail;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_return_bookcondition_catdet_id")
	private GeneralDetail returnBookcondition;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_book_issuedon_catdet_id")
	private GeneralDetail bookIssuedOn;

	// bi-directional many-to-one association to LibMember
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_lib_member_id")
	private LibMember libMember;
	
	@Column(name="fine_amount")
	private BigDecimal fineAmount;

	@Column(name="balance_amount")
	private BigDecimal balanceAmount;
	
	@Column(name = "is_collected")
	private Boolean isCollected;
	
	@Column(name="discount_amount")
	private BigDecimal discountAmount;
	
	@Column(name="paid_amount")
	private BigDecimal paidAmount;

	public BookIssuedetailsHistory() {
	}

	public Long getBookIssuedetailshistoryId() {
		return this.bookIssuedetailshistoryId;
	}

	public void setBookIssuedetailshistoryId(Long bookIssuedetailshistoryId) {
		this.bookIssuedetailshistoryId = bookIssuedetailshistoryId;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsrenewaled() {
		return this.isrenewaled;
	}

	public void setIsrenewaled(Boolean isrenewaled) {
		this.isrenewaled = isrenewaled;
	}

	public Boolean getIsreturned() {
		return this.isreturned;
	}

	public void setIsreturned(Boolean isreturned) {
		this.isreturned = isreturned;
	}

	public Date getIssueDuedate() {
		return this.issueDuedate;
	}

	public void setIssueDuedate(Date issueDuedate) {
		this.issueDuedate = issueDuedate;
	}

	public Date getIssueFromdate() {
		return this.issueFromdate;
	}

	public void setIssueFromdate(Date issueFromdate) {
		this.issueFromdate = issueFromdate;
	}

	public Date getIssueTodate() {
		return this.issueTodate;
	}

	public void setIssueTodate(Date issueTodate) {
		this.issueTodate = issueTodate;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Long getIssuedDays() {
		return issuedDays;
	}

	public void setIssuedDays(Long issuedDays) {
		this.issuedDays = issuedDays;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public BookDetail getBookDetail() {
		return this.bookDetail;
	}

	public void setBookDetail(BookDetail bookDetail) {
		this.bookDetail = bookDetail;
	}

	public BookIssuedetail getBookIssuedetail() {
		return this.bookIssuedetail;
	}

	public void setBookIssuedetail(BookIssuedetail bookIssuedetail) {
		this.bookIssuedetail = bookIssuedetail;
	}

	public LibraryDetail getLibraryDetail() {
		return this.libraryDetail;
	}

	public void setLibraryDetail(LibraryDetail libraryDetail) {
		this.libraryDetail = libraryDetail;
	}

	public GeneralDetail getReturnBookcondition() {
		return this.returnBookcondition;
	}

	public void setReturnBookcondition(GeneralDetail returnBookcondition) {
		this.returnBookcondition = returnBookcondition;
	}

	public GeneralDetail getBookIssuedOn() {
		return bookIssuedOn;
	}

	public void setBookIssuedOn(GeneralDetail bookIssuedOn) {
		this.bookIssuedOn = bookIssuedOn;
	}

	public LibMember getLibMember() {
		return libMember;
	}

	public void setLibMember(LibMember libMember) {
		this.libMember = libMember;
	}

	public BigDecimal getFineAmount() {
		return fineAmount;
	}

	public BigDecimal getBalanceAmount() {
		return balanceAmount;
	}

	public Boolean getIsCollected() {
		return isCollected;
	}

	public void setFineAmount(BigDecimal fineAmount) {
		this.fineAmount = fineAmount;
	}

	public void setBalanceAmount(BigDecimal balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public void setIsCollected(Boolean isCollected) {
		this.isCollected = isCollected;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public BigDecimal getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}
}