package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_lib_book_issuedetails database table.
 * 
 */
/**
 * @author GTS2
 *
 */
@Entity
@Table(name = "t_lib_book_issuedetails")
@NamedQuery(name = "BookIssuedetail.findAll", query = "SELECT b FROM BookIssuedetail b")
public class BookIssuedetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_lib_book_issuedetails_id")
	private Long bookIssuedetailsId;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	/*@Column(name = "fine_collected_date")
	private Date fineCollectedDate;
	
	@Column(name = "fine_remarks")
	private String fineRemarks;

	@Column(name = "fine_collected_amount")
	private BigDecimal fineCollectedAmount;*/

	
	@Column(name = "is_active")
	private Boolean isActive;

	private Boolean isrenewaled;

	private Boolean isreturned;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "issue_duedate")
	private Date issueDuedate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "issue_fromdate")
	private Date issueFromdate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "issue_todate")
	private Date issueTodate;
	

	@Column(name = "issued_days")
	private Long issuedDays;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to BookDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_lib_book_details_id")
	private BookDetail bookDetail;

	// bi-directional many-to-one association to LibraryDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_library_id")
	private LibraryDetail libraryDetail;
	
	// bi-directional many-to-one association to LibFineCollection
	@OneToMany(mappedBy = "libBookIssueDetails", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	private List<LibFineCollection> libFineCollections;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_return_bookcondition_catdet_id")
	private GeneralDetail returnBookcondition;

	// bi-directional many-to-one association to BookIssuedetailsHistory
	@OneToMany(mappedBy = "bookIssuedetail", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<BookIssuedetailsHistory> bookIssuedetailsHistories;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_book_issuedon_catdet_id")
	private GeneralDetail bookIssuedOn;

	// bi-directional many-to-one association to LibMember
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_lib_member_id")
	private LibMember libMember;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_finetype_catdet_id")
	private GeneralDetail fineType;

	@Column(name = "lost_date")
	private Date lostDate;

	@Column(name = "damage_date")
	private Date damageDate;

	@Column(name="fine_amount")
	private BigDecimal fineAmount;

	@Column(name="balance_amount")
	private BigDecimal balanceAmount;
	
	@Column(name = "is_collected")
	private Boolean isCollected;
	
	@Column(name="discount_amount")
	private BigDecimal discountAmount;
	
	@Column(name="paid_amount")
	private BigDecimal paidAmount;

	public BookIssuedetail() {
	}

	public Long getBookIssuedetailsId() {
		return this.bookIssuedetailsId;
	}

	public void setBookIssuedetailsId(Long bookIssuedetailsId) {
		this.bookIssuedetailsId = bookIssuedetailsId;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsrenewaled() {
		return this.isrenewaled;
	}

	public void setIsrenewaled(Boolean isrenewaled) {
		this.isrenewaled = isrenewaled;
	}

	public Boolean getIsreturned() {
		return this.isreturned;
	}

	public void setIsreturned(Boolean isreturned) {
		this.isreturned = isreturned;
	}

	public Date getIssueDuedate() {
		return this.issueDuedate;
	}

	public void setIssueDuedate(Date issueDuedate) {
		this.issueDuedate = issueDuedate;
	}

	public Date getIssueFromdate() {
		return this.issueFromdate;
	}

	public void setIssueFromdate(Date issueFromdate) {
		this.issueFromdate = issueFromdate;
	}

	public Date getIssueTodate() {
		return this.issueTodate;
	}

	public void setIssueTodate(Date issueTodate) {
		this.issueTodate = issueTodate;
	}

	public Long getIssuedDays() {
		return issuedDays;
	}

	public void setIssuedDays(Long issuedDays) {
		this.issuedDays = issuedDays;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public BookDetail getBookDetail() {
		return this.bookDetail;
	}

	public void setBookDetail(BookDetail bookDetail) {
		this.bookDetail = bookDetail;
	}

	public LibraryDetail getLibraryDetail() {
		return this.libraryDetail;
	}

	public void setLibraryDetail(LibraryDetail libraryDetail) {
		this.libraryDetail = libraryDetail;
	}

	public GeneralDetail getReturnBookcondition() {
		return this.returnBookcondition;
	}

	public void setReturnBookcondition(GeneralDetail returnBookcondition) {
		this.returnBookcondition = returnBookcondition;
	}

	public List<BookIssuedetailsHistory> getBookIssuedetailsHistories() {
		return this.bookIssuedetailsHistories;
	}

	public void setBookIssuedetailsHistories(List<BookIssuedetailsHistory> bookIssuedetailsHistories) {
		this.bookIssuedetailsHistories = bookIssuedetailsHistories;
	}

	public BookIssuedetailsHistory addBookIssuedetailsHistory(BookIssuedetailsHistory bookIssuedetailsHistory) {
		getBookIssuedetailsHistories().add(bookIssuedetailsHistory);
		bookIssuedetailsHistory.setBookIssuedetail(this);

		return bookIssuedetailsHistory;
	}

	public BookIssuedetailsHistory removeBookIssuedetailsHistory(BookIssuedetailsHistory bookIssuedetailsHistory) {
		getBookIssuedetailsHistories().remove(bookIssuedetailsHistory);
		bookIssuedetailsHistory.setBookIssuedetail(null);

		return bookIssuedetailsHistory;
	}

	public GeneralDetail getBookIssuedOn() {
		return bookIssuedOn;
	}

	public void setBookIssuedOn(GeneralDetail bookIssuedOn) {
		this.bookIssuedOn = bookIssuedOn;
	}

	public LibMember getLibMember() {
		return libMember;
	}

	public void setLibMember(LibMember libMember) {
		this.libMember = libMember;
	}

	public GeneralDetail getFineType() {
		return fineType;
	}

	public void setFineType(GeneralDetail fineType) {
		this.fineType = fineType;
	}

	public Date getLostDate() {
		return lostDate;
	}

	public void setLostDate(Date lostDate) {
		this.lostDate = lostDate;
	}

	public Date getDamageDate() {
		return damageDate;
	}

	public void setDamageDate(Date damageDate) {
		this.damageDate = damageDate;
	}

	public BigDecimal getFineAmount() {
		return fineAmount;
	}

	public BigDecimal getBalanceAmount() {
		return balanceAmount;
	}

	public Boolean getIsCollected() {
		return isCollected;
	}

	public void setFineAmount(BigDecimal fineAmount) {
		this.fineAmount = fineAmount;
	}

	public void setBalanceAmount(BigDecimal balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public void setIsCollected(Boolean isCollected) {
		this.isCollected = isCollected;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public BigDecimal getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public List<LibFineCollection> getLibFineCollections() {
		return libFineCollections;
	}

	public void setLibFineCollections(List<LibFineCollection> libFineCollections) {
		this.libFineCollections = libFineCollections;
	}
	
}