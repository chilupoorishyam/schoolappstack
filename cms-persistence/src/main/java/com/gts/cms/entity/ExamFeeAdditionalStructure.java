package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_exam_fee_additional_structure database table.
 * 
 */
@Entity
@Table(name = "t_exam_fee_additional_structure")
@NamedQuery(name = "ExamFeeAdditionalStructure.findAll", query = "SELECT e FROM ExamFeeAdditionalStructure e")
public class ExamFeeAdditionalStructure implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_fee_addt_id")
	private Long feeAddtId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	private BigDecimal fee;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to ExamFeeStructure
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_fee_structure_id")
	private ExamFeeStructure examFeeStructure;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_adt_examfeetype_catdet_id")
	private GeneralDetail adtExamfeetypeCat;
	
	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_examtype_catdet_id")
	private GeneralDetail examTypeCat;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	public ExamFeeAdditionalStructure() {
	}

	public Long getFeeAddtId() {
		return this.feeAddtId;
	}

	public void setFeeAddtId(Long feeAddtId) {
		this.feeAddtId = feeAddtId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public BigDecimal getFee() {
		return this.fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public GeneralDetail getAdtExamfeetypeCat() {
		return adtExamfeetypeCat;
	}

	public void setAdtExamfeetypeCat(GeneralDetail adtExamfeetypeCat) {
		this.adtExamfeetypeCat = adtExamfeetypeCat;
	}
	
	public GeneralDetail getExamTypeCat() {
		return examTypeCat;
	}

	public void setExamTypeCatId(GeneralDetail examTypeCatId) {
		this.examTypeCat = examTypeCatId;
	}


	public ExamFeeStructure getExamFeeStructure() {
		return examFeeStructure;
	}

	public void setExamFeeStructure(ExamFeeStructure examFeeStructure) {
		this.examFeeStructure = examFeeStructure;
	}

}