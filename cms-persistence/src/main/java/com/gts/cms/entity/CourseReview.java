package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_dl_course_reviews database table.
 * 
 */
@Entity
@Table(name = "t_dl_course_reviews")
@NamedQuery(name = "CourseReview.findAll", query = "SELECT c FROM CourseReview c")
public class CourseReview implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_course_review_id")
	private Long courseReviewId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Column(name = "review_comments")
	private String reviewComments;

	@Column(name = "review_rating")
	private Integer reviewRating;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to OnlineCourses
	@ManyToOne
	@JoinColumn(name = "fk_dl_onlinecourse_id")
	private OnlineCourses onlineCourses;

	// bi-directional many-to-one association to TDlMember
	@ManyToOne
	@JoinColumn(name = "fk_reviewer_membership_id")
	private DigitalLibraryMember member;

	public CourseReview() {
	}

	public Long getCourseReviewId() {
		return this.courseReviewId;
	}

	public void setCourseReviewId(Long courseReviewId) {
		this.courseReviewId = courseReviewId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReviewComments() {
		return this.reviewComments;
	}

	public void setReviewComments(String reviewComments) {
		this.reviewComments = reviewComments;
	}

	public Integer getReviewRating() {
		return this.reviewRating;
	}

	public void setReviewRating(Integer reviewRating) {
		this.reviewRating = reviewRating;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public OnlineCourses getOnlineCourses() {
		return onlineCourses;
	}

	public void setOnlineCourses(OnlineCourses onlineCourses) {
		this.onlineCourses = onlineCourses;
	}

	public DigitalLibraryMember getMember() {
		return member;
	}

	public void setMember(DigitalLibraryMember member) {
		this.member = member;
	}

}