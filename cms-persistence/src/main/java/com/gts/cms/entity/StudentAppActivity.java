package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_std_app_activities database table.
 * 
 */
@Entity
@Table(name="t_std_app_activities")
@NamedQuery(name="StudentAppActivity.findAll", query="SELECT s FROM StudentAppActivity s")
public class StudentAppActivity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_app_activity_id")
	private Long studentAppActivityId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String level;

	private String particulars;

	@Column(name="sponsored_by")
	private String sponsoredBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;
	
	@Column(name="is_active")
	private Boolean isActive;

	//bi-directional many-to-one association to StudentActivitiesDetail
	@OneToMany(mappedBy="appActivity",fetch = FetchType.LAZY)
	private List<StudentActivitiesDetail> activitiesDetails;

	//bi-directional many-to-one association to StudentApplication
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_app_id")
	private StudentApplication studentApplication;
	
	

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public StudentAppActivity() {
	}

	public Long getStudentAppActivityId() {
		return this.studentAppActivityId;
	}

	public void setStudentAppActivityId(Long studentAppActivityId) {
		this.studentAppActivityId = studentAppActivityId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getLevel() {
		return this.level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getParticulars() {
		return this.particulars;
	}

	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}

	public String getSponsoredBy() {
		return this.sponsoredBy;
	}

	public void setSponsoredBy(String sponsoredBy) {
		this.sponsoredBy = sponsoredBy;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<StudentActivitiesDetail> getActivitiesDetails() {
		return this.activitiesDetails;
	}

	public void setActivitiesDetails(List<StudentActivitiesDetail> activitiesDetails) {
		this.activitiesDetails = activitiesDetails;
	}

	public StudentActivitiesDetail addActivitiesDetail(StudentActivitiesDetail activitiesDetail) {
		getActivitiesDetails().add(activitiesDetail);
		activitiesDetail.setAppActivity(this);

		return activitiesDetail;
	}

	public StudentActivitiesDetail removeActivitiesDetail(StudentActivitiesDetail activitiesDetail) {
		getActivitiesDetails().remove(activitiesDetail);
		activitiesDetail.setAppActivity(null);

		return activitiesDetail;
	}

	public StudentApplication getStudentApplication() {
		return this.studentApplication;
	}

	public void setStudentApplication(StudentApplication studentApplication) {
		this.studentApplication = studentApplication;
	}

}