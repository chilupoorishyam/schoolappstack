package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_exam_online_paper database table.
 */
@Data
@Entity
@Table(name = "t_exam_online_paper")
@NamedQuery(name = "ExamOnlinePaper.findAll", query = "SELECT e FROM ExamOnlinePaper e")
public class ExamOnlinePaper implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_exam_online_paper_id")
    private Long examOnlinePaperId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    // bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_school_id")
    private School school;

    // bi-directional many-to-one association to Course
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_exam_id")
    private ExamMaster examMaster;

    // bi-directional many-to-one association to Course
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_exampapertype_catdet_id")
    private GeneralDetail exampapertypeCatdetId;

    // bi-directional many-to-one association to Course
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_preparedby_emp_id")
    private EmployeeDetail preparedbyEmpId;

    @Column(name = "paper_name")
    private String paperName;

    @Column(name = "setno")
    private Integer setno;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "exam_paper_url")
    private String examPaperUrl;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "prepared_on")
    private Date preparedOn;

    @Column(name = "is_published")
    private Boolean isPublished;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "published_on")
    private Date publishedOn;

    @Column(name = "exam_paper_information")
    private String examPaperInformation;

    @Column(name = "exam_paper_notes")
    private String examPaperNotes;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    //bi-directional many-to-one association to StaffCourseyrSubject
    @OneToMany(mappedBy = "examOnlinePaper", fetch = FetchType.LAZY)
    private List<ExamTimetableDetail> examTimetableDetails;

    //bi-directional many-to-one association to StaffCourseyrSubject
    @OneToMany(mappedBy = "examOnlinePaperId", fetch = FetchType.LAZY)
    private List<ExamOnlineStdPaper> examOnlineStdPapers;

    //bi-directional many-to-one association to StaffCourseyrSubject
    @OneToMany(mappedBy = "examOnlinePaperId", fetch = FetchType.LAZY)
    private List<ExamOnlineQuestion> examOnlineQuestions;
}