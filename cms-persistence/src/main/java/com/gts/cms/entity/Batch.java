package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_batches database table.
 * 
 */
@Entity
@Table(name="t_m_batches")
@NamedQuery(name="Batch.findAll", query="SELECT b FROM Batch b")
public class Batch implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_batch_id")
	private Long batchId;

	@Column(name="batch_name")
	private String batchName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	@Column(name="from_date")
	private Date fromDate;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.DATE)
	@Column(name="to_date")
	private Date toDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to BatchwiseStudent
	@OneToMany(mappedBy="batch",fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<BatchwiseStudent> batchwiseStudents;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to Course
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_id")
	private Course course;



	//bi-directional many-to-one association to StudentApplication
	@OneToMany(mappedBy="batch",fetch = FetchType.LAZY)
	private List<StudentApplication> stdApplications;

	//bi-directional many-to-one association to StudentAcademicbatch
	@OneToMany(mappedBy="fromBatch",fetch = FetchType.LAZY)
	private List<StudentAcademicbatch> StudentAcademicbatches1;

	//bi-directional many-to-one association to StudentAcademicbatch
	@OneToMany(mappedBy="toBatch",fetch = FetchType.LAZY)
	private List<StudentAcademicbatch> studentAcademicbatches2;

	//bi-directional many-to-one association to StudentDetail
	@OneToMany(mappedBy="batch",fetch = FetchType.LAZY)
	private List<StudentDetail> studentDetails;

	public Batch() {
	}

	public Long getBatchId() {
		return this.batchId;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public String getBatchName() {
		return this.batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<BatchwiseStudent> getBatchwiseStudents() {
		return this.batchwiseStudents;
	}

	public void setBatchwiseStudents(List<BatchwiseStudent> batchwiseStudents) {
		this.batchwiseStudents = batchwiseStudents;
	}

	public BatchwiseStudent addBatchwiseStudent(BatchwiseStudent batchwiseStudent) {
		getBatchwiseStudents().add(batchwiseStudent);
		batchwiseStudent.setBatch(this);

		return batchwiseStudent;
	}

	public BatchwiseStudent removeBatchwiseStudent(BatchwiseStudent batchwiseStudent) {
		getBatchwiseStudents().remove(batchwiseStudent);
		batchwiseStudent.setBatch(null);

		return batchwiseStudent;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Course getCourse() {
		return this.course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}



	public List<StudentApplication> getStdApplications() {
		return this.stdApplications;
	}

	public void setStdApplications(List<StudentApplication> stdApplications) {
		this.stdApplications = stdApplications;
	}

	public StudentApplication addStdApplication(StudentApplication stdApplication) {
		getStdApplications().add(stdApplication);
		stdApplication.setBatch(this);

		return stdApplication;
	}

	public StudentApplication removeStdApplication(StudentApplication stdApplication) {
		getStdApplications().remove(stdApplication);
		stdApplication.setBatch(null);

		return stdApplication;
	}

	public List<StudentAcademicbatch> getStudentAcademicbatches1() {
		return this.StudentAcademicbatches1;
	}

	public void setStudentAcademicbatches1(List<StudentAcademicbatch> StudentAcademicbatches1) {
		this.StudentAcademicbatches1 = StudentAcademicbatches1;
	}

	public StudentAcademicbatch addStudentAcademicbatches1(StudentAcademicbatch StudentAcademicbatches1) {
		getStudentAcademicbatches1().add(StudentAcademicbatches1);
		StudentAcademicbatches1.setFromBatch(this);

		return StudentAcademicbatches1;
	}

	public StudentAcademicbatch removeStudentAcademicbatches1(StudentAcademicbatch StudentAcademicbatches1) {
		getStudentAcademicbatches1().remove(StudentAcademicbatches1);
		StudentAcademicbatches1.setFromBatch(null);

		return StudentAcademicbatches1;
	}

	public List<StudentAcademicbatch> getStudentAcademicbatches2() {
		return this.studentAcademicbatches2;
	}

	public void setStudentAcademicbatches2(List<StudentAcademicbatch> studentAcademicbatches2) {
		this.studentAcademicbatches2 = studentAcademicbatches2;
	}

	public StudentAcademicbatch addStudentAcademicbatches2(StudentAcademicbatch studentAcademicbatches2) {
		getStudentAcademicbatches2().add(studentAcademicbatches2);
		studentAcademicbatches2.setToBatch(this);

		return studentAcademicbatches2;
	}

	public StudentAcademicbatch removeStudentAcademicbatches2(StudentAcademicbatch studentAcademicbatches2) {
		getStudentAcademicbatches2().remove(studentAcademicbatches2);
		studentAcademicbatches2.setToBatch(null);

		return studentAcademicbatches2;
	}

	public List<StudentDetail> getStudentDetails() {
		return this.studentDetails;
	}

	public void setStudentDetails(List<StudentDetail> studentDetails) {
		this.studentDetails = studentDetails;
	}

	public StudentDetail addStudentDetail(StudentDetail studentDetail) {
		getStudentDetails().add(studentDetail);
		studentDetail.setBatch(this);

		return studentDetail;
	}

	public StudentDetail removeStudentDetail(StudentDetail studentDetail) {
		getStudentDetails().remove(studentDetail);
		studentDetail.setBatch(null);

		return studentDetail;
	}

}