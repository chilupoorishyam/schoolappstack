package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_m_general_payment_settings database table.
 */
@Data
@Entity
@Table(name = "t_m_general_payment_settings")
@NamedQuery(name = "GeneralPaymentSetting.findAll", query = "SELECT g FROM GeneralPaymentSetting g")
public class GeneralPaymentSetting implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_general_payment_setting_id")
    private Long generalPaymentSettingId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "access_code")
    private String accessCode;

    @Column(name = "merchant_id")
    private String merchantId;

    @Column(name = "working_key")
    private String workingKey;

    @Column(name = "redirect_url")
    private String redirectUrl;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    //bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_school_id")
    private School school;
}