package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_dl_course_questions database table.
 * 
 */
@Entity
@Table(name = "t_dl_course_questions")
@NamedQuery(name = "CourseQuestion.findAll", query = "SELECT c FROM CourseQuestion c")
public class CourseQuestion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_dl_course_question_id")
	private Long courseQuestionId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "fk_correct_anwer_ids")
	private String correctAnwerIds;

	@ManyToOne
	@JoinColumn(name = "fk_dl_course_lesson_id")
	private CourseLesson courseLesson;

	@ManyToOne
	@JoinColumn(name = "fk_dl_course_lesson_topic_id")
	private CourseLessonsTopic courseLessonTopic;

	@ManyToOne
	@JoinColumn(name = "fk_dl_onlinecourse_id")
	private OnlineCourses onlineCourses;

	@ManyToOne
	@JoinColumn(name = "fk_fbinputtype_catdet_id")
	private GeneralDetail fbInputTypeCat;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_allow_multiple_answers")
	private Boolean isAllowMultipleAnswers;

	private Integer marks;

	@Column(name = "no_of_correct_answers")
	private Integer noOfCorrectAnswers;

	@Column(name = "no_of_options")
	private Integer noOfOptions;

	private String question;

	@Column(name = "question_level")
	private Integer questionLevel;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to AssessmentQuestion
	@OneToMany(mappedBy = "courseQuestion")
	private List<AssessmentQuestion> assessmentQuestions;

	// bi-directional many-to-one association to CourseQuestionOption
	@OneToMany(mappedBy = "courseQuestion", cascade = CascadeType.ALL)
	private List<CourseQuestionOption> courseQuestionOptions;

	// bi-directional many-to-one association to MemberAssessmentDetail
	@OneToMany(mappedBy = "courseQuestion")
	private List<MemberAssessmentDetail> memberAssessmentDetails;

	public CourseQuestion() {
	}

	public Long getCourseQuestionId() {
		return this.courseQuestionId;
	}

	public void setCourseQuestionId(Long courseQuestionId) {
		this.courseQuestionId = courseQuestionId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getCorrectAnwerIds() {
		return this.correctAnwerIds;
	}

	public void setCorrectAnwerIds(String correctAnwerIds) {
		this.correctAnwerIds = correctAnwerIds;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsAllowMultipleAnswers() {
		return this.isAllowMultipleAnswers;
	}

	public void setIsAllowMultipleAnswers(Boolean isAllowMultipleAnswers) {
		this.isAllowMultipleAnswers = isAllowMultipleAnswers;
	}

	public Integer getMarks() {
		return this.marks;
	}

	public void setMarks(Integer marks) {
		this.marks = marks;
	}

	public Integer getNoOfCorrectAnswers() {
		return this.noOfCorrectAnswers;
	}

	public void setNoOfCorrectAnswers(Integer noOfCorrectAnswers) {
		this.noOfCorrectAnswers = noOfCorrectAnswers;
	}

	public Integer getNoOfOptions() {
		return this.noOfOptions;
	}

	public void setNoOfOptions(Integer noOfOptions) {
		this.noOfOptions = noOfOptions;
	}

	public String getQuestion() {
		return this.question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public Integer getQuestionLevel() {
		return this.questionLevel;
	}

	public void setQuestionLevel(Integer questionLevel) {
		this.questionLevel = questionLevel;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public CourseLesson getCourseLesson() {
		return courseLesson;
	}

	public void setCourseLesson(CourseLesson courseLesson) {
		this.courseLesson = courseLesson;
	}

	public CourseLessonsTopic getCourseLessonTopic() {
		return courseLessonTopic;
	}

	public void setCourseLessonTopic(CourseLessonsTopic courseLessonTopic) {
		this.courseLessonTopic = courseLessonTopic;
	}

	public OnlineCourses getOnlineCourses() {
		return onlineCourses;
	}

	public void setOnlineCourses(OnlineCourses onlineCourses) {
		this.onlineCourses = onlineCourses;
	}

	public GeneralDetail getFbInputTypeCat() {
		return fbInputTypeCat;
	}

	public void setFbInputTypeCat(GeneralDetail fbInputTypeCat) {
		this.fbInputTypeCat = fbInputTypeCat;
	}

	public List<AssessmentQuestion> getAssessmentQuestions() {
		return assessmentQuestions;
	}

	public void setAssessmentQuestions(List<AssessmentQuestion> assessmentQuestions) {
		this.assessmentQuestions = assessmentQuestions;
	}

	public List<CourseQuestionOption> getCourseQuestionOptions() {
		return courseQuestionOptions;
	}

	public void setCourseQuestionOptions(List<CourseQuestionOption> courseQuestionOptions) {
		this.courseQuestionOptions = courseQuestionOptions;
	}

	public List<MemberAssessmentDetail> getMemberAssessmentDetails() {
		return memberAssessmentDetails;
	}

	public void setMemberAssessmentDetails(List<MemberAssessmentDetail> memberAssessmentDetails) {
		this.memberAssessmentDetails = memberAssessmentDetails;
	}

}