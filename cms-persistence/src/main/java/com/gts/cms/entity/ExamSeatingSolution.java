package com.gts.cms.entity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import javax.persistence.*;
import java.util.List;

@Data
@Entity
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Table(name = "t_exam_seating_solution")
public class ExamSeatingSolution {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_exam_seating_solution_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_timetable_id")
    private ExamTimetable examTimetable;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_room_id")
    private Room room;

    @Transient
    private List<ExamRoomStudentsAllot> examSeatingList;

    public ExamSeatingSolution() {

    }
}
