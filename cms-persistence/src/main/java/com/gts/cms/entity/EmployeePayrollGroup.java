package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_hr_employee_payroll_group database table.
 * 
 */
@Entity
@Table(name = "t_hr_employee_payroll_group")
@NamedQuery(name = "EmployeePayrollGroup.findAll", query = "SELECT e FROM EmployeePayrollGroup e")
public class EmployeePayrollGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_emp_payroll_group_id")
	private Long empPayrollGroupId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	private BigDecimal ctc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "from_date")
	private Date fromDate;

	@Column(name = "gross_pay")
	private BigDecimal grossPay;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "net_amount")
	private BigDecimal netAmount;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "to_date")
	private Date toDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_emp_id")
	private EmployeeDetail employeeDetail;

	// bi-directional many-to-one association to PayrollGroup
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_payroll_group_id")
	private PayrollGroup payrollGroup;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to EmployeePayrollGroup
	@OneToMany(mappedBy = "employeePayrollGroup",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<EmployeeSalaryStructure> employeeSalaryStructure;

	public EmployeePayrollGroup() {
	}

	public Long getEmpPayrollGroupId() {
		return this.empPayrollGroupId;
	}

	public void setEmpPayrollGroupId(Long empPayrollGroupId) {
		this.empPayrollGroupId = empPayrollGroupId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public BigDecimal getCtc() {
		return this.ctc;
	}

	public void setCtc(BigDecimal ctc) {
		this.ctc = ctc;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public BigDecimal getGrossPay() {
		return this.grossPay;
	}

	public void setGrossPay(BigDecimal grossPay) {
		this.grossPay = grossPay;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public BigDecimal getNetAmount() {
		return this.netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getEmployeeDetail() {
		return this.employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public PayrollGroup getPayrollGroup() {
		return this.payrollGroup;
	}

	public void setPayrollGroup(PayrollGroup payrollGroup) {
		this.payrollGroup = payrollGroup;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<EmployeeSalaryStructure> getEmployeeSalaryStructure() {
		return employeeSalaryStructure;
	}

	public void setEmployeeSalaryStructure(List<EmployeeSalaryStructure> employeeSalaryStructure) {
		this.employeeSalaryStructure = employeeSalaryStructure;
	}

}