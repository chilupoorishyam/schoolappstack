package com.gts.cms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "t_inv_stg_item_details")
@NamedQuery(name = "InvStgItemDetails.findAll", query = "SELECT i FROM InvStgItemDetails i")
public class InvStgItemDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_stg_item_id")
	private Long itemId;

	@Column(name = "item_no")
	private String itemNo;

	@Column(name = "date_of_last_order")
	private String dateOfLastOrder;

	@Column(name = "item_name")
	private String itemName;

	@Column(name = "vendor")
	private String vendor;

	@Column(name = "stock_location")
	private String stockLocation;

	@Column(name = "description")
	private String description;

	@Column(name = "cost_per_item")
	private String costPerItem;

	@Column(name = "stock_quantity")
	private String stockQuantity;

	@Column(name = "total_value")
	private String totalValue;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public String getDateOfLastOrder() {
		return dateOfLastOrder;
	}

	public void setDateOfLastOrder(String dateOfLastOrder) {
		this.dateOfLastOrder = dateOfLastOrder;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getStockLocation() {
		return stockLocation;
	}

	public void setStockLocation(String stockLocation) {
		this.stockLocation = stockLocation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStockQuantity() {
		return stockQuantity;
	}

	public void setStockQuantity(String stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public String getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(String totalValue) {
		this.totalValue = totalValue;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getCostPerItem() {
		return costPerItem;
	}

	public void setCostPerItem(String costPerItem) {
		this.costPerItem = costPerItem;
	}
}
