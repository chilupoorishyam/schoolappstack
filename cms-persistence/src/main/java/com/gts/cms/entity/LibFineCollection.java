package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_lib_library_details database table.
 * 
 */
@Entity
@Table(name = "t_lib_fine_collection")
@NamedQuery(name = "LibFineCollection.findAll", query = "SELECT l FROM LibFineCollection l")
public class LibFineCollection implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_lib_fine_collection_id")
	private Long libFineCollectionId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_lib_book_issuedetails_id")
	private BookIssuedetail libBookIssueDetails;

	@Column(name = "receipt_no")
	private String receiptNo;

	@Column(name = "fine_collected_amount")
	private BigDecimal fineCollectedAmount;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_finecollectedby_emp_id")
	private EmployeeDetail fineCollectedbyEmp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fine_collected_date")
	private Date fineCollectedDate;

	@Column(name = "fine_remarks")
	private String fineRemarks;

	@Column(name = "is_cancelled")
	private Boolean isCancelled;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cancelled_date")
	private Date cancelledDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_finecancelledby_emp_id")
	private EmployeeDetail finecancelledbyEmp;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	public LibFineCollection() {
	}

	public Long getLibFineCollectionId() {
		return libFineCollectionId;
	}

	public BookIssuedetail getLibBookIssueDetails() {
		return libBookIssueDetails;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public BigDecimal getFineCollectedAmount() {
		return fineCollectedAmount;
	}

	public EmployeeDetail getFineCollectedbyEmp() {
		return fineCollectedbyEmp;
	}

	public Date getFineCollectedDate() {
		return fineCollectedDate;
	}

	public String getFineRemarks() {
		return fineRemarks;
	}

	public Boolean getIsCancelled() {
		return isCancelled;
	}

	public Date getCancelledDate() {
		return cancelledDate;
	}

	public EmployeeDetail getFinecancelledbyEmp() {
		return finecancelledbyEmp;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public String getReason() {
		return reason;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setLibFineCollectionId(Long libFineCollectionId) {
		this.libFineCollectionId = libFineCollectionId;
	}

	public void setLibBookIssueDetails(BookIssuedetail libBookIssueDetails) {
		this.libBookIssueDetails = libBookIssueDetails;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public void setFineCollectedAmount(BigDecimal fineCollectedAmount) {
		this.fineCollectedAmount = fineCollectedAmount;
	}

	public void setFineCollectedbyEmp(EmployeeDetail fineCollectedbyEmp) {
		this.fineCollectedbyEmp = fineCollectedbyEmp;
	}

	public void setFineCollectedDate(Date fineCollectedDate) {
		this.fineCollectedDate = fineCollectedDate;
	}

	public void setFineRemarks(String fineRemarks) {
		this.fineRemarks = fineRemarks;
	}

	public void setIsCancelled(Boolean isCancelled) {
		this.isCancelled = isCancelled;
	}

	public void setCancelledDate(Date cancelledDate) {
		this.cancelledDate = cancelledDate;
	}

	public void setFinecancelledbyEmp(EmployeeDetail finecancelledbyEmp) {
		this.finecancelledbyEmp = finecancelledbyEmp;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

}