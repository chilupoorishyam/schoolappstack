package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_fee_receipts database table.
 * 
 */
@Entity
@Table(name = "t_fee_receipts")
@NamedQuery(name = "FeeReceipt.findAll", query = "SELECT f FROM FeeReceipt f")
public class FeeReceipt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_fee_receipts_id")
	private Long feeReceiptsId;

	@Column(name = "cheque_no")
	private String chequeNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	private String ddno;

	@Column(name = "fk_hstl_fee_payment_id")
	private Long hstlFeePaymentId;

	@Column(name = "fk_tm_fee_payment_id")
	private Long tmFeePaymentId;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_reverted")
	private Boolean isReverted;

	@Column(name = "other_payment_number")
	private String otherPaymentNumber;

	@Column(name = "payer_name")
	private String payerName;

	@Column(name = "payment_for")
	private String paymentFor;

	@Column(name = "payment_receipts_no")
	private String paymentReceiptsNo;

	private String reason;

	@Column(name = "receipt_amount")
	private BigDecimal receiptAmount;

	@Column(name = "reference_number")
	private String referenceNumber;

	@Column(name = "revert_reason")
	private String revertReason;

	@Column(name = "transaction_no")
	private String transactionNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "receipt_date")
	private Date receiptDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_emp_id")
	private EmployeeDetail employeeDetail;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_revert_by_emp_id")
	private EmployeeDetail revertbByEmployeeDetail;

	// bi-directional many-to-one association to FeeInstantCategory
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_fee_instant_category_id")
	private FeeInstantCategory feeInstantCategory;

	// bi-directional many-to-one association to FeeInstantPayment
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_instant_payment_id")
	private FeeInstantPayment feeInstantPayment;

	// bi-directional many-to-one association to FeeParticularwisePayment
	/*
	 * @ManyToOne
	 * 
	 * @JoinColumn(name="fk_fee_particularwise_payment_id") private
	 * FeeParticularwisePayment feeParticularwisePayment;
	 */

	@OneToMany(mappedBy = "feeReceipt", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	private List<FeeParticularwisePayment> feeParticularwisePayments;
	
	
	@OneToMany(mappedBy = "feeReceipt", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<FeeStudentRefund> feeStudentRefund;

	// bi-directional many-to-one association to FeeStructure
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_fee_structure_id")
	private FeeStructure feeStructure;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_payer_type_catdet_id")
	private GeneralDetail payerType;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_payment_type_catdet_id")
	private GeneralDetail paymentType;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_payment_mode_catdet_id")
	private GeneralDetail paymentMode;

	// bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_financial_year_id")
	private FinancialYear financialYear;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_academic_year_id")
	private AcademicYear academicYear;

	// bi-directional many-to-one association to FeePayment
	/*
	 * @OneToMany(mappedBy="feeReceipt",fetch = FetchType.LAZY) private
	 * List<FeePayment> feePayments;
	 */

	public FeeReceipt() {
	}

	public Long getFeeReceiptsId() {
		return this.feeReceiptsId;
	}

	public void setFeeReceiptsId(Long feeReceiptsId) {
		this.feeReceiptsId = feeReceiptsId;
	}

	public String getChequeNo() {
		return this.chequeNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDdno() {
		return this.ddno;
	}

	public void setDdno(String ddno) {
		this.ddno = ddno;
	}

	public Long getHstlFeePaymentId() {
		return this.hstlFeePaymentId;
	}

	public void setHstlFeePaymentId(Long hstlFeePaymentId) {
		this.hstlFeePaymentId = hstlFeePaymentId;
	}

	public Long getTmFeePaymentId() {
		return this.tmFeePaymentId;
	}

	public void setTmFeePaymentId(Long tmFeePaymentId) {
		this.tmFeePaymentId = tmFeePaymentId;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsReverted() {
		return this.isReverted;
	}

	public void setIsReverted(Boolean isReverted) {
		this.isReverted = isReverted;
	}

	public String getOtherPaymentNumber() {
		return this.otherPaymentNumber;
	}

	public void setOtherPaymentNumber(String otherPaymentNumber) {
		this.otherPaymentNumber = otherPaymentNumber;
	}

	public String getPayerName() {
		return this.payerName;
	}

	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}

	public String getPaymentFor() {
		return this.paymentFor;
	}

	public void setPaymentFor(String paymentFor) {
		this.paymentFor = paymentFor;
	}

	public String getPaymentReceiptsNo() {
		return this.paymentReceiptsNo;
	}

	public void setPaymentReceiptsNo(String paymentReceiptsNo) {
		this.paymentReceiptsNo = paymentReceiptsNo;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getReceiptAmount() {
		return this.receiptAmount;
	}

	public void setReceiptAmount(BigDecimal receiptAmount) {
		this.receiptAmount = receiptAmount;
	}

	public String getReferenceNumber() {
		return this.referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getRevertReason() {
		return this.revertReason;
	}

	public void setRevertReason(String revertReason) {
		this.revertReason = revertReason;
	}

	public String getTransactionNo() {
		return this.transactionNo;
	}

	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getEmployeeDetail() {
		return this.employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public EmployeeDetail getRevertbByEmployeeDetail() {
		return this.revertbByEmployeeDetail;
	}

	public void setRevertbByEmployeeDetail(EmployeeDetail revertbByEmployeeDetail) {
		this.revertbByEmployeeDetail = revertbByEmployeeDetail;
	}

	public FeeInstantCategory getFeeInstantCategory() {
		return this.feeInstantCategory;
	}

	public void setFeeInstantCategory(FeeInstantCategory feeInstantCategory) {
		this.feeInstantCategory = feeInstantCategory;
	}

	public FeeInstantPayment getFeeInstantPayment() {
		return this.feeInstantPayment;
	}

	public void setFeeInstantPayment(FeeInstantPayment feeInstantPayment) {
		this.feeInstantPayment = feeInstantPayment;
	}

	public List<FeeParticularwisePayment> getFeeParticularwisePayments() {
		return feeParticularwisePayments;
	}

	public void setFeeParticularwisePayments(List<FeeParticularwisePayment> feeParticularwisePayments) {
		this.feeParticularwisePayments = feeParticularwisePayments;
	}

	public FeeStructure getFeeStructure() {
		return this.feeStructure;
	}

	public void setFeeStructure(FeeStructure feeStructure) {
		this.feeStructure = feeStructure;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getPayerType() {
		return this.payerType;
	}

	public void setPayerType(GeneralDetail payerType) {
		this.payerType = payerType;
	}

	public GeneralDetail getPaymentType() {
		return this.paymentType;
	}

	public void setPaymentType(GeneralDetail paymentType) {
		this.paymentType = paymentType;
	}

	public GeneralDetail getPaymentMode() {
		return this.paymentMode;
	}

	public void setPaymentMode(GeneralDetail paymentMode) {
		this.paymentMode = paymentMode;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public FinancialYear getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public Date getReceiptDt() {
		return receiptDt;
	}

	public void setReceiptDt(Date receiptDt) {
		this.receiptDt = receiptDt;
	}

	public List<FeeStudentRefund> getFeeStudentRefund() {
		return feeStudentRefund;
	}

	public void setFeeStudentRefund(List<FeeStudentRefund> feeStudentRefund) {
		this.feeStudentRefund = feeStudentRefund;
	}

}