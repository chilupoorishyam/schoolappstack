package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_inv_purchasereturns database table.
 * 
 */
@Entity
@Table(name = "t_inv_purchasereturns")
@NamedQuery(name = "InvPurchasereturn.findAll", query = "SELECT i FROM InvPurchasereturn i")
public class InvPurchasereturn implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_purchasereturn_id")
	private Long purchasereturnId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;
	
	//bi-directional many-to-one association to Department
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_dept_id")
	private Department department;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="fk_inv_transtype_catdet_id")
	private GeneralDetail invTranstypeCatdet;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "pr_ref_file_path1")
	private String prRefFilePath1;

	@Column(name = "pr_ref_file_path2")
	private String prRefFilePath2;

	@Temporal(TemporalType.DATE)
	@Column(name = "purchase_return_date")
	private Date purchaseReturnDate;

	@Column(name = "purchase_return_no")
	private String purchaseReturnNo;

	private String reason;

	@Column(name = "return_actual_amount")
	private BigDecimal returnActualAmount;

	@Column(name = "return_amount")
	private BigDecimal returnAmount;

	@Temporal(TemporalType.DATE)
	@Column(name = "return_date")
	private Date returnDate;

	@Column(name = "return_discount")
	private BigDecimal returnDiscount;

	@Column(name = "return_tax")
	private BigDecimal returnTax;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to InvPurchasereturnItem
	@OneToMany(mappedBy = "invPurchasereturn", cascade = CascadeType.ALL)
	private List<InvPurchasereturnItem> invPurchasereturnItems;

	// bi-directional many-to-one association to InvStoresmaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_store_id")
	private InvStoresmaster invStoresmaster;

	// bi-directional many-to-one association to InvSuppliermaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_supplier_id")
	private InvSuppliermaster invSuppliermaster;

	// bi-directional many-to-one association to InvSrv
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_srv_id")
	private InvSrv invSrv;

	public InvPurchasereturn() {
	}

	public Long getPurchasereturnId() {
		return this.purchasereturnId;
	}

	public void setPurchasereturnId(Long purchasereturnId) {
		this.purchasereturnId = purchasereturnId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getPrRefFilePath1() {
		return this.prRefFilePath1;
	}

	public void setPrRefFilePath1(String prRefFilePath1) {
		this.prRefFilePath1 = prRefFilePath1;
	}

	public String getPrRefFilePath2() {
		return this.prRefFilePath2;
	}

	public void setPrRefFilePath2(String prRefFilePath2) {
		this.prRefFilePath2 = prRefFilePath2;
	}

	public Date getPurchaseReturnDate() {
		return this.purchaseReturnDate;
	}

	public void setPurchaseReturnDate(Date purchaseReturnDate) {
		this.purchaseReturnDate = purchaseReturnDate;
	}

	public String getPurchaseReturnNo() {
		return this.purchaseReturnNo;
	}

	public void setPurchaseReturnNo(String purchaseReturnNo) {
		this.purchaseReturnNo = purchaseReturnNo;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getReturnActualAmount() {
		return this.returnActualAmount;
	}

	public void setReturnActualAmount(BigDecimal returnActualAmount) {
		this.returnActualAmount = returnActualAmount;
	}

	public BigDecimal getReturnAmount() {
		return this.returnAmount;
	}

	public void setReturnAmount(BigDecimal returnAmount) {
		this.returnAmount = returnAmount;
	}

	public Date getReturnDate() {
		return this.returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public BigDecimal getReturnDiscount() {
		return this.returnDiscount;
	}

	public void setReturnDiscount(BigDecimal returnDiscount) {
		this.returnDiscount = returnDiscount;
	}

	public BigDecimal getReturnTax() {
		return this.returnTax;
	}

	public void setReturnTax(BigDecimal returnTax) {
		this.returnTax = returnTax;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public GeneralDetail getInvTranstypeCatdet() {
		return invTranstypeCatdet;
	}

	public void setInvTranstypeCatdet(GeneralDetail invTranstypeCatdet) {
		this.invTranstypeCatdet = invTranstypeCatdet;
	}

	public InvStoresmaster getInvStoresmaster() {
		return invStoresmaster;
	}

	public void setInvStoresmaster(InvStoresmaster invStoresmaster) {
		this.invStoresmaster = invStoresmaster;
	}

	public InvSuppliermaster getInvSuppliermaster() {
		return invSuppliermaster;
	}

	public void setInvSuppliermaster(InvSuppliermaster invSuppliermaster) {
		this.invSuppliermaster = invSuppliermaster;
	}

	public InvSrv getInvSrv() {
		return invSrv;
	}

	public void setInvSrv(InvSrv invSrv) {
		this.invSrv = invSrv;
	}

	public List<InvPurchasereturnItem> getInvPurchasereturnItems() {
		return invPurchasereturnItems;
	}

	public void setInvPurchasereturnItems(List<InvPurchasereturnItem> invPurchasereturnItems) {
		this.invPurchasereturnItems = invPurchasereturnItems;
	}

}