package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_ams_network_member database table.
 * 
 */
@Entity
@Table(name="t_ams_network_member")
@NamedQuery(name="NetworkMember.findAll", query="SELECT n FROM NetworkMember n")
public class NetworkMember implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="pk_ams_network_member_id")
	private Long networkMemberId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@ManyToOne
	@JoinColumn(name="fk_ams_profile_id")
	private AmsProfileDetail profileDetail;

	@ManyToOne
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	@Column(name="is_active")
	private Boolean isActive;

	@Temporal(TemporalType.DATE)
	@Column(name="member_join_date")
	private Date memberJoinDate;

	@Temporal(TemporalType.DATE)
	@Column(name="member_left_date")
	private Date memberLeftDate;
	
	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	public NetworkMember() {
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Date getMemberJoinDate() {
		return this.memberJoinDate;
	}

	public void setMemberJoinDate(Date memberJoinDate) {
		this.memberJoinDate = memberJoinDate;
	}

	public Date getMemberLeftDate() {
		return this.memberLeftDate;
	}

	public void setMemberLeftDate(Date memberLeftDate) {
		this.memberLeftDate = memberLeftDate;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public AmsProfileDetail getProfileDetail() {
		return profileDetail;
	}

	public void setProfileDetail(AmsProfileDetail profileDetail) {
		this.profileDetail = profileDetail;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Long getNetworkMemberId() {
		return networkMemberId;
	}

	public void setNetworkMemberId(Long networkMemberId) {
		this.networkMemberId = networkMemberId;
	}
}