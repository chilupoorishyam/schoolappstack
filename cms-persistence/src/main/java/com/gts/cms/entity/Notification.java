package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_notifications database table.
 * 
 */
@Entity
@Table(name="t_m_notifications")
@NamedQuery(name="Notification.findAll", query="SELECT n FROM Notification n")
public class Notification implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_notification_id")
	private Long notificationId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;
	
	@Column(name="notification_doc_path")
	private String notificationDocPath;
	@Lob
	private String description;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_announcement")
	private Boolean isAnnouncement;

	@Column(name="is_published")
	private Boolean isPublished;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="notification_enddate")
	private Date notificationEnddate;

	@Column(name="notification_title")
	private String notificationTitle;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="publish_date")
	private Date publishDate;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to NotificationAudience
	@OneToMany(mappedBy="notification",fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	private List<NotificationAudience> notificationAudiences;

	//bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	public Notification() {
	}

	public Long getNotificationId() {
		return this.notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsAnnouncement() {
		return this.isAnnouncement;
	}

	public void setIsAnnouncement(Boolean isAnnouncement) {
		this.isAnnouncement = isAnnouncement;
	}

	public Boolean getIsPublished() {
		return this.isPublished;
	}

	public void setIsPublished(Boolean isPublished) {
		this.isPublished = isPublished;
	}

	public Date getNotificationEnddate() {
		return this.notificationEnddate;
	}

	public void setNotificationEnddate(Date notificationEnddate) {
		this.notificationEnddate = notificationEnddate;
	}

	public String getNotificationTitle() {
		return this.notificationTitle;
	}

	public void setNotificationTitle(String notificationTitle) {
		this.notificationTitle = notificationTitle;
	}

	public Date getPublishDate() {
		return this.publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<NotificationAudience> getNotificationAudiences() {
		return this.notificationAudiences;
	}

	public void setNotificationAudiences(List<NotificationAudience> notificationAudiences) {
		this.notificationAudiences = notificationAudiences;
	}

	public NotificationAudience addNotificationAudience(NotificationAudience notificationAudience) {
		getNotificationAudiences().add(notificationAudience);
		notificationAudience.setNotification(this);

		return notificationAudience;
	}

	public NotificationAudience removeNotificationAudience(NotificationAudience notificationAudience) {
		getNotificationAudiences().remove(notificationAudience);
		notificationAudience.setNotification(null);

		return notificationAudience;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public String getNotificationDocPath() {
		return notificationDocPath;
	}

	public void setNotificationDocPath(String notificationDocPath) {
		this.notificationDocPath = notificationDocPath;
	}
}