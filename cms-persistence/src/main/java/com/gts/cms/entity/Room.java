package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_rooms database table.
 * 
 */
@Entity
@Table(name="t_m_rooms")
@NamedQuery(name="Room.findAll", query="SELECT r FROM Room r")
public class Room implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_room_id")
	private Long roomId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Column(name="room_code")
	private String roomCode;

	@Column(name="room_name")
	private String roomName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;
	
	@Column(name="occupancy")
	private Integer occupancy;
	
	@Column(name="exam_rows")
	private Integer examRows;
	
	@Column(name="exam_columns")
	private Integer examColumns;
	
	@Column(name="exam_blocked_seats")
	private Integer examBlockedSeats;
	
	@Column(name="exam_blocked_seats_list")
	private String examBlockedSeatList;
	
	//bi-directional many-to-one association to LibraryDetail
	@OneToMany(mappedBy="room",fetch = FetchType.LAZY)
	private List<LibraryDetail> libraryDetails;

	//bi-directional many-to-one association to Block
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_block_id")
	private Block block;

	//bi-directional many-to-one association to Floor
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_floor_id")
	private Floor floor;

	//bi-directional many-to-one association to RoomType
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_room_type_id")
	private RoomType roomType;

	//bi-directional many-to-one association to ActualClassesSchedule
	@OneToMany(mappedBy="room",fetch = FetchType.LAZY)
	private List<ActualClassesSchedule> actualClassesSchedules;

	//bi-directional many-to-one association to StaffProxy
	@OneToMany(mappedBy="room",fetch = FetchType.LAZY)
	private List<StaffProxy> staffProxies;

	//bi-directional many-to-one association to SubjectResource
	@OneToMany(mappedBy="room",fetch = FetchType.LAZY)
	private List<SubjectResource> subjectResources;

	public Room() {
	}

	public Long getRoomId() {
		return this.roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRoomCode() {
		return this.roomCode;
	}

	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}

	public String getRoomName() {
		return this.roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<LibraryDetail> getLibraryDetails() {
		return this.libraryDetails;
	}

	public void setLibraryDetails(List<LibraryDetail> libraryDetails) {
		this.libraryDetails = libraryDetails;
	}

	public LibraryDetail addLibraryDetail(LibraryDetail libraryDetail) {
		getLibraryDetails().add(libraryDetail);
		libraryDetail.setRoom(this);

		return libraryDetail;
	}

	public LibraryDetail removeLibraryDetail(LibraryDetail libraryDetail) {
		getLibraryDetails().remove(libraryDetail);
		libraryDetail.setRoom(null);

		return libraryDetail;
	}

	public Block getBlock() {
		return this.block;
	}

	public void setBlock(Block block) {
		this.block = block;
	}

	public Floor getFloor() {
		return this.floor;
	}

	public void setFloor(Floor floor) {
		this.floor = floor;
	}

	public RoomType getRoomType() {
		return this.roomType;
	}

	public void setRoomType(RoomType roomType) {
		this.roomType = roomType;
	}

	public List<ActualClassesSchedule> getActualClassesSchedules() {
		return this.actualClassesSchedules;
	}

	public void setActualClassesSchedules(List<ActualClassesSchedule> actualClassesSchedules) {
		this.actualClassesSchedules = actualClassesSchedules;
	}

	public ActualClassesSchedule addActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		getActualClassesSchedules().add(actualClassesSchedule);
		actualClassesSchedule.setRoom(this);

		return actualClassesSchedule;
	}

	public ActualClassesSchedule removeActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		getActualClassesSchedules().remove(actualClassesSchedule);
		actualClassesSchedule.setRoom(null);

		return actualClassesSchedule;
	}

	public List<StaffProxy> getStaffProxies() {
		return this.staffProxies;
	}

	public void setStaffProxies(List<StaffProxy> staffProxies) {
		this.staffProxies = staffProxies;
	}

	public StaffProxy addStaffProxy(StaffProxy staffProxy) {
		getStaffProxies().add(staffProxy);
		staffProxy.setRoom(this);

		return staffProxy;
	}

	public StaffProxy removeStaffProxy(StaffProxy staffProxy) {
		getStaffProxies().remove(staffProxy);
		staffProxy.setRoom(null);

		return staffProxy;
	}

	public List<SubjectResource> getSubjectResources() {
		return this.subjectResources;
	}

	public void setSubjectResources(List<SubjectResource> subjectResources) {
		this.subjectResources = subjectResources;
	}

	public SubjectResource addSubjectResource(SubjectResource subjectResource) {
		getSubjectResources().add(subjectResource);
		subjectResource.setRoom(this);

		return subjectResource;
	}

	public SubjectResource removeSubjectResource(SubjectResource subjectResource) {
		getSubjectResources().remove(subjectResource);
		subjectResource.setRoom(null);

		return subjectResource;
	}

	public Integer getOccupancy() {
		return occupancy;
	}

	public void setOccupancy(Integer occupancy) {
		this.occupancy = occupancy;
	}

	public Integer getExamRows() {
		return examRows;
	}

	public void setExamRows(Integer examRows) {
		this.examRows = examRows;
	}

	public Integer getExamColumns() {
		return examColumns;
	}

	public void setExamColumns(Integer examColumns) {
		this.examColumns = examColumns;
	}

	public Integer getExamBlockedSeats() {
		return examBlockedSeats;
	}

	public void setExamBlockedSeats(Integer examBlockedSeats) {
		this.examBlockedSeats = examBlockedSeats;
	}

	public String getExamBlockedSeatList() {
		return examBlockedSeatList;
	}

	public void setExamBlockedSeatList(String examBlockedSeatList) {
		this.examBlockedSeatList = examBlockedSeatList;
	}
}