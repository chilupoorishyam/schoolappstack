package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_exam_student_registration database table.
 * 
 */
@Entity
@Table(name = "t_exam_student_registration")
@NamedQuery(name = "ExamStudentRegistration.findAll", query = "SELECT e FROM ExamStudentRegistration e")
public class ExamStudentRegistration implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_std_reg_id")
	private Long examStdRegId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_year_id")
	private CourseYear courseYear;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_fee_structure_id")
	private ExamFeeStructure examFeeStructure;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_id")
	private ExamMaster examMaster;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_examtype_catdet_id")
	private GeneralDetail examTypeCat;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;
	

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;
	
	// bi-directional many-to-one association to ExamStudentRegistrationSubject
	@OneToMany(mappedBy = "examStudentRegistration",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	private List<ExamStudentRegistrationSubject> examStdRegSubs;

	public ExamStudentRegistration() {
	}

	public Long getExamStdRegId() {
		return examStdRegId;
	}

	public void setExamStdRegId(Long examStdRegId) {
		this.examStdRegId = examStdRegId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public CourseYear getCourseYear() {
		return courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public ExamFeeStructure getExamFeeStructure() {
		return examFeeStructure;
	}

	public void setExamFeeStructure(ExamFeeStructure examFeeStructure) {
		this.examFeeStructure = examFeeStructure;
	}

	public ExamMaster getExamMaster() {
		return examMaster;
	}

	public void setExamMaster(ExamMaster examMaster) {
		this.examMaster = examMaster;
	}

	public GeneralDetail getExamTypeCat() {
		return examTypeCat;
	}

	public void setExamTypeCat(GeneralDetail examTypeCat) {
		this.examTypeCat = examTypeCat;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<ExamStudentRegistrationSubject> getExamStdRegSubs() {
		return examStdRegSubs;
	}

	public void setExamStdRegSubs(List<ExamStudentRegistrationSubject> examStdRegSubs) {
		this.examStdRegSubs = examStdRegSubs;
	}


	
}