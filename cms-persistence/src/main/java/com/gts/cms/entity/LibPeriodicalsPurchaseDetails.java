package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_lib_book_purchase_details database table.
 * 
 */
@Entity
@Table(name = "t_lib_periodicals_purchase_details")
@NamedQuery(name = "LibPeriodicalsPurchaseDetails.findAll", query = "SELECT b FROM LibPeriodicalsPurchaseDetails b")
public class LibPeriodicalsPurchaseDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_book_purchase_detail_id")
	private Long libPeriodicalsPurchaseDetailsId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_lib_periodical_id")
	private Periodical periodical;

	@Column(name = "purchase_source")
	private String purchaseSource;

	@Column(name = "periodical_amount")
	private BigDecimal periodicalAmount;

	@Column(name = "bill_amount")
	private BigDecimal billAmount;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_currency_catdet_id")
	private GeneralDetail currencyCatdet;

	@Column(name = "no_of_periodicals")
	private Integer noOfPeriodicals;

	@Column(name = "bill_no")
	private String billNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "bill_date")
	private Date billDate;

	@Column(name = "bill_receipt_path")
	private String billReceiptPath;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	public LibPeriodicalsPurchaseDetails() {
	}

	public Long getLibPeriodicalsPurchaseDetailsId() {
		return libPeriodicalsPurchaseDetailsId;
	}

	public Periodical getPeriodical() {
		return periodical;
	}

	public String getPurchaseSource() {
		return purchaseSource;
	}

	public BigDecimal getPeriodicalAmount() {
		return periodicalAmount;
	}

	public BigDecimal getBillAmount() {
		return billAmount;
	}

	public GeneralDetail getCurrencyCatdet() {
		return currencyCatdet;
	}

	public Integer getNoOfPeriodicals() {
		return noOfPeriodicals;
	}

	public String getBillNo() {
		return billNo;
	}

	public Date getBillDate() {
		return billDate;
	}

	public String getBillReceiptPath() {
		return billReceiptPath;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public String getReason() {
		return reason;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setLibPeriodicalsPurchaseDetailsId(Long libPeriodicalsPurchaseDetailsId) {
		this.libPeriodicalsPurchaseDetailsId = libPeriodicalsPurchaseDetailsId;
	}

	public void setPeriodical(Periodical periodical) {
		this.periodical = periodical;
	}

	public void setPurchaseSource(String purchaseSource) {
		this.purchaseSource = purchaseSource;
	}

	public void setPeriodicalAmount(BigDecimal periodicalAmount) {
		this.periodicalAmount = periodicalAmount;
	}

	public void setBillAmount(BigDecimal billAmount) {
		this.billAmount = billAmount;
	}

	public void setCurrencyCatdet(GeneralDetail currencyCatdet) {
		this.currencyCatdet = currencyCatdet;
	}

	public void setNoOfPeriodicals(Integer noOfPeriodicals) {
		this.noOfPeriodicals = noOfPeriodicals;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}

	public void setBillReceiptPath(String billReceiptPath) {
		this.billReceiptPath = billReceiptPath;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

}