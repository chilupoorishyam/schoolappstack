package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_cm_subject_unit_topics database table.
 * 
 */
@Entity
@Table(name="t_cm_subject_unit_topics")
@NamedQuery(name="SubjectUnitTopic.findAll", query="SELECT s FROM SubjectUnitTopic s")
public class SubjectUnitTopic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_sub_unit_topic_id")
	private Long subjectUnitTopicId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="from_page")
	private Integer fromPage;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="no_of_periods")
	private Integer noOfPeriods;

	private String reason;

	@Column(name="sort_order")
	private Integer sortOrder;

	@Column(name="to_page")
	private Integer toPage;

	@Column(name="topic_description")
	private String topicDescription;

	@Column(name="topic_name")
	private String topicName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to SubjectBook
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_sub_book_id")
	private SubjectBook subjectBook;

	//bi-directional many-to-one association to SubjectUnit
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_sub_units_id")
	private SubjectUnit subjectUnit;

	//bi-directional many-to-one association to Subjectregulation
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subreg_id")
	private Subjectregulation subjectregulation;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to Lessonstatus
	@OneToMany(mappedBy="subjectUnitTopic",fetch = FetchType.LAZY)
	private List<Lessonstatus> lessonstatuses;

	public SubjectUnitTopic() {
	}

	public Long getSubjectUnitTopicId() {
		return this.subjectUnitTopicId;
	}

	public void setSubjectUnitTopicId(Long subjectUnitTopicId) {
		this.subjectUnitTopicId = subjectUnitTopicId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Integer getFromPage() {
		return this.fromPage;
	}

	public void setFromPage(Integer fromPage) {
		this.fromPage = fromPage;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getNoOfPeriods() {
		return this.noOfPeriods;
	}

	public void setNoOfPeriods(Integer noOfPeriods) {
		this.noOfPeriods = noOfPeriods;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Integer getToPage() {
		return this.toPage;
	}

	public void setToPage(Integer toPage) {
		this.toPage = toPage;
	}

	public String getTopicDescription() {
		return this.topicDescription;
	}

	public void setTopicDescription(String topicDescription) {
		this.topicDescription = topicDescription;
	}

	public String getTopicName() {
		return this.topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public SubjectBook getSubjectBook() {
		return this.subjectBook;
	}

	public void setSubjectBook(SubjectBook subjectBook) {
		this.subjectBook = subjectBook;
	}

	public SubjectUnit getSubjectUnit() {
		return this.subjectUnit;
	}

	public void setSubjectUnit(SubjectUnit subjectUnit) {
		this.subjectUnit = subjectUnit;
	}

	public Subjectregulation getSubjectregulation() {
		return this.subjectregulation;
	}

	public void setSubjectregulation(Subjectregulation subjectregulation) {
		this.subjectregulation = subjectregulation;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<Lessonstatus> getLessonstatuses() {
		return this.lessonstatuses;
	}

	public void setLessonstatuses(List<Lessonstatus> lessonstatuses) {
		this.lessonstatuses = lessonstatuses;
	}

	public Lessonstatus addLessonstatus(Lessonstatus lessonstatus) {
		getLessonstatuses().add(lessonstatus);
		lessonstatus.setSubjectUnitTopic(this);

		return lessonstatus;
	}

	public Lessonstatus removeLessonstatus(Lessonstatus lessonstatus) {
		getLessonstatuses().remove(lessonstatus);
		lessonstatus.setSubjectUnitTopic(null);

		return lessonstatus;
	}

}