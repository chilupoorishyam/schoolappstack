package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the t_inv_stockledger database table.
 * 
 */
@Entity
@Table(name = "t_inv_stockledger")
@NamedQuery(name = "InvStockledger.findAll", query = "SELECT i FROM InvStockledger i")
public class InvStockledger implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_stockledger_id")
	private Long stockledgerId;

	@Column(name = "batch_no")
	private String batchNo;

	private BigDecimal costprice;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_dept_id")
	private Department department;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_inv_transtype_catdet_id")
	private GeneralDetail invTranstypeCatdet;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_inout")
	private Boolean isInout;

	@Column(name = "item_qty")
	private BigDecimal itemQty;

	private String reason;

	private BigDecimal totalprice;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "transaction_date")
	private Date transactionDate;

	private String transactionno;

	private String transactiontype;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to InvStoresmaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_store_id")
	private InvStoresmaster invStoresmaster;

	// bi-directional many-to-one association to InvItemmaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_item_id")
	private InvItemmaster invItemmaster;

	public InvStockledger() {
	}

	public Long getStockledgerId() {
		return this.stockledgerId;
	}

	public void setStockledgerId(Long stockledgerId) {
		this.stockledgerId = stockledgerId;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public BigDecimal getCostprice() {
		return this.costprice;
	}

	public void setCostprice(BigDecimal costprice) {
		this.costprice = costprice;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsInout() {
		return this.isInout;
	}

	public void setIsInout(Boolean isInout) {
		this.isInout = isInout;
	}

	public BigDecimal getItemQty() {
		return this.itemQty;
	}

	public void setItemQty(BigDecimal itemQty) {
		this.itemQty = itemQty;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getTotalprice() {
		return this.totalprice;
	}

	public void setTotalprice(BigDecimal totalprice) {
		this.totalprice = totalprice;
	}

	public Date getTransactionDate() {
		return this.transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionno() {
		return this.transactionno;
	}

	public void setTransactionno(String transactionno) {
		this.transactionno = transactionno;
	}

	public String getTransactiontype() {
		return this.transactiontype;
	}

	public void setTransactiontype(String transactiontype) {
		this.transactiontype = transactiontype;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public GeneralDetail getInvTranstypeCatdet() {
		return invTranstypeCatdet;
	}

	public void setInvTranstypeCatdet(GeneralDetail invTranstypeCatdet) {
		this.invTranstypeCatdet = invTranstypeCatdet;
	}

	public InvStoresmaster getInvStoresmaster() {
		return invStoresmaster;
	}

	public void setInvStoresmaster(InvStoresmaster invStoresmaster) {
		this.invStoresmaster = invStoresmaster;
	}

	public InvItemmaster getInvItemmaster() {
		return invItemmaster;
	}

	public void setInvItemmaster(InvItemmaster invItemmaster) {
		this.invItemmaster = invItemmaster;
	}

}