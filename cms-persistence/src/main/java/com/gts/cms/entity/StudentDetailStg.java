package com.gts.cms.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the t_std_student_details_stg database table.
 */
@Setter
@Getter
@Entity
@Table(name = "t_std_student_details_stg")
public class StudentDetailStg implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_std_id")
    private Long studentId;

    @Column(name = "organization")
    private String organization;

    @Column(name = "school")
    private String school;

    @Column(name = "course")
    private String course;

    @Column(name = "s_group")
    private String group;

    @Column(name = "course_year")
    private String courseYear;

    @Column(name = "s_section")
    private String section;

    @Column(name = "quota")
    private String quota;


    @Column(name = "batch")
    private String batch;

    @Column(name = "academic_year")
    private String academicYear;

    @Column(name = "roll_no")
    private String rollNo;

    @Column(name = "hall_ticket_number")
    private String hallTicketNumber;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "gender")
    private String gender;

    @Column(name = "date_of_birth")
    private String dateOfBirth;

    @Column(name = "mobile")
    private String mobile;

    @Column(name = "student_emailid")
    private String studentEmailID;

    @Column(name = "aadhaar_no")
    private String aadhaarNo;

    @Column(name = "father_name")
    private String fatherName;

    @Column(name = "father_mobile")
    private String fatherMobile;

    @Column(name = "mother_name")
    private String motherName;

    @Column(name = "mother_mobile")
    private String motherMobile;

    @Column(name = "permanent_address")
    private String permanentAddress;

    @Column(name = "city")
    private String city;

    @Column(name = "district")
    private String district;
}