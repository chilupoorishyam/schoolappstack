package com.gts.cms.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Entity
@Table(name = "t_tt_live_cls_storage")
public class LiveClassStorage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_storage_id")
    private Long pkStorageId;

    @Column(name = "name")
    private String name;

    @Column(name = "mime")
    private String mime;

    @Column(name = "bucket")
    private String bucket;

    @Column(name = "object")
    private String object;

    @Column(name = "group_uid")
    private Long groupUid;

    @Column(name = "is_orginal")
    private Boolean isOriginal;

    @Column(name = "height")
    private Long height;
}
