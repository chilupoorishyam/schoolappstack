package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_bd_bulkdataprocess database table.
 * 
 */
@Entity
@Table(name="t_bd_bulkdataprocess")
@NamedQuery(name="Bulkdataprocess.findAll", query="SELECT b FROM Bulkdataprocess b")
public class Bulkdataprocess implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_bd_process_id")
	private Long bulkdataProcessId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="error_message")
	private String errorMessage;

	private String filename;

	private String filepath;

	@Column(name="is_active")
	private Boolean isActive;

	private Boolean iserrored;

	@Column(name="no_of_rows")
	private Integer noOfRows;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="process_end_date")
	private Date processEndDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="process_start_date")
	private Date processStartDate;

	private String reason;

	@Column(name="skipped_rows")
	private Integer skippedRows;

	@Column(name="success_rows")
	private Integer successRows;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	@Column(name="warning_message")
	private String warningMessage;

	//bi-directional many-to-one association to StgSubject
	@OneToMany(mappedBy="bulkdataprocess",fetch = FetchType.LAZY)
	private List<StgSubject> stgSubjects;

	public Bulkdataprocess() {
	}

	public Long getBulkdataProcessId() {
		return this.bulkdataProcessId;
	}

	public void setBulkdataProcessId(Long bulkdataProcessId) {
		this.bulkdataProcessId = bulkdataProcessId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getErrorMessage() {
		return this.errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getFilename() {
		return this.filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFilepath() {
		return this.filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIserrored() {
		return this.iserrored;
	}

	public void setIserrored(Boolean iserrored) {
		this.iserrored = iserrored;
	}

	public Integer getNoOfRows() {
		return this.noOfRows;
	}

	public void setNoOfRows(Integer noOfRows) {
		this.noOfRows = noOfRows;
	}

	public Date getProcessEndDate() {
		return this.processEndDate;
	}

	public void setProcessEndDate(Date processEndDate) {
		this.processEndDate = processEndDate;
	}

	public Date getProcessStartDate() {
		return this.processStartDate;
	}

	public void setProcessStartDate(Date processStartDate) {
		this.processStartDate = processStartDate;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getSkippedRows() {
		return this.skippedRows;
	}

	public void setSkippedRows(Integer skippedRows) {
		this.skippedRows = skippedRows;
	}

	public Integer getSuccessRows() {
		return this.successRows;
	}

	public void setSuccessRows(Integer successRows) {
		this.successRows = successRows;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getWarningMessage() {
		return this.warningMessage;
	}

	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}

	public List<StgSubject> getStgSubjects() {
		return this.stgSubjects;
	}

	public void setStgSubjects(List<StgSubject> stgSubjects) {
		this.stgSubjects = stgSubjects;
	}

	public StgSubject addStgSubject(StgSubject stgSubject) {
		getStgSubjects().add(stgSubject);
		stgSubject.setBulkdataprocess(this);

		return stgSubject;
	}

	public StgSubject removeStgSubject(StgSubject stgSubject) {
		getStgSubjects().remove(stgSubject);
		stgSubject.setBulkdataprocess(null);

		return stgSubject;
	}

}