package com.gts.cms.entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_dl_member_assessments database table.
 * 
 */
@Entity
@Table(name = "t_dl_member_assessments")
@NamedQuery(name = "MemberAssessment.findAll", query = "SELECT m FROM MemberAssessment m")
public class MemberAssessment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_dl_member_assessment_id")
	private Long memberAssessmentId;

	@Column(name = "attempt_no")
	private Integer attemptNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_time")
	private Date endTime;

	@ManyToOne
	@JoinColumn(name = "fk_dl_assessment_id")
	private Assessment assessment;

	@ManyToOne
	@JoinColumn(name = "fk_dl_course_member_id")
	private CourseMember courseMember;

	@ManyToOne
	@JoinColumn(name = "fk_dl_onlinecourse_id")
	private OnlineCourses onlineCourse;

	@ManyToOne
	@JoinColumn(name = "fk_subscription_membership_id")
	private DigitalLibraryMember subscriptionMembership;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_completed")
	private Boolean isCompleted;

	@Column(name = "is_paused")
	private Boolean isPaused;

	private Integer percentage;

	private String reason;

	@Column(name = "remaining_time")
	private Time remainingTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_time")
	private Date startTime;

	@Column(name = "total_marks")
	private Integer totalMarks;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to MemberAssessmentDetail
	@OneToMany(mappedBy = "memberAssessment")
	private List<MemberAssessmentDetail> memberAssessmentDetails;

	@OneToMany(mappedBy = "memberAssessment")
	private List<MemberAssessmentSummary> memberAssessmentSummary;

	public MemberAssessment() {
	}

	public Long getMemberAssessmentId() {
		return this.memberAssessmentId;
	}

	public void setMemberAssessmentId(Long memberAssessmentId) {
		this.memberAssessmentId = memberAssessmentId;
	}

	public Integer getAttemptNo() {
		return this.attemptNo;
	}

	public void setAttemptNo(Integer attemptNo) {
		this.attemptNo = attemptNo;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsCompleted() {
		return this.isCompleted;
	}

	public void setIsCompleted(Boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	public Boolean getIsPaused() {
		return this.isPaused;
	}

	public void setIsPaused(Boolean isPaused) {
		this.isPaused = isPaused;
	}

	public Integer getPercentage() {
		return this.percentage;
	}

	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Time getRemainingTime() {
		return this.remainingTime;
	}

	public void setRemainingTime(Time remainingTime) {
		this.remainingTime = remainingTime;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Integer getTotalMarks() {
		return this.totalMarks;
	}

	public void setTotalMarks(Integer totalMarks) {
		this.totalMarks = totalMarks;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Assessment getAssessment() {
		return assessment;
	}

	public void setAssessment(Assessment assessment) {
		this.assessment = assessment;
	}

	public CourseMember getCourseMember() {
		return courseMember;
	}

	public void setCourseMember(CourseMember courseMember) {
		this.courseMember = courseMember;
	}

	public OnlineCourses getOnlineCourse() {
		return onlineCourse;
	}

	public void setOnlineCourse(OnlineCourses onlineCourse) {
		this.onlineCourse = onlineCourse;
	}

	public DigitalLibraryMember getSubscriptionMembership() {
		return subscriptionMembership;
	}

	public void setSubscriptionMembership(DigitalLibraryMember subscriptionMembership) {
		this.subscriptionMembership = subscriptionMembership;
	}

	public List<MemberAssessmentDetail> getMemberAssessmentDetails() {
		return memberAssessmentDetails;
	}

	public void setMemberAssessmentDetails(List<MemberAssessmentDetail> memberAssessmentDetails) {
		this.memberAssessmentDetails = memberAssessmentDetails;
	}

	public List<MemberAssessmentSummary> getMemberAssessmentSummary() {
		return memberAssessmentSummary;
	}

	public void setMemberAssessmentSummary(List<MemberAssessmentSummary> memberAssessmentSummary) {
		this.memberAssessmentSummary = memberAssessmentSummary;
	}
}