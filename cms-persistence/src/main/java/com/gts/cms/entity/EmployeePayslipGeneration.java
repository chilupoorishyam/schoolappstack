package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_hr_employee_payslip_generation database table.
 * 
 */
@Entity
@Table(name="t_hr_employee_payslip_generation")
@NamedQuery(name="EmployeePayslipGeneration.findAll", query="SELECT e FROM EmployeePayslipGeneration e")
public class EmployeePayslipGeneration implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_emp_payslip_generation_id")
	private Long empPayslipGenerationId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="gross_pay")
	private BigDecimal grossPay;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="net_pay")
	private BigDecimal netPay;

	@Column(name="no_of_lop_days")
	private Integer noOfLopDays;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="payslip_generation_date")
	private Date payslipGenerationDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="payslip_month")
	private Date payslipMonth;

	private String reason;
	
	private String comments;

	//private String status;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeePayslipDetail
	@OneToMany(mappedBy="employeePayslipGeneration",fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	private List<EmployeePayslipDetail> employeePayslipDetails;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_id")
	private EmployeeDetail employeeDetail;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="payslip_generated_by_emp_id")
	private EmployeeDetail generatedByEmployeeDetail;

	//bi-directional many-to-one association to EmployeeBankDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_bank_detail_id")
	private EmployeeBankDetail employeeBankDetail;

	//bi-directional many-to-one association to PayrollGroup
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_payroll_group_id")
	private PayrollGroup payrollGroup;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;
	
	/*@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_payslip_status_catdet_id")
	private GeneralDetail payslipStatusCatdet;
*/
	//bi-directional many-to-one association to EmployeePayslipGenerationStatus
	@OneToMany(mappedBy="employeePayslipGeneration",fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	private List<EmployeePayslipGenerationStatus> employeePayslipGenerationStatuses;

	public EmployeePayslipGeneration() {
	}

	public Long getEmpPayslipGenerationId() {
		return this.empPayslipGenerationId;
	}

	public void setEmpPayslipGenerationId(Long empPayslipGenerationId) {
		this.empPayslipGenerationId = empPayslipGenerationId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public BigDecimal getGrossPay() {
		return this.grossPay;
	}

	public void setGrossPay(BigDecimal grossPay) {
		this.grossPay = grossPay;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public BigDecimal getNetPay() {
		return this.netPay;
	}

	public void setNetPay(BigDecimal netPay) {
		this.netPay = netPay;
	}

	public Integer getNoOfLopDays() {
		return this.noOfLopDays;
	}

	public void setNoOfLopDays(Integer noOfLopDays) {
		this.noOfLopDays = noOfLopDays;
	}

	public Date getPayslipGenerationDate() {
		return this.payslipGenerationDate;
	}

	public void setPayslipGenerationDate(Date payslipGenerationDate) {
		this.payslipGenerationDate = payslipGenerationDate;
	}

	public Date getPayslipMonth() {
		return this.payslipMonth;
	}

	public void setPayslipMonth(Date payslipMonth) {
		this.payslipMonth = payslipMonth;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	/*public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}*/

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<EmployeePayslipDetail> getEmployeePayslipDetails() {
		return this.employeePayslipDetails;
	}

	public void setEmployeePayslipDetails(List<EmployeePayslipDetail> employeePayslipDetails) {
		this.employeePayslipDetails = employeePayslipDetails;
	}

	public EmployeePayslipDetail addEmployeePayslipDetail(EmployeePayslipDetail employeePayslipDetail) {
		getEmployeePayslipDetails().add(employeePayslipDetail);
		employeePayslipDetail.setEmployeePayslipGeneration(this);

		return employeePayslipDetail;
	}

	public EmployeePayslipDetail removeEmployeePayslipDetail(EmployeePayslipDetail employeePayslipDetail) {
		getEmployeePayslipDetails().remove(employeePayslipDetail);
		employeePayslipDetail.setEmployeePayslipGeneration(null);

		return employeePayslipDetail;
	}

	public EmployeeDetail getEmployeeDetail() {
		return this.employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public EmployeeDetail getGeneratedByEmployeeDetail() {
		return this.generatedByEmployeeDetail;
	}

	public void setGeneratedByEmployeeDetail(EmployeeDetail generatedByEmployeeDetail) {
		this.generatedByEmployeeDetail = generatedByEmployeeDetail;
	}

	public EmployeeBankDetail getEmployeeBankDetail() {
		return this.employeeBankDetail;
	}

	public void setEmployeeBankDetail(EmployeeBankDetail employeeBankDetail) {
		this.employeeBankDetail = employeeBankDetail;
	}

	public PayrollGroup getPayrollGroup() {
		return this.payrollGroup;
	}

	public void setPayrollGroup(PayrollGroup payrollGroup) {
		this.payrollGroup = payrollGroup;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<EmployeePayslipGenerationStatus> getEmployeePayslipGenerationStatuses() {
		return this.employeePayslipGenerationStatuses;
	}

	public void setEmployeePayslipGenerationStatuses(List<EmployeePayslipGenerationStatus> employeePayslipGenerationStatuses) {
		this.employeePayslipGenerationStatuses = employeePayslipGenerationStatuses;
	}

	public EmployeePayslipGenerationStatus addEmployeePayslipGenerationStatus(EmployeePayslipGenerationStatus employeePayslipGenerationStatus) {
		getEmployeePayslipGenerationStatuses().add(employeePayslipGenerationStatus);
		employeePayslipGenerationStatus.setEmployeePayslipGeneration(this);

		return employeePayslipGenerationStatus;
	}

	public EmployeePayslipGenerationStatus removeEmployeePayslipGenerationStatus(EmployeePayslipGenerationStatus employeePayslipGenerationStatus) {
		getEmployeePayslipGenerationStatuses().remove(employeePayslipGenerationStatus);
		employeePayslipGenerationStatus.setEmployeePayslipGeneration(null);

		return employeePayslipGenerationStatus;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	

	/*public GeneralDetail getPayslipStatusCatdet() {
		return payslipStatusCatdet;
	}

	public void setPayslipStatusCatdet(GeneralDetail payslipStatusCatdet) {
		this.payslipStatusCatdet = payslipStatusCatdet;
	}*/

}