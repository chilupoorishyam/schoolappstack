package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_lib_reserve_book database table.
 * 
 */
@Entity
@Table(name="t_lib_reserve_book")
@NamedQuery(name="ReserveBook.findAll", query="SELECT r FROM ReserveBook r")
public class ReserveBook implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_lib_reserve_book_id")
	private Long reserveBookId;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="fk_lib_member_id")
	private Long libMemberId;

	@Column(name="is_active")
	private Boolean isActive;

	private Boolean iscancelled;

	private Boolean iscompleted;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="issued_on")
	private Date issuedOn;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="reserved_on")
	private Date reservedOn;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to Organization
	@ManyToOne
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to LibraryDetail
	@ManyToOne
	@JoinColumn(name="fk_library_id")
	private LibraryDetail libraryDetail;

	//bi-directional many-to-one association to Book
	@ManyToOne
	@JoinColumn(name="fk_lib_book_id")
	private Book book;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne
	@JoinColumn(name="fk_priority_catdet_id")
	private GeneralDetail priorityCatdet;

	public ReserveBook() {
	}

	public Long getReserveBookId() {
		return this.reserveBookId;
	}

	public void setReserveBookId(Long reserveBookId) {
		this.reserveBookId = reserveBookId;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Long getLibMemberId() {
		return this.libMemberId;
	}

	public void setLibMemberId(Long libMemberId) {
		this.libMemberId = libMemberId;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIscancelled() {
		return this.iscancelled;
	}

	public void setIscancelled(Boolean iscancelled) {
		this.iscancelled = iscancelled;
	}

	public Boolean getIscompleted() {
		return this.iscompleted;
	}

	public void setIscompleted(Boolean iscompleted) {
		this.iscompleted = iscompleted;
	}

	public Date getIssuedOn() {
		return this.issuedOn;
	}

	public void setIssuedOn(Date issuedOn) {
		this.issuedOn = issuedOn;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getReservedOn() {
		return this.reservedOn;
	}

	public void setReservedOn(Date reservedOn) {
		this.reservedOn = reservedOn;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public LibraryDetail getLibraryDetail() {
		return this.libraryDetail;
	}

	public void setLibraryDetail(LibraryDetail libraryDetail) {
		this.libraryDetail = libraryDetail;
	}

	public Book getBook() {
		return this.book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public GeneralDetail getPriorityCatdet() {
		return this.priorityCatdet;
	}

	public void setPriorityCatdet(GeneralDetail priorityCatdet) {
		this.priorityCatdet = priorityCatdet;
	}

}