package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the t_fee_online_trans_details database table.
 * 
 */
@Entity
@Table(name = "t_fee_online_trans_details")
@NamedQuery(name = "FeeOnlineTransDetail.findAll", query = "SELECT f FROM FeeOnlineTransDetail f")
public class FeeOnlineTransDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_fee_particularwise_payment_id")
	private Long feeParticularwisePaymentId;

	private BigDecimal amount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "payer_name")
	private String payerName;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to TEmpDetail
	@ManyToOne
	@JoinColumn(name = "fk_emp_id")
	private EmployeeDetail empDetail;

	// bi-directional many-to-one association to TFeeStructure
	@ManyToOne
	@JoinColumn(name = "fk_fee_structure_id")
	private FeeStructure feeStructure;

	// bi-directional many-to-one association to TMSchool
	@ManyToOne
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to TMFinancialYear
	@ManyToOne
	@JoinColumn(name = "fk_financial_year_id")
	private FinancialYear financialYear;

	// bi-directional many-to-one association to TStdStudentDetail
	@ManyToOne
	@JoinColumn(name = "fk_student_id")
	private StudentDetail stdStudentDetail;

	// bi-directional many-to-one association to TFeeStudentDataParticular
	@ManyToOne
	@JoinColumn(name = "fk_fee_std_data_particulars_id")
	private FeeStudentDataParticular feeStudentDataParticular;

	// bi-directional many-to-one association to TFeeStructureParticular
	@ManyToOne
	@JoinColumn(name = "fk_fee_structure_particular_id")
	private FeeStructureParticular feeStructureParticular;

	// bi-directional many-to-one association to TFeeStudentWiseParticular
	@ManyToOne
	@JoinColumn(name = "fk_fee_std_particular_id")
	private FeeStudentWiseParticular feeStudentWiseParticular;

	// bi-directional many-to-one association to TFeeParticular
	@ManyToOne
	@JoinColumn(name = "fk_fee_perticulars_id")
	private FeeParticular feeParticular;

	public FeeOnlineTransDetail() {
	}

	public Long getFeeParticularwisePaymentId() {
		return this.feeParticularwisePaymentId;
	}

	public void setFeeParticularwisePaymentId(Long feeParticularwisePaymentId) {
		this.feeParticularwisePaymentId = feeParticularwisePaymentId;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getPayerName() {
		return this.payerName;
	}

	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getEmpDetail() {
		return empDetail;
	}

	public void setEmpDetail(EmployeeDetail empDetail) {
		this.empDetail = empDetail;
	}

	public FeeStructure getFeeStructure() {
		return feeStructure;
	}

	public void setFeeStructure(FeeStructure feeStructure) {
		this.feeStructure = feeStructure;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public FinancialYear getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}

	public StudentDetail getStdStudentDetail() {
		return stdStudentDetail;
	}

	public void setStdStudentDetail(StudentDetail stdStudentDetail) {
		this.stdStudentDetail = stdStudentDetail;
	}

	public FeeStudentDataParticular getFeeStudentDataParticular() {
		return feeStudentDataParticular;
	}

	public void setFeeStudentDataParticular(FeeStudentDataParticular feeStudentDataParticular) {
		this.feeStudentDataParticular = feeStudentDataParticular;
	}

	public FeeStructureParticular getFeeStructureParticular() {
		return feeStructureParticular;
	}

	public void setFeeStructureParticular(FeeStructureParticular feeStructureParticular) {
		this.feeStructureParticular = feeStructureParticular;
	}

	public FeeStudentWiseParticular getFeeStudentWiseParticular() {
		return feeStudentWiseParticular;
	}

	public void setFeeStudentWiseParticular(FeeStudentWiseParticular feeStudentWiseParticular) {
		this.feeStudentWiseParticular = feeStudentWiseParticular;
	}

	public FeeParticular getFeeParticular() {
		return feeParticular;
	}

	public void setFeeParticular(FeeParticular feeParticular) {
		this.feeParticular = feeParticular;
	}

}