package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_exam_stg_marks database table.
 */
@Entity
@Table(name = "t_stg_exam_marks_bulkupload")
@NamedQuery(name = "StgExamMarksBulkUpload.findAll", query = "SELECT s FROM StgExamMarksBulkUpload s")
public class StgExamMarksBulkUpload implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_stg_exam_marks_bulkupload_id")
	private Long stgExamMarksBulkUploadId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_id")
	private Course course;


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_year_id")
	private CourseYear courseYear;


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_id")
	private ExamMaster exam;

	@Column(name = "uniquecode")
	private String uniqueCode;

	@Column(name = "subject_code")
	private String subjectCode;

	@Column(name = "subject_name")
	private String subjectName;

	@Column(name = "sno")
	private Integer sno;

	@Column(name = "roll_number")
	private String rollNumber;

	@Column(name = "student_name")
	private String studentName;

	@Column(name = "exam_marks")
	private String examMarks;

	private String grade;

	@Column(name = "grade_points")
	private String gradePoints;

	@Column(name = "internal_marks")
	private String internalMarks;

	@Column(name = "external_marks")
	private String externalMarks;

	private String credits;

	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_reevaluation")
	private Boolean isReevaluation;

	@Transient
	private Long subjectId;

	/*// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_examtype_catdet_id")
	private GeneralDetail examTypeCat;*/
	
	public Long getStgExamMarksBulkUploadId() {
		return stgExamMarksBulkUploadId;
	}

	public void setStgExamMarksBulkUploadId(Long stgExamMarksBulkUploadId) {
		this.stgExamMarksBulkUploadId = stgExamMarksBulkUploadId;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}



	public CourseYear getCourseYear() {
		return courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}


	public ExamMaster getExam() {
		return exam;
	}

	public void setExam(ExamMaster exam) {
		this.exam = exam;
	}

	public String getUniqueCode() {
		return uniqueCode;
	}

	public void setUniqueCode(String uniqueCode) {
		this.uniqueCode = uniqueCode;
	}

	public String getSubjectCode() {
		return subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public Integer getSno() {
		return sno;
	}

	public void setSno(Integer sno) {
		this.sno = sno;
	}

	public String getRollNumber() {
		return rollNumber;
	}

	public void setRollNumber(String rollNumber) {
		this.rollNumber = rollNumber;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Long getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}

	public Boolean getReevaluation() {
		return isReevaluation;
	}

	public void setReevaluation(Boolean reevaluation) {
		isReevaluation = reevaluation;
	}

	public String getExamMarks() {
		return examMarks;
	}

	public void setExamMarks(String examMarks) {
		this.examMarks = examMarks;
	}

	public String getGradePoints() {
		return gradePoints;
	}

	public void setGradePoints(String gradePoints) {
		this.gradePoints = gradePoints;
	}

	public String getInternalMarks() {
		return internalMarks;
	}

	public void setInternalMarks(String internalMarks) {
		this.internalMarks = internalMarks;
	}

	public String getExternalMarks() {
		return externalMarks;
	}

	public void setExternalMarks(String externalMarks) {
		this.externalMarks = externalMarks;
	}

	public String getCredits() {
		return credits;
	}

	public void setCredits(String credits) {
		this.credits = credits;
	}

	public Boolean getIsReevaluation() {
		return isReevaluation;
	}

	public void setIsReevaluation(Boolean isReevaluation) {
		this.isReevaluation = isReevaluation;
	}

	/*public GeneralDetail getExamTypeCat() {
		return examTypeCat;
	}

	public void setExamTypeCat(GeneralDetail examTypeCat) {
		this.examTypeCat = examTypeCat;
	}*/
}
