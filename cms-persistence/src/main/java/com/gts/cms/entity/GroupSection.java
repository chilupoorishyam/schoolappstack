package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_group_sections database table.
 * 
 */
@Entity
@Table(name="t_m_group_sections")
@NamedQuery(name="GroupSection.findAll", query="SELECT g FROM GroupSection g")
public class GroupSection implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_group_section_id")
	private Long groupSectionId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	private String section;

	@Column(name="sort_order")
	private Integer sortOrder;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to BatchwiseStudent
	@OneToMany(mappedBy="groupSection",fetch = FetchType.LAZY)
	private List<BatchwiseStudent> batchwiseStudents;

	//bi-directional many-to-one association to ElectiveGroupyrMapping
	@OneToMany(mappedBy="groupSection",fetch = FetchType.LAZY)
	private List<ElectiveGroupyrMapping> electiveGroupyrMappings;

	//bi-directional many-to-one association to SubjectCourseyear
	@OneToMany(mappedBy="groupSection",fetch = FetchType.LAZY)
	private List<SubjectCourseyear> subjectCourseyears;

	//bi-directional many-to-one association to FeeStructureCourseyr
	@OneToMany(mappedBy="groupSection",fetch = FetchType.LAZY)
	private List<FeeStructureCourseyr> feeStructureCourseyrs;

	//bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;


	//bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_year_id")
	private CourseYear courseYear;

	/*
	 * //bi-directional many-to-one association to StudentSubject
	 * 
	 * @OneToMany(mappedBy="groupSection",fetch = FetchType.LAZY) private
	 * List<StudentSubject> studentSubjects;
	 */
	//bi-directional many-to-one association to ActualClassesSchedule
	@OneToMany(mappedBy="groupSection",fetch = FetchType.LAZY)
	private List<ActualClassesSchedule> actualClassesSchedules;

	//bi-directional many-to-one association to Lessonstatus
	@OneToMany(mappedBy="groupSection",fetch = FetchType.LAZY)
	private List<Lessonstatus> lessonstatuses;

	//bi-directional many-to-one association to Schedule
	@OneToMany(mappedBy="groupSection",fetch = FetchType.LAZY)
	private List<Schedule> schedules;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;
	
	public GroupSection() {
	}

	public Long getGroupSectionId() {
		return this.groupSectionId;
	}

	public void setGroupSectionId(Long groupSectionId) {
		this.groupSectionId = groupSectionId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSection() {
		return this.section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public Integer getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<BatchwiseStudent> getBatchwiseStudents() {
		return this.batchwiseStudents;
	}

	public void setBatchwiseStudents(List<BatchwiseStudent> batchwiseStudents) {
		this.batchwiseStudents = batchwiseStudents;
	}

	public BatchwiseStudent addBatchwiseStudent(BatchwiseStudent batchwiseStudent) {
		getBatchwiseStudents().add(batchwiseStudent);
		batchwiseStudent.setGroupSection(this);

		return batchwiseStudent;
	}

	public BatchwiseStudent removeBatchwiseStudent(BatchwiseStudent batchwiseStudent) {
		getBatchwiseStudents().remove(batchwiseStudent);
		batchwiseStudent.setGroupSection(null);

		return batchwiseStudent;
	}

	public List<ElectiveGroupyrMapping> getElectiveGroupyrMappings() {
		return this.electiveGroupyrMappings;
	}

	public void setElectiveGroupyrMappings(List<ElectiveGroupyrMapping> electiveGroupyrMappings) {
		this.electiveGroupyrMappings = electiveGroupyrMappings;
	}

	public ElectiveGroupyrMapping addElectiveGroupyrMapping(ElectiveGroupyrMapping electiveGroupyrMapping) {
		getElectiveGroupyrMappings().add(electiveGroupyrMapping);
		electiveGroupyrMapping.setGroupSection(this);

		return electiveGroupyrMapping;
	}

	public ElectiveGroupyrMapping removeElectiveGroupyrMapping(ElectiveGroupyrMapping electiveGroupyrMapping) {
		getElectiveGroupyrMappings().remove(electiveGroupyrMapping);
		electiveGroupyrMapping.setGroupSection(null);

		return electiveGroupyrMapping;
	}

	public List<SubjectCourseyear> getSubjectCourseyears() {
		return this.subjectCourseyears;
	}

	public void setSubjectCourseyears(List<SubjectCourseyear> subjectCourseyears) {
		this.subjectCourseyears = subjectCourseyears;
	}

	public SubjectCourseyear addSubjectCourseyear(SubjectCourseyear subjectCourseyear) {
		getSubjectCourseyears().add(subjectCourseyear);
		subjectCourseyear.setGroupSection(this);

		return subjectCourseyear;
	}

	public SubjectCourseyear removeSubjectCourseyear(SubjectCourseyear subjectCourseyear) {
		getSubjectCourseyears().remove(subjectCourseyear);
		subjectCourseyear.setGroupSection(null);

		return subjectCourseyear;
	}

	public List<FeeStructureCourseyr> getFeeStructureCourseyrs() {
		return this.feeStructureCourseyrs;
	}

	public void setFeeStructureCourseyrs(List<FeeStructureCourseyr> feeStructureCourseyrs) {
		this.feeStructureCourseyrs = feeStructureCourseyrs;
	}

	public FeeStructureCourseyr addFeeStructureCourseyr(FeeStructureCourseyr feeStructureCourseyr) {
		getFeeStructureCourseyrs().add(feeStructureCourseyr);
		feeStructureCourseyr.setGroupSection(this);

		return feeStructureCourseyr;
	}

	public FeeStructureCourseyr removeFeeStructureCourseyr(FeeStructureCourseyr feeStructureCourseyr) {
		getFeeStructureCourseyrs().remove(feeStructureCourseyr);
		feeStructureCourseyr.setGroupSection(null);

		return feeStructureCourseyr;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}



	public CourseYear getCourseYear() {
		return this.courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	/*
	 * public List<StudentSubject> getStudentSubjects() { return
	 * this.studentSubjects; }
	 * 
	 * public void setStudentSubjects(List<StudentSubject> studentSubjects) {
	 * this.studentSubjects = studentSubjects; }
	 */

	/*
	 * public StudentSubject addStudentSubject(StudentSubject studentSubject) {
	 * getStudentSubjects().add(studentSubject);
	 * studentSubject.setGroupSection(this);
	 * 
	 * return studentSubject; }
	 * 
	 * public StudentSubject removeStudentSubject(StudentSubject studentSubject) {
	 * getStudentSubjects().remove(studentSubject);
	 * studentSubject.setGroupSection(null);
	 * 
	 * return studentSubject; }
	 */

	public List<ActualClassesSchedule> getActualClassesSchedules() {
		return this.actualClassesSchedules;
	}

	public void setActualClassesSchedules(List<ActualClassesSchedule> actualClassesSchedules) {
		this.actualClassesSchedules = actualClassesSchedules;
	}

	public ActualClassesSchedule addActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		getActualClassesSchedules().add(actualClassesSchedule);
		actualClassesSchedule.setGroupSection(this);

		return actualClassesSchedule;
	}

	public ActualClassesSchedule removeActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		getActualClassesSchedules().remove(actualClassesSchedule);
		actualClassesSchedule.setGroupSection(null);

		return actualClassesSchedule;
	}

	public List<Lessonstatus> getLessonstatuses() {
		return this.lessonstatuses;
	}

	public void setLessonstatuses(List<Lessonstatus> lessonstatuses) {
		this.lessonstatuses = lessonstatuses;
	}

	public Lessonstatus addLessonstatus(Lessonstatus lessonstatus) {
		getLessonstatuses().add(lessonstatus);
		lessonstatus.setGroupSection(this);

		return lessonstatus;
	}

	public Lessonstatus removeLessonstatus(Lessonstatus lessonstatus) {
		getLessonstatuses().remove(lessonstatus);
		lessonstatus.setGroupSection(null);

		return lessonstatus;
	}

	public List<Schedule> getSchedules() {
		return this.schedules;
	}

	public void setSchedules(List<Schedule> schedules) {
		this.schedules = schedules;
	}

	public Schedule addSchedule(Schedule schedule) {
		getSchedules().add(schedule);
		schedule.setGroupSection(this);

		return schedule;
	}

	public Schedule removeSchedule(Schedule schedule) {
		getSchedules().remove(schedule);
		schedule.setGroupSection(null);

		return schedule;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

}