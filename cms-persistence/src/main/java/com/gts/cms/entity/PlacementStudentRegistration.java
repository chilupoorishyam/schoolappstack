package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_pa_placement_student_registration database table.
 * 
 */

@Entity
@Table(name = "t_pa_placement_student_registration")
@NamedQuery(name = "placementStudentRegistration.findAll", query = "SELECT r FROM PlacementStudentRegistration r")
public class PlacementStudentRegistration implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_pa_std_reg_id")
	private Long placementStdRegId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;
	
    private String reason;
	
	@Column(name = "is_active")
	private Boolean isActive;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "registered_date")
	private Date registeredDate;
	
	@Column(name = "joined_on")
	private Date joinedOn;
	
	@Column(name = "joining_date")
	private Date joiningDate;
	
	@Column(name = "offer_date")
	private Date offerDate;
	
	@Column(name = "is_joined")
	private Boolean isJoined;
	
	@Column(name = "interviewer_comments")
	private String interviewerComments;
	
	@Column(name = "is_registered")
	private Boolean isRegistered;
	
	@Column(name = "is_cv_shortlisted")
	private Boolean isCVShortlisted;
	
	@Column(name = "is_cleared_written")
	private Boolean isClearedWritten;
	
	@Column(name = "is_cleared_gd")
	private Boolean isClearedGD;
	
	@Column(name = "is_cleared_pre_hr")
	private Boolean isClearedPreHR;
	
	@Column(name = "is_cleared_first_tech")
	private Boolean isClearedFirstTech;
	
	@Column(name = "is_cleared_second_tech")
	private Boolean isClearedSecondTech;
	
	@Column(name = "is_cleared_third_tech")
	private Boolean isClearedThirdTech;
	
	@Column(name = "is_cleared_hr")
	private Boolean isClearedHR;
	
	@Column(name = "is_cleared_manager_round")
	private Boolean isClearedManagerRound;
	
	@Column(name = "is_placed")
	private Boolean isPlaced;
	
	@Column(name = "is_offer_roll_out")
	private Boolean isOfferRollOut;
	

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_company_id")
	private Company company;
	
	// bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_pa_placement_id")
	private Placement placement;
	
	@Column(name ="yearly_package")
	private BigDecimal yearlyPackage;

	public Long getPlacementStdRegId() {
		return placementStdRegId;
	}

	public void setPlacementStdRegId(Long placementStdRegId) {
		this.placementStdRegId = placementStdRegId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Date getRegisteredDate() {
		return registeredDate;
	}

	public void setRegisteredDate(Date registeredDate) {
		this.registeredDate = registeredDate;
	}

	public Date getJoinedOn() {
		return joinedOn;
	}

	public void setJoinedOn(Date joinedOn) {
		this.joinedOn = joinedOn;
	}

	public Date getJoiningDate() {
		return joiningDate;
	}

	public void setJoiningDate(Date joiningDate) {
		this.joiningDate = joiningDate;
	}

	public Date getOfferDate() {
		return offerDate;
	}

	public void setOfferDate(Date offerDate) {
		this.offerDate = offerDate;
	}

	public Boolean getIsJoined() {
		return isJoined;
	}

	public void setIsJoined(Boolean isJoined) {
		this.isJoined = isJoined;
	}

	public String getInterviewerComments() {
		return interviewerComments;
	}

	public void setInterviewerComments(String interviewerComments) {
		this.interviewerComments = interviewerComments;
	}

	public Boolean getIsRegistered() {
		return isRegistered;
	}

	public void setIsRegistered(Boolean isRegistered) {
		this.isRegistered = isRegistered;
	}

	public Boolean getIsCVShortlisted() {
		return isCVShortlisted;
	}

	public void setIsCVShortlisted(Boolean isCVShortlisted) {
		this.isCVShortlisted = isCVShortlisted;
	}

	public Boolean getIsClearedWritten() {
		return isClearedWritten;
	}

	public void setIsClearedWritten(Boolean isClearedWritten) {
		this.isClearedWritten = isClearedWritten;
	}

	public Boolean getIsClearedGD() {
		return isClearedGD;
	}

	public void setIsClearedGD(Boolean isClearedGD) {
		this.isClearedGD = isClearedGD;
	}

	public Boolean getIsClearedPreHR() {
		return isClearedPreHR;
	}

	public void setIsClearedPreHR(Boolean isClearedPreHR) {
		this.isClearedPreHR = isClearedPreHR;
	}

	public Boolean getIsClearedFirstTech() {
		return isClearedFirstTech;
	}

	public void setIsClearedFirstTech(Boolean isClearedFirstTech) {
		this.isClearedFirstTech = isClearedFirstTech;
	}

	public Boolean getIsClearedSecondTech() {
		return isClearedSecondTech;
	}

	public void setIsClearedSecondTech(Boolean isClearedSecondTech) {
		this.isClearedSecondTech = isClearedSecondTech;
	}

	public Boolean getIsClearedThirdTech() {
		return isClearedThirdTech;
	}

	public void setIsClearedThirdTech(Boolean isClearedThirdTech) {
		this.isClearedThirdTech = isClearedThirdTech;
	}

	public Boolean getIsClearedHR() {
		return isClearedHR;
	}

	public void setIsClearedHR(Boolean isClearedHR) {
		this.isClearedHR = isClearedHR;
	}

	public Boolean getIsClearedManagerRound() {
		return isClearedManagerRound;
	}

	public void setIsClearedManagerRound(Boolean isClearedManagerRound) {
		this.isClearedManagerRound = isClearedManagerRound;
	}

	public Boolean getIsPlaced() {
		return isPlaced;
	}

	public void setIsPlaced(Boolean isPlaced) {
		this.isPlaced = isPlaced;
	}

	public Boolean getIsOfferRollOut() {
		return isOfferRollOut;
	}

	public void setIsOfferRollOut(Boolean isOfferRollOut) {
		this.isOfferRollOut = isOfferRollOut;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public Placement getPlacement() {
		return placement;
	}

	public void setPlacement(Placement placement) {
		this.placement = placement;
	}

	public BigDecimal getYearlyPackage() {
		return yearlyPackage;
	}

	public void setYearlyPackage(BigDecimal yearlyPackage) {
		this.yearlyPackage = yearlyPackage;
	}
	
}
