package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the t_fee_student_refunds database table.
 * 
 */
@Entity
@Table(name="t_fee_student_refunds")
@NamedQuery(name="FeeStudentRefund.findAll", query="SELECT f FROM FeeStudentRefund f")
public class FeeStudentRefund implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_student_refunds_id")
	private Long feeStudentRefundsId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_reverted")
	private Boolean isReverted;

	private String reason;

	@Column(name="refund_amount")
	private BigDecimal refundAmount;

	@Column(name="refund_reason")
	private String refundReason;

	@Column(name="refund_receipts_no")
	private String refundReceiptsNo;

	@Column(name="revert_reason")
	private String revertReason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_revert_emp_id")
	private EmployeeDetail employeeDetail;

	//bi-directional many-to-one association to FeeStructure
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_structure_id")
	private FeeStructure feeStructure;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_student_id")
	private StudentDetail studentDetail;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_financial_year_id")
	private FinancialYear financialYear;
	
	//bi-directional many-to-one association to FeeReceipt
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_receipts_id")
	private FeeReceipt feeReceipt;

	//bi-directional many-to-one association to FeeReceipt
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_ref_receipts_id")
	private FeeReceipt reffeeReceipt;
	
	
	//bi-directional many-to-one association to FeeReceipt
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_ref_fee_particularwise_payment_id")
	private FeeParticularwisePayment feeParticularwisePayment;
	
	public FeeStudentRefund() {
	}

	public Long getFeeStudentRefundsId() {
		return this.feeStudentRefundsId;
	}

	public void setFeeStudentRefundsId(Long feeStudentRefundsId) {
		this.feeStudentRefundsId = feeStudentRefundsId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsReverted() {
		return this.isReverted;
	}

	public void setIsReverted(Boolean isReverted) {
		this.isReverted = isReverted;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getRefundAmount() {
		return this.refundAmount;
	}

	public void setRefundAmount(BigDecimal refundAmount) {
		this.refundAmount = refundAmount;
	}

	public String getRefundReason() {
		return this.refundReason;
	}

	public void setRefundReason(String refundReason) {
		this.refundReason = refundReason;
	}

	public String getRefundReceiptsNo() {
		return this.refundReceiptsNo;
	}

	public void setRefundReceiptsNo(String refundReceiptsNo) {
		this.refundReceiptsNo = refundReceiptsNo;
	}

	public String getRevertReason() {
		return this.revertReason;
	}

	public void setRevertReason(String revertReason) {
		this.revertReason = revertReason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getEmployeeDetail() {
		return this.employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public FeeStructure getFeeStructure() {
		return this.feeStructure;
	}

	public void setFeeStructure(FeeStructure feeStructure) {
		this.feeStructure = feeStructure;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public FinancialYear getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}

	public FeeReceipt getFeeReceipt() {
		return feeReceipt;
	}

	public void setFeeReceipt(FeeReceipt feeReceipt) {
		this.feeReceipt = feeReceipt;
	}

	public FeeReceipt getReffeeReceipt() {
		return reffeeReceipt;
	}

	public void setReffeeReceipt(FeeReceipt reffeeReceipt) {
		this.reffeeReceipt = reffeeReceipt;
	}

	public FeeParticularwisePayment getFeeParticularwisePayment() {
		return feeParticularwisePayment;
	}

	public void setFeeParticularwisePayment(FeeParticularwisePayment feeParticularwisePayment) {
		this.feeParticularwisePayment = feeParticularwisePayment;
	}

	
}