package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the t_m_school_certificates database table.
 * 
 */
@Entity
@Table(name="t_m_school_certificates")
@NamedQuery(name="SchoolCertificate.findAll", query="SELECT c FROM SchoolCertificate c")
public class SchoolCertificate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_clg_certificate_id")
	private Long schoolCertificateId;

	private BigDecimal amount;

	@Column(name="certifcate_code")
	private String certifcateCode;

	@Column(name="certificate_name")
	private String certificateName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="duplicate_certificate_amount")
	private BigDecimal duplicateCertificateAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="from_date")
	private Date fromDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_approval_req")
	private Boolean isApprovalReq;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="to_date")
	private Date toDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to Campus
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_campus_id")
	private Campus campus;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_certificate_type_catdet_id")
	private GeneralDetail certificateType;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	public SchoolCertificate() {
	}

	public Long getSchoolCertificateId() {
		return this.schoolCertificateId;
	}

	public void setSchoolCertificateId(Long schoolCertificateId) {
		this.schoolCertificateId = schoolCertificateId;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCertifcateCode() {
		return this.certifcateCode;
	}

	public void setCertifcateCode(String certifcateCode) {
		this.certifcateCode = certifcateCode;
	}

	public String getCertificateName() {
		return this.certificateName;
	}

	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public BigDecimal getDuplicateCertificateAmount() {
		return this.duplicateCertificateAmount;
	}

	public void setDuplicateCertificateAmount(BigDecimal duplicateCertificateAmount) {
		this.duplicateCertificateAmount = duplicateCertificateAmount;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsApprovalReq() {
		return this.isApprovalReq;
	}

	public void setIsApprovalReq(Boolean isApprovalReq) {
		this.isApprovalReq = isApprovalReq;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Campus getCampus() {
		return this.campus;
	}

	public void setCampus(Campus campus) {
		this.campus = campus;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getCertificateType() {
		return this.certificateType;
	}

	public void setCertificateType(GeneralDetail certificateType) {
		this.certificateType = certificateType;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

}