package com.gts.cms.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Setter
@Getter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_lib_books_stg")
public class BookStg implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_book_id")
    private Long pkBookId;

    @Column(name = "library_code")
    private String libraryCode;

    @Column(name = "acc_no")
    private String accNo;

    @Column(name = "title")
    private String title;

    @Column(name = "author")
    private String author;

    @Column(name = "publisher")
    private String publisher;

    @Column(name = "edition")
    private String edition;

    @Column(name = "volume")
    private String volume;

    @Column(name = "year")
    private String year;

    @Column(name = "cost")
    private String cost;

    @Column(name = "no_of_pages")
    private String noOfPages;

    @Column(name = "isbn")
    private String isbn;

    @Column(name = "invoice_no")
    private String invoiceNo;

    @Column(name = "supplier")
    private String supplier;

    @Column(name = "department_id")
    private String departmentId;

    @Column(name = "language")
    private String language;

    @Column(name = "book_registration_type")
    private String bookRegistrationType;

}