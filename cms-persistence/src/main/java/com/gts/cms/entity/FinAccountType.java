package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_fin_account_types database table.
 * 
 */
@Entity
@Table(name="t_fin_account_types")
@NamedQuery(name="FinAccountType.findAll", query="SELECT f FROM FinAccountType f")
public class FinAccountType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_account_type_id")
	private Long accountTypeId;

	@Column(name="accounttype_code")
	private String accounttypeCode;

	@Column(name="accounttype_name")
	private String accounttypeName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FeeStructureParticular
	@OneToMany(mappedBy="mappingAccountType",fetch = FetchType.LAZY)
	private List<FeeStructureParticular> feeStructureParticulars1;

	//bi-directional many-to-one association to FeeStructureParticular
	@OneToMany(mappedBy="cashAccountType",fetch = FetchType.LAZY)
	private List<FeeStructureParticular> feeStructureParticulars2;

	//bi-directional many-to-one association to FeeStructureParticular
	@OneToMany(mappedBy="bankAccountType",fetch = FetchType.LAZY)
	private List<FeeStructureParticular> feeStructureParticulars3;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_acc_entity_id")
	private AccountEntity accountEntity;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_parent_account_type_id")
	private FinAccountType parentAccountType;
	
	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_major_accounttype_catdet_id")
	private GeneralDetail majorAccountType;
	
	@Column(name="is_group_account")
	private Boolean isGroupAccount;

	public FinAccountType() {
	}

	public Long getAccountTypeId() {
		return this.accountTypeId;
	}

	public void setAccountTypeId(Long accountTypeId) {
		this.accountTypeId = accountTypeId;
	}

	public String getAccounttypeCode() {
		return this.accounttypeCode;
	}

	public void setAccounttypeCode(String accounttypeCode) {
		this.accounttypeCode = accounttypeCode;
	}

	public String getAccounttypeName() {
		return this.accounttypeName;
	}

	public void setAccounttypeName(String accounttypeName) {
		this.accounttypeName = accounttypeName;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<FeeStructureParticular> getFeeStructureParticulars1() {
		return this.feeStructureParticulars1;
	}

	public void setFeeStructureParticulars1(List<FeeStructureParticular> feeStructureParticulars1) {
		this.feeStructureParticulars1 = feeStructureParticulars1;
	}

	public FeeStructureParticular addFeeStructureParticulars1(FeeStructureParticular feeStructureParticulars1) {
		getFeeStructureParticulars1().add(feeStructureParticulars1);
		feeStructureParticulars1.setMappingAccountType(this);

		return feeStructureParticulars1;
	}

	public FeeStructureParticular removeFeeStructureParticulars1(FeeStructureParticular feeStructureParticulars1) {
		getFeeStructureParticulars1().remove(feeStructureParticulars1);
		feeStructureParticulars1.setMappingAccountType(null);

		return feeStructureParticulars1;
	}

	public List<FeeStructureParticular> getFeeStructureParticulars2() {
		return this.feeStructureParticulars2;
	}

	public void setFeeStructureParticulars2(List<FeeStructureParticular> feeStructureParticulars2) {
		this.feeStructureParticulars2 = feeStructureParticulars2;
	}

	public FeeStructureParticular addFeeStructureParticulars2(FeeStructureParticular feeStructureParticulars2) {
		getFeeStructureParticulars2().add(feeStructureParticulars2);
		feeStructureParticulars2.setCashAccountType(this);

		return feeStructureParticulars2;
	}

	public FeeStructureParticular removeFeeStructureParticulars2(FeeStructureParticular feeStructureParticulars2) {
		getFeeStructureParticulars2().remove(feeStructureParticulars2);
		feeStructureParticulars2.setCashAccountType(null);

		return feeStructureParticulars2;
	}

	public List<FeeStructureParticular> getFeeStructureParticulars3() {
		return this.feeStructureParticulars3;
	}

	public void setFeeStructureParticulars3(List<FeeStructureParticular> feeStructureParticulars3) {
		this.feeStructureParticulars3 = feeStructureParticulars3;
	}

	public FeeStructureParticular addFeeStructureParticulars3(FeeStructureParticular feeStructureParticulars3) {
		getFeeStructureParticulars3().add(feeStructureParticulars3);
		feeStructureParticulars3.setBankAccountType(this);

		return feeStructureParticulars3;
	}

	public FeeStructureParticular removeFeeStructureParticulars3(FeeStructureParticular feeStructureParticulars3) {
		getFeeStructureParticulars3().remove(feeStructureParticulars3);
		feeStructureParticulars3.setBankAccountType(null);

		return feeStructureParticulars3;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public AccountEntity getAccountEntity() {
		return accountEntity;
	}

	public void setAccountEntity(AccountEntity accountEntity) {
		this.accountEntity = accountEntity;
	}

	public FinAccountType getParentAccountType() {
		return parentAccountType;
	}

	public void setParentAccountType(FinAccountType parentAccountType) {
		this.parentAccountType = parentAccountType;
	}

	public GeneralDetail getMajorAccountType() {
		return majorAccountType;
	}

	public void setMajorAccountType(GeneralDetail majorAccountType) {
		this.majorAccountType = majorAccountType;
	}

	public Boolean getIsGroupAccount() {
		return isGroupAccount;
	}

	public void setIsGroupAccount(Boolean isGroupAccount) {
		this.isGroupAccount = isGroupAccount;
	}
	
	

}