package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_inv_srv_items database table.
 * 
 */
@Entity
@Table(name = "t_inv_srv_items")
@NamedQuery(name = "InvSrvItem.findAll", query = "SELECT i FROM InvSrvItem i")
public class InvSrvItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_srv_item_id")
	private Long srvItemId;

	@Temporal(TemporalType.DATE)
	@Column(name = "approved_date")
	private Date approvedDate;

	@Column(name = "approved_qty")
	private BigDecimal approvedQty;

	@Column(name = "batch_no")
	private String batchNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	@Column(name = "delivery_date")
	private Date deliveryDate;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_approved_emp_id")
	private EmployeeDetail approvedEmpDetail;

	// bi-directional many-to-one association to Department
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_ordered_dept_id")
	private Department department;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_rejected_emp_id")
	private EmployeeDetail rejectedEmp;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_free")
	private Boolean isFree;

	@Column(name = "item_discount_percentage")
	private BigDecimal itemDiscountPercentage;

	@Column(name = "item_total_actual_amount")
	private BigDecimal itemTotalActualAmount;

	@Column(name = "item_total_cost")
	private BigDecimal itemTotalCost;

	@Column(name = "item_total_discount_amount")
	private BigDecimal itemTotalDiscountAmount;

	@Column(name = "item_total_tax_amount")
	private BigDecimal itemTotalTaxAmount;

	@Column(name = "item_unit_amount")
	private BigDecimal itemUnitAmount;

	@Column(name = "order_quantity")
	private BigDecimal orderQuantity;

	private String reason;

	@Column(name = "received_qty")
	private BigDecimal receivedQty;

	@Temporal(TemporalType.DATE)
	@Column(name = "rejected_date")
	private Date rejectedDate;

	@Column(name = "rejected_qty")
	private BigDecimal rejectedQty;

	@Column(name = "rejected_reason")
	private String rejectedReason;

	@Column(name = "returned_qty")
	private BigDecimal returnedQty;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to InvPurchasereturnItem
	@OneToMany(mappedBy = "invSrvItem", cascade = CascadeType.ALL)
	private List<InvPurchasereturnItem> invPurchasereturnItems;

	// bi-directional many-to-one association to InvSrv
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_srv_id")
	private InvSrv invSrv;

	// bi-directional many-to-one association to InvPoItem
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_po_item_id")
	private InvPoItem invPoItem;

	// bi-directional many-to-one association to InvItemmaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_item_id")
	private InvItemmaster invItemMaster;

	// bi-directional many-to-one association to InvUommaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_uom_id")
	private InvUommaster invUommaster;

	// bi-directional many-to-one association to InvItemmaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_freefor_item_id")
	private InvItemmaster freeForItemMaster;

	public InvSrvItem() {
	}

	public Long getSrvItemId() {
		return this.srvItemId;
	}

	public void setSrvItemId(Long srvItemId) {
		this.srvItemId = srvItemId;
	}

	public Date getApprovedDate() {
		return this.approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public BigDecimal getApprovedQty() {
		return this.approvedQty;
	}

	public void setApprovedQty(BigDecimal approvedQty) {
		this.approvedQty = approvedQty;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getDeliveryDate() {
		return this.deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsFree() {
		return this.isFree;
	}

	public void setIsFree(Boolean isFree) {
		this.isFree = isFree;
	}

	public BigDecimal getItemDiscountPercentage() {
		return this.itemDiscountPercentage;
	}

	public void setItemDiscountPercentage(BigDecimal itemDiscountPercentage) {
		this.itemDiscountPercentage = itemDiscountPercentage;
	}

	public BigDecimal getItemTotalActualAmount() {
		return this.itemTotalActualAmount;
	}

	public void setItemTotalActualAmount(BigDecimal itemTotalActualAmount) {
		this.itemTotalActualAmount = itemTotalActualAmount;
	}

	public BigDecimal getItemTotalCost() {
		return this.itemTotalCost;
	}

	public void setItemTotalCost(BigDecimal itemTotalCost) {
		this.itemTotalCost = itemTotalCost;
	}

	public BigDecimal getItemTotalDiscountAmount() {
		return this.itemTotalDiscountAmount;
	}

	public void setItemTotalDiscountAmount(BigDecimal itemTotalDiscountAmount) {
		this.itemTotalDiscountAmount = itemTotalDiscountAmount;
	}

	public BigDecimal getItemTotalTaxAmount() {
		return this.itemTotalTaxAmount;
	}

	public void setItemTotalTaxAmount(BigDecimal itemTotalTaxAmount) {
		this.itemTotalTaxAmount = itemTotalTaxAmount;
	}

	public BigDecimal getItemUnitAmount() {
		return this.itemUnitAmount;
	}

	public void setItemUnitAmount(BigDecimal itemUnitAmount) {
		this.itemUnitAmount = itemUnitAmount;
	}

	public BigDecimal getOrderQuantity() {
		return this.orderQuantity;
	}

	public void setOrderQuantity(BigDecimal orderQuantity) {
		this.orderQuantity = orderQuantity;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getReceivedQty() {
		return this.receivedQty;
	}

	public void setReceivedQty(BigDecimal receivedQty) {
		this.receivedQty = receivedQty;
	}

	public Date getRejectedDate() {
		return this.rejectedDate;
	}

	public void setRejectedDate(Date rejectedDate) {
		this.rejectedDate = rejectedDate;
	}

	public BigDecimal getRejectedQty() {
		return this.rejectedQty;
	}

	public void setRejectedQty(BigDecimal rejectedQty) {
		this.rejectedQty = rejectedQty;
	}

	public String getRejectedReason() {
		return this.rejectedReason;
	}

	public void setRejectedReason(String rejectedReason) {
		this.rejectedReason = rejectedReason;
	}

	public BigDecimal getReturnedQty() {
		return this.returnedQty;
	}

	public void setReturnedQty(BigDecimal returnedQty) {
		this.returnedQty = returnedQty;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public InvSrv getInvSrv() {
		return invSrv;
	}

	public void setInvSrv(InvSrv invSrv) {
		this.invSrv = invSrv;
	}

	public EmployeeDetail getApprovedEmpDetail() {
		return approvedEmpDetail;
	}

	public void setApprovedEmpDetail(EmployeeDetail approvedEmpDetail) {
		this.approvedEmpDetail = approvedEmpDetail;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public EmployeeDetail getRejectedEmp() {
		return rejectedEmp;
	}

	public void setRejectedEmp(EmployeeDetail rejectedEmp) {
		this.rejectedEmp = rejectedEmp;
	}

	public InvPoItem getInvPoItem() {
		return invPoItem;
	}

	public void setInvPoItem(InvPoItem invPoItem) {
		this.invPoItem = invPoItem;
	}

	public InvUommaster getInvUommaster() {
		return invUommaster;
	}

	public void setInvUommaster(InvUommaster invUommaster) {
		this.invUommaster = invUommaster;
	}

	public List<InvPurchasereturnItem> getInvPurchasereturnItems() {
		return invPurchasereturnItems;
	}

	public void setInvPurchasereturnItems(List<InvPurchasereturnItem> invPurchasereturnItems) {
		this.invPurchasereturnItems = invPurchasereturnItems;
	}

	public InvItemmaster getInvItemMaster() {
		return invItemMaster;
	}

	public void setInvItemMaster(InvItemmaster invItemMaster) {
		this.invItemMaster = invItemMaster;
	}

	public InvItemmaster getFreeForItemMaster() {
		return freeForItemMaster;
	}

	public void setFreeForItemMaster(InvItemmaster freeForItemMaster) {
		this.freeForItemMaster = freeForItemMaster;
	}
}