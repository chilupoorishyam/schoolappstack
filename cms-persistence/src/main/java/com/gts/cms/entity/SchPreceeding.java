package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_sch_preceedings database table.
 * 
 */
@Entity
@Table(name = "t_sch_preceedings")
@NamedQuery(name = "SchPreceeding.findAll", query = "SELECT s FROM SchPreceeding s")
public class SchPreceeding implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_sch_preceeding_id")
	private Long schPreceedingId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "bank_amount_credited_on")
	private Date bankAmountCreditedOn;

	@Column(name = "bank_code")
	private String bankCode;

	@Column(name = "branch_name")
	private String branchName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "no_of_students")
	private Integer noOfStudents;

	@Column(name = "preceeding_amount")
	private BigDecimal preceedingAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "preceeding_date")
	private Date preceedingDate;

	@Column(name = "preceeding_description")
	private String preceedingDescription;

	@Column(name = "preceeding_no")
	private String preceedingNo;

	@Column(name = "preceeding_title")
	private String preceedingTitle;
	
	@Column(name="scholarship_type")
	private String scholarshipType;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to Bank
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_bank_id")
	private Bank bank;

	// bi-directional many-to-one association to Caste
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_caste_id")
	private Caste caste;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to FinancialYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_financial_year_id")
	private FinancialYear financialYear;

	// bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_academic_year_id")
	private AcademicYear academicYear;

	// bi-directional many-to-one association to SchAccountsPreceeding
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_sch_acc_preceedings_id")
	private SchAccountsPreceeding accountsPreceeding;

	// bi-directional many-to-one association to SchStdPreceeding
	@OneToMany(mappedBy = "preceeding", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<SchStdPreceeding> stdPreceedings;

	public SchPreceeding() {
	}

	public Long getSchPreceedingId() {
		return this.schPreceedingId;
	}

	public void setSchPreceedingId(Long schPreceedingId) {
		this.schPreceedingId = schPreceedingId;
	}

	public Date getBankAmountCreditedOn() {
		return this.bankAmountCreditedOn;
	}

	public void setBankAmountCreditedOn(Date bankAmountCreditedOn) {
		this.bankAmountCreditedOn = bankAmountCreditedOn;
	}

	public String getBankCode() {
		return this.bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getNoOfStudents() {
		return this.noOfStudents;
	}

	public void setNoOfStudents(Integer noOfStudents) {
		this.noOfStudents = noOfStudents;
	}

	public BigDecimal getPreceedingAmount() {
		return this.preceedingAmount;
	}

	public void setPreceedingAmount(BigDecimal preceedingAmount) {
		this.preceedingAmount = preceedingAmount;
	}

	public Date getPreceedingDate() {
		return this.preceedingDate;
	}

	public void setPreceedingDate(Date preceedingDate) {
		this.preceedingDate = preceedingDate;
	}

	public String getPreceedingDescription() {
		return this.preceedingDescription;
	}

	public void setPreceedingDescription(String preceedingDescription) {
		this.preceedingDescription = preceedingDescription;
	}

	public String getPreceedingNo() {
		return this.preceedingNo;
	}

	public void setPreceedingNo(String preceedingNo) {
		this.preceedingNo = preceedingNo;
	}

	public String getPreceedingTitle() {
		return this.preceedingTitle;
	}

	public void setPreceedingTitle(String preceedingTitle) {
		this.preceedingTitle = preceedingTitle;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Bank getBank() {
		return this.bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public Caste getCaste() {
		return this.caste;
	}

	public void setCaste(Caste caste) {
		this.caste = caste;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public FinancialYear getFinancialYear() {
		return this.financialYear;
	}

	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}

	public SchAccountsPreceeding getAccountsPreceeding() {
		return this.accountsPreceeding;
	}

	public void setAccountsPreceeding(SchAccountsPreceeding accountsPreceeding) {
		this.accountsPreceeding = accountsPreceeding;
	}

	public List<SchStdPreceeding> getStdPreceedings() {
		return this.stdPreceedings;
	}

	public void setStdPreceedings(List<SchStdPreceeding> stdPreceedings) {
		this.stdPreceedings = stdPreceedings;
	}

	public SchStdPreceeding addStdPreceeding(SchStdPreceeding stdPreceeding) {
		getStdPreceedings().add(stdPreceeding);
		stdPreceeding.setPreceeding(this);

		return stdPreceeding;
	}

	public SchStdPreceeding removeStdPreceeding(SchStdPreceeding stdPreceeding) {
		getStdPreceedings().remove(stdPreceeding);
		stdPreceeding.setPreceeding(null);

		return stdPreceeding;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public String getScholarshipType() {
		return scholarshipType;
	}

	public void setScholarshipType(String scholarshipType) {
		this.scholarshipType = scholarshipType;
	}

	
}