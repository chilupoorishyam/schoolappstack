package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_lib_library_details database table.
 * 
 */
@Entity
@Table(name="t_lib_library_details")
@NamedQuery(name="LibraryDetail.findAll", query="SELECT l FROM LibraryDetail l")
public class LibraryDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_library_id")
	private Long libraryId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="library_code")
	private String libraryCode;

	@Column(name="library_name")
	private String libraryName;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to Author
	@OneToMany(mappedBy="libraryDetail",fetch = FetchType.LAZY)
	private List<Author> authors;

	//bi-directional many-to-one association to BookDetail
	@OneToMany(mappedBy="libraryDetail",fetch = FetchType.LAZY)
	private List<BookDetail> bookDetails;

	//bi-directional many-to-one association to BookIssuedetail
	@OneToMany(mappedBy="libraryDetail",fetch = FetchType.LAZY)
	private List<BookIssuedetail> bookIssuedetails;

	//bi-directional many-to-one association to BookIssuedetailsHistory
	@OneToMany(mappedBy="libraryDetail",fetch = FetchType.LAZY)
	private List<BookIssuedetailsHistory> bookIssuedetailsHistories;

	//bi-directional many-to-one association to BookTag
	@OneToMany(mappedBy="libraryDetail",fetch = FetchType.LAZY)
	private List<BookTag> bookTags;

	//bi-directional many-to-one association to Book
	@OneToMany(mappedBy="libraryDetail",fetch = FetchType.LAZY)
	private List<Book> books;

	//bi-directional many-to-one association to Campus
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_campus_id")
	private Campus campus;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to Room
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_room_id")
	private Room room;

	//bi-directional many-to-one association to Publisher
	@OneToMany(mappedBy="libraryDetail",fetch = FetchType.LAZY)
	private List<Publisher> libPublishers;

	public LibraryDetail() {
	}

	public Long getLibraryId() {
		return this.libraryId;
	}

	public void setLibraryId(Long libraryId) {
		this.libraryId = libraryId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getLibraryCode() {
		return this.libraryCode;
	}

	public void setLibraryCode(String libraryCode) {
		this.libraryCode = libraryCode;
	}

	public String getLibraryName() {
		return this.libraryName;
	}

	public void setLibraryName(String libraryName) {
		this.libraryName = libraryName;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<Author> getAuthors() {
		return this.authors;
	}

	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}

	public Author addAuthor(Author author) {
		getAuthors().add(author);
		author.setLibraryDetail(this);

		return author;
	}

	public Author removeAuthor(Author author) {
		getAuthors().remove(author);
		author.setLibraryDetail(null);

		return author;
	}

	public List<BookDetail> getBookDetails() {
		return this.bookDetails;
	}

	public void setBookDetails(List<BookDetail> bookDetails) {
		this.bookDetails = bookDetails;
	}

	public BookDetail addBookDetail(BookDetail bookDetail) {
		getBookDetails().add(bookDetail);
		bookDetail.setLibraryDetail(this);

		return bookDetail;
	}

	public BookDetail removeBookDetail(BookDetail bookDetail) {
		getBookDetails().remove(bookDetail);
		bookDetail.setLibraryDetail(null);

		return bookDetail;
	}

	public List<BookIssuedetail> getBookIssuedetails() {
		return this.bookIssuedetails;
	}

	public void setBookIssuedetails(List<BookIssuedetail> bookIssuedetails) {
		this.bookIssuedetails = bookIssuedetails;
	}

	public BookIssuedetail addBookIssuedetail(BookIssuedetail bookIssuedetail) {
		getBookIssuedetails().add(bookIssuedetail);
		bookIssuedetail.setLibraryDetail(this);

		return bookIssuedetail;
	}

	public BookIssuedetail removeBookIssuedetail(BookIssuedetail bookIssuedetail) {
		getBookIssuedetails().remove(bookIssuedetail);
		bookIssuedetail.setLibraryDetail(null);

		return bookIssuedetail;
	}

	public List<BookIssuedetailsHistory> getBookIssuedetailsHistories() {
		return this.bookIssuedetailsHistories;
	}

	public void setBookIssuedetailsHistories(List<BookIssuedetailsHistory> bookIssuedetailsHistories) {
		this.bookIssuedetailsHistories = bookIssuedetailsHistories;
	}

	public BookIssuedetailsHistory addBookIssuedetailsHistory(BookIssuedetailsHistory bookIssuedetailsHistory) {
		getBookIssuedetailsHistories().add(bookIssuedetailsHistory);
		bookIssuedetailsHistory.setLibraryDetail(this);

		return bookIssuedetailsHistory;
	}

	public BookIssuedetailsHistory removeBookIssuedetailsHistory(BookIssuedetailsHistory bookIssuedetailsHistory) {
		getBookIssuedetailsHistories().remove(bookIssuedetailsHistory);
		bookIssuedetailsHistory.setLibraryDetail(null);

		return bookIssuedetailsHistory;
	}

	public List<BookTag> getBookTags() {
		return this.bookTags;
	}

	public void setBookTags(List<BookTag> bookTags) {
		this.bookTags = bookTags;
	}

	public BookTag addBookTag(BookTag bookTag) {
		getBookTags().add(bookTag);
		bookTag.setLibraryDetail(this);

		return bookTag;
	}

	public BookTag removeBookTag(BookTag bookTag) {
		getBookTags().remove(bookTag);
		bookTag.setLibraryDetail(null);

		return bookTag;
	}

	public List<Book> getBooks() {
		return this.books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public Book addBook(Book book) {
		getBooks().add(book);
		book.setLibraryDetail(this);

		return book;
	}

	public Book removeBook(Book book) {
		getBooks().remove(book);
		book.setLibraryDetail(null);

		return book;
	}

	public Campus getCampus() {
		return this.campus;
	}

	public void setCampus(Campus campus) {
		this.campus = campus;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Room getRoom() {
		return this.room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public List<Publisher> getLibPublishers() {
		return this.libPublishers;
	}

	public void setLibPublishers(List<Publisher> libPublishers) {
		this.libPublishers = libPublishers;
	}

	public Publisher addLibPublisher(Publisher libPublisher) {
		getLibPublishers().add(libPublisher);
		libPublisher.setLibraryDetail(this);

		return libPublisher;
	}

	public Publisher removeLibPublisher(Publisher libPublisher) {
		getLibPublishers().remove(libPublisher);
		libPublisher.setLibraryDetail(null);

		return libPublisher;
	}

}