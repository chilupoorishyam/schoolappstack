package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_fee_fine_slab database table.
 * 
 */
@Entity
@Table(name="t_fee_fine_slab")
@NamedQuery(name="FeeFineSlab.findAll", query="SELECT f FROM FeeFineSlab f")
public class FeeFineSlab implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_fine_slab_id")
	private Long fineSlabId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="days_after_due_date")
	private Long daysAfterDueDate;

	@Column(name="fine_mode")
	private String fineMode;

	@Column(name="fine_value")
	private Integer fineValue;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FeeFine
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_fine_id")
	private FeeFine feeFine;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	public FeeFineSlab() {
	}

	public Long getFineSlabId() {
		return this.fineSlabId;
	}

	public void setFineSlabId(Long fineSlabId) {
		this.fineSlabId = fineSlabId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Long getDaysAfterDueDate() {
		return this.daysAfterDueDate;
	}

	public void setDaysAfterDueDate(Long daysAfterDueDate) {
		this.daysAfterDueDate = daysAfterDueDate;
	}

	public String getFineMode() {
		return this.fineMode;
	}

	public void setFineMode(String fineMode) {
		this.fineMode = fineMode;
	}

	public Integer getFineValue() {
		return this.fineValue;
	}

	public void setFineValue(Integer fineValue) {
		this.fineValue = fineValue;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public FeeFine getFeeFine() {
		return this.feeFine;
	}

	public void setFeeFine(FeeFine feeFine) {
		this.feeFine = feeFine;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

}