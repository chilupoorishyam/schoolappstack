package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_dl_course_member_topic_logs database table.
 * 
 */
@Entity
@Table(name = "t_dl_course_member_topic_logs")
@NamedQuery(name = "CourseMemberTopicLog.findAll", query = "SELECT c FROM CourseMemberTopicLog c")
public class CourseMemberTopicLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_dl_member_topic_log_id")
	private Long memberTopicLogId;

	@Column(name = "completed_percentage")
	private Integer completedPercentage;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_completed")
	private Boolean isCompleted;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_date")
	private Date startDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to CourseMember
	@ManyToOne
	@JoinColumn(name = "fk_dl_course_member_id")
	private CourseMember courseMember;

	// bi-directional many-to-one association to TDlMember
	@ManyToOne
	@JoinColumn(name = "fk_dl_membership_id")
	private DigitalLibraryMember member;

	// bi-directional many-to-one association to OnlineCourses
	@ManyToOne
	@JoinColumn(name = "fk_dl_onlinecourse_id")
	private OnlineCourses onlineCourses;

	// bi-directional many-to-one association to CourseTopicDetail
	@ManyToOne
	@JoinColumn(name = "fk_dl_course_topic_det_id")
	private CourseTopicDetail courseTopicDetail;

	public CourseMemberTopicLog() {
	}

	public Long getMemberTopicLogId() {
		return this.memberTopicLogId;
	}

	public void setMemberTopicLogId(Long memberTopicLogId) {
		this.memberTopicLogId = memberTopicLogId;
	}

	public Integer getCompletedPercentage() {
		return this.completedPercentage;
	}

	public void setCompletedPercentage(Integer completedPercentage) {
		this.completedPercentage = completedPercentage;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsCompleted() {
		return this.isCompleted;
	}

	public void setIsCompleted(Boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public CourseMember getCourseMember() {
		return courseMember;
	}

	public void setCourseMember(CourseMember courseMember) {
		this.courseMember = courseMember;
	}

	public DigitalLibraryMember getMember() {
		return member;
	}

	public void setMember(DigitalLibraryMember member) {
		this.member = member;
	}

	public CourseTopicDetail getCourseTopicDetail() {
		return courseTopicDetail;
	}

	public void setCourseTopicDetail(CourseTopicDetail courseTopicDetail) {
		this.courseTopicDetail = courseTopicDetail;
	}

	public OnlineCourses getOnlineCourses() {
		return onlineCourses;
	}

	public void setOnlineCourses(OnlineCourses onlineCourses) {
		this.onlineCourses = onlineCourses;
	}

}