package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_cm_subjectregulation database table.
 * 
 */
@Entity
@Table(name="t_cm_subjectregulation")
@NamedQuery(name="Subjectregulation.findAll", query="SELECT s FROM Subjectregulation s")
public class Subjectregulation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_subreg_id")
	private Long subjectRegulationId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Column(name="subject_order")
	private Integer subjectOrder;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;
	
	@Column(name="Syllabus_Code")
	private String syllabusCode;
	
	@Column(name="Reference_Code")
	private String referenceCode;
	
	@Column(name="Internal_Exam_Code")
	private String internalExamCode;
	
	@Column(name="External_Exam_Code")
	private String externalExamCode;
	
	@Column(name="Internal_Exam_Marks")
	private Integer internalExamMarks;
	
	@Column(name="External_Exam_Marks")
	private Integer externalExamMarks;
	
	@Column(name="Credits")
	private Integer credits;
	
	//bi-directional many-to-one association to SubjectCourseyear
	@OneToMany(mappedBy="subjectregulation",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	private List<SubjectCourseyear> subjectCourseyears;

	//bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;



	//bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_year_id")
	private CourseYear courseYear;

	//bi-directional many-to-one association to Electivetype
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_electivetype_id")
	private Electivetype electivetype;


	//bi-directional many-to-one association to Subject
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subject_id")
	private Subject subject;
	
	@OneToMany(mappedBy="subjectregulation",fetch = FetchType.LAZY)
	private List<SubjectUnit> subjectUnits;
	
	@Column(name = "is_include_in_std_reg")
	private Boolean isIncludeInStdReg;
	
	@Column(name = "syllabus_copy_url")
	private String syllabusCopyURL;
	
	public Subjectregulation() {
	}

	public Long getSubjectRegulationId() {
		return this.subjectRegulationId;
	}

	public void setSubjectRegulationId(Long subjectRegulationId) {
		this.subjectRegulationId = subjectRegulationId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getSubjectOrder() {
		return this.subjectOrder;
	}

	public void setSubjectOrder(Integer subjectOrder) {
		this.subjectOrder = subjectOrder;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<SubjectCourseyear> getSubjectCourseyears() {
		return this.subjectCourseyears;
	}

	public void setSubjectCourseyears(List<SubjectCourseyear> subjectCourseyears) {
		this.subjectCourseyears = subjectCourseyears;
	}

	public SubjectCourseyear addSubjectCourseyear(SubjectCourseyear subjectCourseyear) {
		getSubjectCourseyears().add(subjectCourseyear);
		subjectCourseyear.setSubjectregulation(this);

		return subjectCourseyear;
	}

	public SubjectCourseyear removeSubjectCourseyear(SubjectCourseyear subjectCourseyear) {
		getSubjectCourseyears().remove(subjectCourseyear);
		subjectCourseyear.setSubjectregulation(null);

		return subjectCourseyear;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}



	public CourseYear getCourseYear() {
		return this.courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}

	public Electivetype getElectivetype() {
		return this.electivetype;
	}

	public void setElectivetype(Electivetype electivetype) {
		this.electivetype = electivetype;
	}


	public Subject getSubject() {
		return this.subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public List<SubjectUnit> getSubjectUnits() {
		return subjectUnits;
	}

	public void setSubjectUnits(List<SubjectUnit> subjectUnits) {
		this.subjectUnits = subjectUnits;
	}

	public String getSyllabusCode() {
		return syllabusCode;
	}

	public void setSyllabusCode(String syllabusCode) {
		this.syllabusCode = syllabusCode;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public String getInternalExamCode() {
		return internalExamCode;
	}

	public void setInternalExamCode(String internalExamCode) {
		this.internalExamCode = internalExamCode;
	}

	public String getExternalExamCode() {
		return externalExamCode;
	}

	public void setExternalExamCode(String externalExamCode) {
		this.externalExamCode = externalExamCode;
	}

	public Integer getInternalExamMarks() {
		return internalExamMarks;
	}

	public void setInternalExamMarks(Integer internalExamMarks) {
		this.internalExamMarks = internalExamMarks;
	}

	public Integer getExternalExamMarks() {
		return externalExamMarks;
	}

	public void setExternalExamMarks(Integer externalExamMarks) {
		this.externalExamMarks = externalExamMarks;
	}

	public Integer getCredits() {
		return credits;
	}

	public void setCredits(Integer credits) {
		this.credits = credits;
	}

	public Boolean getIsIncludeInStdReg() {
		return isIncludeInStdReg;
	}

	public void setIsIncludeInStdReg(Boolean isIncludeInStdReg) {
		this.isIncludeInStdReg = isIncludeInStdReg;
	}

	public String getSyllabusCopyURL() {
		return syllabusCopyURL;
	}

	public void setSyllabusCopyURL(String syllabusCopyURL) {
		this.syllabusCopyURL = syllabusCopyURL;
	}
	
}