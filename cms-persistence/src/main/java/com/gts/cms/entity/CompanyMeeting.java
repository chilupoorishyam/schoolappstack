package com.gts.cms.entity;

import java.io.Serializable;
import java.sql.Time;

import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_pa_company_meetings database table.
 * 
 */
@Entity
@Table(name = "t_pa_company_meetings")
@NamedQuery(name = "CompanyMeeting.findAll", query = "SELECT c FROM CompanyMeeting c")
public class CompanyMeeting implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_company_meeting_id")
	private Long companyMeetingId;

	@Column(name = "attendees_names")
	private String attendeesNames;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_company_contact_id")
	private CompanyContact companyContact;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_meetingtype_catdet_id")
	private GeneralDetail meetingtypeCatdet;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_po_emp_id")
	private EmployeeDetail poEmp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "followup_meeting_on")
	private Date followupMeetingOn;

	@Column(name = "followup_points")
	private String followupPoints;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "meeting_description")
	private String meetingDescription;

	@Column(name = "meeting_from_time")
	private Time meetingFromTime;

	@Temporal(TemporalType.DATE)
	@Column(name = "meeting_on")
	private Date meetingOn;

	@Column(name = "meeting_output")
	private String meetingOutput;

	@Column(name = "meeting_title")
	private String meetingTitle;

	@Column(name = "meeting_to_time")
	private Time meetingToTime;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	public Long getCompanyMeetingId() {
		return companyMeetingId;
	}

	public void setCompanyMeetingId(Long companyMeetingId) {
		this.companyMeetingId = companyMeetingId;
	}

	public String getAttendeesNames() {
		return attendeesNames;
	}

	public void setAttendeesNames(String attendeesNames) {
		this.attendeesNames = attendeesNames;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public CompanyContact getCompanyContact() {
		return companyContact;
	}

	public void setCompanyContact(CompanyContact companyContact) {
		this.companyContact = companyContact;
	}

	public GeneralDetail getMeetingtypeCatdet() {
		return meetingtypeCatdet;
	}

	public void setMeetingtypeCatdet(GeneralDetail meetingtypeCatdet) {
		this.meetingtypeCatdet = meetingtypeCatdet;
	}

	public EmployeeDetail getPoEmp() {
		return poEmp;
	}

	public void setPoEmp(EmployeeDetail poEmp) {
		this.poEmp = poEmp;
	}

	public Date getFollowupMeetingOn() {
		return followupMeetingOn;
	}

	public void setFollowupMeetingOn(Date followupMeetingOn) {
		this.followupMeetingOn = followupMeetingOn;
	}

	public String getFollowupPoints() {
		return followupPoints;
	}

	public void setFollowupPoints(String followupPoints) {
		this.followupPoints = followupPoints;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getMeetingDescription() {
		return meetingDescription;
	}

	public void setMeetingDescription(String meetingDescription) {
		this.meetingDescription = meetingDescription;
	}

	public Time getMeetingFromTime() {
		return meetingFromTime;
	}

	public void setMeetingFromTime(Time meetingFromTime) {
		this.meetingFromTime = meetingFromTime;
	}

	public Date getMeetingOn() {
		return meetingOn;
	}

	public void setMeetingOn(Date meetingOn) {
		this.meetingOn = meetingOn;
	}

	public String getMeetingOutput() {
		return meetingOutput;
	}

	public void setMeetingOutput(String meetingOutput) {
		this.meetingOutput = meetingOutput;
	}

	public String getMeetingTitle() {
		return meetingTitle;
	}

	public void setMeetingTitle(String meetingTitle) {
		this.meetingTitle = meetingTitle;
	}

	public Time getMeetingToTime() {
		return meetingToTime;
	}

	public void setMeetingToTime(Time meetingToTime) {
		this.meetingToTime = meetingToTime;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}
}