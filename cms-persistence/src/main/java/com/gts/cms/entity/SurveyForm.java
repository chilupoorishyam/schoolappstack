package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_hr_survey_form database table.
 * 
 */
@Entity
@Table(name="t_hr_survey_form")
@NamedQuery(name="SurveyForm.findAll", query="SELECT s FROM SurveyForm s")
public class SurveyForm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_survey_form_id")
	private Long surveyFormId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private String footerinfo;

	private String footerinfo1;

	private String headerinfo;

	private String headerinfo1;

	private String instructions;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_html")
	private Boolean isHtml;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="survey_end_date")
	private Date surveyEndDate;

	@Column(name="survey_name")
	private String surveyName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="survey_start_date")
	private Date surveyStartDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to SurveyDetail
	@OneToMany(mappedBy="surveyForm",fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	private List<SurveyDetail> surveyDetails;

	//bi-directional many-to-one association to SurveyFeedback
	@OneToMany(mappedBy="surveyForm",fetch = FetchType.LAZY)
	private List<SurveyFeedback> surveyFeedbacks;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fbfrom_catdetid")
	private GeneralDetail fbfrom;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fbfor_catdetid")
	private GeneralDetail fbfor;

	public SurveyForm() {
	}

	public Long getSurveyFormId() {
		return this.surveyFormId;
	}

	public void setSurveyFormId(Long surveyFormId) {
		this.surveyFormId = surveyFormId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getFooterinfo() {
		return this.footerinfo;
	}

	public void setFooterinfo(String footerinfo) {
		this.footerinfo = footerinfo;
	}

	public String getFooterinfo1() {
		return this.footerinfo1;
	}

	public void setFooterinfo1(String footerinfo1) {
		this.footerinfo1 = footerinfo1;
	}

	public String getHeaderinfo() {
		return this.headerinfo;
	}

	public void setHeaderinfo(String headerinfo) {
		this.headerinfo = headerinfo;
	}

	public String getHeaderinfo1() {
		return this.headerinfo1;
	}

	public void setHeaderinfo1(String headerinfo1) {
		this.headerinfo1 = headerinfo1;
	}

	public String getInstructions() {
		return this.instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsHtml() {
		return this.isHtml;
	}

	public void setIsHtml(Boolean isHtml) {
		this.isHtml = isHtml;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getSurveyEndDate() {
		return this.surveyEndDate;
	}

	public void setSurveyEndDate(Date surveyEndDate) {
		this.surveyEndDate = surveyEndDate;
	}

	public String getSurveyName() {
		return this.surveyName;
	}

	public void setSurveyName(String surveyName) {
		this.surveyName = surveyName;
	}

	public Date getSurveyStartDate() {
		return this.surveyStartDate;
	}

	public void setSurveyStartDate(Date surveyStartDate) {
		this.surveyStartDate = surveyStartDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<SurveyDetail> getSurveyDetails() {
		return this.surveyDetails;
	}

	public void setSurveyDetails(List<SurveyDetail> surveyDetails) {
		this.surveyDetails = surveyDetails;
	}

	public SurveyDetail addSurveyDetail(SurveyDetail surveyDetail) {
		getSurveyDetails().add(surveyDetail);
		surveyDetail.setSurveyForm(this);

		return surveyDetail;
	}

	public SurveyDetail removeSurveyDetail(SurveyDetail surveyDetail) {
		getSurveyDetails().remove(surveyDetail);
		surveyDetail.setSurveyForm(null);

		return surveyDetail;
	}

	public List<SurveyFeedback> getSurveyFeedbacks() {
		return this.surveyFeedbacks;
	}

	public void setSurveyFeedbacks(List<SurveyFeedback> surveyFeedbacks) {
		this.surveyFeedbacks = surveyFeedbacks;
	}

	public SurveyFeedback addSurveyFeedback(SurveyFeedback surveyFeedback) {
		getSurveyFeedbacks().add(surveyFeedback);
		surveyFeedback.setSurveyForm(this);

		return surveyFeedback;
	}

	public SurveyFeedback removeSurveyFeedback(SurveyFeedback surveyFeedback) {
		getSurveyFeedbacks().remove(surveyFeedback);
		surveyFeedback.setSurveyForm(null);

		return surveyFeedback;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getFbfrom() {
		return this.fbfrom;
	}

	public void setFbfrom(GeneralDetail fbfrom) {
		this.fbfrom = fbfrom;
	}

	public GeneralDetail getFbfor() {
		return this.fbfor;
	}

	public void setFbfor(GeneralDetail fbfor) {
		this.fbfor = fbfor;
	}

}