package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_hr_leave_application database table.
 * 
 */
@Entity
@Table(name="t_hr_leave_application")
public class LeaveApplication implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_leave_appliction_id")
	private Long leaveApplictionId;

	@Temporal(TemporalType.DATE)
	@Column(name="application_date")
	private Date applicationDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="leave_description")
	private String leaveDescription;

	@Temporal(TemporalType.DATE)
	@Column(name="leave_from_date")
	private Date leaveFromDate;

	@Temporal(TemporalType.DATE)
	@Column(name="leave_to_date")
	private Date leaveToDate;

	@Column(name="no_of_leaves")
	private BigDecimal noOfLeaves;

	@Column(name="balance_leaves")
	private BigDecimal balanceLeaves;
	
	@Column(name="leave_year")
	private Integer leaveYear;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;
	

	private String isForenoonAfternoon;  
	
	//bi-directional many-to-one association to EmpMedicalCertificate
	@OneToMany(mappedBy="leaveApplication",fetch = FetchType.LAZY)
	private List<EmployeeMedicalCertificate> empMedicalCertificates;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_id")
	private EmployeeDetail employeeDetail;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="fk_assigned_emp_id")
	private EmployeeDetail assignedEmployeeDetail;

	//bi-directional many-to-one association to LeaveType
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_leave_type_id")
	private LeaveType leaveType;

	//bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne
	@JoinColumn(name="fk_leaveprocess_status_catdet_id")
	private GeneralDetail leaveprocessStatus;

	//bi-directional many-to-one association to LeaveApplicationStatus
	@OneToMany(mappedBy="leaveApplication",fetch = FetchType.LAZY)
	private List<LeaveApplicationStatus> leaveApplicationStatuses;

	public LeaveApplication() {
	}

	public Long getLeaveApplictionId() {
		return this.leaveApplictionId;
	}

	public void setLeaveApplictionId(Long leaveApplictionId) {
		this.leaveApplictionId = leaveApplictionId;
	}

	public Date getApplicationDate() {
		return this.applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getLeaveDescription() {
		return this.leaveDescription;
	}

	public void setLeaveDescription(String leaveDescription) {
		this.leaveDescription = leaveDescription;
	}

	public Date getLeaveFromDate() {
		return this.leaveFromDate;
	}

	public void setLeaveFromDate(Date leaveFromDate) {
		this.leaveFromDate = leaveFromDate;
	}

	public Date getLeaveToDate() {
		return this.leaveToDate;
	}

	public void setLeaveToDate(Date leaveToDate) {
		this.leaveToDate = leaveToDate;
	}



	public BigDecimal getNoOfLeaves() {
		return noOfLeaves;
	}

	public void setNoOfLeaves(BigDecimal noOfLeaves) {
		this.noOfLeaves = noOfLeaves;
	}

	public BigDecimal getBalanceLeaves() {
		return balanceLeaves;
	}

	public void setBalanceLeaves(BigDecimal balanceLeaves) {
		this.balanceLeaves = balanceLeaves;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<EmployeeMedicalCertificate> getEmpMedicalCertificates() {
		return this.empMedicalCertificates;
	}

	public void setEmpMedicalCertificates(List<EmployeeMedicalCertificate> empMedicalCertificates) {
		this.empMedicalCertificates = empMedicalCertificates;
	}

	public EmployeeMedicalCertificate addEmpMedicalCertificate(EmployeeMedicalCertificate empMedicalCertificate) {
		getEmpMedicalCertificates().add(empMedicalCertificate);
		empMedicalCertificate.setLeaveApplication(this);

		return empMedicalCertificate;
	}

	public EmployeeMedicalCertificate removeEmpMedicalCertificate(EmployeeMedicalCertificate empMedicalCertificate) {
		getEmpMedicalCertificates().remove(empMedicalCertificate);
		empMedicalCertificate.setLeaveApplication(null);

		return empMedicalCertificate;
	}

	public EmployeeDetail getEmployeeDetail() {
		return this.employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public EmployeeDetail getAssignedEmployeeDetail() {
		return this.assignedEmployeeDetail;
	}

	public void setAssignedEmployeeDetail(EmployeeDetail assignedEmployeeDetail) {
		this.assignedEmployeeDetail = assignedEmployeeDetail;
	}

	public LeaveType getLeaveType() {
		return this.leaveType;
	}

	public void setLeaveType(LeaveType leaveType) {
		this.leaveType = leaveType;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getLeaveprocessStatus() {
		return this.leaveprocessStatus;
	}

	public void setLeaveprocessStatus(GeneralDetail leaveprocessStatus) {
		this.leaveprocessStatus = leaveprocessStatus;
	}

	public List<LeaveApplicationStatus> getLeaveApplicationStatuses() {
		return this.leaveApplicationStatuses;
	}

	public void setLeaveApplicationStatuses(List<LeaveApplicationStatus> leaveApplicationStatuses) {
		this.leaveApplicationStatuses = leaveApplicationStatuses;
	}

	public LeaveApplicationStatus addLeaveApplicationStatus(LeaveApplicationStatus leaveApplicationStatus) {
		getLeaveApplicationStatuses().add(leaveApplicationStatus);
		leaveApplicationStatus.setLeaveApplication(this);

		return leaveApplicationStatus;
	}

	public LeaveApplicationStatus removeLeaveApplicationStatus(LeaveApplicationStatus leaveApplicationStatus) {
		getLeaveApplicationStatuses().remove(leaveApplicationStatus);
		leaveApplicationStatus.setLeaveApplication(null);

		return leaveApplicationStatus;
	}

	public String getIsForenoonAfternoon() {
		return isForenoonAfternoon;
	}

	public void setIsForenoonAfternoon(String isForenoonAfternoon) {
		this.isForenoonAfternoon = isForenoonAfternoon;
	}
	public Integer getLeaveYear() {
		return leaveYear;
	}

	public void setLeaveYear(Integer leaveYear) {
		this.leaveYear = leaveYear;
	}
	
	

}