package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_inv_itemcategory database table.
 * 
 */
@Entity
@Table(name="t_inv_itemcategory")
@NamedQuery(name="InvItemcategory.findAll", query="SELECT i FROM InvItemcategory i")
public class InvItemcategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_item_category_id")
	private Long itemCategoryId;

	@Column(name="category_code")
	private String categoryCode;

	@Column(name="category_name")
	private String categoryName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;
	
	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;
/*
	//bi-directional many-to-one association to InvItemmaster
	@OneToMany(mappedBy="InvItemcategory")
	private List<InvItemmaster> invItemmasters;

	//bi-directional many-to-one association to InvItemsubcategory
	@OneToMany(mappedBy="InvItemcategory")
	private List<InvItemsubcategory> invItemsubcategories;*/

	public InvItemcategory() {
	}

	public Long getItemCategoryId() {
		return this.itemCategoryId;
	}

	public void setItemCategoryId(Long itemCategoryId) {
		this.itemCategoryId = itemCategoryId;
	}

	public String getCategoryCode() {
		return this.categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getCategoryName() {
		return this.categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	
	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	/*public List<InvItemmaster> getInvItemmasters() {
		return invItemmasters;
	}

	public void setInvItemmasters(List<InvItemmaster> invItemmasters) {
		this.invItemmasters = invItemmasters;
	}

	public List<InvItemsubcategory> getInvItemsubcategories() {
		return invItemsubcategories;
	}

	public void setInvItemsubcategories(List<InvItemsubcategory> invItemsubcategories) {
		this.invItemsubcategories = invItemsubcategories;
	}*/


}