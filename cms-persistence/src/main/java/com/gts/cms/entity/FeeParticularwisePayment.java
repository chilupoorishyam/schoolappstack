package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the t_fee_particularwise_payments database table.
 * 
 */
@Entity
@Table(name="t_fee_particularwise_payments")
@NamedQuery(name="FeeParticularwisePayment.findAll", query="SELECT f FROM FeeParticularwisePayment f")
public class FeeParticularwisePayment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_particularwise_payment_id")
	private Long feeParticularwisePaymentId;

	private BigDecimal amount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="discount_amount")
	private BigDecimal discountAmount;

	@Column(name="fine_amount")
	private BigDecimal fineAmount;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="paid_amount")
	private BigDecimal paidAmount;

	@Column(name="payer_name")
	private String payerName;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_id")
	private EmployeeDetail employeeDetail;

	//bi-directional many-to-one association to FeeParticular
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="fk_fee_perticulars_id")
	private FeeParticular feeParticular;

	//bi-directional many-to-one association to FeeStructure
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_structure_id")
	private FeeStructure feeStructure;

	//bi-directional many-to-one association to FeeStructureParticular
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_structure_particular_id")
	private FeeStructureParticular feeStructureParticular;

	//bi-directional many-to-one association to FeeStudentWiseParticular
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_std_particular_id")
	private FeeStudentWiseParticular feeStudentWiseParticular;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_student_id")
	private StudentDetail studentDetail;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_financial_year_id")
	private FinancialYear financialYear;

	//bi-directional many-to-one association to FeeReceipt
	/*@OneToMany(mappedBy="feeParticularwisePayment")
	private List<FeeReceipt> feeReceipts;*/
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="fk_fee_receipts_id")
	private FeeReceipt feeReceipt;

	
	@Column(name="refund_amount")
	private BigDecimal refundAmount;

	@Column(name="scholarship_amount")
	private BigDecimal scholarshipAmount;
	
	@Column(name="net_amount")
	private BigDecimal netAmount;
	
	@Column(name="balance_amount")
	private BigDecimal balanceAmount;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_std_data_particulars_id")
	private FeeStudentDataParticular feeStudentDataParticular;
	
	public FeeParticularwisePayment() {
	}

	public Long getFeeParticularwisePaymentId() {
		return this.feeParticularwisePaymentId;
	}

	public void setFeeParticularwisePaymentId(Long feeParticularwisePaymentId) {
		this.feeParticularwisePaymentId = feeParticularwisePaymentId;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public BigDecimal getDiscountAmount() {
		return this.discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public BigDecimal getFineAmount() {
		return this.fineAmount;
	}

	public void setFineAmount(BigDecimal fineAmount) {
		this.fineAmount = fineAmount;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public BigDecimal getPaidAmount() {
		return this.paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getPayerName() {
		return this.payerName;
	}

	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getEmployeeDetail() {
		return this.employeeDetail;
	}

	public void setEmployeeDetail(EmployeeDetail employeeDetail) {
		this.employeeDetail = employeeDetail;
	}

	public FeeParticular getFeeParticular() {
		return this.feeParticular;
	}

	public void setFeeParticular(FeeParticular feeParticular) {
		this.feeParticular = feeParticular;
	}

	public FeeStructure getFeeStructure() {
		return this.feeStructure;
	}

	public void setFeeStructure(FeeStructure feeStructure) {
		this.feeStructure = feeStructure;
	}

	public FeeStructureParticular getFeeStructureParticular() {
		return this.feeStructureParticular;
	}

	public void setFeeStructureParticular(FeeStructureParticular feeStructureParticular) {
		this.feeStructureParticular = feeStructureParticular;
	}

	public FeeStudentWiseParticular getFeeStudentWiseParticular() {
		return this.feeStudentWiseParticular;
	}

	public void setFeeStudentWiseParticular(FeeStudentWiseParticular feeStudentWiseParticular) {
		this.feeStudentWiseParticular = feeStudentWiseParticular;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public StudentDetail getStudentDetail() {
		return this.studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public FeeReceipt getFeeReceipt() {
		return feeReceipt;
	}

	public void setFeeReceipt(FeeReceipt feeReceipt) {
		this.feeReceipt = feeReceipt;
	}

	public FinancialYear getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}

	public BigDecimal getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(BigDecimal refundAmount) {
		this.refundAmount = refundAmount;
	}

	public BigDecimal getScholarshipAmount() {
		return scholarshipAmount;
	}

	public void setScholarshipAmount(BigDecimal scholarshipAmount) {
		this.scholarshipAmount = scholarshipAmount;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(BigDecimal balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public FeeStudentDataParticular getFeeStudentDataParticular() {
		return feeStudentDataParticular;
	}

	public void setFeeStudentDataParticular(FeeStudentDataParticular feeStudentDataParticular) {
		this.feeStudentDataParticular = feeStudentDataParticular;
	}
	
}