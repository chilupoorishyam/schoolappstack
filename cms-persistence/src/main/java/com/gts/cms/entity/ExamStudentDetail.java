package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_exam_student_details database table.
 * 
 */
@Data
@Entity
@Table(name = "t_exam_student_details")
@NamedQuery(name = "ExamStudentDetail.findAll", query = "SELECT e FROM ExamStudentDetail e")
public class ExamStudentDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_exam_std_det_id")
	private Long examStdDetId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_marks_entered_emp_id")
	private EmployeeDetail marksEnteredEmp;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_reevaluation_entered_emp_id")
	private EmployeeDetail reEvaluationEnteredEmp;
	
	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_subject_id")
	private Subject subject;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_marks_published")
	private Boolean isMarksPublished;

	@Column(name = "is_pass")
	private Boolean isPass;

	@Column(name = "is_present")
	private Boolean isPresent;

	@Column(name = "is_reevaluation_applied")
	private Boolean isReevaluationApplied;

	@Column(name = "is_reevaluation_marks_published")
	private Boolean isReevaluationMarksPublished;

	private Integer marks;

	@Column(name = "marks_comments")
	private String marksComments;

	private String reason;

	@Column(name = "reevaluation_marks")
	private Integer reevaluationMarks;

	@Column(name = "reevaluation_marks_comments")
	private String reevaluationMarksComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to ExamStudent
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_std_id")
	private ExamStudent examStudent;


}