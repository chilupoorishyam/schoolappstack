package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_studentbatches database table.
 * 
 */
@Entity
@Table(name="t_m_studentbatches")
@NamedQuery(name="Studentbatch.findAll", query="SELECT s FROM Studentbatch s")
public class Studentbatch implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_stdbatch_id")
	private Long studentbatchId;

	@Column(name="batch_name")
	private String batchName;

	private Integer capacity;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="sort_order")
	private BigInteger sortOrder;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to BatchwiseStudent
	@OneToMany(mappedBy="studentbatch",fetch = FetchType.LAZY)
	private List<BatchwiseStudent> batchwiseStudents;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to Course
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_course_id")
	private Course course;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subjecttype_catdet_id")
	private GeneralDetail subjecttype;

	/*
	 * //bi-directional many-to-one association to StudentSubject
	 * 
	 * @OneToMany(mappedBy="studentbatch",fetch = FetchType.LAZY) private
	 * List<StudentSubject> studentSubjects;
	 */

	//bi-directional many-to-one association to ActualClassesSchedule
	@OneToMany(mappedBy="studentbatch",fetch = FetchType.LAZY)
	private List<ActualClassesSchedule> actualClassesSchedules;

	//bi-directional many-to-one association to StaffProxy
	@OneToMany(mappedBy="studentbatch",fetch = FetchType.LAZY)
	private List<StaffProxy> staffProxies;

	//bi-directional many-to-one association to SubjectResource
	@OneToMany(mappedBy="studentbatch",fetch = FetchType.LAZY)
	private List<SubjectResource> subjectResources;

	public Studentbatch() {
	}

	public Long getStudentbatchId() {
		return this.studentbatchId;
	}

	public void setStudentbatchId(Long studentbatchId) {
		this.studentbatchId = studentbatchId;
	}

	public String getBatchName() {
		return this.batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public Integer getCapacity() {
		return this.capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<BatchwiseStudent> getBatchwiseStudents() {
		return this.batchwiseStudents;
	}

	public void setBatchwiseStudents(List<BatchwiseStudent> batchwiseStudents) {
		this.batchwiseStudents = batchwiseStudents;
	}

	public BatchwiseStudent addBatchwiseStudent(BatchwiseStudent batchwiseStudent) {
		getBatchwiseStudents().add(batchwiseStudent);
		batchwiseStudent.setStudentbatch(this);

		return batchwiseStudent;
	}

	public BatchwiseStudent removeBatchwiseStudent(BatchwiseStudent batchwiseStudent) {
		getBatchwiseStudents().remove(batchwiseStudent);
		batchwiseStudent.setStudentbatch(null);

		return batchwiseStudent;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Course getCourse() {
		return this.course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public GeneralDetail getSubjecttype() {
		return this.subjecttype;
	}

	public void setSubjecttype(GeneralDetail subjecttype) {
		this.subjecttype = subjecttype;
	}

	/*
	 * public List<StudentSubject> getStudentSubjects() { return
	 * this.studentSubjects; }
	 * 
	 * public void setStudentSubjects(List<StudentSubject> studentSubjects) {
	 * this.studentSubjects = studentSubjects; }
	 */

	/*
	 * public StudentSubject addStudentSubject(StudentSubject studentSubject) {
	 * getStudentSubjects().add(studentSubject);
	 * studentSubject.setStudentbatch(this);
	 * 
	 * return studentSubject; }
	 * 
	 * public StudentSubject removeStudentSubject(StudentSubject studentSubject) {
	 * getStudentSubjects().remove(studentSubject);
	 * studentSubject.setStudentbatch(null);
	 * 
	 * return studentSubject; }
	 */

	public List<ActualClassesSchedule> getActualClassesSchedules() {
		return this.actualClassesSchedules;
	}

	public void setActualClassesSchedules(List<ActualClassesSchedule> actualClassesSchedules) {
		this.actualClassesSchedules = actualClassesSchedules;
	}

	public ActualClassesSchedule addActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		getActualClassesSchedules().add(actualClassesSchedule);
		actualClassesSchedule.setStudentbatch(this);

		return actualClassesSchedule;
	}

	public ActualClassesSchedule removeActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		getActualClassesSchedules().remove(actualClassesSchedule);
		actualClassesSchedule.setStudentbatch(null);

		return actualClassesSchedule;
	}

	public List<StaffProxy> getStaffProxies() {
		return this.staffProxies;
	}

	public void setStaffProxies(List<StaffProxy> staffProxies) {
		this.staffProxies = staffProxies;
	}

	public StaffProxy addStaffProxy(StaffProxy staffProxy) {
		getStaffProxies().add(staffProxy);
		staffProxy.setStudentbatch(this);

		return staffProxy;
	}

	public StaffProxy removeStaffProxy(StaffProxy staffProxy) {
		getStaffProxies().remove(staffProxy);
		staffProxy.setStudentbatch(null);

		return staffProxy;
	}

	public List<SubjectResource> getSubjectResources() {
		return this.subjectResources;
	}

	public void setSubjectResources(List<SubjectResource> subjectResources) {
		this.subjectResources = subjectResources;
	}

	public SubjectResource addSubjectResource(SubjectResource subjectResource) {
		getSubjectResources().add(subjectResource);
		subjectResource.setStudentbatch(this);

		return subjectResource;
	}

	public SubjectResource removeSubjectResource(SubjectResource subjectResource) {
		getSubjectResources().remove(subjectResource);
		subjectResource.setStudentbatch(null);

		return subjectResource;
	}

	public BigInteger getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(BigInteger sortOrder) {
		this.sortOrder = sortOrder;
	}

	
}