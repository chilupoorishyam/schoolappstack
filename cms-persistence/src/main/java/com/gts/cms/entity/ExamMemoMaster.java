package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_exam_memo_master database table.
 * 
 */
@Entity
@Table(name = "t_exam_memo_master")
@NamedQuery(name = "ExamMemoMaster.findAll", query = "SELECT e FROM ExamMemoMaster e")
public class ExamMemoMaster implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_exam_memo_id")
	private Long examMemoId;
	
	@Temporal(TemporalType.DATE)
	@Column(name="dateOfIssue")
	private Date dateOfIssue;
	
	@Temporal(TemporalType.DATE)
	@Column(name="dateOfPrint")
	private Date dateOfPrint;
	
	private Boolean isActive;
	private Date memoDate;
	private String memoNo;
	private String memoSerialNo;
	private String reason;
	
	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;
	
	// bi-directional many-to-one association to ExamMaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_exam_id")
	private ExamMaster examMaster;
	
	// bi-directional many-to-one association to CourseYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_course_year_id")
	private CourseYear courseYear;
	
	// bi-directional many-to-one association to ExamMaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_examtype_catdet_id")
	private GeneralDetail examtypeCatdet;
	
	// bi-directional many-to-one association to StudentDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_student_id")
	private StudentDetail studentDetail;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;
	
	@Column(name="registered")
	private Integer registered;
	
	@Column(name="appeared")
	private Integer appeared;
	
	@Column(name="passed")
	private Integer passed;
	
	@Column(name="sgpa")
	private BigDecimal sgpa;
	
	@Column(name="cgpa")
	private BigDecimal cgpa;
	
	

		
	// bi-directional many-to-one association to ExamStudentDetail
	@OneToMany(mappedBy = "examMemoMaster", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ExamStudentMemoSubject> examStudentMemoSubjects;

	public Long getExamMemoId() {
		return examMemoId;
	}

	public void setExamMemoId(Long examMemoId) {
		this.examMemoId = examMemoId;
	}

	public Date getDateOfIssue() {
		return dateOfIssue;
	}

	public void setDateOfIssue(Date dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public Date getDateOfPrint() {
		return dateOfPrint;
	}

	public void setDateOfPrint(Date dateOfPrint) {
		this.dateOfPrint = dateOfPrint;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Date getMemoDate() {
		return memoDate;
	}

	public void setMemoDate(Date memoDate) {
		this.memoDate = memoDate;
	}

	public String getMemoNo() {
		return memoNo;
	}

	public void setMemoNo(String memoNo) {
		this.memoNo = memoNo;
	}

	public String getMemoSerialNo() {
		return memoSerialNo;
	}

	public void setMemoSerialNo(String memoSerialNo) {
		this.memoSerialNo = memoSerialNo;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public ExamMaster getExamMaster() {
		return examMaster;
	}

	public void setExamMaster(ExamMaster examMaster) {
		this.examMaster = examMaster;
	}

	public CourseYear getCourseYear() {
		return courseYear;
	}

	public void setCourseYear(CourseYear courseYear) {
		this.courseYear = courseYear;
	}
	
	public GeneralDetail getExamtypeCatdet() {
		return examtypeCatdet;
	}

	public void setExamtypeCatdet(GeneralDetail examtypeCatdet) {
		this.examtypeCatdet = examtypeCatdet;
	}

	public StudentDetail getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(StudentDetail studentDetail) {
		this.studentDetail = studentDetail;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public List<ExamStudentMemoSubject> getExamStudentMemoSubjects() {
		return examStudentMemoSubjects;
	}

	public void setExamStudentMemoSubjects(List<ExamStudentMemoSubject> examStudentMemoSubjects) {
		this.examStudentMemoSubjects = examStudentMemoSubjects;
	}


	
	public Integer getRegistered() {
		return registered;
	}

	public void setRegistered(Integer registered) {
		this.registered = registered;
	}

	public Integer getAppeared() {
		return appeared;
	}

	public void setAppeared(Integer appeared) {
		this.appeared = appeared;
	}

	public Integer getPassed() {
		return passed;
	}

	public void setPassed(Integer passed) {
		this.passed = passed;
	}

	public BigDecimal getSgpa() {
		return sgpa;
	}

	public void setSgpa(BigDecimal sgpa) {
		this.sgpa = sgpa;
	}

	public BigDecimal getCgpa() {
		return cgpa;
	}

	public void setCgpa(BigDecimal cgpa) {
		this.cgpa = cgpa;
	}
}