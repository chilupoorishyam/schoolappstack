package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the t_m_notification_audience database table.
 * 
 */
@Entity
@Table(name="t_m_notification_audience")
@NamedQuery(name="NotificationAudience.findAll", query="SELECT n FROM NotificationAudience n")
public class NotificationAudience implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_notification_audience_id")
	private Long notificationAudienceId;

	@Column(name="category_name")
	private String categoryName;

	@Column(name="category_value")
	private String categoryValue;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_audience_type_catdet_id")
	private GeneralDetail audienceType;

	//bi-directional many-to-one association to Notification
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_notification_id")
	private Notification notification;

	public NotificationAudience() {
	}

	public Long getNotificationAudienceId() {
		return this.notificationAudienceId;
	}

	public void setNotificationAudienceId(Long notificationAudienceId) {
		this.notificationAudienceId = notificationAudienceId;
	}

	public String getCategoryName() {
		return this.categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryValue() {
		return this.categoryValue;
	}

	public void setCategoryValue(String categoryValue) {
		this.categoryValue = categoryValue;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getAudienceType() {
		return this.audienceType;
	}

	public void setAudienceType(GeneralDetail audienceType) {
		this.audienceType = audienceType;
	}

	public Notification getNotification() {
		return this.notification;
	}

	public void setNotification(Notification notification) {
		this.notification = notification;
	}

}