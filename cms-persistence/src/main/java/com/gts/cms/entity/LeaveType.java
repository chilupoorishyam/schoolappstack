package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_hr_leave_types database table.
 * 
 */
@Entity
@Table(name="t_hr_leave_types")
@NamedQuery(name="LeaveType.findAll", query="SELECT l FROM LeaveType l")
public class LeaveType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_leavetype_id")
	private Long leavetypeId;

	@Column(name="additional_leaves")
	private Integer additionalLeaves;

	@Column(name="allow_carry_forward")
	private Boolean allowCarryForward;

	@Column(name="carry_all")
	private Boolean carryAll;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="leave_code")
	private String leaveCode;

	@Column(name="leave_count")
	private BigDecimal leaveCount;

	@Column(name="leave_name")
	private String leaveName;

	private String reason;

	@Column(name="specific_count")
	private Integer specificCount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	@Temporal(TemporalType.DATE)
	@Column(name="valid_from")
	private Date validFrom;

	@Temporal(TemporalType.DATE)
	@Column(name="valid_to")
	private Date validTo;

	//bi-directional many-to-one association to EmployeeAttendance
	@OneToMany(mappedBy="leaveType",fetch = FetchType.LAZY)
	private List<EmployeeAttendance> empAttendances;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_leavetype_duration_catdet_id")
	private GeneralDetail leavetypeDuration;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	public LeaveType() {
	}

	public Long getLeavetypeId() {
		return this.leavetypeId;
	}

	public void setLeavetypeId(Long leavetypeId) {
		this.leavetypeId = leavetypeId;
	}

	public Integer getAdditionalLeaves() {
		return this.additionalLeaves;
	}

	public void setAdditionalLeaves(Integer additionalLeaves) {
		this.additionalLeaves = additionalLeaves;
	}

	public Boolean getAllowCarryForward() {
		return this.allowCarryForward;
	}

	public void setAllowCarryForward(Boolean allowCarryForward) {
		this.allowCarryForward = allowCarryForward;
	}

	public Boolean getCarryAll() {
		return this.carryAll;
	}

	public void setCarryAll(Boolean carryAll) {
		this.carryAll = carryAll;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getLeaveCode() {
		return this.leaveCode;
	}

	public void setLeaveCode(String leaveCode) {
		this.leaveCode = leaveCode;
	}
	public BigDecimal getLeaveCount() {
		return leaveCount;
	}
	
	

	
	public void setLeaveCount(BigDecimal leaveCount) {
		this.leaveCount = leaveCount;
	}

	public String getLeaveName() {
		return this.leaveName;
	}

	public void setLeaveName(String leaveName) {
		this.leaveName = leaveName;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getSpecificCount() {
		return this.specificCount;
	}

	public void setSpecificCount(Integer specificCount) {
		this.specificCount = specificCount;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public List<EmployeeAttendance> getEmpAttendances() {
		return this.empAttendances;
	}

	public void setEmpAttendances(List<EmployeeAttendance> empAttendances) {
		this.empAttendances = empAttendances;
	}

	public EmployeeAttendance addEmpAttendance(EmployeeAttendance empAttendance) {
		getEmpAttendances().add(empAttendance);
		empAttendance.setLeaveType(this);

		return empAttendance;
	}

	public EmployeeAttendance removeEmpAttendance(EmployeeAttendance empAttendance) {
		getEmpAttendances().remove(empAttendance);
		empAttendance.setLeaveType(null);

		return empAttendance;
	}

	public GeneralDetail getLeavetypeDuration() {
		return this.leavetypeDuration;
	}

	public void setLeavetypeDuration(GeneralDetail leavetypeDuration) {
		this.leavetypeDuration = leavetypeDuration;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	


}