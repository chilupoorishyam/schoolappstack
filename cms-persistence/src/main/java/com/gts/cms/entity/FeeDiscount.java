package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_fee_discounts database table.
 * 
 */
@Entity
@Table(name="t_fee_discounts")
@NamedQuery(name="FeeDiscount.findAll", query="SELECT f FROM FeeDiscount f")
public class FeeDiscount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_fee_discount_id")
	private Long feeDiscountId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	private Integer discount;

	@Column(name="discount_mode")
	private String discountMode;

	@Column(name="discount_name")
	private String discountName;

	@Column(name="is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to FeeDiscountApplyFor
	@OneToMany(mappedBy="feeDiscount",cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	private List<FeeDiscountApplyFor> feeDiscountApplyFors;

	//bi-directional many-to-one association to FeeStructure
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_fee_structure_id")
	private FeeStructure feeStructure;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to FeeDiscountsParticular
	@OneToMany(mappedBy="feeDiscount",cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	private List<FeeDiscountsParticular> feeDiscountsParticulars;

	//bi-directional many-to-one association to FeeStudentwiseDiscount
	@OneToMany(mappedBy="feeDiscount",fetch = FetchType.LAZY)
	private List<FeeStudentwiseDiscount> feeStudentwiseDiscounts;

	public FeeDiscount() {
	}

	public Long getFeeDiscountId() {
		return this.feeDiscountId;
	}

	public void setFeeDiscountId(Long feeDiscountId) {
		this.feeDiscountId = feeDiscountId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Integer getDiscount() {
		return this.discount;
	}

	public void setDiscount(Integer discount) {
		this.discount = discount;
	}

	public String getDiscountMode() {
		return this.discountMode;
	}

	public void setDiscountMode(String discountMode) {
		this.discountMode = discountMode;
	}

	public String getDiscountName() {
		return this.discountName;
	}

	public void setDiscountName(String discountName) {
		this.discountName = discountName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<FeeDiscountApplyFor> getFeeDiscountApplyFors() {
		return this.feeDiscountApplyFors;
	}

	public void setFeeDiscountApplyFors(List<FeeDiscountApplyFor> feeDiscountApplyFors) {
		this.feeDiscountApplyFors = feeDiscountApplyFors;
	}

	public FeeDiscountApplyFor addFeeDiscountApplyFor(FeeDiscountApplyFor feeDiscountApplyFor) {
		getFeeDiscountApplyFors().add(feeDiscountApplyFor);
		feeDiscountApplyFor.setFeeDiscount(this);

		return feeDiscountApplyFor;
	}

	public FeeDiscountApplyFor removeFeeDiscountApplyFor(FeeDiscountApplyFor feeDiscountApplyFor) {
		getFeeDiscountApplyFors().remove(feeDiscountApplyFor);
		feeDiscountApplyFor.setFeeDiscount(null);

		return feeDiscountApplyFor;
	}

	public FeeStructure getFeeStructure() {
		return this.feeStructure;
	}

	public void setFeeStructure(FeeStructure feeStructure) {
		this.feeStructure = feeStructure;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<FeeDiscountsParticular> getFeeDiscountsParticulars() {
		return this.feeDiscountsParticulars;
	}

	public void setFeeDiscountsParticulars(List<FeeDiscountsParticular> feeDiscountsParticulars) {
		this.feeDiscountsParticulars = feeDiscountsParticulars;
	}

	public FeeDiscountsParticular addFeeDiscountsParticular(FeeDiscountsParticular feeDiscountsParticular) {
		getFeeDiscountsParticulars().add(feeDiscountsParticular);
		feeDiscountsParticular.setFeeDiscount(this);

		return feeDiscountsParticular;
	}

	public FeeDiscountsParticular removeFeeDiscountsParticular(FeeDiscountsParticular feeDiscountsParticular) {
		getFeeDiscountsParticulars().remove(feeDiscountsParticular);
		feeDiscountsParticular.setFeeDiscount(null);

		return feeDiscountsParticular;
	}

	public List<FeeStudentwiseDiscount> getFeeStudentwiseDiscounts() {
		return this.feeStudentwiseDiscounts;
	}

	public void setFeeStudentwiseDiscounts(List<FeeStudentwiseDiscount> feeStudentwiseDiscounts) {
		this.feeStudentwiseDiscounts = feeStudentwiseDiscounts;
	}

	public FeeStudentwiseDiscount addFeeStudentwiseDiscount(FeeStudentwiseDiscount feeStudentwiseDiscount) {
		getFeeStudentwiseDiscounts().add(feeStudentwiseDiscount);
		feeStudentwiseDiscount.setFeeDiscount(this);

		return feeStudentwiseDiscount;
	}

	public FeeStudentwiseDiscount removeFeeStudentwiseDiscount(FeeStudentwiseDiscount feeStudentwiseDiscount) {
		getFeeStudentwiseDiscounts().remove(feeStudentwiseDiscount);
		feeStudentwiseDiscount.setFeeDiscount(null);

		return feeStudentwiseDiscount;
	}

}