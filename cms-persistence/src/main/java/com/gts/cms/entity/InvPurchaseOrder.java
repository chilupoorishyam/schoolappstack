package com.gts.cms.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_inv_purchase_order database table.
 * 
 */
@Entity
@Table(name = "t_inv_purchase_order")
@NamedQuery(name = "InvPurchaseOrder.findAll", query = "SELECT i FROM InvPurchaseOrder i")
public class InvPurchaseOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_po_id")
	private Long poId;

	@Column(name = "authorization_comments")
	private String authorizationComments;

	private BigDecimal cgst;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "discount_amount")
	private BigDecimal discountAmount;
	
	@Column(name ="invoice_no")
	private String invoiceNo;
	
	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_authorized_by_emp_id")
	private EmployeeDetail authorizedByEmpId;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_inv_transtype_catdet_id")
	private GeneralDetail invTranstypeCatdet;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_po_status_catdet_id")
	private GeneralDetail poStatusCatdet;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_potype_catdet_id")
	private GeneralDetail potypeCatdet;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_status_emp_id")
	private EmployeeDetail statusEmp;

	private BigDecimal igst;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_srv_completed")
	private Boolean isSrvCompleted;

	@Column(name = "other_charges")
	private BigDecimal otherCharges;

	@Column(name = "po_actual_amount")
	private BigDecimal poActualAmount;

	@Column(name = "po_comments")
	private String poComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "po_date")
	private Date poDate;

	@Column(name = "po_ref_file_path1")
	private String poRefFilePath1;

	@Column(name = "po_ref_file_path2")
	private String poRefFilePath2;

	private String pono;

	private String reason;

	private BigDecimal sgst;

	@Column(name = "po_net_cost")
	private BigDecimal poNetCost;

	@Column(name = "shipping_charges")
	private BigDecimal shippingCharges;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "shipping_date")
	private Date shippingDate;

	@Column(name = "status_comments")
	private String statusComments;

	private String termsconditions;

	@Column(name = "total_tax")
	private BigDecimal totalTax;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to InvPoItem
	@OneToMany(mappedBy = "invPurchaseOrder", cascade = CascadeType.ALL)
	private List<InvPoItem> invPoItems;

	// bi-directional many-to-one association to InvPoWorkflow
	@OneToMany(mappedBy = "invPurchaseOrder", cascade = CascadeType.ALL)
	private List<InvPoWorkflow> invPoWorkflows;

	// bi-directional many-to-one association to InvStoresmaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_store_id")
	private InvStoresmaster invStoresmaster;

	// bi-directional many-to-one association to InvSuppliermaster
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_supplier_id")
	private InvSuppliermaster invSuppliermaster;

	/*
	 * // bi-directional many-to-one association to InvSrv
	 * 
	 * @OneToMany(mappedBy = "InvPurchaseOrder") 
	 * private List<InvSrv> invSrvs;
	 */

	public InvPurchaseOrder() {
	}

	public Long getPoId() {
		return this.poId;
	}

	public void setPoId(Long poId) {
		this.poId = poId;
	}

	public String getAuthorizationComments() {
		return this.authorizationComments;
	}

	public void setAuthorizationComments(String authorizationComments) {
		this.authorizationComments = authorizationComments;
	}

	public BigDecimal getCgst() {
		return this.cgst;
	}

	public void setCgst(BigDecimal cgst) {
		this.cgst = cgst;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public BigDecimal getDiscountAmount() {
		return this.discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public BigDecimal getIgst() {
		return this.igst;
	}

	public void setIgst(BigDecimal igst) {
		this.igst = igst;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsSrvCompleted() {
		return this.isSrvCompleted;
	}

	public void setIsSrvCompleted(Boolean isSrvCompleted) {
		this.isSrvCompleted = isSrvCompleted;
	}

	public BigDecimal getOtherCharges() {
		return this.otherCharges;
	}

	public void setOtherCharges(BigDecimal otherCharges) {
		this.otherCharges = otherCharges;
	}

	public BigDecimal getPoActualAmount() {
		return this.poActualAmount;
	}

	public void setPoActualAmount(BigDecimal poActualAmount) {
		this.poActualAmount = poActualAmount;
	}

	public String getPoComments() {
		return this.poComments;
	}

	public void setPoComments(String poComments) {
		this.poComments = poComments;
	}

	public Date getPoDate() {
		return this.poDate;
	}

	public void setPoDate(Date poDate) {
		this.poDate = poDate;
	}

	public String getPoRefFilePath1() {
		return this.poRefFilePath1;
	}

	public void setPoRefFilePath1(String poRefFilePath1) {
		this.poRefFilePath1 = poRefFilePath1;
	}

	public String getPoRefFilePath2() {
		return this.poRefFilePath2;
	}

	public void setPoRefFilePath2(String poRefFilePath2) {
		this.poRefFilePath2 = poRefFilePath2;
	}

	/*
	 * public BigDecimal getPoTotalCost() { return this.poTotalCost; }
	 * 
	 * public void setPoTotalCost(BigDecimal poTotalCost) { this.poTotalCost =
	 * poTotalCost; }
	 */

	public String getPono() {
		return this.pono;
	}

	public void setPono(String pono) {
		this.pono = pono;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public BigDecimal getSgst() {
		return this.sgst;
	}

	public void setSgst(BigDecimal sgst) {
		this.sgst = sgst;
	}

	public BigDecimal getShippingCharges() {
		return this.shippingCharges;
	}

	public void setShippingCharges(BigDecimal shippingCharges) {
		this.shippingCharges = shippingCharges;
	}

	public Date getShippingDate() {
		return this.shippingDate;
	}

	public void setShippingDate(Date shippingDate) {
		this.shippingDate = shippingDate;
	}

	public String getStatusComments() {
		return this.statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public String getTermsconditions() {
		return this.termsconditions;
	}

	public void setTermsconditions(String termsconditions) {
		this.termsconditions = termsconditions;
	}

	public BigDecimal getTotalTax() {
		return this.totalTax;
	}

	public void setTotalTax(BigDecimal totalTax) {
		this.totalTax = totalTax;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public EmployeeDetail getAuthorizedByEmpId() {
		return authorizedByEmpId;
	}

	public void setAuthorizedByEmpId(EmployeeDetail authorizedByEmpId) {
		this.authorizedByEmpId = authorizedByEmpId;
	}

	public GeneralDetail getInvTranstypeCatdet() {
		return invTranstypeCatdet;
	}

	public void setInvTranstypeCatdet(GeneralDetail invTranstypeCatdet) {
		this.invTranstypeCatdet = invTranstypeCatdet;
	}

	public GeneralDetail getPoStatusCatdet() {
		return poStatusCatdet;
	}

	public void setPoStatusCatdet(GeneralDetail poStatusCatdet) {
		this.poStatusCatdet = poStatusCatdet;
	}

	public GeneralDetail getPotypeCatdet() {
		return potypeCatdet;
	}

	public void setPotypeCatdet(GeneralDetail potypeCatdet) {
		this.potypeCatdet = potypeCatdet;
	}

	public EmployeeDetail getStatusEmp() {
		return statusEmp;
	}

	public void setStatusEmp(EmployeeDetail statusEmp) {
		this.statusEmp = statusEmp;
	}

	public List<InvPoItem> getInvPoItems() {
		return invPoItems;
	}

	public void setInvPoItems(List<InvPoItem> invPoItems) {
		this.invPoItems = invPoItems;
	}

	public BigDecimal getPoNetCost() {
		return poNetCost;
	}

	public void setPoNetCost(BigDecimal poNetCost) {
		this.poNetCost = poNetCost;
	}

	public InvStoresmaster getInvStoresmaster() {
		return invStoresmaster;
	}

	public void setInvStoresmaster(InvStoresmaster invStoresmaster) {
		this.invStoresmaster = invStoresmaster;
	}

	public InvSuppliermaster getInvSuppliermaster() {
		return invSuppliermaster;
	}

	public void setInvSuppliermaster(InvSuppliermaster invSuppliermaster) {
		this.invSuppliermaster = invSuppliermaster;
	}

	public List<InvPoWorkflow> getInvPoWorkflows() {
		return invPoWorkflows;
	}

	public void setInvPoWorkflows(List<InvPoWorkflow> invPoWorkflows) {
		this.invPoWorkflows = invPoWorkflows;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

}