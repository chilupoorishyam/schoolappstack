package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_cm_subject_courseyear database table.
 * 
 */
@Entity
@Table(name="t_cm_subject_courseyear")
@NamedQuery(name="SubjectCourseyear.findAll", query="SELECT s FROM SubjectCourseyear s")
public class SubjectCourseyear implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_sub_courseyear_id")
	private Long subjectCourseyearId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="credit_hours")
	private Integer creditHours;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="max_weekly_classes")
	private Integer maxWeeklyClasses;

	@Column(name="no_exams")
	private Boolean noExams;

	@Column(name="prefer_consecutive")
	private Boolean preferConsecutive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to StaffCourseyrSubject
	@OneToMany(mappedBy="subjectCourseyear",fetch = FetchType.LAZY)
	private List<StaffCourseyrSubject> staffCourseyrSubjects;

	//bi-directional many-to-one association to SubjectBook
	/*@OneToMany(mappedBy="subjectCourseyear")
	private List<SubjectBook> subjectBooks;
*/
	//bi-directional many-to-one association to Subjectregulation
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subreg_id")
	private Subjectregulation subjectregulation;

	//bi-directional many-to-one association to AcademicYear
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_academic_year_id")
	private AcademicYear academicYear;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to GroupSection
	@ManyToOne(fetch = FetchType.LAZY)
	@OrderBy("sortOrder")
	@JoinColumn(name="fk_group_section_id")
	private GroupSection groupSection;

	//bi-directional many-to-one association to Subject
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_subject_id")
	private Subject subject;

	//bi-directional many-to-one association to SubjectUnitTopic
	/*@OneToMany(mappedBy="subjectCourseyear")
	private List<SubjectUnitTopic> subjectUnitTopics;

	//bi-directional many-to-one association to SubjectUnit
	@OneToMany(mappedBy="subjectCourseyear")
	private List<SubjectUnit> subjectUnits;*/

	//bi-directional many-to-one association to ActualClassesSchedule
	/*@OneToMany(mappedBy="subjectCourseyear")
	private List<ActualClassesSchedule> actualClassesSchedules;*/

	//bi-directional many-to-one association to StaffProxy
	@OneToMany(mappedBy="subjectCourseyear",fetch = FetchType.LAZY)
	private List<StaffProxy> staffProxies;

	//bi-directional many-to-one association to SubjectResource
	@OneToMany(mappedBy="subjectCourseyear",fetch = FetchType.LAZY)
	private List<SubjectResource> subjectResources;

	public SubjectCourseyear() {
	}

	public Long getSubjectCourseyearId() {
		return this.subjectCourseyearId;
	}

	public void setSubjectCourseyearId(Long subjectCourseyearId) {
		this.subjectCourseyearId = subjectCourseyearId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Integer getCreditHours() {
		return this.creditHours;
	}

	public void setCreditHours(Integer creditHours) {
		this.creditHours = creditHours;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getMaxWeeklyClasses() {
		return this.maxWeeklyClasses;
	}

	public void setMaxWeeklyClasses(Integer maxWeeklyClasses) {
		this.maxWeeklyClasses = maxWeeklyClasses;
	}

	public Boolean getNoExams() {
		return this.noExams;
	}

	public void setNoExams(Boolean noExams) {
		this.noExams = noExams;
	}

	public Boolean getPreferConsecutive() {
		return this.preferConsecutive;
	}

	public void setPreferConsecutive(Boolean preferConsecutive) {
		this.preferConsecutive = preferConsecutive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<StaffCourseyrSubject> getStaffCourseyrSubjects() {
		return this.staffCourseyrSubjects;
	}

	public void setStaffCourseyrSubjects(List<StaffCourseyrSubject> staffCourseyrSubjects) {
		this.staffCourseyrSubjects = staffCourseyrSubjects;
	}

	public StaffCourseyrSubject addStaffCourseyrSubject(StaffCourseyrSubject staffCourseyrSubject) {
		getStaffCourseyrSubjects().add(staffCourseyrSubject);
		staffCourseyrSubject.setSubjectCourseyear(this);

		return staffCourseyrSubject;
	}

	public StaffCourseyrSubject removeStaffCourseyrSubject(StaffCourseyrSubject staffCourseyrSubject) {
		getStaffCourseyrSubjects().remove(staffCourseyrSubject);
		staffCourseyrSubject.setSubjectCourseyear(null);

		return staffCourseyrSubject;
	}

	/*public List<SubjectBook> getSubjectBooks() {
		return this.subjectBooks;
	}

	public void setSubjectBooks(List<SubjectBook> subjectBooks) {
		this.subjectBooks = subjectBooks;
	}*/

	public Subjectregulation getSubjectregulation() {
		return this.subjectregulation;
	}

	public void setSubjectregulation(Subjectregulation subjectregulation) {
		this.subjectregulation = subjectregulation;
	}

	public AcademicYear getAcademicYear() {
		return this.academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GroupSection getGroupSection() {
		return this.groupSection;
	}

	public void setGroupSection(GroupSection groupSection) {
		this.groupSection = groupSection;
	}

	public Subject getSubject() {
		return this.subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	/*public List<SubjectUnitTopic> getSubjectUnitTopics() {
		return this.subjectUnitTopics;
	}

	public void setSubjectUnitTopics(List<SubjectUnitTopic> subjectUnitTopics) {
		this.subjectUnitTopics = subjectUnitTopics;
	}


	public List<SubjectUnit> getSubjectUnits() {
		return this.subjectUnits;
	}

	public void setSubjectUnits(List<SubjectUnit> subjectUnits) {
		this.subjectUnits = subjectUnits;
	}
*/
	/*public List<ActualClassesSchedule> getActualClassesSchedules() {
		return this.actualClassesSchedules;
	}

	public void setActualClassesSchedules(List<ActualClassesSchedule> actualClassesSchedules) {
		this.actualClassesSchedules = actualClassesSchedules;
	}*/

/*	public ActualClassesSchedule addActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		getActualClassesSchedules().add(actualClassesSchedule);
		actualClassesSchedule.setSubjectCourseyear(this);

		return actualClassesSchedule;
	}

	public ActualClassesSchedule removeActualClassesSchedule(ActualClassesSchedule actualClassesSchedule) {
		getActualClassesSchedules().remove(actualClassesSchedule);
		actualClassesSchedule.setSubjectCourseyear(null);

		return actualClassesSchedule;
	}*/

	public List<StaffProxy> getStaffProxies() {
		return this.staffProxies;
	}

	public void setStaffProxies(List<StaffProxy> staffProxies) {
		this.staffProxies = staffProxies;
	}

	public StaffProxy addStaffProxy(StaffProxy staffProxy) {
		getStaffProxies().add(staffProxy);
		staffProxy.setSubjectCourseyear(this);

		return staffProxy;
	}

	public StaffProxy removeStaffProxy(StaffProxy staffProxy) {
		getStaffProxies().remove(staffProxy);
		staffProxy.setSubjectCourseyear(null);

		return staffProxy;
	}

	public List<SubjectResource> getSubjectResources() {
		return this.subjectResources;
	}

	public void setSubjectResources(List<SubjectResource> subjectResources) {
		this.subjectResources = subjectResources;
	}

	public SubjectResource addSubjectResource(SubjectResource subjectResource) {
		getSubjectResources().add(subjectResource);
		subjectResource.setSubjectCourseyear(this);

		return subjectResource;
	}

	public SubjectResource removeSubjectResource(SubjectResource subjectResource) {
		getSubjectResources().remove(subjectResource);
		subjectResource.setSubjectCourseyear(null);

		return subjectResource;
	}

}