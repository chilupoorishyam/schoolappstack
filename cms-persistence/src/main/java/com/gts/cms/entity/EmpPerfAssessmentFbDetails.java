package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "t_hr_emp_perf_assessment_fb_details")
@NamedQuery(name="EmpPerfAssessmentFbDetails.findAll", query="SELECT e FROM EmpPerfAssessmentFbDetails e")
public class EmpPerfAssessmentFbDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_assessment_fb_details", nullable = false)
    private Long assessmentFbDetailsId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_assessment_feedback_id", nullable = false)
    private EmpPerfAssessmentFeedback assessmentFeedbackId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_assessment_question_id", nullable = false)
    private EmpPerfAssessmentQuestions assessmentQuestionId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_assessment_option_id", nullable = false)
    private EmpPerfAssessmentQutnOpts assessmentOptionId;

    @Column(name = "option_value")
    private Integer optionValue;

    @Column(name = "description")
    private String description;

    @Column(name = "rating")
    private Integer rating;

    @Column(name = "auth_rating")
    private Integer authRating;

    @Column(name = "is_active", nullable = false)
    private Boolean active;

    @Column(name = "reason")
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    public Long getAssessmentFbDetailsId() {
        return assessmentFbDetailsId;
    }

    public void setAssessmentFbDetailsId(Long assessmentFbDetailsId) {
        this.assessmentFbDetailsId = assessmentFbDetailsId;
    }

    public EmpPerfAssessmentFeedback getAssessmentFeedbackId() {
        return assessmentFeedbackId;
    }

    public void setAssessmentFeedbackId(EmpPerfAssessmentFeedback assessmentFeedbackId) {
        this.assessmentFeedbackId = assessmentFeedbackId;
    }

    public EmpPerfAssessmentQuestions getAssessmentQuestionId() {
        return assessmentQuestionId;
    }

    public void setAssessmentQuestionId(EmpPerfAssessmentQuestions assessmentQuestionId) {
        this.assessmentQuestionId = assessmentQuestionId;
    }

    public EmpPerfAssessmentQutnOpts getAssessmentOptionId() {
        return assessmentOptionId;
    }

    public void setAssessmentOptionId(EmpPerfAssessmentQutnOpts assessmentOptionId) {
        this.assessmentOptionId = assessmentOptionId;
    }

    public Integer getOptionValue() {
        return optionValue;
    }

    public void setOptionValue(Integer optionValue) {
        this.optionValue = optionValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Integer getAuthRating() {
        return authRating;
    }

    public void setAuthRating(Integer authRating) {
        this.authRating = authRating;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    public Long getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(Long createdUser) {
        this.createdUser = createdUser;
    }

    public Date getUpdatedDt() {
        return updatedDt;
    }

    public void setUpdatedDt(Date updatedDt) {
        this.updatedDt = updatedDt;
    }

    public Long getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(Long updatedUser) {
        this.updatedUser = updatedUser;
    }
}
