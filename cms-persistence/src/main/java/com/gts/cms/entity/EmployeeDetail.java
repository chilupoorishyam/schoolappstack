package com.gts.cms.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_emp_details database table.
 * 
 */
@Entity
@Table(name="t_emp_details")
@NamedQuery(name="EmployeeDetail.findAll", query="SELECT e FROM EmployeeDetail e")
public class EmployeeDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_emp_id")
	private Long employeeId;

	@Column(name="aadhar_no")
	private String aadharNo;

	@Column(name="aadhar_path")
	private String aadharPath;

	private String address;

	@Column(name="aicte_reg_no")
	private String aicteRegNo;

	@Column(name="biometric_code")
	private String biometricCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Temporal(TemporalType.DATE)
	@Column(name="date_of_birth")
	private Date dateOfBirth;

	@Temporal(TemporalType.DATE)
	@Column(name="date_of_relieving")
	private Date dateOfRelieving;

	private String email;

	@Column(name="emergency_mobile")
	private String emergencyMobile;

	@Column(name="emp_number")
	private String empNumber;

	@Column(name="epf_no")
	private String epfNo;

	@Column(name="esi_reg_no")
	private String esiRegNo;

	@Column(name="facebook_url")
	private String facebookUrl;

	@Column(name="father_name")
	private String fatherName;

	@Column(name="first_name")
	private String firstName;

	@Column(name="fk_biometric_id")
	private Long biometricId;

	@Column(name="fk_current_pay_id")
	private Long currentPayId;

	@Column(name="fk_payroll_pay_id")
	private Long payrollPayId;

	@Column(name="fk_payscale_id")
	private Long payscaleId;
	
	//bi-directional many-to-one association to Caste
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_reporting_manager_id")
	private EmployeeDetail reportingManager;

	@Column(name="googleplus_url")
	private String googleplusUrl;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_manager")
	private Boolean isManager;

	@Column(name="is_ptax")
	private Boolean isPtax;

	@Column(name="is_ratified")
	private Boolean isRatified;

	@Column(name="is_tds")
	private Boolean isTds;

	@Column(name="is_using_camp_accommodation")
	private Boolean isUsingCampAccommodation;

	@Column(name="is_using_transport")
	private Boolean isUsingTransport;

	@Temporal(TemporalType.DATE)
	@Column(name="jntu_date_of_joining")
	private Date jntuDateOfJoining;

	@Column(name="jntu_reg_no")
	private String jntuRegNo;

	@Temporal(TemporalType.DATE)
	@Column(name="joining_date")
	private Date joiningDate;

	@Column(name="last_name")
	private String lastName;

	@Column(name="lic_no")
	private String licNo;

	@Column(name="linkedin_url")
	private String linkedinUrl;

	@Column(name="middle_name")
	private String middleName;

	private String mobile;

	@Column(name="monthly_salary")
	private Integer monthlySalary;

	@Column(name="mother_name")
	private String motherName;

	@Column(name="non_teaching_exp")
	private Integer nonTeachingExp;

	@Column(name="official_mobile")
	private String officialMobile;

	private String pancard;

	@Column(name="pancard_path")
	private String pancardPath;

	@Column(name="passport_no")
	private String passportNo;

	@Column(name="passport_path")
	private String passportPath;

	@Column(name="permanent_address")
	private String permanentAddress;

	@Column(name="permanent_mandal")
	private String permanentMandal;

	@Column(name="permanent_pincode")
	private String permanentPincode;

	@Column(name="permanent_street")
	private String permanentStreet;

	@Column(name="photo_path")
	private String photoPath;

	@Column(name="present_address")
	private String presentAddress;

	@Column(name="present_mandal")
	private String presentMandal;

	@Column(name="present_pincode")
	private String presentPincode;

	@Column(name="present_street")
	private String presentStreet;

	@Temporal(TemporalType.DATE)
	@Column(name="promoted_date")
	private Date promotedDate;

	@Column(name="research_exp")
	private Long researchExp;

	@Column(name="residence_phone")
	private String residencePhone;

	@Temporal(TemporalType.DATE)
	@Column(name="resignation_date")
	private Date resignationDate;

	private String rfid;

	@Column(name="service_break_yrs")
	private Integer serviceBreakYrs;

	@Column(name="status_description")
	private String statusDescription;

	@Column(name="teaching_exp")
	private Integer teachingExp;

	@Column(name="tenure_days")
	private Integer tenureDays;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	@Column(name="voter_id")
	private String voterId;

	@Column(name="voter_id_path")
	private String voterIdPath;

	@Temporal(TemporalType.DATE)
	@Column(name="wedding_date")
	private Date weddingDate;
	
	@Column(name="reason")
	private String reason;

	//bi-directional many-to-one association to StaffCourseyrSubject
	@OneToMany(mappedBy="employeeDetail",fetch = FetchType.LAZY)
	private List<StaffCourseyrSubject> staffCourseyrSubjects;

	//bi-directional many-to-one association to EmployeeAttendance
	@OneToMany(mappedBy="employeeDetail",fetch = FetchType.LAZY)
	private List<EmployeeAttendance> empAttendances1;

	//bi-directional many-to-one association to EmployeeAttendance
	@OneToMany(mappedBy="verifiedbyEmployeeDetail",fetch = FetchType.LAZY)
	private List<EmployeeAttendance> empAttendances2;

	//bi-directional many-to-one association to EmployeeDailylog
	@OneToMany(mappedBy="employeeDetail",fetch = FetchType.LAZY)
	private List<EmployeeDailylog> empDailylogs;

	//bi-directional many-to-one association to Caste
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_caste_id")
	private Caste caste;

	//bi-directional many-to-one association to City
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_city_present_id")
	private City presentCity;

	//bi-directional many-to-one association to City
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_city_permanent_id")
	private City permanentCity;

	//bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_school_id")
	private School school;

	//bi-directional many-to-one association to Department
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_dept_id")
	private Department employeeDepartment;

	//bi-directional many-to-one association to Department
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_working_dept_id")
	private Department employeeWorkingDepartment;

	//bi-directional many-to-one association to Designation
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_designation_id")
	private Designation designation;

	//bi-directional many-to-one association to Designation
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_working_designation_id")
	private Designation workingDesignation;

	//bi-directional many-to-one association to District
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_district_id")
	private District district;

	//bi-directional many-to-one association to District
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_district_present_id")
	private District presentDistrict;

	//bi-directional many-to-one association to District
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_district_permanent_id")
	private District permanentDistrict;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_maritalstatus_catdet_id")
	private GeneralDetail maritalStatus;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_status_catdet_id")
	private GeneralDetail employeeStatus;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_state_catdet_id")
	private GeneralDetail employeeState;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_type_catdet_id")
	private GeneralDetail employeeType;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_category_catdet_id")
	private GeneralDetail employeeCategory;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_emp_wrk_category_catdet_id")
	private GeneralDetail employeeWorkCategory;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_teachingfor_catdet_id")
	private GeneralDetail teachingFor;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_appointment_catdet_id")
	private GeneralDetail appointment;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_bloodgroup_catdet_id")
	private GeneralDetail bloodGroup;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_paymode_catdet_id")
	private GeneralDetail paymode;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_resident_catdet_id")
	private GeneralDetail resident;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_accommodation_catdet_id")
	private GeneralDetail accommodation;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_title_catdet_id")
	private GeneralDetail title;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_gender_catdet_id")
	private GeneralDetail gender;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_religion_catdet_id")
	private GeneralDetail religion;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_nationality_catdet_id")
	private GeneralDetail nationality;

	//bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_empgrade_catdet_id")
	private GeneralDetail employeeGrade;

	//bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_org_id")
	private Organization organization;

	//bi-directional many-to-one association to Qualification
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_qualification_id")
	private Qualification qualification;

	//bi-directional many-to-one association to SubCaste
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_sub_caste_id")
	private SubCaste subCaste;

	//bi-directional many-to-one association to User
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="fk_user_id")
	private User user;
	
	//bi-directional many-to-one association to EmployeeDetail
    @OneToMany(mappedBy="employeeDetail",fetch = FetchType.LAZY)
    private List<EmpDeptHeads> empDeptHeads;

    //bi-directional many-to-one association to EmployeeDocumentCollection
    @OneToMany(mappedBy="employeeDetail",cascade=CascadeType.ALL,fetch = FetchType.LAZY)
    private List<EmployeeDocumentCollection> empDocumentCollections1;

    //bi-directional many-to-one association to EmployeeDocumentCollection
    @OneToMany(mappedBy="verifiedByEmployeeDetail",fetch = FetchType.LAZY)
    private List<EmployeeDocumentCollection> empDocumentCollections2;

    //bi-directional many-to-one association to EmployeeBankDetail
    @OneToMany(mappedBy="employeeDetail",cascade=CascadeType.ALL,fetch = FetchType.LAZY)
    private List<EmployeeBankDetail> empEmployeeBankDetails;

    //bi-directional many-to-one association to EmployeeEducation
    @OneToMany(mappedBy="empDetail",cascade=CascadeType.ALL,fetch = FetchType.LAZY)
    private List<EmployeeEducation> empEmployeeEducations;

    //bi-directional many-to-one association to EmployeeReporting
    @OneToMany(mappedBy="employeeDetail",cascade=CascadeType.ALL,fetch = FetchType.LAZY)
    private List<EmployeeReporting> empEmployeeReportings1;

	//bi-directional many-to-one association to EmployeeReporting
	@OneToMany(mappedBy="managerEmployeeDetail",fetch = FetchType.LAZY)
	private List<EmployeeReporting> empEmployeeReportings2;

	//bi-directional many-to-one association to EmployeeExperienceDetail
	@OneToMany(mappedBy="employeeDetail",cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	private List<EmployeeExperienceDetail> empExperienceDetails;

	//bi-directional many-to-one association to FeeInstantPayment
	@OneToMany(mappedBy="employeeDetail",fetch = FetchType.LAZY)
	private List<FeeInstantPayment> feeInstantPayments;

	//bi-directional many-to-one association to FeeParticularwisePayment
	@OneToMany(mappedBy="employeeDetail",fetch = FetchType.LAZY)
	private List<FeeParticularwisePayment> feeParticularwisePayments;

	//bi-directional many-to-one association to FeeReceipt
	@OneToMany(mappedBy="employeeDetail",fetch = FetchType.LAZY)
	private List<FeeReceipt> feeReceipts;

	//bi-directional many-to-one association to FeeReceipt
	@OneToMany(mappedBy="revertbByEmployeeDetail",fetch = FetchType.LAZY)
	private List<FeeReceipt> feeReceipts2;

	//bi-directional many-to-one association to FeeStudentRefund
	@OneToMany(mappedBy="employeeDetail",fetch = FetchType.LAZY)
	private List<FeeStudentRefund> feeStudentRefunds;

	//bi-directional many-to-one association to FeeStudentwiseDiscount
	@OneToMany(mappedBy="requestedtoEmployeeDetail",fetch = FetchType.LAZY)
	private List<FeeStudentwiseDiscount> feeStudentwiseDiscounts1;

	//bi-directional many-to-one association to FeeStudentwiseDiscount
	@OneToMany(mappedBy="authbyEmployee",fetch = FetchType.LAZY)
	private List<FeeStudentwiseDiscount> feeStudentwiseDiscounts2;

	//bi-directional many-to-one association to FeeTransactionMaster
	@OneToMany(mappedBy="collectedEmployeeDetail",fetch = FetchType.LAZY)
	private List<FeeTransactionMaster> feeTransactionMasters;

	//bi-directional many-to-one association to EmployeePayrollGroup
	@OneToMany(mappedBy="employeeDetail",fetch = FetchType.LAZY)
	private List<EmployeePayrollGroup> employeePayrollGroups;

	//bi-directional many-to-one association to EmployeePayslipGeneration
	@OneToMany(mappedBy="employeeDetail",fetch = FetchType.LAZY)
	private List<EmployeePayslipGeneration> employeePayslipGenerations1;

	//bi-directional many-to-one association to EmployeePayslipGeneration
	@OneToMany(mappedBy="generatedByEmployeeDetail",fetch = FetchType.LAZY)
	private List<EmployeePayslipGeneration> employeePayslipGenerations2;

	//bi-directional many-to-one association to SurveyFeedback
	@OneToMany(mappedBy="fromEmployeeDetail",fetch = FetchType.LAZY)
	private List<SurveyFeedback> surveyFeedbacks1;

	//bi-directional many-to-one association to SurveyFeedback
	@OneToMany(mappedBy="forEmployeeDetail",fetch = FetchType.LAZY)
	private List<SurveyFeedback> surveyFeedbacks2;

	//bi-directional many-to-one association to Event
	@OneToMany(mappedBy="inchargeEmployeeDetail",fetch = FetchType.LAZY)
	private List<Event> events;

	//bi-directional many-to-one association to Messaging
	@OneToMany(mappedBy="sentByEmployeeDetail",fetch = FetchType.LAZY)
	private List<Messaging> messagings;

	//bi-directional many-to-one association to MessagingRecipient
	@OneToMany(mappedBy="employeeDetail",fetch = FetchType.LAZY)
	private List<MessagingRecipient> messagingRecipients;

	//bi-directional many-to-one association to StudentAppDocCollection
	@OneToMany(mappedBy="verifiedbyEmployeeDetail",fetch = FetchType.LAZY)
	private List<StudentAppDocCollection> stdAppDocCollections;

	//bi-directional many-to-one association to StudentDocumentCollection
	@OneToMany(mappedBy="verifiedbyEmployeeDetail",fetch = FetchType.LAZY)
	private List<StudentDocumentCollection> stdDocumentCollections;

	//bi-directional many-to-one association to FeePayment
	@OneToMany(mappedBy="employeeDetail",fetch = FetchType.LAZY)
	private List<TransportFeePayment> feePayments;

	//bi-directional many-to-one association to TransportAllocation
	@OneToMany(mappedBy="employeeDetail",fetch = FetchType.LAZY)
	private List<TransportAllocation> transportAllocations;

	//bi-directional many-to-one association to VehicleMaintenance
	@OneToMany(mappedBy="approvedEmployeeDetail",fetch = FetchType.LAZY)
	private List<VehicleMaintenance> vehicleMaintenances;

	//bi-directional many-to-one association to StaffProxy
	@OneToMany(mappedBy="approvedByEmployeeDetail",fetch = FetchType.LAZY)
	private List<StaffProxy> staffProxies;

	//bi-directional many-to-one association to StudentAttendance
	/*@OneToMany(mappedBy="takenbyEmployeeDetail")
	private List<StudentAttendance> stdAttendances1;*/

	//bi-directional many-to-one association to StudentAttendance
	@OneToMany(mappedBy="verifiedbyEmployeeDetail",fetch = FetchType.LAZY)
	private List<StudentAttendance> stdAttendances2;

	public EmployeeDetail() {
	}

	public Long getEmployeeId() {
		return this.employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getAadharNo() {
		return this.aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}

	public String getAadharPath() {
		return this.aadharPath;
	}

	public void setAadharPath(String aadharPath) {
		this.aadharPath = aadharPath;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAicteRegNo() {
		return this.aicteRegNo;
	}

	public void setAicteRegNo(String aicteRegNo) {
		this.aicteRegNo = aicteRegNo;
	}

	public String getBiometricCode() {
		return this.biometricCode;
	}

	public void setBiometricCode(String biometricCode) {
		this.biometricCode = biometricCode;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Date getDateOfRelieving() {
		return this.dateOfRelieving;
	}

	public void setDateOfRelieving(Date dateOfRelieving) {
		this.dateOfRelieving = dateOfRelieving;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmergencyMobile() {
		return this.emergencyMobile;
	}

	public void setEmergencyMobile(String emergencyMobile) {
		this.emergencyMobile = emergencyMobile;
	}

	public String getEmpNumber() {
		return this.empNumber;
	}

	public void setEmpNumber(String empNumber) {
		this.empNumber = empNumber;
	}

	public String getEpfNo() {
		return this.epfNo;
	}

	public void setEpfNo(String epfNo) {
		this.epfNo = epfNo;
	}

	public String getEsiRegNo() {
		return this.esiRegNo;
	}

	public void setEsiRegNo(String esiRegNo) {
		this.esiRegNo = esiRegNo;
	}

	public String getFacebookUrl() {
		return this.facebookUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}

	public String getFatherName() {
		return this.fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Long getBiometricId() {
		return this.biometricId;
	}

	public void setBiometricId(Long biometricId) {
		this.biometricId = biometricId;
	}

	public Long getCurrentPayId() {
		return this.currentPayId;
	}

	public void setCurrentPayId(Long currentPayId) {
		this.currentPayId = currentPayId;
	}

	public Long getPayrollPayId() {
		return this.payrollPayId;
	}

	public void setPayrollPayId(Long payrollPayId) {
		this.payrollPayId = payrollPayId;
	}

	public Long getPayscaleId() {
		return this.payscaleId;
	}

	public void setPayscaleId(Long payscaleId) {
		this.payscaleId = payscaleId;
	}
	
	public EmployeeDetail getReportingManager() {
		return reportingManager;
	}

	public void setReportingManager(EmployeeDetail reportingManager) {
		this.reportingManager = reportingManager;
	}

	public String getGoogleplusUrl() {
		return this.googleplusUrl;
	}

	public void setGoogleplusUrl(String googleplusUrl) {
		this.googleplusUrl = googleplusUrl;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsManager() {
		return this.isManager;
	}

	public void setIsManager(Boolean isManager) {
		this.isManager = isManager;
	}

	public Boolean getIsPtax() {
		return this.isPtax;
	}

	public void setIsPtax(Boolean isPtax) {
		this.isPtax = isPtax;
	}

	public Boolean getIsRatified() {
		return this.isRatified;
	}

	public void setIsRatified(Boolean isRatified) {
		this.isRatified = isRatified;
	}

	public Boolean getIsTds() {
		return this.isTds;
	}

	public void setIsTds(Boolean isTds) {
		this.isTds = isTds;
	}

	public Boolean getIsUsingCampAccommodation() {
		return this.isUsingCampAccommodation;
	}

	public void setIsUsingCampAccommodation(Boolean isUsingCampAccommodation) {
		this.isUsingCampAccommodation = isUsingCampAccommodation;
	}

	public Boolean getIsUsingTransport() {
		return this.isUsingTransport;
	}

	public void setIsUsingTransport(Boolean isUsingTransport) {
		this.isUsingTransport = isUsingTransport;
	}

	public Date getJntuDateOfJoining() {
		return this.jntuDateOfJoining;
	}

	public void setJntuDateOfJoining(Date jntuDateOfJoining) {
		this.jntuDateOfJoining = jntuDateOfJoining;
	}

	public String getJntuRegNo() {
		return this.jntuRegNo;
	}

	public void setJntuRegNo(String jntuRegNo) {
		this.jntuRegNo = jntuRegNo;
	}

	public Date getJoiningDate() {
		return this.joiningDate;
	}

	public void setJoiningDate(Date joiningDate) {
		this.joiningDate = joiningDate;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLicNo() {
		return this.licNo;
	}

	public void setLicNo(String licNo) {
		this.licNo = licNo;
	}

	public String getLinkedinUrl() {
		return this.linkedinUrl;
	}

	public void setLinkedinUrl(String linkedinUrl) {
		this.linkedinUrl = linkedinUrl;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getMonthlySalary() {
		return this.monthlySalary;
	}

	public void setMonthlySalary(Integer monthlySalary) {
		this.monthlySalary = monthlySalary;
	}

	public String getMotherName() {
		return this.motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public Integer getNonTeachingExp() {
		return this.nonTeachingExp;
	}

	public void setNonTeachingExp(Integer nonTeachingExp) {
		this.nonTeachingExp = nonTeachingExp;
	}

	public String getOfficialMobile() {
		return this.officialMobile;
	}

	public void setOfficialMobile(String officialMobile) {
		this.officialMobile = officialMobile;
	}

	public String getPancard() {
		return this.pancard;
	}

	public void setPancard(String pancard) {
		this.pancard = pancard;
	}

	public String getPancardPath() {
		return this.pancardPath;
	}

	public void setPancardPath(String pancardPath) {
		this.pancardPath = pancardPath;
	}

	public String getPassportNo() {
		return this.passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public String getPassportPath() {
		return this.passportPath;
	}

	public void setPassportPath(String passportPath) {
		this.passportPath = passportPath;
	}

	public String getPermanentAddress() {
		return this.permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getPermanentMandal() {
		return this.permanentMandal;
	}

	public void setPermanentMandal(String permanentMandal) {
		this.permanentMandal = permanentMandal;
	}

	public String getPermanentPincode() {
		return this.permanentPincode;
	}

	public void setPermanentPincode(String permanentPincode) {
		this.permanentPincode = permanentPincode;
	}

	public String getPermanentStreet() {
		return this.permanentStreet;
	}

	public void setPermanentStreet(String permanentStreet) {
		this.permanentStreet = permanentStreet;
	}

	public String getPhotoPath() {
		return this.photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public String getPresentAddress() {
		return this.presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getPresentMandal() {
		return this.presentMandal;
	}

	public void setPresentMandal(String presentMandal) {
		this.presentMandal = presentMandal;
	}

	public String getPresentPincode() {
		return this.presentPincode;
	}

	public void setPresentPincode(String presentPincode) {
		this.presentPincode = presentPincode;
	}

	public String getPresentStreet() {
		return this.presentStreet;
	}

	public void setPresentStreet(String presentStreet) {
		this.presentStreet = presentStreet;
	}

	public Date getPromotedDate() {
		return this.promotedDate;
	}

	public void setPromotedDate(Date promotedDate) {
		this.promotedDate = promotedDate;
	}

	public Long getResearchExp() {
		return this.researchExp;
	}

	public void setResearchExp(Long researchExp) {
		this.researchExp = researchExp;
	}

	public String getResidencePhone() {
		return this.residencePhone;
	}

	public void setResidencePhone(String residencePhone) {
		this.residencePhone = residencePhone;
	}

	public Date getResignationDate() {
		return this.resignationDate;
	}

	public void setResignationDate(Date resignationDate) {
		this.resignationDate = resignationDate;
	}

	public String getRfid() {
		return this.rfid;
	}

	public void setRfid(String rfid) {
		this.rfid = rfid;
	}

	public Integer getServiceBreakYrs() {
		return this.serviceBreakYrs;
	}

	public void setServiceBreakYrs(Integer serviceBreakYrs) {
		this.serviceBreakYrs = serviceBreakYrs;
	}

	public String getStatusDescription() {
		return this.statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public Integer getTeachingExp() {
		return this.teachingExp;
	}

	public void setTeachingExp(Integer teachingExp) {
		this.teachingExp = teachingExp;
	}

	public Integer getTenureDays() {
		return this.tenureDays;
	}

	public void setTenureDays(Integer tenureDays) {
		this.tenureDays = tenureDays;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getVoterId() {
		return this.voterId;
	}

	public void setVoterId(String voterId) {
		this.voterId = voterId;
	}

	public String getVoterIdPath() {
		return this.voterIdPath;
	}

	public void setVoterIdPath(String voterIdPath) {
		this.voterIdPath = voterIdPath;
	}

	public Date getWeddingDate() {
		return this.weddingDate;
	}

	public void setWeddingDate(Date weddingDate) {
		this.weddingDate = weddingDate;
	}

	public List<StaffCourseyrSubject> getStaffCourseyrSubjects() {
		return this.staffCourseyrSubjects;
	}

	public void setStaffCourseyrSubjects(List<StaffCourseyrSubject> staffCourseyrSubjects) {
		this.staffCourseyrSubjects = staffCourseyrSubjects;
	}

	public StaffCourseyrSubject addStaffCourseyrSubject(StaffCourseyrSubject staffCourseyrSubject) {
		getStaffCourseyrSubjects().add(staffCourseyrSubject);
		staffCourseyrSubject.setEmployeeDetail(this);

		return staffCourseyrSubject;
	}

	public StaffCourseyrSubject removeStaffCourseyrSubject(StaffCourseyrSubject staffCourseyrSubject) {
		getStaffCourseyrSubjects().remove(staffCourseyrSubject);
		staffCourseyrSubject.setEmployeeDetail(null);

		return staffCourseyrSubject;
	}

	public List<EmployeeAttendance> getEmpAttendances1() {
		return this.empAttendances1;
	}

	public void setEmpAttendances1(List<EmployeeAttendance> empAttendances1) {
		this.empAttendances1 = empAttendances1;
	}

	public EmployeeAttendance addEmpAttendances1(EmployeeAttendance empAttendances1) {
		getEmpAttendances1().add(empAttendances1);
		empAttendances1.setEmployeeDetail(this);

		return empAttendances1;
	}

	public EmployeeAttendance removeEmpAttendances1(EmployeeAttendance empAttendances1) {
		getEmpAttendances1().remove(empAttendances1);
		empAttendances1.setEmployeeDetail(null);

		return empAttendances1;
	}

	public List<EmployeeAttendance> getEmpAttendances2() {
		return this.empAttendances2;
	}

	public void setEmpAttendances2(List<EmployeeAttendance> empAttendances2) {
		this.empAttendances2 = empAttendances2;
	}

	public EmployeeAttendance addEmpAttendances2(EmployeeAttendance empAttendances2) {
		getEmpAttendances2().add(empAttendances2);
		empAttendances2.setVerifiedbyEmployeeDetail(this);

		return empAttendances2;
	}

	public EmployeeAttendance removeEmpAttendances2(EmployeeAttendance empAttendances2) {
		getEmpAttendances2().remove(empAttendances2);
		empAttendances2.setVerifiedbyEmployeeDetail(null);

		return empAttendances2;
	}

	public List<EmployeeDailylog> getEmpDailylogs() {
		return this.empDailylogs;
	}

	public void setEmpDailylogs(List<EmployeeDailylog> empDailylogs) {
		this.empDailylogs = empDailylogs;
	}

	public EmployeeDailylog addEmpDailylog(EmployeeDailylog empDailylog) {
		getEmpDailylogs().add(empDailylog);
		empDailylog.setEmployeeDetail(this);

		return empDailylog;
	}

	public EmployeeDailylog removeEmpDailylog(EmployeeDailylog empDailylog) {
		getEmpDailylogs().remove(empDailylog);
		empDailylog.setEmployeeDetail(null);

		return empDailylog;
	}

	public Caste getCaste() {
		return this.caste;
	}

	public void setCaste(Caste caste) {
		this.caste = caste;
	}

	public City getPresentCity() {
		return this.presentCity;
	}

	public void setPresentCity(City presentCity) {
		this.presentCity = presentCity;
	}

	public City getPermanentCity() {
		return this.permanentCity;
	}

	public void setPermanentCity(City permanentCity) {
		this.permanentCity = permanentCity;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Department getEmployeeDepartment() {
		return this.employeeDepartment;
	}

	public void setEmployeeDepartment(Department employeeDepartment) {
		this.employeeDepartment = employeeDepartment;
	}

	public Department getEmployeeWorkingDepartment() {
		return this.employeeWorkingDepartment;
	}

	public void setEmployeeWorkingDepartment(Department employeeWorkingDepartment) {
		this.employeeWorkingDepartment = employeeWorkingDepartment;
	}

	public Designation getDesignation() {
		return this.designation;
	}

	public void setDesignation(Designation designation) {
		this.designation = designation;
	}

	public Designation getWorkingDesignation() {
		return this.workingDesignation;
	}

	public void setWorkingDesignation(Designation workingDesignation) {
		this.workingDesignation = workingDesignation;
	}

	public District getDistrict() {
		return this.district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public District getPresentDistrict() {
		return this.presentDistrict;
	}

	public void setPresentDistrict(District presentDistrict) {
		this.presentDistrict = presentDistrict;
	}

	public District getPermanentDistrict() {
		return this.permanentDistrict;
	}

	public void setPermanentDistrict(District permanentDistrict) {
		this.permanentDistrict = permanentDistrict;
	}

	public GeneralDetail getMaritalStatus() {
		return this.maritalStatus;
	}

	public void setMaritalStatus(GeneralDetail maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public GeneralDetail getEmployeeStatus() {
		return this.employeeStatus;
	}

	public void setEmployeeStatus(GeneralDetail employeeStatus) {
		this.employeeStatus = employeeStatus;
	}

	public GeneralDetail getEmployeeState() {
		return this.employeeState;
	}

	public void setEmployeeState(GeneralDetail employeeState) {
		this.employeeState = employeeState;
	}

	public GeneralDetail getEmployeeType() {
		return this.employeeType;
	}

	public void setEmployeeType(GeneralDetail employeeType) {
		this.employeeType = employeeType;
	}

	public GeneralDetail getEmployeeCategory() {
		return this.employeeCategory;
	}

	public void setEmployeeCategory(GeneralDetail employeeCategory) {
		this.employeeCategory = employeeCategory;
	}

	public GeneralDetail getEmployeeWorkCategory() {
		return this.employeeWorkCategory;
	}

	public void setEmployeeWorkCategory(GeneralDetail employeeWorkCategory) {
		this.employeeWorkCategory = employeeWorkCategory;
	}

	public GeneralDetail getTeachingFor() {
		return this.teachingFor;
	}

	public void setTeachingFor(GeneralDetail teachingFor) {
		this.teachingFor = teachingFor;
	}

	public GeneralDetail getAppointment() {
		return this.appointment;
	}

	public void setAppointment(GeneralDetail appointment) {
		this.appointment = appointment;
	}

	public GeneralDetail getBloodGroup() {
		return this.bloodGroup;
	}

	public void setBloodGroup(GeneralDetail bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public GeneralDetail getPaymode() {
		return this.paymode;
	}

	public void setPaymode(GeneralDetail paymode) {
		this.paymode = paymode;
	}

	public GeneralDetail getResident() {
		return this.resident;
	}

	public void setResident(GeneralDetail resident) {
		this.resident = resident;
	}

	public GeneralDetail getAccommodation() {
		return this.accommodation;
	}

	public void setAccommodation(GeneralDetail accommodation) {
		this.accommodation = accommodation;
	}

	public GeneralDetail getTitle() {
		return this.title;
	}

	public void setTitle(GeneralDetail title) {
		this.title = title;
	}

	public GeneralDetail getGender() {
		return this.gender;
	}

	public void setGender(GeneralDetail gender) {
		this.gender = gender;
	}

	public GeneralDetail getReligion() {
		return this.religion;
	}

	public void setReligion(GeneralDetail religion) {
		this.religion = religion;
	}

	public GeneralDetail getNationality() {
		return this.nationality;
	}

	public void setNationality(GeneralDetail nationality) {
		this.nationality = nationality;
	}

	public GeneralDetail getEmployeeGrade() {
		return this.employeeGrade;
	}

	public void setEmployeeGrade(GeneralDetail employeeGrade) {
		this.employeeGrade = employeeGrade;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Qualification getQualification() {
		return this.qualification;
	}

	public void setQualification(Qualification qualification) {
		this.qualification = qualification;
	}

	public SubCaste getSubCaste() {
		return this.subCaste;
	}

	public void setSubCaste(SubCaste subCaste) {
		this.subCaste = subCaste;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<EmployeeDocumentCollection> getEmpDocumentCollections1() {
		return this.empDocumentCollections1;
	}

	public void setEmpDocumentCollections1(List<EmployeeDocumentCollection> empDocumentCollections1) {
		this.empDocumentCollections1 = empDocumentCollections1;
	}

	public EmployeeDocumentCollection addEmpDocumentCollections1(EmployeeDocumentCollection empDocumentCollections1) {
		getEmpDocumentCollections1().add(empDocumentCollections1);
		empDocumentCollections1.setEmployeeDetail(this);

		return empDocumentCollections1;
	}

	public EmployeeDocumentCollection removeEmpDocumentCollections1(EmployeeDocumentCollection empDocumentCollections1) {
		getEmpDocumentCollections1().remove(empDocumentCollections1);
		empDocumentCollections1.setEmployeeDetail(null);

		return empDocumentCollections1;
	}

	public List<EmployeeDocumentCollection> getEmpDocumentCollections2() {
		return this.empDocumentCollections2;
	}

	public void setEmpDocumentCollections2(List<EmployeeDocumentCollection> empDocumentCollections2) {
		this.empDocumentCollections2 = empDocumentCollections2;
	}

	public EmployeeDocumentCollection addEmpDocumentCollections2(EmployeeDocumentCollection empDocumentCollections2) {
		getEmpDocumentCollections2().add(empDocumentCollections2);
		empDocumentCollections2.setVerifiedByEmployeeDetail(this);

		return empDocumentCollections2;
	}

	public EmployeeDocumentCollection removeEmpDocumentCollections2(EmployeeDocumentCollection empDocumentCollections2) {
		getEmpDocumentCollections2().remove(empDocumentCollections2);
		empDocumentCollections2.setVerifiedByEmployeeDetail(null);

		return empDocumentCollections2;
	}

	public List<EmployeeBankDetail> getEmpEmployeeBankDetails() {
		return this.empEmployeeBankDetails;
	}

	public void setEmpEmployeeBankDetails(List<EmployeeBankDetail> empEmployeeBankDetails) {
		this.empEmployeeBankDetails = empEmployeeBankDetails;
	}

	public EmployeeBankDetail addEmpEmployeeBankDetail(EmployeeBankDetail empEmployeeBankDetail) {
		getEmpEmployeeBankDetails().add(empEmployeeBankDetail);
		empEmployeeBankDetail.setEmployeeDetail(this);

		return empEmployeeBankDetail;
	}

	public EmployeeBankDetail removeEmpEmployeeBankDetail(EmployeeBankDetail empEmployeeBankDetail) {
		getEmpEmployeeBankDetails().remove(empEmployeeBankDetail);
		empEmployeeBankDetail.setEmployeeDetail(null);

		return empEmployeeBankDetail;
	}

	public List<EmployeeEducation> getEmpEmployeeEducations() {
		return this.empEmployeeEducations;
	}

	public void setEmpEmployeeEducations(List<EmployeeEducation> empEmployeeEducations) {
		this.empEmployeeEducations = empEmployeeEducations;
	}

	public EmployeeEducation addEmpEmployeeEducation(EmployeeEducation empEmployeeEducation) {
		getEmpEmployeeEducations().add(empEmployeeEducation);
		empEmployeeEducation.setEmpDetail(this);

		return empEmployeeEducation;
	}

	public EmployeeEducation removeEmpEmployeeEducation(EmployeeEducation empEmployeeEducation) {
		getEmpEmployeeEducations().remove(empEmployeeEducation);
		empEmployeeEducation.setEmpDetail(null);

		return empEmployeeEducation;
	}

	public List<EmployeeReporting> getEmpEmployeeReportings1() {
		return this.empEmployeeReportings1;
	}

	public void setEmpEmployeeReportings1(List<EmployeeReporting> empEmployeeReportings1) {
		this.empEmployeeReportings1 = empEmployeeReportings1;
	}

	public EmployeeReporting addEmpEmployeeReportings1(EmployeeReporting empEmployeeReportings1) {
		getEmpEmployeeReportings1().add(empEmployeeReportings1);
		empEmployeeReportings1.setEmployeeDetail(this);

		return empEmployeeReportings1;
	}

	public EmployeeReporting removeEmpEmployeeReportings1(EmployeeReporting empEmployeeReportings1) {
		getEmpEmployeeReportings1().remove(empEmployeeReportings1);
		empEmployeeReportings1.setEmployeeDetail(null);

		return empEmployeeReportings1;
	}

	public List<EmployeeReporting> getEmpEmployeeReportings2() {
		return this.empEmployeeReportings2;
	}

	public void setEmpEmployeeReportings2(List<EmployeeReporting> empEmployeeReportings2) {
		this.empEmployeeReportings2 = empEmployeeReportings2;
	}

	public EmployeeReporting addEmpEmployeeReportings2(EmployeeReporting empEmployeeReportings2) {
		getEmpEmployeeReportings2().add(empEmployeeReportings2);
		empEmployeeReportings2.setManagerEmployeeDetail(this);

		return empEmployeeReportings2;
	}

	public EmployeeReporting removeEmpEmployeeReportings2(EmployeeReporting empEmployeeReportings2) {
		getEmpEmployeeReportings2().remove(empEmployeeReportings2);
		empEmployeeReportings2.setManagerEmployeeDetail(null);

		return empEmployeeReportings2;
	}

	public List<EmployeeExperienceDetail> getEmpExperienceDetails() {
		return this.empExperienceDetails;
	}

	public void setEmpExperienceDetails(List<EmployeeExperienceDetail> empExperienceDetails) {
		this.empExperienceDetails = empExperienceDetails;
	}

	public EmployeeExperienceDetail addEmpExperienceDetail(EmployeeExperienceDetail empExperienceDetail) {
		getEmpExperienceDetails().add(empExperienceDetail);
		empExperienceDetail.setEmployeeDetail(this);

		return empExperienceDetail;
	}

	public EmployeeExperienceDetail removeEmpExperienceDetail(EmployeeExperienceDetail empExperienceDetail) {
		getEmpExperienceDetails().remove(empExperienceDetail);
		empExperienceDetail.setEmployeeDetail(null);

		return empExperienceDetail;
	}

	public List<FeeInstantPayment> getFeeInstantPayments() {
		return this.feeInstantPayments;
	}

	public void setFeeInstantPayments(List<FeeInstantPayment> feeInstantPayments) {
		this.feeInstantPayments = feeInstantPayments;
	}

	public FeeInstantPayment addFeeInstantPayment(FeeInstantPayment feeInstantPayment) {
		getFeeInstantPayments().add(feeInstantPayment);
		feeInstantPayment.setEmployeeDetail(this);

		return feeInstantPayment;
	}

	public FeeInstantPayment removeFeeInstantPayment(FeeInstantPayment feeInstantPayment) {
		getFeeInstantPayments().remove(feeInstantPayment);
		feeInstantPayment.setEmployeeDetail(null);

		return feeInstantPayment;
	}

	public List<FeeParticularwisePayment> getFeeParticularwisePayments() {
		return this.feeParticularwisePayments;
	}

	public void setFeeParticularwisePayments(List<FeeParticularwisePayment> feeParticularwisePayments) {
		this.feeParticularwisePayments = feeParticularwisePayments;
	}

	public FeeParticularwisePayment addFeeParticularwisePayment(FeeParticularwisePayment feeParticularwisePayment) {
		getFeeParticularwisePayments().add(feeParticularwisePayment);
		feeParticularwisePayment.setEmployeeDetail(this);

		return feeParticularwisePayment;
	}

	public FeeParticularwisePayment removeFeeParticularwisePayment(FeeParticularwisePayment feeParticularwisePayment) {
		getFeeParticularwisePayments().remove(feeParticularwisePayment);
		feeParticularwisePayment.setEmployeeDetail(null);

		return feeParticularwisePayment;
	}

	public List<FeeReceipt> getFeeReceipts() {
		return this.feeReceipts;
	}

	public void setFeeReceipts(List<FeeReceipt> feeReceipts) {
		this.feeReceipts = feeReceipts;
	}

	public FeeReceipt addFeeReceipt(FeeReceipt feeReceipt) {
		getFeeReceipts().add(feeReceipt);
		feeReceipt.setEmployeeDetail(this);

		return feeReceipt;
	}

	public FeeReceipt removeFeeReceipt(FeeReceipt feeReceipt) {
		getFeeReceipts().remove(feeReceipt);
		feeReceipt.setEmployeeDetail(null);

		return feeReceipt;
	}

	public List<FeeReceipt> getFeeReceipts2() {
		return this.feeReceipts2;
	}

	public void setFeeReceipts2(List<FeeReceipt> feeReceipts2) {
		this.feeReceipts2 = feeReceipts2;
	}

	public FeeReceipt addFeeReceipts2(FeeReceipt feeReceipts2) {
		getFeeReceipts2().add(feeReceipts2);
		feeReceipts2.setRevertbByEmployeeDetail(this);

		return feeReceipts2;
	}

	public FeeReceipt removeFeeReceipts2(FeeReceipt feeReceipts2) {
		getFeeReceipts2().remove(feeReceipts2);
		feeReceipts2.setRevertbByEmployeeDetail(null);

		return feeReceipts2;
	}

	public List<FeeStudentRefund> getFeeStudentRefunds() {
		return this.feeStudentRefunds;
	}

	public void setFeeStudentRefunds(List<FeeStudentRefund> feeStudentRefunds) {
		this.feeStudentRefunds = feeStudentRefunds;
	}

	public FeeStudentRefund addFeeStudentRefund(FeeStudentRefund feeStudentRefund) {
		getFeeStudentRefunds().add(feeStudentRefund);
		feeStudentRefund.setEmployeeDetail(this);

		return feeStudentRefund;
	}

	public FeeStudentRefund removeFeeStudentRefund(FeeStudentRefund feeStudentRefund) {
		getFeeStudentRefunds().remove(feeStudentRefund);
		feeStudentRefund.setEmployeeDetail(null);

		return feeStudentRefund;
	}

	public List<FeeStudentwiseDiscount> getFeeStudentwiseDiscounts1() {
		return this.feeStudentwiseDiscounts1;
	}

	public void setFeeStudentwiseDiscounts1(List<FeeStudentwiseDiscount> feeStudentwiseDiscounts1) {
		this.feeStudentwiseDiscounts1 = feeStudentwiseDiscounts1;
	}

	public FeeStudentwiseDiscount addFeeStudentwiseDiscounts1(FeeStudentwiseDiscount feeStudentwiseDiscounts1) {
		getFeeStudentwiseDiscounts1().add(feeStudentwiseDiscounts1);
		feeStudentwiseDiscounts1.setRequestedtoEmployeeDetail(this);

		return feeStudentwiseDiscounts1;
	}

	public FeeStudentwiseDiscount removeFeeStudentwiseDiscounts1(FeeStudentwiseDiscount feeStudentwiseDiscounts1) {
		getFeeStudentwiseDiscounts1().remove(feeStudentwiseDiscounts1);
		feeStudentwiseDiscounts1.setRequestedtoEmployeeDetail(null);

		return feeStudentwiseDiscounts1;
	}

	public List<FeeStudentwiseDiscount> getFeeStudentwiseDiscounts2() {
		return this.feeStudentwiseDiscounts2;
	}

	public void setFeeStudentwiseDiscounts2(List<FeeStudentwiseDiscount> feeStudentwiseDiscounts2) {
		this.feeStudentwiseDiscounts2 = feeStudentwiseDiscounts2;
	}

	public FeeStudentwiseDiscount addFeeStudentwiseDiscounts2(FeeStudentwiseDiscount feeStudentwiseDiscounts2) {
		getFeeStudentwiseDiscounts2().add(feeStudentwiseDiscounts2);
		feeStudentwiseDiscounts2.setAuthbyEmployee(this);

		return feeStudentwiseDiscounts2;
	}

	public FeeStudentwiseDiscount removeFeeStudentwiseDiscounts2(FeeStudentwiseDiscount feeStudentwiseDiscounts2) {
		getFeeStudentwiseDiscounts2().remove(feeStudentwiseDiscounts2);
		feeStudentwiseDiscounts2.setAuthbyEmployee(null);

		return feeStudentwiseDiscounts2;
	}

	public List<FeeTransactionMaster> getFeeTransactionMasters() {
		return this.feeTransactionMasters;
	}

	public void setFeeTransactionMasters(List<FeeTransactionMaster> feeTransactionMasters) {
		this.feeTransactionMasters = feeTransactionMasters;
	}

	public FeeTransactionMaster addFeeTransactionMaster(FeeTransactionMaster feeTransactionMaster) {
		getFeeTransactionMasters().add(feeTransactionMaster);
		feeTransactionMaster.setCollectedEmployeeDetail(this);

		return feeTransactionMaster;
	}

	public FeeTransactionMaster removeFeeTransactionMaster(FeeTransactionMaster feeTransactionMaster) {
		getFeeTransactionMasters().remove(feeTransactionMaster);
		feeTransactionMaster.setCollectedEmployeeDetail(null);

		return feeTransactionMaster;
	}

	public List<EmployeePayrollGroup> getEmployeePayrollGroups() {
		return this.employeePayrollGroups;
	}

	public void setEmployeePayrollGroups(List<EmployeePayrollGroup> employeePayrollGroups) {
		this.employeePayrollGroups = employeePayrollGroups;
	}

	public EmployeePayrollGroup addEmployeePayrollGroup(EmployeePayrollGroup employeePayrollGroup) {
		getEmployeePayrollGroups().add(employeePayrollGroup);
		employeePayrollGroup.setEmployeeDetail(this);

		return employeePayrollGroup;
	}

	public EmployeePayrollGroup removeEmployeePayrollGroup(EmployeePayrollGroup employeePayrollGroup) {
		getEmployeePayrollGroups().remove(employeePayrollGroup);
		employeePayrollGroup.setEmployeeDetail(null);

		return employeePayrollGroup;
	}

	public List<EmployeePayslipGeneration> getEmployeePayslipGenerations1() {
		return this.employeePayslipGenerations1;
	}

	public void setEmployeePayslipGenerations1(List<EmployeePayslipGeneration> employeePayslipGenerations1) {
		this.employeePayslipGenerations1 = employeePayslipGenerations1;
	}

	public EmployeePayslipGeneration addEmployeePayslipGenerations1(EmployeePayslipGeneration employeePayslipGenerations1) {
		getEmployeePayslipGenerations1().add(employeePayslipGenerations1);
		employeePayslipGenerations1.setEmployeeDetail(this);

		return employeePayslipGenerations1;
	}

	public EmployeePayslipGeneration removeEmployeePayslipGenerations1(EmployeePayslipGeneration employeePayslipGenerations1) {
		getEmployeePayslipGenerations1().remove(employeePayslipGenerations1);
		employeePayslipGenerations1.setEmployeeDetail(null);

		return employeePayslipGenerations1;
	}

	public List<EmployeePayslipGeneration> getEmployeePayslipGenerations2() {
		return this.employeePayslipGenerations2;
	}

	public void setEmployeePayslipGenerations2(List<EmployeePayslipGeneration> employeePayslipGenerations2) {
		this.employeePayslipGenerations2 = employeePayslipGenerations2;
	}

	public EmployeePayslipGeneration addEmployeePayslipGenerations2(EmployeePayslipGeneration employeePayslipGenerations2) {
		getEmployeePayslipGenerations2().add(employeePayslipGenerations2);
		employeePayslipGenerations2.setGeneratedByEmployeeDetail(this);

		return employeePayslipGenerations2;
	}

	public EmployeePayslipGeneration removeEmployeePayslipGenerations2(EmployeePayslipGeneration employeePayslipGenerations2) {
		getEmployeePayslipGenerations2().remove(employeePayslipGenerations2);
		employeePayslipGenerations2.setGeneratedByEmployeeDetail(null);

		return employeePayslipGenerations2;
	}

	public List<SurveyFeedback> getSurveyFeedbacks1() {
		return this.surveyFeedbacks1;
	}

	public void setSurveyFeedbacks1(List<SurveyFeedback> surveyFeedbacks1) {
		this.surveyFeedbacks1 = surveyFeedbacks1;
	}

	public SurveyFeedback addSurveyFeedbacks1(SurveyFeedback surveyFeedbacks1) {
		getSurveyFeedbacks1().add(surveyFeedbacks1);
		surveyFeedbacks1.setFromEmployeeDetail(this);

		return surveyFeedbacks1;
	}

	public SurveyFeedback removeSurveyFeedbacks1(SurveyFeedback surveyFeedbacks1) {
		getSurveyFeedbacks1().remove(surveyFeedbacks1);
		surveyFeedbacks1.setFromEmployeeDetail(null);

		return surveyFeedbacks1;
	}

	public List<SurveyFeedback> getSurveyFeedbacks2() {
		return this.surveyFeedbacks2;
	}

	public void setSurveyFeedbacks2(List<SurveyFeedback> surveyFeedbacks2) {
		this.surveyFeedbacks2 = surveyFeedbacks2;
	}

	public SurveyFeedback addSurveyFeedbacks2(SurveyFeedback surveyFeedbacks2) {
		getSurveyFeedbacks2().add(surveyFeedbacks2);
		surveyFeedbacks2.setForEmployeeDetail(this);

		return surveyFeedbacks2;
	}

	public SurveyFeedback removeSurveyFeedbacks2(SurveyFeedback surveyFeedbacks2) {
		getSurveyFeedbacks2().remove(surveyFeedbacks2);
		surveyFeedbacks2.setForEmployeeDetail(null);

		return surveyFeedbacks2;
	}

	public List<Event> getEvents() {
		return this.events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public Event addEvent(Event event) {
		getEvents().add(event);
		event.setInchargeEmployeeDetail(this);

		return event;
	}

	public Event removeEvent(Event event) {
		getEvents().remove(event);
		event.setInchargeEmployeeDetail(null);

		return event;
	}

	public List<Messaging> getMessagings() {
		return this.messagings;
	}

	public void setMessagings(List<Messaging> messagings) {
		this.messagings = messagings;
	}

	public Messaging addMessaging(Messaging messaging) {
		getMessagings().add(messaging);
		messaging.setSentByEmployeeDetail(this);

		return messaging;
	}

	public Messaging removeMessaging(Messaging messaging) {
		getMessagings().remove(messaging);
		messaging.setSentByEmployeeDetail(null);

		return messaging;
	}

	public List<MessagingRecipient> getMessagingRecipients() {
		return this.messagingRecipients;
	}

	public void setMessagingRecipients(List<MessagingRecipient> messagingRecipients) {
		this.messagingRecipients = messagingRecipients;
	}

	public MessagingRecipient addMessagingRecipient(MessagingRecipient messagingRecipient) {
		getMessagingRecipients().add(messagingRecipient);
		messagingRecipient.setEmployeeDetail(this);

		return messagingRecipient;
	}

	public MessagingRecipient removeMessagingRecipient(MessagingRecipient messagingRecipient) {
		getMessagingRecipients().remove(messagingRecipient);
		messagingRecipient.setEmployeeDetail(null);

		return messagingRecipient;
	}

	public List<StudentAppDocCollection> getStdAppDocCollections() {
		return this.stdAppDocCollections;
	}

	public void setStdAppDocCollections(List<StudentAppDocCollection> stdAppDocCollections) {
		this.stdAppDocCollections = stdAppDocCollections;
	}

	public StudentAppDocCollection addStdAppDocCollection(StudentAppDocCollection stdAppDocCollection) {
		getStdAppDocCollections().add(stdAppDocCollection);
		stdAppDocCollection.setVerifiedbyEmployeeDetail(this);

		return stdAppDocCollection;
	}

	public StudentAppDocCollection removeStdAppDocCollection(StudentAppDocCollection stdAppDocCollection) {
		getStdAppDocCollections().remove(stdAppDocCollection);
		stdAppDocCollection.setVerifiedbyEmployeeDetail(null);

		return stdAppDocCollection;
	}

	public List<StudentDocumentCollection> getStdDocumentCollections() {
		return this.stdDocumentCollections;
	}

	public void setStdDocumentCollections(List<StudentDocumentCollection> stdDocumentCollections) {
		this.stdDocumentCollections = stdDocumentCollections;
	}

	public StudentDocumentCollection addStdDocumentCollection(StudentDocumentCollection stdDocumentCollection) {
		getStdDocumentCollections().add(stdDocumentCollection);
		stdDocumentCollection.setVerifiedbyEmployeeDetail(this);

		return stdDocumentCollection;
	}

	public StudentDocumentCollection removeStdDocumentCollection(StudentDocumentCollection stdDocumentCollection) {
		getStdDocumentCollections().remove(stdDocumentCollection);
		stdDocumentCollection.setVerifiedbyEmployeeDetail(null);

		return stdDocumentCollection;
	}

	public List<TransportFeePayment> getFeePayments() {
		return this.feePayments;
	}

	public void setFeePayments(List<TransportFeePayment> feePayments) {
		this.feePayments = feePayments;
	}

	public TransportFeePayment addFeePayment(TransportFeePayment feePayment) {
		getFeePayments().add(feePayment);
		feePayment.setEmployeeDetail(this);

		return feePayment;
	}

	public TransportFeePayment removeFeePayment(TransportFeePayment feePayment) {
		getFeePayments().remove(feePayment);
		feePayment.setEmployeeDetail(null);

		return feePayment;
	}

	public List<TransportAllocation> getTransportAllocations() {
		return this.transportAllocations;
	}

	public void setTransportAllocations(List<TransportAllocation> transportAllocations) {
		this.transportAllocations = transportAllocations;
	}

	public TransportAllocation addTransportAllocation(TransportAllocation transportAllocation) {
		getTransportAllocations().add(transportAllocation);
		transportAllocation.setEmployeeDetail(this);

		return transportAllocation;
	}

	public TransportAllocation removeTransportAllocation(TransportAllocation transportAllocation) {
		getTransportAllocations().remove(transportAllocation);
		transportAllocation.setEmployeeDetail(null);

		return transportAllocation;
	}

	public List<VehicleMaintenance> getVehicleMaintenances() {
		return this.vehicleMaintenances;
	}

	public void setVehicleMaintenances(List<VehicleMaintenance> vehicleMaintenances) {
		this.vehicleMaintenances = vehicleMaintenances;
	}

	public VehicleMaintenance addVehicleMaintenance(VehicleMaintenance vehicleMaintenance) {
		getVehicleMaintenances().add(vehicleMaintenance);
		vehicleMaintenance.setApprovedEmployeeDetail(this);

		return vehicleMaintenance;
	}

	public VehicleMaintenance removeVehicleMaintenance(VehicleMaintenance vehicleMaintenance) {
		getVehicleMaintenances().remove(vehicleMaintenance);
		vehicleMaintenance.setApprovedEmployeeDetail(null);

		return vehicleMaintenance;
	}

	public List<StaffProxy> getStaffProxies() {
		return this.staffProxies;
	}

	public void setStaffProxies(List<StaffProxy> staffProxies) {
		this.staffProxies = staffProxies;
	}

	public StaffProxy addStaffProxy(StaffProxy staffProxy) {
		getStaffProxies().add(staffProxy);
		staffProxy.setApprovedByEmployeeDetail(this);

		return staffProxy;
	}

	public StaffProxy removeStaffProxy(StaffProxy staffProxy) {
		getStaffProxies().remove(staffProxy);
		staffProxy.setApprovedByEmployeeDetail(null);

		return staffProxy;
	}

	/*public List<StudentAttendance> getStdAttendances1() {
		return this.stdAttendances1;
	}

	public void setStdAttendances1(List<StudentAttendance> stdAttendances1) {
		this.stdAttendances1 = stdAttendances1;
	}
*/
	/*public StudentAttendance addStdAttendances1(StudentAttendance stdAttendances1) {
		getStdAttendances1().add(stdAttendances1);
		stdAttendances1.setTakenbyEmployeeDetail(this);

		return stdAttendances1;
	}

	public StudentAttendance removeStdAttendances1(StudentAttendance stdAttendances1) {
		getStdAttendances1().remove(stdAttendances1);
		stdAttendances1.setTakenbyEmployeeDetail(null);

		return stdAttendances1;
	}*/

	public List<StudentAttendance> getStdAttendances2() {
		return this.stdAttendances2;
	}

	public void setStdAttendances2(List<StudentAttendance> stdAttendances2) {
		this.stdAttendances2 = stdAttendances2;
	}

	public StudentAttendance addStdAttendances2(StudentAttendance stdAttendances2) {
		getStdAttendances2().add(stdAttendances2);
		stdAttendances2.setVerifiedbyEmployeeDetail(this);

		return stdAttendances2;
	}

	public StudentAttendance removeStdAttendances2(StudentAttendance stdAttendances2) {
		getStdAttendances2().remove(stdAttendances2);
		stdAttendances2.setVerifiedbyEmployeeDetail(null);

		return stdAttendances2;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public List<EmpDeptHeads> getEmpDeptHeads() {
		return empDeptHeads;
	}

	public void setEmpDeptHeads(List<EmpDeptHeads> empDeptHeads) {
		this.empDeptHeads = empDeptHeads;
	}
	
}