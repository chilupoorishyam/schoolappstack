package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_dl_assessments database table.
 * 
 */
@Entity
@Table(name = "t_dl_assessments")
@NamedQuery(name = "Assessment.findAll", query = "SELECT a FROM Assessment a")
public class Assessment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_dl_assessment_id")
	private Long assessmentId;

	@Column(name = "assessment_description")
	private String assessmentDescription;

	@Column(name = "assessment_name")
	private String assessmentName;

	@Column(name = "assessment_no")
	private Integer assessmentNo;

	@Column(name = "banner_url")
	private String bannerUrl;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	private Time duration;

	@Column(name = "exam_fee")
	private Integer examFee;

	@ManyToOne
	@JoinColumn(name = "fk_currency_catdet_id")
	private GeneralDetail currencyCat;

	@ManyToOne
	@JoinColumn(name = "fk_dl_course_lesson_id")
	private CourseLesson courseLesson;

	@ManyToOne
	@JoinColumn(name = "fk_dl_course_lesson_topic_id")
	private CourseLessonsTopic courseLessonTopic;

	@ManyToOne
	@JoinColumn(name = "fk_dl_onlinecourse_id")
	private OnlineCourses onlineCourses;

	@Column(name = "have_penalties_for_wrong_answers")
	private Boolean havePenaltiesForWrongAnswers;

	private String instructions;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_certification")
	private Boolean isCertification;

	@Column(name = "is_for_practice")
	private Boolean isForPractice;

	@Column(name = "is_for_questionbank")
	private Boolean isForQuestionbank;

	@Column(name = "is_systemcorrection")
	private Boolean isSystemcorrection;

	private Integer level;

	@Column(name = "logo_url")
	private String logoUrl;

	@Column(name = "min_marks_percentage")
	private Integer minMarksPercentage;

	@Column(name = "min_marks_to_pass")
	private Integer minMarksToPass;

	@Column(name = "no_of_max_attempts")
	private Integer noOfMaxAttempts;

	@Column(name = "progresscard_reportname")
	private String progresscardReportname;

	private String reason;

	@Column(name = "required_assessment_percentage")
	private Integer requiredAssessmentPercentage;

	@Column(name = "required_video_completed_percentage")
	private Integer requiredVideoCompletedPercentage;

	@Column(name = "total_questions")
	private Integer totalQuestions;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "validity_dates")
	private Integer validityDates;

	// bi-directional many-to-one association to AssessmentQuestion
	@OneToMany(mappedBy = "assessment", cascade = CascadeType.ALL)
	private List<AssessmentQuestion> assessmentQuestions;

	@OneToMany(mappedBy = "assessment", cascade = CascadeType.ALL)
	private List<AssessmentSettings> assessmentSettings;

	public Assessment() {
	}

	public Long getAssessmentId() {
		return this.assessmentId;
	}

	public void setAssessmentId(Long assessmentId) {
		this.assessmentId = assessmentId;
	}

	public String getAssessmentDescription() {
		return this.assessmentDescription;
	}

	public void setAssessmentDescription(String assessmentDescription) {
		this.assessmentDescription = assessmentDescription;
	}

	public String getAssessmentName() {
		return this.assessmentName;
	}

	public void setAssessmentName(String assessmentName) {
		this.assessmentName = assessmentName;
	}

	public Integer getAssessmentNo() {
		return this.assessmentNo;
	}

	public void setAssessmentNo(Integer assessmentNo) {
		this.assessmentNo = assessmentNo;
	}

	public String getBannerUrl() {
		return this.bannerUrl;
	}

	public void setBannerUrl(String bannerUrl) {
		this.bannerUrl = bannerUrl;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Time getDuration() {
		return this.duration;
	}

	public void setDuration(Time duration) {
		this.duration = duration;
	}

	public Integer getExamFee() {
		return this.examFee;
	}

	public void setExamFee(Integer examFee) {
		this.examFee = examFee;
	}

	public Boolean getHavePenaltiesForWrongAnswers() {
		return this.havePenaltiesForWrongAnswers;
	}

	public void setHavePenaltiesForWrongAnswers(Boolean havePenaltiesForWrongAnswers) {
		this.havePenaltiesForWrongAnswers = havePenaltiesForWrongAnswers;
	}

	public String getInstructions() {
		return this.instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsCertification() {
		return this.isCertification;
	}

	public void setIsCertification(Boolean isCertification) {
		this.isCertification = isCertification;
	}

	public Boolean getIsForPractice() {
		return this.isForPractice;
	}

	public void setIsForPractice(Boolean isForPractice) {
		this.isForPractice = isForPractice;
	}

	public Boolean getIsForQuestionbank() {
		return this.isForQuestionbank;
	}

	public void setIsForQuestionbank(Boolean isForQuestionbank) {
		this.isForQuestionbank = isForQuestionbank;
	}

	public Boolean getIsSystemcorrection() {
		return this.isSystemcorrection;
	}

	public void setIsSystemcorrection(Boolean isSystemcorrection) {
		this.isSystemcorrection = isSystemcorrection;
	}

	public Integer getLevel() {
		return this.level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getLogoUrl() {
		return this.logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public Integer getMinMarksPercentage() {
		return this.minMarksPercentage;
	}

	public void setMinMarksPercentage(Integer minMarksPercentage) {
		this.minMarksPercentage = minMarksPercentage;
	}

	public Integer getMinMarksToPass() {
		return this.minMarksToPass;
	}

	public void setMinMarksToPass(Integer minMarksToPass) {
		this.minMarksToPass = minMarksToPass;
	}

	public Integer getNoOfMaxAttempts() {
		return this.noOfMaxAttempts;
	}

	public void setNoOfMaxAttempts(Integer noOfMaxAttempts) {
		this.noOfMaxAttempts = noOfMaxAttempts;
	}

	public String getProgresscardReportname() {
		return this.progresscardReportname;
	}

	public void setProgresscardReportname(String progresscardReportname) {
		this.progresscardReportname = progresscardReportname;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getRequiredAssessmentPercentage() {
		return this.requiredAssessmentPercentage;
	}

	public void setRequiredAssessmentPercentage(Integer requiredAssessmentPercentage) {
		this.requiredAssessmentPercentage = requiredAssessmentPercentage;
	}

	public Integer getRequiredVideoCompletedPercentage() {
		return this.requiredVideoCompletedPercentage;
	}

	public void setRequiredVideoCompletedPercentage(Integer requiredVideoCompletedPercentage) {
		this.requiredVideoCompletedPercentage = requiredVideoCompletedPercentage;
	}

	public Integer getTotalQuestions() {
		return this.totalQuestions;
	}

	public void setTotalQuestions(Integer totalQuestions) {
		this.totalQuestions = totalQuestions;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Integer getValidityDates() {
		return this.validityDates;
	}

	public void setValidityDates(Integer validityDates) {
		this.validityDates = validityDates;
	}

	public GeneralDetail getCurrencyCat() {
		return currencyCat;
	}

	public void setCurrencyCat(GeneralDetail currencyCat) {
		this.currencyCat = currencyCat;
	}

	public CourseLesson getCourseLesson() {
		return courseLesson;
	}

	public void setCourseLesson(CourseLesson courseLesson) {
		this.courseLesson = courseLesson;
	}

	public CourseLessonsTopic getCourseLessonTopic() {
		return courseLessonTopic;
	}

	public void setCourseLessonTopic(CourseLessonsTopic courseLessonTopic) {
		this.courseLessonTopic = courseLessonTopic;
	}

	public OnlineCourses getOnlineCourses() {
		return onlineCourses;
	}

	public void setOnlineCourses(OnlineCourses onlineCourses) {
		this.onlineCourses = onlineCourses;
	}

	public List<AssessmentQuestion> getAssessmentQuestions() {
		return assessmentQuestions;
	}

	public void setAssessmentQuestions(List<AssessmentQuestion> assessmentQuestions) {
		this.assessmentQuestions = assessmentQuestions;
	}

	public void setAssessmentSettings(List<AssessmentSettings> assessmentSettings) {
		this.assessmentSettings = assessmentSettings;
	}

	public List<AssessmentSettings> getAssessmentSettings() {
		return assessmentSettings;
	}
}