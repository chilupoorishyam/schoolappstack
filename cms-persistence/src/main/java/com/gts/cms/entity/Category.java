package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_pa_categories database table.
 * 
 */
@Entity
@Table(name = "t_pa_categories")
@NamedQuery(name = "Category.findAll", query = "SELECT c FROM Category c")
public class Category implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_pa_category_id")
	private Long categoryId;

	@Column(name = "achievement_category")
	private String achievementCategory;

	@Column(name = "achievement_category_code")
	private String achievementCategoryCode;

	@Column(name = "category_logo_path")
	private String categoryLogoPath;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	// bi-directional many-to-one association to SubCategory
	@OneToMany(mappedBy = "category")
	private List<SubCategory> subCategories;

	public Category() {
	}

	public Long getCategoryId() {
		return this.categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getAchievementCategory() {
		return this.achievementCategory;
	}

	public void setAchievementCategory(String achievementCategory) {
		this.achievementCategory = achievementCategory;
	}

	public String getAchievementCategoryCode() {
		return this.achievementCategoryCode;
	}

	public void setAchievementCategoryCode(String achievementCategoryCode) {
		this.achievementCategoryCode = achievementCategoryCode;
	}

	public String getCategoryLogoPath() {
		return this.categoryLogoPath;
	}

	public void setCategoryLogoPath(String categoryLogoPath) {
		this.categoryLogoPath = categoryLogoPath;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<SubCategory> getSubCategories() {
		return subCategories;
	}

	public void setSubCategories(List<SubCategory> subCategories) {
		this.subCategories = subCategories;
	}

	public SubCategory addSubCategory(SubCategory SubCategory) {
		getSubCategories().add(SubCategory);
		SubCategory.setCategory(this);

		return SubCategory;
	}

	public SubCategory removeSubCategory(SubCategory SubCategory) {
		getSubCategories().remove(SubCategory);
		SubCategory.setCategory(null);

		return SubCategory;
	}

}