package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_exam_subjectwise_evaluation database table.
 */
@Data
@Entity
@Table(name = "t_exam_subjectwise_evaluation")
@NamedQuery(name = "ExamSubjectwiseEvaluation.findAll", query = "SELECT e FROM ExamSubjectwiseEvaluation e")
public class ExamSubjectwiseEvaluation implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_exam_sub_evaluation_id")
    private Long examSubEvaluationId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "evaluation_name")
    private String evaluationName;

    @Column(name = "maxmarks")
    private int maxmarks;

    @Column(name = "is_summative")
    private Boolean isSummative;

    @Column(name = "is_formative")
    private Boolean isFormative;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "from_date")
    private Date fromDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "toDate")
    private Date toDate;

    @Column(name = "is_havingexam")
    private Boolean isHavingexam;

    // bi-directional many-to-one association to AcademicYear
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_school_id")
    private School school;

    // bi-directional many-to-one association to AcademicYear
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_subject_id")
    private Subject subject;

    // bi-directional many-to-one association to AcademicYear
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_course_year_id")
    private CourseYear courseYear;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;
}