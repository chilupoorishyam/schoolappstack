package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_fin_category database table.
 */
@Entity
@Table(name = "t_fin_category")
@NamedQuery(name = "FinCategory.findAll", query = "SELECT f FROM FinCategory f")
public class FinCategory implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_fin_category_id")
    private Long finCategoryId;

    @Column(name = "fin_category_name")
    private String categoryName;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    private String description;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    //bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_school_id")
    private School school;

    //bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_account_type_id")
    private FinAccountType accountTypeId;

    //bi-directional many-to-one association to FinSubCategory
    @OneToMany(mappedBy = "finCategory", fetch = FetchType.LAZY)
    private List<FinSubCategory> finSubCategories;

    @Column(name = "fin_category_code")
    private String finCategoryCode;

    public FinCategory() {
    }

    public Long getFinCategoryId() {
        return this.finCategoryId;
    }

    public void setFinCategoryId(Long finCategoryId) {
        this.finCategoryId = finCategoryId;
    }

    public String getCategoryName() {
        return this.categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Date getCreatedDt() {
        return this.createdDt;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    public Long getCreatedUser() {
        return this.createdUser;
    }

    public void setCreatedUser(Long createdUser) {
        this.createdUser = createdUser;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getUpdatedDt() {
        return this.updatedDt;
    }

    public void setUpdatedDt(Date updatedDt) {
        this.updatedDt = updatedDt;
    }

    public Long getUpdatedUser() {
        return this.updatedUser;
    }

    public void setUpdatedUser(Long updatedUser) {
        this.updatedUser = updatedUser;
    }

    public School getSchool() {
        return this.school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public List<FinSubCategory> getFinSubCategories() {
        return this.finSubCategories;
    }

    public void setFinSubCategories(List<FinSubCategory> finSubCategories) {
        this.finSubCategories = finSubCategories;
    }

    public FinSubCategory addFinSubCategory(FinSubCategory finSubCategory) {
        getFinSubCategories().add(finSubCategory);
        finSubCategory.setFinCategory(this);

        return finSubCategory;
    }

    public FinSubCategory removeFinSubCategory(FinSubCategory finSubCategory) {
        getFinSubCategories().remove(finSubCategory);
        finSubCategory.setFinCategory(null);

        return finSubCategory;
    }

    public String getFinCategoryCode() {
        return finCategoryCode;
    }

    public void setFinCategoryCode(String finCategoryCode) {
        this.finCategoryCode = finCategoryCode;
    }

    public FinAccountType getAccountTypeId() {
        return accountTypeId;
    }

    public void setAccountTypeId(FinAccountType accountTypeId) {
        this.accountTypeId = accountTypeId;
    }
}