package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the t_m_general_master database table.
 * 
 */
@Entity
@Table(name="t_m_general_master")
@NamedQuery(name="GeneralMaster.findAll", query="SELECT g FROM GeneralMaster g")
public class GeneralMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pk_gm_id")
	private Long generalMasterId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt")
	private Date createdDt;

	@Column(name="created_user")
	private Long createdUser;

	@Column(name="gm_code")
	private String generalMasterCode;

	@Column(name="gm_description")
	private String generalMasterDescription;

	@Column(name="gm_display_name")
	private String generalMasterDisplayName;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_editable")
	private Boolean isEditable;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_dt")
	private Date updatedDt;

	@Column(name="updated_user")
	private Long updatedUser;

	//bi-directional many-to-one association to GeneralDetail
	@OneToMany(mappedBy="generalMaster",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	private List<GeneralDetail> generalDetails;

	public GeneralMaster() {
	}

	public Long getGeneralMasterId() {
		return this.generalMasterId;
	}

	public void setGeneralMasterId(Long generalMasterId) {
		this.generalMasterId = generalMasterId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getGeneralMasterCode() {
		return this.generalMasterCode;
	}

	public void setGeneralMasterCode(String generalMasterCode) {
		this.generalMasterCode = generalMasterCode;
	}

	public String getGeneralMasterDescription() {
		return this.generalMasterDescription;
	}

	public void setGeneralMasterDescription(String generalMasterDescription) {
		this.generalMasterDescription = generalMasterDescription;
	}

	public String getGeneralMasterDisplayName() {
		return this.generalMasterDisplayName;
	}

	public void setGeneralMasterDisplayName(String generalMasterDisplayName) {
		this.generalMasterDisplayName = generalMasterDisplayName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsEditable() {
		return this.isEditable;
	}

	public void setIsEditable(Boolean isEditable) {
		this.isEditable = isEditable;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public List<GeneralDetail> getGeneralDetails() {
		return this.generalDetails;
	}

	public void setGeneralDetails(List<GeneralDetail> generalDetails) {
		this.generalDetails = generalDetails;
	}

	public GeneralDetail addGeneralDetail(GeneralDetail generalDetail) {
		getGeneralDetails().add(generalDetail);
		generalDetail.setGeneralMaster(this);

		return generalDetail;
	}

	public GeneralDetail removeGeneralDetail(GeneralDetail generalDetail) {
		getGeneralDetails().remove(generalDetail);
		generalDetail.setGeneralMaster(null);

		return generalDetail;
	}

}