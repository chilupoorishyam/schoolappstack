package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_dl_course_lessons_topics_video_url database table.
 */
@Data
@Entity
@Table(name = "t_dl_course_lessons_topics_video_url")
@NamedQuery(name = "CourseLessonsTopicVideo.findAll", query = "SELECT c FROM CourseLessonsTopicVideo c")
public class CourseLessonsTopicVideo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_dl_course_lessons_topics_video_url_id")
    private Long courseLessonTopicVideoId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @ManyToOne
    @JoinColumn(name = "fk_dl_course_lesson_topic_id")
    private CourseLessonsTopic courseLessonsTopic;

    @ManyToOne
    @JoinColumn(name = "fk_emp_id")
    private EmployeeDetail employeeDetail;

    @ManyToOne
    @JoinColumn(name = "fk_subject_id")
    private Subject subject;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

    @Column(name = "video_url")
    private String videoUrl;

}