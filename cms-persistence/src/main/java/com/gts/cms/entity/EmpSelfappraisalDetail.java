package com.gts.cms.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the t_emp_selfappraisal_details database table.
 */
@Data
@Entity
@Table(name = "t_emp_selfappraisal_details")
@NamedQuery(name = "EmpSelfappraisalDetail.findAll", query = "SELECT c FROM EmpSelfappraisalDetail c")
public class EmpSelfappraisalDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_emp_selfappraisal_det_id")
    private Long empSelfappraisalDetId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Column(name = "employee_inputs")
    private String employeeInputs;

    @Column(name = "manager_comments")
    private String managerComments;

    @Column(name = "management_comments")
    private String managementComments;

    @ManyToOne
    @JoinColumn(name = "fk_emp_selfappraisal_id")
    private EmpSelfappraisal empSelfappraisalId;

    @ManyToOne
    @JoinColumn(name = "fk_selfappraisal_form_det_id")
    private EmpSelfappraisalFormDetail selfappraisalFormDetId;

    @Column(name = "is_active")
    private Boolean isActive;

    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;
}