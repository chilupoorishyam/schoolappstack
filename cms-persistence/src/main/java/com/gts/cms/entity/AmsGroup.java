package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_ams_groups database table.
 * 
 */
@Entity
@Table(name = "t_ams_groups")
@NamedQuery(name = "AmsGroup.findAll", query = "SELECT a FROM AmsGroup a")
public class AmsGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_ams_group_id")
	private Long amsGroupId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	// bi-directional many-to-one association to School
	@ManyToOne
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to Organization
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	// bi-directional many-to-one association to EmployeeDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_verifiedby_emp_id")
	private EmployeeDetail verifiedbyEmp;

	@Column(name = "group_code")
	private String groupCode;

	@Column(name = "group_name")
	private String groupName;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_verifiedby_incharge")
	private Boolean isVerifiedbyIncharge;

	private String reason;

	private String skillset;

	private String tags;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	@Column(name = "verification_comments")
	private String verificationComments;
	
	@ManyToOne
	@JoinColumn(name = "fk_group_created_profile_id")
	private AmsProfileDetail profileDetail;
	
	@Column(name ="group_icon")
	private String groupIcon;

	public AmsGroup() {
	}

	public Long getAmsGroupId() {
		return this.amsGroupId;
	}

	public void setAmsGroupId(Long amsGroupId) {
		this.amsGroupId = amsGroupId;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public EmployeeDetail getVerifiedbyEmp() {
		return verifiedbyEmp;
	}

	public void setVerifiedbyEmp(EmployeeDetail verifiedbyEmp) {
		this.verifiedbyEmp = verifiedbyEmp;
	}

	public String getGroupCode() {
		return this.groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getGroupName() {
		return this.groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsVerifiedbyIncharge() {
		return this.isVerifiedbyIncharge;
	}

	public void setIsVerifiedbyIncharge(Boolean isVerifiedbyIncharge) {
		this.isVerifiedbyIncharge = isVerifiedbyIncharge;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSkillset() {
		return this.skillset;
	}

	public void setSkillset(String skillset) {
		this.skillset = skillset;
	}

	public String getTags() {
		return this.tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getVerificationComments() {
		return this.verificationComments;
	}

	public void setVerificationComments(String verificationComments) {
		this.verificationComments = verificationComments;
	}

	public AmsProfileDetail getProfileDetail() {
		return profileDetail;
	}

	public void setProfileDetail(AmsProfileDetail profileDetail) {
		this.profileDetail = profileDetail;
	}

	public String getGroupIcon() {
		return groupIcon;
	}

	public void setGroupIcon(String groupIcon) {
		this.groupIcon = groupIcon;
	}
}