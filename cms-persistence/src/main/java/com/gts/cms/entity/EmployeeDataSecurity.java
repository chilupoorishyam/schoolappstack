package com.gts.cms.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the t_eds_emp_data_security database table.
 */
@Entity
@Setter
@Getter
@Table(name = "t_eds_emp_data_security")
@NamedQuery(name = "EmployeeDataSecurity.findAll", query = "SELECT e FROM EmployeeDataSecurity e")
public class EmployeeDataSecurity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_emp_data_security_id")
    private Long empDataSecurityId;

    //bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_emp_id")
    private EmployeeDetail employeeDetailId;

    //bi-directional many-to-one association to School
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_school_id")
    private School school;

    //bi-directional many-to-one association to Department
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_dept_id")
    private Department employeeDepartment;

    //bi-directional many-to-one association to Designation
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_course_id")
    private Course courseId;



    //bi-directional many-to-one association to District
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_course_year_id")
    private CourseYear courseYearId;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "reason")
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_dt")
    private Date createdDt;

    @Column(name = "created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_dt")
    private Date updatedDt;

    @Column(name = "updated_user")
    private Long updatedUser;

}