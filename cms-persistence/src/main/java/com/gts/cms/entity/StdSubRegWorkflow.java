package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the t_cm_std_sub_reg_workflow database table.
 * 
 */
@Entity
@Table(name = "t_cm_std_sub_reg_workflow")
@NamedQuery(name = "StdSubRegWorkflow.findAll", query = "SELECT s FROM StdSubRegWorkflow s")
public class StdSubRegWorkflow implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_std_sub_reg_workflow_id")
	private Long stdSubRegWorkflowId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;
	
	@ManyToOne
	@JoinColumn(name = "fk_from_std_sub_reg_wf_catdet_id")
	private GeneralDetail fromStdSubRegWfCat;

	@ManyToOne
	@JoinColumn(name = "fk_status_emp_id")
	private EmployeeDetail statusEmp;

	@ManyToOne
	@JoinColumn(name = "fk_to_std_sub_reg_wf_catdet_id")
	private GeneralDetail toStdSubRegWfCat;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Column(name = "status_comments")
	private String statusComments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "status_date")
	private Date statusDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to StdSubRegistration
	@ManyToOne
	@JoinColumn(name = "fk_std_sub_reg_id")
	private StdSubRegistration stdSubRegistration;

	public Long getStdSubRegWorkflowId() {
		return stdSubRegWorkflowId;
	}

	public void setStdSubRegWorkflowId(Long stdSubRegWorkflowId) {
		this.stdSubRegWorkflowId = stdSubRegWorkflowId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public GeneralDetail getFromStdSubRegWfCat() {
		return fromStdSubRegWfCat;
	}

	public void setFromStdSubRegWfCat(GeneralDetail fromStdSubRegWfCat) {
		this.fromStdSubRegWfCat = fromStdSubRegWfCat;
	}

	public EmployeeDetail getStatusEmp() {
		return statusEmp;
	}

	public void setStatusEmp(EmployeeDetail statusEmp) {
		this.statusEmp = statusEmp;
	}

	public GeneralDetail getToStdSubRegWfCat() {
		return toStdSubRegWfCat;
	}

	public void setToStdSubRegWfCat(GeneralDetail toStdSubRegWfCat) {
		this.toStdSubRegWfCat = toStdSubRegWfCat;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStatusComments() {
		return statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public Date getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public StdSubRegistration getStdSubRegistration() {
		return stdSubRegistration;
	}

	public void setStdSubRegistration(StdSubRegistration stdSubRegistration) {
		this.stdSubRegistration = stdSubRegistration;
	}

	
}