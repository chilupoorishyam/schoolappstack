package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_ams_friendslist database table.
 * 
 */
@Entity
@Table(name = "t_ams_friendslist")
@NamedQuery(name = "FriendsList.findAll", query = "SELECT f FROM FriendsList f")
public class FriendsList implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_ams_friendlist_id")
	private Long friendListId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "accepted_date")
	private Date acceptedDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@ManyToOne
	@JoinColumn(name = "fk_ams_friendprofile_id")
	private AmsProfileDetail friendProfile;

	@ManyToOne
	@JoinColumn(name = "fk_ams_profile_id")
	private AmsProfileDetail profile;

	@Column(name = "is_accepted")
	private Boolean isAccepted;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_following")
	private Boolean isFollowing;

	@Column(name = "is_friend")
	private Boolean isFriend;

	@Column(name = "is_requested")
	private Boolean isRequested;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "request_date")
	private Date requestDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	public FriendsList() {
	}

	public Date getAcceptedDate() {
		return this.acceptedDate;
	}

	public void setAcceptedDate(Date acceptedDate) {
		this.acceptedDate = acceptedDate;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsAccepted() {
		return this.isAccepted;
	}

	public void setIsAccepted(Boolean isAccepted) {
		this.isAccepted = isAccepted;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsFollowing() {
		return this.isFollowing;
	}

	public void setIsFollowing(Boolean isFollowing) {
		this.isFollowing = isFollowing;
	}

	public Boolean getIsFriend() {
		return this.isFriend;
	}

	public void setIsFriend(Boolean isFriend) {
		this.isFriend = isFriend;
	}

	public Boolean getIsRequested() {
		return this.isRequested;
	}

	public void setIsRequested(Boolean isRequested) {
		this.isRequested = isRequested;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getRequestDate() {
		return this.requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Long getFriendListId() {
		return friendListId;
	}

	public void setFriendListId(Long friendListId) {
		this.friendListId = friendListId;
	}

	public AmsProfileDetail getFriendProfile() {
		return friendProfile;
	}

	public void setFriendProfile(AmsProfileDetail friendProfile) {
		this.friendProfile = friendProfile;
	}

	public AmsProfileDetail getProfile() {
		return profile;
	}

	public void setProfile(AmsProfileDetail profile) {
		this.profile = profile;
	}
}