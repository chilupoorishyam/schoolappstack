package com.gts.cms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the t_pa_placements database table.
 * 
 */
@Entity
@Table(name = "t_pa_placements")
@NamedQuery(name = "Placement.findAll", query = "SELECT p FROM Placement p")
public class Placement implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_pa_placement_id")
	private Long placementId;

	// bi-directional many-to-one association to Campus
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_org_id")
	private Organization organization;

	// bi-directional many-to-one association to Campus
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_campus_id")
	private Campus campus;

	@Column(name = "is_offcampus")
	private Boolean isOffcampus;

	@Column(name = "offcampus_location")
	private String offcampusLocation;

	@Column(name = "plaecment_title")
	private String plaecmentTitle;

	private String description;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "placement_start_date")
	private Date placementStartDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "placement_end_date")
	private Date placementEndDate;

	@Column(name = "contact_person")
	private String contactPerson;

	@Column(name = "contact_details")
	private String contactDetails;

	private String address;

	private String city;

	// bi-directional many-to-one association to Campus
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_placementstatus_catdet_id")
	private GeneralDetail placementCat;

	@Column(name = "placement_status_comments")
	private String placementStatusComments;

	@Column(name = "is_active")
	private Boolean isActive;

	private String reason;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to PlacementCompany
	@OneToMany(mappedBy = "placement", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<PlacementCompany> placementCompanies;

	public Placement() {
	}

	public Long getPlacementId() {
		return this.placementId;
	}

	public void setPlacementId(Long placementId) {
		this.placementId = placementId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getContactDetails() {
		return this.contactDetails;
	}

	public void setContactDetails(String contactDetails) {
		this.contactDetails = contactDetails;
	}

	public String getContactPerson() {
		return this.contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Campus getCampus() {
		return campus;
	}

	public void setCampus(Campus campus) {
		this.campus = campus;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsOffcampus() {
		return this.isOffcampus;
	}

	public void setIsOffcampus(Boolean isOffcampus) {
		this.isOffcampus = isOffcampus;
	}

	public String getOffcampusLocation() {
		return this.offcampusLocation;
	}

	public void setOffcampusLocation(String offcampusLocation) {
		this.offcampusLocation = offcampusLocation;
	}

	public String getPlacementStatusComments() {
		return this.placementStatusComments;
	}

	public void setPlacementStatusComments(String placementStatusComments) {
		this.placementStatusComments = placementStatusComments;
	}

	public String getPlaecmentTitle() {
		return this.plaecmentTitle;
	}

	public void setPlaecmentTitle(String plaecmentTitle) {
		this.plaecmentTitle = plaecmentTitle;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public GeneralDetail getPlacementCat() {
		return placementCat;
	}

	public void setPlacementCat(GeneralDetail placementCat) {
		this.placementCat = placementCat;
	}

	public List<PlacementCompany> getPlacementCompanies() {
		return placementCompanies;
	}

	public void setPlacementCompanies(List<PlacementCompany> placementCompanies) {
		this.placementCompanies = placementCompanies;
	}

	public Date getPlacementStartDate() {
		return placementStartDate;
	}

	public void setPlacementStartDate(Date placementStartDate) {
		this.placementStartDate = placementStartDate;
	}

	public Date getPlacementEndDate() {
		return placementEndDate;
	}

	public void setPlacementEndDate(Date placementEndDate) {
		this.placementEndDate = placementEndDate;
	}
}