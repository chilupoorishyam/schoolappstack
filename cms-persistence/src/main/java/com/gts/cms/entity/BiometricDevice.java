package com.gts.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the t_m_biometric_devices database table.
 * 
 */
@Entity
@Table(name = "t_m_biometric_devices")
@NamedQuery(name = "BiometricDevice.findAll", query = "SELECT b FROM BiometricDevice b")
public class BiometricDevice implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pk_biometric_id")
	private Long biometricId;

	private String baudrate;

	private String commkey;

	private String comport;

	private String connectiontype;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_dt")
	private Date createdDt;

	@Column(name = "created_user")
	private Long createdUser;

	@Column(name = "device_code")
	private String deviceCode;

	private String devicedirection;
	
	@Column(name = "deviceid")
	private Long deviceId;

	private String devicelocation;
	
	@Column(name = "devicename")
	private String deviceName;

	@Column(name = "downloadtype")
	private Integer downloadType;

	private String ipaddress;

	@Column(name = "is_active")
	private Boolean isActive;

	@Temporal(TemporalType.TIMESTAMP)
	private Date lastlogdownloaddate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date lastping;

	@Column(name = "opstamp")
	private Integer opStamp;

	private String reason;

	@Column(name = "serialnumber")
	private String serialNumber;

	private Integer timeout;

	private Integer timezone;

	@Temporal(TemporalType.TIMESTAMP)
	private Date transactionstamp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_dt")
	private Date updatedDt;

	@Column(name = "updated_user")
	private Long updatedUser;

	// bi-directional many-to-one association to School
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_school_id")
	private School school;

	// bi-directional many-to-one association to GeneralDetail
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_devicetype_catdet_id")
	private GeneralDetail devicetypeCat;

	public BiometricDevice() {
	}

	public Long getBiometricId() {
		return this.biometricId;
	}

	public void setBiometricId(Long biometricId) {
		this.biometricId = biometricId;
	}

	public String getBaudrate() {
		return this.baudrate;
	}

	public void setBaudrate(String baudrate) {
		this.baudrate = baudrate;
	}

	public String getCommkey() {
		return this.commkey;
	}

	public void setCommkey(String commkey) {
		this.commkey = commkey;
	}

	public String getComport() {
		return this.comport;
	}

	public void setComport(String comport) {
		this.comport = comport;
	}

	public String getConnectiontype() {
		return this.connectiontype;
	}

	public void setConnectiontype(String connectiontype) {
		this.connectiontype = connectiontype;
	}

	public Date getCreatedDt() {
		return this.createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return this.createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDeviceCode() {
		return this.deviceCode;
	}

	public void setDeviceCode(String deviceCode) {
		this.deviceCode = deviceCode;
	}

	public String getDevicedirection() {
		return this.devicedirection;
	}

	public void setDevicedirection(String devicedirection) {
		this.devicedirection = devicedirection;
	}

	public String getDevicelocation() {
		return this.devicelocation;
	}

	public void setDevicelocation(String devicelocation) {
		this.devicelocation = devicelocation;
	}

	public Integer getDownloadType() {
		return this.downloadType;
	}

	public void setDownloadType(Integer downloadType) {
		this.downloadType = downloadType;
	}

	public String getIpaddress() {
		return this.ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Date getLastlogdownloaddate() {
		return this.lastlogdownloaddate;
	}

	public void setLastlogdownloaddate(Date lastlogdownloaddate) {
		this.lastlogdownloaddate = lastlogdownloaddate;
	}

	public Date getLastping() {
		return this.lastping;
	}

	public void setLastping(Date lastping) {
		this.lastping = lastping;
	}

	public Integer getOpStamp() {
		return this.opStamp;
	}

	public void setOpStamp(Integer opStamp) {
		this.opStamp = opStamp;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSerialNumber() {
		return this.serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Integer getTimeout() {
		return this.timeout;
	}

	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}

	public Integer getTimezone() {
		return this.timezone;
	}

	public void setTimezone(Integer timezone) {
		this.timezone = timezone;
	}

	public Date getTransactionstamp() {
		return this.transactionstamp;
	}

	public void setTransactionstamp(Date transactionstamp) {
		this.transactionstamp = transactionstamp;
	}

	public Date getUpdatedDt() {
		return this.updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return this.updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public GeneralDetail getDevicetypeCat() {
		return devicetypeCat;
	}

	public void setDevicetypeCat(GeneralDetail devicetypeCat) {
		this.devicetypeCat = devicetypeCat;
	}
}