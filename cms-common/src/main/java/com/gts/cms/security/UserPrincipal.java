package com.gts.cms.security;

import java.util.Collection;
import java.util.Objects;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class UserPrincipal implements UserDetails {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private String name;

	private String username;

	@JsonIgnore
	private String email;

	@JsonIgnore
	private String password;

	private Collection<? extends GrantedAuthority> authorities;

	private Long organizationId;
	private String orgCode;
	private String organizationName;
	private Long schoolId;
	private String schoolCode;
	private String schoolName;
	private String mobileNumber;
	private Long academicYearId;
	private String academicYear;

	private Long userRoleId;

	private String userRoleName;
	
	public UserPrincipal(Long id, String name, String username, String email, String password,
			Collection<? extends GrantedAuthority> authorities, Long organizationId, String orgCode,String organizationName, Long schoolId,
			String schoolCode,String schoolName, String mobileNumber, Long userRoleId,String userRoleName,
			Long academicYearId,String academicYear) {
		this.id = id;
		this.name = name;
		this.username = username;
		this.email = email;
		this.password = password;
		this.authorities = authorities;
		this.organizationId = organizationId;
		this.organizationName=organizationName;
		this.orgCode = orgCode;
		this.schoolId = schoolId;
		this.schoolCode = schoolCode;
		this.schoolName=schoolName;
		this.mobileNumber = mobileNumber;
		this.userRoleId = userRoleId;
		this.userRoleName=userRoleName;
		this.academicYearId = academicYearId;
		this.academicYear=academicYear;
	}

	
	public String getUserRoleName() {
		return userRoleName;
	}


	public void setUserRoleName(String userRoleName) {
		this.userRoleName = userRoleName;
	}


	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public Long getUserRoleId() {
		return userRoleId;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		UserPrincipal that = (UserPrincipal) o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode() {

		return Objects.hash(id);
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public void setUserRoleId(Long userRoleId) {
		this.userRoleId = userRoleId;
	}


	public Long getAcademicYearId() {
		return academicYearId;
	}


	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}


	public String getAcademicYear() {
		return academicYear;
	}


	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}

}
