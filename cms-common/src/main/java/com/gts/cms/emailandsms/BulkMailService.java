package com.gts.cms.emailandsms;

import com.amazonaws.services.simpleemail.model.AmazonSimpleEmailServiceException;
import com.amazonaws.services.simpleemail.model.MessageRejectedException;
import com.amazonaws.services.simpleemail.model.SendEmailResult;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BulkMailService {
    private static final Logger LOGGER = Logger.getLogger(BulkMailService.class);

    @Autowired
    private SMTPBulkMail smtpBulkMail;

    @Autowired
    private SendGmailSMTPMail sendGmailSMTPMail;

    /*@Autowired
    private SendGridSMTPBulkMail sendGridSMTPBulkMail;*/

    public void sendEmail(String from, String to, String subject, String content, String htmlContent) {
        AwsBulkMail awsBulkMail = new AwsBulkMail(MailUtil.getAmazonSES());
        awsBulkMail.withFrom(from).withTo(to).withSubject(subject).withBody(content).withHtmlBody(htmlContent).send();

    }

    public SendEmailResult sendEmail(String from, List<String> to, String subject, String content, String htmlContent) {
    	LOGGER.info("BulkMailService.sendEmail()");
        SendEmailResult sendEmailResult = null;
        try {
            //AwsBulkMail awsBulkMail = new AwsBulkMail(MailUtil.getAmazonSES());
            /*sendEmailResult = sendGmailSMTPMail.withFrom(from).withToList(to)
                    .withSubject(subject).withBody(content)
                    .withHtmlBody(htmlContent).sendMail();*/
        } catch (MessageRejectedException mre) {
            LOGGER.error(mre.getMessage());
        } catch (AmazonSimpleEmailServiceException ase) {
            LOGGER.error(ase.getMessage());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return sendEmailResult;
    }

    public void sendEmailWithSmtp(String from, List<String> to, String subject, String content, String htmlContent, String file) {
    	LOGGER.info("BulkMailService.sendEmailWithSmtp()");
        try {
            //SMTPBulkMail smtpBulkMail = new SMTPBulkMail();
            /*sendGmailSMTPMail.withFrom(from).withToList(to)
                    .withSubject(subject).withBody(content)
                    .withHtmlBody(htmlContent).withAttachment(file).sendAttachment();*/
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }


    public void sendEmailAttachment(String from, List<String> to, String subject, String content, String htmlContent,
                                    String filePath) {
        AwsBulkMailAttachment awsBulkMail = new AwsBulkMailAttachment(MailUtil.getAmazonSES());
        awsBulkMail.withFrom(from).withToList(to).withSubject(subject).withBody(content).withHtmlBody(htmlContent)
                .withAttachment(filePath).send();
    }
}
