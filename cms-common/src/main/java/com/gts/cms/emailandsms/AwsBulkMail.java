package com.gts.cms.emailandsms;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.*;
import com.gts.cms.emailandsms.service.impl.EmailServiceImpl;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AwsBulkMail implements BulkMail{

	private static final Logger LOGGER = Logger.getLogger(EmailServiceImpl.class);
	
	private String to,from,subject,body,htmlBody;
	private List<String> toList;
	private AmazonSimpleEmailService amazonSimpleEmailService;
	
	public AwsBulkMail(AmazonSimpleEmailService amazonSimpleEmailService) {
		this.amazonSimpleEmailService = amazonSimpleEmailService;
	}
	
	public void setFrom(String from) {
		this.from = from;
	}

	public void setTo(String to) {
		this.to = to;
	}
	
	public void setToList(List<String> toList) {
		this.toList = toList;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public void setHtmlBody(String htmlBody) {
		this.htmlBody = htmlBody;
	}
	
	private List<String> getToAsList(){
		return Arrays.asList(to.split(","));
	}

	public SendEmailResult sendMail() {
		SendEmailResult sendEmailResult=null;
		try {
			List<List<String>> shortList;
			if(toList != null) {
				shortList = chunkList(toList,45);
			}else {
				shortList = chunkList(getToAsList(),45);
			}
			
			for(List<String> list:shortList) {
					Destination destination = new Destination().withBccAddresses(list);
					Content awsSubject = new Content(subject);
					Body awsBody = new Body(new Content(body));
					awsBody.setHtml(new Content(htmlBody));
					Message message = new Message(awsSubject,awsBody);
					SendEmailRequest request = new SendEmailRequest(from, destination, message).withReturnPath(from);
					sendEmailResult= amazonSimpleEmailService.sendEmail(request);
			}
		}catch (MessageRejectedException mre) {
			LOGGER.error(mre.getMessage());
		}catch (AmazonSimpleEmailServiceException ase) {
			LOGGER.error(ase.getMessage());
		}catch(Exception e) {
			LOGGER.error("Exception occured while sending email: "+e.getMessage());
			e.printStackTrace();
		}
		return sendEmailResult;
	}

	public BulkMail withFrom(String from) {
		this.from = from;
		return this;
	}

	public BulkMail withTo(String to) {
		this.to= to;
		return this;
	}
	
	public BulkMail withBody(String body) {
		this.body = body;
		return this;
	}

	public BulkMail withHtmlBody(String htmlBody) {
		this.htmlBody = htmlBody;
		return this;
	}

	public BulkMail withSubject(String subject) {
		this.subject = subject;
		return this;
	}
	
	public static  <T> List<List<T>> chunkList(List<T> list, int chunkSize) {
	    if (chunkSize <= 0) {
	        throw new IllegalArgumentException("Invalid chunk size: " + chunkSize);
	    }
	    List<List<T>> chunkList = new ArrayList<>(list.size() / chunkSize);
	    for (int i = 0; i < list.size(); i += chunkSize) {
	        chunkList.add(list.subList(i, i + chunkSize >= list.size() ? list.size() : i + chunkSize));
	    }
	    return chunkList;
	}

	@Override
	public BulkMail withToList(List<String> toList) {
		this.toList= toList;
		return this;
	}

	@Override
	public BulkMail withAttachment(String attachment) {
		// TODO Auto-generated method stub
		return null;
	}

	/*@Override
	public SendRawEmailResult sendAttachment() {
		// TODO Auto-generated method stub
		return null;
	}
*/
	@Override
	public String send() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
