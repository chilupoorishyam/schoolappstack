package com.gts.cms.emailandsms.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SmsApiResponseDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String status;
	private Boolean test_mode;
	private Integer num_messages;
	private Integer cost;
	private Integer batch_id;
	private Integer balance;
	private ArrayList<SmsApiResponseErrorDTO> errors;
	private SmsApiResponseMessageDTO message;
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getTest_mode() {
		return test_mode;
	}
	public void setTest_mode(Boolean test_mode) {
		this.test_mode = test_mode;
	}
	public Integer getNum_messages() {
		return num_messages;
	}
	public void setNum_messages(Integer num_messages) {
		this.num_messages = num_messages;
	}
	public Integer getCost() {
		return cost;
	}
	public void setCost(Integer cost) {
		this.cost = cost;
	}
	public Integer getBatch_id() {
		return batch_id;
	}
	public void setBatch_id(Integer batch_id) {
		this.batch_id = batch_id;
	}
	public Integer getBalance() {
		return balance;
	}
	public void setBalance(Integer balance) {
		this.balance = balance;
	}
	public SmsApiResponseMessageDTO getMessage() {
		return message;
	}
	public void setMessage(SmsApiResponseMessageDTO message) {
		this.message = message;
	}
	public ArrayList<SmsApiResponseErrorDTO> getErrors() {
		return errors;
	}
	public void setErrors(ArrayList<SmsApiResponseErrorDTO> errors) {
		this.errors = errors;
	}
	
	
	
	

}
