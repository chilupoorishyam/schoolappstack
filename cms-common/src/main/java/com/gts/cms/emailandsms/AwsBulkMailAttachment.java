package com.gts.cms.emailandsms;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.RawMessage;
import com.amazonaws.services.simpleemail.model.SendEmailResult;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;
import com.amazonaws.services.simpleemail.model.SendRawEmailResult;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class AwsBulkMailAttachment implements BulkMail{
	
	public AwsBulkMailAttachment(AmazonSimpleEmailService amazonSimpleEmailService) {
		this.amazonSimpleEmailService = amazonSimpleEmailService;
	}
	
	private String to,from,subject,body,htmlBody,attachment;
	private List<String> toList;
	private AmazonSimpleEmailService amazonSimpleEmailService;
	
	
	public void setFrom(String from) {
		this.from = from;
	}

	public void setTo(String to) {
		this.to = to;
	}
	
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	
	public void setToList(List<String> toList) {
		this.toList = toList;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public void setHtmlBody(String htmlBody) {
		this.htmlBody = htmlBody;
	}
	
	private List<String> getToAsList(){
		return Arrays.asList(to.split(","));
	}

	public SendRawEmailResult sendAttachment() {
		SendRawEmailResult sendRawEmailResult=null;
		try {
			List<List<String>> shortList;
			if(toList != null) {
				shortList = chunkList(toList,45);
			}else {
				shortList = chunkList(getToAsList(),45);
			}
			
			for(List<String> list:shortList) {
				for(String emailId:list) {
					Session session = Session.getDefaultInstance(new Properties());
					MimeMessage message = new MimeMessage(session);
					message.setSubject(subject, "UTF-8");
					message.setFrom(new InternetAddress(from));
					 message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailId));
					 MimeMultipart msg_body = new MimeMultipart("alternative");
					 MimeBodyPart wrap = new MimeBodyPart();
					 MimeBodyPart textPart = new MimeBodyPart();
					 textPart.setContent(body, "text/plain; charset=UTF-8");
					 
					 MimeBodyPart htmlPart = new MimeBodyPart();
					 htmlPart.setContent(htmlBody,"text/html; charset=UTF-8");
					 msg_body.addBodyPart(textPart);
					 msg_body.addBodyPart(htmlPart);
					 wrap.setContent(msg_body);
					 MimeMultipart msg = new MimeMultipart("mixed");
					 message.setContent(msg);
					 msg.addBodyPart(wrap);
					 
					 MimeBodyPart att = new MimeBodyPart();
					 DataSource fds = new FileDataSource(attachment);
					 att.setDataHandler(new DataHandler(fds));
					 att.setFileName(fds.getName());
					 msg.addBodyPart(att);
					
		             //PrintStream out = System.out;
		             //message.writeTo(out);
		             ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		             message.writeTo(outputStream);
		             RawMessage rawMessage = new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));
		             SendRawEmailRequest rawEmailRequest = new SendRawEmailRequest(rawMessage);
		             sendRawEmailResult=amazonSimpleEmailService.sendRawEmail(rawEmailRequest);
		             System.out.println("Email sent!");					
				}
			}
		}catch(Exception e) {
			System.out.println("Exception occured while sending email");
			e.printStackTrace();
		}
		return sendRawEmailResult;
	}

	public BulkMail withFrom(String from) {
		this.from = from;
		return this;
	}

	public BulkMail withTo(String to) {
		this.to= to;
		return this;
	}
	
	public BulkMail withAttachment(String attachment) {
		this.attachment= attachment;
		return this;
	}
	
	public BulkMail withBody(String body) {
		this.body = body;
		return this;
	}

	public BulkMail withHtmlBody(String htmlBody) {
		this.htmlBody = htmlBody;
		return this;
	}

	public BulkMail withSubject(String subject) {
		this.subject = subject;
		return this;
	}
	
	public static  <T> List<List<T>> chunkList(List<T> list, int chunkSize) {
	    if (chunkSize <= 0) {
	        throw new IllegalArgumentException("Invalid chunk size: " + chunkSize);
	    }
	    List<List<T>> chunkList = new ArrayList<>(list.size() / chunkSize);
	    for (int i = 0; i < list.size(); i += chunkSize) {
	        chunkList.add(list.subList(i, i + chunkSize >= list.size() ? list.size() : i + chunkSize));
	    }
	    return chunkList;
	}

	@Override
	public BulkMail withToList(List<String> toList) {
		this.toList= toList;
		return this;
	}

	/*@Override
	public SendEmailResult sendMail() {
		// TODO Auto-generated method stub
		return null;
	}*/

	@Override
	public String send() {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
