package com.gts.cms.emailandsms.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SmsApiResponseMessageDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String num_parts;
	private String sender;
	private String content;
	
	public String getNum_parts() {
		return num_parts;
	}
	public void setNum_parts(String num_parts) {
		this.num_parts = num_parts;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	
	
	

}
