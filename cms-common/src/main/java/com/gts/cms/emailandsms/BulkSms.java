package com.gts.cms.emailandsms;

import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.json.util.JsonUtil;
import com.gts.cms.emailandsms.dto.SmsApiBatchStatusResponseDTO;
import com.gts.cms.emailandsms.dto.SmsApiResponseDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.AsyncRestTemplate;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

@Service
public class BulkSms {
	private static final Logger LOG = Logger.getLogger(BulkSms.class);

	@Value("${sms.api-key}")
	private String apiKey;

	@Value("${sms.sender-id}")
	private String senderId;

	@Value("${sms.sms-test}")
	private Boolean isTest;

	@Autowired
	private AsyncRestTemplate asyncRestTemplate;

	public String sendSingleMessage(String number, String message, String sender) {
		LOG.info("Entered into sendSingleMessage()");
		String smsSenderId = "";
		if (sender != null) {
			smsSenderId = sender;
		} else {
			smsSenderId = senderId;
		}
		try {
			message = URLEncoder.encode(message, "UTF-8");
			//$url = "http://pointsms.in/API/sms.php?username=[xxxxxx]&password=[xxxxxx]&from=[xxxxxxxx]&to=[xxxxxxxxxx]&msg=[xxxx]&type=1&dnd_check=0";
			HttpURLConnection conn = (HttpURLConnection) new URL("https://api.textlocal.in/send/?").openConnection();
			String data = "apikey=" + apiKey + "&numbers=" + number + "&message=" + message + "&sender=" + smsSenderId
					+ "&test=" + isTest;
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
			conn.getOutputStream().write(data.getBytes("UTF-8"));
			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			final StringBuffer stringBuffer = new StringBuffer();
			String line;
			while ((line = rd.readLine()) != null) {
				stringBuffer.append(line);
			}
			rd.close();
			// LOG.info("SMS "+stringBuffer.toString());
			//SmsApiResponseDTO apiResponse = JsonUtil.jsonToJava(stringBuffer.toString(), SmsApiResponseDTO.class);
			//return JsonUtil.javaToJson(apiResponse);
			return stringBuffer.toString();
		} catch (Exception e) {
			LOG.error("Error SMS" + e);
			return "Error " + e;
		}
	}

	public SmsApiResponseDTO sendGroupMessage(List<String> numbersList, String message, String sender) {
		LOG.info("Entered into sendGroupMessage()");
		String smsSenderId = "";
		String numbers = "&numbers=";

		if (sender != null) {
			smsSenderId = sender;
		} else {
			smsSenderId = senderId;
		}
		for (int i = 0; i < numbersList.size(); i++) {
			if (i != numbersList.size() - 1) {
				numbers = numbers + numbersList.get(i) + ",";
			} else {
				numbers = numbers + numbersList.get(i);
			}
		}
		try {
			HttpURLConnection conn = (HttpURLConnection) new URL("https://api.textlocal.in/send/?").openConnection();
			String data = "apikey=" + apiKey + numbers + "&message=" + message + "&sender=" + smsSenderId + "&test="
					+ isTest;
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
			conn.getOutputStream().write(data.getBytes("UTF-8"));
			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			final StringBuilder stringBuilder = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null) {
				stringBuilder.append(line);
			}
			rd.close();
			// LOG.info("SMS "+stringBuilder.toString());
			SmsApiResponseDTO apiResponse = JsonUtil.jsonToJava(stringBuilder.toString(), SmsApiResponseDTO.class);
			return apiResponse;
		} catch (Exception e) {
			throw new  ApiException(e);
		}
	}
	
	public SmsApiBatchStatusResponseDTO getBatchStatus(String batchId) {
		LOG.info("Entered into getBatchStatus()");
		try {
			HttpURLConnection conn = (HttpURLConnection) new URL(ApiEndpoints.API_BATCH_STATUS_URL+"?").openConnection();
			String data = "apikey=" + apiKey + "&batch_id=" + batchId ;
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
			conn.getOutputStream().write(data.getBytes("UTF-8"));
			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			final StringBuilder stringBuilder = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null) {
				stringBuilder.append(line);
			}
			rd.close();
			SmsApiBatchStatusResponseDTO apiResponse = JsonUtil.jsonToJava(stringBuilder.toString(), SmsApiBatchStatusResponseDTO.class);
			return apiResponse;
		} catch (Exception e) {
			throw new  ApiException(e);
		}
	}
}
