package com.gts.cms.emailandsms;

import com.amazonaws.services.simpleemail.model.AmazonSimpleEmailServiceException;
import com.amazonaws.services.simpleemail.model.MessageRejectedException;
import com.sendgrid.*;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class TestMail {

    public static void main(String[] args) {
        System.out.println("Email sending");
        List<String> s = new ArrayList<>();
        s.add("chilupoorishyam@gentechsyspro.com");
        s.add("chilupoorishyam@gmail.com");
        String mailResponse = sendEmail("shyamchilupoori@gmail.com", s, "Sub: Bulk mail testing", "Body: sending mail through GMAIL", "html: sending mail through send grid");
        // senWebMail();
        //sendGmailEmail();
        System.out.println("Email sent: " + mailResponse);

    }

    private static void sendWebMail() {
        Email from = new Email("shyamchilupoori@example.com");
        String subject = "Sending with SendGrid is Fun";
        Email to = new Email("chilupoorishyam@example.com");
        Content content = new Content("text/plain", "and easy to do anywhere, even with Java");
        Mail mail = new Mail(from, subject, to, content);

        SendGrid sg = new SendGrid(System.getenv("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"));
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody());
            System.out.println(response.getHeaders());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            System.out.println(ex.getCause());
        }
    }

    public static String sendGEmail(String from, List<String> to, String subject, String content, String htmlContent) {
        String response = null;
        String host="smtp.gmail.com";
        String username="dev@gentechsys.com";
        String password="Genadmin123";
        String port="587";
        Transport transport = null;
        try {
            Properties props = new Properties();
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.ssl.trust", host);
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", port);
            //props.put("mail.smtp.connectiontimeout", "3600");
            // props.put("mail.smtp.timeout", "3600");
            // Get the Session object
            Session session = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password);
                        }
                    });

            // Create a default MimeMessage object
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("chilupoorishyam@gentechsyspro.com,chilupoorishyam@gmail.com"));

            // Set Subject
            message.setSubject("Subject of the Gemail");
            // Put the content of your message
            message.setText("Body of the email");
            message.setContent("htmlBody of the gmail", "text/html");
            // Send message
            //Transport.send(message);
            transport = session.getTransport();
            transport.connect(host, username, password);
            transport.sendMessage(message, message.getAllRecipients());
            System.out.println("Sent message successfully....");
        } catch (MessageRejectedException mre) {
            System.out.println(mre.getMessage());
        } catch (AmazonSimpleEmailServiceException ase) {
            System.out.println(ase.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return response;
    }

    public static String sendEmail(String from, List<String> to, String subject, String content, String htmlContent) {
        String response = null;
        try {
            SendGridSMTPBulkMail awsBulkMail = new SendGridSMTPBulkMail();
            response = awsBulkMail.withFrom(from).withToList(to)
                    .withSubject(subject).withBody(content)
                    .withHtmlBody(htmlContent).send();
        } catch (MessageRejectedException mre) {
            System.out.println(mre.getMessage());
        } catch (AmazonSimpleEmailServiceException ase) {
            System.out.println(ase.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return response;
    }

    public static void sendGmailEmail() {

        // Add recipient
        String to = "chilupoorishyam@gmail.com";

// Add sender
        String from = "shyamchilupoori@gmail.com";
        final String username = "";//your Gmail username
        final String password = "";//your Gmail password

        String host = "smtp.gmail.com";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", "587");

// Get the Session object
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            // Create a default MimeMessage object
            Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress(from));

            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));

            // Set Subject
            message.setSubject("Hi JAXenter");

            // Put the content of your message
            message.setText("Hi there,we are just experimenting with JavaMail here");

// Send message
            Transport.send(message);

            System.out.println("Sent message successfully....");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }

    }

}

