package com.gts.cms.emailandsms;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

@Service
public class SendGmailSMTPMail implements BulkMail {

    private static final Logger LOGGER = Logger.getLogger(SendGmailSMTPMail.class);

    @Value("${spring.mail.host}")
    private String host;

    @Value("${spring.mail.username}")
    private String username;

    @Value("${spring.mail.password}")
    private String password;

    @Value("${spring.mail.port}")
    private String port;

    private String to;
    String from;
    String subject;
    String body;
    String htmlBody;
    String attachment;

    public void setFrom(String from) {
        this.from = from;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setHtmlBody(String htmlBody) {
        this.htmlBody = htmlBody;
    }

    public String send() {
        Transport transport = null;
        String result = null;
        try {
            Properties props = new Properties();
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.ssl.trust", host);
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", port);
            //props.put("mail.smtp.connectiontimeout", "3600");
            // props.put("mail.smtp.timeout", "3600");
            // Get the Session object
            Session session = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password);
                        }
                    });

            // Create a default MimeMessage object
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));

            // Set Subject
            message.setSubject(subject);
            // Put the content of your message
            message.setText(body);
            message.setContent(htmlBody, "text/html");
            // Send message
            //Transport.send(message);
            transport = session.getTransport();
            transport.connect(host, username, password);
            transport.sendMessage(message, message.getAllRecipients());
            LOGGER.info("Email sent!");
            result = MailParameter.MAIL_SUCCESS.getParamName();

        } catch (Exception ex) {
            LOGGER.info("The email was not sent.");
            LOGGER.error("Error message: " + ex.getMessage());
            result = MailParameter.MAIL_FAILURE.getParamName();
        } finally {
            try {
                if (transport != null) {
                    transport.close();
                }
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    /*@Override
    public SendEmailResult sendMail() {
    	LOGGER.info("SendGmailSMTPMail.sendMail()");
        Transport transport = null;
        SendEmailResult sendEmailResult = new SendEmailResult();
        try {
            Properties props = new Properties();
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.ssl.trust", host);
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", port);
            //props.put("mail.smtp.connectiontimeout", "3600");
            // props.put("mail.smtp.timeout", "3600");
            // Get the Session object
            Session session = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password);
                        }
                    });

            // Create a default MimeMessage object
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));

            // Set Subject
            message.setSubject(subject);
            // Put the content of your message
            message.setText(body);
            message.setContent(htmlBody, "text/html");
            // Send message
            //Transport.send(message);
            transport = session.getTransport();
            transport.connect(host, username, password);
            transport.sendMessage(message, message.getAllRecipients());
            LOGGER.info("Email sent!");
            sendEmailResult.setMessageId(MailParameter.MAIL_SUCCESS.getParamName());
        } catch (Exception ex) {
            LOGGER.info("The email was not sent.");
            LOGGER.error("Error message: " + ex.getMessage());
            sendEmailResult.setMessageId(MailParameter.MAIL_FAILURE.getParamName());
        } finally {
            try {
                if (transport != null) {
                    transport.close();
                }
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
        return sendEmailResult;
    }*/

    public BulkMail withFrom(String from) {
        this.from = from;
        return this;
    }

    public BulkMail withTo(String to) {
        this.to = to;
        return this;
    }

    public BulkMail withBody(String body) {
        this.body = body;
        return this;
    }

    public BulkMail withHtmlBody(String htmlBody) {
        this.htmlBody = htmlBody;
        return this;
    }

    public BulkMail withSubject(String subject) {
        this.subject = subject;
        return this;
    }

    @Override
    public BulkMail withToList(List<String> toList) {
        String toData = toList.stream().collect(Collectors.joining(","));
        this.to = toData;
        return this;
    }

    @Override
    public BulkMail withAttachment(String attachment) {
        this.attachment = attachment;
        return this;
    }


    /*@Override
    public SendRawEmailResult sendAttachment() {
    	LOGGER.info("SendGmailSMTPMail.sendAttachment()");
        Transport transport = null;
        SendRawEmailResult sendRawEmailResult = new SendRawEmailResult();
        try {
            Properties props = new Properties();
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.ssl.trust", host);
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", port);
            //props.put("mail.smtp.connectiontimeout", "3600");
            // props.put("mail.smtp.timeout", "3600");
            // Get the Session object
            Session session = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password);
                        }
                    });

            // Create a default MimeMessage object
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));

            // Set Subject
            message.setSubject(subject);
            // Put the content of your message
            message.setText(body);
            message.setContent(htmlBody, "text/html");
            Multipart multipart = new MimeMultipart();
            MimeBodyPart attachmentPart = new MimeBodyPart();
            MimeBodyPart textPart = new MimeBodyPart();
           
               // File f = new File(attachment);
               // attachmentPart.attachFile(f);
            File f =null;
            if(attachment!=null && attachment !="") {
            	FileSystemResource fsr = new FileSystemResource(attachment);
            	f = new File(fsr.getFilename());	
            	downloadUsingStream(attachment, f); 
            	
            	attachmentPart.attachFile(f);
            	multipart.addBodyPart(attachmentPart);
            }
                textPart.setText(body);
                textPart.setContent(htmlBody, "text/html");
                multipart.addBodyPart(textPart);
                         
            message.setContent(multipart);

            // Send message
            //Transport.send(message);
            transport = session.getTransport();
            transport.connect(host, username, password);
            transport.sendMessage(message, message.getAllRecipients());
            LOGGER.info("Email sent!");
            sendRawEmailResult.setMessageId(MailParameter.MAIL_SUCCESS.getParamName());
        } catch (Exception ex) {
            LOGGER.info("The email was not sent.");
            LOGGER.error("Error message: " + ex.getMessage());
            sendRawEmailResult.setMessageId(MailParameter.MAIL_FAILURE.getParamName());
        } finally {
            try {
                if (transport != null) {
                    transport.close();
                }
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
        return sendRawEmailResult;
    }

    public static <T> List<List<T>> chunkList(List<T> list, int chunkSize) {
        if (chunkSize <= 0) {
            throw new IllegalArgumentException("Invalid chunk size: " + chunkSize);
        }
        List<List<T>> chunkList = new ArrayList<>(list.size() / chunkSize);
        for (int i = 0; i < list.size(); i += chunkSize) {
            chunkList.add(list.subList(i, i + chunkSize >= list.size() ? list.size() : i + chunkSize));
        }
        return chunkList;
    }*/

    private enum MailParameter {

        /**
         * The HTL_CD.
         */
        MAIL_SUCCESS("SUCCESS"),

        /**
         * The RM_CD.
         */
        MAIL_FAILURE("FAILURE");

        /**
         * The param name.
         */
        private final String paramName;

        /**
         * Instantiates a new field parameter.
         *
         * @param paramName the param name
         */
        private MailParameter(String paramName) {
            this.paramName = paramName;
        }

        /**
         * Gets the param name.
         *
         * @return the param name
         */
        public String getParamName() {
            return paramName;
        }
    }

    private static void downloadUsingStream(String urlStr, File file) {
        try {
            URL url = new URL(urlStr);
            BufferedInputStream bis = new BufferedInputStream(url.openStream());
            FileOutputStream fis = new FileOutputStream(file);
            byte[] buffer = new byte[1024];
            int count = 0;
            while ((count = bis.read(buffer, 0, 1024)) != -1) {
                fis.write(buffer, 0, count);
            }
            fis.close();
            bis.close();

        } catch (Exception ex) {

        }
    }

}
