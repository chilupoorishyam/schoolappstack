package com.gts.cms.emailandsms.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PointSmsApiResponseDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@JsonProperty("JobId")
	private String jobId;
	
	@JsonProperty("Report")
	private ArrayList<Object> report;
	
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public ArrayList<Object> getReport() {
		return report;
	}
	public void setReport(ArrayList<Object> report) {
		this.report = report;
	}
	

}
