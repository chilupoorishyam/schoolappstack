package com.gts.cms.emailandsms.repository;

import com.gts.cms.emailandsms.repository.custom.MessagingRepositoryCustom;
import com.gts.cms.entity.Messaging;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface MessagingRepository extends JpaRepository<Messaging, Long>,MessagingRepositoryCustom{
	
	
	@Query("SELECT e FROM Messaging e"
			+ " WHERE e.smsPattern.messagePatternId = :patternId "
			+ " AND e.school.schoolId = :schoolId "
			+ " AND DATE(e.messagingDate) = :date"
			+ " AND e.isActive = true"
			)
	List<Messaging> findMessagingByDate(@Param("schoolId") Long schoolId,
			@Param("patternId") Long patternId,
			@Param("date") Date date);

}
