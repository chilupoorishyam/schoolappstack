package com.gts.cms.emailandsms.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

@Service
public class SmsEventPublisher {
//private static Logger logger=Logger.getLogger(SmsEventPublisher.class);
@Autowired
private ApplicationEventPublisher applicationEventPublisher;
public void sendSmsPublisher(SmsEvent smsEvent){
	  applicationEventPublisher.publishEvent(smsEvent);
	}
}