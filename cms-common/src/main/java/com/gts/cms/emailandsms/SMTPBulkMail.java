package com.gts.cms.emailandsms;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Component
public class SMTPBulkMail implements BulkMail {

	@Autowired
	private JavaMailSender mailSender;

	private static final Logger LOGGER = Logger.getLogger(SMTPBulkMail.class);
	private String to, from, subject, body, htmlBody, attachment;
	private List<String> toList;
	public void setFrom(String from) {
		this.from = from;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public void setHtmlBody(String htmlBody) {
		this.htmlBody = htmlBody;
	}

	public String send() {
		try {
			List<List<String>> shortList;
			shortList = chunkList(toList,20);
			for(List<String> list:shortList) {
				MimeMessage message = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(message, true);
				helper.setSubject(subject);
				helper.setText(body);
				//htmlBody = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional //EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:v=\"urn:schemas-microsoft-com:vml\"><head><meta content=\"text/html; charset=utf-8\" http-equiv=\"Content-Type\"/><meta content=\"width=device-width\" name=\"viewport\"/><meta content=\"IE=edge\" http-equiv=\"X-UA-Compatible\"/><title></title><style type=\"text/css\">body{margin: 0;padding: 0;}table,td,tr{vertical-align: top;border-collapse: collapse;}*{line-height: inherit;}a[x-apple-data-detectors=true]{color: inherit !important;text-decoration: none !important;}.ie-browser table{table-layout: fixed;}[owa] .img-container div,[owa] .img-container button{display: block !important;}[owa] .fullwidth button{width: 100% !important;}[owa] .block-grid .col{display: table-cell;float: none !important;vertical-align: top;}.ie-browser .block-grid,.ie-browser .num12,[owa] .num12,[owa] .block-grid{width: 600px !important;}.ie-browser .mixed-two-up .num4,[owa] .mixed-two-up .num4{width: 200px !important;}.ie-browser .mixed-two-up .num8,[owa] .mixed-two-up .num8{width: 400px !important;}.ie-browser .block-grid.two-up .col,[owa] .block-grid.two-up .col{width: 300px !important;}.ie-browser .block-grid.three-up .col,[owa] .block-grid.three-up .col{width: 300px !important;}.ie-browser .block-grid.four-up .col [owa] .block-grid.four-up .col{width: 150px !important;}.ie-browser .block-grid.five-up .col [owa] .block-grid.five-up .col{width: 120px !important;}.ie-browser .block-grid.six-up .col,[owa] .block-grid.six-up .col{width: 100px !important;}.ie-browser .block-grid.seven-up .col,[owa] .block-grid.seven-up .col{width: 85px !important;}.ie-browser .block-grid.eight-up .col,[owa] .block-grid.eight-up .col{width: 75px !important;}.ie-browser .block-grid.nine-up .col,[owa] .block-grid.nine-up .col{width: 66px !important;}.ie-browser .block-grid.ten-up .col,[owa] .block-grid.ten-up .col{width: 60px !important;}.ie-browser .block-grid.eleven-up .col,[owa] .block-grid.eleven-up .col{width: 54px !important;}.ie-browser .block-grid.twelve-up .col,[owa] .block-grid.twelve-up .col{width: 50px !important;}</style><style id=\"media-query\" type=\"text/css\">@media only screen and (min-width: 620px){.block-grid{width: 600px !important;}.block-grid .col{vertical-align: top;}.block-grid .col.num12{width: 600px !important;}.block-grid.mixed-two-up .col.num3{width: 150px !important;}.block-grid.mixed-two-up .col.num4{width: 200px !important;}.block-grid.mixed-two-up .col.num8{width: 400px !important;}.block-grid.mixed-two-up .col.num9{width: 450px !important;}.block-grid.two-up .col{width: 300px !important;}.block-grid.three-up .col{width: 200px !important;}.block-grid.four-up .col{width: 150px !important;}.block-grid.five-up .col{width: 120px !important;}.block-grid.six-up .col{width: 100px !important;}.block-grid.seven-up .col{width: 85px !important;}.block-grid.eight-up .col{width: 75px !important;}.block-grid.nine-up .col{width: 66px !important;}.block-grid.ten-up .col{width: 60px !important;}.block-grid.eleven-up .col{width: 54px !important;}.block-grid.twelve-up .col{width: 50px !important;}}@media (max-width: 620px){.block-grid,.col{min-width: 320px !important;max-width: 100% !important;display: block !important;}.block-grid{width: 100% !important;}.col{width: 100% !important;}.col>div{margin: 0 auto;}img.fullwidth,img.fullwidthOnMobile{max-width: 100% !important;}.no-stack .col{min-width: 0 !important;display: table-cell !important;}.no-stack.two-up .col{width: 50% !important;}.no-stack .col.num4{width: 33% !important;}.no-stack .col.num8{width: 66% !important;}.no-stack .col.num4{width: 33% !important;}.no-stack .col.num3{width: 25% !important;}.no-stack .col.num6{width: 50% !important;}.no-stack .col.num9{width: 75% !important;}.video-block{max-width: none !important;}.mobile_hide{min-height: 0px;max-height: 0px;max-width: 0px;display: none;overflow: hidden;font-size: 0px;}.desktop_hide{display: block !important;max-height: none !important;}}</style></head><body class=\"clean-body\" style=\"margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #B8CCE2;\"><style id=\"media-query-bodytag\" type=\"text/css\">@media (max-width: 620px){.block-grid{min-width: 320px!important; max-width: 100%!important; width: 100%!important; display: block!important;}.col{min-width: 320px!important; max-width: 100%!important; width: 100%!important; display: block!important;}.col > div{margin: 0 auto;}img.fullwidth{max-width: 100%!important; height: auto!important;}img.fullwidthOnMobile{max-width: 100%!important; height: auto!important;}.no-stack .col{min-width: 0!important; display: table-cell!important;}.no-stack.two-up .col{width: 50%!important;}.no-stack.mixed-two-up .col.num4{width: 33%!important;}.no-stack.mixed-two-up .col.num8{width: 66%!important;}.no-stack.three-up .col.num4{width: 33%!important}.no-stack.four-up .col.num3{width: 25%!important}}</style><table bgcolor=\"#B8CCE2\" cellpadding=\"0\" cellspacing=\"0\" class=\"nl-container\" role=\"presentation\" style=\"table-layout: fixed; vertical-align: top; min-width: 320px; Margin: 0 auto; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #B8CCE2; width: 100%;\" valign=\"top\" width=\"100%\"><tbody><tr style=\"vertical-align: top;\" valign=\"top\"><td style=\"word-break: break-word; vertical-align: top; border-collapse: collapse;\" valign=\"top\"><div style=\"background-color:transparent;\"><div class=\"block-grid\" style=\"Margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;;\"><div style=\"border-collapse: collapse;display: table;width: 100%;background-color:transparent;\"><div class=\"col num12\" style=\"min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top;;\"><div style=\"width:100% !important;\"><div style=\"border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;\"><div class=\"mobile_hide\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"divider\" role=\"presentation\" style=\"table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;\" valign=\"top\" width=\"100%\"><tbody><tr style=\"vertical-align: top;\" valign=\"top\"><td class=\"divider_inner\" style=\"word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 5px; padding-right: 5px; padding-bottom: 5px; padding-left: 5px; border-collapse: collapse;\" valign=\"top\"><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"divider_content\" height=\"40\" role=\"presentation\" style=\"table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; border-top: 0px solid transparent; height: 40px;\" valign=\"top\" width=\"100%\"><tbody><tr style=\"vertical-align: top;\" valign=\"top\"><td height=\"40\" style=\"word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse;\" valign=\"top\"><span></span></td></tr></tbody></table></td></tr></tbody></table></div></div></div></div></div></div></div><div style=\"background-color:transparent;\"><div class=\"block-grid\" style=\"Margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffd500;;\"><div style=\"border-collapse: collapse;display: table;width: 100%;background-color:#ffd500;\"><div class=\"col num12\" style=\"min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top;;\"><div style=\"width:100% !important;\"><div style=\"border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 20px;\"><div style=\"color:#555555;font-family:Georgia, Times, 'Times New Roman', serif;line-height:120%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;\"><div style=\"font-family: Georgia, Times, 'Times New Roman', serif; font-size: 12px; line-height: 14px; color: #555555;\"><p style=\"font-size: 14px; line-height: 24px; margin: 0;\"><span style=\"font-size: 20px;\"><strong>Genesis Tech Systems</strong></span></p></div></div></div></div></div></div></div></div><div style=\"background-color:transparent;\"><div class=\"block-grid\" style=\"Margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #FFFFFF;;\"><div style=\"border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;\"><div class=\"col num12\" style=\"min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top;;\"><div style=\"width:100% !important;\"><div style=\"border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:35px; padding-bottom:40px; padding-right: 35px; padding-left: 35px;\"><div style=\"color:#132F40;font-family:'Cabin', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:120%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;\"><div style=\"font-size: 12px; line-height: 14px; font-family: 'Cabin', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #132F40;\"><p style=\"font-size: 14px; line-height: 26px; margin: 0;\"><span style=\"font-size: 22px;\">Hello <strong>Username</strong>,</span></p></div></div><div style=\"color:#555555;font-family:'Cabin', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:120%;padding-top:20px;padding-right:10px;padding-bottom:10px;padding-left:10px;\"><div style=\"font-size: 12px; line-height: 14px; font-family: 'Cabin', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #555555;\"><p style=\"font-size: 14px; line-height: 19px; margin: 0;\"><span style=\"font-size: 16px;\"><span style=\"font-size: 16px; line-height: 19px;\">This is a sample email Test Mail<strong style=\"color: #ffbf00;\">Test Mail</strong></span></span></p></div></div></div></div></div></div></div></div><div style=\"background-color:transparent;\"><div class=\"block-grid two-up no-stack\" style=\"Margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #132f40;;\"><div style=\"border-collapse: collapse;display: table;width: 100%;background-color:#132f40;\"><div class=\"col num6\" style=\"max-width: 320px; min-width: 300px; display: table-cell; vertical-align: top;;\"><div style=\"width:100% !important;\"><div style=\"border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:15px; padding-bottom:15px; padding-right: 0px; padding-left: 25px;\"><div style=\"color:#F8F8F8;font-family:'Cabin', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:120%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;\"><div style=\"font-size: 12px; line-height: 14px; font-family: 'Cabin', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #F8F8F8;\"><p style=\"font-size: 14px; line-height: 16px; margin: 0;\"><span style=\"background-color: transparent; font-size: 14px; line-height: 16px;\"><strong>Genesis Tech Systems</strong></span></p><p style=\"font-size: 14px; line-height: 16px; margin: 0;\"><span style=\"background-color: transparent; font-size: 14px; line-height: 16px;\">dev@gentechsys.com</span></p></div></div></div></div></div><div class=\"col num6\" style=\"max-width: 320px; min-width: 300px; display: table-cell; vertical-align: top;;\"><div style=\"width:100% !important;\"><div style=\"border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;\"><div></div></div></div></div></div></div></div><div style=\"background-color:transparent;\"><div class=\"block-grid\" style=\"Margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;;\"><div style=\"border-collapse: collapse;display: table;width: 100%;background-color:transparent;\"><div class=\"col num12\" style=\"min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top;;\"><div style=\"width:100% !important;\"><div style=\"border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"divider\" role=\"presentation\" style=\"table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;\" valign=\"top\" width=\"100%\"><tbody><tr style=\"vertical-align: top;\" valign=\"top\"><td class=\"divider_inner\" style=\"word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 5px; padding-right: 5px; padding-bottom: 5px; padding-left: 5px; border-collapse: collapse;\" valign=\"top\"><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"divider_content\" height=\"30\" role=\"presentation\" style=\"table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; border-top: 0px solid transparent; height: 30px;\" valign=\"top\" width=\"100%\"><tbody><tr style=\"vertical-align: top;\" valign=\"top\"><td height=\"30\" style=\"word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border-collapse: collapse;\" valign=\"top\"><span></span></td></tr></tbody></table></td></tr></tbody></table></div></div></div></div></div></div></td></tr></tbody></table></body></html>";
				helper.setText(body, htmlBody);
				//helper.setTo(getListAsStringArr(list));
				helper.setBcc(getListAsStringArr(list));
				helper.setFrom(from);
				
				if (attachment != null && !attachment.isEmpty()) {
					  FileSystemResource fsr = new FileSystemResource(attachment);
					//  helper.addAttachment(fsr.getFilename(), fsr);
					///System.out.println(fsr.getFilename());
					//String newFileName = FileUtil.getRelativePathForMail(attachment);
					//String[] newFileNameArr = newFileName.split("\\?");   
					//attachment="http:\\ec2-13-127-125-121.ap-south-1.compute.amazonaws.com:9000\\cms\\genesis\\uploads\\1558613075559-testfile1.png";
					File f = new File(fsr.getFilename());					
					//String fileName = FileUtil.getAbsolutePath(attachment);
					  downloadUsingStream(attachment, f); 
					  helper.addAttachment(f.getName(), f);
				}
				 
				mailSender.send(message);
			}
			LOGGER.info("Email sent!"+shortList);
		}catch(Exception ex) {
			//ex.printStackTrace();
			if(ex instanceof FileNotFoundException) {
				LOGGER.debug("ERROR:Given file not found!");
			}
			LOGGER.error("The email was not sent.");
			LOGGER.error("Error message: " + ex.getMessage());
		}
		return null;
	}

	public BulkMail withFrom(String from) {
		this.from = from;
		return this;
	}

	public BulkMail withTo(String to) {
		this.to = to;
		return this;
	}

	public BulkMail withBody(String body) {
		this.body = body;
		return this;
	}

	public BulkMail withHtmlBody(String htmlBody) {
		this.htmlBody = htmlBody;
		return this;
	}

	public BulkMail withSubject(String subject) {
		this.subject = subject;
		return this;
	}

	@Override
	public BulkMail withToList(List<String> toList) {
		this.toList = toList;
		return this;
	}

	@Override
	public BulkMail withAttachment(String attachment) {
		this.attachment = attachment;
		return this;
	}

	/*@Override
	public SendEmailResult sendMail() {
		//This Method sh
		return null;
	}

	@Override
	public SendRawEmailResult sendAttachment() {
		// TODO Auto-generated method stub
		return null;
	}*/

	public Boolean sendSmtpMail() throws MessagingException {
		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true);

		helper.setSubject(subject);
		helper.setText(body);
		helper.setTo(to);
		helper.setFrom(from);

		helper.addAttachment("attachment.jpg", new ClassPathResource("attachment.jpg"));
		mailSender.send(message);
		return null;
	}

	/**
	 * Converts List<String> to String[]
	 * @param list
	 * @return String[]
	 */
	private String[] getListAsStringArr(List<String> list) {
		String[] emailArr = new String[list.size()];
			for (int j = 0; j < list.size(); j++) {
				emailArr[j] = list.get(j);
			}
		return emailArr;
	}

	public static  <T> List<List<T>> chunkList(List<T> list, int chunkSize) {
	    if (chunkSize <= 0) {
	        throw new IllegalArgumentException("Invalid chunk size: " + chunkSize);
	    }
	    List<List<T>> chunkList = new ArrayList<>(list.size() / chunkSize);
	    for (int i = 0; i < list.size(); i += chunkSize) {
	        chunkList.add(list.subList(i, i + chunkSize >= list.size() ? list.size() : i + chunkSize));
	    }
	    return chunkList;
	}
	
	private static void downloadUsingStream(String urlStr, File file) {
		try {
			URL url = new URL(urlStr);
	        BufferedInputStream bis = new BufferedInputStream(url.openStream());
	        FileOutputStream fis = new FileOutputStream(file);
	        byte[] buffer = new byte[1024];
	        int count=0;
	        while((count = bis.read(buffer,0,1024)) != -1)
	        {
	            fis.write(buffer, 0, count);
	        }
	        fis.close();
	        bis.close();
			
		} catch (Exception ex) {
			
		}
    }	
}
