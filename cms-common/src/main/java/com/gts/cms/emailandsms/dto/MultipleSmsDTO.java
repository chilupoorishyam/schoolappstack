package com.gts.cms.emailandsms.dto;

import java.io.Serializable;
import java.util.Date;

public class MultipleSmsDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String mobileNumber;
	private String message;
	
	private String studentName;
	private String studentRollNo;
	private String studentCourse;
	private Long schoolId;
	private String senderId;
	private Long patternId;
	private Date messagingDate;
	private String messageContent;
	private Boolean isSmsAlert;
	private Long sentByEmployeeId;
	private Long studentId;
	private Long academicYearId;
	
	
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getStudentRollNo() {
		return studentRollNo;
	}
	public void setStudentRollNo(String studentRollNo) {
		this.studentRollNo = studentRollNo;
	}
	public Long getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}
	public String getSenderId() {
		return senderId;
	}
	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}
	public Long getPatternId() {
		return patternId;
	}
	public void setPatternId(Long patternId) {
		this.patternId = patternId;
	}
	public Date getMessagingDate() {
		return messagingDate;
	}
	public void setMessagingDate(Date messagingDate) {
		this.messagingDate = messagingDate;
	}
	public String getMessageContent() {
		return messageContent;
	}
	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}
	public Boolean getIsSmsAlert() {
		return isSmsAlert;
	}
	public void setIsSmsAlert(Boolean isSmsAlert) {
		this.isSmsAlert = isSmsAlert;
	}
	public Long getSentByEmployeeId() {
		return sentByEmployeeId;
	}
	public void setSentByEmployeeId(Long sentByEmployeeId) {
		this.sentByEmployeeId = sentByEmployeeId;
	}
	public String getStudentCourse() {
		return studentCourse;
	}
	public void setStudentCourse(String studentCourse) {
		this.studentCourse = studentCourse;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}
}
