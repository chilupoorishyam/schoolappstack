package com.gts.cms.emailandsms;

import com.gts.cms.common.exception.ApiException;
import com.gts.cms.emailandsms.dto.MultipleSmsDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.AsyncRestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;

@Service
public class BulkSmsPointSms {
	private static final Logger LOG = Logger.getLogger(BulkSmsPointSms.class);

	@Value("${sms.point-sms-username}")
	private String username;

	@Value("${sms.point-sms-password}")
	private String password;

	@Value("${sms.point-sms-sender-id}")
	private String defaultSenderId;

	@Autowired
	private AsyncRestTemplate asyncRestTemplate;
	
	@Value("${temp.mobile}")
	private String mobileNumber;

	@Value("${spring.profiles}")
	private String environment;
	

/*	public String sendSingleMessage(String number, String message, String sender) {
		LOG.info("Entered into sendSingleMessage()");
		String smsSenderId = "";
		if (sender != null) {
			smsSenderId = sender;
		} else {
			smsSenderId = defaultSenderId;
		}
		try {
			// "http://pointsms.in/API/sms.php?username=[xxxxxx]&password=[xxxxxx]&from=[xxxxxxxx]&to=[xxxxxxxxxx]&msg=[xxxx]&type=1&dnd_check=0";
			String encUsername = "username=" + URLEncoder.encode(username, "UTF-8");
			String encPassword = "&password=" + URLEncoder.encode(password, "UTF-8");
			String encMessage = "&message=" + URLEncoder.encode(message, "UTF-8");
			String from = "&from=" + URLEncoder.encode(smsSenderId, "UTF-8");
			String to = "&to=" + URLEncoder.encode(number, "UTF-8");

			String data = "http://pointsms.in/API/sms.php?" + encUsername + encPassword + from + to + encMessage
					+ "&type=1&dnd_check=0";
			URL url = new URL(data);
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);

			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			final StringBuffer stringBuffer = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				stringBuffer.append(line);
			}
			rd.close();
			return stringBuffer.toString();
		} catch (Exception e) {
			LOG.error("Error SMS" + e);
			return "Error " + e;
		}
	}*/

	public String sendGroupMessage(List<String> numbersList, String message, String sender) {
		LOG.info("Entered into sendGroupMessage()");
		String smsSenderId = "";
		String numbers = "";

		if (sender != null) {
			smsSenderId = sender;
		} else {
			smsSenderId = defaultSenderId;
		}
		for (int i = 0; i < numbersList.size(); i++) {
			if (i != numbersList.size() - 1) {
				numbers = numbers + numbersList.get(i) + ",";
			} else {
				numbers = numbers + numbersList.get(i);
			
			}
		}
		final String[][] CHARACTERS = {
				{ "\\+", "%20" },
				{ "\\%21", "!" },
				{ "\\%27", "'" },
				{ "\\%28", "(" },
				{ "\\%29", ")" },
				{ "\\%7E", "~" }
		};
		try {
			String encUsername = "username=" + URLEncoder.encode(username, "UTF-8");
			String encPassword = "&password=" + URLEncoder.encode(password, "UTF-8");
			String mobile = "&mobile=" + numbers;
			String senderName = "&sendername=" + defaultSenderId;
			String encMessage = "&message=" + URLEncoder.encode(message, "UTF-8");
			for(String[] entry : CHARACTERS) {
				encMessage = encMessage.replaceAll(entry[0], entry[1]);
			}
			String data = "http://smsmaa.com/SMS_API/sendsms.php?" + encUsername + encPassword + mobile + senderName + encMessage
					+ "&routetype=1";
			URL url = new URL(data);
			URLConnection conn = null;
			conn = url.openConnection();
			conn.setDoOutput(true);

			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			final StringBuffer stringBuffer = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				stringBuffer.append(line);
			}
			rd.close();
			return stringBuffer.toString();
		} catch (Exception e) {
			throw new ApiException(e);
		}
	}

	public String sendMultipleMessageToMultipleUsers(MultipleSmsDTO sms, String sender) {
		LOG.info("Entered into sendMultipleMessageToMultipleUsers()");
		String smsSenderId = "";
		String numbers = "";

		if (sender != null) {
			smsSenderId = sender;
		} else {
			smsSenderId = defaultSenderId;
		}
		final String[][] CHARACTERS = {
				{ "\\+", "%20" },
				{ "\\%21", "!" },
				{ "\\%27", "'" },
				{ "\\%28", "(" },
				{ "\\%29", ")" },
				{ "\\%7E", "~" }
		};
		try {
			String encUsername = "username=" + URLEncoder.encode(username, "UTF-8");
			String encPassword = "&password=" + URLEncoder.encode(password, "UTF-8");
			String mobile = "&mobile=" + sms.getMobileNumber();
			String senderName = "&sendername=" + defaultSenderId;
			String encMessage = "&message=" + URLEncoder.encode(sms.getMessage(), "UTF-8");
			for(String[] entry : CHARACTERS) {
				encMessage = encMessage.replaceAll(entry[0], entry[1]);
			}
			String data = "http://smsmaa.com/SMS_API/sendsms.php?" + encUsername + encPassword + mobile + senderName + encMessage
					+ "&routetype=1";
			URL url = new URL(data);
			URLConnection conn = null;
			conn = url.openConnection();
			conn.setDoOutput(true);

			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			final StringBuffer stringBuffer = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				stringBuffer.append(line);
			}
			rd.close();
			return stringBuffer.toString();
		} catch (Exception e) {
			throw new ApiException(e);
		}
	}

	private void smsCall(String smsSenderId, final StringBuffer stringBuffer, StringBuilder numbers_msgs)
			throws UnsupportedEncodingException, MalformedURLException, IOException {
		String encUsername = "username=" + URLEncoder.encode(username, "UTF-8");
		String encPassword = "&password=" + URLEncoder.encode(password, "UTF-8");
		String from = "&from=" + URLEncoder.encode(smsSenderId, "UTF-8");
		String to = "&mno_msg=" + numbers_msgs.toString();

		String data = "http://pointsms.in/API/multi_messages.php?" + encUsername + encPassword + from + to
				+ "&type=1&dnd_check=0";
		LOG.info("Calling SMS API:"+data);
		URL url = new URL(data);
		URLConnection conn = url.openConnection();
		conn.setDoOutput(true);
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		
		String line;
		
		while ((line = rd.readLine()) != null) {
			stringBuffer.append(line);
		}
		rd.close();
	}

	
/*	public SmsApiBatchStatusResponseDTO getBatchStatus(String batchId) {
		LOG.info("Entered into getBatchStatus()");
		try {
			HttpURLConnection conn = (HttpURLConnection) new URL(ApiEndpoints.API_BATCH_STATUS_URL + "?")
					.openConnection();
			String data = "apikey=" + apiKey + "&batch_id=" + batchId;
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
			conn.getOutputStream().write(data.getBytes("UTF-8"));
			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			final StringBuilder stringBuilder = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null) {
				stringBuilder.append(line);
			}
			rd.close();
			SmsApiBatchStatusResponseDTO apiResponse = JsonUtil.jsonToJava(stringBuilder.toString(),
					SmsApiBatchStatusResponseDTO.class);
			return apiResponse;
		} catch (Exception e) {
			throw new ApiException(e);
		}
	}*/
}
