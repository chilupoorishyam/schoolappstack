package com.gts.cms.emailandsms.repository.custom.impl;

import com.gts.cms.emailandsms.repository.custom.MessagingRecipientRepositoryCustom;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

public class MessagingRecipientRepositoryCustomImpl extends QuerydslRepositorySupport implements MessagingRecipientRepositoryCustom {

	public MessagingRecipientRepositoryCustomImpl() {
		super(MessagingRecipientRepositoryCustomImpl.class);
	}
	


}
