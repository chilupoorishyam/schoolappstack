package com.gts.cms.emailandsms.service.impl;

import com.gts.cms.common.employee.repository.EmployeeDetailRepository;
import com.gts.cms.common.repository.SmsPatternRepository;
import com.gts.cms.common.student.repository.StudentDetailRepository;
import com.gts.cms.emailandsms.BulkMailService;
import com.gts.cms.emailandsms.repository.MessagingRecipientRepository;
import com.gts.cms.emailandsms.repository.MessagingRepository;
import com.gts.cms.emailandsms.service.EmailService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmailServiceImpl implements EmailService {

	private static final Logger LOGGER = Logger.getLogger(EmailServiceImpl.class);

	@Autowired
	MessagingRepository messagingRepository;

	@Autowired
	MessagingRecipientRepository messagingRecipientRepository;

	@Autowired
	private BulkMailService bulkMailService;

	@Autowired
	SmsPatternRepository smsPatternRepository;

	@Autowired
	StudentDetailRepository studentDetailRepository;

	@Autowired
	EmployeeDetailRepository employeeDetailRepository;
	
	private String ADMIN_EMAIL = "naveenkumar.gts@gmail.com";
	
	@Value("${temp.mobile}")
	private String mobileNumber;

	@Value("${spring.profiles}")
	private String environment;
	
	@Value("${temp.email}")
	private String tempEmail;
	
	@Autowired
	@Qualifier("threadPoolTaskExecutor")
	private TaskExecutor taskExecutor;



	/**
	 * Returns List of the List argument passed to this function with size =
	 * chunkSize
	 * 
	 * @param chunkSize maximum size of each partition
	 * @param           <T> Generic type of the List
	 * @return A list of Lists which is portioned from the original list
	 */
	public static <T> List<List<T>> chunkList(List<T> list, int chunkSize) {
		if (chunkSize <= 0) {
			throw new IllegalArgumentException("Invalid chunk size: " + chunkSize);
		}
		List<List<T>> chunkList = new ArrayList<>(list.size() / chunkSize);
		for (int i = 0; i < list.size(); i += chunkSize) {
			chunkList.add(list.subList(i, i + chunkSize >= list.size() ? list.size() : i + chunkSize));
		}
		return chunkList;
	}
}

	

