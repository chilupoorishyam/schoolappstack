package com.gts.cms.emailandsms;

import java.util.List;

public interface BulkMail {
	void setFrom(String from);
	void setTo(String to);
	void setSubject(String subject);
	void setBody(String body);
	void setHtmlBody(String htmlBody);
	String send();
	
	//SendEmailResult sendMail();
	//SendRawEmailResult sendAttachment();*/
	
	BulkMail withFrom(String from);
	BulkMail withTo(String to);
	BulkMail withToList(List<String> toList);
	BulkMail withBody(String body);
	BulkMail withHtmlBody(String htmlBody);
	BulkMail withSubject(String subject);
	BulkMail withAttachment(String attachment);
}
