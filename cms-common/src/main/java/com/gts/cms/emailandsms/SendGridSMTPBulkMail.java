package com.gts.cms.emailandsms;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

@Service
public class SendGridSMTPBulkMail implements BulkMail {

    private static final Logger LOGGER = Logger.getLogger(SendGridSMTPBulkMail.class);

    private String to;
    String from;
    String subject;
    String body;
    String htmlBody;

    //SendGrid credentials
    public static final String SMTP_USERNAME = "apikey";
    public static final String SMTP_PASSWORD = "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";
    public static final String HOST = "smtp.sendgrid.net";
    public static final int PORT_587 = 587;

    //SendinBlue credentials
	/*public static final String SMTP_USERNAME = "shyamchilupoori@gmail.com";
	public static final String SMTP_PASSWORD = "RVYXSO1tIh5wpK7B";
	public static final String HOST = "smtp-relay.sendinblue.com";
	public static final int PORT_587 = 587;
    public static final int PORT_465 = 465;*/


    final static String SUBJECT = "SendGrid test (SMTP interface accessed using Java)";
    final static String BODY = String.join(
            System.getProperty("line.separator"),
            "<h1>SendGrid SES SMTP Email Test</h1>",
            "<p>This email was sent with Amazon SES using the </p>"
    );

    public void setFrom(String from) {
        this.from = from;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setHtmlBody(String htmlBody) {
        this.htmlBody = htmlBody;
    }

    public String send() {
        Transport transport = null;
        String result = null;
        try {
            Properties props = System.getProperties();
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.ssl.trust", HOST);
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", HOST);
            props.put("mail.smtp.port", PORT_587);
            //props.put("mail.smtp.connectiontimeout", "3600");
            // props.put("mail.smtp.timeout", "3600");

            Session session = Session.getDefaultInstance(props);
            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(from));
            // msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            // msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse("chilupoorishyam@gentechsyspro.com,chilupoorishyam@gmail.com"));
            msg.setSubject(subject);
            msg.setContent(body,"text/html");
            msg.setContent(htmlBody, "text/html");
            transport = session.getTransport();

            LOGGER.info("Sending...");
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
            transport.sendMessage(msg, msg.getAllRecipients());
            LOGGER.info("Email sent!");
            result = MailParameter.MAIL_SUCCESS.getParamName();
        } catch (Exception ex) {
            LOGGER.info("The email was not sent.");
            LOGGER.error("Error message: " + ex.getMessage());
            result = MailParameter.MAIL_FAILURE.getParamName();
        } finally {
            try {
                if (transport != null) {
                    transport.close();
                }
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    /*@Override
    public SendEmailResult sendMail() {
        Transport transport = null;
        SendEmailResult sendEmailResult = new SendEmailResult();
        try {
            Properties props = System.getProperties();
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.ssl.trust", HOST);
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", HOST);
            props.put("mail.smtp.port", PORT_587);
            //props.put("mail.smtp.connectiontimeout", "3600");
            // props.put("mail.smtp.timeout", "3600");

            Session session = Session.getDefaultInstance(props);
            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(from));
            // msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            // msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse("chilupoorishyam@gentechsyspro.com,chilupoorishyam@gmail.com"));
            msg.setSubject(subject);
            msg.setContent(body,"text/html");
            msg.setContent(htmlBody, "text/html");
            transport = session.getTransport();

            LOGGER.info("Sending...");
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
            transport.sendMessage(msg, msg.getAllRecipients());
            LOGGER.info("Email sent!");
            sendEmailResult.setMessageId(MailParameter.MAIL_SUCCESS.getParamName());
        } catch (Exception ex) {
            LOGGER.info("The email was not sent.");
            LOGGER.error("Error message: " + ex.getMessage());
            sendEmailResult.setMessageId(MailParameter.MAIL_FAILURE.getParamName());
        } finally {
            try {
                if (transport != null) {
                    transport.close();
                }
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
        return sendEmailResult;
    }*/

    public BulkMail withFrom(String from) {
        this.from = from;
        return this;
    }

    public BulkMail withTo(String to) {
        this.to = to;
        return this;
    }

    public BulkMail withBody(String body) {
        this.body = body;
        return this;
    }

    public BulkMail withHtmlBody(String htmlBody) {
        this.htmlBody = htmlBody;
        return this;
    }

    public BulkMail withSubject(String subject) {
        this.subject = subject;
        return this;
    }

    @Override
    public BulkMail withToList(List<String> toList) {
        String toData = toList.stream().collect(Collectors.joining(","));
        this.to = toData;
        return this;
    }

    @Override
    public BulkMail withAttachment(String attachment) {
        // TODO Auto-generated method stub
        return null;
    }


    /*@Override
    public SendRawEmailResult sendAttachment() {
        // TODO Auto-generated method stub
        return null;
    }*/

    public static <T> List<List<T>> chunkList(List<T> list, int chunkSize) {
        if (chunkSize <= 0) {
            throw new IllegalArgumentException("Invalid chunk size: " + chunkSize);
        }
        List<List<T>> chunkList = new ArrayList<>(list.size() / chunkSize);
        for (int i = 0; i < list.size(); i += chunkSize) {
            chunkList.add(list.subList(i, i + chunkSize >= list.size() ? list.size() : i + chunkSize));
        }
        return chunkList;
    }

    private enum MailParameter {

        /**
         * The HTL_CD.
         */
        MAIL_SUCCESS("SUCCESS"),

        /**
         * The RM_CD.
         */
        MAIL_FAILURE("FAILURE");

        /**
         * The param name.
         */
        private final String paramName;

        /**
         * Instantiates a new field parameter.
         *
         * @param paramName the param name
         */
        private MailParameter(String paramName) {
            this.paramName = paramName;
        }

        /**
         * Gets the param name.
         *
         * @return the param name
         */
        public String getParamName() {
            return paramName;
        }
    }

/*ERROR	MESSAGE	EXPLANATION
250: Queued mail for delivery`	Your mail has been successfully queued! This response indicates that the recipient server has accepted the message.
403: You are not authorized to send from that email address`	This error means that you need to update the “from” address for all of your emails to include a domain you own and control.
421: Message from (X.X.X.X) temporarily deferred`	Messages are temporarily deferred because of recipient server policy - often it's because of too many messages or connections in too short of a timeframe. We continue to retry deferred messages for up to 72 hours. Consider temporarily sending less messages to a domain that is returning this code because this could further delay your messages currently being tried.
450: too frequent connects from 198.37.147.135, please try again later.`	The message failed because the recipient's mailbox was unavailable, perhaps because it was locked or was not routable at the time. We continue to retry messages for up to 72 hours. Consider temporarily sending less messages to a domain that is returning this code because this could further delay your messages currently being tried.
451: Temporary local problem - please try later`	The message simply failed, usually due to a far-end server error. We continue to retry messages for up to 72 hours.
451: Authentication failed: Maximum credits exceeded`	There is a credit limit of emails per day enforced in error. Contact support to remove that limit.
452: Too many recipients received this hour (throttled)`	The message has been deferred due to insufficient system storage. We continue to retry messages for up to 72 hours.
550: Requested action not taken: mailbox unavailable`	The user’s mailbox was unavailable. Usually because it could not be found, or because of incoming policy reasons. Remove these address from your list - it is likely a fake, or it was mistyped.
551: User does not exist.`	The intended mailbox does not exist on this recipient server. Remove these addresses from your list.
552: This message is larger than the current system limit or the recipient’s mailbox is full. Create a shorter message body or remove attachments and try sending it again.`	The recipients mailbox has exceeded its storage limits. We don't resend messages with this error code because this is usually a sign this is an abandoned email.
553: Invalid/inactive user.`	The message was refused because the mailbox name is either malformed or does not exist. Remove these addresses from your list.
554: ERROR: Mail refused`	This is a default response that can be caused by a lot of issues. There is often a human readable portion of this error that gives more detailed information, but if not, remove these addresses from your list.
Other: Delayed Bounce - Unable to Parse Server Reason`	This is what SendGrid displays when the reciepients server returns a blank reason code.*/

}
