package com.gts.cms.emailandsms.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SmsApiBatchStatusResponseDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String batch_id;
	private Integer num_messages;
	private Integer num_delivered;
	private Integer num_undelivered;
	private Integer num_unknown;
	private Integer num_invalid;
	private String status;
	private ArrayList<SmsApiResponseBatchMessageDTO> messages;
	public String getBatch_id() {
		return batch_id;
	}
	public void setBatch_id(String batch_id) {
		this.batch_id = batch_id;
	}
	public Integer getNum_messages() {
		return num_messages;
	}
	public void setNum_messages(Integer num_messages) {
		this.num_messages = num_messages;
	}
	public Integer getNum_delivered() {
		return num_delivered;
	}
	public void setNum_delivered(Integer num_delivered) {
		this.num_delivered = num_delivered;
	}
	public Integer getNum_undelivered() {
		return num_undelivered;
	}
	public void setNum_undelivered(Integer num_undelivered) {
		this.num_undelivered = num_undelivered;
	}
	public Integer getNum_unknown() {
		return num_unknown;
	}
	public void setNum_unknown(Integer num_unknown) {
		this.num_unknown = num_unknown;
	}
	public Integer getNum_invalid() {
		return num_invalid;
	}
	public void setNum_invalid(Integer num_invalid) {
		this.num_invalid = num_invalid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ArrayList<SmsApiResponseBatchMessageDTO> getMessages() {
		return messages;
	}
	public void setMessages(ArrayList<SmsApiResponseBatchMessageDTO> messages) {
		this.messages = messages;
	}

}
