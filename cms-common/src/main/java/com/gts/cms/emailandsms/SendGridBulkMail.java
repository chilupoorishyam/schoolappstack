package com.gts.cms.emailandsms;

import com.amazonaws.services.simpleemail.model.SendEmailResult;
import com.amazonaws.services.simpleemail.model.SendRawEmailResult;
import com.sendgrid.*;

import java.util.Arrays;
import java.util.List;

public class SendGridBulkMail implements BulkMail {
	
	private static String  SENDGRID_API_KEY = "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";

	private String to, from, subject, body, htmlBody;

	public void setFrom(String from) {
		this.from = from;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public void setHtmlBody(String htmlBody) {
		this.htmlBody = htmlBody;
	}

	private List<String> getToAsList() {
		return Arrays.asList(to.split(","));
	}

	public String send() {
		Email from2 = new Email(from);
		//Personalization personalization = new Personalization();
		 //personalization.setSubject(subject);
		// String subject = "Sending with SendGrid is Fun";
		for (String e : getToAsList()) {
			Email to = new Email();
			// to.setName("Example User");
			to.setEmail(e);
			
			Content content = new Content("text/plain", body);
			Mail mail = new Mail(from2, subject, to, content);

			SendGrid sg = new SendGrid(SENDGRID_API_KEY);
			Request request = new Request();
			try {
				request.setMethod(Method.POST);
				request.setEndpoint("mail/send");
				request.setBody(mail.build());
				Response response = sg.api(request);
				System.out.println(response.getStatusCode());
				System.out.println(response.getBody());
				System.out.println(response.getHeaders());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	public BulkMail withFrom(String from) {
		this.from = from;
		return this;
	}

	public BulkMail withTo(String to) {
		this.to = to;
		return this;
	}

	public BulkMail withBody(String body) {
		this.body = body;
		return this;
	}

	public BulkMail withHtmlBody(String htmlBody) {
		this.htmlBody = htmlBody;
		return this;
	}

	public BulkMail withSubject(String subject) {
		this.subject = subject;
		return this;
	}

	@Override
	public BulkMail withToList(List<String> toList) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BulkMail withAttachment(String attachment) {
		// TODO Auto-generated method stub
		return null;
	}

	/*@Override
	public SendEmailResult sendMail() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SendRawEmailResult sendAttachment() {
		// TODO Auto-generated method stub
		return null;
	}*/

}
