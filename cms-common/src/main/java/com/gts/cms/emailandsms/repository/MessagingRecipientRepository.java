package com.gts.cms.emailandsms.repository;

import com.gts.cms.emailandsms.repository.custom.MessagingRecipientRepositoryCustom;
import com.gts.cms.entity.MessagingRecipient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
@Repository
public interface MessagingRecipientRepository extends JpaRepository<MessagingRecipient, Long>,MessagingRecipientRepositoryCustom{

	@Query("SELECT mr FROM MessagingRecipient mr "
            + " where 1=1 "
            + " AND (:schoolId is null or mr.school.schoolId = :schoolId)"
            + " AND (:studentId is null or mr.studentDetail.studentId = :studentId) "
            + " AND (:employeeId is null or mr.employeeDetail.employeeId = :employeeId) "
            + " AND (:messageSentDate is null or (DATE(mr.messageSentDate)) = (DATE(:messageSentDate))) "
            + " AND (mr.emailId is NOT NULL) "
            )
   

	List<MessagingRecipient> findMessagingDetails(@Param("schoolId") Long schoolId,
                                                  @Param("studentId") Long studentId,
                                                  @Param("employeeId") Long employeeId,
                                                  @Param("messageSentDate") Date messageSentDate);



}
