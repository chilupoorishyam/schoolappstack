package com.gts.cms.emailandsms.events;

import com.gts.cms.emailandsms.dto.SmsApiResponseDTO;
import org.springframework.context.ApplicationEvent;

import java.util.ArrayList;

public class SmsEvent extends ApplicationEvent {
	private ArrayList<String> mobile;
	private String sms;
	private String status;
	private SmsApiResponseDTO apiResponse;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setMobile(ArrayList<String> mobile) {
		this.mobile = mobile;
	}

	public void setSms(String sms) {
		this.sms = sms;
	}
	public SmsEvent(Object source, ArrayList<String> mobile, String sms) {
		super(source);
		this.mobile = mobile;
		this.sms = sms;
		
	}
	public SmsEvent(Object source, ArrayList<String> mobile, String sms,SmsApiResponseDTO apiResponse) {
		super(source);
		this.mobile = mobile;
		this.sms = sms;
		this.apiResponse=apiResponse;
	}

	public ArrayList<String> getMobile() {
		return mobile;
	}

	public String getSms() {
		return sms;
	}

	public SmsApiResponseDTO getApiResponse() {
		return apiResponse;
	}

	public void setApiResponse(SmsApiResponseDTO apiResponse) {
		this.apiResponse = apiResponse;
	}
}