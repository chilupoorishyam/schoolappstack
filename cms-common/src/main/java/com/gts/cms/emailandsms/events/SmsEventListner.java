package com.gts.cms.emailandsms.events;

import com.gts.cms.emailandsms.BulkSms;
import com.gts.cms.emailandsms.dto.SmsApiResponseDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class SmsEventListner implements ApplicationListener<SmsEvent> {

	private static final Logger logger = Logger.getLogger(SmsEventListner.class);
	@Autowired
	private BulkSms smsService;

	@Override
	public void onApplicationEvent(SmsEvent smsEvent) {
		logger.debug("Entered into onApplicationEvent() ");
		//ArrayList<String> nos = new ArrayList<String>();
		//nos.add(smsEvent.getMobile());
		//String jsonString = smsService.sendMessage(smsEvent.getSms(),"GENTECH",nos);
		SmsApiResponseDTO apiResponseDTO = smsService.sendGroupMessage(smsEvent.getMobile(),smsEvent.getSms(),null);
		smsEvent.setApiResponse(apiResponseDTO);
		logger.debug("onApplicationEvent() executed");
	}

}