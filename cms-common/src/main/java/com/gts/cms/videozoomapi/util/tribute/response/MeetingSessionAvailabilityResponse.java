package com.gts.cms.videozoomapi.util.tribute.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MeetingSessionAvailabilityResponse {

    private Long sessionId;
    private Boolean isAvailable;
    private String bookedBy;

}
