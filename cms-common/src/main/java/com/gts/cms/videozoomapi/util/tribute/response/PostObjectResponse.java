package com.gts.cms.videozoomapi.util.tribute.response;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class PostObjectResponse {

    private String uploadUri;

    private String staticUri;

    private Map<String, String> formData;

}
