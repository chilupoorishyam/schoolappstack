package com.gts.cms.videozoomapi.repository;

import com.gts.cms.entity.LiveClassStorage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LiveClassStorageRepo extends JpaRepository<LiveClassStorage, Long> {
    List<LiveClassStorage> findByGroupUid(String groupUid);
}
