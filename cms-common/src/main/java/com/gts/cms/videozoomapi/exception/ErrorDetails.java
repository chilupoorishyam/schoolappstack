package com.gts.cms.videozoomapi.exception;

import lombok.*;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class ErrorDetails {
    private Date timestamp;
    private String message;
    private String details;
}
