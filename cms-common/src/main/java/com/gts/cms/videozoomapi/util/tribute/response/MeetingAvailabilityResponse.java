package com.gts.cms.videozoomapi.util.tribute.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MeetingAvailabilityResponse {

    private Long hostId;

    private Set<MeetingSessionAvailabilityResponse> sessions;
}
