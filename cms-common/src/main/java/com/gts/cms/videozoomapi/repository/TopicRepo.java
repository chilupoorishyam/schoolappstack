package com.gts.cms.videozoomapi.repository;

import com.gts.cms.entity.Topic;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicRepo extends PagingAndSortingRepository<Topic, Long> {

    Page<Topic> findByUnit_Id(Long unitId, Pageable pageable);
}
