package com.gts.cms.videozoomapi.repository;

import com.gts.cms.entity.CodissLiveClassSchedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface CodissLiveClassScheduleRepo extends JpaRepository<CodissLiveClassSchedule, Long> {
    @Query("SELECT ls FROM CodissLiveClassSchedule ls"
            + " WHERE 1=1 "
            + " AND (:employeeId is null or ls.classEmployeeDetail.employeeId=:employeeId) "
            + " AND (:groupSectionId is null or ls.groupSection.groupSectionId=:groupSectionId) "
            + " AND (:timetableScheduleId is null or ls.schedule.timetableScheduleId=:timetableScheduleId) "
            + " AND (:weekdayId is null or ls.weekday.weekdayId=:weekdayId) "
            + " AND (:schoolId is null or ls.school.schoolId=:schoolId) "
            + " AND (:scheduledOnDate is null or ls.scheduledOnDate=:scheduledOnDate)"
            + " ORDER BY scheduledOnDate DESC, fromTime DESC"
    )
    List<CodissLiveClassSchedule> findZoomDetails(Long employeeId, Long groupSectionId,
                                            Long timetableScheduleId, Long weekdayId,
                                            Long schoolId, LocalDate scheduledOnDate);
}
