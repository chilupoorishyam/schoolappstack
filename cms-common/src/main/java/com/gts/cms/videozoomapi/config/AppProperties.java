package com.gts.cms.videozoomapi.config;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@ConfigurationProperties(prefix = "app")
@Getter
@Setter
public class AppProperties {

    private final Storage storage = new Storage();
    private final Zoom zoom = new Zoom();
    private final MicrosoftGraph microsoftGraph = new MicrosoftGraph();

    @Data
    public static final class Storage {
        private String host;
        private String uploadUri;
        private String accessKey;
        private String secretKey;
        private String bucketRoot;

    }
    @Data
    public static final class Zoom {
        private String clientId;
        private String clientSecret;
        private String apiKey;
        private String apiSecret;
        private String apiRootUri;
        private String tokenUri;
        private String redirectUri;
    }

    @Data
    public static final class MicrosoftGraph {
        private String clientId;
        private String clientSecret;
        private String tenantId;
        private String preferredTimezone;
        private List<String> scopes;
    }
}
