package com.gts.cms.videozoomapi.repository;

import com.gts.cms.entity.Subjeects;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubjectRepo extends PagingAndSortingRepository<Subjeects, Long> {
}
