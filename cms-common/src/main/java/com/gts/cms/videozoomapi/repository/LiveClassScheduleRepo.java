package com.gts.cms.videozoomapi.repository;

import com.gts.cms.entity.LiveClassSchedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface LiveClassScheduleRepo extends JpaRepository<LiveClassSchedule, Long> {
    @Query("SELECT ls FROM LiveClassSchedule ls"
            + " WHERE 1=1 "
            + " AND (ls.zoomId != null) "
            + " AND (:eventDate is null or ls.scheduledOnDate=:eventDate)"
    )
    List<LiveClassSchedule> findZoomDetails(LocalDate eventDate);

    @Query("SELECT ls FROM LiveClassSchedule ls"
            + " WHERE 1=1 "
            + " AND (ls.team != null) "
            + " AND (:eventDate is null or ls.scheduledOnDate=:eventDate)"
    )
    List<LiveClassSchedule> findTeamsDetails(LocalDate eventDate);

    @Query("SELECT ls FROM LiveClassSchedule ls"
            + " WHERE 1=1 "
            + " AND (ls.zoomId != null) "
            + " AND (:employeeId is null or ls.classEmployeeDetail.employeeId=:employeeId) "
            + " AND (:groupSectionId is null or ls.groupSection.groupSectionId=:groupSectionId) "
            + " AND (:timetableScheduleId is null or ls.schedule.timetableScheduleId=:timetableScheduleId) "
            + " AND (:weekdayId is null or ls.weekday.weekdayId=:weekdayId) "
            + " AND (:schoolId is null or ls.school.schoolId=:schoolId) "
            + " AND (:scheduledOnDate is null or ls.scheduledOnDate=:scheduledOnDate)"
            + " ORDER BY scheduledOnDate DESC, fromTime DESC"
    )
    List<LiveClassSchedule> findZoomDetails(Long employeeId, Long groupSectionId,
                                        Long timetableScheduleId, Long weekdayId,
                                        Long schoolId, LocalDate scheduledOnDate);

    @Query("SELECT ls FROM LiveClassSchedule ls"
            + " WHERE 1=1 "
            + " AND (ls.team != null) "
            + " AND (:employeeId is null or ls.classEmployeeDetail.employeeId=:employeeId) "
            + " AND (:groupSectionId is null or ls.groupSection.groupSectionId=:groupSectionId) "
            + " AND (:timetableScheduleId is null or ls.schedule.timetableScheduleId=:timetableScheduleId) "
            + " AND (:weekdayId is null or ls.weekday.weekdayId=:weekdayId) "
            + " AND (:schoolId is null or ls.school.schoolId=:schoolId) "
            + " AND (:scheduledOnDate is null or ls.scheduledOnDate=:scheduledOnDate)"
    )
    List<LiveClassSchedule> findTeamsDetails(Long employeeId, Long groupSectionId,
                                            Long timetableScheduleId, Long weekdayId,
                                            Long schoolId, LocalDate scheduledOnDate);

    List<LiveClassSchedule> findByRecordingFilesAvailable(boolean bool);

    LiveClassSchedule findByZoomMeetingId(String meetingNumber);
}
