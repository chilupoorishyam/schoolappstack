package com.gts.cms.videozoomapi.util.tribute.request;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class MeetingScheduleRequest {

    private String agenda;

    @NotNull
    private LocalDate eventDate;

    @NotNull
    private Boolean hostVideo;

    @Pattern(regexp = "^[a-zA-Z0-9]+$")
    @NotNull
    @Size(min = 5, max = 10)
    private String password;

    @NotNull
    @NotEmpty
    private Set<Long> sessionIds;

    @NotNull
    private String topic;

}
