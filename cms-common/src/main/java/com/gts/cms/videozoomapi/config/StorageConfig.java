package com.gts.cms.videozoomapi.config;

import io.minio.MinioClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class StorageConfig {

    private final AppProperties app;

    @Bean
    public MinioClient minioClient() {
        return MinioClient
                .builder()
                .endpoint(app.getStorage().getHost())
                .credentials(app.getStorage().getAccessKey(), app.getStorage().getSecretKey())
                .build();
    }

}
