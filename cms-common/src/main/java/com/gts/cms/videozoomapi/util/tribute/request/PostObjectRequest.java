package com.gts.cms.videozoomapi.util.tribute.request;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class PostObjectRequest {

    @NotNull
    private String subject;

    @NotNull
    private String filename;

}
