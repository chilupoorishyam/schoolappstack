package com.gts.cms.videozoomapi.repository;

import com.gts.cms.entity.MeetingSchedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface MeetingScheduleRepo extends JpaRepository<MeetingSchedule, String> {

    List<MeetingSchedule> findByEventDateEquals(LocalDate eventDate);
}
