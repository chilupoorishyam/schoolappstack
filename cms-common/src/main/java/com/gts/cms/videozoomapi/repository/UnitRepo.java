package com.gts.cms.videozoomapi.repository;

import com.gts.cms.entity.Unit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UnitRepo extends PagingAndSortingRepository<Unit, Long> {

    Page<Unit> findBySubject_Id(Long subjectId, Pageable pageable);

}
