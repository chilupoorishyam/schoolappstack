package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.CategoryDTO;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.Category;
import com.gts.cms.entity.Organization;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class CategoryMapper implements BaseMapper<Category, CategoryDTO> {

	@Override
	public CategoryDTO convertEntityToDTO(Category entityObject) {
		CategoryDTO dtoObject = null;
		if(entityObject != null) {
			dtoObject = new CategoryDTO();
			dtoObject.setCategoryId(entityObject.getCategoryId());
			if(entityObject.getOrganization() != null) {
				  dtoObject.setOrganizationId(entityObject.getOrganization().getOrganizationId());
				  dtoObject.setOrgCode(entityObject.getOrganization().getOrgCode());
				  dtoObject.setOrgName(entityObject.getOrganization().getOrgName());
		
				}
			dtoObject.setAchievementCategory(entityObject.getAchievementCategory());
			dtoObject.setAchievementCategoryCode(entityObject.getAchievementCategoryCode());
			dtoObject.setCategoryLogoPath(entityObject.getCategoryLogoPath());
			dtoObject.setIsActive(entityObject.getIsActive());
			dtoObject.setReason(entityObject.getReason());
			dtoObject.setCreatedDt(new Date());
			dtoObject.setCreatedUser(SecurityUtil.getCurrentUser());
			dtoObject.setUpdatedDt(new Date());
			dtoObject.setUpdatedUser(SecurityUtil.getCurrentUser());
		}
		return dtoObject;
			
	}

	@Override
	public List<CategoryDTO> convertEntityListToDTOList(List<Category> entityObjectList) {
		List<CategoryDTO> dtoObjectList = new ArrayList<>();
		entityObjectList.forEach(entityObject -> dtoObjectList.add(convertEntityToDTO(entityObject)));
		return dtoObjectList;
	}

	@Override
	public List<Category> convertDTOListToEntityList(List<CategoryDTO> dtoObjectList) {
		List<Category> entityObjectList = new ArrayList<>();
		dtoObjectList.forEach(dtoObject -> entityObjectList.add(convertDTOtoEntity(dtoObject, null)));
		return entityObjectList;
	}

	@Override
	public Category convertDTOtoEntity(CategoryDTO dtoObject, Category entityObject) {
		if (null == entityObject) {
			entityObject = new  Category();
			entityObject.setIsActive(Boolean.TRUE);
			entityObject.setCreatedDt(new Date());
			entityObject.setCreatedUser(SecurityUtil.getCurrentUser());
		} else {
			entityObject.setIsActive(dtoObject.getIsActive());
		}
		entityObject.setCategoryId(dtoObject.getCategoryId());
		
		if (dtoObject.getOrganizationId() != null) {
			Organization organization = new Organization();
			organization.setOrganizationId(dtoObject.getOrganizationId());
			
			entityObject.setOrganization(organization);
		}
		entityObject.setAchievementCategory(dtoObject.getAchievementCategory());
		entityObject.setAchievementCategoryCode(dtoObject.getAchievementCategoryCode());
		entityObject.setCategoryLogoPath(dtoObject.getCategoryLogoPath());
		entityObject.setReason(dtoObject.getReason());
		entityObject.setUpdatedDt(new Date());
		entityObject.setUpdatedUser(SecurityUtil.getCurrentUser());
		
		return entityObject;
	}
	}

