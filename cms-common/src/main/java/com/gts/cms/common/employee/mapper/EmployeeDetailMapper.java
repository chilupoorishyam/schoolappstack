package com.gts.cms.common.employee.mapper;

import com.gts.cms.common.dto.EmployeeReportingDTO;
import com.gts.cms.common.employee.dto.*;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.util.FileUtil;
import com.gts.cms.common.util.OptionalUtil;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Genesis
 *
 */
@Component
public class EmployeeDetailMapper implements BaseMapper<EmployeeDetail, EmployeeDetailDTO> {


	@Autowired
	EmployeeDocumentCollectionMapper employeeDocumentCollectionMapper;

	@Autowired
	EmployeeBankDetailMapper employeeBankDetailMapper;

	@Autowired
	EmployeeReportingMapper employeeReportingMapper;

	@Autowired
	EmployeeExperienceDetailMapper employeeExperienceDetailMapper;

	@Autowired
	EmployeeEducationMapper employeeEducationMapper;

	@Value("${s3.url}")
	private String url;

	@Value("${s3.bucket-name}")
	private String bucketName;
	private static final String fileSeparator = FileUtil.getFileSeparator();

	/*
	 * @Autowired StudentAppDocCollectionMapper studentAppDocCollectionMapper;
	 * 
	 * @Autowired StudentDocumentCollectionMapper studentDocumentCollectionMapper;
	 */
	public EmployeeDetailDTO convertEntityToDTO(EmployeeDetail empDetail) {
		EmployeeDetailDTO empDetailDTO = null;
		if (empDetail != null) {
			empDetailDTO = new EmployeeDetailDTO();
			empDetailDTO.setEmployeeId(empDetail.getEmployeeId());
			empDetailDTO.setAadharNo(empDetail.getAadharNo());
			if (empDetail.getAadharPath() != null) {
				StringBuilder aadharFilePath = new StringBuilder();
				aadharFilePath = aadharFilePath.append(url).append(fileSeparator).append(bucketName)
						.append(fileSeparator).append(empDetail.getAadharPath());
				empDetailDTO.setAadharPath(aadharFilePath.toString());
			}
			empDetailDTO.setAddress(empDetail.getAddress());
			empDetailDTO.setAicteRegNo(empDetail.getAicteRegNo());
			empDetailDTO.setBiometricCode(empDetail.getBiometricCode());
			empDetailDTO.setCreatedDt(empDetail.getCreatedDt());
			empDetailDTO.setCreatedUser(empDetail.getCreatedUser());
			empDetailDTO.setDateOfBirth(empDetail.getDateOfBirth());
			empDetailDTO.setDateOfRelieving(empDetail.getDateOfRelieving());
			empDetailDTO.setEmail(empDetail.getEmail());
			empDetailDTO.setEmergencyMobile(empDetail.getEmergencyMobile());
			empDetailDTO.setEmpNumber(empDetail.getEmpNumber());
			empDetailDTO.setEpfNo(empDetail.getEpfNo());
			empDetailDTO.setEsiRegNo(empDetail.getEsiRegNo());
			empDetailDTO.setFacebookUrl(empDetail.getFacebookUrl());
			empDetailDTO.setFatherName(empDetail.getFatherName());
			empDetailDTO.setFirstName(empDetail.getFirstName());
			empDetailDTO.setBiometricId(empDetail.getBiometricId());
			empDetailDTO.setCurrentPayId(empDetail.getCurrentPayId());
			empDetailDTO.setPayrollPayId(empDetail.getPayrollPayId());
			empDetailDTO.setPayscaleId(empDetail.getPayscaleId());
			/*empDetailDTO.setReportingManagerId(empDetail.getReportingManagerId());*/
			
			if (empDetail.getReportingManager() != null) {
				empDetailDTO.setReportingManagerId(empDetail.getReportingManager().getEmployeeId());
				empDetailDTO.setReportingManagerName(empDetail.getReportingManager().getFirstName());
				empDetailDTO.setReportingManagerNumber(empDetail.getReportingManager().getEmpNumber());
			}
			empDetailDTO.setGoogleplusUrl(empDetail.getGoogleplusUrl());
			empDetailDTO.setIsActive(empDetail.getIsActive());
			empDetailDTO.setIsManager(empDetail.getIsManager());
			empDetailDTO.setIsPtax(empDetail.getIsPtax());
			empDetailDTO.setIsRatified(empDetail.getIsRatified());
			empDetailDTO.setIsTds(empDetail.getIsTds());
			empDetailDTO.setIsUsingCampAccommodation(empDetail.getIsUsingCampAccommodation());
			empDetailDTO.setIsUsingTransport(empDetail.getIsUsingTransport());
			empDetailDTO.setJntuDateOfJoining(empDetail.getJntuDateOfJoining());
			empDetailDTO.setJntuRegNo(empDetail.getJntuRegNo());
			empDetailDTO.setJoiningDate(empDetail.getJoiningDate());
			empDetailDTO.setLastName(empDetail.getLastName());
			empDetailDTO.setLicNo(empDetail.getLicNo());
			empDetailDTO.setLinkedinUrl(empDetail.getLinkedinUrl());
			empDetailDTO.setMiddleName(empDetail.getMiddleName());
			empDetailDTO.setMonthlySalary(empDetail.getMonthlySalary());
			empDetailDTO.setMotherName(empDetail.getMotherName());
			empDetailDTO.setNonTeachingExp(empDetail.getNonTeachingExp());
			empDetailDTO.setOfficialMobile(empDetail.getOfficialMobile());
			empDetailDTO.setPancard(empDetail.getPancard());
			empDetailDTO.setReason(empDetail.getReason());
			if (empDetail.getPancardPath() != null) {
				StringBuilder pancardPath = new StringBuilder();
				pancardPath = pancardPath.append(url).append(fileSeparator).append(bucketName).append(fileSeparator)
						.append(empDetail.getPancardPath());
				empDetailDTO.setPancardPath(pancardPath.toString());
			}

			empDetailDTO.setPassportNo(empDetail.getPassportNo());

			if (empDetail.getPassportPath() != null) {
				StringBuilder passportPath = new StringBuilder();
				passportPath = passportPath.append(url).append(fileSeparator).append(bucketName).append(fileSeparator)
						.append(empDetail.getPassportPath());
				empDetailDTO.setPassportPath(passportPath.toString());
			}
			empDetailDTO.setPermanentAddress(empDetail.getPermanentAddress());
			empDetailDTO.setPermanentMandal(empDetail.getPermanentMandal());
			empDetailDTO.setPermanentPincode(empDetail.getPermanentPincode());
			empDetailDTO.setPermanentStreet(empDetail.getPermanentStreet());

			if (empDetail.getPhotoPath() != null) {
				StringBuilder photoPath = new StringBuilder();
				photoPath = photoPath.append(url).append(fileSeparator).append(bucketName).append(fileSeparator)
						.append(empDetail.getPhotoPath());
				empDetailDTO.setPhotoPath(photoPath.toString());
			}

			empDetailDTO.setPresentAddress(empDetail.getPresentAddress());
			empDetailDTO.setPresentMandal(empDetail.getPresentMandal());
			empDetailDTO.setPresentPincode(empDetail.getPresentPincode());
			empDetailDTO.setPresentStreet(empDetail.getPresentStreet());
			empDetailDTO.setPromotedDate(empDetail.getPromotedDate());
			empDetailDTO.setResearchExp(empDetail.getResearchExp());
			empDetailDTO.setResidencePhone(empDetail.getResidencePhone());
			empDetailDTO.setResignationDate(empDetail.getResignationDate());
			empDetailDTO.setServiceBreakYrs(empDetail.getServiceBreakYrs());
			empDetailDTO.setStatusDescription(empDetail.getStatusDescription());
			empDetailDTO.setTeachingExp(empDetail.getTeachingExp());
			empDetailDTO.setTenureDays(empDetail.getTenureDays());
			empDetailDTO.setVoterId(empDetail.getVoterId());

			if (empDetail.getVoterIdPath() != null) {
				StringBuilder voterIdPath = new StringBuilder();
				voterIdPath = voterIdPath.append(url).append(fileSeparator).append(bucketName).append(fileSeparator)
						.append(empDetail.getVoterIdPath());
				empDetailDTO.setVoterIdPath(voterIdPath.toString());
			}

			empDetailDTO.setWeddingDate(empDetail.getWeddingDate());
			empDetailDTO.setMobile(empDetail.getMobile());
			empDetailDTO.setOrganizationId(
					OptionalUtil.resolve(() -> empDetail.getOrganization().getOrganizationId()).orElse(null));
			
			empDetailDTO
					.setDistrictId(OptionalUtil.resolve(() -> empDetail.getDistrict().getDistrictId()).orElse(null));
			/*empDetailDTO.setEmpStatusId(
					OptionalUtil.resolve(() -> empDetail.getEmployeeStatus().getGeneralDetailId()).orElse(null));
			*/
			if(empDetail.getEmployeeStatus() != null) {
				empDetailDTO.setEmpStatusId(empDetail.getEmployeeStatus().getGeneralDetailId());
				empDetailDTO.setEmpStatusCode(empDetail.getEmployeeStatus().getGeneralDetailCode());
				empDetailDTO.setEmpStatusDisplayName(empDetail.getEmployeeStatus().getGeneralDetailDisplayName());
				}
			/*empDetailDTO.setEmpStateId(
					OptionalUtil.resolve(() -> empDetail.getEmployeeState().getGeneralDetailId()).orElse(null));
					
*/		
			if(empDetail.getEmployeeState() != null) {
				empDetailDTO.setEmpStateId(empDetail.getEmployeeState().getGeneralDetailId());
				empDetailDTO.setEmpStateCode(empDetail.getEmployeeState().getGeneralDetailCode());
				empDetailDTO.setEmpDisplayName(empDetail.getEmployeeState().getGeneralDetailDisplayName());
				}
			empDetailDTO.setEmpTypeId(
					OptionalUtil.resolve(() -> empDetail.getEmployeeType().getGeneralDetailId()).orElse(null));

			
			empDetailDTO.setEmpWorkingDeptId(OptionalUtil
					.resolve(() -> empDetail.getEmployeeWorkingDepartment().getDepartmentId()).orElse(null));

			empDetailDTO.setQualificationId(
					OptionalUtil.resolve(() -> empDetail.getQualification().getQualificationId()).orElse(null));
			
			empDetailDTO.setWorkingDesignationId(
					OptionalUtil.resolve(() -> empDetail.getWorkingDesignation().getDesignationId()).orElse(null));

			//empDetailDTO.setSchoolId(OptionalUtil.resolve(() -> empDetail.getSchool().getSchoolId()).orElse(null));
			
			if (empDetail.getSchool() != null) {
				empDetailDTO.setSchoolId(empDetail.getSchool().getSchoolId());
				empDetailDTO.setSchoolName(empDetail.getSchool().getSchoolName());
				empDetailDTO.setSchoolCode(empDetail.getSchool().getSchoolCode());
			}
			empDetailDTO.setEmpWrkCategoryId(
					OptionalUtil.resolve(() -> empDetail.getEmployeeWorkCategory().getGeneralDetailId()).orElse(null));
			empDetailDTO.setTeachingforId(
					OptionalUtil.resolve(() -> empDetail.getTeachingFor().getGeneralDetailId()).orElse(null));
			empDetailDTO.setAppointmentId(
					OptionalUtil.resolve(() -> empDetail.getAppointment().getGeneralDetailId()).orElse(null));

			/*
			 * empDetailDTO .setCityPresentId(OptionalUtil.resolve(() ->
			 * empDetail.getPresentCity().getCityId()).orElse(null));
			 * empDetailDTO.setDistrictPresentId( OptionalUtil.resolve(() ->
			 * empDetail.getPresentDistrict().getDistrictId()).orElse(null));
			 */
			if (empDetail.getPresentCity() != null) {
				empDetailDTO.setCityPresentId(empDetail.getPresentCity().getCityId());
				if (empDetail.getPresentDistrict() != null) {
					empDetailDTO.setDistrictPresentId(empDetail.getPresentDistrict().getDistrictId());
					if (empDetail.getPresentDistrict().getState() != null) {
						empDetailDTO.setPresentStateId(empDetail.getPresentDistrict().getState().getStateId());
						if (empDetail.getPresentDistrict().getState().getCountry() != null) {
							empDetailDTO.setPresentCountryId(
									empDetail.getPresentDistrict().getState().getCountry().getCountryId());

						}
					}

				}

			}

			if (empDetail.getPermanentCity() != null) {
				empDetailDTO.setCityPermanentId(empDetail.getPermanentCity().getCityId());
				if (empDetail.getPermanentDistrict() != null) {
					empDetailDTO.setDistrictPermanentId(empDetail.getPermanentDistrict().getDistrictId());
					if (empDetail.getPermanentDistrict().getState() != null) {
						empDetailDTO.setPermanentStateId(empDetail.getPermanentDistrict().getState().getStateId());
						if (empDetail.getPermanentDistrict().getState().getCountry() != null) {
							empDetailDTO.setPermanentCountryId(
									empDetail.getPermanentDistrict().getState().getCountry().getCountryId());

						}
					}

				}
			}

			/*
			 * empDetailDTO.setCityPermanentId( OptionalUtil.resolve(() ->
			 * empDetail.getPermanentCity().getCityId()).orElse(null));
			 * empDetailDTO.setDistrictPermanentId( OptionalUtil.resolve(() ->
			 * empDetail.getPermanentDistrict().getDistrictId()).orElse(null));
			 */

			empDetailDTO.setBloodgroupId(
					OptionalUtil.resolve(() -> empDetail.getBloodGroup().getGeneralDetailId()).orElse(null));
			empDetailDTO
					.setPaymodeId(OptionalUtil.resolve(() -> empDetail.getPaymode().getGeneralDetailId()).orElse(null));
			empDetailDTO.setUserId(OptionalUtil.resolve(() -> empDetail.getUser().getUserId()).orElse(null));
			empDetailDTO.setResidentId(
					OptionalUtil.resolve(() -> empDetail.getResident().getGeneralDetailId()).orElse(null));
			empDetailDTO.setAccommodationId(
					OptionalUtil.resolve(() -> empDetail.getAccommodation().getGeneralDetailId()).orElse(null));
			empDetailDTO
					.setTitleId(OptionalUtil.resolve(() -> (empDetail.getTitle().getGeneralDetailId())).orElse(null));
			empDetailDTO
					.setGenderId(OptionalUtil.resolve(() -> empDetail.getGender().getGeneralDetailId()).orElse(null));
			if (empDetail.getGender() != null) {
				empDetailDTO.setGender(empDetail.getGender().getGeneralDetailDisplayName());
			}
			empDetailDTO.setReligionId(
					OptionalUtil.resolve(() -> empDetail.getReligion().getGeneralDetailId()).orElse(null));
			
			empDetailDTO.setCasteId(OptionalUtil.resolve(() -> empDetail.getCaste().getCasteId()).orElse(null));
			empDetailDTO
					.setSubCasteId(OptionalUtil.resolve(() -> empDetail.getSubCaste().getSubCasteId()).orElse(null));

			/*empDetailDTO.setEmpgrade(
					OptionalUtil.resolve(() -> empDetail.getEmployeeGrade().getGeneralDetailId()).orElse(null));*/
			if (empDetail.getEmpEmployeeBankDetails() != null) {
				empDetailDTO.setEmployeeBankDetails(
						employeeBankDetailMapper.convertEntityListToDTOList(empDetail.getEmpEmployeeBankDetails()));
			}
			if (empDetail.getEmpDocumentCollections1() != null) {
				empDetailDTO.setEmployeeDocumentCollection(employeeDocumentCollectionMapper
						.convertEntityListToDTOList(empDetail.getEmpDocumentCollections1()));
			}
			if (empDetail.getEmpDocumentCollections2() != null) {
				empDetailDTO.setVerifiedEmployeeDocumentCollection(employeeDocumentCollectionMapper
						.convertEntityListToDTOList(empDetail.getEmpDocumentCollections2()));
			}
			if (empDetail.getEmpEmployeeReportings1() != null) {
				empDetailDTO.setEmployeeReportings(
						employeeReportingMapper.convertEntityListToDTOList(empDetail.getEmpEmployeeReportings1()));
			}
			if (empDetail.getEmpEmployeeReportings2() != null) {
				empDetailDTO.setEmployeeReportingMangers(
						employeeReportingMapper.convertEntityListToDTOList(empDetail.getEmpEmployeeReportings2()));
			}
			if (empDetail.getEmpExperienceDetails() != null) {
				empDetailDTO.setEmpExperienceDetails(
						employeeExperienceDetailMapper.convertEntityListToDTOList(empDetail.getEmpExperienceDetails()));
			}
			if (empDetail.getEmpEmployeeEducations() != null) {
				empDetailDTO.setEmployeeEducations(
						employeeEducationMapper.convertEntityListToDTOList(empDetail.getEmpEmployeeEducations()));
			}
			if(empDetail.getEmployeeGrade()!=null) {
				empDetailDTO.setEmpgrade(empDetail.getEmployeeGrade().getGeneralDetailId());
				empDetailDTO.setEmpGradeName(empDetail.getEmployeeGrade().getGeneralDetailDisplayName());
				empDetailDTO.setEmpGradeCode(empDetail.getEmployeeGrade().getGeneralDetailCode());
			}
			
			if(empDetail.getDesignation()!=null) {
				empDetailDTO.setDesignationId(empDetail.getDesignation().getDesignationId());
				empDetailDTO.setDesignationName(empDetail.getDesignation().getDesignationName());
			}
			
			if(empDetail.getEmployeeDepartment()!=null) {
				empDetailDTO.setEmpDeptId(empDetail.getEmployeeDepartment().getDepartmentId());
				empDetailDTO.setDeptName(empDetail.getEmployeeDepartment().getDeptName());
			}
			if(empDetail.getEmployeeCategory()!=null) {
				empDetailDTO.setEmpCategoryId(empDetail.getEmployeeCategory().getGeneralDetailId());
				empDetailDTO.setEmpCategoryName(empDetail.getEmployeeCategory().getGeneralDetailDisplayName());
			}

			if(empDetail.getNationality()!=null) {
				empDetailDTO.setNationalityId(empDetail.getNationality().getGeneralDetailId());
				empDetailDTO.setNationality(empDetail.getNationality().getGeneralDetailDisplayName());
			}
			
			if(empDetail.getMaritalStatus()!=null) {
				empDetailDTO.setMaritalStatusId(empDetail.getMaritalStatus().getGeneralDetailId());
				empDetailDTO.setMaritalStatus(empDetail.getMaritalStatus().getGeneralDetailDisplayName());
			}
		}
		return empDetailDTO;
	}

	public EmployeeDetail convertDTOtoEntity(EmployeeDetailDTO empDetailDTO, EmployeeDetail empDetail) {
		if (empDetailDTO != null) {
			if (null == empDetail) {
				empDetail = new EmployeeDetail();
				empDetail.setIsActive(Boolean.TRUE);
				empDetail.setCreatedDt(new Date());
				empDetail.setCreatedUser(SecurityUtil.getCurrentUser());
			} else {
				empDetail.setIsActive(empDetailDTO.getIsActive());
			}

			empDetail.setEmployeeId(empDetailDTO.getEmployeeId());
			empDetail.setAadharNo(empDetailDTO.getAadharNo());
			empDetail.setAadharPath(FileUtil.getRelativePath(empDetailDTO.getAadharPath()));
			empDetail.setAddress(empDetailDTO.getAddress());
			empDetail.setAicteRegNo(empDetailDTO.getAicteRegNo());
			empDetail.setBiometricCode(empDetailDTO.getBiometricCode());

			empDetail.setUpdatedDt(new Date());
			empDetail.setUpdatedUser(SecurityUtil.getCurrentUser());

			empDetail.setDateOfBirth(empDetailDTO.getDateOfBirth());
			empDetail.setDateOfRelieving(empDetailDTO.getDateOfRelieving());
			empDetail.setEmail(empDetailDTO.getEmail());
			empDetail.setEmergencyMobile(empDetailDTO.getEmergencyMobile());
			empDetail.setEmpNumber(empDetailDTO.getEmpNumber());
			empDetail.setEpfNo(empDetailDTO.getEpfNo());
			empDetail.setEsiRegNo(empDetailDTO.getEsiRegNo());
			empDetail.setFacebookUrl(empDetailDTO.getFacebookUrl());
			empDetail.setFatherName(empDetailDTO.getFatherName());
			empDetail.setFirstName(empDetailDTO.getFirstName());
			empDetail.setBiometricId(empDetailDTO.getBiometricId());
			empDetail.setCurrentPayId(empDetailDTO.getCurrentPayId());
			empDetail.setPayrollPayId(empDetailDTO.getPayrollPayId());
			empDetail.setPayscaleId(empDetailDTO.getPayscaleId());
			/*empDetail.setReportingManagerId(empDetailDTO.getReportingManagerId());*/
			empDetail.setGoogleplusUrl(empDetailDTO.getGoogleplusUrl());
			empDetail.setIsManager(empDetailDTO.getIsManager());
			empDetail.setIsPtax(empDetailDTO.getIsPtax());
			empDetail.setIsRatified(empDetailDTO.getIsRatified());
			empDetail.setIsTds(empDetailDTO.getIsTds());
			empDetail.setIsUsingCampAccommodation(empDetailDTO.getIsUsingCampAccommodation());
			empDetail.setIsUsingTransport(empDetailDTO.getIsUsingTransport());
			empDetail.setJntuDateOfJoining(empDetailDTO.getJntuDateOfJoining());
			empDetail.setJntuRegNo(empDetailDTO.getJntuRegNo());
			empDetail.setJoiningDate(empDetailDTO.getJoiningDate());
			empDetail.setLastName(empDetailDTO.getLastName());
			empDetail.setLicNo(empDetailDTO.getLicNo());
			empDetail.setLinkedinUrl(empDetailDTO.getLinkedinUrl());
			empDetail.setMiddleName(empDetailDTO.getMiddleName());
			empDetail.setMonthlySalary(empDetailDTO.getMonthlySalary());
			empDetail.setMotherName(empDetailDTO.getMotherName());
			empDetail.setNonTeachingExp(empDetailDTO.getNonTeachingExp());
			empDetail.setOfficialMobile(empDetailDTO.getOfficialMobile());
			empDetail.setPancard(empDetailDTO.getPancard());
			empDetail.setPancardPath(FileUtil.getRelativePath(empDetailDTO.getPancardPath()));
			empDetail.setPassportNo(empDetailDTO.getPassportNo());
			empDetail.setPassportPath(FileUtil.getRelativePath(empDetailDTO.getPassportPath()));
			empDetail.setPermanentAddress(empDetailDTO.getPermanentAddress());
			empDetail.setPermanentMandal(empDetailDTO.getPermanentMandal());
			empDetail.setPermanentPincode(empDetailDTO.getPermanentPincode());
			empDetail.setPermanentStreet(empDetailDTO.getPermanentStreet());
			empDetail.setPhotoPath(FileUtil.getRelativePath(empDetailDTO.getPhotoPath()));
			empDetail.setPresentAddress(empDetailDTO.getPresentAddress());
			empDetail.setPresentMandal(empDetailDTO.getPresentMandal());
			empDetail.setPresentPincode(empDetailDTO.getPresentPincode());
			empDetail.setPresentStreet(empDetailDTO.getPresentStreet());
			empDetail.setPromotedDate(empDetailDTO.getPromotedDate());
			empDetail.setResearchExp(empDetailDTO.getResearchExp());
			empDetail.setResidencePhone(empDetailDTO.getResidencePhone());
			empDetail.setResignationDate(empDetailDTO.getResignationDate());
			empDetail.setServiceBreakYrs(empDetailDTO.getServiceBreakYrs());
			empDetail.setStatusDescription(empDetailDTO.getStatusDescription());
			empDetail.setTeachingExp(empDetailDTO.getTeachingExp());
			empDetail.setTenureDays(empDetailDTO.getTenureDays());
			empDetail.setVoterId(empDetailDTO.getVoterId());
			empDetail.setVoterIdPath(FileUtil.getRelativePath(empDetailDTO.getVoterIdPath()));
			empDetail.setWeddingDate(empDetailDTO.getWeddingDate());
			empDetail.setMobile(empDetailDTO.getMobile());
			empDetail.setReason(empDetailDTO.getReason());
			if (empDetailDTO.getOrganizationId() != null) {
				Organization organization = new Organization();
				organization.setOrganizationId(empDetailDTO.getOrganizationId());
				empDetail.setOrganization(organization);
			}
			
			if (empDetailDTO.getReportingManagerId() != null) {
				EmployeeDetail reportManager = new EmployeeDetail();
				reportManager.setEmployeeId(empDetailDTO.getEmployeeId());
				empDetail.setReportingManager(reportManager);
			}

			if (empDetailDTO.getMaritalStatusId() != null) {
				GeneralDetail maritalStatus = new GeneralDetail();
				maritalStatus.setGeneralDetailId(empDetailDTO.getMaritalStatusId());
				empDetail.setMaritalStatus(maritalStatus);
			}

			if (empDetailDTO.getDistrictId() != null) {
				District district = new District();
				district.setDistrictId(empDetailDTO.getDistrictId());
				empDetail.setDistrict(district);
			}

			if (empDetailDTO.getEmpStatusId() != null) {
				GeneralDetail empStatus = new GeneralDetail();
				empStatus.setGeneralDetailId(empDetailDTO.getEmpStatusId());
				empDetail.setEmployeeStatus(empStatus);
			}

			if (empDetailDTO.getEmpStateId() != null) {
				GeneralDetail empState = new GeneralDetail();
				empState.setGeneralDetailId(empDetailDTO.getEmpStateId());
				empDetail.setEmployeeState(empState);
			}

			if (empDetailDTO.getEmpTypeId() != null) {
				GeneralDetail empType = new GeneralDetail();
				empType.setGeneralDetailId(empDetailDTO.getEmpTypeId());
				empDetail.setEmployeeType(empType);
			}

			if (empDetailDTO.getEmpDeptId() != null) {
				Department empDept = new Department();
				empDept.setDepartmentId(empDetailDTO.getEmpDeptId());
				empDetail.setEmployeeDepartment(empDept);
			}

			if (empDetailDTO.getEmpWorkingDeptId() != null) {
				Department empWorkingDept = new Department();
				empWorkingDept.setDepartmentId(empDetailDTO.getEmpWorkingDeptId());
				empDetail.setEmployeeWorkingDepartment(empWorkingDept);
			}

			if (empDetailDTO.getQualificationId() != null) {
				Qualification qualification = new Qualification();
				qualification.setQualificationId(empDetailDTO.getQualificationId());
				empDetail.setQualification(qualification);
			}

			if (empDetailDTO.getDesignationId() != null) {
				Designation designation = new Designation();
				designation.setDesignationId(empDetailDTO.getDesignationId());
				empDetail.setDesignation(designation);
			}

			if (empDetailDTO.getWorkingDesignationId() != null) {
				Designation workingDesignation = new Designation();
				workingDesignation.setDesignationId(empDetailDTO.getWorkingDesignationId());
				empDetail.setWorkingDesignation(workingDesignation);
			}

			if (empDetailDTO.getSchoolId() != null) {
				School school = new School();
				school.setSchoolId(empDetailDTO.getSchoolId());
				empDetail.setSchool(school);
			}

			if (empDetailDTO.getEmpCategoryId() != null) {
				GeneralDetail empCategory = new GeneralDetail();
				empCategory.setGeneralDetailId(empDetailDTO.getEmpCategoryId());
				empDetail.setEmployeeCategory(empCategory);
			}

			if (empDetailDTO.getEmpWrkCategoryId() != null) {
				GeneralDetail empWrkCategory = new GeneralDetail();
				empWrkCategory.setGeneralDetailId(empDetailDTO.getEmpWrkCategoryId());
				empDetail.setEmployeeWorkCategory(empWrkCategory);
			}

			if (empDetailDTO.getTeachingforId() != null) {
				GeneralDetail teachingFor = new GeneralDetail();
				teachingFor.setGeneralDetailId(empDetailDTO.getTeachingforId());
				empDetail.setTeachingFor(teachingFor);
			}

			if (empDetailDTO.getAppointmentId() != null) {
				GeneralDetail appointment = new GeneralDetail();
				appointment.setGeneralDetailId(empDetailDTO.getAppointmentId());
				empDetail.setAppointment(appointment);
			}

			if (empDetailDTO.getCityPresentId() != null) {
				City cityPresent = new City();
				cityPresent.setCityId(empDetailDTO.getCityPresentId());
				empDetail.setPresentCity(cityPresent);
			}

			if (empDetailDTO.getDistrictPresentId() != null) {
				District districtPresent = new District();
				districtPresent.setDistrictId(empDetailDTO.getDistrictPresentId());
				empDetail.setPresentDistrict(districtPresent);
			}

			if (empDetailDTO.getCityPermanentId() != null) {
				City cityPermanent = new City();
				cityPermanent.setCityId(empDetailDTO.getCityPermanentId());
				empDetail.setPermanentCity(cityPermanent);
			}

			if (empDetailDTO.getDistrictPermanentId() != null) {
				District districtPermanent = new District();
				districtPermanent.setDistrictId(empDetailDTO.getDistrictPermanentId());
				empDetail.setPermanentDistrict(districtPermanent);
			}

			if (empDetailDTO.getBloodgroupId() != null) {
				GeneralDetail bloodgroup = new GeneralDetail();
				bloodgroup.setGeneralDetailId(empDetailDTO.getBloodgroupId());
				empDetail.setBloodGroup(bloodgroup);
			}

			if (empDetailDTO.getPaymodeId() != null) {
				GeneralDetail paymode = new GeneralDetail();
				paymode.setGeneralDetailId(empDetailDTO.getPaymodeId());
				empDetail.setPaymode(paymode);
			}

			/*Commented by Naveen - 10-12-2018
			 * Employee user Id is different from created user id
			 * 
			 * if (SecurityUtil.getCurrentUser() != null) {
				User user = new User();
				user.setUserId(SecurityUtil.getCurrentUser());
				empDetail.setUser(user);
			}*/
			if (empDetailDTO.getUserId() != null) {
				User user = new User();
				user.setUserId(empDetailDTO.getUserId());
				empDetail.setUser(user);
			}

			if (empDetailDTO.getResidentId() != null) {
				GeneralDetail resident = new GeneralDetail();
				resident.setGeneralDetailId(empDetailDTO.getResidentId());
				empDetail.setResident(resident);
			}

			if (empDetailDTO.getAccommodationId() != null) {
				GeneralDetail accommodation = new GeneralDetail();
				accommodation.setGeneralDetailId(empDetailDTO.getAccommodationId());
				empDetail.setAccommodation(accommodation);
			}

			if (empDetailDTO.getTitleId() != null) {
				GeneralDetail title = new GeneralDetail();
				title.setGeneralDetailId(empDetailDTO.getTitleId());
				empDetail.setTitle(title);
			}

			if (empDetailDTO.getGenderId() != null) {
				GeneralDetail gender = new GeneralDetail();
				gender.setGeneralDetailId(empDetailDTO.getGenderId());
				empDetail.setGender(gender);
			}

			if (empDetailDTO.getReligionId() != null) {
				GeneralDetail religion = new GeneralDetail();
				religion.setGeneralDetailId(empDetailDTO.getReligionId());
				empDetail.setReligion(religion);
			}

			if (empDetailDTO.getNationalityId() != null) {
				GeneralDetail nationality = new GeneralDetail();
				nationality.setGeneralDetailId(empDetailDTO.getNationalityId());
				empDetail.setNationality(nationality);
			}

			if (empDetailDTO.getCasteId() != null) {
				Caste caste = new Caste();
				caste.setCasteId(empDetailDTO.getCasteId());
				empDetail.setCaste(caste);
			}

			if (empDetailDTO.getSubCasteId() != null) {
				SubCaste subCaste = new SubCaste();
				subCaste.setSubCasteId(empDetailDTO.getSubCasteId());
				empDetail.setSubCaste(subCaste);
			}

			if (empDetailDTO.getEmpgrade() != null) {
				GeneralDetail empGrade = new GeneralDetail();
				empGrade.setGeneralDetailId(empDetailDTO.getEmpgrade());
				empDetail.setEmployeeGrade(empGrade);
			}

			setEmployeeBankDetails(empDetailDTO, empDetail);
			setEmployeeDocumentCollection(empDetailDTO, empDetail);
			// setVerifiedEmployeeDocumentCollection(empDetailDTO, empDetail);
			setEmployeeReportings(empDetailDTO, empDetail);
			// setEmpEmployeeReportingMangers(empDetailDTO, empDetail);
			setEmployeeExperienceDetail(empDetailDTO, empDetail);
			setEmployeeEducations(empDetailDTO, empDetail);
		}
		return empDetail;
	}

	public List<EmployeeDetail> convertDTOListToEntityList(List<EmployeeDetailDTO> empDetailDTOList) {
		List<EmployeeDetail> empDetailList = new ArrayList<>();
		if (empDetailDTOList != null) {
			empDetailDTOList.forEach(empDetailDTO -> empDetailList.add(convertDTOtoEntity(empDetailDTO, null)));
		}
		return empDetailList;

	}

	public List<EmployeeDetailDTO> convertEntityListToDTOList(List<EmployeeDetail> empDetailList) {
		List<EmployeeDetailDTO> empDetailDTOList = new ArrayList<>();
		empDetailList.forEach(empDetail -> empDetailDTOList.add(convertEntityToDTO(empDetail)));
		return empDetailDTOList;
	}

	private void setEmployeeDocumentCollection(EmployeeDetailDTO employeeDetailDTO, EmployeeDetail employeeDetail) {
		if (!CollectionUtils.isEmpty(employeeDetailDTO.getEmployeeDocumentCollection())) {
			List<EmployeeDocumentCollection> employeeDocumentCollectionList = new ArrayList<>();
			for (EmployeeDocumentCollectionDTO empDocumentCollectionDTO : employeeDetailDTO
					.getEmployeeDocumentCollection()) {
				EmployeeDocumentCollection empDocumentCollection = new EmployeeDocumentCollection();
				if (empDocumentCollectionDTO.getEmployeeDocCollId() == null) {
					empDocumentCollection = new EmployeeDocumentCollection();
					empDocumentCollection.setIsActive(Boolean.TRUE);
					empDocumentCollection.setCreatedDt(new Date());
					empDocumentCollection.setCreatedUser(SecurityUtil.getCurrentUser());
				} else {
					empDocumentCollection.setCreatedDt(empDocumentCollectionDTO.getCreatedDt());
					empDocumentCollection.setCreatedUser(empDocumentCollectionDTO.getCreatedUser());
					empDocumentCollection.setIsActive(empDocumentCollectionDTO.getIsActive());
					empDocumentCollection.setUpdatedDt(new Date());
					empDocumentCollection.setUpdatedUser(SecurityUtil.getCurrentUser());
				}

				empDocumentCollection.setEmployeeDocCollId(empDocumentCollectionDTO.getEmployeeDocCollId());

				empDocumentCollection.setFileName(empDocumentCollectionDTO.getFileName());
				empDocumentCollection.setFilePath(FileUtil.getRelativePath(empDocumentCollectionDTO.getFilePath()));
				empDocumentCollection.setIsHardCopy(empDocumentCollectionDTO.getIsHardCopy());
				empDocumentCollection.setIsOriginal(empDocumentCollectionDTO.getIsOriginal());
				empDocumentCollection.setIsSoftCopy(empDocumentCollectionDTO.getIsSoftCopy());
				empDocumentCollection.setIsVerified(empDocumentCollectionDTO.getIsVerified());
				empDocumentCollection.setRackNumber(empDocumentCollectionDTO.getRackNumber());
				empDocumentCollection.setReason(empDocumentCollectionDTO.getReason());

				empDocumentCollection.setEmployeeDetail(employeeDetail);

				if (empDocumentCollectionDTO.getDocumentRepositoryId() != null) {
					DocumentRepository documentRepository = new DocumentRepository();
					documentRepository.setDocumentRepositoryId(empDocumentCollectionDTO.getDocumentRepositoryId());
					empDocumentCollection.setDocumentRepository(documentRepository);
				}

				if (empDocumentCollectionDTO.getDoctypeCatdetId() != null) {
					GeneralDetail doctype = new GeneralDetail();
					doctype.setGeneralDetailId(empDocumentCollectionDTO.getDoctypeCatdetId());
					empDocumentCollection.setDoctype(doctype);
				}

				if (empDocumentCollectionDTO.getIsVerified() != null) {
					if (empDocumentCollectionDTO.getVerifiedbyEmpId() != null) {
						EmployeeDetail verifiedByEmployeeDetail = new EmployeeDetail();
						verifiedByEmployeeDetail.setEmployeeId(empDocumentCollectionDTO.getVerifiedbyEmpId());
						empDocumentCollection.setVerifiedByEmployeeDetail(verifiedByEmployeeDetail);
					} else {
						empDocumentCollection.setVerifiedByEmployeeDetail(null);
					}
				} else {
					empDocumentCollection.setVerifiedByEmployeeDetail(null);
				}

				employeeDocumentCollectionList.add(empDocumentCollection);

			}
			if (!CollectionUtils.isEmpty(employeeDocumentCollectionList)) {
				employeeDetail.setEmpDocumentCollections1(employeeDocumentCollectionList);
			}
		}
	}

	private void setVerifiedEmployeeDocumentCollection(EmployeeDetailDTO employeeDetailDTO,
			EmployeeDetail employeeDetail) {
		if (!CollectionUtils.isEmpty(employeeDetailDTO.getVerifiedEmployeeDocumentCollection())) {
			List<EmployeeDocumentCollection> employeeDocumentCollectionList = new ArrayList<>();
			for (EmployeeDocumentCollectionDTO empDocumentCollectionDTO : employeeDetailDTO
					.getVerifiedEmployeeDocumentCollection()) {
				EmployeeDocumentCollection empDocumentCollection = new EmployeeDocumentCollection();
				if (empDocumentCollectionDTO.getEmployeeDocCollId() == null) {
					empDocumentCollection = new EmployeeDocumentCollection();
					empDocumentCollection.setIsActive(Boolean.TRUE);
					empDocumentCollection.setCreatedDt(new Date());
					empDocumentCollection.setCreatedUser(SecurityUtil.getCurrentUser());
				} else {
					empDocumentCollection.setCreatedDt(empDocumentCollectionDTO.getCreatedDt());
					empDocumentCollection.setCreatedUser(empDocumentCollectionDTO.getCreatedUser());
					empDocumentCollection.setIsActive(empDocumentCollectionDTO.getIsActive());
					empDocumentCollection.setUpdatedDt(new Date());
					empDocumentCollection.setUpdatedUser(SecurityUtil.getCurrentUser());
				}

				empDocumentCollection.setEmployeeDocCollId(empDocumentCollectionDTO.getEmployeeDocCollId());

				empDocumentCollection.setFileName(empDocumentCollectionDTO.getFileName());
				empDocumentCollection.setFilePath(FileUtil.getRelativePath(empDocumentCollectionDTO.getFilePath()));
				empDocumentCollection.setIsHardCopy(empDocumentCollectionDTO.getIsHardCopy());
				empDocumentCollection.setIsOriginal(empDocumentCollectionDTO.getIsOriginal());
				empDocumentCollection.setIsSoftCopy(empDocumentCollectionDTO.getIsSoftCopy());
				empDocumentCollection.setIsVerified(empDocumentCollectionDTO.getIsVerified());
				empDocumentCollection.setRackNumber(empDocumentCollectionDTO.getRackNumber());
				empDocumentCollection.setReason(empDocumentCollectionDTO.getReason());

				if (empDocumentCollectionDTO.getEmpId() != null) {
					EmployeeDetail employeeFormDetail = new EmployeeDetail();
					employeeFormDetail.setEmployeeId(empDocumentCollectionDTO.getEmpId());
					empDocumentCollection.setVerifiedByEmployeeDetail(employeeFormDetail);
				} else {
					empDocumentCollection.setEmployeeDetail(employeeDetail);
				}

				if (empDocumentCollectionDTO.getDocumentRepositoryId() != null) {
					DocumentRepository documentRepository = new DocumentRepository();
					documentRepository.setDocumentRepositoryId(empDocumentCollectionDTO.getDocumentRepositoryId());
					empDocumentCollection.setDocumentRepository(documentRepository);
				}

				if (empDocumentCollectionDTO.getDoctypeCatdetId() != null) {
					GeneralDetail doctype = new GeneralDetail();
					doctype.setGeneralDetailId(empDocumentCollectionDTO.getDoctypeCatdetId());
					empDocumentCollection.setDoctype(doctype);
				}

				if (empDocumentCollectionDTO.getVerifiedbyEmpId() != null) {
					EmployeeDetail verifiedByEmployeeDetail = new EmployeeDetail();
					verifiedByEmployeeDetail.setEmployeeId(empDocumentCollectionDTO.getVerifiedbyEmpId());
					empDocumentCollection.setVerifiedByEmployeeDetail(verifiedByEmployeeDetail);
				} else {
					empDocumentCollection.setVerifiedByEmployeeDetail(employeeDetail);
				}
				employeeDocumentCollectionList.add(empDocumentCollection);

			}
			if (!CollectionUtils.isEmpty(employeeDocumentCollectionList)) {
				employeeDetail.setEmpDocumentCollections2(employeeDocumentCollectionList);
			}
		}
	}

	private void setEmployeeBankDetails(EmployeeDetailDTO employeeDetailDTO, EmployeeDetail employeeDetail) {
		if (!CollectionUtils.isEmpty(employeeDetailDTO.getEmployeeBankDetails())) {
			List<EmployeeBankDetail> employeeBankDetailList = new ArrayList<>();
			for (EmployeeBankDetailDTO employeeBankDetailDTO : employeeDetailDTO.getEmployeeBankDetails()) {
				EmployeeBankDetail employeeBankDetail = new EmployeeBankDetail();
				if (employeeBankDetailDTO.getEmployeeBankDetailId() == null) {
					employeeBankDetail = new EmployeeBankDetail();
					employeeBankDetail.setIsActive(Boolean.TRUE);
					employeeBankDetail.setCreatedDt(new Date());
					employeeBankDetail.setCreatedUser(SecurityUtil.getCurrentUser());
				} else {
					employeeBankDetail.setCreatedDt(employeeBankDetailDTO.getCreatedDt());
					employeeBankDetail.setCreatedUser(employeeBankDetailDTO.getCreatedUser());
					employeeBankDetail.setIsActive(employeeBankDetailDTO.getIsActive());
					employeeBankDetail.setUpdatedDt(new Date());
					employeeBankDetail.setUpdatedUser(SecurityUtil.getCurrentUser());
				}

				employeeBankDetail.setEmployeeBankDetailId(employeeBankDetailDTO.getEmployeeBankDetailId());
				employeeBankDetail.setAccountNumber(employeeBankDetailDTO.getAccountNumber());
				employeeBankDetail.setBankAddress(employeeBankDetailDTO.getBankAddress());
				employeeBankDetail.setBankName(employeeBankDetailDTO.getBankName());
				employeeBankDetail.setBranchName(employeeBankDetailDTO.getBranchName());
				employeeBankDetail.setDdPayableAddress(employeeBankDetailDTO.getDdPayableAddress());
				employeeBankDetail.setIfscCode(employeeBankDetailDTO.getIfscCode());
				employeeBankDetail.setReason(employeeBankDetailDTO.getReason());
				employeeBankDetail.setPhone(employeeBankDetailDTO.getPhone());

				employeeBankDetail.setEmployeeDetail(employeeDetail);

				Organization organization = new Organization();
				organization.setOrganizationId(employeeDetailDTO.getOrganizationId());
				employeeBankDetail.setOrganization(organization);
				employeeBankDetailList.add(employeeBankDetail);
			}
			if (!CollectionUtils.isEmpty(employeeBankDetailList)) {
				employeeDetail.setEmpEmployeeBankDetails(employeeBankDetailList);
			}
		}
	}

	private void setEmployeeReportings(EmployeeDetailDTO employeeDetailDTO, EmployeeDetail employeeDetail) {
		if (!CollectionUtils.isEmpty(employeeDetailDTO.getEmployeeReportings())) {
			List<EmployeeReporting> employeeReportingList = new ArrayList<>();
			for (EmployeeReportingDTO employeeReportingDTO : employeeDetailDTO.getEmployeeReportings()) {
				EmployeeReporting employeeReporting = new EmployeeReporting();

				if (employeeReportingDTO.getEmpReportingId() == null) {
					employeeReporting.setCreatedDt(new Date());
					employeeReporting.setIsActive(Boolean.TRUE);
					employeeReporting.setCreatedUser(SecurityUtil.getCurrentUser());
				} else {
					employeeReporting.setCreatedDt(employeeReportingDTO.getCreatedDt());
					employeeReporting.setCreatedUser(employeeReportingDTO.getCreatedUser());
					employeeReporting.setUpdatedDt(new Date());
					employeeReporting.setIsActive(employeeReportingDTO.getIsActive());
					employeeReporting.setUpdatedUser(SecurityUtil.getCurrentUser());
				}

				employeeReporting.setEmployeeReportingId(employeeReportingDTO.getEmpId());
				employeeReporting.setFromDate(employeeReportingDTO.getFromDate());
				employeeReporting.setToDate(employeeReportingDTO.getToDate());
				employeeReporting.setReason(employeeReportingDTO.getReason());
				employeeReporting.setEmployeeDetail(employeeDetail);

				if (employeeReportingDTO.getManagerEmpId() != null) {
					EmployeeDetail managerEmployeeDetail = new EmployeeDetail();
					managerEmployeeDetail.setEmployeeId(employeeReportingDTO.getManagerEmpId());
					employeeReporting.setManagerEmployeeDetail(managerEmployeeDetail);
				} else {
					employeeReporting.setManagerEmployeeDetail(null);
				}

				if (employeeReportingDTO.getEmpDesignationId() != null) {
					Designation designation = new Designation();
					designation.setDesignationId(employeeReportingDTO.getEmpDesignationId());
					employeeReporting.setDesignation(employeeDetail.getDesignation());
				} else {
					employeeReporting.setDesignation(employeeDetail.getDesignation());
				}

				employeeReporting.setOrganization(employeeDetail.getOrganization());

				employeeReportingList.add(employeeReporting);
			}
			if (!CollectionUtils.isEmpty(employeeReportingList)) {
				employeeDetail.setEmpEmployeeReportings1(employeeReportingList);
			}
		}
	}

	private void setEmpEmployeeReportingMangers(EmployeeDetailDTO employeeDetailDTO, EmployeeDetail employeeDetail) {
		if (!CollectionUtils.isEmpty(employeeDetailDTO.getEmployeeReportingMangers())) {
			List<EmployeeReporting> employeeReportingList = new ArrayList<>();
			for (EmployeeReportingDTO employeeReportingDTO : employeeDetailDTO.getEmployeeReportingMangers()) {
				EmployeeReporting employeeReporting = new EmployeeReporting();

				if (employeeReportingDTO.getEmpReportingId() == null) {
					employeeReporting.setCreatedDt(new Date());
					employeeReporting.setCreatedUser(SecurityUtil.getCurrentUser());
				} else {
					employeeReporting.setCreatedDt(employeeReportingDTO.getCreatedDt());
					employeeReporting.setCreatedUser(employeeReportingDTO.getCreatedUser());
					employeeReporting.setUpdatedDt(new Date());
					employeeReporting.setUpdatedUser(SecurityUtil.getCurrentUser());
				}

				employeeReporting.setEmployeeReportingId(employeeReportingDTO.getEmpId());
				employeeReporting.setUpdatedDt(new Date());
				employeeReporting.setUpdatedUser(SecurityUtil.getCurrentUser());
				employeeReporting.setFromDate(employeeReportingDTO.getFromDate());
				employeeReporting.setToDate(employeeReportingDTO.getToDate());

				employeeReporting.setEmployeeDetail(employeeDetail);

				if (employeeReportingDTO.getManagerEmpId() != null) {
					EmployeeDetail managerEmployeeDetail = new EmployeeDetail();
					managerEmployeeDetail.setEmployeeId(employeeReportingDTO.getManagerEmpId());
					employeeReporting.setManagerEmployeeDetail(managerEmployeeDetail);
				} else {
					employeeReporting.setManagerEmployeeDetail(null);
				}

				if (employeeReportingDTO.getEmpDesignationId() != null) {
					Designation designation = new Designation();
					designation.setDesignationId(employeeReportingDTO.getEmpDesignationId());
					employeeReporting.setDesignation(employeeDetail.getDesignation());
				} else {
					employeeReporting.setDesignation(employeeDetail.getDesignation());
				}

				employeeReporting.setOrganization(employeeDetail.getOrganization());

				employeeReportingList.add(employeeReporting);
			}
			if (!CollectionUtils.isEmpty(employeeReportingList)) {
				employeeDetail.setEmpEmployeeReportings2(employeeReportingList);
			}
		}
	}

	private void setEmployeeExperienceDetail(EmployeeDetailDTO employeeDetailDTO, EmployeeDetail employeeDetail) {
		if (!CollectionUtils.isEmpty(employeeDetailDTO.getEmpExperienceDetails())) {
			List<EmployeeExperienceDetail> employeeExperienceList = new ArrayList<>();
			for (EmployeeExperienceDetailDTO employeeExperienceDetailDTO : employeeDetailDTO
					.getEmpExperienceDetails()) {
				EmployeeExperienceDetail employeeExperienceDetail = new EmployeeExperienceDetail();

				if (employeeExperienceDetailDTO.getEmpExperiencedetailId() == null) {
					employeeExperienceDetail.setIsActive(Status.INACTIVE.getId());
					employeeExperienceDetail.setCreatedDt(new Date());
					employeeExperienceDetail.setCreatedUser(SecurityUtil.getCurrentUser());
				} else {
					employeeExperienceDetail.setCreatedDt(employeeExperienceDetailDTO.getCreatedDt());
					employeeExperienceDetail.setCreatedUser(employeeExperienceDetailDTO.getCreatedUser());
					employeeExperienceDetail.setIsActive(employeeExperienceDetailDTO.getIsActive());
					employeeExperienceDetail.setUpdatedDt(new Date());
					employeeExperienceDetail.setUpdatedUser(SecurityUtil.getCurrentUser());
				}

				employeeExperienceDetail
						.setEmployeeExperiencedetailId(employeeExperienceDetailDTO.getEmpExperiencedetailId());
				if (employeeExperienceDetailDTO.getDesignation() != null) {
					employeeExperienceDetail.setDesignation(employeeExperienceDetailDTO.getDesignation().toString());
				}
				employeeExperienceDetail.setExperienceDetail(employeeExperienceDetailDTO.getExperienceDetail());
				employeeExperienceDetail.setExperienceMonth(employeeExperienceDetailDTO.getExperienceMonth());
				employeeExperienceDetail.setExperienceYear(employeeExperienceDetailDTO.getExperienceYear());
				employeeExperienceDetail.setFromYrMonth(employeeExperienceDetailDTO.getFromYrMonth());

				employeeExperienceDetail.setPrevoiusInstitutions(employeeExperienceDetailDTO.getPrevoiusInstitutions());
				employeeExperienceDetail.setReason(employeeExperienceDetailDTO.getReason());
				employeeExperienceDetail.setSubjects(employeeExperienceDetailDTO.getSubjects());
				employeeExperienceDetail.setToYrMonth(employeeExperienceDetailDTO.getToYrMonth());

				employeeExperienceDetail.setEmployeeDetail(employeeDetail);
				employeeExperienceDetail.setOrganization(employeeDetail.getOrganization());

				employeeExperienceList.add(employeeExperienceDetail);
			}
			if (!CollectionUtils.isEmpty(employeeExperienceList)) {
				employeeDetail.setEmpExperienceDetails(employeeExperienceList);
			}
		}
	}

	private void setEmployeeEducations(EmployeeDetailDTO employeeDetailDTO, EmployeeDetail employeeDetail) {
		if (!CollectionUtils.isEmpty(employeeDetailDTO.getEmployeeEducations())) {
			List<EmployeeEducation> employeeEducationList = new ArrayList<>();
			for (EmployeeEducationDTO employeeEducationDTO : employeeDetailDTO.getEmployeeEducations()) {
				EmployeeEducation employeeEducation = new EmployeeEducation();

				if (employeeEducationDTO.getEmpEducationId() == null) {
					employeeEducation.setIsActive(Boolean.TRUE);
					employeeEducation.setCreatedDt(new Date());
					employeeEducation.setCreatedUser(SecurityUtil.getCurrentUser());
				} else {
					employeeEducation.setCreatedDt(employeeEducationDTO.getCreatedDt());
					employeeEducation.setCreatedUser(employeeEducationDTO.getCreatedUser());
					employeeEducation.setIsActive(employeeEducationDTO.getIsActive());
					employeeEducation.setUpdatedDt(new Date());
					employeeEducation.setUpdatedUser(SecurityUtil.getCurrentUser());
				}

				employeeEducation.setEmpEducationId(employeeEducationDTO.getEmpEducationId());
				employeeEducation.setAddress(employeeEducationDTO.getAddress());
				employeeEducation.setBoard(employeeEducationDTO.getBoard());
				employeeEducation.setGradeClassSecured(employeeEducationDTO.getGradeClassSecured());

				employeeEducation.setMajorSubjects(employeeEducationDTO.getMajorSubjects());
				employeeEducation.setMedium(employeeEducationDTO.getMedium());
				employeeEducation.setNameOfInstitution(employeeEducationDTO.getNameOfInstitution());
				employeeEducation.setPrecentage(employeeEducationDTO.getPrecentage());
				employeeEducation.setReason(employeeEducationDTO.getReason());
				employeeEducation.setYearOfCompletion(employeeEducationDTO.getYearOfCompletion());

				employeeEducation.setEmpDetail(employeeDetail);

				if (employeeEducationDTO.getModeofstudy() != null) {
					GeneralDetail modeofstudy = new GeneralDetail();
					modeofstudy.setGeneralDetailId(employeeEducationDTO.getModeofstudy());
					employeeEducation.setModeofstudy(modeofstudy);
				}

				employeeEducationList.add(employeeEducation);
			}
			if (!CollectionUtils.isEmpty(employeeEducationList)) {
				employeeDetail.setEmpEmployeeEducations(employeeEducationList);
			}
		}
	}

}
