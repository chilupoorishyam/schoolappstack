package com.gts.cms.common.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
public class StudentProfileDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long studentProfileId;
    private String studentName;
    private Long studentId;
    private Long eventMasterCatdetId;
    private String eventMasterCatdetCode;
    private String eventMasterCatdetName;
    private Long eventTitleCatdetId;
    private String eventTitleCatdetCode;
    private String eventTitleCatdetName;
    private Long courseYearId;
    private String courseYearName;
    private String courseYearCode;
    private Boolean certificate;
    private Integer videosPhotos;
    private Date createdDt;
    private Long createdUser;
    private Boolean isActive;
    private String reason;
    private Date updatedDt;
    private Long updatedUser;
}