package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

public class StudentAcademicbatchDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long studentAcademicbatchId;

	private Date fromDate;

	private Date toDate;

	private Boolean isPromoted;

	private Boolean isActive;

	private String reason;

	private Date createdDt;

	private Long createdUser;

	private Date updatedDt;

	private Long updatedUser;

	private Long schoolId;
	private String schoolName;
	private String schoolCode;

	private Long studentId;
	private String firstName;
	private String middleName;
	private String lastName;
	private String rollNo;

	private Long courseId;
	private String courseName;
	private String courseCode;

	private Long academicYearId;
	private String academicYear;

	private Long fromCourseYearId;
	private String fromCourseYearName;
	private String fromCourseYearCode;

	private Long toCourseYearId;
	private String toCourseYearName;
	private String toCourseYearCode;

	private Long fromBatchId;
	private String fromBatchName;

	private Long toBatchId;
	private String toBatchName;

	private Long studentStatusId;
	private String studentStatusCode;
	private String studentStatusName;

	private Long fromGroupSectionId;
	private String fromGroupSectionName;

	private Long toGroupSectionId;
	private String toGroupSectionName;

	public Long getStudentAcademicbatchId() {
		return studentAcademicbatchId;
	}

	public void setStudentAcademicbatchId(Long studentAcademicbatchId) {
		this.studentAcademicbatchId = studentAcademicbatchId;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Boolean getIsPromoted() {
		return isPromoted;
	}

	public void setIsPromoted(Boolean isPromoted) {
		this.isPromoted = isPromoted;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public Long getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}

	public String getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}

	public Long getFromCourseYearId() {
		return fromCourseYearId;
	}

	public void setFromCourseYearId(Long fromCourseYearId) {
		this.fromCourseYearId = fromCourseYearId;
	}

	public String getFromCourseYearName() {
		return fromCourseYearName;
	}

	public void setFromCourseYearName(String fromCourseYearName) {
		this.fromCourseYearName = fromCourseYearName;
	}

	public String getFromCourseYearCode() {
		return fromCourseYearCode;
	}

	public void setFromCourseYearCode(String fromCourseYearCode) {
		this.fromCourseYearCode = fromCourseYearCode;
	}

	public Long getToCourseYearId() {
		return toCourseYearId;
	}

	public void setToCourseYearId(Long toCourseYearId) {
		this.toCourseYearId = toCourseYearId;
	}

	public String getToCourseYearName() {
		return toCourseYearName;
	}

	public void setToCourseYearName(String toCourseYearName) {
		this.toCourseYearName = toCourseYearName;
	}

	public String getToCourseYearCode() {
		return toCourseYearCode;
	}

	public void setToCourseYearCode(String toCourseYearCode) {
		this.toCourseYearCode = toCourseYearCode;
	}

	public Long getFromBatchId() {
		return fromBatchId;
	}

	public void setFromBatchId(Long fromBatchId) {
		this.fromBatchId = fromBatchId;
	}

	public String getFromBatchName() {
		return fromBatchName;
	}

	public void setFromBatchName(String fromBatchName) {
		this.fromBatchName = fromBatchName;
	}

	public Long getToBatchId() {
		return toBatchId;
	}

	public void setToBatchId(Long toBatchId) {
		this.toBatchId = toBatchId;
	}

	public String getToBatchName() {
		return toBatchName;
	}

	public void setToBatchName(String toBatchName) {
		this.toBatchName = toBatchName;
	}

	public Long getStudentStatusId() {
		return studentStatusId;
	}

	public void setStudentStatusId(Long studentStatusId) {
		this.studentStatusId = studentStatusId;
	}

	public String getStudentStatusCode() {
		return studentStatusCode;
	}

	public void setStudentStatusCode(String studentStatusCode) {
		this.studentStatusCode = studentStatusCode;
	}

	public String getStudentStatusName() {
		return studentStatusName;
	}

	public void setStudentStatusName(String studentStatusName) {
		this.studentStatusName = studentStatusName;
	}

	public Long getFromGroupSectionId() {
		return fromGroupSectionId;
	}

	public void setFromGroupSectionId(Long fromGroupSectionId) {
		this.fromGroupSectionId = fromGroupSectionId;
	}

	public Long getToGroupSectionId() {
		return toGroupSectionId;
	}

	public void setToGroupSectionId(Long toGroupSectionId) {
		this.toGroupSectionId = toGroupSectionId;
	}

	public String getFromGroupSectionName() {
		return fromGroupSectionName;
	}

	public void setFromGroupSectionName(String fromGroupSectionName) {
		this.fromGroupSectionName = fromGroupSectionName;
	}

	public String getToGroupSectionName() {
		return toGroupSectionName;
	}

	public void setToGroupSectionName(String toGroupSectionName) {
		this.toGroupSectionName = toGroupSectionName;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public String getRollNo() {
		return rollNo;
	}

	public void setRollNo(String rollNo) {
		this.rollNo = rollNo;
	}


}
