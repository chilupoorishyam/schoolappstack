package com.gts.cms.common.repository;

import com.gts.cms.entity.SpclActivityAttendance;
import com.gts.cms.entity.SpclActivityAttendee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface SpclActivityAttendanceRepository extends  JpaRepository<SpclActivityAttendance, Long>{
    @Query("SELECT spcl FROM SpclActivityAttendee spcl"
            + " WHERE 1=1"
            + " AND(:groupSectionId is null or spcl.groupSection.groupSectionId=:groupSectionId)"
            + " AND spcl.isActive=true "
            + " ORDER BY spcl.specialActivity.fromDate desc ")
    List<SpclActivityAttendee> getSpclActivityAttendance(@Param("groupSectionId") Long groupSectionId);
}
