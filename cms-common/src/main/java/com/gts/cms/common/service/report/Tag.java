package com.gts.cms.common.service.report;

import java.util.HashMap;
import java.util.Set;

public class Tag {

	private String tagName; // for tag name ex : table, div
	private HashMap<String, String> attributes = null; // for attributes like id, style, class etc.
	private StringBuilder content = new StringBuilder(); // tag value.

	// parameterized constructor.
	public Tag(String tagName) {
		this.tagName = tagName; // setting tag value.
		this.attributes = new HashMap<String, String>(); // creating new HashMap Object for attributes whenever a new instance created.  
	}

	/*
	 * to add or change the value of the attributes.
	 * 
	 * @Tag#setAttribute() set value to attribute.
	 */

	public void setAttribute(String attributeName, String attributeValue) {
		/*
		 *  checking for the class and style attributes, because
		 *  an element can contain multiple class and style values. 
		 */
		if ("class".equals(attributeName) || "style".equals(attributeName)) {
			
			if (this.attributes.containsKey(attributeName)) 
				this.attributes.put(attributeName, this.attributes.get(attributeName)+" "+attributeValue);

			else 
				this.attributes.put(attributeName, attributeValue);
		}

		/*if (attributeName.equals("class") && this.attributes.containsKey("class")) 
			this.attributes.put("class", this.attributes.get("class")+" "+attributeValue);

		else if (attributeName.equals("style") && this.attributes.containsKey("style"))
			this.attributes.put("style", this.attributes.get("style")+" "+attributeValue);*/

		else 
			this.attributes.put(attributeName, attributeValue);
	}

	/*
	 * to add style values
	 */
	public void addStyle(String stylevalues) {
		this.setAttribute("style", stylevalues);
	}

	/*
	 * to get attribute value.
	 */

	public String getAttribute(String attributeName) {
		return this.attributes.get(attributeName);
	}

	/*
	 * to remove attribute.
	 */
	public void removeAttribute(String attributeName) {
		this.attributes.remove(attributeName);
	}

	/*
	 * to add a class to the instance.
	 */

	public void addClass(String className) {

		/*if (this.attributes.containsKey("class"))
			this.attributes.put("class", this.attributes.get("class")+" "+className);

		else
			this.attributes.put("class", className);
		 */

		this.setAttribute("class", className);

	}

	/*
	 * to remove Class from the instance.
	 */
	public void removeClass(String className) {
		StringBuilder classNames = this.attributes.get("class")==null ? new StringBuilder() : new StringBuilder(this.attributes.get("class"));

		int index = classNames.indexOf(className);

		if (index!=-1)
			classNames.delete(index, index+className.length());

		this.attributes.put("class", classNames.toString().trim());
	}

	/*
	 * to add multiple classes.
	 */

	public void addMultipleClasses(String ... classNames) {

		StringBuilder classes = new StringBuilder();

		for(String className : classNames) {
			classes.append(" "+className);
		}

		this.addClass(classes.toString().trim()); // calling addClass method.
	}

	/*public void setTagValue(String tagValue) {
		this.tagValue = tagValue;
	}*/

	/*
	 * to append to value to the tag.
	 */
	public void append(String str) {
		this.content.append(str);
	}

	/*
	 * overriding the super class method. to give the tag structure.  
	 * 
	 */

	public String toString() {
		StringBuilder tag = new StringBuilder(); 

		tag.append("<");
		tag.append(this.tagName);

		if (attributes!=null) {
			Set<String> keySet = attributes.keySet(); // getting all attributes using keySet.

			for (String key : keySet) 
				tag.append(" "+key+"=\""+this.attributes.get(key)+"\"");
		}

		tag.append(">");
					tag.append ( ""+this.content == null || this.content.toString().trim()=="" ? "" : ""+this.content.toString().trim());
					tag.append("</"+this.tagName+">");

					return tag.toString();
	}
}
