package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.SmsTemplateRepositoryCustom;
import com.gts.cms.entity.QSmsTemplate;
import com.gts.cms.entity.SmsTemplate;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Genesis
 *
 */
@Repository
public class SmsTemplateRepositoryImpl extends QuerydslRepositorySupport implements SmsTemplateRepositoryCustom {

	public SmsTemplateRepositoryImpl() {
		super(SmsTemplateRepositoryImpl.class);
	}

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public SmsTemplate getSmsTemplateByCode(String templateCode) {
		JPQLQuery<SmsTemplate> query = from(QSmsTemplate.smsTemplate)
				.where(QSmsTemplate.smsTemplate.templateCode.eq(templateCode).and(QSmsTemplate.smsTemplate.isActive.eq(true)));
		return query.fetchOne();
	}
}
