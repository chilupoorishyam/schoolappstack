package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.CampusRepositoryCustom;
import com.gts.cms.entity.Campus;
import com.gts.cms.entity.QCampus;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * created by Naveen on 14/09/2018
 * 
 */
@Repository
public class CampusRepositoryImpl extends QuerydslRepositorySupport implements CampusRepositoryCustom {
	public CampusRepositoryImpl() {
		super(Campus.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public Long deleteCampus(Long campusId) {
		return update(QCampus.campus).where(QCampus.campus.campusId.eq(campusId))
				.set(QCampus.campus.isActive, false).execute();
	}

}
