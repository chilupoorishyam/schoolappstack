package com.gts.cms.common.student.repository;

import com.gts.cms.common.student.repository.custom.StudentEnquiryCustomRepository;
import com.gts.cms.entity.StudentEnquiry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentEnquiryRepository extends JpaRepository<StudentEnquiry, Long>, StudentEnquiryCustomRepository {

}
