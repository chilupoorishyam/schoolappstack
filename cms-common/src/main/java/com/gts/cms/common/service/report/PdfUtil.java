package com.gts.cms.common.service.report;

/**
 * Shyamsunder
 */
public class PdfUtil {

    public static final String EMPTY_STRING="";
    public static final String PDF_HEADER="<div style='text-align: center;width:99%;'>%%HEADER%%<br/><hr style='height:1px;border:none;color:#333;background-color:#dcdcdc;'></hr></div>";
    public static final String PDF_FIXED_HEADER="<div style='text-align: center;width:99%;position: fixed;'>%%HEADER%%<br/><hr style='height:1px;border:none;color:#333;background-color:#dcdcdc;'></hr></div>";

    private PdfUtil() {
        throw new IllegalStateException("Utility class");
    }

    public static String replaceWithEmpty(String search, String replace, String subject) {
        if (replace != null) {
            return subject.replace(search, replace);
        } else {
            return subject.replace(search, "N/A");
        }
    }

    public static Div getLogo(String logoPath) {
        Div div = addImage(logoPath);
        div.addStyle("width:100%; height:4%;");
        return div;
    }

    public static Div getCheckMarks(String logoPath) {
        Div div = addImage(logoPath);
        div.addStyle("width:13px; height:13px;");

        return div;
    }

    public static Div addImage(String imagePath) {
        Div div = new Div();
        div.addClass("add-image");
        div.setAttribute("img-src", imagePath);
        return div;
    }

}
