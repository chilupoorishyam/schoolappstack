package com.gts.cms.common.employee.mapper;

import com.gts.cms.common.dto.EmployeeDailyDetailDTO;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Genesis
 */
@Component
public class EmployeeDailyDetailMapper implements BaseMapper<EmployeeDailyDetail, EmployeeDailyDetailDTO> {


    @Override
    public EmployeeDailyDetailDTO convertEntityToDTO(EmployeeDailyDetail employeeDailyDetail) {
        EmployeeDailyDetailDTO employeeDailyDetailDTO = null;
        if (employeeDailyDetail != null) {
            employeeDailyDetailDTO = new EmployeeDailyDetailDTO();
            employeeDailyDetailDTO.setEmployeeDailyDetailsId(employeeDailyDetail.getEmployeeDailyDetailsId());
            employeeDailyDetailDTO.setPeriodNo(employeeDailyDetail.getPeriodNo());
            employeeDailyDetailDTO.setNoOfStudentsAttended(employeeDailyDetail.getNoOfStudentsAttended());
            employeeDailyDetailDTO.setAbsenteesRollno(employeeDailyDetail.getAbsenteesRollno());
            employeeDailyDetailDTO.setOtherActivites(employeeDailyDetail.getOtherActivites());
            employeeDailyDetailDTO.setIsAttendanceUpdated(employeeDailyDetail.getIsAttendanceUpdated());
            employeeDailyDetailDTO.setIsMentReportUpdated(employeeDailyDetail.getIsMentReportUpdated());
            employeeDailyDetailDTO.setIsFacultyportfolioUpdated(employeeDailyDetail.getIsFacultyportfolioUpdated());
            employeeDailyDetailDTO.setInsertedDate(employeeDailyDetail.getInsertedDate());
            employeeDailyDetailDTO.setRemarks(employeeDailyDetail.getRemarks());
            employeeDailyDetailDTO.setIsActive(employeeDailyDetail.getIsActive());
            employeeDailyDetailDTO.setReason(employeeDailyDetail.getReason());
            employeeDailyDetailDTO.setCreatedDt(new Date());
            employeeDailyDetailDTO.setCreatedUser(SecurityUtil.getCurrentUser());
            employeeDailyDetailDTO.setUpdatedDt(new Date());
            employeeDailyDetailDTO.setUpdatedUser(SecurityUtil.getCurrentUser());

            if (employeeDailyDetail.getEmployeeDetail() != null) {
                employeeDailyDetailDTO.setEmployeeDetailId(employeeDailyDetail.getEmployeeDetail().getEmployeeId());
                employeeDailyDetailDTO.setEmployeeName(employeeDailyDetail.getEmployeeDetail().getFirstName());
            }

            if (employeeDailyDetail.getSchool() != null) {
                employeeDailyDetailDTO.setSchoolId(employeeDailyDetail.getSchool().getSchoolId());
                employeeDailyDetailDTO.setSchoolName(employeeDailyDetail.getSchool().getSchoolName());
                employeeDailyDetailDTO.setSchoolCode(employeeDailyDetail.getSchool().getSchoolCode());
            }

            if (employeeDailyDetail.getDepartment() != null) {
                employeeDailyDetailDTO.setDepartmentId(employeeDailyDetail.getDepartment().getDepartmentId());
                employeeDailyDetailDTO.setDepartmentName(employeeDailyDetail.getDepartment().getDeptName());
                employeeDailyDetailDTO.setDepartmentCode(employeeDailyDetail.getDepartment().getDeptCode());
            }

            if (employeeDailyDetail.getDesignation() != null) {
                employeeDailyDetailDTO.setDesignationId(employeeDailyDetail.getDesignation().getDesignationId());
                employeeDailyDetailDTO.setDesignationName(employeeDailyDetail.getDesignation().getDesignationName());
            }

            if (employeeDailyDetail.getGroupSection() != null) {
                employeeDailyDetailDTO.setGroupSectionId(employeeDailyDetail.getGroupSection().getGroupSectionId());
                employeeDailyDetailDTO.setGroupSection(employeeDailyDetail.getGroupSection().getSection());
                if (employeeDailyDetail.getGroupSection().getAcademicYear() != null)
                    employeeDailyDetailDTO.setGroupSectionAcademicYear(employeeDailyDetail.getGroupSection().getAcademicYear().getAcademicYear());
                if (employeeDailyDetail.getGroupSection().getCourseYear() != null)
                    employeeDailyDetailDTO.setGroupSectionCourseYear(employeeDailyDetail.getGroupSection().getCourseYear().getCourseYearCode());
            }


            if (employeeDailyDetail.getTheoryLabCatdetId() != null) {
                employeeDailyDetailDTO.setTheoryLabCatdetId(employeeDailyDetail.getTheoryLabCatdetId().getGeneralDetailId());
                employeeDailyDetailDTO.setTheoryLabCatdetCode(employeeDailyDetail.getTheoryLabCatdetId().getGeneralDetailCode());
            }

            if (employeeDailyDetail.getSubUnitTopicId() != null) {
                employeeDailyDetailDTO.setSubUnitTopicId(employeeDailyDetail.getSubUnitTopicId().getSubjectUnitTopicId());
                employeeDailyDetailDTO.setSubUnitTopicName(employeeDailyDetail.getSubUnitTopicId().getTopicName());
                if (employeeDailyDetail.getSubUnitTopicId().getSubjectregulation() != null) {
                    employeeDailyDetailDTO.setSubjectName(employeeDailyDetail.getSubUnitTopicId().getSubjectregulation().getSubject().getSubjectName());
                    employeeDailyDetailDTO.setSubjectCode(employeeDailyDetail.getSubUnitTopicId().getSubjectregulation().getSubject().getSubjectCode());
                }
            }

        }
        return employeeDailyDetailDTO;
    }

    @Override
    public List<EmployeeDailyDetailDTO> convertEntityListToDTOList(List<EmployeeDailyDetail> entityList) {
        List<EmployeeDailyDetailDTO> employeeDailyDetailDTOS = new ArrayList<>();
        entityList
                .forEach(employeeDailyDetail -> employeeDailyDetailDTOS.add(convertEntityToDTO(employeeDailyDetail)));
        return employeeDailyDetailDTOS;
    }

    @Override
    public List<EmployeeDailyDetail> convertDTOListToEntityList(List<EmployeeDailyDetailDTO> dtoList) {
        List<EmployeeDailyDetail> employeeDailyDetails = new ArrayList<>();
        if (dtoList != null) {
            dtoList.forEach(employeeDailyDetailDTO -> employeeDailyDetails
                    .add(convertDTOtoEntity(employeeDailyDetailDTO, null)));
        }
        return employeeDailyDetails;
    }

    @Override
    public EmployeeDailyDetail convertDTOtoEntity(EmployeeDailyDetailDTO employeeDailyDetailDTO, EmployeeDailyDetail employeeDailyDetail) {
        if (employeeDailyDetailDTO != null) {
            if (employeeDailyDetail == null) {
                employeeDailyDetail = new EmployeeDailyDetail();
                employeeDailyDetail.setCreatedDt(new Date());
                employeeDailyDetail.setIsActive(Boolean.TRUE);
                employeeDailyDetail.setCreatedUser(SecurityUtil.getCurrentUser());
            } else {
                employeeDailyDetail.setIsActive(employeeDailyDetailDTO.getIsActive());
            }
            employeeDailyDetail.setEmployeeDailyDetailsId(employeeDailyDetailDTO.getEmployeeDailyDetailsId());

            employeeDailyDetail.setPeriodNo(employeeDailyDetailDTO.getPeriodNo());
            employeeDailyDetail.setNoOfStudentsAttended(employeeDailyDetailDTO.getNoOfStudentsAttended());
            employeeDailyDetail.setAbsenteesRollno(employeeDailyDetailDTO.getAbsenteesRollno());
            employeeDailyDetail.setOtherActivites(employeeDailyDetailDTO.getOtherActivites());
            employeeDailyDetail.setIsAttendanceUpdated(employeeDailyDetailDTO.getIsAttendanceUpdated());
            employeeDailyDetail.setIsMentReportUpdated(employeeDailyDetailDTO.getIsMentReportUpdated());
            employeeDailyDetail.setIsFacultyportfolioUpdated(employeeDailyDetailDTO.getIsFacultyportfolioUpdated());
            employeeDailyDetail.setInsertedDate(employeeDailyDetailDTO.getInsertedDate());
            employeeDailyDetail.setRemarks(employeeDailyDetailDTO.getRemarks());
            employeeDailyDetail.setReason(employeeDailyDetailDTO.getReason());
            if (employeeDailyDetailDTO.getEmployeeDetailId() != null) {
                EmployeeDetail employeeDetail = new EmployeeDetail();
                employeeDetail.setEmployeeId(employeeDailyDetailDTO.getEmployeeDetailId());
                employeeDailyDetail.setEmployeeDetail(employeeDetail);
            }
            if (employeeDailyDetailDTO.getSchoolId() != null) {
                School school = new School();
                school.setSchoolId(employeeDailyDetailDTO.getSchoolId());
                employeeDailyDetail.setSchool(school);
            }
            if (employeeDailyDetailDTO.getDepartmentId() != null) {
                Department department = new Department();
                department.setDepartmentId(employeeDailyDetailDTO.getDepartmentId());
                employeeDailyDetail.setDepartment(department);
            }
            if (employeeDailyDetailDTO.getDesignationId() != null) {
                Designation designation = new Designation();
                designation.setDesignationId(employeeDailyDetailDTO.getDesignationId());
                employeeDailyDetail.setDesignation(designation);
            }
            if (employeeDailyDetailDTO.getGroupSectionId() != null) {
                GroupSection groupSection = new GroupSection();
                groupSection.setGroupSectionId(employeeDailyDetailDTO.getGroupSectionId());
                employeeDailyDetail.setGroupSection(groupSection);
            }

            if (employeeDailyDetailDTO.getTheoryLabCatdetId() != null) {
                GeneralDetail generalDetail = new GeneralDetail();
                generalDetail.setGeneralDetailId(employeeDailyDetailDTO.getTheoryLabCatdetId());
                employeeDailyDetail.setTheoryLabCatdetId(generalDetail);
            }
            if (employeeDailyDetailDTO.getSubUnitTopicId() != null) {
                SubjectUnitTopic subjectUnitTopic = new SubjectUnitTopic();
                subjectUnitTopic.setSubjectUnitTopicId(employeeDailyDetailDTO.getSubUnitTopicId());
                employeeDailyDetail.setSubUnitTopicId(subjectUnitTopic);
            }
        }
        return employeeDailyDetail;
    }
}
