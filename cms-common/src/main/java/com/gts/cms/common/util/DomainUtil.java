package com.gts.cms.common.util;

import org.apache.log4j.Logger;

import com.gts.cms.common.exception.ApiException;

public class DomainUtil {
	private static final Logger LOGGER = Logger.getLogger(DomainUtil.class);

	public static Class<?> getEntityClazz(String entityName) {
		try {
			return Class.forName("com.gts.cms." + entityName);
		} catch (ClassNotFoundException e) {
			LOGGER.error("Error occured while initializing entity:" + entityName);
			throw new ApiException("Given Entity Not found - " + entityName);
		}
	}
	
	public static Class<?> getDTOClazz(String dtoName) {
		try {
			return Class.forName("com.gts.cms.common.dto." + dtoName);
		} catch (ClassNotFoundException e) {
			LOGGER.error("Error occured while initializing DTO:" + dtoName);
			throw new ApiException("Given DTO Not found - " + dtoName);
		}
	}
}
