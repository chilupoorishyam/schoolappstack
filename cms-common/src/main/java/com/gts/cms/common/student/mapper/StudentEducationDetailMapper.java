package com.gts.cms.common.student.mapper;

import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.student.dto.StudentEducationDetailDTO;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.GeneralDetail;
import com.gts.cms.entity.StudentAppEducation;
import com.gts.cms.entity.StudentDetail;
import com.gts.cms.entity.StudentEducationDetail;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class StudentEducationDetailMapper implements BaseMapper<StudentEducationDetail, StudentEducationDetailDTO> {

	@Override
	public StudentEducationDetail convertDTOtoEntity(StudentEducationDetailDTO dtoObject,
			StudentEducationDetail entityObject) {
		if (null == entityObject) {
			entityObject = new StudentEducationDetail();
			entityObject.setCreatedDt(new Date());
			entityObject.setCreatedUser(SecurityUtil.getCurrentUser());
		}

		entityObject.setIsActive(dtoObject.getIsActive());

		if (entityObject.getIsActive() == null) {
			entityObject.setIsActive(Boolean.TRUE);
		}

		entityObject.setStudentEducationId(dtoObject.getStudentEducationId());

		entityObject.setAddress(dtoObject.getAddress());

		entityObject.setBoard(dtoObject.getBoard());

		entityObject.setGradeClassSecured(dtoObject.getGradeClassSecured());

		entityObject.setMajorSubjects(dtoObject.getMajorSubjects());

		entityObject.setMedium(dtoObject.getMedium());

		entityObject.setNameOfInstitution(dtoObject.getNameOfInstitution());

		entityObject.setPrecentage(dtoObject.getPrecentage());

		entityObject.setYearOfCompletion(dtoObject.getYearOfCompletion());

		entityObject.setReason(dtoObject.getReason());

		entityObject.setUpdatedDt(new Date());
		entityObject.setUpdatedUser(SecurityUtil.getCurrentUser());

		if (dtoObject.getGeneralDetailId() != null) {
			GeneralDetail gd = new GeneralDetail();
			gd.setGeneralDetailId(dtoObject.getGeneralDetailId());
			gd.setGeneralDetailDisplayName(dtoObject.getGeneralDetailName());
			entityObject.setTMGeneralDetail(gd);
		}

		if (dtoObject.getAppEducationId() != null) {
			StudentAppEducation studentAppEducation = new StudentAppEducation();
			studentAppEducation.setAppEducationId(dtoObject.getAppEducationId());
			entityObject.setStudentAppEducation(studentAppEducation);
		}

		if (dtoObject.getStudentId() != null) {
			StudentDetail studentDetail = new StudentDetail();
			studentDetail.setStudentId(dtoObject.getStudentId());
			studentDetail.setFirstName(dtoObject.getFirstName());
			entityObject.setStudentDetail(studentDetail);
		}

		return entityObject;
	}

	@Override
	public StudentEducationDetailDTO convertEntityToDTO(StudentEducationDetail entityObject) {
		StudentEducationDetailDTO dtoObject = null;
		if (entityObject != null) {
			dtoObject = new StudentEducationDetailDTO();

			dtoObject.setStudentEducationId(entityObject.getStudentEducationId());
			
			dtoObject.setCreatedDt(entityObject.getCreatedDt());
			dtoObject.setCreatedUser(entityObject.getCreatedUser());
			dtoObject.setUpdatedDt(entityObject.getUpdatedDt());
			dtoObject.setUpdatedUser(entityObject.getUpdatedUser());
			
			dtoObject.setAddress(entityObject.getAddress());

			dtoObject.setBoard(entityObject.getBoard());

			dtoObject.setGradeClassSecured(entityObject.getGradeClassSecured());

			dtoObject.setMajorSubjects(entityObject.getMajorSubjects());

			dtoObject.setMedium(entityObject.getMedium());

			dtoObject.setNameOfInstitution(entityObject.getNameOfInstitution());

			dtoObject.setPrecentage(entityObject.getPrecentage());

			dtoObject.setYearOfCompletion(entityObject.getYearOfCompletion());

			dtoObject.setReason(entityObject.getReason());

			dtoObject.setIsActive(entityObject.getIsActive());

			if (entityObject.getTMGeneralDetail() != null) {
				dtoObject.setGeneralDetailId(entityObject.getTMGeneralDetail().getGeneralDetailId());
				dtoObject.setGeneralDetailCode(entityObject.getTMGeneralDetail().getGeneralDetailCode());
				dtoObject.setGeneralDetailName(entityObject.getTMGeneralDetail().getGeneralDetailDisplayName());
			}

			if (entityObject.getStudentAppEducation() != null) {
				dtoObject.setAppEducationId(entityObject.getStudentAppEducation().getAppEducationId());

			}
			if (entityObject.getStudentDetail() != null) {
				dtoObject.setStudentId(entityObject.getStudentDetail().getStudentId());
				dtoObject.setFirstName(entityObject.getStudentDetail().getFirstName());
			}
		}
		return dtoObject;
	}

	@Override
	public List<StudentEducationDetail> convertDTOListToEntityList(List<StudentEducationDetailDTO> dtoObjectList) {
		List<StudentEducationDetail> entityObjectList = new ArrayList<>();
		dtoObjectList.forEach(dtoObject -> entityObjectList.add(convertDTOtoEntity(dtoObject, null)));
		return entityObjectList;
	}

	@Override
	public List<StudentEducationDetailDTO> convertEntityListToDTOList(List<StudentEducationDetail> entityObjectList) {
		List<StudentEducationDetailDTO> dtoObjectList = new ArrayList<>();
		entityObjectList.forEach(entityObject -> dtoObjectList.add(convertEntityToDTO(entityObject)));
		return dtoObjectList;
	}
}