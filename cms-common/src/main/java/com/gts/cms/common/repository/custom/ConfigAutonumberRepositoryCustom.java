package com.gts.cms.common.repository.custom;

import com.gts.cms.entity.ConfigAutonumber;

import java.util.List;

public interface ConfigAutonumberRepositoryCustom {
Long deleteConfigAutoNumber(Long autoconfigId);
	
	List<ConfigAutonumber> findAllConfigAutoNumbers();
	
	ConfigAutonumber findSchoolConfigAutoNumbers(Long schoolId,String configAtttributeCode);
	
	public Long updateCurrentNumber(ConfigAutonumber configAutoNumber);
	
}
