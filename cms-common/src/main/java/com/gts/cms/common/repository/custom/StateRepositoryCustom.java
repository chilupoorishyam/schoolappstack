package com.gts.cms.common.repository.custom;

import com.gts.cms.entity.State;

import java.util.List;

/**
 * created by Naveen on 04/09/2018
 * 
 */
public interface StateRepositoryCustom {
	Long deleteStateById(Long stateId);

	List<State> findAllByCountryId(Long countryId);

}
