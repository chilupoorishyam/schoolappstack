package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.AcademicYearRepositoryCustom;
import com.gts.cms.entity.AcademicYear;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AcademicYearRepository extends JpaRepository<AcademicYear, Long>, AcademicYearRepositoryCustom {

    @Query("SELECT a FROM AcademicYear a"
            + " WHERE a.school.schoolId = :schoolId"
            + " AND  a.isActive = true"
            + " AND a.isDefault = true ORDER BY a.academicYearId"
    )
    List<AcademicYear> getDefaultAcademicYear(@Param("schoolId") Long schoolId);


    @Query("SELECT a FROM AcademicYear a"
            + " WHERE 1=1"
            + " AND(:schoolId is null or  a.school.schoolId = :schoolId)"
            + " AND(a.isActive = true) "
            + " ORDER BY a.academicYear DESC "
    )
    List<AcademicYear> getAcademicYear(@Param("schoolId") Long schoolId);

    AcademicYear findByAcademicYearId(@Param("academicYearId") Long academicYearId);

    @Query("SELECT a FROM AcademicYear a"
            + " WHERE a.school.schoolId = :schoolId"
            + " AND  a.academicYear = :academicYear"
            + " AND  a.isActive = true"
    )
    AcademicYear getDefaultAcademicYear(@Param("schoolId") Long schoolId,
                                        @Param("academicYear") String academicYear);
}