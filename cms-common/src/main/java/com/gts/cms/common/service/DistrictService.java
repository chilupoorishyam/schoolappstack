package com.gts.cms.common.service;

/**
 * The Interface DistrictService.
 */
public interface DistrictService {

	/**
	 * Gets the district details.
	 *
	 * @param districtId the district id
	 * @return the district details
	 *//*
	ApiResponse<DistrictDTO> getDistrictDetails(Long districtId);

	*//**
	 * Gets the all districts.
	 *
	 * @return the all districts
	 *//*
	ApiResponse<List<DistrictDTO>> getAllDistricts();

	*//**
	 * Adds the district.
	 *
	 * @param districtDTO the district DTO
	 * @return the api response
	 *//*
	ApiResponse<Long> addDistrict(DistrictDTO districtDTO);

	*//**
	 * Update district.
	 *
	 * @param districtDTO the district DTO
	 * @return the api response
	 *//*
	ApiResponse<Long> updateDistrict(DistrictDTO districtDTO);

	*//**
	 * Delete district.
	 *
	 * @param districtDTO the district DTO
	 * @return the api response
	 *//*
	ApiResponse<Long> deleteDistrict(DistrictDTO districtDTO);

	public ApiResponse<List<DistrictDTO>> getAllDistrictByState(Long stateId);*/

}
