package com.gts.cms.common.comparator;

import java.util.Comparator;

import com.gts.cms.common.dto.PageDTO;

public class PageDTOComparator implements Comparator<PageDTO>{

	@Override
	public int compare(PageDTO o1, PageDTO o2) {

		int var = o1.getSortOrder().compareTo(o2.getSortOrder());
		if (var == 0 && !o1.getPageId().equals(o2.getPageId())) {
			var = 1;
		}
		return var;
	}
	
}
