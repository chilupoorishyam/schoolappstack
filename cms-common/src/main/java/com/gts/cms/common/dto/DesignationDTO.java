package com.gts.cms.common.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class DesignationDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long designationId;
	@NotNull(message = "Organization Id is required.")
	private Long organizationId;
	private String orgName;
	private String orgCode;
	private String designationName;
	private String reason;
	@NotNull(message = "IsActive is required.")
	private Boolean isActive;
	
	public Long getDesignationId() {
		return designationId;
	}
	public void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}
	public Long getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getOrgCode() {
	       return this.orgCode;
	}
	public void setOrgCode(String orgCode) {
	        this.orgCode = orgCode;
	}
	public String getDesignationName() {
		return designationName;
	}
	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}
