package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.DistrictDTO;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.District;
import com.gts.cms.entity.State;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class DistrictMapper implements BaseMapper<District, DistrictDTO> {

	@Override
	public DistrictDTO convertEntityToDTO(District district) {
		DistrictDTO districtDTO = null;
		if (district != null) {
			districtDTO = new DistrictDTO();
			districtDTO.setDistrictId(district.getDistrictId());
			districtDTO.setDistrictName(district.getDistrictName());
			districtDTO.setDistrictCode(district.getDistrictCode());
			State state = new State();
			districtDTO.setStateId(state.getStateId());
		}
		return districtDTO;
	}

	@Override
	public District convertDTOtoEntity(DistrictDTO districtDto, District district) {
		if (null == district) {
			district = new District();
			district.setIsActive(Boolean.TRUE);
			district.setCreatedDt(new Date());
			district.setCreatedUser(SecurityUtil.getCurrentUser());
		}
		district.setDistrictId(districtDto.getDistrictId());
		district.setDistrictName(districtDto.getDistrictName());
		district.setDistrictCode(districtDto.getDistrictCode());
		if(districtDto.getStateId() != null) {
			State state=new State();
			state.setStateId(districtDto.getStateId());
			district.setState(state);
		}
		district.setUpdatedDt(new Date());
		district.setUpdatedUser(SecurityUtil.getCurrentUser());
		return district;
	}

	public List<District> convertDistrictDTOListToDistrictList(List<DistrictDTO> countryDTOList) {
		List<District> districtList = new ArrayList<>();
		countryDTOList.forEach(districtDTO -> districtList.add(convertDTOtoEntity(districtDTO, null)));
		return districtList;

	}

	@Override
	public List<DistrictDTO> convertEntityListToDTOList(List<District> countryList) {
		List<DistrictDTO> districtDTOList = new ArrayList<>();
		countryList.forEach(district -> districtDTOList.add(convertEntityToDTO(district)));
		return districtDTOList;
	}

	@Override
	public List<District> convertDTOListToEntityList(List<DistrictDTO> dtoList) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
