package com.gts.cms.common.student.service.impl;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.StudentDetailListDTO;
import com.gts.cms.common.mapper.StudentDetailListMapper;
import com.gts.cms.common.service.report.CommonUtils;
import com.gts.cms.common.student.mapper.StudentDetailMapper;
import com.gts.cms.common.student.repository.StudentAcademicBatchesRepository;
import com.gts.cms.common.student.repository.StudentDetailRepository;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.GroupSection;
import com.gts.cms.entity.StudentAcademicbatch;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
@Service
public class StudentSectionChange{
	private static final Logger LOGGER = Logger.getLogger(StudentSectionChange.class);
	@Autowired
	StudentDetailRepository studentDetailRepository;
/*	@Autowired
	StudentAttendanceRepository studentAttendanceRepository;*/
	@Autowired
	StudentDetailMapper studentDetailMapper;
	@Autowired
	StudentDetailListMapper studentDetailListMapper;
	@Autowired
	StudentAcademicBatchesRepository studentAcademicBatchesRepository;
/*
	@Autowired
	StudentAttendanceMapper studentAttendanceMapper;
	@Autowired
	StudentAcademicbatchMapper studentAcademicbatchMapper;
*/

	/*Section change
	 * 07-05-2020
	 * 
	 * @rif
	 */
	// @Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ApiResponse<?> updateStudentsList(List<StudentDetailListDTO> studentDetailListDTOs) {
		return null;
	}

	/**
	 * @param studentDetailListDTOObj
	 * @param studentAcademicbatch
	 */
	private void stdAcadBatchUpdateAndInsert(StudentDetailListDTO studentDetailListDTOObj, StudentAcademicbatch studentAcademicbatch) throws Exception{
		LOGGER.info("StudentSectionChange.stdAcadBatchUpdateAndInsert()");
		if (studentAcademicbatch != null) {
			
			studentAcademicbatch.setIsActive(Boolean.TRUE);
			Date todate=CommonUtils.getLessThanOneDate(studentDetailListDTOObj.getToDate());
			studentAcademicbatch.setToDate(todate);
			LOGGER.info("StudentAcademicBatch toDate updating==>" + todate);
			// studentAcademicbatch.setToDate(new Date());
			GroupSection groupSection = new GroupSection();
			if (studentDetailListDTOObj.getGroupSectionId() != null) {
				groupSection.setGroupSectionId(studentDetailListDTOObj.getGroupSectionId());
				studentAcademicbatch.setToGroupSection(groupSection);
			} else {
				studentAcademicbatch.setToGroupSection(null);
			}

			studentAcademicbatch.setUpdatedDt(new Date());
			studentAcademicbatch.setUpdatedUser(SecurityUtil.getCurrentUser());
			studentAcademicBatchesRepository.save(studentAcademicbatch);
			studentAcademicBatchesRepository.flush();
			LOGGER.info("StudentSectionChange.stdAcadBatchUpdateAndInsert()==>StudentAcademicBatch record updated");
			//inserting new record
			StudentAcademicbatch studentAcademicbatchNew = new StudentAcademicbatch();
			
			studentAcademicbatchNew.setSchool(studentAcademicbatch.getSchool());
			studentAcademicbatchNew.setStudentDetail(studentAcademicbatch.getStudentDetail());
			studentAcademicbatchNew.setCourse(studentAcademicbatch.getCourse());
			studentAcademicbatchNew.setAcademicYear(studentAcademicbatch.getAcademicYear());
			studentAcademicbatchNew.setStudentStatus(studentAcademicbatch.getStudentStatus());
			studentAcademicbatchNew.setFromBatch(studentAcademicbatch.getFromBatch());
			studentAcademicbatchNew.setFromCourseYear(studentAcademicbatch.getFromCourseYear());
			if (studentDetailListDTOObj.getGroupSectionId() != null) {
				studentAcademicbatchNew.setFromGroupSection(groupSection);
			} else {
				studentAcademicbatchNew.setFromGroupSection(null);
			}
			studentAcademicbatchNew.setToBatch(studentAcademicbatch.getToBatch());
			studentAcademicbatchNew.setToCourseYear(studentAcademicbatch.getToCourseYear());
			studentAcademicbatchNew.setToGroupSection(null);
			studentAcademicbatchNew.setReason("Section update");
			studentAcademicbatchNew.setIsActive(Boolean.TRUE);
			Date fromDate=CommonUtils.getDateAndTime(studentDetailListDTOObj.getToDate());
			if (studentDetailListDTOObj.getToDate() != null && fromDate!=null) {
				studentAcademicbatchNew.setFromDate(fromDate);
			}
			LOGGER.info("StudentAcademicBatch ==>new record==>fromDate==> "+fromDate);
			studentAcademicbatchNew.setToDate(null);
			studentAcademicbatchNew.setCreatedDt(new Date());
			studentAcademicbatchNew.setCreatedUser(SecurityUtil.getCurrentUser());

			studentAcademicBatchesRepository.save(studentAcademicbatchNew);
			studentAcademicBatchesRepository.flush();
			LOGGER.info("StudentSectionChange.stdAcadBatchUpdateAndInsert()==>StudentAcademicBatch New record inserted");
			LOGGER.info("StudentSectionChange.stdAcadBatchUpdateAndInsert()==>executed");
		}
	}
}
