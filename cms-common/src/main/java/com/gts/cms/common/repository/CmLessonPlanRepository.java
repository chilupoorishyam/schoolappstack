package com.gts.cms.common.repository;

import com.gts.cms.entity.CmLessonPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CmLessonPlanRepository extends JpaRepository<CmLessonPlan, Long> {
}