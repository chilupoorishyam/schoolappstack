package com.gts.cms.common.student.mapper;

import com.gts.cms.common.student.dto.StudentDocumentCollectionDTO;
import com.gts.cms.common.util.FileUtil;
import com.gts.cms.common.util.OptionalUtil;
import com.gts.cms.entity.StudentDocumentCollection;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Genesis
 *
 */
@Component
public class StudentDocumentCollectionMapper {

	public StudentDocumentCollectionDTO convertStudentDocumentCollectionToStudentDocumentCollectionDTO(
			StudentDocumentCollection studentDocumentCollection) {
		StudentDocumentCollectionDTO studentDocumentCollectionDTO = null;
		if (studentDocumentCollection != null) {
			studentDocumentCollectionDTO = new StudentDocumentCollectionDTO();

			studentDocumentCollectionDTO.setStdDocCollId(studentDocumentCollection.getStudentDocCollId());
			studentDocumentCollectionDTO.setCreatedDt(studentDocumentCollection.getCreatedDt());
			studentDocumentCollectionDTO.setCreatedUser(studentDocumentCollection.getCreatedUser());
			studentDocumentCollectionDTO.setFileName(studentDocumentCollection.getFileName());
			studentDocumentCollectionDTO.setFilePath(FileUtil.getAbsolutePath(studentDocumentCollection.getFilePath()));
			studentDocumentCollectionDTO.setIsHardCopy(studentDocumentCollection.getIsHardCopy());
			studentDocumentCollectionDTO.setIsOriginal(studentDocumentCollection.getIsOriginal());
			studentDocumentCollectionDTO.setIsSoftCopy(studentDocumentCollection.getIsSoftCopy());
			studentDocumentCollectionDTO.setIsVerified(studentDocumentCollection.getIsVerified());
			studentDocumentCollectionDTO.setRackNumber(studentDocumentCollection.getRackNumber());
			studentDocumentCollectionDTO.setIsActive(studentDocumentCollection.getIsActive());
			studentDocumentCollectionDTO.setReason(studentDocumentCollection.getReason());

			if (studentDocumentCollection.getStudentDetail() != null) {
				studentDocumentCollectionDTO.setStudentId(studentDocumentCollection.getStudentDetail().getStudentId());
			}
			if(studentDocumentCollection.getDocumentRepository()!=null) {
				studentDocumentCollectionDTO.setDocumentRepositoryId(studentDocumentCollection.getDocumentRepository().getDocumentRepositoryId());
			}

			studentDocumentCollectionDTO.setVerifiedByEmpId(
					OptionalUtil.resolve(() -> studentDocumentCollection.getVerifiedbyEmployeeDetail().getEmployeeId())
							.orElse(null));
			studentDocumentCollectionDTO.setDocRepId(OptionalUtil
					.resolve(() -> studentDocumentCollection.getDocumentRepository().getDocumentRepositoryId())
					.orElse(null));

			studentDocumentCollectionDTO.setAppDocCollId(
					OptionalUtil.resolve(() -> studentDocumentCollection.getStudentAppDocCollection().getAppDocCollId())
							.orElse(null));

		}
		return studentDocumentCollectionDTO;
	}

	public StudentDocumentCollection convertStudentDocumentCollectionDTOToStudentDocumentCollection(
			StudentDocumentCollectionDTO studentDocumentCollectionDTO) {
		StudentDocumentCollection studentDocumentCollection = null;
		if (studentDocumentCollectionDTO != null) {
			studentDocumentCollection = new StudentDocumentCollection();

			/*
			 * studentDocumentCollection.setCreatedDt(studentDocumentCollectionDTO.
			 * getCreatedDt());
			 * studentDocumentCollection.setCreatedUser(studentDocumentCollectionDTO.
			 * getCreatedUser());
			 */
			studentDocumentCollection.setIsActive(studentDocumentCollectionDTO.getIsActive());
			studentDocumentCollection.setReason(studentDocumentCollectionDTO.getReason());

		}
		return studentDocumentCollection;
	}

	public List<StudentDocumentCollection> convertStudentDocumentCollectionDTOListToStudentDocumentCollectionList(
			List<StudentDocumentCollectionDTO> studentDocumentCollectionDTOList) {
		List<StudentDocumentCollection> studentDocumentCollectionList = new ArrayList<>();
		if (studentDocumentCollectionDTOList != null) {
			studentDocumentCollectionDTOList.forEach(studentDocumentCollection -> studentDocumentCollectionList
					.add(convertStudentDocumentCollectionDTOToStudentDocumentCollection(studentDocumentCollection)));
		}
		return studentDocumentCollectionList;

	}

	public List<StudentDocumentCollectionDTO> convertStudentDocumentCollectionListToStudentDocumentCollectionDTOList(
			List<StudentDocumentCollection> studentDocumentCollectionList) {
		List<StudentDocumentCollectionDTO> studentDocumentCollectionDTOList = new ArrayList<>();
		studentDocumentCollectionList.forEach(studentDocumentCollection -> studentDocumentCollectionDTOList
				.add(convertStudentDocumentCollectionToStudentDocumentCollectionDTO(studentDocumentCollection)));
		return studentDocumentCollectionDTOList;
	}

}
