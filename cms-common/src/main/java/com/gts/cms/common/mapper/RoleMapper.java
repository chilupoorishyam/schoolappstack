package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.RoleDTO;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.Organization;
import com.gts.cms.entity.Role;
import com.gts.cms.entity.School;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class RoleMapper implements BaseMapper<Role, RoleDTO> {
@Override
public RoleDTO convertEntityToDTO(Role role) {
	RoleDTO roleDTO = null;
	if (role != null) {
	roleDTO = new RoleDTO();
	  roleDTO.setCreatedDt(role.getCreatedDt());
	  roleDTO.setUpdatedDt(role.getUpdatedDt());
	  roleDTO.setUpdatedUser(role.getUpdatedUser());
	  roleDTO.setIsActive(role.getIsActive());
	  roleDTO.setCreatedUser(role.getCreatedUser());
	  roleDTO.setRoleName(role.getRoleName());
	  roleDTO.setIsEditable(role.getIsEditable());
	  roleDTO.setDescription(role.getDescription());
	  roleDTO.setRoleId(role.getRoleId());
	  roleDTO.setReason(role.getReason());
	  
	  if(role.getSchool()!= null){
		  roleDTO.setSchoolId(role.getSchool().getSchoolId());
		  roleDTO.setSchoolName(role.getSchool().getSchoolName());
		  roleDTO.setSchoolCode(role.getSchool().getSchoolCode());
	  }
	 if(role.getOrganization()!=null) {
		 roleDTO.setOrganizationId(role.getOrganization().getOrganizationId());
		 roleDTO.setOrgCode(role.getOrganization().getOrgCode());
		 roleDTO.setOrgName(role.getOrganization().getOrgName());
	 }
	}
	return roleDTO;
}
@Override
public Role convertDTOtoEntity(RoleDTO roleDTO,Role role) {
	if ( null == role ) {
	 role = new Role();
	 role.setIsActive(Status.ACTIVE.getId());
	 role.setCreatedDt(new Date());
	 role.setCreatedUser(SecurityUtil.getCurrentUser());
	}else{
	 role.setIsActive(roleDTO.getIsActive());
	}
	  role.setUpdatedDt(new Date());
	  role.setUpdatedUser(SecurityUtil.getCurrentUser());
	  role.setRoleName(roleDTO.getRoleName());
	  role.setIsEditable(roleDTO.getIsEditable());
	 
	  role.setDescription(roleDTO.getDescription());
	  role.setRoleId(roleDTO.getRoleId());
	 if(roleDTO.getSchoolId()!= null){
		 School school = new School();
		 school.setSchoolId(roleDTO.getSchoolId());
		 school.setSchoolName(roleDTO.getSchoolName());
		 school.setSchoolCode(roleDTO.getSchoolCode());
		 role.setSchool(school);
	 }
	 if(roleDTO.getOrganizationId()!= null){
		 Organization organization = new Organization();
		 organization.setOrganizationId(roleDTO.getOrganizationId());
		 organization.setOrgName(roleDTO.getOrgName());
		 organization.setOrgCode(roleDTO.getOrgCode());
		 role.setOrganization(organization);
	 }
	  role.setReason(roleDTO.getReason());
	 
	return role;
}
@Override
public List<Role> convertDTOListToEntityList(List<RoleDTO> roleDTOList) {
	List<Role> roleList = new ArrayList<>();
	 roleDTOList.forEach(roleDTO -> roleList.add(convertDTOtoEntity(roleDTO, null)));
	return roleList;
}
@Override
public List<RoleDTO> convertEntityListToDTOList(List<Role> roleList) {
	List<RoleDTO> roleDTOList = new ArrayList<>();
	 roleList.forEach(role -> roleDTOList.add(convertEntityToDTO(role)));
	return roleDTOList;
}
}