package com.gts.cms.common.repository.custom;

import com.gts.cms.entity.WorkflowStage;

public interface WorkflowStageRepositoryCustom {
	public WorkflowStage getWorkflowStageStatus(Long schoolId, String workflowCode, Integer stage);
	public WorkflowStage getworkflowStageByCode(Long schoolId, String workflowStageCode,String workflowForCode);
	public WorkflowStage getworkflowForStageByCode(Long schoolId, String workflowForCode,String workflowStageCode);
	
		
}
