package com.gts.cms.common.student.service.impl;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.repository.*;
import com.gts.cms.common.student.dto.StudentDetailDTO;
import com.gts.cms.common.student.dto.StudentDetailStgDTO;
import com.gts.cms.common.student.mapper.StudentDetailMapper;
import com.gts.cms.common.student.mapper.StudentDetailStgMapper;
import com.gts.cms.common.student.repository.StudentAcademicBatchesRepository;
import com.gts.cms.common.student.repository.StudentDetailRepository;
import com.gts.cms.common.student.repository.StudentDetailStgRepository;
import com.gts.cms.common.student.service.StudentDetailStgService;
import com.gts.cms.entity.*;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class StudentDetailStgServiceImpl implements StudentDetailStgService {
    private static final Logger LOGGER = Logger.getLogger(StudentDetailStgServiceImpl.class);

    private static final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    @Autowired
    private StudentDetailRepository studentDetailRepository;

    @Autowired
    private StudentAcademicBatchesRepository studentAcademicBatchesRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StudentDetailStgRepository studentDetailStgRepository;

    @Autowired
    private StudentDetailStgMapper studentDetailStgMapper;

    @Autowired
    private StudentDetailMapper studentDetailMapper;

    @Autowired
    OrganizationRepository organizationRepository;

   /* @Autowired
    SchoolRepository schoolRepository;*/

    @Autowired
    UserTypeRepository userTypeRepository;

    @Autowired
    CourseRepository courseRepository;

   /* @Autowired
    CourseGroupRepository courseGroupRepository;*/

    @Autowired
    CourseYearRepository courseYearRepository;

    @Autowired
    GroupSectionRepository groupSectionRepository;


    @Autowired
    BatchRepository batchRepository;

    @Autowired
    AcademicYearRepository academicYearRepository;

    @Autowired
    GeneralDetailRepository generalDetailRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    QualificationRepository qualificationRepository;

    @Autowired
    CityRepository cityRepository;

    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    SchoolRepository schoolRepository;

    @Override
    public ApiResponse<List<StudentDetailStgDTO>> uploadExcel(StudentDetailStgDTO studentDetailStgDTO) {
        LOGGER.info("StudentDetailStgServiceImpl.uploadExcel()" + System.currentTimeMillis());
        Long startTime = System.currentTimeMillis();
        ApiResponse<List<StudentDetailStgDTO>> response = null;
        Workbook workbook = null;
        Sheet sheet = null;
        Cell cell = null;
        List<StudentDetailStg> studentDetailStgList = new ArrayList<>();
        try {
            MultipartFile excelFile = studentDetailStgDTO.getFiles().get("file");
            workbook = getWorkbook(excelFile, workbook);
            LOGGER.info("Excel file ==>" + excelFile.getName());
            if (workbook != null) {
                sheet = workbook.getSheetAt(0);
                for (Integer i = 1; i <= sheet.getLastRowNum(); i++) {
                    Row row = sheet.getRow(i);
                    LOGGER.info("Row Count of excel ==>" + i);
                    StudentDetailStg studentDetailStg = new StudentDetailStg();
                    if (row.getCell(0) != null) {
                        studentDetailStg.setOrganization(row.getCell(0).getStringCellValue());
                    }
                    if (row.getCell(1) != null) {
                        studentDetailStg.setSchool(row.getCell(1).getStringCellValue());
                    }
                    if (row.getCell(2) != null) {
                        studentDetailStg.setCourse(row.getCell(2).getStringCellValue());
                    }
                    if (row.getCell(3) != null) {
                        studentDetailStg.setGroup(row.getCell(3).getStringCellValue());
                    }
                    if (row.getCell(4) != null) {
                        studentDetailStg.setCourseYear(row.getCell(4).getStringCellValue());
                    }
                    if (row.getCell(5) != null) {
                        studentDetailStg.setSection(row.getCell(5).getStringCellValue());
                    }
                    if (row.getCell(6) != null) {
                        studentDetailStg.setQuota(row.getCell(6).getStringCellValue());
                    }
                    /*if (row.getCell(7) != null) {
                        studentDetailStg.setRegulation(row.getCell(7).getStringCellValue());
                    }*/
                    if (row.getCell(8) != null) {
                        studentDetailStg.setBatch(row.getCell(8).getStringCellValue());
                    }
                    if (row.getCell(9) != null) {
                        studentDetailStg.setAcademicYear(row.getCell(9).getStringCellValue());
                    }
                    if (row.getCell(10) != null) {
                        cell = row.getCell(10);
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        studentDetailStg.setRollNo(cell.getStringCellValue());
                    }
                    if (row.getCell(11) != null) {
                        cell = row.getCell(11);
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        studentDetailStg.setHallTicketNumber(cell.getStringCellValue());
                    }
                    if (row.getCell(12) != null) {
                        studentDetailStg.setFirstName(row.getCell(12).getStringCellValue());
                    }
                    if (row.getCell(13) != null) {
                        studentDetailStg.setMiddleName(row.getCell(13).getStringCellValue());
                    }
                    if (row.getCell(14) != null) {
                        studentDetailStg.setLastName(row.getCell(14).getStringCellValue());
                    }
                    if (row.getCell(15) != null) {
//                        if (row.getCell(15).getStringCellValue() == null
//                                || row.getCell(15).getStringCellValue().isEmpty())
//                            throw new ApiException("Gender is mandatory");
//                        else
                        studentDetailStg.setGender(row.getCell(15).getStringCellValue());
                    }
                    if (row.getCell(16) != null) {
                        try {
                            Date dateOfBirth = row.getCell(16).getDateCellValue();
                            if (dateOfBirth != null) {
                                String strDateOfBirth = dateFormat.format(dateOfBirth);
                                studentDetailStg.setDateOfBirth(strDateOfBirth);
                            }
                        } catch (Exception e) {
                            throw new ApiException("Check DOB format it should be DD-MM-YYYY");
                        }
                    }


                    if (row.getCell(17) != null) {
                        cell = row.getCell(17);
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        if (cell.getStringCellValue().length() > 10)
                            throw new ApiException("Check Student Mobile Number it should be 10 digits");
                        else
                            studentDetailStg.setMobile(cell.getStringCellValue());
                    } if (row.getCell(18) != null) {
                        studentDetailStg.setStudentEmailID(row.getCell(18).getStringCellValue());
                    }

                    if (row.getCell(19) != null) {
                        //studentDetailStg.setDateOfBirth(String.valueOf(row.getCell(15).getDateCellValue()));
                        cell = row.getCell(19);
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        studentDetailStg.setAadhaarNo(cell.getStringCellValue());
                    }


                    if (row.getCell(20) != null) {
                        cell = row.getCell(20);
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        studentDetailStg.setFatherName(cell.getStringCellValue());
                    }
                    if (row.getCell(21) != null) {
                        cell = row.getCell(21);
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        if (cell.getStringCellValue().length() > 10)
                            throw new ApiException("Check Father Mobile Number it should be 10 digits");
                        else
                            studentDetailStg.setFatherMobile(cell.getStringCellValue());
                    }
                    if (row.getCell(22) != null) {
                        cell = row.getCell(22);
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        studentDetailStg.setMotherName(cell.getStringCellValue());
                    }
                    if (row.getCell(23) != null) {
                        cell = row.getCell(23);
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        if (cell.getStringCellValue().length() > 10)
                            throw new ApiException("Check Mother Mobile Number it should be 10 digits");
                        else
                            studentDetailStg.setMotherMobile(cell.getStringCellValue());
                    }
                    if (row.getCell(24) != null) {
                        cell = row.getCell(24);
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        studentDetailStg.setPermanentAddress(cell.getStringCellValue());
                    }
                    if (row.getCell(25) != null) {
                        cell = row.getCell(25);
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        studentDetailStg.setCity(cell.getStringCellValue());
                    }
                    if (row.getCell(26) != null) {
                        cell = row.getCell(26);
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        studentDetailStg.setDistrict(cell.getStringCellValue());
                    }
                    studentDetailStgList.add(studentDetailStg);
                }
                studentDetailStgRepository.saveAll(studentDetailStgList);
                try {
                    workbook.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    LOGGER.error("Exception while closing sheet: " + e);
                }
                List<StudentDetailStgDTO> studentDetailStgDTOList = studentDetailStgMapper.convertEntityListToDTOList(studentDetailStgList);
                response = new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(), studentDetailStgDTOList);
            } else {
                response = new ApiResponse<>("Unable to read workbook", null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Error while parsing workbook:: " + e);
            throw new ApiException(e.getMessage(), e);
        }
        Long endTime = System.currentTimeMillis();
        Long totalTime = endTime - startTime;
        LOGGER.info("StudentDetailStgServiceImpl.uploadExcel() total time: " + totalTime);
        return response;
    }

    @Override
    public ApiResponse<List<StudentDetailStgDTO>> getStgStudentDetails() {
        List<StudentDetailStg> studentDetailStgList = studentDetailStgRepository.findAll();
        List<StudentDetailStgDTO> studentDetailStgDTOList = studentDetailStgMapper.convertEntityListToDTOList(studentDetailStgList);
        ApiResponse<List<StudentDetailStgDTO>> response = null;
        if (!studentDetailStgDTOList.isEmpty()) {
            response = new ApiResponse<>("Data", studentDetailStgDTOList);
        } else
            response = new ApiResponse<>(Messages.RECORDS_NOT_FOUND.getValue(), studentDetailStgDTOList);
        return response;
    }

    @Override
    public ApiResponse<List<StudentDetailDTO>> processStgStudentDetails() {
        LOGGER.info("StudentDetailStgServiceImpl.processStgEmployeeDetails()" + System.currentTimeMillis());
        Long startTime = System.currentTimeMillis();
        ApiResponse<List<StudentDetailDTO>> response = null;
        List<StudentDetail> studentDetailList = new ArrayList<>();
        List<StudentDetailStg> deleteList = new ArrayList<>();
        List<StudentAcademicbatch> academicBatchList = new ArrayList<>();
        List<User> usersList = new ArrayList<>();
        try {
            List<StudentDetailStg> studentDetailStgList = studentDetailStgRepository.findAll();
            for (StudentDetailStg studentDetailStg : studentDetailStgList) {
                List<StudentDetail> checkUserExists = studentDetailRepository.checkUserExists(studentDetailStg.getFirstName(), studentDetailStg.getLastName());
                if (checkUserExists.isEmpty()) {
                    StudentDetail studentDetail = new StudentDetail();
                    StudentAcademicbatch studentAcademicbatch = new StudentAcademicbatch();
                    User user = new User();
                    Optional<Organization> organization = organizationRepository.findByOrgCode(studentDetailStg.getOrganization());
                    if (organization.isPresent()) {
                        studentDetail.setOrganization(organization.get());
                        user.setOrganization(organization.get());
                    }
                    Optional<School> school = schoolRepository.findBySchoolCodeAndIsActive(studentDetailStg.getSchool(), true);
                    Long schoolId = 1L;
                    Long courseId = 1L;
                    Long courseYearId = 1L;
                    Long academicYearId = 1L;
                    if (school.isPresent()) {
                        studentDetail.setSchool(school.get());
                        studentAcademicbatch.setSchool(school.get());
                        user.setSchool(school.get());
                        schoolId = school.get().getSchoolId();
                    }
                    Usertype usertype = new Usertype();
                    usertype.setUserTypeId(userTypeRepository.findByUserName("STUDENT"));
                    user.setUserType(usertype);
                    List<Course> courses = courseRepository.findCourseByName(schoolId, studentDetailStg.getCourse());
                    if (!courses.isEmpty()) {
                        studentDetail.setCourse(courses.get(0));
                        studentAcademicbatch.setCourse(courses.get(0));
                        courseId = courses.get(0).getCourseId();
                    }

                    List<CourseYear> courseYears = courseYearRepository
                            .getCourseYearsByName(schoolId, courseId, studentDetailStg.getCourseYear());
                    if (!courseYears.isEmpty()) {
                        studentDetail.setCourseYear(courseYears.get(0));
                        studentAcademicbatch.setFromCourseYear(courseYears.get(0));
                        courseYearId = courseYears.get(0).getCourseYearId();
                    }
                    AcademicYear academicYear = academicYearRepository.getDefaultAcademicYear(schoolId, studentDetailStg.getAcademicYear());
                    if (academicYear != null) {
                        studentDetail.setAcademicYear(academicYear);
                        studentAcademicbatch.setAcademicYear(academicYear);
                        academicYearId = academicYear.getAcademicYearId();
                    }
                    List<GroupSection> groupSections = groupSectionRepository
                            .findByIsActiveTrue(schoolId, academicYearId, courseYearId,studentDetailStg.getSection());
                    if (!groupSections.isEmpty()) {
                        studentDetail.setGroupSection(groupSections.get(0));
                        studentAcademicbatch.setFromGroupSection(groupSections.get(0));
                    }


                    List<Batch> batches = batchRepository.getBatch(courseId);
                    if (!batches.isEmpty()) {
                        studentDetail.setBatch(batches.get(0));
                        studentAcademicbatch.setFromBatch(batches.get(0));
                    }
                    GeneralDetail studentStatusCat = new GeneralDetail();
                    studentStatusCat.setGeneralDetailId(generalDetailRepository.findByGeneralDetailCode("INCOLLEGE"));
                    studentDetail.setStudentStatus(studentStatusCat);
                    studentAcademicbatch.setStudentStatus(studentStatusCat);
                    studentDetail.setRollNumber(studentDetailStg.getRollNo());
                    user.setUserName(studentDetailStg.getHallTicketNumber());
                    user.setPassword(studentDetailStg.getRollNo());
                    studentDetail.setHallticketNumber(studentDetailStg.getHallTicketNumber());
                    studentDetail.setFirstName(studentDetailStg.getFirstName());
                    studentDetail.setMiddleName(studentDetailStg.getMiddleName());
                    user.setFirstName(studentDetailStg.getFirstName());
                    user.setLastName(studentDetailStg.getMiddleName());
                    studentDetail.setLastName(studentDetailStg.getLastName());
                    //Gender Details
                    GeneralDetail gender = new GeneralDetail();
                    gender.setGeneralDetailId(generalDetailRepository.findByGeneralDetailCode(studentDetailStg.getGender()));
                    studentDetail.setGender(gender);
                    if (studentDetailStg.getDateOfBirth() != null) {
                        Date dateOfBirth = dateFormat.parse(studentDetailStg.getDateOfBirth());
                        studentDetail.setDateOfBirth(dateOfBirth);
                    }
                    GeneralDetail quota = new GeneralDetail();
                    quota.setGeneralDetailId(generalDetailRepository.findByGeneralDetailCode(studentDetailStg.getQuota()));
                    studentDetail.setQuota(quota);
                    studentDetail.setMobile(studentDetailStg.getMobile());
                    studentDetail.setStdEmailId(studentDetailStg.getStudentEmailID());
                    user.setMobileNumber(studentDetailStg.getMobile());
                    if (studentDetailStg.getStudentEmailID() != null)
                        user.setEmail(studentDetailStg.getStudentEmailID());
                    else
                        user.setEmail("default@xyz.com");
                    studentDetail.setAadharCardNo(studentDetailStg.getAadhaarNo());
                    studentDetail.setFatherName(studentDetailStg.getFatherName());
                    studentDetail.setFatherMobileNo(studentDetailStg.getFatherMobile());
                    studentDetail.setMotherName(studentDetailStg.getMotherName());
                    studentDetail.setMotherMobileNo(studentDetailStg.getMotherMobile());
                    studentDetail.setPermanentAddress(studentDetailStg.getPermanentAddress());
                    Optional<City> city = cityRepository.findByCityName(studentDetailStg.getCity());
                    if (city.isPresent()) {
                        studentDetail.setPresentCity(city.get());
                        studentDetail.setPremenentCity(city.get());
                    }
                    Optional<District> district = districtRepository.
                            findByDistrictName(studentDetailStg.getDistrict());
                    if (district.isPresent()) {
                        studentDetail.setPermanentDistrict(district.get());
                        studentDetail.setPresentDistrict(district.get());
                    }
                    String createdDate = dateFormat.format(new Date());
                    Date todaysDate = dateFormat.parse(createdDate);
                    studentDetail.setCreatedDt(todaysDate);
                    studentDetail.setIsActive(Boolean.TRUE);
                    studentAcademicbatch.setFromDate(todaysDate);
                    studentAcademicbatch.setCreatedDt(todaysDate);
                    studentAcademicbatch.setIsActive(Boolean.TRUE);
                    user.setCreatedDt(todaysDate);
                    user.setIsActive(Boolean.TRUE);
                    user.setIsEditable(Boolean.TRUE);
                    user.setIsReset(Boolean.FALSE);
                    studentAcademicbatch.setStudentDetail(studentDetail);
                    studentDetailList.add(studentDetail);
                    deleteList.add(studentDetailStg);
                    academicBatchList.add(studentAcademicbatch);
                    usersList.add(user);
                }
            }
            studentDetailRepository.saveAll(studentDetailList);
            studentAcademicBatchesRepository.saveAll(academicBatchList);
            //userRepository.saveAll(usersList);
            studentDetailStgRepository.deleteAll(deleteList);
            List<StudentDetailDTO> studentDetailStgDTOList = studentDetailMapper.convertStudentDetailToStudentDetailDTO(studentDetailList);
            response = new ApiResponse<>("Data", studentDetailStgDTOList);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Error while processing the Student Data:: ");
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }
        Long endTime = System.currentTimeMillis();
        Long totalTime = endTime - startTime;
        LOGGER.info("StudentDetailStgServiceImpl.processStgEmployeeDetails() total time: " + totalTime);
        return response;
    }

    private Workbook getWorkbook(MultipartFile excelFile, Workbook workbook) {
        try {
            if (excelFile != null) {
                String originalFileName = excelFile.getOriginalFilename();
                if (originalFileName != null && originalFileName.length() > 0) {
                    switch (originalFileName.substring(originalFileName.lastIndexOf('.') + 1,
                            originalFileName.length())) {
                        case "xls":
                            try {
                                workbook = WorkbookFactory.create(excelFile.getInputStream());
                            } catch (org.apache.poi.openxml4j.exceptions.InvalidFormatException ie) {
                                LOGGER.error("Malformed Excel");
                                throw new IOException();
                            }
                            break;
                        case "xlsx":
                            try {
                                workbook = WorkbookFactory.create(excelFile.getInputStream());
                            } catch (org.apache.poi.openxml4j.exceptions.InvalidFormatException ie) {
                                LOGGER.error("Malformed Excel");
                                throw new IOException();
                            }
                            break;
                        default:
                            LOGGER.error("File type is not  recognized  Excell type");
                            throw new IOException();
                    }

                } else {
                    LOGGER.error("Can Not Read File Name");
                    throw new IOException();
                }
            } else {
                LOGGER.error("Did not select a file");
                throw new IOException();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return workbook;
    }
}
