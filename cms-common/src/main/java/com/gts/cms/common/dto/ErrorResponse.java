package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorResponse extends ApiResponse<ErrorResponse> implements Serializable{

	private static final long serialVersionUID = 1L;
	private List<FieldErrorResource> fieldErrors;

	public ErrorResponse() {
		super();
	}

	public ErrorResponse(boolean success, String message, Integer statusCode,List<FieldErrorResource> fieldErrors) {
		super(success,message, statusCode);
		this.fieldErrors = fieldErrors;
	}

	public List<FieldErrorResource> getFieldErrors() {
		return fieldErrors;
	}

	public void setFieldErrors(List<FieldErrorResource> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}

}
