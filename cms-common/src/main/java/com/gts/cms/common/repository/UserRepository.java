package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.UserRepositoryCustom;
import com.gts.cms.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * created by Raghu on 02/09/2018
 * 
 */
@Repository

public interface UserRepository extends JpaRepository<User, Long>, UserRepositoryCustom {
	User findByUserNameOrEmail(String userName, String email);
	
	User findByUserName(String userName);

	@Query("SELECT u FROM User u"
			+ " WHERE ( u.userName = :usernameOrEmail or u.email=:usernameOrEmail )"
			+ " AND u.isActive = true"
	)
	User findByUserNameOrEmail(@Param("usernameOrEmail")String usernameOrEmail);
	
	
	@Query("SELECT u FROM User u"
			+ " WHERE ( u.userName = :usernameOrEmail or u.email=:usernameOrEmail )"
			+ " AND ( u.password = :password)"
			+ " AND u.isActive = true"
			)
	User findByUserNameAndPassword(@Param("usernameOrEmail")String usernameOrEmail,@Param("password") String password);

	@Query("SELECT u FROM User u"
			+ " WHERE ( u.userName IN (:rollNumber))"
			+ " AND u.isActive = true"
			)
	User findByRollNumber(@Param("rollNumber") String rollNumber);
	
	
	@Query(" SELECT u FROM User u "
			+ " where 1=1 "
			+ " AND u.isActive =true " 
			+ " AND(:userId is null or u.userId=:userId) "
			+ " AND(:userTypeId is null or u.userType.userTypeId=:userTypeId) "
			+ " ORDER BY u.createdDt DESC "
			)
	Page<User> findByUserDetailsPagination(@Param("userId") Long userId,
											@Param("userTypeId") Long userTypeId,Pageable pageable);
	
	/*SELECT u.*
	FROM t_m_users u
	WHERE 1=1
	AND u.`fk_user_type_id`=10
	AND u.`is_active`=1
	*/
	@Query("SELECT u.userId,u.firstName,u.email,u.mobileNumber FROM User u"
			+ " WHERE 1=1"
			+ " AND(:userTypeId is null or u.userType.userTypeId=:userTypeId)"
			+ " AND u.isActive=true ")
	List<Object[]>  smsToLimitUser(@Param("userTypeId") Long userTypeId);
	
	
}
