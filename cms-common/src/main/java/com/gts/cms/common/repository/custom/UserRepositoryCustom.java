package com.gts.cms.common.repository.custom;

import com.gts.cms.entity.User;

import java.util.List;

/**
 * created by Raghu on 02/09/2018 
 * 
*/
public interface UserRepositoryCustom {
	User getUserDetails(Long userId, Boolean status);
	
	public List<User> findUsersbyUserType(Boolean status, Long userTypeId,Long userId);

	default public List<User> searchUserDetails(Long schoolId, Long userTypeId, String firstName, String lastName, String userName, Boolean status){
		return null;
		
	}

	
}
