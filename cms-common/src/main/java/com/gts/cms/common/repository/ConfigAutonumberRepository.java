package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.ConfigAutonumberRepositoryCustom;
import com.gts.cms.entity.ConfigAutonumber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ConfigAutonumberRepository
		extends JpaRepository<ConfigAutonumber, Long>, ConfigAutonumberRepositoryCustom {

	public List<ConfigAutonumber> findByIsActiveTrue();

	public Optional<ConfigAutonumber> findByAutoconfigIdAndIsActiveTrue(Long autoconfigId);
	
	public ConfigAutonumber findConfigAutoNumberByCode(String code);

}
