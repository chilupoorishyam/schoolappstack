package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.CountryDTO;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.Country;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class CountryMapper implements BaseMapper<Country, CountryDTO> {

	@Override
	public CountryDTO convertEntityToDTO(Country country) {
		CountryDTO countryDTO = null;
		if (country != null) {
			countryDTO = new CountryDTO();
			countryDTO.setCountryId(country.getCountryId());
			countryDTO.setCountryName(country.getCountryName());
			countryDTO.setCountryCode(country.getCountryCode());
			countryDTO.setCreatedUser(country.getCreatedUser());
			countryDTO.setIsActive(country.getIsActive());
		}
		return countryDTO;
	}

	@Override
	public Country convertDTOtoEntity(CountryDTO countryDto, Country country) {
		if (null == country) {
			country = new Country();
			country.setIsActive(Boolean.TRUE);
			country.setCreatedDt(new Date());
			country.setCreatedUser(SecurityUtil.getCurrentUser());
		}
		country.setCountryId(countryDto.getCountryId());
		country.setCountryName(countryDto.getCountryName());
		country.setCountryCode(countryDto.getCountryCode());

		country.setUpdatedDt(new Date());
		country.setUpdatedUser(SecurityUtil.getCurrentUser());
		return country;
	}

	public List<Country> convertCountryDTOListToCountryList(List<CountryDTO> countryDTOList) {
		List<Country> countryList = new ArrayList<>();
		countryDTOList.forEach(countryDTO -> countryList.add(convertDTOtoEntity(countryDTO, null)));
		return countryList;

	}

	@Override
	public List<CountryDTO> convertEntityListToDTOList(List<Country> countryList) {
		List<CountryDTO> countryDTOList = new ArrayList<>();
		countryList.forEach(country -> countryDTOList.add(convertEntityToDTO(country)));
		return countryDTOList;
	}

	@Override
	public List<Country> convertDTOListToEntityList(List<CountryDTO> dtoList) {
		// TODO Auto-generated method stub
		return null;
	}
}
