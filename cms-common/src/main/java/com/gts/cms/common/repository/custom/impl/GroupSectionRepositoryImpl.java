package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.GroupSectionRepositoryCustom;
import com.gts.cms.entity.GroupSection;
import com.gts.cms.entity.QGroupSection;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class GroupSectionRepositoryImpl extends QuerydslRepositorySupport implements GroupSectionRepositoryCustom {
	public GroupSectionRepositoryImpl() {
		super(GroupSection.class);
	}

	@Override
	public List<GroupSection> getGroupSections(Long courseYearId, Long academicYearId) {
		JPQLQuery<GroupSection> query = from(QGroupSection.groupSection)
				.where(QGroupSection.groupSection.courseYear.courseYearId.eq(courseYearId)
						.and(QGroupSection.groupSection.academicYear.academicYearId.eq(academicYearId)));
		List<GroupSection> groupSectionList = query.fetch();
		return groupSectionList;
	}

	@Override
	public List<GroupSection> getGroupSectionByCourseId(Long courseId) {
		return null;
	}


}
