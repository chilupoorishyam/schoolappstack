package com.gts.cms.common.student.mapper;

import com.gts.cms.entity.StudentActivitiesDetail;
import com.gts.cms.entity.StudentDetail;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.student.dto.StudentActivitiesDetailDTO;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.StudentActivitiesDetail;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class StudentActivitiesDetailMapper implements BaseMapper<StudentActivitiesDetail, StudentActivitiesDetailDTO> {
	
	@Override
	public StudentActivitiesDetail convertDTOtoEntity(StudentActivitiesDetailDTO dtoObject,StudentActivitiesDetail entityObject) {
		if(null==entityObject) {
			entityObject=new StudentActivitiesDetail();
			entityObject.setCreatedDt(new Date());
			entityObject.setCreatedUser(SecurityUtil.getCurrentUser());
		}
		
		/*entityObject.setIsActive(dtoObject.getIsActive());
		
		if(entityObject.getIsActive()==null) {
			entityObject.setIsActive(Boolean.TRUE);
		}*/
		
		entityObject.setStudentActivityId(dtoObject.getStudentActivityId());

		entityObject.setLevel(dtoObject.getLevel());

		entityObject.setParticulars(dtoObject.getParticulars());

		entityObject.setSponsoredBy(dtoObject.getSponsoredBy());
		
		if(dtoObject.getStudentId() != null) {
			StudentDetail studentDetail = new StudentDetail();
			studentDetail.setStudentId(dtoObject.getStudentId());
			studentDetail.setFirstName(dtoObject.getFirstName());
			entityObject.setStudentDetail(studentDetail);
		}

		entityObject.setUpdatedDt(new Date());
		entityObject.setUpdatedUser(SecurityUtil.getCurrentUser());
		
		return entityObject;
	}
	
	@Override
	public StudentActivitiesDetailDTO convertEntityToDTO(StudentActivitiesDetail entityObject) {
		StudentActivitiesDetailDTO dtoObject = null;
		if(entityObject != null) {
			dtoObject = new StudentActivitiesDetailDTO();
			
			dtoObject.setStudentActivityId(entityObject.getStudentActivityId());
			dtoObject.setCreatedDt(entityObject.getCreatedDt());
			dtoObject.setCreatedUser(entityObject.getCreatedUser());
			dtoObject.setLevel(entityObject.getLevel());
			dtoObject.setParticulars(entityObject.getParticulars());
			dtoObject.setSponsoredBy(entityObject.getSponsoredBy());
			dtoObject.setUpdatedDt(entityObject.getUpdatedDt());
			dtoObject.setUpdatedUser(entityObject.getUpdatedUser());
			dtoObject.setIsActive(entityObject.getIsActive());
			dtoObject.setReason(entityObject.getReason());

			if(entityObject.getStudentDetail() != null) {
				dtoObject.setStudentId(entityObject.getStudentDetail().getStudentId());
				//dtoObject.setFirstName(entityObject.getStudentDetail().getFirstName());
			}

		}
		return dtoObject;
	}
	
	@Override
	public List<StudentActivitiesDetail> convertDTOListToEntityList(List<StudentActivitiesDetailDTO> dtoObjectList) {
		List<StudentActivitiesDetail> entityObjectList = new ArrayList<>();
		dtoObjectList
				.forEach(dtoObject -> entityObjectList.add(convertDTOtoEntity(dtoObject, null)));
		return entityObjectList;
	}

	@Override
	public List<StudentActivitiesDetailDTO> convertEntityListToDTOList(List<StudentActivitiesDetail> entityObjectList) {
		List<StudentActivitiesDetailDTO> dtoObjectList = new ArrayList<>();
		entityObjectList.forEach(entityObject -> dtoObjectList.add(convertEntityToDTO(entityObject)));
		return dtoObjectList;
	}
}
