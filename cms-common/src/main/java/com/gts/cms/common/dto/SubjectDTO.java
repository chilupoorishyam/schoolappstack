package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

public class SubjectDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long subjectId;
	@NotNull(message = "SchoolId is required.")
	private Long schoolId;
	private String schoolName;
	private String schoolCode;
	private Long courseId;
	private String courseName;
	private String courseCode;
	private String subjectName;

	private String subjectCode;

	private Integer orderNo;
	@NotNull(message = "SubjectTypeId is required.")
	private Long subjectTypeId;
	private String subjectTypeName;
	@NotNull(message = "SubjectCategoryId is required.")
	private Long subjectCategoryId;
	private String subjectCategoryName;

	private Integer subCredits;

	private Integer subCreditHrs;
	@NotNull(message = "Short Name is required.")
	private String shortName;

	private Boolean isLanguage;
	@NotNull(message = "Is Active is required.")
	private Boolean isActive;

	private String reason;

	private Date createdDt;

	private Long createdUser;
	private Date updatedDt;

	private Long updatedUser;
	private Long subjectRegulationId;

	private Long examTimetableDetId;
	private Date examDate;
	private Long examTimetableId;
	private Long examTypeCatId;
	private String examTypeCatDisplayName;
	private String examTypeCatCode;
	public Long getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getSubjectCode() {
		return subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	public Integer getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(Integer orderNo) {
		this.orderNo = orderNo;
	}

	public Long getSubjectTypeId() {
		return subjectTypeId;
	}

	public void setSubjectTypeId(Long subjectTypeId) {
		this.subjectTypeId = subjectTypeId;
	}

	public String getSubjectTypeName() {
		return subjectTypeName;
	}

	public void setSubjectTypeName(String subjectTypeName) {
		this.subjectTypeName = subjectTypeName;
	}

	public Long getSubjectCategoryId() {
		return subjectCategoryId;
	}

	public void setSubjectCategoryId(Long subjectCategoryId) {
		this.subjectCategoryId = subjectCategoryId;
	}

	public String getSubjectCategoryName() {
		return subjectCategoryName;
	}

	public void setSubjectCategoryName(String subjectCategoryName) {
		this.subjectCategoryName = subjectCategoryName;
	}

	public Integer getSubCredits() {
		return subCredits;
	}

	public void setSubCredits(Integer subCredits) {
		this.subCredits = subCredits;
	}

	public Integer getSubCreditHrs() {
		return subCreditHrs;
	}

	public void setSubCreditHrs(Integer subCreditHrs) {
		this.subCreditHrs = subCreditHrs;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public Boolean getIsLanguage() {
		return isLanguage;
	}

	public void setIsLanguage(Boolean isLanguage) {
		this.isLanguage = isLanguage;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Long getSubjectRegulationId() {
		return subjectRegulationId;
	}

	public void setSubjectRegulationId(Long subjectRegulationId) {
		this.subjectRegulationId = subjectRegulationId;
	}



	public Long getExamTimetableDetId() {
		return examTimetableDetId;
	}

	public void setExamTimetableDetId(Long examTimetableDetId) {
		this.examTimetableDetId = examTimetableDetId;
	}

	public Long getExamTimetableId() {
		return examTimetableId;
	}

	public void setExamTimetableId(Long examTimetableId) {
		this.examTimetableId = examTimetableId;
	}

	public Long getExamTypeCatId() {
		return examTypeCatId;
	}

	public void setExamTypeCatId(Long examTypeCatId) {
		this.examTypeCatId = examTypeCatId;
	}

	public String getExamTypeCatDisplayName() {
		return examTypeCatDisplayName;
	}

	public void setExamTypeCatDisplayName(String examTypeCatDisplayName) {
		this.examTypeCatDisplayName = examTypeCatDisplayName;
	}

	public String getExamTypeCatCode() {
		return examTypeCatCode;
	}

	public void setExamTypeCatCode(String examTypeCatCode) {
		this.examTypeCatCode = examTypeCatCode;
	}

	public Date getExamDate() {
		return examDate;
	}

	public void setExamDate(Date examDate) {
		this.examDate = examDate;
	}
}
