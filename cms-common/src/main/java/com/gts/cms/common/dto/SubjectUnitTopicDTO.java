package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

public class SubjectUnitTopicDTO implements Serializable {
private static final long serialVersionUID = 1L;
private Long subjectUnitTopicId;
private Date createdDt;
private Long createdUser;
private Integer fromPage;
private Boolean isActive;
private Integer noOfPeriods;
private String reason;
private Integer sortOrder;
private Integer toPage;
private String topicDescription;
@NotNull(message="Topic Name is required")
private String topicName;
private Date updatedDt;
private Long updatedUser;
@NotNull(message="Subject Book Id is required")
private Long subBookId;
@NotNull(message="Subject Unit Id is required")
private Long subjectUnitsId;
private String unitCode;
private String unitName;
@NotNull(message="School Id is required")
private Long schoolId;
private String schoolCode;
private String schoolName;
private Long subjectRegulationId;

public void setSubjectUnitTopicId(Long subjectUnitTopicId) {
 this.subjectUnitTopicId = subjectUnitTopicId;
}
public Long getSubjectUnitTopicId(){
 return subjectUnitTopicId;
}
public void setCreatedDt(Date createdDt) {
 this.createdDt = createdDt;
}
public Date getCreatedDt(){
 return createdDt;
}
public void setCreatedUser(Long createdUser) {
 this.createdUser = createdUser;
}
public Long getCreatedUser(){
 return createdUser;
}
public void setFromPage(Integer fromPage) {
 this.fromPage = fromPage;
}
public Integer getFromPage(){
 return fromPage;
}
public void setIsActive(Boolean isActive) {
 this.isActive = isActive;
}
public Boolean getIsActive(){
 return isActive;
}
public void setNoOfPeriods(Integer noOfPeriods) {
 this.noOfPeriods = noOfPeriods;
}
public Integer getNoOfPeriods(){
 return noOfPeriods;
}
public void setReason(String reason) {
 this.reason = reason;
}
public String getReason(){
 return reason;
}
public void setSortOrder(Integer sortOrder) {
 this.sortOrder = sortOrder;
}
public Integer getSortOrder(){
 return sortOrder;
}
public void setToPage(Integer toPage) {
 this.toPage = toPage;
}
public Integer getToPage(){
 return toPage;
}
public void setTopicDescription(String topicDescription) {
 this.topicDescription = topicDescription;
}
public String getTopicDescription(){
 return topicDescription;
}
public void setTopicName(String topicName) {
 this.topicName = topicName;
}
public String getTopicName(){
 return topicName;
}
public void setUpdatedDt(Date updatedDt) {
 this.updatedDt = updatedDt;
}
public Date getUpdatedDt(){
 return updatedDt;
}
public void setUpdatedUser(Long updatedUser) {
 this.updatedUser = updatedUser;
}
public Long getUpdatedUser(){
 return updatedUser;
}
public void setSubBookId(Long subBookId) {
 this.subBookId = subBookId;
}
public Long getSubBookId(){
 return subBookId;
}
public void setSubjectUnitsId(Long subjectUnitsId) {
 this.subjectUnitsId = subjectUnitsId;
}
public Long getSubjectUnitsId(){
 return subjectUnitsId;
}
public void setUnitCode(String unitCode) {
 this.unitCode = unitCode;
}
public String getUnitCode(){
 return unitCode;
}
public void setUnitName(String unitName) {
 this.unitName = unitName;
}
public String getUnitName(){
 return unitName;
}
public void setSchoolId(Long schoolId) {
 this.schoolId = schoolId;
}
public Long getSchoolId(){
 return schoolId;
}
public void setSchoolCode(String schoolCode) {
 this.schoolCode = schoolCode;
}
public String getSchoolCode(){
 return schoolCode;
}
public void setSchoolName(String schoolName) {
 this.schoolName = schoolName;
}
public String getSchoolName(){
 return schoolName;
}
public void setSubjectRegulationId(Long subjectRegulationId) {
 this.subjectRegulationId = subjectRegulationId;
}
public Long getSubjectRegulationId(){
 return subjectRegulationId;
}
}