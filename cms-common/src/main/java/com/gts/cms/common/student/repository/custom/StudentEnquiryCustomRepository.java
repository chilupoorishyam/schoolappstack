package com.gts.cms.common.student.repository.custom;

import com.gts.cms.entity.StudentEnquiry;

import java.util.List;

public interface StudentEnquiryCustomRepository {

	List<StudentEnquiry> findByStudentData(String studentData);

	List<StudentEnquiry> findAllEnquiryForms(Long schoolId);

	List<StudentEnquiry> findAllEnquiryForms(Long schoolId, String status);
}
