package com.gts.cms.common.repository.custom;

import com.gts.cms.entity.AcademicYear;

import java.util.List;

/**
 * created by Naveen on 04/09/2018
 * 
 */
public interface AcademicYearRepositoryCustom {
	Long deleteAcademicYear(Long academicYearId);

	AcademicYear findByAcademicYearIdAndIsActiveTrue(Long academicYearId);

	List<AcademicYear> findByOrganizationIdAndSchoolIdAndIsActiveTrue(Long schoolId);

}
