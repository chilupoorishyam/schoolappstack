package com.gts.cms.common.enums;

public enum FCMApiKey {
	PRODAPIKEY("AAAAbrXnCrA:APA91bHBuoI5e9_C-lLT9EtJZXbvDGtCqANlERlAywKqxi_UTa39KVLGJBkR8bPs9xTZozx5_nT4ooa664FD7hrWwcGN7APErgR4vroopeH5-pAovSFNe_p2IviO5pLtWt1y5AHaZjWm"),
	//DEVAPIKEY("AAAAc4eQnRU:APA91bE0QtjmVr7z3Ntctpp_tuP0QIc6GR98xGLRIfx4K98tplDZwk5Fu0ocHKE7zgI_4oKhHhbmo4gBMXqnkWjoJXRPHtnTdawXQETo9G6wPi096K8C-oXsjH1ucDhhh-LkJOXPBuyO");
	DEVAPIKEY("AAAAoSwb8EU:APA91bEWs8biTWjIBMUkv-qjzHTv2bn6GkTgg8L7NfB3G6drHkadeP3atFprTJMFf7LMNxwJh6JF_okoZeaxIPpSwNmOmz-VeddURRe27oVtYemCl-C6HjzBAsXMnQ2KRtAmwQhhhOl1");
	
	private String value;

	private FCMApiKey(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
