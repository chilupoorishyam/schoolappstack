package com.gts.cms.common.service.impl;

import com.gts.cms.common.service.GeneralDetailService;
import org.springframework.stereotype.Service;

/**
 * The Class GeneralDetailServiceImpl.
 */
@Service
public class GeneralDetailServiceImpl implements GeneralDetailService {

	/*private static final Logger LOGGER = Logger.getLogger(GeneralDetailServiceImpl.class);

	@Autowired
	private GeneralDetailRepository generalDetailRepository;

	@Autowired
	private GeneralDetailMapper generalDetailMapper;

	@Override
	public ApiResponse<Long> addGeneralDetails(GeneralDetailDTO generalDetailDTO) {
		ApiResponse<Long> response = null;
		try {
			GeneralDetail generalDetail = generalDetailMapper.convertDTOtoEntity(generalDetailDTO,Status.ACTIVE.getId());
			generalDetail.setCreatedDt(new Date());
			generalDetail.setCreatedUser(SecurityUtil.getCurrentUser());
			generalDetail = generalDetailRepository.save(generalDetail);
			if (generalDetail != null && generalDetail.getGeneralDetailId() != 0) {
				response = new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(), generalDetail.getGeneralDetailId(),
						HttpStatus.OK.value());
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occured while adding GeneralDetail : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;
	}

	@Override
	public ApiResponse<GeneralDetailDTO> getGeneralDetailsDetails(Long generalDetailId) {
		ApiResponse<GeneralDetailDTO> response = null;
		if (null == generalDetailId) {
			throw new ApiException(Messages.RECORD_ID_MUST_NOT_NULL.getValue());
		}
		try {
			GeneralDetail generalDetail = generalDetailRepository.findByGeneralDetailIdAndIsActiveTrue(generalDetailId);
			if (null != generalDetail) {
				GeneralDetailDTO generalDetailDto = generalDetailMapper
						.convertEntityToDTO(generalDetail);
				response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), generalDetailDto);
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
						HttpStatus.OK.value());
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occured while getting generalDetailDetails : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;
	}

	@Override
	public ApiResponse<Long> updateGeneralDetails(GeneralDetailDTO generalDetailDTO) {
		ApiResponse<Long> response = null;
		try {
			GeneralDetail generalDetail = generalDetailMapper.convertDTOtoEntity(generalDetailDTO,Status.INACTIVE.getId());
			generalDetail = generalDetailRepository.save(generalDetail);
			if (generalDetail != null && generalDetail.getGeneralDetailId() != 0) {
				response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), generalDetail.getGeneralDetailId(),
						HttpStatus.OK.value());
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.UPDATE_FAILURE.getValue(), HttpStatus.OK.value());
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occured while updating GeneralDetail : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;

	}
	
	@Override
	public ApiResponse<List<GeneralDetailDTO>> getAllGeneralDetailsDetails(Long generalMasterId) {
		ApiResponse<List<GeneralDetailDTO>> response = null;
		try {
			List<GeneralDetail> generalDetailList =generalDetailRepository.findByGeneralMasterIdAndIsActiveTrue(generalMasterId);
			if (!CollectionUtils.isEmpty(generalDetailList)) {
				List<GeneralDetailDTO> generalDetailListDto = generalDetailMapper
						.convertEntityListToDTOList(generalDetailList);
				response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), generalDetailListDto);
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
						HttpStatus.OK.value());
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occured while getting GeneralDetail : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}
		return response;
	}

	
	@Override
	public ApiResponse<List<GeneralDetailDTO>> getAllGeneralDetailsDetailsByCode(String generalMasterCode) {
		ApiResponse<List<GeneralDetailDTO>> response = null;
		if (null == generalMasterCode) {
			throw new ApiException(Messages.INVALIDREQUEST_MESSAGE.getValue());
		}
		try {
			List<GeneralDetail> generalDetailList =generalDetailRepository.findByGeneralMasterCode(generalMasterCode);
			if (!CollectionUtils.isEmpty(generalDetailList)) {
				List<GeneralDetailDTO> generalDetailListDto = generalDetailMapper
						.convertEntityListToDTOList(generalDetailList);
				response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), generalDetailListDto);
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
						HttpStatus.OK.value());
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occured while getting GeneralDetail : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}
		return response;
	}
	
	
	@Override
	@Transactional
	public ApiResponse<Long> deleteGeneralDetails(Long generalDetailId) {
		ApiResponse<Long> response = null;
		if (null == generalDetailId) {
			throw new ApiException(Messages.RECORD_ID_MUST_NOT_NULL.getValue());
		}
		try {
			Long deleteCount = generalDetailRepository.deleteGeneralDetail(generalDetailId);
			if (deleteCount > 0) {
				response = new ApiResponse<>(Messages.DELETE_SUCCESS.getValue(), generalDetailId,
						HttpStatus.OK.value());
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.DELETE_FAILURE.getValue(), HttpStatus.OK.value());
			}

		} catch (Exception e) {
			LOGGER.error("Exception Occured while deleting GeneralDetail : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;
	}*/

}
