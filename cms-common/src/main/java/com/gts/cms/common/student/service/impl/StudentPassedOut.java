package com.gts.cms.common.student.service.impl;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.StudentAcademicbatchDTO;
import com.gts.cms.common.dto.StudentDetainRequestDTO;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.service.report.CommonUtils;
import com.gts.cms.common.student.repository.StudentAcademicBatchesRepository;
import com.gts.cms.common.student.repository.StudentDetailRepository;
import com.gts.cms.entity.GeneralDetail;
import com.gts.cms.entity.StudentAcademicbatch;
import com.gts.cms.entity.StudentDetail;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class StudentPassedOut {
	private static final Logger LOGGER = Logger.getLogger(StudentPassedOut.class);
	@Autowired
	StudentAcademicBatchesRepository studentAcademicBatchesRepository;
	@Autowired
	StudentDetailRepository studentDetailRepository;


	// @Override
	public ApiResponse<?> passedOutStudent(@Valid List<StudentDetainRequestDTO> studentDetainRequestDTO) {
		LOGGER.info("StudentPassedOut.passedOutStudent()");
		ApiResponse<?> response = null;
		int flag = 0;
		List<StudentAcademicbatch> studentAcademicbatchs = null;
		Integer value = null;
		StudentDetail studentDetail = null;
		StudentAcademicbatch studentAcademicbatch = null;
		List<StudentAcademicbatch> stuAcademicbatchsExist = new ArrayList<>();
		List<StudentAcademicbatchDTO> studentAcademicbatchDTOs = null;
		try {

			for (StudentDetainRequestDTO studentPassedOutDTO : studentDetainRequestDTO) {
				Date date = CommonUtils.getLessThanOneDate(studentPassedOutDTO.getToDate());
				// fromDate and toDate exist
				studentAcademicbatchs = studentAcademicBatchesRepository
						.findAlreadyExistWithDate(studentPassedOutDTO.getStudentId(), date);
				studentAcademicbatch = studentAcademicBatchesRepository.getDetails(studentPassedOutDTO.getSchoolId(),
						studentPassedOutDTO.getStudentId());
				if(studentAcademicbatch!=null) {
				value = CommonUtils.compareDate(studentAcademicbatch.getFromDate(), date);
				}
				// student academic batch exist
				if (value == -1) {
					stuAcademicbatchsExist.add(studentAcademicbatch);
				}
				// student academic batch exist
				if (!(CollectionUtils.isEmpty(studentAcademicbatchs))) {
					stuAcademicbatchsExist.addAll(studentAcademicbatchs);
				}
				if (CollectionUtils.isEmpty(studentAcademicbatchs) && studentAcademicbatch != null
						&& ((value == 0) || (value == 1))) {

					studentAcademicbatch.setSchool(studentAcademicbatch.getSchool());
					studentAcademicbatch.setStudentDetail(studentAcademicbatch.getStudentDetail());
					studentAcademicbatch.setCourse(studentAcademicbatch.getCourse());
					studentAcademicbatch.setAcademicYear(studentAcademicbatch.getAcademicYear());
					studentAcademicbatch.setFromCourseYear(studentAcademicbatch.getFromCourseYear());
					studentAcademicbatch.setToCourseYear(studentAcademicbatch.getToCourseYear());
					studentAcademicbatch.setFromBatch(studentAcademicbatch.getFromBatch());
					studentAcademicbatch.setToBatch(studentAcademicbatch.getToBatch());
					studentAcademicbatch.setFromGroupSection(studentAcademicbatch.getFromGroupSection());
					studentAcademicbatch.setToGroupSection(studentAcademicbatch.getToGroupSection());
					if (studentPassedOutDTO.getStudentStatusId() != null) {
						GeneralDetail studentStatus = new GeneralDetail();
						studentStatus.setGeneralDetailId(studentPassedOutDTO.getStudentStatusId());
						studentAcademicbatch.setStudentStatus(studentStatus);
					}
					studentAcademicbatch.setIsPromoted(studentAcademicbatch.getIsPromoted());
					studentAcademicbatch.setIsAlmuni(studentAcademicbatch.getIsAlmuni());
					studentAcademicbatch.setReason(studentAcademicbatch.getReason());
					studentAcademicbatch.setIsActive(studentAcademicbatch.getIsActive());
					studentAcademicbatch.setFromDate(studentAcademicbatch.getFromDate());
					studentAcademicbatch.setToDate(date);
					studentAcademicbatch.setCreatedDt(studentAcademicbatch.getCreatedDt());
					studentAcademicbatch.setCreatedUser(studentAcademicbatch.getCreatedUser());
					studentAcademicBatchesRepository.save(studentAcademicbatch);

					studentDetail = studentDetailRepository.findByStudentId(studentPassedOutDTO.getStudentId());

					if (studentPassedOutDTO.getStudentStatusId() != null) {
						GeneralDetail studentStatus = new GeneralDetail();
						studentStatus.setGeneralDetailId(studentPassedOutDTO.getStudentStatusId());
						studentDetail.setStudentStatus(studentStatus);
					}
					studentDetail.setUpdatedDt(new Date());
					studentDetailRepository.save(studentDetail);
					flag += 1;
				}
			}
			/*StudentAttendanceAndDates studentAttendanceAndDates = new StudentAttendanceAndDates();
			studentAcademicbatchDTOs = studentAcademicbatchMapper.convertEntityListToDTOList(stuAcademicbatchsExist);
			studentAttendanceAndDates.setStudentAcademicbatchDTOs(studentAcademicbatchDTOs);
			if (flag > 0) {
				response = new ApiResponse<>(Boolean.TRUE, Messages.UPDATE_SUCCESS.getValue(), studentAttendanceAndDates,
						HttpStatus.OK.value());
			} else {
				response = new ApiResponse<>(Boolean.FALSE,Messages.UPDATE_FAILURE.getValue(), studentAttendanceAndDates,
						HttpStatus.OK.value());
			}*/
			LOGGER.info("StudentPassedOut.passedOutStudent()==>executed");
		} catch (Exception e) {
			LOGGER.error("StudentPassedOut.passedOutStudent()"+e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;
	}
}
