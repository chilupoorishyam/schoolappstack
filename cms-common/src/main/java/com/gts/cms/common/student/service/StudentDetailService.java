package com.gts.cms.common.student.service;

import com.gts.cms.common.dto.*;
import com.gts.cms.common.student.dto.StudentDetailDTO;
import com.gts.cms.common.student.dto.StudentDetailFileUploadDTO;
import com.gts.cms.common.student.dto.StudentRollNumberDTO;

import javax.validation.Valid;
import java.util.List;
/**
 *created by sathish on 19/sep/2018
 */
public interface StudentDetailService {

	public ApiResponse<List<StudentDetailListDTO>> getStudentsList(Boolean status, Long schoolId, Long courseId,
                                                                   Long courseYearId, Long groupSectionId, Long academicYearId, String statusCode);
	
	public ApiResponse<List<StudentDetailListDTO>> getStudentsListByStructure(Boolean status, Long schoolId,
			Long academicYearId,Long feeStructureId);
	
	public ApiResponse<StudentDetailDTO> getStudentDetails(Long studentId, Long userId);
	
	public ApiResponse<List<StudentDetailDTO>> getParentDetails(Long parentId);
	
	/*	public ApiResponse<List<StudentDetailDTO>> getAllApplicationForms(String status);
	public ApiResponse<?> updateStudentDetails(StudentDetailDTO studentDetailDTO);
	public ApiResponse<?> deleteStudentAdmission(Long id);*/

	public ApiResponse<?> updateStudentsList(List<StudentDetailListDTO> studentApplicationDTO);
	
	public ApiResponse<StudentDetailDTO> updateStudentDetail(StudentDetailDTO studentDetailDTO);
	
	public ApiResponse<List<StudentDetailSearchDTO>> searchStudentDetails(String query, Long schoolId, Long academicYearId, Boolean status, Long courseId, Long courseYearId);
	
	public ApiResponse<StudentDetailFileUploadDTO> uploadStudentDetailUploadFiles(
			StudentDetailFileUploadDTO studentDetailFileUploadDTO);

	public ApiResponse<?> detainStudentDetail(List<StudentDetainRequestDTO> studentDetainRequestDTOs);

	public ApiResponse<List<StudentDetailListDTO>> getDetainStudentsList(Boolean status, Long schoolId, Long courseId,
			 Long courseYearId, Long groupSectionId, Long academicYearId);

	public ApiResponse<?> reAdmissionStudent(StudentDetainRequestDTO studentDetainRequestDTO);

	public ApiResponse<?> changeStudentSection(Long schoolId, Long studentId, Long groupSectionId);

	/*public ApiResponse<List<StudentDetailListDTO>> getFeeMappingStudentsList(StudentFeeRequestDTO studentFeeRequestDTO);*/
	
	/*public ApiResponse<List<StudentFeeListDTO>> getStudentFeeList(Boolean status,Long schoolId,Long courseId,Long courseGroupId,
			Long courseYearId,Long groupSectionId, Long feeStructureId, Boolean balance,
			Boolean paid,Integer page,Integer size, Long studentId,Long quotaId);
*/
	public ApiResponse<?> updateRollno(@Valid List<StudentRollNumberDTO> studentRollNumberDTO);

	public ApiResponse<List<StudentDetailListDTO>> getStudentsScholarshipList(Boolean status, Long schoolId,
			Long courseId, Long courseYearId, Long groupSectionId, Long academicYearId,
			Boolean isScholarship);

	public ApiResponse<?> discontinueStudentDetail(@Valid List<StudentDiscontinueRequestDTO> studentDiscontinueRequestDTOs);

	public ApiResponse<?> detainRecommended(@Valid List<StudentDetainRequestDTO> studentDetainRequestDTOs);

	public ApiResponse<?> assignSectionToStudent(List<StudentDetailListDTO> studentApplicationDTOs);

	public ApiResponse<?> passedOutStudent(@Valid List<StudentDetainRequestDTO> studentDetainRequestDTO);

	public ApiResponse<List<StudentDetailDTO>> addStudentsList(List<StudentDetailDTO> studentDetailDTO);

}
