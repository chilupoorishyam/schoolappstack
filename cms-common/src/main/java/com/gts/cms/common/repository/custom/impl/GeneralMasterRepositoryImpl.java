package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.GeneralMasterRepositoryCustom;
import com.gts.cms.entity.GeneralMaster;
import com.gts.cms.entity.QGeneralDetail;
import com.gts.cms.entity.QGeneralMaster;
import com.querydsl.core.dml.UpdateClause;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAUpdateClause;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class GeneralMasterRepositoryImpl extends QuerydslRepositorySupport implements GeneralMasterRepositoryCustom {

	public GeneralMasterRepositoryImpl() {
		super(GeneralMaster.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public GeneralMaster findByGeneralMasterIdAndIsActiveTrue(Long generalMasterId) {
		JPQLQuery<GeneralMaster> query = from(QGeneralMaster.generalMaster)
				.where(QGeneralMaster.generalMaster.generalMasterId.eq(generalMasterId).and(QGeneralMaster.generalMaster.isActive.eq(true)));
		return query.fetchOne();
	}

	@Override
	public Long deleteGeneralMaster(Long generalMasterId) {
		UpdateClause<JPAUpdateClause> updateCount = update(QGeneralMaster.generalMaster)
				.where(QGeneralMaster.generalMaster.generalMasterId.eq(generalMasterId)).set(QGeneralMaster.generalMaster.isActive, false);
		Long parent = updateCount.execute();
		UpdateClause<JPAUpdateClause> updateChildCount = update(QGeneralDetail.generalDetail)
				.where(QGeneralDetail.generalDetail.generalMaster.generalMasterId.eq(generalMasterId))
				.set(QGeneralDetail.generalDetail.isActive, false);
		Long child = updateChildCount.execute();
		return parent;
	}

	@Override
	public List<GeneralMaster> findByIsActiveTrue() {
		JPQLQuery<GeneralMaster> query = from(QGeneralMaster.generalMaster).where(QGeneralMaster.generalMaster.isActive.eq(true));
		query.orderBy(QGeneralMaster.generalMaster.createdDt.desc());
		return query.fetch();
	}

}
