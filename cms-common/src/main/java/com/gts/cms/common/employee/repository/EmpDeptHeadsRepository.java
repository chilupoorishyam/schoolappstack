package com.gts.cms.common.employee.repository;

import com.gts.cms.entity.EmpDeptHeads;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Genesis
 *
 */

@Repository
public interface EmpDeptHeadsRepository extends JpaRepository<EmpDeptHeads, Long>{
	
	@Query("SELECT e FROM EmpDeptHeads e"
			+ " WHERE (:deptId is null or e.department.departmentId = :deptId)"
			+ " AND e.isActive = true"
			)
	List<EmpDeptHeads> findByDeptIdAndIsActiveTrue(@Param("deptId") Long deptId);
	
	@Query("SELECT e FROM EmpDeptHeads e"
			+ " WHERE 1=1"
			+ " AND e.isActive = true"
			)
	EmpDeptHeads findByCourseGroupId();
	
	@Query("SELECT e FROM EmpDeptHeads e"
			+ " WHERE 1=1"
			+ " AND (:schoolId is null or e.school.schoolId = :schoolId)"
			+ " AND e.isActive = true"
			)
	EmpDeptHeads findByCourseGroupIdAndSchoolId(@Param("schoolId") Long schoolId);

	@Query("SELECT e FROM EmpDeptHeads e"
			+ " WHERE 1=1"
			+ " AND (:deptId is null or e.department.departmentId = :deptId)"
			+ " AND (:schoolId is null or e.school.schoolId=:schoolId)"
			+ " AND (:employeeId is  null or e.employeeDetail.employeeId =:employeeId)"
			+ " AND e.isActive = true"
			)
	EmpDeptHeads findByDeptIdAndSchoolId(@Param("deptId") Long deptId,@Param("schoolId") Long schoolId, Long employeeId);

}
