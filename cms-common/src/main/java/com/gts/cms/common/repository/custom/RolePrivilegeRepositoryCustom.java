package com.gts.cms.common.repository.custom;

import java.util.List;

import com.gts.cms.repo.dto.AuthorityRepoDTO;

/**
 * created by Naveen on 04/09/2018 
 * 
*/
public interface RolePrivilegeRepositoryCustom {
	List<AuthorityRepoDTO> getAuthoritiesForUser(Long roleId, Long orgId, Long schoolId);
}
