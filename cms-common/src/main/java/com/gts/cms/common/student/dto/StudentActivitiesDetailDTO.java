package com.gts.cms.common.student.dto;

import java.io.Serializable;
import java.util.Date;

public class StudentActivitiesDetailDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long studentActivityId;

	private Date createdDt;

	private Long createdUser;

	private String level;

	private String particulars;

	private String sponsoredBy;

	private Date updatedDt;

	private Long updatedUser;
	
	private Long studentId;
	private String firstName;
	private String middleName;
	private String lastName;
	
	private String reason;
	
	private Boolean isActive;
	
	private Long appEducationId;

	public Long getStudentActivityId() {
		return studentActivityId;
	}

	public void setStudentActivityId(Long studentActivityId) {
		this.studentActivityId = studentActivityId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getParticulars() {
		return particulars;
	}

	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}

	public String getSponsoredBy() {
		return sponsoredBy;
	}

	public void setSponsoredBy(String sponsoredBy) {
		this.sponsoredBy = sponsoredBy;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getAppEducationId() {
		return appEducationId;
	}

	public void setAppEducationId(Long appEducationId) {
		this.appEducationId = appEducationId;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
	
}