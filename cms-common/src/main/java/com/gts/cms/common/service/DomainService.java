package com.gts.cms.common.service;

import java.util.Map;

import com.gts.cms.common.dto.ApiResponse;

public interface DomainService {

	public ApiResponse<?> findOneEntity(String entityName, Map<String, String> params);

	public ApiResponse<?> findEntityList(String entityName, Map<String, String> params);

	public ApiResponse<?> createEntity(String entityName, Map<String, Object> wrappedRequestBody);

	public ApiResponse<?> deleteOneEntity(String entityName, Map<String, String> params);

	public ApiResponse<?> updateEntity(String entityName, Map<String, Object> wrappedRequestBody,Map<String, String> params);
	
	public ApiResponse<?> searchEntityList(String entityName, Map<String, String> params);

}
