package com.gts.cms.common.repository;

import com.gts.cms.common.dto.PageResponse;

public interface DomainRepository {
	Object findOneEntity(Class<?> entityClass, String queryString);

	PageResponse findEntityList(Class<?> entityClass, String queryString, int pageNumber, int pageSize);

	PageResponse searchEntityList(Class<?> entityClass, String queryString,String fields, int pageNumber, int pageSize);

	Object create(Object convertDTOtoEntity);

	Object deleteOneEntity(Object entityPatch, String query) throws Exception;

}
