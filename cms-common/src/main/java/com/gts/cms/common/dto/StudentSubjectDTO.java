package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

public class StudentSubjectDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long studentSubjectId;
	private Boolean isActive;
	private String reason;
	
	private Date createdDt;
	private Long createdUser;
	private Date updatedDt;
	private Long updatedUser;

	private Long schoolId;
	private String schoolName;
	private String schoolCode;
	
	private Long studentId;
	private String stdFirstName;
	
	private Long studentbatchId;
	private String batchName;

	
	private Long courseId;
	private String courseName;
	
	private Long courseYearId;
	private String courseYearName;
	private String courseYearCode;
	
	private Long sectionId;
	private String section;
	
	private Long academicYearId;
	private String academicYear;
	
	private Long subjectTypeId;
	private String subjectTypeCode;
	private String subjectTypeName;
	
	private Long subjectId;
	private String subjectName;
	private String subjectCode;
	private Integer subCredits;
	private String shortName;
	
	
	public Long getStudentSubjectId() {
		return studentSubjectId;
	}
	public void setStudentSubjectId(Long studentSubjectId) {
		this.studentSubjectId = studentSubjectId;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Date getCreatedDt() {
		return createdDt;
	}
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	public Long getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}
	public Date getUpdatedDt() {
		return updatedDt;
	}
	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}
	public Long getUpdatedUser() {
		return updatedUser;
	}
	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}
	public Long getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getSchoolCode() {
		return schoolCode;
	}
	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}
	public Long getStudentId() {
		return studentId;
	}
	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}
	public Long getStudentbatchId() {
		return studentbatchId;
	}
	public void setStudentbatchId(Long studentbatchId) {
		this.studentbatchId = studentbatchId;
	}
	public String getBatchName() {
		return batchName;
	}
	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public Long getCourseId() {
		return courseId;
	}
	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public Long getCourseYearId() {
		return courseYearId;
	}
	public void setCourseYearId(Long courseYearId) {
		this.courseYearId = courseYearId;
	}
	public String getCourseYearName() {
		return courseYearName;
	}
	public void setCourseYearName(String courseYearName) {
		this.courseYearName = courseYearName;
	}
	public String getCourseYearCode() {
		return courseYearCode;
	}
	public void setCourseYearCode(String courseYearCode) {
		this.courseYearCode = courseYearCode;
	}
	public Long getSectionId() {
		return sectionId;
	}
	public void setSectionId(Long sectionId) {
		this.sectionId = sectionId;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public Long getAcademicYearId() {
		return academicYearId;
	}
	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}
	public String getAcademicYear() {
		return academicYear;
	}
	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}
	public Long getSubjectTypeId() {
		return subjectTypeId;
	}
	public void setSubjectTypeId(Long subjectTypeId) {
		this.subjectTypeId = subjectTypeId;
	}
	
	public String getSubjectTypeCode() {
		return subjectTypeCode;
	}
	public void setSubjectTypeCode(String subjectTypeCode) {
		this.subjectTypeCode = subjectTypeCode;
	}
	public String getSubjectTypeName() {
		return subjectTypeName;
	}
	public void setSubjectTypeName(String subjectTypeName) {
		this.subjectTypeName = subjectTypeName;
	}
	public Long getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public String getSubjectCode() {
		return subjectCode;
	}
	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}
	public String getStdFirstName() {
		return stdFirstName;
	}
	public void setStdFirstName(String stdFirstName) {
		this.stdFirstName = stdFirstName;
	}
	public Integer getSubCredits() {
		return subCredits;
	}
	public void setSubCredits(Integer subCredits) {
		this.subCredits = subCredits;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
}
