package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.UserDTO;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.Organization;
import com.gts.cms.entity.School;
import com.gts.cms.entity.User;
import com.gts.cms.entity.Usertype;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class UserMapper implements BaseMapper<User, UserDTO> {
@Override
public UserDTO convertEntityToDTO(User user) {
	UserDTO userDTO = null;
	if (user != null) {
	userDTO = new UserDTO();
	  userDTO.setCreatedDt(user.getCreatedDt());
	  userDTO.setUpdatedDt(user.getUpdatedDt());
	  userDTO.setUpdatedUser(user.getUpdatedUser());
	  userDTO.setIsActive(user.getIsActive());
	  userDTO.setCreatedUser(user.getCreatedUser());
	  userDTO.setFirstName(user.getFirstName());
	  userDTO.setPassword(user.getPassword());
	  userDTO.setUserName(user.getUserName());
	  userDTO.setIsReset(user.getIsReset());
	  userDTO.setLastName(user.getLastName());
	  //userDTO.setUserTypeId(user.getUserTypeId());
	  userDTO.setPasswordExpDate(user.getPasswordExpDate());
	  userDTO.setResetPasswordCode(user.getResetPasswordCode());
	  userDTO.setPasswordAttempts(user.getPasswordAttempts());
	  userDTO.setUserId(user.getUserId());
	  userDTO.setIsEditable(user.getIsEditable());
	  userDTO.setReason(user.getReason());
	  userDTO.setMobileNumber(user.getMobileNumber());
	  userDTO.setEmail(user.getEmail());
	  if(user.getOrganization()!= null){
		  userDTO.setOrganizationId(user.getOrganization().getOrganizationId());
		  userDTO.setOrganizationName(user.getOrganization().getOrgName());
		  userDTO.setOrganizationCode(user.getOrganization().getOrgCode());
	  }
	  if(user.getSchool()!= null){
		  userDTO.setSchoolId(user.getSchool().getSchoolId());
		  userDTO.setSchoolName(user.getSchool().getSchoolName());
		  userDTO.setSchoolCode(user.getSchool().getSchoolCode());
	  }
	  if (user.getUserType() != null) {
			userDTO.setUserTypeId(user.getUserType().getUserTypeId());
			userDTO.setUserTypeCode(user.getUserType().getUserTypeCode());
			userDTO.setUserTypeName(user.getUserType().getUserTypeName());
		}
	  
	}
	return userDTO;
}
@Override
public User convertDTOtoEntity(UserDTO userDTO, User user) {
	if ( null == user ) {
	 user = new User();
	 user.setIsActive(Status.ACTIVE.getId());
	 user.setCreatedDt(new Date());
	 user.setCreatedUser(SecurityUtil.getCurrentUser());
	}else{
	 user.setIsActive(userDTO.getIsActive());
	}
	  user.setUpdatedDt(new Date());
	  user.setUpdatedUser(SecurityUtil.getCurrentUser());
	  user.setFirstName(userDTO.getFirstName());
	  //user.setPassword(MD5Hashing.md5EncyptionUsingHex(userDTO.getPassword()));
	  user.setPassword(userDTO.getPassword());
	  user.setUserName(userDTO.getUserName());
	  user.setIsReset(userDTO.getIsReset());
	  user.setLastName(userDTO.getLastName());
	  
	  user.setPasswordExpDate(userDTO.getPasswordExpDate());
	  user.setResetPasswordCode(userDTO.getResetPasswordCode());
	  user.setPasswordAttempts(userDTO.getPasswordAttempts());
	  user.setUserId(userDTO.getUserId());
	  user.setIsEditable(userDTO.getIsEditable());
	if(userDTO.getUserTypeId()!=null) {
		Usertype usertype=new Usertype();
		usertype.setUserTypeId(userDTO.getUserTypeId());
		user.setUserType(usertype);
	}
	 if(userDTO.getSchoolId()!= null){
		 School school = new School();
		 school.setSchoolId(userDTO.getSchoolId());
		 user.setSchool(school);
	 }
	 if(userDTO.getOrganizationId()!= null){
		 Organization organization = new Organization();
		 organization.setOrganizationId(userDTO.getOrganizationId());
		 user.setOrganization(organization);
	 }
	  user.setReason(userDTO.getReason());
	  user.setMobileNumber(userDTO.getMobileNumber());
	 
	  user.setEmail(userDTO.getEmail());
	return user;
}
@Override
public List<User> convertDTOListToEntityList(List<UserDTO> userDTOList) {
	List<User> userList = new ArrayList<>();
	 userDTOList.forEach(userDTO -> userList.add(convertDTOtoEntity(userDTO, null)));
	return userList;
}
@Override
public List<UserDTO> convertEntityListToDTOList(List<User> userList) {
	List<UserDTO> userDTOList = new ArrayList<>();
	 userList.forEach(user -> userDTOList.add(convertEntityToDTO(user)));
	return userDTOList;
}

}