package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.FinancialYearRepositoryCustom;
import com.gts.cms.entity.FinancialYear;
import com.gts.cms.entity.QFinancialYear;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class FinancialYearRepositoryImpl extends QuerydslRepositorySupport implements FinancialYearRepositoryCustom {
	public FinancialYearRepositoryImpl() {
		super(FinancialYear.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public Long deleteFinancialYear(Long financialYearId) {
		return update(QFinancialYear.financialYear1).where(QFinancialYear.financialYear1.financialYearId.eq(financialYearId))
				.set(QFinancialYear.financialYear1.isActive, false).execute();
	}

	@Override
	public List<FinancialYear> findByOrganizationIdAndSchoolIdAndIsActiveTrue(Long schoolId) {

		JPQLQuery<FinancialYear> query = from(QFinancialYear.financialYear1)
				.where(QFinancialYear.financialYear1.school.schoolId.eq(schoolId).and(QFinancialYear.financialYear1.isActive.eq(true)));
		List<FinancialYear> financialYearList = query.fetch();
		return financialYearList;

	}

	@Override
	public FinancialYear findByIdAndIsActiveTrue(Long financialYearId) {

		JPQLQuery<FinancialYear> query = from(QFinancialYear.financialYear1)
				.where(QFinancialYear.financialYear1.financialYearId.eq(financialYearId).and(QFinancialYear.financialYear1.isActive.eq(true)));
		FinancialYear financialYearList = query.fetchOne();
		return financialYearList;

	}
	
	@Override
	public FinancialYear findFinancialYearBySchoolId(Long schoolId) {

		JPQLQuery<FinancialYear> query = from(QFinancialYear.financialYear1)
				.where(QFinancialYear.financialYear1.school.schoolId.eq(schoolId)
						.and(QFinancialYear.financialYear1.isActive.eq(true))
						.and(QFinancialYear.financialYear1.isDefault.eq(true)));
		FinancialYear financialYearList = query.fetchOne();
		return financialYearList;

	}

}
