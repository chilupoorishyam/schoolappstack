package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

import javax.validation.constraints.NotNull;

public class StudentFeeRequestDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@NotNull(message="School is required")
	private Long schoolId;
	
	@NotNull(message="AcademicYear is required")
	private Long academicYearId;
	
	public Long getSchoolId() {
		return schoolId;
	}

	public Long getAcademicYearId() {
		return academicYearId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}



}
