package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.SchoolRepositoryCustom;
import com.gts.cms.entity.School;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * created by Naveen(Auto) on 02/09/2018
 * 
 */
@Repository
public interface SchoolRepository extends JpaRepository<School, Long>, SchoolRepositoryCustom {

	@Query("SELECT c.schoolCode FROM  School c" 
			+ " WHERE c.schoolId IN (:schoolIdList)"
			+ " AND c.isActive = true"
			)
	List<String> findSchoolDetails(@Param("schoolIdList") List<Long> schoolIdList);

	@Query("SELECT c FROM  School c"
			+ " WHERE 1=1 "
			+ " AND (:schoolName is null or upper(c.schoolName)=upper(:schoolName)) "
			+ " AND c.isActive = true"
	)
	List<School> findSchoolByName(@Param("schoolName") String schoolName);

	@Query("SELECT c FROM  School c"
			+ " WHERE 1=1 "
			+ " AND (:schoolId is null or  c.schoolId =:schoolId)"
			+ " AND c.isActive = true"
	)
	School findBySchool(Long schoolId);

	Optional<School> findBySchoolCodeAndIsActive(String schoolCode, Boolean isActive);

}
