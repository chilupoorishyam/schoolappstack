package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

public class StudentDetailSearchDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Long studentId;
	private String admissionNumber;
	private Date dateOfBirth;
	private String fatherName;
	private String firstName;
	private String guardianName;
	private String hallticketNumber;
	private Boolean isActive;
	private String lastName;
	private String middleName;
	private String mobile;
	private String motherName;
	private String rollNumber;
	private String stdEmailId;
	private Long groupSectionId;
	private Long batchId;
	private Long academicYearId;
	private String academicYear;
	private String studentPhotoPath;
	private Long schoolId;
	private String schoolCode;
	private String schoolName;
	private Long courseId;
	private String courseCode;
	private String groupCode;
	private Long courseYearId;
	private String courseYearCode;
	private String courseYearName;
	private String section;	
	private Long userId;
	
	private Long quotaId;
	private String quotaDisplayName;
	
	private Long studentStatusId;
	private String studentStatusCode;
	private String studentStatusDisplayName;
	private Boolean isLateral;
	private Long genderId;
	private String genderDsiplayName;
	private String genderCode;

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getAdmissionNumber() {
		return admissionNumber;
	}

	public void setAdmissionNumber(String admissionNumber) {
		this.admissionNumber = admissionNumber;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}


	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGuardianName() {
		return guardianName;
	}

	public void setGuardianName(String guardianName) {
		this.guardianName = guardianName;
	}

	public String getHallticketNumber() {
		return hallticketNumber;
	}

	public void setHallticketNumber(String hallticketNumber) {
		this.hallticketNumber = hallticketNumber;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	public String getRollNumber() {
		return rollNumber;
	}

	public void setRollNumber(String rollNumber) {
		this.rollNumber = rollNumber;
	}
	public String getStdEmailId() {
		return stdEmailId;
	}

	public void setStdEmailId(String stdEmailId) {
		this.stdEmailId = stdEmailId;
	}

	public Long getGroupSectionId() {
		return groupSectionId;
	}

	public void setGroupSectionId(Long groupSectionId) {
		this.groupSectionId = groupSectionId;
	}




	public Long getBatchId() {
		return batchId;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public Long getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}

	public String getStudentPhotoPath() {
		return studentPhotoPath;
	}

	public void setStudentPhotoPath(String studentPhotoPath) {
		this.studentPhotoPath = studentPhotoPath;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}


	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public Long getCourseYearId() {
		return courseYearId;
	}

	public void setCourseYearId(Long courseYearId) {
		this.courseYearId = courseYearId;
	}

	public String getCourseYearCode() {
		return courseYearCode;
	}

	public void setCourseYearCode(String courseYearCode) {
		this.courseYearCode = courseYearCode;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getCourseYearName() {
		return courseYearName;
	}

	public void setCourseYearName(String courseYearName) {
		this.courseYearName = courseYearName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getQuotaId() {
		return quotaId;
	}

	public void setQuotaId(Long quotaId) {
		this.quotaId = quotaId;
	}

	public String getQuotaDisplayName() {
		return quotaDisplayName;
	}

	public void setQuotaDisplayName(String quotaDisplayName) {
		this.quotaDisplayName = quotaDisplayName;
	}

	public String getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}

	public Long getStudentStatusId() {
		return studentStatusId;
	}

	public void setStudentStatusId(Long studentStatusId) {
		this.studentStatusId = studentStatusId;
	}

	public String getStudentStatusCode() {
		return studentStatusCode;
	}

	public void setStudentStatusCode(String studentStatusCode) {
		this.studentStatusCode = studentStatusCode;
	}

	public String getStudentStatusDisplayName() {
		return studentStatusDisplayName;
	}

	public void setStudentStatusDisplayName(String studentStatusDisplayName) {
		this.studentStatusDisplayName = studentStatusDisplayName;
	}

	public Boolean getIsLateral() {
		return isLateral;
	}

	public void setIsLateral(Boolean isLateral) {
		this.isLateral = isLateral;
	}

	public Long getGenderId() {
		return genderId;
	}

	public void setGenderId(Long genderId) {
		this.genderId = genderId;
	}

	public String getGenderDsiplayName() {
		return genderDsiplayName;
	}

	public void setGenderDsiplayName(String genderDsiplayName) {
		this.genderDsiplayName = genderDsiplayName;
	}

	public String getGenderCode() {
		return genderCode;
	}

	public void setGenderCode(String genderCode) {
		this.genderCode = genderCode;
	}
}