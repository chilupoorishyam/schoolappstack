package com.gts.cms.common.repository.custom;

import com.gts.cms.entity.GroupSection;

import java.util.List;

public interface GroupSectionRepositoryCustom {
	List<GroupSection> getGroupSections(Long courseYearId, Long academicYearId);
	List<GroupSection> getGroupSectionByCourseId(Long courseId);
}
