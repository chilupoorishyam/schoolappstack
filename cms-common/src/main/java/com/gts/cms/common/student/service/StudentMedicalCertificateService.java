package com.gts.cms.common.student.service;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.StudentMedicalCertificateDTO;
import com.gts.cms.common.student.dto.StudentMedicalCertificateUploadDTO;

import java.util.List;

/**
 * @author Genesis
 *
 */
public interface StudentMedicalCertificateService {

	public ApiResponse<Long> addStudentMedicalCertificateForm(
			StudentMedicalCertificateDTO studentMedicalCertificateDTO);

	public ApiResponse<StudentMedicalCertificateDTO> getStudentMedicalCertificateForm(
			Long studentMedicalCertificateId);

	public ApiResponse<List<StudentMedicalCertificateDTO>> getAllEnquiryForms(Long schoolId, Long academicYearId,
			Long courseYearId, Long groupSectionId, Long studentId, Boolean status);

	public ApiResponse<Long> updateStudentMedicalCertificateForm(
			StudentMedicalCertificateDTO studentMedicalCertificateDTO);

	public ApiResponse<StudentMedicalCertificateUploadDTO> uploadMedicalCertificates(
			StudentMedicalCertificateUploadDTO studentMedicalCertificateUploadDTO);

}
