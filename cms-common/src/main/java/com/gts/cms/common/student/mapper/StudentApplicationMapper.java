package com.gts.cms.common.student.mapper;

import com.gts.cms.common.dto.StudentAppActivityDTO;
import com.gts.cms.common.dto.StudentAppEducationDTO;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.student.dto.StudentAppDocumentCollectionDTO;
import com.gts.cms.common.student.dto.StudentApplicationDTO;
import com.gts.cms.common.util.FileUtil;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class StudentApplicationMapper implements BaseMapper<StudentApplication, StudentApplicationDTO> {

	@Value("${s3.url}")
	private String url;

	@Value("${s3.bucket-name}")
	private String bucketName;
	private static final String fileSeparator = FileUtil.getFileSeparator();
	public StudentApplicationDTO convertStudentApplicationToStudentApplicationDTO(
			StudentApplication studentApplication) {
		StudentApplicationDTO studentApplicationDTO = null;
		if (studentApplication != null) {
			studentApplicationDTO = new StudentApplicationDTO();
			studentApplicationDTO.setStudentAppId(studentApplication.getStudentAppId());
			studentApplicationDTO.setApplicationNumber(studentApplication.getApplicationNo());
			studentApplicationDTO.setBiometricNo(studentApplication.getBiometricNo());
			studentApplicationDTO.setAadharCardNo(studentApplication.getAadharCardNo());
			studentApplicationDTO.setIsMinority(studentApplication.getIsMinority());
			if(studentApplication.getAadharFilePath()!=null) {
			StringBuilder aadharFilePath=new StringBuilder();
			/*aadharFilePath = aadharFilePath.append(url).append(fileSeparator).append(bucketName).append(fileSeparator)
					.append(studentApplication.getAadharFilePath());*/
		
			studentApplicationDTO.setAadharFilePath(FileUtil.getAbsolutePath(studentApplication.getAadharFilePath()));
			}
			
			studentApplicationDTO.setAdminssionDate(studentApplication.getAdminssionDate());
			studentApplicationDTO.setAdmissionNumber(studentApplication.getAdmissionNumber());
			
			studentApplicationDTO.setCreatedDate(studentApplication.getCreatedDt());
			studentApplicationDTO.setCreatedUser(studentApplication.getCreatedUser());
			if (studentApplication.getBatch() != null) {
				studentApplicationDTO.setBatchId(studentApplication.getBatch().getBatchId());
				studentApplicationDTO.setBatchName(studentApplication.getBatch().getBatchName());
			}
			if (studentApplication.getBloodGroup() != null) {
				studentApplicationDTO.setBloodGroupId(studentApplication.getBloodGroup().getGeneralDetailId());
				studentApplicationDTO.setBloodGroup(studentApplication.getBloodGroup().getGeneralDetailDisplayName());
			}
			
			if (studentApplication.getTitle() != null) {
				studentApplicationDTO.setTitleId(studentApplication.getTitle().getGeneralDetailId());
				studentApplicationDTO.setTitle(studentApplication.getTitle().getGeneralDetailDisplayName());
			}
			
			if (studentApplication.getCourse() != null) {
				studentApplicationDTO.setCourseId(studentApplication.getCourse().getCourseId());
				studentApplicationDTO.setCourseName(studentApplication.getCourse().getCourseName());
			}
			if (studentApplication.getCourseYear() != null) {
				studentApplicationDTO.setCourseYearId(studentApplication.getCourseYear().getCourseYearId());
				studentApplicationDTO.setCourseYearName(studentApplication.getCourseYear().getCourseYearName());
			}
			if (studentApplication.getAcademicYear() != null) {
				studentApplicationDTO.setAcademicYearId(studentApplication.getAcademicYear().getAcademicYearId());
				studentApplicationDTO.setAcademicYearName(studentApplication.getAcademicYear().getAcademicYear());
			}

			
			if(studentApplication.getQualifyingCat()!=null) {
				studentApplicationDTO.setQualifyingId(studentApplication.getQualifyingCat().getGeneralDetailId());
				studentApplicationDTO.setQualifyingName(studentApplication.getQualifyingCat().getGeneralDetailDisplayName());
				studentApplicationDTO.setQualifyingCode(studentApplication.getQualifyingCat().getGeneralDetailCode());
			}
			
			if (studentApplication.getLanguage1() != null) {
				studentApplicationDTO.setLanguageId1(studentApplication.getLanguage1().getGeneralDetailId());
			}
			
			if (studentApplication.getLanguage2() != null) {
				studentApplicationDTO.setLanguageId2(studentApplication.getLanguage2().getGeneralDetailId());
			}
			
		
			if (studentApplication.getLanguage3() != null) {
				studentApplicationDTO.setLanguageId3(studentApplication.getLanguage3().getGeneralDetailId());
			}
			
			if (studentApplication.getLanguage4() != null) {
				studentApplicationDTO.setLanguageId4(studentApplication.getLanguage4().getGeneralDetailId());
			}
			
		
			if (studentApplication.getLanguage5() != null) {
				studentApplicationDTO.setLanguageId5(studentApplication.getLanguage5().getGeneralDetailId());
			}
			
			
			
			
			
			studentApplicationDTO.setDateOfExpiry(studentApplication.getDateOfExpiry());
			studentApplicationDTO.setDateOfIssue(studentApplication.getDateOfIssue());
			studentApplicationDTO.setDateOfRegistration(studentApplication.getDateOfRegistration());
			if (studentApplication.getDisability() != null) {
				studentApplicationDTO.setDisabilityId(studentApplication.getDisability().getGeneralDetailId());
				studentApplicationDTO.setDisabilityName(studentApplication.getDisability().getGeneralDetailDisplayName());
			}
			studentApplicationDTO.setDob(studentApplication.getDateOfBirth());
			studentApplicationDTO.setEamcetRank(studentApplication.getEamcetRank());
			studentApplicationDTO.setEmail(studentApplication.getStudentEmailId());
			studentApplicationDTO.setEntranceHTNumber(studentApplication.getEntranceHtNo());
			studentApplicationDTO.setFatherEmailId(studentApplication.getFatherEmailId());
			studentApplicationDTO.setFatherAddress(studentApplication.getFatherAddress());
			studentApplicationDTO.setFatherMobileNo(studentApplication.getFatherMobileNo());
			studentApplicationDTO.setFatherName(studentApplication.getFatherName());
			studentApplicationDTO.setFatherOccupation(studentApplication.getFatherOccupation());
			
			if(studentApplication.getFatherPhotoPath()!=null) {
			StringBuilder fatherPhotoPath=new StringBuilder();
			/*fatherPhotoPath = fatherPhotoPath.append(url).append(fileSeparator).append(bucketName).append(fileSeparator)
					.append(studentApplication.getFatherPhotoPath());*/
		
			studentApplicationDTO.setFatherPhotoPath(FileUtil.getAbsolutePath(studentApplication.getFatherPhotoPath()));
			}
			
			studentApplicationDTO.setFatherQualification(studentApplication.getFatherQualification());
			studentApplicationDTO.setFathersIncomePa(studentApplication.getFathersIncomePa());
			studentApplicationDTO.setFirstName(studentApplication.getFirstName());
			studentApplicationDTO.setFolderPath(studentApplication.getFolderPath());
			if (studentApplication.getGender() != null) {
				studentApplicationDTO.setGenderId(studentApplication.getGender().getGeneralDetailId());
				studentApplicationDTO.setGenderName(studentApplication.getGender().getGeneralDetailDisplayName());
			}
			studentApplicationDTO.setGuardianAddress(studentApplication.getGuardianAddress());
			studentApplicationDTO.setGuardianEmailId(studentApplication.getGuardianEmailId());
			studentApplicationDTO.setGuardianIncomePa(studentApplication.getGuardianIncomePa());
			studentApplicationDTO.setGuardianName(studentApplication.getGuardianName());
			studentApplicationDTO.setHallticketNumber(studentApplication.getHallticketNumber());
			studentApplicationDTO.setHobbies(studentApplication.getHobbies());
			studentApplicationDTO.setIdentificationMarks(studentApplication.getIdentificationMarks());
			studentApplicationDTO.setInterests(studentApplication.getInterests());
			studentApplicationDTO.setIsActive(studentApplication.getIsActive());
			studentApplicationDTO.setIsgovtempFather(studentApplication.getIsgovtempFather());
			studentApplicationDTO.setIsgovtempMother(studentApplication.getIsgovtempMother());
			studentApplicationDTO.setIsLocal(studentApplication.getIsLocal());
			studentApplicationDTO.setLangStatus1(studentApplication.getLangStatus1());
			studentApplicationDTO.setLangStatus2(studentApplication.getLangStatus2());
			studentApplicationDTO.setLangStatus3(studentApplication.getLangStatus3());
			studentApplicationDTO.setLangStatus4(studentApplication.getLangStatus4());
			studentApplicationDTO.setLangStatus5(studentApplication.getLangStatus5());
			studentApplicationDTO.setLastName(studentApplication.getLastName());
			studentApplicationDTO.setMiddleName(studentApplication.getMiddleName());
			studentApplicationDTO.setMobile(studentApplication.getMobile());
			studentApplicationDTO.setMotherAddress(studentApplication.getMotherAddress());
			studentApplicationDTO.setMotherEmailId(studentApplication.getMotherEmailId());
			studentApplicationDTO.setMotherIncomePa(studentApplication.getMotherIncomePa());
			studentApplicationDTO.setMotherMobileNo(studentApplication.getMotherMobileNo());
			studentApplicationDTO.setMotherName(studentApplication.getMotherName());
			studentApplicationDTO.setMotherOccupation(studentApplication.getMotherOccupation());
			studentApplicationDTO.setWeddingDate(studentApplication.getWeddingDate());
			
			if(studentApplication.getMotherPhotoPath()!=null) {
			/*StringBuilder motherPhotoPath=new StringBuilder();
			motherPhotoPath = motherPhotoPath.append(url).append(fileSeparator).append(bucketName).append(fileSeparator)
					.append(studentApplication.getMotherPhotoPath());*/
		
		
			studentApplicationDTO.setMotherPhotoPath(FileUtil.getAbsolutePath(studentApplication.getMotherPhotoPath()));
			}
			
			
			studentApplicationDTO.setMotherQualification(studentApplication.getMotherQualification());
			studentApplicationDTO.setStatusComments(studentApplication.getStatusComments());
			if (studentApplication.getNationality() != null) {
				studentApplicationDTO.setNationalityId(studentApplication.getNationality().getGeneralDetailId());
				studentApplicationDTO.setNationality(studentApplication.getNationality().getGeneralDetailDisplayName());
			}
			
			if(studentApplication.getPancardFilePath()!=null) {
			/*StringBuilder pancardPhotoPath=new StringBuilder();
			pancardPhotoPath = pancardPhotoPath.append(url).append(fileSeparator).append(bucketName).append(fileSeparator)
					.append(studentApplication.getPancardFilePath());*/
			
			studentApplicationDTO.setPancardFilePath(FileUtil.getAbsolutePath(studentApplication.getPancardFilePath()));
			}
			
			
			studentApplicationDTO.setPancardNo(studentApplication.getPancardNo());
			studentApplicationDTO.setPassportNo(studentApplication.getPassportNo());
			studentApplicationDTO.setPermanentAddress(studentApplication.getPermanentAddress());
			studentApplicationDTO.setPermanentPincode(studentApplication.getPermanentPincode());
			studentApplicationDTO.setPermanentStreet(studentApplication.getPermanentStreet());
			studentApplicationDTO.setPresentAddress(studentApplication.getPresentAddress());
			studentApplicationDTO.setPresentMandal(studentApplication.getPresentMandal());
			studentApplicationDTO.setPermanentMandal(studentApplication.getPermanentMandal());
			studentApplicationDTO.setPresentPincode(studentApplication.getPresentPincode());
			studentApplicationDTO.setPresentStreetName(studentApplication.getPresentStreetName());
			  if(studentApplication.getPresentCity()!=null) {
			studentApplicationDTO.setPresentCityId(studentApplication.getPresentCity().getCityId());
			studentApplicationDTO.setPresentCityName(studentApplication.getPresentCity().getCityName());
			   if(studentApplication.getPresentDistrict()!=null) {
				   studentApplicationDTO.setPresentDistrictId(studentApplication.getPresentDistrict().getDistrictId());
				   studentApplicationDTO.setPresentDistrictName(studentApplication.getPresentDistrict().getDistrictName());
				   if(studentApplication.getPresentDistrict().getState()!=null) {
					   studentApplicationDTO.setPresentStateId(studentApplication.getPresentDistrict().getState().getStateId());
					   studentApplicationDTO.setPresentStateName(studentApplication.getPresentDistrict().getState().getStateName());
					   if(studentApplication.getPresentDistrict().getState().getCountry()!=null) {
						   studentApplicationDTO.setPresentCountryId(studentApplication.getPresentDistrict().getState().getCountry().getCountryId());
						   studentApplicationDTO.setPresentCountryName(studentApplication.getPresentDistrict().getState().getCountry().getCountryName());
						   
					   }
				   }
				   
			   }
			   
			  }
			  
			  if(studentApplication.getPermenentCity()!=null) {
					studentApplicationDTO.setPermanentCityId(studentApplication.getPermenentCity().getCityId());
					studentApplicationDTO.setPermanentCityName(studentApplication.getPermenentCity().getCityName());
					if(studentApplication.getPermanentDistrict()!=null) {
						   studentApplicationDTO.setPermanentDistrictId(studentApplication.getPermanentDistrict().getDistrictId());
						   studentApplicationDTO.setPermanentDistrictName(studentApplication.getPermanentDistrict().getDistrictName());
						   if(studentApplication.getPermanentDistrict().getState()!=null) {
							   studentApplicationDTO.setPermanentStateId(studentApplication.getPermanentDistrict().getState().getStateId());
							   studentApplicationDTO.setPermanentStateName(studentApplication.getPermanentDistrict().getState().getStateName());
							   if(studentApplication.getPermanentDistrict().getState().getCountry()!=null) {
								   studentApplicationDTO.setPermanentCountryId(studentApplication.getPermanentDistrict().getState().getCountry().getCountryId());
								   studentApplicationDTO.setPermanentCountryName(studentApplication.getPermanentDistrict().getState().getCountry().getCountryName());
								   
							   }
						   }
						   
					   }
					  }
					  
			
			  
			  
			studentApplicationDTO.setPrimaryContact(studentApplication.getPrimaryContact());
			if (studentApplication.getQuota() != null) {
				studentApplicationDTO.setQuotaId(studentApplication.getQuota().getGeneralDetailId());
				studentApplicationDTO.setQuotaName(studentApplication.getQuota().getGeneralDetailDisplayName());
				studentApplicationDTO.setQuotaCode(studentApplication.getQuota().getGeneralDetailCode());
			}
			studentApplicationDTO.setReason(studentApplication.getReason());
			studentApplicationDTO.setReceiptNo(studentApplication.getReceiptNo());
			studentApplicationDTO.setRefApplicationNo(studentApplication.getRefApplicationNo());

			if (studentApplication.getReligion() != null) {
				studentApplicationDTO.setReligionId(studentApplication.getReligion().getGeneralDetailId());
			   studentApplicationDTO.setReligion(studentApplication.getReligion().getGeneralDetailDisplayName());	
			}
			studentApplicationDTO.setResidencePhone(studentApplication.getResidencePhone());
			studentApplicationDTO.setRollNumber(studentApplication.getRollNumber());
			studentApplicationDTO.setSscNo(studentApplication.getSscNo());
			if(studentApplication.getStudentPhotoPath()!=null) {
			/*StringBuilder studentPhotoPath=new StringBuilder();
			studentPhotoPath = studentPhotoPath.append(url).append(fileSeparator).append(bucketName).append(fileSeparator)
					.append(studentApplication.getStudentPhotoPath());*/
			studentApplicationDTO.setStudentPhotoPath(FileUtil.getAbsolutePath(studentApplication.getStudentPhotoPath()));
			}
			
			if (studentApplication.getStudenttype() != null) {
				studentApplicationDTO.setStudentTypeId(studentApplication.getStudenttype().getGeneralDetailId());
				studentApplicationDTO.setStudentTypeName(studentApplication.getStudenttype().getGeneralDetailDisplayName());
			}
			if (studentApplication.getSubCaste() != null) {
				studentApplicationDTO.setSubCasteId(studentApplication.getSubCaste().getSubCasteId());
				studentApplicationDTO.setSubCasteName(studentApplication.getSubCaste().getSubCaste());
			}
			if (studentApplication.getCaste() != null) {
				studentApplicationDTO.setCasteId(studentApplication.getCaste().getCasteId());
				studentApplicationDTO.setCasteName(studentApplication.getCaste().getCaste());
			}

			
			if (studentApplication.getSchool() != null) {
				studentApplicationDTO.setSchoolId(studentApplication.getSchool().getSchoolId());
				studentApplicationDTO.setSchoolCode(studentApplication.getSchool().getSchoolCode());
				studentApplicationDTO.setSchoolName(studentApplication.getSchool().getSchoolName());
				
			}
			if (studentApplication.getOrganization() != null) {
				studentApplicationDTO.setOrganizationId(studentApplication.getOrganization().getOrganizationId());
				studentApplicationDTO.setOrganizationCode(studentApplication.getOrganization().getOrgCode());
				studentApplicationDTO.setOrganizationName(studentApplication.getOrganization().getOrgName());
				
			}
			if (studentApplication.getWorkflowStage() != null) {
				studentApplicationDTO.setCurrentWorkflowStatusId(studentApplication.getWorkflowStage().getWorkflowStageId());
				studentApplicationDTO.setCurrentWorkflowStatusName(studentApplication.getWorkflowStage().getWfName());
				

			}
			if(studentApplication.getIsCurrentYear()!=null) {
			studentApplicationDTO.setIsCurrentYear(studentApplication.getIsCurrentYear());
			}
			if(studentApplication.getIsLateral()!=null) {
			studentApplicationDTO.setIsLateral(studentApplication.getIsLateral());
			}
			if(studentApplication.getIsScholorship()!=null) {
				studentApplicationDTO.setIsScholorship(studentApplication.getIsScholorship());
				}
			
              setStudentAppActivities(studentApplicationDTO, studentApplication);
			  setStudentAppEducations(studentApplicationDTO, studentApplication);
			  setStudentAppDocumentCollection(studentApplicationDTO, studentApplication);
			// add studentEducationDetails, appActitivites,appDocumentCollection,appWorkflow 
			// 
		}
		return studentApplicationDTO;
	}

	public StudentApplication convertStudentApplicationDTOToStudentApplication(
			StudentApplicationDTO studentApplicationDTO,boolean isCreate) {
		StudentApplication studentApplication = null;
		if (studentApplicationDTO != null) {
			studentApplication = new StudentApplication();
			studentApplication.setCreatedDt(studentApplicationDTO.getCreatedDate());
			studentApplication.setCreatedUser(studentApplicationDTO.getCreatedUser());
			studentApplication.setUpdatedDt(new Date());
			studentApplication.setUpdatedUser(SecurityUtil.getCurrentUser());
			studentApplication.setAdminssionDate(new Date());
			if(isCreate) {
				studentApplication.setAdminssionDate(new Date());
				studentApplication.setCreatedDt(new Date());
				studentApplication.setCreatedUser(SecurityUtil.getCurrentUser());
			}
			studentApplication.setIsActive(studentApplicationDTO.getIsActive());
			studentApplication.setStudentAppId(studentApplicationDTO.getStudentAppId());
			studentApplication.setIsLateral(studentApplicationDTO.getIsLateral());
			studentApplication.setIsScholorship(studentApplicationDTO.getIsScholorship());
			studentApplication.setApplicationNo(studentApplicationDTO.getApplicationNumber());
			studentApplication.setBiometricNo(studentApplicationDTO.getBiometricNo());
			studentApplication.setAadharCardNo(studentApplicationDTO.getAadharCardNo());
			studentApplication.setAadharFilePath(FileUtil.getRelativePath(studentApplicationDTO.getAadharFilePath()));//rel
			if(!isCreate && studentApplicationDTO.getAdmissionNumber()!=null) {
			studentApplication.setAdminssionDate(new Date());
			studentApplication.setAdmissionNumber(studentApplicationDTO.getAdmissionNumber());
			}else {
				studentApplication.setAdminssionDate(studentApplicationDTO.getAdminssionDate());
			}
			studentApplication.setIsCurrentYear(studentApplicationDTO.getIsCurrentYear());
         	studentApplication.setDateOfExpiry(studentApplicationDTO.getDateOfExpiry());
			studentApplication.setDateOfIssue(studentApplicationDTO.getDateOfIssue());
			studentApplication.setDateOfRegistration(studentApplicationDTO.getDateOfRegistration());
			studentApplication.setEamcetRank(studentApplicationDTO.getEamcetRank());
			studentApplication.setStatusComments(studentApplicationDTO.getStatusComments());
			studentApplication.setFatherEmailId(studentApplicationDTO.getFatherEmailId());
			studentApplication.setFatherAddress(studentApplicationDTO.getFatherAddress());
			studentApplication.setFatherMobileNo(studentApplicationDTO.getFatherMobileNo());
			studentApplication.setFatherName(studentApplicationDTO.getFatherName());
			studentApplication.setFatherOccupation(studentApplicationDTO.getFatherOccupation());
			studentApplication.setFatherPhotoPath(FileUtil.getRelativePath(studentApplicationDTO.getFatherPhotoPath()));//rel
			studentApplication.setFatherQualification(studentApplicationDTO.getFatherQualification());
			studentApplication.setFathersIncomePa(studentApplicationDTO.getFathersIncomePa());
			studentApplication.setFirstName(studentApplicationDTO.getFirstName());
			studentApplication.setFolderPath(studentApplicationDTO.getFolderPath());
			studentApplication.setGuardianAddress(studentApplicationDTO.getGuardianAddress());
			studentApplication.setGuardianEmailId(studentApplicationDTO.getGuardianEmailId());
			studentApplication.setGuardianIncomePa(studentApplicationDTO.getGuardianIncomePa());
			studentApplication.setGuardianName(studentApplicationDTO.getGuardianName());
			studentApplication.setHallticketNumber(studentApplicationDTO.getHallticketNumber());
			studentApplication.setHobbies(studentApplicationDTO.getHobbies());
			studentApplication.setIdentificationMarks(studentApplicationDTO.getIdentificationMarks());
			studentApplication.setInterests(studentApplicationDTO.getInterests());
			studentApplication.setIsgovtempFather(studentApplicationDTO.getIsgovtempFather());
			studentApplication.setIsgovtempMother(studentApplicationDTO.getIsgovtempMother());
			studentApplication.setIsLocal(studentApplicationDTO.getIsLocal());
			studentApplication.setLangStatus1(studentApplicationDTO.getLangStatus1());
			studentApplication.setLangStatus2(studentApplicationDTO.getLangStatus2());
			studentApplication.setLangStatus3(studentApplicationDTO.getLangStatus3());
			studentApplication.setLangStatus4(studentApplicationDTO.getLangStatus4());
			studentApplication.setLangStatus5(studentApplicationDTO.getLangStatus5());
			studentApplication.setWeddingDate(studentApplicationDTO.getWeddingDate());
			studentApplication.setIsMinority(studentApplicationDTO.getIsMinority());
			if (studentApplicationDTO.getLanguageId1() != null) {
				GeneralDetail lang1 = new GeneralDetail();
				lang1.setGeneralDetailId(studentApplicationDTO.getLanguageId1());
				studentApplication.setLanguage1(lang1);
			}
			
			if (studentApplicationDTO.getLanguageId2() != null) {
				GeneralDetail lang2 = new GeneralDetail();
				lang2.setGeneralDetailId(studentApplicationDTO.getLanguageId2());
				studentApplication.setLanguage2(lang2);
			}
			
			if (studentApplicationDTO.getLanguageId3() != null) {
				GeneralDetail lang3 = new GeneralDetail();
				lang3.setGeneralDetailId(studentApplicationDTO.getLanguageId3());
				studentApplication.setLanguage3(lang3);
			}
			
			if (studentApplicationDTO.getLanguageId4() != null) {
				GeneralDetail lang4 = new GeneralDetail();
				lang4.setGeneralDetailId(studentApplicationDTO.getLanguageId4());
				studentApplication.setLanguage4(lang4);
			}
			
			if (studentApplicationDTO.getLanguageId5() != null) {
				GeneralDetail lang5 = new GeneralDetail();
				lang5.setGeneralDetailId(studentApplicationDTO.getLanguageId5());
				studentApplication.setLanguage5(lang5);
			}
			if (studentApplicationDTO.getTitleId() != null) {
				GeneralDetail title = new GeneralDetail();
				title.setGeneralDetailId(studentApplicationDTO.getTitleId());
				studentApplication.setTitle(title);
			}
			
			studentApplication.setLastName(studentApplicationDTO.getLastName());
			studentApplication.setMiddleName(studentApplicationDTO.getMiddleName());
			studentApplication.setMobile(studentApplicationDTO.getMobile());
			studentApplication.setMotherAddress(studentApplicationDTO.getMotherAddress());
			studentApplication.setMotherEmailId(studentApplicationDTO.getMotherEmailId());
			studentApplication.setMotherIncomePa(studentApplicationDTO.getMotherIncomePa());
			studentApplication.setMotherMobileNo(studentApplicationDTO.getMotherMobileNo());
			studentApplication.setMotherName(studentApplicationDTO.getMotherName());
			studentApplication.setMotherOccupation(studentApplicationDTO.getMotherOccupation());
			studentApplication.setMotherPhotoPath(FileUtil.getRelativePath(studentApplicationDTO.getMotherPhotoPath()));//rel
			studentApplication.setMotherQualification(studentApplicationDTO.getMotherQualification());
			studentApplication.setPancardFilePath(FileUtil.getRelativePath(studentApplicationDTO.getPancardFilePath()));//changed to relative path
			studentApplication.setPancardNo(studentApplicationDTO.getPancardNo());
			studentApplication.setPassportNo(studentApplicationDTO.getPassportNo());
			studentApplication.setPermanentAddress(studentApplicationDTO.getPermanentAddress());
			studentApplication.setPermanentPincode(studentApplicationDTO.getPermanentPincode());
			studentApplication.setPermanentStreet(studentApplicationDTO.getPermanentStreet());
			studentApplication.setPermanentMandal(studentApplicationDTO.getPermanentMandal());
			saveStudentAppPermanentCity(studentApplicationDTO, studentApplication);

			saveStudentAppPresentCity(studentApplicationDTO, studentApplication);
			// we need to add district also
			studentApplication.setPresentAddress(studentApplicationDTO.getPresentAddress());
			studentApplication.setPresentMandal(studentApplicationDTO.getPresentMandal());
			studentApplication.setPresentPincode(studentApplicationDTO.getPresentPincode());
			studentApplication.setPresentStreetName(studentApplicationDTO.getPresentStreetName());
			studentApplication.setPrimaryContact(studentApplicationDTO.getPrimaryContact());
			studentApplication.setReason(studentApplicationDTO.getReason());
			studentApplication.setReceiptNo(studentApplicationDTO.getReceiptNo());
			studentApplication.setRefApplicationNo(studentApplicationDTO.getRefApplicationNo());
			studentApplication.setResidencePhone(studentApplicationDTO.getResidencePhone());
			studentApplication.setRollNumber(studentApplicationDTO.getRollNumber());
			studentApplication.setSscNo(studentApplicationDTO.getSscNo());
			studentApplication.setStudentPhotoPath(FileUtil.getRelativePath(studentApplicationDTO.getStudentPhotoPath()));

			studentApplication.setDateOfBirth(studentApplicationDTO.getDob());
			studentApplication.setStudentEmailId(studentApplicationDTO.getEmail());
			studentApplication.setEntranceHtNo(studentApplicationDTO.getEntranceHTNumber());
			if (studentApplicationDTO.getCourseId() != null) {
				Course course = new Course();
				course.setCourseId(studentApplicationDTO.getCourseId());
				studentApplication.setCourse(course);
			}
			if (studentApplicationDTO.getSchoolId() != null) {
				School school = new School();
				school.setSchoolId(studentApplicationDTO.getSchoolId());
				studentApplication.setSchool(school);
			}
			
			if (studentApplicationDTO.getOrganizationId()!=null) {
				Organization org=new Organization();
				org.setOrganizationId(studentApplicationDTO.getOrganizationId());
				studentApplication.setOrganization(org);
			}
			
			
			if (studentApplicationDTO.getCourseYearId() != null) {
				CourseYear courseYear = new CourseYear();
				courseYear.setCourseYearId(studentApplicationDTO.getCourseYearId());
				studentApplication.setCourseYear(courseYear);
			}

			if (studentApplicationDTO.getBatchId() != null) {
				Batch batch = new Batch();
				batch.setBatchId(studentApplicationDTO.getBatchId());
				studentApplication.setBatch(batch);
			}
			if (studentApplicationDTO.getCasteId() != null) {
				Caste caste = new Caste();
				caste.setCasteId(studentApplicationDTO.getCasteId());
				studentApplication.setCaste(caste);
			}
			if (studentApplicationDTO.getSubCasteId() != null) {
				SubCaste subCaste = new SubCaste();
				subCaste.setSubCasteId(studentApplicationDTO.getSubCasteId());
				studentApplication.setSubCaste(subCaste);
			}


			if (studentApplicationDTO.getQualifyingId() != null) {
				GeneralDetail generalDetail = new GeneralDetail();
				generalDetail.setGeneralDetailId(studentApplicationDTO.getQualifyingId());
				
				studentApplication.setQualifyingCat(generalDetail);
			}
			
			if (studentApplicationDTO.getDisabilityId() != null) {
				GeneralDetail disability = new GeneralDetail();
				disability.setGeneralDetailId(studentApplicationDTO.getDisabilityId());
				studentApplication.setDisability(disability);
			}

			if (studentApplicationDTO.getGenderId() != null) {
				GeneralDetail gender = new GeneralDetail();
				gender.setGeneralDetailId(studentApplicationDTO.getGenderId());
				studentApplication.setGender(gender);
			}

			if (studentApplicationDTO.getQuotaId() != null) {
				GeneralDetail quota = new GeneralDetail();
				quota.setGeneralDetailId(studentApplicationDTO.getQuotaId());
				quota.setGeneralDetailDisplayName(studentApplicationDTO.getQuotaName());
				quota.setGeneralDetailCode(studentApplicationDTO.getQuotaCode());
				studentApplication.setQuota(quota);
			}

			if (studentApplicationDTO.getReligionId() != null) {
				GeneralDetail religion = new GeneralDetail();
				religion.setGeneralDetailId(studentApplicationDTO.getReligionId());
				studentApplication.setReligion(religion);
			}

			if (studentApplicationDTO.getStudentTypeId() != null) {
				GeneralDetail studentType = new GeneralDetail();
				studentType.setGeneralDetailId(studentApplicationDTO.getStudentTypeId());
				studentApplication.setStudenttype(studentType);
			}

			if (studentApplicationDTO.getBloodGroupId() != null) {
				GeneralDetail bloodGroup = new GeneralDetail();
				bloodGroup.setGeneralDetailId(studentApplicationDTO.getBloodGroupId());
				studentApplication.setBloodGroup(bloodGroup);
			}

			if (studentApplicationDTO.getNationalityId() != null) {
				GeneralDetail nationality = new GeneralDetail();
				nationality.setGeneralDetailId(studentApplicationDTO.getNationalityId());
				studentApplication.setNationality(nationality);
			}


			AcademicYear academicYear = new AcademicYear();
			academicYear.setAcademicYearId(studentApplicationDTO.getAcademicYearId());
			studentApplication.setAcademicYear(academicYear);

			setStudentAppDocumentCollection(studentApplicationDTO, studentApplication,isCreate);
			setStudentAppActivities(studentApplicationDTO, studentApplication,isCreate);
			setStudentAppEducations(studentApplicationDTO, studentApplication,isCreate);
		//	setStudentAppWorkflows(studentApplicationDTO, studentApplication,isCreate);

		}
		return studentApplication;
	}

	/*private void setStudentAppWorkflows(StudentApplicationDTO studentApplicationDTO,
			StudentApplication studentApplication,boolean isCreate) {
		if (!CollectionUtils.isEmpty(studentApplicationDTO.getStdAppWorkflowsDTOList())) {
			List<StudentAppWorkflow> studentAppWorkflowList = new ArrayList<>();
			for (StudentAppWorkflowDTO studentAppWorkflowDTO : studentApplicationDTO.getStdAppWorkflowsDTOList()) {
				 if(studentAppWorkflowDTO.getFromAppWorkflowStatusId()!=null && studentAppWorkflowDTO.getToAppWorkflowStatusId()!=null) {
				StudentAppWorkflow appWorkflow = new StudentAppWorkflow();
			
				appWorkflow.setComments(studentAppWorkflowDTO.getComments());
				appWorkflow.setCreatedUser(SecurityUtil.getCurrentUser());
				//appWorkflow.setIsActive(studentAppWorkflowDTO.getIsActive());
				appWorkflow.setIsActive(Boolean.TRUE);
				appWorkflow.setIsCurrentStatus(studentAppWorkflowDTO.getIsCurrentStatus());
				appWorkflow.setReason(studentAppWorkflowDTO.getReason());
				appWorkflow.setUpdatedDt(new Date());
				appWorkflow.setUpdatedUser(SecurityUtil.getCurrentUser());
				WorkflowStage toAppStatus = new WorkflowStage();
				toAppStatus.setWorkflowStageId(studentAppWorkflowDTO.getToAppWorkflowStatusId());
				appWorkflow.setToAppStatus(toAppStatus);

				WorkflowStage fromAppStatus = new WorkflowStage();
				fromAppStatus.setWorkflowStageId(studentAppWorkflowDTO.getFromAppWorkflowStatusId());
				appWorkflow.setToAppStatus(fromAppStatus);
				appWorkflow.setStudentApplication(studentApplication);
				studentAppWorkflowList.add(appWorkflow);
				 }
			}
			if (!CollectionUtils.isEmpty(studentAppWorkflowList)) {
				studentApplication.setStdAppWorkflows(studentAppWorkflowList);
			}
		}
	}*/

	private void setStudentAppEducations(StudentApplicationDTO studentApplicationDTO,
			StudentApplication studentApplication,boolean isCreate) {

		if (!CollectionUtils.isEmpty(studentApplicationDTO.getStdAppEducations())) {
			List<StudentAppEducation> studentAppEducationList = new ArrayList<>();
			for (StudentAppEducationDTO studentAppEducationDTO : studentApplicationDTO.getStdAppEducations()) {

				StudentAppEducation studentAppEducation = new StudentAppEducation();
			    studentAppEducation.setAppEducationId(studentAppEducationDTO.getAppEducationId());
				studentAppEducation.setAddress(studentAppEducationDTO.getAddress());
				studentAppEducation.setBoard(studentAppEducationDTO.getBoard());
				studentAppEducation.setCreatedUser(SecurityUtil.getCurrentUser());
				studentAppEducation.setGradeClassSecured(studentAppEducationDTO.getGradeClassSecured());
				studentAppEducation.setCreatedDt(studentApplicationDTO.getCreatedDate());
				studentAppEducation.setCreatedUser(studentApplicationDTO.getCreatedUser());
				if(isCreate) {
		
				studentAppEducation.setCreatedDt(new Date());
				studentAppEducation.setCreatedUser(SecurityUtil.getCurrentUser());
				}
				studentAppEducation.setIsActive(studentAppEducationDTO.getIsActive());
				studentAppEducation.setMajorSubjects(studentAppEducationDTO.getMajorSubjects());
				studentAppEducation.setMedium(studentAppEducationDTO.getMedium());
				studentAppEducation.setYearOfCompletion(studentAppEducationDTO.getYearOfCompletion());
				studentAppEducation.setNameOfInstitution(studentAppEducationDTO.getNameOfInstitution());
				if(studentAppEducationDTO.getPrecentage()!=null) {
				studentAppEducation.setPrecentage(BigDecimal.valueOf(studentAppEducationDTO.getPrecentage()).setScale(2, RoundingMode.HALF_UP));
				}
				studentAppEducation.setReason(studentAppEducationDTO.getReason());
				studentAppEducation.setUpdatedDt(new Date());
				studentAppEducation.setUpdatedUser(SecurityUtil.getCurrentUser());
				studentAppEducation.setStudentApplication(studentApplication);
				studentAppEducationList.add(studentAppEducation);
			}
			if (!CollectionUtils.isEmpty(studentAppEducationList)) {
				studentApplication.setStdAppEducations(studentAppEducationList);
			}

		}
	}
	
	private void setStudentAppEducations(StudentDetail studentDetail,
			StudentApplication studentApplication,boolean isCreate) {
 
		if (!CollectionUtils.isEmpty(studentApplication.getStdAppEducations())) {
			List<StudentEducationDetail> studentEducationDetails = new ArrayList<>();
			for (StudentAppEducation studentAppEducation : studentApplication.getStdAppEducations()) {

				StudentEducationDetail studentEducationDetail = new StudentEducationDetail();
				studentEducationDetail.setStudentEducationId(studentAppEducation.getAppEducationId());
				studentEducationDetail.setAddress(studentAppEducation.getAddress());
				studentEducationDetail.setBoard(studentAppEducation.getBoard());
				studentEducationDetail.setCreatedUser(SecurityUtil.getCurrentUser());
				studentEducationDetail.setGradeClassSecured(studentAppEducation.getGradeClassSecured());
				studentEducationDetail.setCreatedDt(studentAppEducation.getCreatedDt());
				studentEducationDetail.setCreatedUser(studentAppEducation.getCreatedUser());
				if(isCreate) {
		
					studentEducationDetail.setCreatedDt(new Date());
					studentEducationDetail.setCreatedUser(SecurityUtil.getCurrentUser());
				}
				studentEducationDetail.setIsActive(studentAppEducation.getIsActive());
				studentEducationDetail.setMajorSubjects(studentAppEducation.getMajorSubjects());
				studentEducationDetail.setMedium(studentAppEducation.getMedium());
				studentEducationDetail.setNameOfInstitution(studentAppEducation.getNameOfInstitution());
				studentEducationDetail.setYearOfCompletion(studentAppEducation.getYearOfCompletion());
				if(studentAppEducation.getPrecentage()!=null) {
				studentAppEducation.setPrecentage(studentAppEducation.getPrecentage());
				}
				studentEducationDetail.setReason(studentAppEducation.getReason());
				studentEducationDetail.setUpdatedDt(new Date());
				studentEducationDetail.setUpdatedUser(SecurityUtil.getCurrentUser());
				
				studentEducationDetails.add(studentEducationDetail);
			}
			if (!CollectionUtils.isEmpty(studentEducationDetails)) {
				studentDetail.setStdEducationDetails(studentEducationDetails);
			}

		}
	}
	
	private void setStudentAppEducations(StudentApplicationDTO studentApplicationDTO,
			StudentApplication studentApplication) {

		if (!CollectionUtils.isEmpty(studentApplication.getStdAppEducations())) {
			List<StudentAppEducationDTO> studentAppEducations = new ArrayList<>();
			for (StudentAppEducation studentAppEducation : studentApplication.getStdAppEducations()) {

				StudentAppEducationDTO studentAppEducationDTO = new StudentAppEducationDTO();
			   studentAppEducationDTO.setAppEducationId(studentAppEducation.getAppEducationId());
				studentAppEducationDTO.setAddress(studentAppEducation.getAddress());
				studentAppEducationDTO.setBoard(studentAppEducation.getBoard());
				studentAppEducationDTO.setCreatedUser(studentAppEducation.getCreatedUser());
				studentAppEducationDTO.setGradeClassSecured(studentAppEducation.getGradeClassSecured());			
				studentAppEducationDTO.setIsActive(studentAppEducation.getIsActive());
				studentAppEducationDTO.setMajorSubjects(studentAppEducation.getMajorSubjects());
				studentAppEducationDTO.setMedium(studentAppEducation.getMedium());
				studentAppEducationDTO.setNameOfInstitution(studentAppEducation.getNameOfInstitution());
				studentAppEducationDTO.setYearOfCompletion(studentAppEducation.getYearOfCompletion());
				if(studentAppEducation.getPrecentage()!=null) {
					studentAppEducationDTO.setPrecentage(studentAppEducation.getPrecentage().doubleValue());
				}
				studentAppEducationDTO.setReason(studentAppEducation.getReason());
				studentAppEducationDTO.setUpdatedDt(new Date());
				studentAppEducationDTO.setUpdatedUser(studentAppEducation.getUpdatedUser());
				studentAppEducations.add(studentAppEducationDTO);
				
			}
			if (!CollectionUtils.isEmpty(studentAppEducations)) {
				studentApplicationDTO.setStdAppEducations(studentAppEducations);
			}

		}
	}
	
	

	private void setStudentAppActivities(StudentApplicationDTO studentApplicationDTO,
			StudentApplication studentApplication,boolean isCreate) {

		if (!CollectionUtils.isEmpty(studentApplicationDTO.getStdAppActivities())) {
			List<StudentAppActivity> studentAppActivityList = new ArrayList<>();
			for (StudentAppActivityDTO studentAppActivityDTO : studentApplicationDTO.getStdAppActivities()) {

				StudentAppActivity studentAppActivity = new StudentAppActivity();
				studentAppActivity.setStudentAppActivityId(studentAppActivityDTO.getStudentAppActivityId());
				studentAppActivity.setCreatedDt(studentApplicationDTO.getCreatedDate());
				studentAppActivity.setCreatedUser(studentApplicationDTO.getCreatedUser());
				
				if(isCreate) {
					studentAppActivity.setCreatedDt(new Date());
					studentAppActivity.setCreatedUser(SecurityUtil.getCurrentUser());
				}
				studentAppActivity.setLevel(studentAppActivityDTO.getLevel());
				studentAppActivity.setParticulars(studentAppActivityDTO.getParticulars());
				studentAppActivity.setSponsoredBy(studentAppActivity.getSponsoredBy());
				studentAppActivity.setUpdatedDt(new Date());
				studentAppActivity.setSponsoredBy(studentAppActivityDTO.getSponsoredBy());
				studentAppActivity.setUpdatedUser(SecurityUtil.getCurrentUser());
				studentAppActivity.setStudentApplication(studentApplication);
				studentAppActivity.setIsActive(studentAppActivityDTO.getIsActive());
				studentAppActivityList.add(studentAppActivity);
			}
			if (!CollectionUtils.isEmpty(studentAppActivityList)) {
				studentApplication.setStdAppActivities(studentAppActivityList);
			}

		}

	}

	private void setStudentAppActivities(StudentApplicationDTO studentApplicationDTO,
			StudentApplication studentApplication) {

		if (!CollectionUtils.isEmpty(studentApplication.getStdAppActivities())) {
			List<StudentAppActivityDTO> studentAppActivities = new ArrayList<>();
			for (StudentAppActivity studentAppActivity : studentApplication.getStdAppActivities()) {

				StudentAppActivityDTO studentAppActivityDTO = new StudentAppActivityDTO();
				studentAppActivityDTO.setStudentAppActivityId(studentAppActivity.getStudentAppActivityId());
				studentAppActivityDTO.setLevel(studentAppActivity.getLevel());
				studentAppActivityDTO.setParticulars(studentAppActivity.getParticulars());
				studentAppActivityDTO.setSponsoredBy(studentAppActivity.getSponsoredBy());
				studentAppActivityDTO.setUpdatedDt(studentAppActivity.getUpdatedDt());
				studentAppActivityDTO.setUpdatedUser(studentAppActivity.getUpdatedUser());
				studentAppActivityDTO.setCreatedUser(SecurityUtil.getCurrentUser());
				studentAppActivityDTO.setIsActive(studentAppActivity.getIsActive());
				studentAppActivities.add(studentAppActivityDTO);
			}
			if (!CollectionUtils.isEmpty(studentAppActivities)) {
				studentApplicationDTO.setStdAppActivities(studentAppActivities);
			}

		}

	}

	
	
	private void saveStudentAppPermanentCity(StudentApplicationDTO studentApplicationDTO,
			StudentApplication studentApplication) {
		if (studentApplicationDTO.getPermanentCityId() != null) {
			City permanentCity = new City();
			permanentCity.setCityId(studentApplicationDTO.getPermanentCityId());
			studentApplication.setPermenentCity(permanentCity);
			if(studentApplicationDTO.getPermanentDistrictId()!=null){
				District district=new District();
				district.setDistrictId(studentApplicationDTO.getPermanentDistrictId());
				district.setDistrictName(studentApplicationDTO.getPermanentDistrictName());
				permanentCity.setDistrict(district);
				studentApplication.setPermanentDistrict(district);
				
				if(studentApplicationDTO.getPermanentStateId()!=null){
					State state=new State();
					state.setStateId(studentApplicationDTO.getPermanentStateId());
					state.setStateName(studentApplicationDTO.getPermanentStateName());
				   district.setState(state);
				   
				   if(studentApplicationDTO.getPermanentCountryId()!=null) {
					   Country country=new Country();
					   country.setCountryId(studentApplicationDTO.getPermanentCountryId());
					   country.setCountryName(studentApplicationDTO.getPermanentCountryName());
					   state.setCountry(country);
				   }
				}
			}
			
		}

	}

	private void saveStudentAppPresentCity(StudentApplicationDTO studentApplicationDTO,
			StudentApplication studentApplication) {
		if (studentApplicationDTO.getPresentCityId() != null) {
			City presentCity = new City();
			presentCity.setCityId(studentApplicationDTO.getPresentCityId());
			studentApplication.setPresentCity(presentCity);
			if(studentApplicationDTO.getPresentDistrictId()!=null){
				District district=new District();
				district.setDistrictId(studentApplicationDTO.getPresentDistrictId());
				district.setDistrictName(studentApplicationDTO.getPresentDistrictName());
				presentCity.setDistrict(district);
				studentApplication.setPresentDistrict(district);
				
				if(studentApplicationDTO.getPresentStateId()!=null){
					State state=new State();
					state.setStateId(studentApplicationDTO.getPresentStateId());
					state.setStateName(studentApplicationDTO.getPresentStateName());
				   district.setState(state);
				   
				   if(studentApplicationDTO.getPresentCountryId()!=null) {
					   Country country=new Country();
					   country.setCountryId(studentApplicationDTO.getPresentCountryId());
					   country.setCountryName(studentApplicationDTO.getPresentCountryName());
					   state.setCountry(country);
				   }
				}
			}
		}
	}

	private void setStudentAppDocumentCollection(StudentApplicationDTO studentApplicationDTO,
			StudentApplication studentApplication,boolean isCreate) {
		if (!CollectionUtils.isEmpty(studentApplicationDTO.getStdAppDocCollections())) {
			List<StudentAppDocCollection> studentAppDocCollectionList = new ArrayList<>();
			for (StudentAppDocumentCollectionDTO studentAppDocumentCollectionDTO : studentApplicationDTO
					.getStdAppDocCollections()) {
				StudentAppDocCollection studentAppDocCollection = new StudentAppDocCollection();
				studentAppDocCollection.setAppDocCollId(studentAppDocumentCollectionDTO.getAppDocCollId());
				studentAppDocCollection.setCreatedDt(studentApplicationDTO.getCreatedDate());
				studentAppDocCollection.setCreatedUser(studentApplicationDTO.getCreatedUser());
				studentAppDocCollection.setUpdatedDt(new Date());
				studentAppDocCollection.setUpdatedUser(SecurityUtil.getCurrentUser());
				if(isCreate) {
					studentAppDocCollection.setCreatedDt(new Date());
					studentAppDocCollection.setCreatedUser(SecurityUtil.getCurrentUser());
					
				}
				studentAppDocCollection.setIsActive(studentAppDocumentCollectionDTO.getIsActive());
				studentAppDocCollection.setFileName(studentAppDocumentCollectionDTO.getFileName());
				//
				studentAppDocCollection.setFilePath(FileUtil.getRelativePath(studentAppDocumentCollectionDTO.getFilePath()));//
				studentAppDocCollection.setIsActive(studentAppDocumentCollectionDTO.getIsActive());
				studentAppDocCollection.setIsHardCopy(studentAppDocumentCollectionDTO.getIsHardCopy());
				studentAppDocCollection.setIsSoftCopy(studentAppDocumentCollectionDTO.getIsSoftCopy());
				studentAppDocCollection.setIsOriginal(studentAppDocumentCollectionDTO.getIsOriginal());
				studentAppDocCollection.setIsVerified(studentAppDocumentCollectionDTO.getIsVerified());
				studentAppDocCollection.setRackNumber(studentAppDocumentCollectionDTO.getRackNumber());
				studentAppDocCollection.setReason(studentAppDocumentCollectionDTO.getReason());
				DocumentRepository documentRepository = new DocumentRepository();
				documentRepository.setDocumentRepositoryId(studentAppDocumentCollectionDTO.getDocumentRepositoryId());
				studentAppDocCollection.setDocumentRepository(documentRepository);
				studentAppDocCollection.setStudentApplication(studentApplication);
				studentAppDocCollectionList.add(studentAppDocCollection);
			}
			if (!CollectionUtils.isEmpty(studentAppDocCollectionList)) {
				studentApplication.setStdAppDocCollections(studentAppDocCollectionList);
			}
		}
	}

	
	private void setStudentAppDocumentCollection(StudentApplicationDTO studentApplicationDTO,
			StudentApplication studentApplication) {
		if (!CollectionUtils.isEmpty(studentApplication.getStdAppDocCollections())) {
			List<StudentAppDocumentCollectionDTO> studentAppDocCollections= new ArrayList<>();
			for (StudentAppDocCollection studentAppDocCollection: studentApplication
					.getStdAppDocCollections()) {
				StudentAppDocumentCollectionDTO studentAppDocCollectionDTO = new StudentAppDocumentCollectionDTO();
				studentAppDocCollectionDTO.setAppDocCollId(studentAppDocCollection.getAppDocCollId());
				//studentAppDocCollectionDTO.setCreatedDt(studentAppDocCollection.getCreatedDt());
				studentAppDocCollectionDTO.setCreatedUser(studentAppDocCollection.getCreatedUser());
				studentAppDocCollectionDTO.setIsActive(studentAppDocCollection.getIsActive());
				studentAppDocCollectionDTO.setFileName(studentAppDocCollection.getFileName());
				if(studentAppDocCollection.getFilePath()!=null) {
				StringBuilder filePath=new StringBuilder();
				
		//need modifications -abs path
				/*filePath = filePath.append(url).append(fileSeparator).append(bucketName).append(fileSeparator)
						.append(studentAppDocCollection.getFilePath());
			*/
				studentAppDocCollectionDTO.setFilePath(FileUtil.getAbsolutePath(studentAppDocCollection.getFilePath()));
				}
				
				studentAppDocCollectionDTO.setIsHardCopy(studentAppDocCollection.getIsHardCopy());
				studentAppDocCollectionDTO.setIsSoftCopy(studentAppDocCollection.getIsSoftCopy());
				studentAppDocCollectionDTO.setIsOriginal(studentAppDocCollection.getIsOriginal());
				studentAppDocCollectionDTO.setIsVerified(studentAppDocCollection.getIsVerified());
				studentAppDocCollectionDTO.setRackNumber(studentAppDocCollection.getRackNumber());
				studentAppDocCollectionDTO.setReason(studentAppDocCollection.getReason());
				if(studentAppDocCollection.getDocumentRepository()!=null) {
				studentAppDocCollectionDTO.setDocumentRepositoryId(studentAppDocCollection.getDocumentRepository().getDocumentRepositoryId());
				}
				studentAppDocCollections.add(studentAppDocCollectionDTO);
			}
			if (!CollectionUtils.isEmpty(studentAppDocCollections)) {
				studentApplicationDTO.setStdAppDocCollections(studentAppDocCollections);
			}
		}
	}
	
	
	public List<StudentApplicationDTO> convertStudentApplicationListToStudentApplicationDTOList(
			List<StudentApplication> studentApplicationList) {
		List<StudentApplicationDTO> studentApplicationDTOList = new ArrayList<>();
		studentApplicationList.forEach(studentApplication -> studentApplicationDTOList
				.add(convertStudentApplicationToStudentApplicationDTO(studentApplication)));
		return studentApplicationDTOList;
	}

	@Override
	public StudentApplicationDTO convertEntityToDTO(StudentApplication e) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<StudentApplicationDTO> convertEntityListToDTOList(List<StudentApplication> entityList) {
		List<StudentApplicationDTO> dtoObjectList = new ArrayList<>();
		entityList.forEach(entityObject -> dtoObjectList.add(convertStudentApplicationToStudentApplicationDTO(entityObject)));
		return dtoObjectList;
	}

	@Override
	public List<StudentApplication> convertDTOListToEntityList(List<StudentApplicationDTO> dtoList) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StudentApplication convertDTOtoEntity(StudentApplicationDTO d, StudentApplication e) {
		// TODO Auto-generated method stub
		return null;
	}
	
	


}
