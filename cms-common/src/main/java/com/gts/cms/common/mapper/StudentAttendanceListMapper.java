package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.StudentAttendanceListDTO;
import com.gts.cms.entity.AcademicYear;
import com.gts.cms.entity.GroupSection;
import com.gts.cms.entity.StudentDetail;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Genesis
 *
 */
@Component
public class StudentAttendanceListMapper implements BaseMapper<StudentDetail, StudentAttendanceListDTO> {

	/*@Autowired
	StudentAttendanceMapper studentAttendanceMapper;*/

	@Override
	public StudentAttendanceListDTO convertEntityToDTO(StudentDetail entity) {
		StudentAttendanceListDTO studentAttendanceListDTO = null;
		if (entity != null) {
			studentAttendanceListDTO = new StudentAttendanceListDTO();
			studentAttendanceListDTO.setStudentId(entity.getStudentId());
			studentAttendanceListDTO.setApplicationNo(entity.getApplicationNo());
			studentAttendanceListDTO.setAdmissionNumber(entity.getAdmissionNumber());
			studentAttendanceListDTO.setDateOfBirth(entity.getDateOfBirth());
			studentAttendanceListDTO.setFirstName(entity.getFirstName());
			studentAttendanceListDTO.setHallticketNumber(entity.getHallticketNumber());
			studentAttendanceListDTO.setIsActive(entity.getIsActive());
			studentAttendanceListDTO.setLastName(entity.getLastName());
			studentAttendanceListDTO.setMiddleName(entity.getMiddleName());
			studentAttendanceListDTO.setMobile(entity.getMobile());
			studentAttendanceListDTO.setRollNumber(entity.getRollNumber());
			if (entity.getGroupSection() != null) {
				studentAttendanceListDTO.setGroupSectionId(entity.getGroupSection().getGroupSectionId());
			}

			if (entity.getBatch() != null) {
				studentAttendanceListDTO.setBatchId(entity.getBatch().getBatchId());
			}

			if (entity.getAcademicYear() != null) {
				studentAttendanceListDTO.setAcademicYearId(entity.getAcademicYear().getAcademicYearId());
			}

			/*if (entity.getStdAttendances() != null) {
				studentAttendanceListDTO.setStudentAttendances(
						studentAttendanceMapper.convertEntityListToDTOList(entity.getStdAttendances()));
			}*/

		}
		return studentAttendanceListDTO;
	}

	@Override
	public List<StudentAttendanceListDTO> convertEntityListToDTOList(List<StudentDetail> entityList) {
		List<StudentAttendanceListDTO> studentAttendanceListDTOs = new ArrayList<>();
		entityList.forEach(studentDetail -> studentAttendanceListDTOs.add(convertEntityToDTO(studentDetail)));
		return studentAttendanceListDTOs;
	}

	@Override
	public List<StudentDetail> convertDTOListToEntityList(List<StudentAttendanceListDTO> dtoList) {
		List<StudentDetail> studentDetailList = new ArrayList<>();
		dtoList.forEach(studentDetailDto -> studentDetailList.add(convertDTOtoEntity(studentDetailDto, null)));
		return studentDetailList;
	}

	@Override
	public StudentDetail convertDTOtoEntity(StudentAttendanceListDTO dto, StudentDetail entity) {
		if (dto != null) {
			entity = new StudentDetail();
			entity.setStudentId(dto.getStudentId());
			entity.setApplicationNo(dto.getApplicationNo());
			entity.setAdmissionNumber(dto.getAdmissionNumber());
			entity.setDateOfBirth(dto.getDateOfBirth());
			entity.setFirstName(dto.getFirstName());
			entity.setHallticketNumber(dto.getHallticketNumber());
			entity.setIsActive(dto.getIsActive());
			entity.setLastName(dto.getLastName());
			entity.setMiddleName(dto.getMiddleName());
			entity.setMobile(dto.getMobile());
			entity.setRollNumber(dto.getRollNumber());
			GroupSection groupSection = new GroupSection();
			groupSection.setGroupSectionId(dto.getGroupSectionId());
			entity.setGroupSection(groupSection);


			AcademicYear academicYear = new AcademicYear();
			academicYear.setAcademicYearId(dto.getAcademicYearId());
			entity.setAcademicYear(academicYear);

		}
		return entity;
	}
}
