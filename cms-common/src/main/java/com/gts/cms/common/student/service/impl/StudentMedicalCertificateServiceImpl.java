package com.gts.cms.common.student.service.impl;

import com.gts.cms.entity.StudentMedicalCertificate;
import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.StudentMedicalCertificateDTO;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.repository.ConfigAutonumberRepository;
import com.gts.cms.common.service.FileService;
import com.gts.cms.common.student.dto.StudentMedicalCertificateUploadDTO;
import com.gts.cms.common.student.mapper.StudentMedicalCertificateMapper;
import com.gts.cms.common.student.repository.StudentMedicalCertificateRepository;
import com.gts.cms.common.student.service.StudentMedicalCertificateService;
import com.gts.cms.common.util.FileUtil;
import com.gts.cms.common.util.SecurityUtil;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Genesis
 *
 */
@Service
public class StudentMedicalCertificateServiceImpl implements StudentMedicalCertificateService {
	private static final Logger LOGGER = Logger.getLogger(StudentMedicalCertificateServiceImpl.class);

	private static final String FILE_ORG = "org_";
	private static final String FILE_COLLEGE = "school_";
	private static final String FILE_MEDI = "MedicalCertificates_";

	private static final String FILE_STD_MEDI = "StudentMedical_";

	private static final String FILE_MEDI1 = "medicalcertificate1.";
	private static final String FILE_MEDI2 = "medicalcertificate2.";
	private static final String FILE_MEDI3 = "medicalcertificate3.";

	private static final String FILE_SEPARATOR = "file.separator";

	private static final String fileSeparator = FileUtil.getFileSeparator();

	@Autowired
	private StudentMedicalCertificateMapper studentMedicalCertificateMapper;

	@Autowired
	StudentMedicalCertificateRepository studentMedicalCertificateRepository;

	@Autowired
	ConfigAutonumberRepository configAutoNumberRepository;

	@Autowired
	FileService fileService;

	@Override
	@Transactional
	public ApiResponse<Long> addStudentMedicalCertificateForm(
			StudentMedicalCertificateDTO studentMedicalCertificateDTO) {

		ApiResponse<Long> response = null;

		try {
			StudentMedicalCertificate studentMedicalCertificate = studentMedicalCertificateMapper
					.convertDTOtoEntity(studentMedicalCertificateDTO, null);

			studentMedicalCertificate = studentMedicalCertificateRepository.save(studentMedicalCertificate);
			studentMedicalCertificateRepository.flush();
			if (studentMedicalCertificate != null
					&& studentMedicalCertificate.getStudentMedicalCertificateId() != null) {
				studentMedicalCertificateDTO
						.setStudentMedicalCertificateId(studentMedicalCertificate.getStudentMedicalCertificateId());
				response = new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(),
						studentMedicalCertificate.getStudentMedicalCertificateId());
			} else {
				throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue());
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occured while adding student : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}
		return response;

	}

	@Override
	public ApiResponse<StudentMedicalCertificateDTO> getStudentMedicalCertificateForm(
			Long studentMedicalCertificateId) {
		ApiResponse<StudentMedicalCertificateDTO> response = null;
		try {
			StudentMedicalCertificate studentMedicalCertificate = studentMedicalCertificateRepository
					.getOne(studentMedicalCertificateId);
			if (studentMedicalCertificate != null) {
				StudentMedicalCertificateDTO studentMedicalCertificateDto = studentMedicalCertificateMapper
						.convertEntityToDTO(studentMedicalCertificate);
				response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), studentMedicalCertificateDto);
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
						HttpStatus.OK.value());
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occured while getting StudentMedicalCertificate : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;
	}

	@Override
	public ApiResponse<List<StudentMedicalCertificateDTO>> getAllEnquiryForms(Long schoolId, Long academicYearId,
			Long courseYearId, Long groupSectionId, Long studentId, Boolean status) {
		ApiResponse<List<StudentMedicalCertificateDTO>> response = null;
		List<StudentMedicalCertificate> studentMedicalCertificateList = studentMedicalCertificateRepository
				.findAllEnquiryForms(schoolId, academicYearId, courseYearId, groupSectionId, studentId, status);
		if (!CollectionUtils.isEmpty(studentMedicalCertificateList)) {
			List<StudentMedicalCertificateDTO> studentMedicalCertificateListDto = studentMedicalCertificateMapper
					.convertEntityListToDTOList(studentMedicalCertificateList);
			response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), studentMedicalCertificateListDto);
		} else {
			response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(), HttpStatus.OK.value());
		}
		return response;
	}

	@Override
	public ApiResponse<Long> updateStudentMedicalCertificateForm(
			StudentMedicalCertificateDTO studentMedicalCertificateDTO) {
		ApiResponse<Long> response = null;
		StudentMedicalCertificate studentMedicalCertificate = studentMedicalCertificateRepository
				.getOne(studentMedicalCertificateDTO.getStudentMedicalCertificateId()); // modify the
		// code here
		try {
			studentMedicalCertificate = studentMedicalCertificateMapper.convertDTOtoEntity(studentMedicalCertificateDTO,
					studentMedicalCertificate);
			// we need to get SchoolId based on user_id
			if (studentMedicalCertificate != null) {

				studentMedicalCertificate = studentMedicalCertificateRepository.save(studentMedicalCertificate);
				studentMedicalCertificateRepository.flush();
				if (studentMedicalCertificate != null
						&& studentMedicalCertificate.getStudentMedicalCertificateId() != null) {
					response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(),
							studentMedicalCertificate.getStudentMedicalCertificateId());

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception Occured while updateStudentMedicalCertificateForm : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;

	}

	@Override
	@Transactional
	public ApiResponse<StudentMedicalCertificateUploadDTO> uploadMedicalCertificates(
			StudentMedicalCertificateUploadDTO studentMedicalCertificateUploadDTO) {
		ApiResponse<StudentMedicalCertificateUploadDTO> response = null;

		if (null == studentMedicalCertificateUploadDTO
				|| null == studentMedicalCertificateUploadDTO.getStudentMedicalCertificateId() || null == studentMedicalCertificateUploadDTO.getStudentId()) {
			throw new ApiException(Messages.RECORD_ID_MUST_NOT_NULL.getValue());
		}
		try {
			String fileName1 = null;
			String fileName2 = null;
			String fileName3 = null;
			if (studentMedicalCertificateUploadDTO.getFiles() == null) {
				throw new ApiException(Messages.NO_FILES_FOUND.getValue());
			} else {

				MultipartFile file1 = studentMedicalCertificateUploadDTO.getFiles().get("file1");
				MultipartFile file2 = studentMedicalCertificateUploadDTO.getFiles().get("file2");
				MultipartFile file3 = studentMedicalCertificateUploadDTO.getFiles().get("file3");

				String orgCode = SecurityUtil.getOrgCode();
				String schoolCode = SecurityUtil.getSchoolCode();
				
				if (file1 != null) {
					try {
						fileName1 = fileService.uploadFile(file1, FILE_ORG + orgCode + fileSeparator + FILE_COLLEGE + schoolCode + fileSeparator +FILE_MEDI+studentMedicalCertificateUploadDTO.getStudentId() + fileSeparator + FILE_STD_MEDI
								+ studentMedicalCertificateUploadDTO.getStudentMedicalCertificateId() + fileSeparator
								+ FILE_MEDI1 + FilenameUtils.getExtension(file1.getOriginalFilename()));
						studentMedicalCertificateUploadDTO.setFileStatus1(Status.SUCESS.getName());

					} catch (Exception e) {
						studentMedicalCertificateUploadDTO.setFileStatus1(Messages.UPLOAD_AADHAR_FAILURE.getValue());
					}
				}

				if (file2 != null) {
					try {
						fileName2 = fileService.uploadFile(file2, FILE_MEDI + fileSeparator + FILE_STD_MEDI
								+ studentMedicalCertificateUploadDTO.getStudentMedicalCertificateId() + fileSeparator
								+ FILE_MEDI2 + FilenameUtils.getExtension(file2.getOriginalFilename()));
						studentMedicalCertificateUploadDTO.setFileStatus2(Status.SUCESS.getName());
					} catch (Exception e) {
						studentMedicalCertificateUploadDTO.setFileStatus2(Messages.UPLOAD_PAN_FAILURE.getValue());
					}
				}

				if (file3 != null) {
					try {
						fileName3 = fileService.uploadFile(file3, FILE_MEDI + fileSeparator + FILE_STD_MEDI
								+ studentMedicalCertificateUploadDTO.getStudentMedicalCertificateId() + fileSeparator
								+ FILE_MEDI3 + FilenameUtils.getExtension(file3.getOriginalFilename()));
						studentMedicalCertificateUploadDTO.setFileStatus3(Status.SUCESS.getName());
					} catch (Exception e) {
						studentMedicalCertificateUploadDTO.setFileStatus3(Messages.UPLOAD_PASSPORT_FAILURE.getValue());
					}
				}
				studentMedicalCertificateUploadDTO.setFiles(null);

				if (fileName1 != null || fileName2 != null || fileName3 != null) {
					try {
						Long updatedCount = studentMedicalCertificateRepository.uploadMedicalCertificateFiles(
								studentMedicalCertificateUploadDTO.getStudentMedicalCertificateId(), fileName1,
								fileName2, fileName3);

						if (updatedCount != null && updatedCount != 0) {
							response = new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(),
									studentMedicalCertificateUploadDTO, HttpStatus.OK.value());
						} else {
							throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue());
						}
					} catch (Exception e) {
						response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(),
								studentMedicalCertificateUploadDTO, HttpStatus.OK.value());
					}
				} else {
					response = new ApiResponse<>(Messages.FAILED_TO_UPLOAD_IMAGES.getValue(),
							studentMedicalCertificateUploadDTO, HttpStatus.OK.value());
				}
			}

		} catch (Exception e) {
			LOGGER.error("Exception Occured while upload EmployeeApplicationFiles: " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}
		return response;
	}

}