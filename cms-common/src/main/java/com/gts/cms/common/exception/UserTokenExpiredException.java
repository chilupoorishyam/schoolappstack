package com.gts.cms.common.exception;

/**
 * created by sathish on 02/09/2018.
 */

public class UserTokenExpiredException extends RuntimeException {


	private static final long serialVersionUID = 1L;
	public UserTokenExpiredException() {
		super();
	}

	public UserTokenExpiredException(String message) {
		super(message);
	}

}
