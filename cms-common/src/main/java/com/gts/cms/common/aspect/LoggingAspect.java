package com.gts.cms.common.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {
	@Before("execution(* *(..)) && (within(*..service..*.*))")
	public void loggingBeforeAdvice(JoinPoint joinPoint){
		final Logger logger = Logger.getLogger(joinPoint.getTarget().getClass());
		final String methodName = joinPoint.getSignature().getName();
		logger.debug("Entered into " + methodName+"()");
	}
	
	@After("execution(* *(..)) && (within(*..service..*.*))")
	public void loggingAfterAdvice(JoinPoint joinPoint){
		final Logger logger = Logger.getLogger(joinPoint.getTarget().getClass());
		final String methodName = joinPoint.getSignature().getName();
		logger.debug(methodName+"() executed.");
	}
	
	
}
