package com.gts.cms.common.employee.dto;

import com.gts.cms.common.dto.EmployeeAttendanceDTO;

import java.util.List;

public class EmployeeAttendanceListDTO {

	private Long employeeId;
	
	private String email;

	private String empNumber;
	
	private String firstName;

	private Long biometricId;

	private Long reportingManagerId;

	private String lastName;

	private String middleName;

	private String mobile;

	private String photoPath;

	private Long organizationId;

	private Long districtId;

	private Long empStatusId;

	private Long empTypeId;

	private Long empDeptId;

	private Long empWorkingDeptId;

	private Long designationId;

	private Long workingDesignationId;

	private Long schoolId;

	private Long teachingforId;

	private Long genderId;
	
	private String gender;

	
	private List<EmployeeAttendanceDTO> employeeAttendances;
	
	public Long getEmployeeId() {
		return employeeId;
	}

	public String getEmail() {
		return email;
	}

	public String getEmpNumber() {
		return empNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public Long getBiometricId() {
		return biometricId;
	}

	public Long getReportingManagerId() {
		return reportingManagerId;
	}

	public String getLastName() {
		return lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public String getMobile() {
		return mobile;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public Long getDistrictId() {
		return districtId;
	}

	public Long getEmpStatusId() {
		return empStatusId;
	}

	public Long getEmpTypeId() {
		return empTypeId;
	}

	public Long getEmpDeptId() {
		return empDeptId;
	}

	public Long getEmpWorkingDeptId() {
		return empWorkingDeptId;
	}

	public Long getDesignationId() {
		return designationId;
	}

	public Long getWorkingDesignationId() {
		return workingDesignationId;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public Long getTeachingforId() {
		return teachingforId;
	}

	public Long getGenderId() {
		return genderId;
	}

	public String getGender() {
		return gender;
	}

	public List<EmployeeAttendanceDTO> getEmployeeAttendances() {
		return employeeAttendances;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEmpNumber(String empNumber) {
		this.empNumber = empNumber;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setBiometricId(Long biometricId) {
		this.biometricId = biometricId;
	}

	public void setReportingManagerId(Long reportingManagerId) {
		this.reportingManagerId = reportingManagerId;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public void setDistrictId(Long districtId) {
		this.districtId = districtId;
	}

	public void setEmpStatusId(Long empStatusId) {
		this.empStatusId = empStatusId;
	}

	public void setEmpTypeId(Long empTypeId) {
		this.empTypeId = empTypeId;
	}

	public void setEmpDeptId(Long empDeptId) {
		this.empDeptId = empDeptId;
	}

	public void setEmpWorkingDeptId(Long empWorkingDeptId) {
		this.empWorkingDeptId = empWorkingDeptId;
	}

	public void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}

	public void setWorkingDesignationId(Long workingDesignationId) {
		this.workingDesignationId = workingDesignationId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public void setTeachingforId(Long teachingforId) {
		this.teachingforId = teachingforId;
	}

	public void setGenderId(Long genderId) {
		this.genderId = genderId;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setEmployeeAttendances(List<EmployeeAttendanceDTO> employeeAttendances) {
		this.employeeAttendances = employeeAttendances;
	}


}
