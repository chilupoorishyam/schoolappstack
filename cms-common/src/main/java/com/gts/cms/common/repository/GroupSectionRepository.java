package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.GroupSectionRepositoryCustom;
import com.gts.cms.entity.GroupSection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * created by Naveen(Auto) on 02/09/2018
 * 
 */
@Repository
public interface GroupSectionRepository extends JpaRepository<GroupSection, Long>, GroupSectionRepositoryCustom {

	@Query("SELECT DISTINCT gs FROM  GroupSection gs" 
			+ " JOIN FETCH gs.schedules s "
			+ " WHERE gs.groupSectionId IN (:groupSectionIds)"
			+ " AND s.timetable.startDate <= :endDate AND s.timetable.endDate >= :startDate"
			+ " AND s.isActive = true"
			+ " AND s.timetable.isActive = true"
			)
	List<GroupSection> checkSectionAvailabilityForSchedule(
			@Param("groupSectionIds") List<Long>  groupSectionIds,
			@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);
	
	/*public Optional<GroupSection> findByGroupSectionIdAndIsActiveTrue(Long groupSectionId);
	
	public List<GroupSection> findByIsActiveTrue();*/
	@Query("SELECT DISTINCT gs FROM  GroupSection gs" 
			+ " WHERE gs.school.schoolId = :schoolId "
			+ " AND gs.academicYear.academicYearId = :academicYearId"
			+ " AND gs.courseYear.courseYearId = :courseYearId  "
			+ " AND gs.section = :section  "
			+ " AND gs.isActive = true"
			)
	public List<GroupSection> findByIsActiveTrue(
			@Param("schoolId") Long  schoolId,
			@Param("academicYearId") Long  academicYearId,
			@Param("courseYearId") Long  courseYearId,
			@Param("section") String section
			);
	@Query("SELECT groupSectionId FROM  GroupSection g " 
			+ " WHERE g.school.schoolId = :schoolId "
			+ " AND g.courseYear.courseYearId = :courseYearId  "
			+ " AND g.isActive = true"
			)
	List<Long> getGroupSectionByCourseYearId(
			@Param("schoolId") Long schoolId,
			@Param("courseYearId") Long courseYearId
			);



}
