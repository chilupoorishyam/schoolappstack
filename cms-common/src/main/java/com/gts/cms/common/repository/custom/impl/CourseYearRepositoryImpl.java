package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.CourseYearRepositoryCustom;
import com.gts.cms.entity.CourseYear;
import com.gts.cms.entity.QCourseYear;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * created by Naveen on 04/09/2018
 * 
 */
@Repository
public class CourseYearRepositoryImpl extends QuerydslRepositorySupport implements CourseYearRepositoryCustom {
	public CourseYearRepositoryImpl() {
		super(CourseYear.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public Long deleteCourseYear(Long courseYearId) {
		return update(QCourseYear.courseYear).where(QCourseYear.courseYear.courseYearId.eq(courseYearId)).set(QCourseYear.courseYear.isActive, false)
				.execute();
	}
	
	@Override
	public List<CourseYear> findByCourseId(Long courseId) {

		JPQLQuery<CourseYear> query = from(QCourseYear.courseYear)
				.where(QCourseYear.courseYear.school.schoolId.eq(courseId));
		List<CourseYear> courseYearList = query.fetch();
		return courseYearList;

	}
	

}
