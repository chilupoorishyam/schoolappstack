package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.GeneralSettingDTO;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.GeneralSetting;
import com.gts.cms.entity.School;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class GeneralSettingMapper implements BaseMapper<GeneralSetting, GeneralSettingDTO> {

	@Override
	public GeneralSettingDTO convertEntityToDTO(GeneralSetting generalSetting) {
		GeneralSettingDTO generalSettingDTO = null;
		if (generalSetting != null) {
			generalSettingDTO = new GeneralSettingDTO();
			generalSettingDTO.setGeneralSettingId(generalSetting.getGeneralSettingId());

			if (generalSetting.getSchool() != null) {
				generalSettingDTO.setSchoolId(generalSetting.getSchool().getSchoolId());
				generalSettingDTO.setSchoolName(generalSetting.getSchool().getSchoolName());
				generalSettingDTO.setSchoolCode(generalSetting.getSchool().getSchoolCode());
			}

			generalSettingDTO.setSettingName(generalSetting.getSettingName());
			generalSettingDTO.setSettingCode(generalSetting.getSettingCode());
			generalSettingDTO.setSettingValue(generalSetting.getSettingValue());
			generalSettingDTO.setIsActive(generalSetting.getIsActive());
			generalSettingDTO.setReason(generalSetting.getReason());
		}
		return generalSettingDTO;

	}

	@Override
	public List<GeneralSettingDTO> convertEntityListToDTOList(List<GeneralSetting> generalSettingList) {
		List<GeneralSettingDTO> generalSettingDTOList = new ArrayList<>();
		generalSettingList.forEach(generalSetting -> generalSettingDTOList.add(convertEntityToDTO(generalSetting)));
		return generalSettingDTOList;
	}

	@Override
	public List<GeneralSetting> convertDTOListToEntityList(List<GeneralSettingDTO> generalSettingDTOList) {
		List<GeneralSetting> generalSettingList = new ArrayList<>();
		generalSettingDTOList
				.forEach(generalSettingDTO -> generalSettingList.add(convertDTOtoEntity(generalSettingDTO, null)));
		return generalSettingList;

	}

	@Override
	public GeneralSetting convertDTOtoEntity(GeneralSettingDTO generalSettingDTO, GeneralSetting generalSetting) {
		if (null == generalSetting) {
			generalSetting = new GeneralSetting();
			generalSetting.setIsActive(Status.ACTIVE.getId());
			generalSetting.setCreatedDt(new Date());
			generalSetting.setCreatedUser(SecurityUtil.getCurrentUser());
		} else {
			generalSetting.setIsActive(generalSettingDTO.getIsActive());
		}
		generalSetting.setGeneralSettingId(generalSettingDTO.getGeneralSettingId());
		if (generalSettingDTO.getSchoolId() != null) {
			School school = new School();
			school.setSchoolId(generalSettingDTO.getSchoolId());
			school.setSchoolName(generalSettingDTO.getSchoolName());
			generalSetting.setSchool(school);
		}
		generalSetting.setSettingName(generalSettingDTO.getSettingName());
		generalSetting.setSettingCode(generalSettingDTO.getSettingCode());
		generalSetting.setSettingValue(generalSettingDTO.getSettingValue());
		generalSetting.setReason(generalSettingDTO.getReason());
		generalSetting.setUpdatedDt(new Date());
		generalSetting.setUpdatedUser(SecurityUtil.getCurrentUser());
		
		
		return generalSetting;

	}

}
