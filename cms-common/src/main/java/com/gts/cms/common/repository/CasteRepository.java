package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.CasteRepositoryCustom;
import com.gts.cms.entity.Caste;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * created by Naveen(Auto) on 02/09/2018
 * 
 */
@Repository
public interface CasteRepository extends JpaRepository<Caste, Long>, CasteRepositoryCustom {


}
