package com.gts.cms.common.student.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.Map;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentDetailStgDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long studentId;
    private String school;
    private String course;
    private String group;
    private String courseYear;
    private String section;
    private String quota;
    private String regulation;
    private String batch;
    private String academicYear;
    private String rollNo;
    private String hallTicketNumber;
    private String firstName;
    private String middleName;
    private String lastName;
    private String gender;
    private String dateOfBirth;
    private String mobile;
    private String studentEmailID;
    private String aadhaarNo;
    private String fatherName;
    private String fatherMobile;
    private String motherName;
    private String motherMobile;
    private String permanentAddress;
    private String city;
    private String district;
    @JsonIgnore
    private Map<String, MultipartFile> files;
}