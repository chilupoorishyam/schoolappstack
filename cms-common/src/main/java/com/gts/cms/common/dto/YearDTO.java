package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Set;

public class YearDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Set<Integer> yearSet;

	public Set<Integer> getYearSet() {
		return yearSet;
	}

	public void setYearSet(Set<Integer> yearSet) {
		this.yearSet = yearSet;
	}
}
