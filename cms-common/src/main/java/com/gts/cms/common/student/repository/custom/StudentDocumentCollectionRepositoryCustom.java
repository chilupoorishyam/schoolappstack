package com.gts.cms.common.student.repository.custom;

import com.gts.cms.entity.StudentDocumentCollection;

import java.util.List;

public interface StudentDocumentCollectionRepositoryCustom {
	Long updateStudentDocumentFiles(Long studentId, String docRepId, String docPath);
	List<StudentDocumentCollection> findDocCollectionByStdId(Long studentId);
}
