package com.gts.cms.common.repository;

import com.gts.cms.entity.PasswordResetToken;
import com.gts.cms.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

/**
 * created by Raghu on 02/09/2018
 */
@Repository

public interface PasswordResetTokenRepository extends JpaRepository<PasswordResetToken, Long> {

    @Query("SELECT u FROM PasswordResetToken u"
            + " WHERE ( :userId is null or u.user.userId=:userId )"
            + " AND u.isActive = true" +
            " AND expiry_date > CURRENT_DATE order by u.passwordResetTokenId desc"
    )
    List<PasswordResetToken> findTokenByUser(Long userId);

    @Query("SELECT u FROM PasswordResetToken u"
            + " WHERE ( :userId is null or u.user.userId=:userId )"
            + " AND ( u.token = :token)"
            + " AND u.isActive = true order by u.passwordResetTokenId desc"
    )
    List<PasswordResetToken> findByUserAndToken(Long userId, String token);

    PasswordResetToken findByUser(User user);

    Stream<PasswordResetToken> findAllByExpiryDateLessThan(Date now);

    void deleteByExpiryDateLessThan(Date now);

    @Modifying
    @Query("delete from PasswordResetToken t where t.expiryDate <= ?1")
    void deleteAllExpiredSince(Date now);

}
