package com.gts.cms.common.repository;

import com.gts.cms.entity.FinTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface FinTransactionRepository extends JpaRepository<FinTransaction, Long> {
}