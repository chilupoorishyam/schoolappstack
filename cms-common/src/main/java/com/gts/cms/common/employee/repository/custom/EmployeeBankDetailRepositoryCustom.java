package com.gts.cms.common.employee.repository.custom;

import com.gts.cms.entity.EmployeeBankDetail;

import java.util.List;

/**
 * @author Genesis
 *
 */
public interface EmployeeBankDetailRepositoryCustom {

	List<EmployeeBankDetail> findAllEmployeeBankDetail(Long employeeId, Boolean true1);
}
