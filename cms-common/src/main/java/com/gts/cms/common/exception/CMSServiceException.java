package com.gts.cms.common.exception;

/**
 * @author Shyamsunder
 */
public class CMSServiceException
  extends RuntimeException
{

  /**
   * The Constructor.
   */
  CMSServiceException()
  {
    super();
  }

  /**
   * The Constructor.
   * @param message
   *          the message
   * @param cause
   *          the cause
   */
  public CMSServiceException(final String message, final Throwable cause)
  {
    super( message, cause );
  }
}
