package com.gts.cms.common.student.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class StudentEducationDetailDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long studentEducationId;

	private String address;

	private String board;

	private String gradeClassSecured;

	private String yearOfCompletion;
	
	private String majorSubjects;

	private String medium;

	private String nameOfInstitution;

	private BigDecimal precentage;

	private Boolean isActive;
	
	private String reason;

	private Date createdDt;

	private Long createdUser;
	
	private Date updatedDt;

	private Long updatedUser;
	
	private Long studentId;
	private String firstName;
	private String middleName;
	private String lastName;
	
	private Long generalDetailId;
	private String generalDetailCode;
	private String generalDetailName;
	
	private Long appEducationId;

	public Long getStudentEducationId() {
		return studentEducationId;
	}

	public void setStudentEducationId(Long studentEducationId) {
		this.studentEducationId = studentEducationId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public String getGradeClassSecured() {
		return gradeClassSecured;
	}

	public void setGradeClassSecured(String gradeClassSecured) {
		this.gradeClassSecured = gradeClassSecured;
	}

	public String getYearOfCompletion() {
		return yearOfCompletion;
	}

	public void setYearOfCompletion(String yearOfCompletion) {
		this.yearOfCompletion = yearOfCompletion;
	}

	public String getMajorSubjects() {
		return majorSubjects;
	}

	public void setMajorSubjects(String majorSubjects) {
		this.majorSubjects = majorSubjects;
	}

	public String getMedium() {
		return medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

	public String getNameOfInstitution() {
		return nameOfInstitution;
	}

	public void setNameOfInstitution(String nameOfInstitution) {
		this.nameOfInstitution = nameOfInstitution;
	}

	public BigDecimal getPrecentage() {
		return precentage;
	}

	public void setPrecentage(BigDecimal precentage) {
		this.precentage = precentage;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getGeneralDetailId() {
		return generalDetailId;
	}

	public void setGeneralDetailId(Long generalDetailId) {
		this.generalDetailId = generalDetailId;
	}

	public String getGeneralDetailCode() {
		return generalDetailCode;
	}

	public void setGeneralDetailCode(String generalDetailCode) {
		this.generalDetailCode = generalDetailCode;
	}

	public String getGeneralDetailName() {
		return generalDetailName;
	}

	public void setGeneralDetailName(String generalDetailName) {
		this.generalDetailName = generalDetailName;
	}

	public Long getAppEducationId() {
		return appEducationId;
	}

	public void setAppEducationId(Long appEducationId) {
		this.appEducationId = appEducationId;
	}
}