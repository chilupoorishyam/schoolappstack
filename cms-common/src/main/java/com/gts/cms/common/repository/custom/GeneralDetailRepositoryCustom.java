package com.gts.cms.common.repository.custom;

import com.gts.cms.entity.GeneralDetail;

import java.util.List;

public interface GeneralDetailRepositoryCustom {

	public GeneralDetail findByGeneralDetailIdAndIsActiveTrue(Long generalDetailId);

	public List<GeneralDetail> findByGeneralMasterIdAndIsActiveTrue(Long generalMasterId);
	
	public List<GeneralDetail> findByGeneralMasterCode(String generalMasterCode);
	
	public Long deleteGeneralDetail(Long generalDetailId);
	
	public GeneralDetail getFeeDataTypeByCode(String generalDetailCode);
	
	public GeneralDetail getGeneralDetailByCode(String generalDetailCode);

	GeneralDetail getGeneralMasterDetailByCodes(String generalDetailCode, String generalMasterCode);

}
