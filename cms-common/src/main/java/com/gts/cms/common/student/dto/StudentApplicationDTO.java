package com.gts.cms.common.student.dto;

import com.gts.cms.common.dto.StudentAppActivityDTO;
import com.gts.cms.common.dto.StudentAppEducationDTO;

import java.util.Date;
import java.util.List;

public class StudentApplicationDTO {
	private Long schoolId;
	private String schoolName;
	private String schoolCode;
	private Long organizationId;
	private String organizationCode;
	private String organizationName;
	private String applicationNumber;
	private String biometricNo;
	private Long courseId;
	private String courseName;
	private String quotaCode;
	private Long courseYearId;
	private String courseYearName;
	private Long quotaId;
	private Boolean isCurrentYear;
	private Boolean isLateral;
	private Boolean isScholorship;
	private Long batchId;
	private String batchName;
	private String refApplicationNo;
	private Date dateOfRegistration;
	private String receiptNo;
	private Long studentTypeId;
	private String studentTypeName;
	private String firstName;
	private String lastName;
	private String middleName;
	private Long genderId;
	private String genderName;
	private Date dob;
	private Long disabilityId;
	private String disabilityName;
	private String eamcetRank;
	private String entranceHTNumber;
	private String identificationMarks;
	private String mobile;
	private String email;
	private Long casteId;
	private String casteName;
	private Long subCasteId;
	private String subCasteName;
	private Long nationalityId;
	private String nationality;
	private Long religionId;
	private String religion;

	private Date dateOfIssue;
	private Date dateOfExpiry;
	private Long bloodGroupId;
	private String bloodGroup;
	private Long titleId;
	private String title;
	private String studentPhotoPath;
	private String folderPath;

	private Long studentAppId;

	private String aadharCardNo;

	private String aadharFilePath;

	private Date adminssionDate;

	private String admissionNumber;
	private Date createdDate;
	private Long createdUser;

	private Long presentCityId;
	private String presentCityName;

	private Long permanentCityId;
	private String permanentCityName;

	private Long presentDistrictId;
	private String presentDistrictName;

	private Long permanentDistrictId;
	private String permanentDistrictName;

	private Long permanentStateId;
	private String permanentStateName;

	private Long permanentCountryId;
	private String permanentCountryName;

	private Long presentStateId;
	private String presentStateName;

	private Long presentCountryId;
	private String presentCountryName;

	private String fatherAddress;

	private String fatherEmailId;

	private String fatherMobileNo;

	private String fatherName;

	private String fatherOccupation;

	private String fatherPhotoPath;

	private String fatherQualification;

	private String fathersIncomePa;

	private String guardianAddress;

	private String guardianEmailId;

	private String guardianIncomePa;

	private String guardianMobileNo;

	private String guardianName;

	private String hallticketNumber;

	private String hobbies;

	private String interests;

	private Boolean isActive;

	private Boolean isLocal;

	private Boolean isgovtempFather;

	private Boolean isgovtempMother;

	private String langStatus1;
	private Long languageId1;
	private String langStatus2;
	private Long languageId2;
	private String langStatus3;
	private Long languageId3;
	private String langStatus4;
	private Long languageId4;
	private String langStatus5;
	private Long languageId5;
	private String motherAddress;

	private String motherEmailId;

	private String motherIncomePa;

	private String motherMobileNo;

	private String motherName;

	private String motherOccupation;

	private String motherPhotoPath;

	private String motherQualification;

	private String pancardFilePath;

	private String pancardNo;

	private String passportNo;

	private String permanentAddress;

	private String permanentMandal;

	private String permanentPincode;

	private String permanentStreet;

	private String presentAddress;

	private String presentMandal;

	private String presentPincode;

	private String presentStreetName;

	private String primaryContact;

	private String reason;

	private String residencePhone;

	private String rollNumber;

	private String sscNo;

	private String statusComments;
	private String quotaName;
	private String applicationReason;
	private boolean isCurrentStatus;

	private List<StudentAppDocumentCollectionDTO> stdAppDocCollections;

	private List<StudentAppActivityDTO> stdAppActivities;
	private List<StudentAppEducationDTO> stdAppEducations;
	private Long currentWorkflowStatusId;
	private String currentWorkflowStatusName;

	private Long toAppWorkflowStatusId;
	private String toAppWorkflowStatusName;
	private Long admissionId;

	private Date weddingDate;
	private Long academicYearId;
	private String academicYearName;
	
	private Long qualifyingId;
	private String qualifyingName;
	private String qualifyingCode;
	
	private Boolean isMinority;
	public Long getAdmissionId() {
		return admissionId;
	}

	public void setAdmissionId(Long admissionId) {
		this.admissionId = admissionId;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public String getPermanentStateName() {
		return permanentStateName;
	}

	public Long getTitleId() {
		return titleId;
	}

	public void setTitleId(Long titleId) {
		this.titleId = titleId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setPermanentStateName(String permanentStateName) {
		this.permanentStateName = permanentStateName;
	}

	public String getPermanentCountryName() {
		return permanentCountryName;
	}

	public void setPermanentCountryName(String permanentCountryName) {
		this.permanentCountryName = permanentCountryName;
	}

	public Long getPresentStateId() {
		return presentStateId;
	}

	public void setPresentStateId(Long presentStateId) {
		this.presentStateId = presentStateId;
	}

	public Long getPresentCountryId() {
		return presentCountryId;
	}

	public void setPresentCountryId(Long presentCountryId) {
		this.presentCountryId = presentCountryId;
	}

	public Long getPermanentStateId() {
		return permanentStateId;
	}

	public void setPermanentStateId(Long permanentStateId) {
		this.permanentStateId = permanentStateId;
	}

	public String getPresentStateName() {
		return presentStateName;
	}

	public void setPresentStateName(String presentStateName) {
		this.presentStateName = presentStateName;
	}

	public Long getPermanentCountryId() {
		return permanentCountryId;
	}

	public void setPermanentCountryId(Long permanentCountryId) {
		this.permanentCountryId = permanentCountryId;
	}

	public String getPresentCountryName() {
		return presentCountryName;
	}

	public void setPresentCountryName(String presentCountryName) {
		this.presentCountryName = presentCountryName;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getOrganizationCode() {
		return organizationCode;
	}

	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getApplicationNumber() {
		return applicationNumber;
	}

	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}

	public String getBiometricNo() {
		return biometricNo;
	}

	public void setBiometricNo(String biometricNo) {
		this.biometricNo = biometricNo;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public Long getCourseYearId() {
		return courseYearId;
	}

	public void setCourseYearId(Long courseYearId) {
		this.courseYearId = courseYearId;
	}

	public String getCourseYearName() {
		return courseYearName;
	}

	public void setCourseYearName(String courseYearName) {
		this.courseYearName = courseYearName;
	}

	public Long getQuotaId() {
		return quotaId;
	}

	public void setQuotaId(Long quotaId) {
		this.quotaId = quotaId;
	}

	public Boolean getIsCurrentYear() {
		return isCurrentYear;
	}

	public void setIsCurrentYear(Boolean isCurrentYear) {
		this.isCurrentYear = isCurrentYear;
	}

	public Boolean getIsLateral() {
		return isLateral;
	}

	public void setIsLateral(Boolean isLateral) {
		this.isLateral = isLateral;
	}

	public Boolean getIsScholorship() {
		return isScholorship;
	}

	public void setIsScholorship(Boolean isScholorship) {
		this.isScholorship = isScholorship;
	}

	public Long getBatchId() {
		return batchId;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public String getRefApplicationNo() {
		return refApplicationNo;
	}

	public void setRefApplicationNo(String refApplicationNo) {
		this.refApplicationNo = refApplicationNo;
	}

	public Date getDateOfRegistration() {
		return dateOfRegistration;
	}

	public void setDateOfRegistration(Date dateOfRegistration) {
		this.dateOfRegistration = dateOfRegistration;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public Long getStudentTypeId() {
		return studentTypeId;
	}

	public void setStudentTypeId(Long studentTypeId) {
		this.studentTypeId = studentTypeId;
	}

	public String getStudentTypeName() {
		return studentTypeName;
	}

	public void setStudentTypeName(String studentTypeName) {
		this.studentTypeName = studentTypeName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Long getGenderId() {
		return genderId;
	}

	public void setGenderId(Long genderId) {
		this.genderId = genderId;
	}

	public String getGenderName() {
		return genderName;
	}

	public void setGenderName(String genderName) {
		this.genderName = genderName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Long getDisabilityId() {
		return disabilityId;
	}

	public void setDisabilityId(Long disabilityId) {
		this.disabilityId = disabilityId;
	}

	public String getDisabilityName() {
		return disabilityName;
	}

	public void setDisabilityName(String disabilityName) {
		this.disabilityName = disabilityName;
	}

	public String getEamcetRank() {
		return eamcetRank;
	}

	public void setEamcetRank(String eamcetRank) {
		this.eamcetRank = eamcetRank;
	}

	public String getEntranceHTNumber() {
		return entranceHTNumber;
	}

	public void setEntranceHTNumber(String entranceHTNumber) {
		this.entranceHTNumber = entranceHTNumber;
	}

	public String getIdentificationMarks() {
		return identificationMarks;
	}

	public void setIdentificationMarks(String identificationMarks) {
		this.identificationMarks = identificationMarks;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getCasteId() {
		return casteId;
	}

	public void setCasteId(Long casteId) {
		this.casteId = casteId;
	}

	public String getCasteName() {
		return casteName;
	}

	public void setCasteName(String casteName) {
		this.casteName = casteName;
	}

	public Long getSubCasteId() {
		return subCasteId;
	}

	public void setSubCasteId(Long subCasteId) {
		this.subCasteId = subCasteId;
	}

	public String getSubCasteName() {
		return subCasteName;
	}

	public void setSubCasteName(String subCasteName) {
		this.subCasteName = subCasteName;
	}

	public Long getNationalityId() {
		return nationalityId;
	}

	public void setNationalityId(Long nationalityId) {
		this.nationalityId = nationalityId;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Long getReligionId() {
		return religionId;
	}

	public void setReligionId(Long religionId) {
		this.religionId = religionId;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public Date getDateOfIssue() {
		return dateOfIssue;
	}

	public void setDateOfIssue(Date dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public Date getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(Date dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public Long getBloodGroupId() {
		return bloodGroupId;
	}

	public void setBloodGroupId(Long bloodGroupId) {
		this.bloodGroupId = bloodGroupId;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public String getStudentPhotoPath() {
		return studentPhotoPath;
	}

	public void setStudentPhotoPath(String studentPhotoPath) {
		this.studentPhotoPath = studentPhotoPath;
	}

	public String getFolderPath() {
		return folderPath;
	}

	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}

	public Long getStudentAppId() {
		return studentAppId;
	}

	public void setStudentAppId(Long studentAppId) {
		this.studentAppId = studentAppId;
	}

	public String getAadharCardNo() {
		return aadharCardNo;
	}

	public void setAadharCardNo(String aadharCardNo) {
		this.aadharCardNo = aadharCardNo;
	}

	public String getAadharFilePath() {
		return aadharFilePath;
	}

	public void setAadharFilePath(String aadharFilePath) {
		this.aadharFilePath = aadharFilePath;
	}

	public Date getAdminssionDate() {
		return adminssionDate;
	}

	public void setAdminssionDate(Date adminssionDate) {
		this.adminssionDate = adminssionDate;
	}

	public String getAdmissionNumber() {
		return admissionNumber;
	}

	public void setAdmissionNumber(String admissionNumber) {
		this.admissionNumber = admissionNumber;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Long getPresentCityId() {
		return presentCityId;
	}

	public void setPresentCityId(Long presentCityId) {
		this.presentCityId = presentCityId;
	}

	public String getPresentCityName() {
		return presentCityName;
	}

	public void setPresentCityName(String presentCityName) {
		this.presentCityName = presentCityName;
	}

	public Long getPermanentCityId() {
		return permanentCityId;
	}

	public void setPermanentCityId(Long permanentCityId) {
		this.permanentCityId = permanentCityId;
	}

	public String getPermanentCityName() {
		return permanentCityName;
	}

	public void setPermanentCityName(String permanentCityName) {
		this.permanentCityName = permanentCityName;
	}

	public Long getPresentDistrictId() {
		return presentDistrictId;
	}

	public void setPresentDistrictId(Long presentDistrictId) {
		this.presentDistrictId = presentDistrictId;
	}

	public String getPresentDistrictName() {
		return presentDistrictName;
	}

	public void setPresentDistrictName(String presentDistrictName) {
		this.presentDistrictName = presentDistrictName;
	}

	public Long getPermanentDistrictId() {
		return permanentDistrictId;
	}

	public void setPermanentDistrictId(Long permanentDistrictId) {
		this.permanentDistrictId = permanentDistrictId;
	}

	public String getPermanentDistrictName() {
		return permanentDistrictName;
	}

	public void setPermanentDistrictName(String permanentDistrictName) {
		this.permanentDistrictName = permanentDistrictName;
	}

	public String getFatherAddress() {
		return fatherAddress;
	}

	public void setFatherAddress(String fatherAddress) {
		this.fatherAddress = fatherAddress;
	}

	public String getFatherEmailId() {
		return fatherEmailId;
	}

	public void setFatherEmailId(String fatherEmailId) {
		this.fatherEmailId = fatherEmailId;
	}

	public String getFatherMobileNo() {
		return fatherMobileNo;
	}

	public void setFatherMobileNo(String fatherMobileNo) {
		this.fatherMobileNo = fatherMobileNo;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getFatherOccupation() {
		return fatherOccupation;
	}

	public void setFatherOccupation(String fatherOccupation) {
		this.fatherOccupation = fatherOccupation;
	}

	public String getFatherPhotoPath() {
		return fatherPhotoPath;
	}

	public void setFatherPhotoPath(String fatherPhotoPath) {
		this.fatherPhotoPath = fatherPhotoPath;
	}

	public String getFatherQualification() {
		return fatherQualification;
	}

	public void setFatherQualification(String fatherQualification) {
		this.fatherQualification = fatherQualification;
	}

	public String getFathersIncomePa() {
		return fathersIncomePa;
	}

	public void setFathersIncomePa(String fathersIncomePa) {
		this.fathersIncomePa = fathersIncomePa;
	}

	public String getGuardianAddress() {
		return guardianAddress;
	}

	public void setGuardianAddress(String guardianAddress) {
		this.guardianAddress = guardianAddress;
	}

	public String getGuardianEmailId() {
		return guardianEmailId;
	}

	public void setGuardianEmailId(String guardianEmailId) {
		this.guardianEmailId = guardianEmailId;
	}

	public String getGuardianIncomePa() {
		return guardianIncomePa;
	}

	public void setGuardianIncomePa(String guardianIncomePa) {
		this.guardianIncomePa = guardianIncomePa;
	}

	public String getGuardianMobileNo() {
		return guardianMobileNo;
	}

	public void setGuardianMobileNo(String guardianMobileNo) {
		this.guardianMobileNo = guardianMobileNo;
	}

	public String getGuardianName() {
		return guardianName;
	}

	public void setGuardianName(String guardianName) {
		this.guardianName = guardianName;
	}

	public String getHallticketNumber() {
		return hallticketNumber;
	}

	public void setHallticketNumber(String hallticketNumber) {
		this.hallticketNumber = hallticketNumber;
	}

	public String getHobbies() {
		return hobbies;
	}

	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}

	public String getInterests() {
		return interests;
	}

	public void setInterests(String interests) {
		this.interests = interests;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsLocal() {
		return isLocal;
	}

	public void setIsLocal(Boolean isLocal) {
		this.isLocal = isLocal;
	}

	public Boolean getIsgovtempFather() {
		return isgovtempFather;
	}

	public void setIsgovtempFather(Boolean isgovtempFather) {
		this.isgovtempFather = isgovtempFather;
	}

	public Boolean getIsgovtempMother() {
		return isgovtempMother;
	}

	public void setIsgovtempMother(Boolean isgovtempMother) {
		this.isgovtempMother = isgovtempMother;
	}

	public String getLangStatus1() {
		return langStatus1;
	}

	public void setLangStatus1(String langStatus1) {
		this.langStatus1 = langStatus1;
	}

	public Long getLanguageId1() {
		return languageId1;
	}

	public void setLanguageId1(Long languageId1) {
		this.languageId1 = languageId1;
	}

	public String getLangStatus2() {
		return langStatus2;
	}

	public void setLangStatus2(String langStatus2) {
		this.langStatus2 = langStatus2;
	}

	public Long getLanguageId2() {
		return languageId2;
	}

	public void setLanguageId2(Long languageId2) {
		this.languageId2 = languageId2;
	}

	public String getLangStatus3() {
		return langStatus3;
	}

	public void setLangStatus3(String langStatus3) {
		this.langStatus3 = langStatus3;
	}

	public Long getLanguageId3() {
		return languageId3;
	}

	public void setLanguageId3(Long languageId3) {
		this.languageId3 = languageId3;
	}

	public String getLangStatus4() {
		return langStatus4;
	}

	public void setLangStatus4(String langStatus4) {
		this.langStatus4 = langStatus4;
	}

	public Long getLanguageId4() {
		return languageId4;
	}

	public void setLanguageId4(Long languageId4) {
		this.languageId4 = languageId4;
	}

	public String getLangStatus5() {
		return langStatus5;
	}

	public void setLangStatus5(String langStatus5) {
		this.langStatus5 = langStatus5;
	}

	public Long getLanguageId5() {
		return languageId5;
	}

	public void setLanguageId5(Long languageId5) {
		this.languageId5 = languageId5;
	}

	public String getMotherAddress() {
		return motherAddress;
	}

	public void setMotherAddress(String motherAddress) {
		this.motherAddress = motherAddress;
	}

	public String getMotherEmailId() {
		return motherEmailId;
	}

	public void setMotherEmailId(String motherEmailId) {
		this.motherEmailId = motherEmailId;
	}

	public String getMotherIncomePa() {
		return motherIncomePa;
	}

	public void setMotherIncomePa(String motherIncomePa) {
		this.motherIncomePa = motherIncomePa;
	}

	public String getMotherMobileNo() {
		return motherMobileNo;
	}

	public void setMotherMobileNo(String motherMobileNo) {
		this.motherMobileNo = motherMobileNo;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getMotherOccupation() {
		return motherOccupation;
	}

	public void setMotherOccupation(String motherOccupation) {
		this.motherOccupation = motherOccupation;
	}

	public String getMotherPhotoPath() {
		return motherPhotoPath;
	}

	public void setMotherPhotoPath(String motherPhotoPath) {
		this.motherPhotoPath = motherPhotoPath;
	}

	public String getMotherQualification() {
		return motherQualification;
	}

	public void setMotherQualification(String motherQualification) {
		this.motherQualification = motherQualification;
	}

	public String getPancardFilePath() {
		return pancardFilePath;
	}

	public void setPancardFilePath(String pancardFilePath) {
		this.pancardFilePath = pancardFilePath;
	}

	public String getPancardNo() {
		return pancardNo;
	}

	public void setPancardNo(String pancardNo) {
		this.pancardNo = pancardNo;
	}

	public String getPassportNo() {
		return passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getPermanentMandal() {
		return permanentMandal;
	}

	public void setPermanentMandal(String permanentMandal) {
		this.permanentMandal = permanentMandal;
	}

	public String getPermanentPincode() {
		return permanentPincode;
	}

	public void setPermanentPincode(String permanentPincode) {
		this.permanentPincode = permanentPincode;
	}

	public String getPermanentStreet() {
		return permanentStreet;
	}

	public void setPermanentStreet(String permanentStreet) {
		this.permanentStreet = permanentStreet;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getPresentMandal() {
		return presentMandal;
	}

	public void setPresentMandal(String presentMandal) {
		this.presentMandal = presentMandal;
	}

	public String getPresentPincode() {
		return presentPincode;
	}

	public void setPresentPincode(String presentPincode) {
		this.presentPincode = presentPincode;
	}

	public String getPresentStreetName() {
		return presentStreetName;
	}

	public void setPresentStreetName(String presentStreetName) {
		this.presentStreetName = presentStreetName;
	}

	public String getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(String primaryContact) {
		this.primaryContact = primaryContact;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getResidencePhone() {
		return residencePhone;
	}

	public void setResidencePhone(String residencePhone) {
		this.residencePhone = residencePhone;
	}

	public String getRollNumber() {
		return rollNumber;
	}

	public void setRollNumber(String rollNumber) {
		this.rollNumber = rollNumber;
	}

	public String getSscNo() {
		return sscNo;
	}

	public void setSscNo(String sscNo) {
		this.sscNo = sscNo;
	}

	public String getStatusComments() {
		return statusComments;
	}

	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}

	public String getQuotaName() {
		return quotaName;
	}

	public void setQuotaName(String quotaName) {
		this.quotaName = quotaName;
	}

	public String getApplicationReason() {
		return applicationReason;
	}

	public void setApplicationReason(String applicationReason) {
		this.applicationReason = applicationReason;
	}

	public boolean isCurrentStatus() {
		return isCurrentStatus;
	}

	public void setCurrentStatus(boolean isCurrentStatus) {
		this.isCurrentStatus = isCurrentStatus;
	}

	public List<StudentAppDocumentCollectionDTO> getStdAppDocCollections() {
		return stdAppDocCollections;
	}

	public void setStdAppDocCollections(List<StudentAppDocumentCollectionDTO> stdAppDocCollections) {
		this.stdAppDocCollections = stdAppDocCollections;
	}

	public List<StudentAppActivityDTO> getStdAppActivities() {
		return stdAppActivities;
	}

	public void setStdAppActivities(List<StudentAppActivityDTO> stdAppActivities) {
		this.stdAppActivities = stdAppActivities;
	}

	public List<StudentAppEducationDTO> getStdAppEducations() {
		return stdAppEducations;
	}

	public void setStdAppEducations(List<StudentAppEducationDTO> stdAppEducations) {
		this.stdAppEducations = stdAppEducations;
	}

	public Long getCurrentWorkflowStatusId() {
		return currentWorkflowStatusId;
	}

	public void setCurrentWorkflowStatusId(Long currentWorkflowStatusId) {
		this.currentWorkflowStatusId = currentWorkflowStatusId;
	}

	public String getCurrentWorkflowStatusName() {
		return currentWorkflowStatusName;
	}

	public void setCurrentWorkflowStatusName(String currentWorkflowStatusName) {
		this.currentWorkflowStatusName = currentWorkflowStatusName;
	}

	public Long getToAppWorkflowStatusId() {
		return toAppWorkflowStatusId;
	}

	public void setToAppWorkflowStatusId(Long toAppWorkflowStatusId) {
		this.toAppWorkflowStatusId = toAppWorkflowStatusId;
	}

	public String getToAppWorkflowStatusName() {
		return toAppWorkflowStatusName;
	}

	public void setToAppWorkflowStatusName(String toAppWorkflowStatusName) {
		this.toAppWorkflowStatusName = toAppWorkflowStatusName;
	}

	public Date getWeddingDate() {
		return weddingDate;
	}

	public void setWeddingDate(Date weddingDate) {
		this.weddingDate = weddingDate;
	}

	public Long getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}

	public String getAcademicYearName() {
		return academicYearName;
	}

	public void setAcademicYearName(String academicYearName) {
		this.academicYearName = academicYearName;
	}

	public String getQuotaCode() {
		return quotaCode;
	}

	public void setQuotaCode(String quotaCode) {
		this.quotaCode = quotaCode;
	}

	public Long getQualifyingId() {
		return qualifyingId;
	}

	public void setQualifyingId(Long qualifyingId) {
		this.qualifyingId = qualifyingId;
	}

	public String getQualifyingName() {
		return qualifyingName;
	}

	public void setQualifyingName(String qualifyingName) {
		this.qualifyingName = qualifyingName;
	}

	public String getQualifyingCode() {
		return qualifyingCode;
	}

	public void setQualifyingCode(String qualifyingCode) {
		this.qualifyingCode = qualifyingCode;
	}

	public Boolean getIsMinority() {
		return isMinority;
	}

	public void setIsMinority(Boolean isMinority) {
		this.isMinority = isMinority;
	}
	
}
