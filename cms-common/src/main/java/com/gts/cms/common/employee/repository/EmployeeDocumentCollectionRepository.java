package com.gts.cms.common.employee.repository;

import com.gts.cms.common.employee.repository.custom.EmployeeDocumentCollectionRepositoryCustom;
import com.gts.cms.entity.EmployeeDocumentCollection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Genesis
 *
 */
@Repository
public interface EmployeeDocumentCollectionRepository
		extends JpaRepository<EmployeeDocumentCollection, Long>, EmployeeDocumentCollectionRepositoryCustom {

}
