package com.gts.cms.common.service;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.OrganizationDTO;


public interface OrganizationService {

	/*ApiResponse<OrganizationDTO> getOrganizationDetails(Long organizationId);
	
	ApiResponse<List<OrganizationDTO>> getAllOrganizations();

	ApiResponse<Long> addOrganization(OrganizationDTO organizationDTO);

	ApiResponse<Long> updateOrganization(OrganizationDTO organizationDTO);

	ApiResponse<Long> deleteOrganization(Long organizationId);*/
	
	ApiResponse<OrganizationDTO> uploadOrganizationLogo(OrganizationDTO organizationDTO);
}
