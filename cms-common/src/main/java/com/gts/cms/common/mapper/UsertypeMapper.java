package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.UsertypeDTO;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.Organization;
import com.gts.cms.entity.Usertype;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class UsertypeMapper implements BaseMapper<Usertype, UsertypeDTO> {
@Override
public UsertypeDTO convertEntityToDTO(Usertype usertype) {
	UsertypeDTO usertypeDTO = null;
	if (usertype != null) {
	usertypeDTO = new UsertypeDTO();
	  usertypeDTO.setCreatedDt(usertype.getCreatedDt());
	  usertypeDTO.setUpdatedDt(usertype.getUpdatedDt());
	  usertypeDTO.setUpdatedUser(usertype.getUpdatedUser());
	  usertypeDTO.setIsActive(usertype.getIsActive());
	  usertypeDTO.setCreatedUser(usertype.getCreatedUser());
	  usertypeDTO.setUserTypeId(usertype.getUserTypeId());
	  usertypeDTO.setReason(usertype.getReason());
	  usertypeDTO.setUserTypeCode(usertype.getUserTypeCode());
	  usertypeDTO.setUserTypeName(usertype.getUserTypeName());
	  if(usertype.getOrganization()!= null){
		  usertypeDTO.setOrganizationId(usertype.getOrganization().getOrganizationId());
		  usertypeDTO.setOrgName(usertype.getOrganization().getOrgName());
		  usertypeDTO.setOrgCode(usertype.getOrganization().getOrgCode());
	}
	}
	return usertypeDTO;
}
@Override
public Usertype convertDTOtoEntity(UsertypeDTO usertypeDTO,Usertype usertype) {
	if ( null == usertype ) {
	 usertype = new Usertype();
	 usertype.setIsActive(Status.ACTIVE.getId());
	 usertype.setCreatedDt(new Date());
	 usertype.setCreatedUser(SecurityUtil.getCurrentUser());
	}else{
	 usertype.setIsActive(usertypeDTO.getIsActive());
	}
	  usertype.setUpdatedDt(new Date());
	  usertype.setUpdatedUser(SecurityUtil.getCurrentUser());
	  usertype.setUserTypeId(usertypeDTO.getUserTypeId());
	 if(usertypeDTO.getOrganizationId()!= null){
		 Organization organization = new Organization();
		 organization.setOrganizationId(usertypeDTO.getOrganizationId());
		 organization.setOrgName(usertypeDTO.getOrgName());
		 organization.setOrgCode(usertypeDTO.getOrgCode());
		 usertype.setOrganization(organization);
	 }
	  usertype.setReason(usertypeDTO.getReason());
	  usertype.setUserTypeCode(usertypeDTO.getUserTypeCode());
	  usertype.setUserTypeName(usertypeDTO.getUserTypeName());
	  usertype.setUpdatedDt(new Date());
	  usertype.setUpdatedUser(SecurityUtil.getCurrentUser());
	return usertype;
}
@Override
public List<Usertype> convertDTOListToEntityList(List<UsertypeDTO> usertypeDTOList) {
	List<Usertype> usertypeList = new ArrayList<>();
	 usertypeDTOList.forEach(usertypeDTO -> usertypeList.add(convertDTOtoEntity(usertypeDTO, null)));
	return usertypeList;
}
@Override
public List<UsertypeDTO> convertEntityListToDTOList(List<Usertype> usertypeList) {
	List<UsertypeDTO> usertypeDTOList = new ArrayList<>();
	 usertypeList.forEach(usertype -> usertypeDTOList.add(convertEntityToDTO(usertype)));
	return usertypeDTOList;
}
}