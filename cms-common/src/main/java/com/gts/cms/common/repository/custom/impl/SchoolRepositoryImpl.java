package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.SchoolRepositoryCustom;
import com.gts.cms.entity.School;
import com.querydsl.core.dml.UpdateClause;
import com.querydsl.jpa.impl.JPAUpdateClause;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static com.gts.cms.entity.QSchool.school;

/**
 * created by Naveen on 04/09/2018
 * 
 */
@Repository
public class SchoolRepositoryImpl extends QuerydslRepositorySupport implements SchoolRepositoryCustom {
	
	public SchoolRepositoryImpl() {
		super(School.class);
	}

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public Long deleteSchool(Long schoolId) {
		return update(school).where(school.schoolId.eq(schoolId))
				.set(school.isActive, false).execute();
	}
	
	@Override
	public Long uploadSchoolLogo(Long schoolId, String logoName) {
		UpdateClause<JPAUpdateClause> query = update(school).where(school.schoolId.eq(schoolId));
		if (logoName != null) {
			query.set(school.logoFilename, logoName);
		}
		return query.execute();
	}
	
}
