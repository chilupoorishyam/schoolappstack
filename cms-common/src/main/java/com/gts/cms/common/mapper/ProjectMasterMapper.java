package com.gts.cms.common.mapper;

import com.gts.cms.*;
import com.gts.cms.common.dto.ProjectMastertDTO;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class ProjectMasterMapper implements BaseMapper<ProjectMaster, ProjectMastertDTO> {


    @Override
    public ProjectMastertDTO convertEntityToDTO(ProjectMaster projectMaster) {
        ProjectMastertDTO projectMastertDTO = null;
        if (projectMaster != null) {
            projectMastertDTO = new ProjectMastertDTO();
            if (projectMaster.getSchoolId() != null) {
                projectMastertDTO.setSchoolId(projectMaster.getSchoolId().getSchoolId());
            }
            if (projectMaster.getAcademicProjectId() != null) {
                projectMastertDTO.setAcademicProjectId(projectMaster.getAcademicProjectId().getAcademicProjectId());
            }
            projectMastertDTO.setProjectTitle(projectMaster.getProjectTitle());
            projectMastertDTO.setProjectDescription(projectMaster.getProjectDescription());
            projectMastertDTO.setPrjTechnologyIds(projectMaster.getPrjTechnologyIds());
            projectMastertDTO.setPrjKeywordIds(projectMaster.getPrjKeywordIds());
            if (projectMaster.getProjecttypeCatdetId() != null) {
                projectMastertDTO.setProjecttypeCatdetId(projectMaster.getProjecttypeCatdetId().getGeneralDetailId());
            }
            if (projectMaster.getInchargeEmployeeDetail() != null) {
                projectMastertDTO.setInchargeEmployeeId(projectMaster.getInchargeEmployeeDetail().getEmployeeId());
            }
            if (projectMaster.getPrimaryStudentDetail() != null) {
                projectMastertDTO.setPrimaryStudentId(projectMaster.getPrimaryStudentDetail().getStudentId());
            }
            projectMastertDTO.setOutsourceCompanyName(projectMaster.getOutsourceCompanyName());
            projectMastertDTO.setOutsourceCompanyContactDet(projectMaster.getOutsourceCompanyContactDet());
            projectMastertDTO.setProjectApproved(projectMaster.getProjectApproved());
            if (projectMaster.getApprovedEmpDetails() != null) {
                projectMastertDTO.setApprovedEmpId(projectMaster.getApprovedEmpDetails().getEmployeeId());
            }
            projectMastertDTO.setApprovedDate(projectMaster.getApprovedDate());
            projectMastertDTO.setApprovalComments(projectMaster.getApprovalComments());
            projectMastertDTO.setNextReviewDate(projectMaster.getNextReviewDate());
            projectMastertDTO.setProjectSubmitted(projectMaster.getProjectSubmitted());
            if (projectMaster.getSubmittedbyEmployeeDetail() != null) {
                projectMastertDTO.setSubmittedbyEmployeeId(projectMaster.getSubmittedbyEmployeeDetail().getEmployeeId());
            }
            projectMastertDTO.setSubmittedDate(projectMaster.getSubmittedDate());
            projectMastertDTO.setSubmissionComments(projectMaster.getSubmissionComments());
            projectMastertDTO.setActive(projectMaster.getIsActive());
            projectMastertDTO.setReason(projectMaster.getReason());
            projectMastertDTO.setCreatedDt(projectMaster.getCreatedDt());
            projectMastertDTO.setCreatedUser(projectMaster.getCreatedUser());
            projectMastertDTO.setUpdatedDt(projectMaster.getUpdatedDt());
            projectMastertDTO.setUpdatedUser(projectMaster.getUpdatedUser());
        }
        return projectMastertDTO;
    }

    @Override
    public List<ProjectMastertDTO> convertEntityListToDTOList(List<ProjectMaster> entityList) {
        List<ProjectMastertDTO> projectMastertDTOS = new ArrayList<>();
        entityList.forEach(projectMaster -> projectMastertDTOS.add(convertEntityToDTO(projectMaster)));
        return projectMastertDTOS;
    }

    @Override
    public List<ProjectMaster> convertDTOListToEntityList(List<ProjectMastertDTO> dtoList) {
        List<ProjectMaster> projectMasterList = new ArrayList<>();
        dtoList.forEach(projectMastertDTO -> projectMasterList.add(convertDTOtoEntity(projectMastertDTO, null)));
        return projectMasterList;
    }

    @Override
    public ProjectMaster convertDTOtoEntity(ProjectMastertDTO projectMastertDTO, ProjectMaster projectMaster) {
        if (null == projectMaster) {
            projectMaster = new ProjectMaster();
            projectMaster.setIsActive(Boolean.TRUE);
            projectMaster.setCreatedDt(new Date());
            projectMaster.setCreatedUser(SecurityUtil.getCurrentUser());
        } else {
            projectMaster.setIsActive(projectMastertDTO.getActive());
        }
        if (projectMastertDTO.getSchoolId() != null) {
            School school = new School();
            school.setSchoolId(projectMastertDTO.getSchoolId());
            projectMaster.setSchoolId(school);
        }
        if (projectMastertDTO.getAcademicProjectId() != null) {
            AcademicProject academicProject = new AcademicProject();
            academicProject.setAcademicProjectId(projectMastertDTO.getAcademicProjectId());
            projectMaster.setAcademicProjectId(academicProject);
        }
        projectMaster.setProjectTitle(projectMastertDTO.getProjectTitle());
        projectMaster.setProjectDescription(projectMastertDTO.getProjectDescription());
        projectMaster.setPrjTechnologyIds(projectMastertDTO.getPrjTechnologyIds());
        projectMaster.setPrjKeywordIds(projectMastertDTO.getPrjKeywordIds());
        if (projectMastertDTO.getProjecttypeCatdetId() != null) {
            GeneralDetail generalDetail = new GeneralDetail();
            generalDetail.setGeneralDetailId(projectMastertDTO.getProjecttypeCatdetId());
            projectMaster.setProjecttypeCatdetId(generalDetail);
        }
        if (projectMastertDTO.getInchargeEmployeeId() != null) {
            EmployeeDetail employeeDetail = new EmployeeDetail();
            employeeDetail.setEmployeeId(projectMastertDTO.getInchargeEmployeeId());
            projectMaster.setInchargeEmployeeDetail(employeeDetail);
        }
        if (projectMastertDTO.getPrimaryStudentId() != null) {
            StudentDetail studentDetail = new StudentDetail();
            studentDetail.setStudentId(projectMastertDTO.getPrimaryStudentId());
            projectMaster.setPrimaryStudentDetail(studentDetail);
        }
        projectMaster.setOutsourceCompanyName(projectMastertDTO.getOutsourceCompanyName());
        projectMaster.setOutsourceCompanyContactDet(projectMastertDTO.getOutsourceCompanyContactDet());
        projectMaster.setProjectApproved(projectMastertDTO.getProjectApproved());
        if (projectMastertDTO.getApprovedEmpId() != null) {
            EmployeeDetail employeeDetail = new EmployeeDetail();
            employeeDetail.setEmployeeId(projectMastertDTO.getApprovedEmpId());
            projectMaster.setApprovedEmpDetails(employeeDetail);
        }
        projectMaster.setApprovedDate(projectMastertDTO.getApprovedDate());
        projectMaster.setApprovalComments(projectMastertDTO.getApprovalComments());
        projectMaster.setNextReviewDate(projectMastertDTO.getNextReviewDate());
        projectMaster.setProjectSubmitted(projectMastertDTO.getProjectSubmitted());
        if (projectMastertDTO.getSubmittedbyEmployeeId() != null) {
            EmployeeDetail employeeDetail = new EmployeeDetail();
            employeeDetail.setEmployeeId(projectMastertDTO.getSubmittedbyEmployeeId());
            projectMaster.setSubmittedbyEmployeeDetail(employeeDetail);
        }
        projectMaster.setSubmittedDate(projectMastertDTO.getSubmittedDate());
        projectMaster.setSubmissionComments(projectMastertDTO.getSubmissionComments());
        projectMaster.setIsActive(projectMastertDTO.getActive());
        projectMaster.setReason(projectMastertDTO.getReason());
        projectMaster.setCreatedDt(projectMastertDTO.getCreatedDt());
        projectMaster.setCreatedUser(projectMastertDTO.getCreatedUser());
        projectMaster.setUpdatedDt(projectMastertDTO.getUpdatedDt());
        projectMaster.setUpdatedUser(projectMastertDTO.getUpdatedUser());

        return projectMaster;
    }
}
