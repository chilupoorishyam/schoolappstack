package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.GeneralMasterRepositoryCustom;
import com.gts.cms.entity.GeneralMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface GeneralMasterRepository extends JpaRepository<GeneralMaster, Long>, GeneralMasterRepositoryCustom {

}
