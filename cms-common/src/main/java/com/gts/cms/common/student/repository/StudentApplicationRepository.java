package com.gts.cms.common.student.repository;

import com.gts.cms.common.student.repository.custom.StudentApplicationRepositoryCustom;
import com.gts.cms.entity.StudentApplication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentApplicationRepository extends JpaRepository<StudentApplication, Long>, StudentApplicationRepositoryCustom {

    @Query("SELECT sa FROM StudentApplication sa "
            + " INNER JOIN Organization org "
            + " ON sa.organization.organizationId= org.organizationId "
            + " WHERE 1=1 "
            + " AND (:organizationId is null or org.organizationId = :organizationId) "
            + " AND (:status is null or sa.isActive=:status) "
            + " ORDER BY sa.studentAppId DESC "
    )
    Page<StudentApplication> fetchAllStudentApplication(@Param("organizationId") Long organizationId,
                                                        @Param("status") Boolean status,
                                                        Pageable page);

    @Query("SELECT sa FROM StudentApplication sa "
            + " WHERE 1=1 "
            + " AND (sa.firstName = :firstName) "
            + " AND (sa.fatherName = :fatherName) "
            + " AND (sa.isActive=true) "
    )
    List<StudentApplication> isStudentDuplicate(@Param("firstName") String firstName,
                                                @Param("fatherName") String fatherName);

}
