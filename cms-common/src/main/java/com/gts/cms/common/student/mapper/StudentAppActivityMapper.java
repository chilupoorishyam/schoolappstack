package com.gts.cms.common.student.mapper;

import com.gts.cms.common.dto.StudentAppActivityDTO;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.StudentAppActivity;
import com.gts.cms.entity.StudentAppActivity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class StudentAppActivityMapper implements BaseMapper<StudentAppActivity, StudentAppActivityDTO> {

	@Override
	public StudentAppActivityDTO convertEntityToDTO(StudentAppActivity studentAppActivity) {
		StudentAppActivityDTO studentAppActivityDTO = null;
		if (studentAppActivity != null) {
			studentAppActivityDTO = new StudentAppActivityDTO();
			studentAppActivityDTO.setStudentAppActivityId(studentAppActivity.getStudentAppActivityId());
			studentAppActivityDTO.setParticulars(studentAppActivity.getParticulars());
			studentAppActivityDTO.setLevel(studentAppActivity.getLevel());
			studentAppActivityDTO.setSponsoredBy(studentAppActivity.getSponsoredBy());
			studentAppActivityDTO.setIsActive(studentAppActivity.getIsActive());
		}
		return studentAppActivityDTO;
	}

	@Override
	public List<StudentAppActivityDTO> convertEntityListToDTOList(List<StudentAppActivity> studentAppActivityList) {
		List<StudentAppActivityDTO> studentAppActivityDTOList = new ArrayList<>();
		studentAppActivityList
				.forEach(studentAppActivity -> studentAppActivityDTOList.add(convertEntityToDTO(studentAppActivity)));
		return studentAppActivityDTOList;
	}

	@Override
	public List<StudentAppActivity> convertDTOListToEntityList(List<StudentAppActivityDTO> studentAppActivityDTOList) {
		List<StudentAppActivity> studentAppActivityList = new ArrayList<>();
		studentAppActivityDTOList.forEach(
				studentAppActivityDTO -> studentAppActivityList.add(convertDTOtoEntity(studentAppActivityDTO, null)));
		return studentAppActivityList;

	}

	@Override
	public StudentAppActivity convertDTOtoEntity(StudentAppActivityDTO studentAppActivityDTO,
			StudentAppActivity studentAppActivity) {
		if (null == studentAppActivity) {
			studentAppActivity = new StudentAppActivity();
			studentAppActivity.setCreatedDt(new Date());
			studentAppActivity.setCreatedUser(SecurityUtil.getCurrentUser());
		}
		studentAppActivity.setStudentAppActivityId(studentAppActivity.getStudentAppActivityId());
		studentAppActivity.setParticulars(studentAppActivityDTO.getParticulars());
		studentAppActivity.setLevel(studentAppActivityDTO.getLevel());
		studentAppActivity.setIsActive(studentAppActivityDTO.getIsActive());
		studentAppActivity.setSponsoredBy(studentAppActivity.getSponsoredBy());
		return studentAppActivity;
	}
}
