package com.gts.cms.common.mapper;

import com.gts.cms.*;
import com.gts.cms.common.dto.RolePrivilegeDTO;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class RolePrivilegeMapper implements BaseMapper<RolePrivilege, RolePrivilegeDTO> {
	@Override
	public RolePrivilegeDTO convertEntityToDTO(RolePrivilege rolePrivilege) {
		RolePrivilegeDTO rolePrivilegeDTO = null;
		if (rolePrivilege != null) {
			rolePrivilegeDTO = new RolePrivilegeDTO();
			rolePrivilegeDTO.setCreatedDt(rolePrivilege.getCreatedDt());
			rolePrivilegeDTO.setUpdatedDt(rolePrivilege.getUpdatedDt());
			rolePrivilegeDTO.setUpdatedUser(rolePrivilege.getUpdatedUser());
			rolePrivilegeDTO.setIsActive(rolePrivilege.getIsActive());
			rolePrivilegeDTO.setCreatedUser(rolePrivilege.getCreatedUser());
			rolePrivilegeDTO.setCanAdd(rolePrivilege.getCanAdd());
			rolePrivilegeDTO.setCanView(rolePrivilege.getCanView());
			rolePrivilegeDTO.setCanDelete(rolePrivilege.getCanDelete());
			rolePrivilegeDTO.setCanEdit(rolePrivilege.getCanEdit());
			rolePrivilegeDTO.setReason(rolePrivilege.getReason());
			rolePrivilegeDTO.setRolePrivilegeId(rolePrivilege.getRolePrivilegeId());

			if (rolePrivilege.getSchool() != null) {
				rolePrivilegeDTO.setSchoolId(rolePrivilege.getSchool().getSchoolId());
				rolePrivilegeDTO.setSchoolName(rolePrivilege.getSchool().getSchoolName());
			}
			if (rolePrivilege.getOrganization() != null) {
				rolePrivilegeDTO.setOrganizationId(rolePrivilege.getOrganization().getOrganizationId());
				rolePrivilegeDTO.setOrgName(rolePrivilege.getOrganization().getOrgName());
			}
			if (rolePrivilege.getRole() != null) {
				rolePrivilegeDTO.setRoleId(rolePrivilege.getRole().getRoleId());
				rolePrivilegeDTO.setRoleName(rolePrivilege.getRole().getRoleName());
			}
			if (rolePrivilege.getModule() != null) {
				rolePrivilegeDTO.setModuleId(rolePrivilege.getModule().getModuleId());
				rolePrivilegeDTO.setModuleName(rolePrivilege.getModule().getModuleName());
			}
			if (rolePrivilege.getPage() != null) {
				rolePrivilegeDTO.setPageId(rolePrivilege.getPage().getPageId());
				rolePrivilegeDTO.setPageName(rolePrivilege.getPage().getPageName());
			}
			if (rolePrivilege.getSubmodule() != null) {
				rolePrivilegeDTO.setSubmoduleId(rolePrivilege.getSubmodule().getSubmoduleId());
				rolePrivilegeDTO.setSubmoduleName(rolePrivilege.getSubmodule().getSubmoduleName());
			}

		}
		return rolePrivilegeDTO;
	}

	@Override
	public RolePrivilege convertDTOtoEntity(RolePrivilegeDTO rolePrivilegeDTO, RolePrivilege rolePrivilege) {
		if (rolePrivilegeDTO != null) {
			if (rolePrivilege == null) {
				rolePrivilege = new RolePrivilege();
				if (rolePrivilegeDTO.getIsActive() != null) {
					rolePrivilege.setIsActive(rolePrivilegeDTO.getIsActive());
				} else {
					rolePrivilege.setIsActive(Status.ACTIVE.getId());
				}
				rolePrivilege.setCreatedDt(new Date());
				rolePrivilege.setCreatedUser(SecurityUtil.getCurrentUser());
			} else {
				rolePrivilege.setIsActive(rolePrivilegeDTO.getIsActive());
			}
			rolePrivilege.setUpdatedDt(new Date());
			rolePrivilege.setUpdatedUser(SecurityUtil.getCurrentUser());
			if (rolePrivilegeDTO.getPageId() != null) {
				Page page = new Page();
				page.setPageId(rolePrivilegeDTO.getPageId());
				rolePrivilege.setPage(page);
			}
			if (rolePrivilegeDTO.getModuleId() != null) {
				Module module = new Module();
				module.setModuleId(rolePrivilegeDTO.getModuleId());
				rolePrivilege.setModule(module);
			}
			if (rolePrivilegeDTO.getSubmoduleId() != null) {
				Submodule submodule = new Submodule();
				submodule.setSubmoduleId(rolePrivilegeDTO.getSubmoduleId());
				rolePrivilege.setSubmodule(submodule);
			}
			rolePrivilege.setCanAdd(rolePrivilegeDTO.getCanAdd());
			rolePrivilege.setCanView(rolePrivilegeDTO.getCanView());
			if (rolePrivilegeDTO.getSchoolId() != null) {
				School school = new School();
				school.setSchoolId(rolePrivilegeDTO.getSchoolId());
				rolePrivilege.setSchool(school);
			}
			rolePrivilege.setCanDelete(rolePrivilegeDTO.getCanDelete());
			if (rolePrivilegeDTO.getOrganizationId() != null) {
				Organization organization = new Organization();
				organization.setOrganizationId(rolePrivilegeDTO.getOrganizationId());
				rolePrivilege.setOrganization(organization);
			}
			rolePrivilege.setCanEdit(rolePrivilegeDTO.getCanEdit());
			if (rolePrivilegeDTO.getRoleId() != null) {
				Role role = new Role();
				role.setRoleId(rolePrivilegeDTO.getRoleId());
				rolePrivilege.setRole(role);
			}
			rolePrivilege.setReason(rolePrivilegeDTO.getReason());
			rolePrivilege.setRolePrivilegeId(rolePrivilegeDTO.getRolePrivilegeId());
		}
		return rolePrivilege;

	}

	@Override
	public List<RolePrivilege> convertDTOListToEntityList(List<RolePrivilegeDTO> rolePrivilegeDTOList) {
		List<RolePrivilege> rolePrivilegeList = new ArrayList<>();
		rolePrivilegeDTOList
				.forEach(rolePrivilegeDTO -> rolePrivilegeList.add(convertDTOtoEntity(rolePrivilegeDTO, null)));
		return rolePrivilegeList;
	}

	@Override
	public List<RolePrivilegeDTO> convertEntityListToDTOList(List<RolePrivilege> rolePrivilegeList) {
		List<RolePrivilegeDTO> rolePrivilegeDTOList = new ArrayList<>();
		rolePrivilegeList.forEach(rolePrivilege -> rolePrivilegeDTOList.add(convertEntityToDTO(rolePrivilege)));
		return rolePrivilegeDTOList;
	}
}