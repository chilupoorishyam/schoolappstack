package com.gts.cms.common.repository.custom;

import com.gts.cms.entity.GeneralSetting;

public interface GeneralSettingRepositoryCustom {

	public GeneralSetting getGeneralSettingByCode(String generalSettingCode);
	
	public GeneralSetting getGeneralSettingName(Long schoolId, String generalSettingName);
		

}
