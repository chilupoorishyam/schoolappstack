package com.gts.cms.common.repository;

import com.gts.cms.entity.BookStg;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookStgRepository extends JpaRepository<BookStg, Long> {
}
