package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.RolePrivilegeRepositoryCustom;
import com.gts.cms.entity.QPage;
import com.gts.cms.entity.QRolePrivilege;
import com.gts.cms.entity.RolePrivilege;
import com.gts.cms.repo.dto.AuthorityRepoDTO;
import com.querydsl.core.types.Projections;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * created by Naveen on 04/09/2018
 * 
 */
@Repository
public class RolePrivilegeRepositoryImpl extends QuerydslRepositorySupport implements RolePrivilegeRepositoryCustom {

	public RolePrivilegeRepositoryImpl() {
		super(RolePrivilege.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<AuthorityRepoDTO> getAuthoritiesForUser(Long roleId, Long orgId, Long schoolId) {
		/*return from(rolePrivilege).leftJoin(rolePrivilege.Module, module).leftJoin(rolePrivilege.TSecPage, page)
				.leftJoin(rolePrivilege.Submodule, submodule)
				.select(Projections.constructor(AuthorityRepoDTO.class, rolePrivilege.rolePrivilegeId, module.moduleName,
						page.pageName, submodule.submoduleName, rolePrivilege.canView, rolePrivilege.canAdd,
						rolePrivilege.canEdit, rolePrivilege.canDelete))
				.where(rolePrivilege.TMSchool.schoolId.eq(schoolId).and(rolePrivilege.TMOrganization.organizationId
						.eq(orgId).and(rolePrivilege.TSecRole.roleId.eq(roleId))))
				.fetch();*/
		
		return from(QRolePrivilege.rolePrivilege).leftJoin(QRolePrivilege.rolePrivilege.page, QPage.page)
				.select(Projections.constructor(AuthorityRepoDTO.class, QRolePrivilege.rolePrivilege.rolePrivilegeId,
						QPage.page.pageName, QRolePrivilege.rolePrivilege.canView, QRolePrivilege.rolePrivilege.canAdd,
						QRolePrivilege.rolePrivilege.canEdit, QRolePrivilege.rolePrivilege.canDelete))
				.where(QRolePrivilege.rolePrivilege.school.schoolId.eq(schoolId).and(QRolePrivilege.rolePrivilege.organization.organizationId
						.eq(orgId).and(QRolePrivilege.rolePrivilege.role.roleId.eq(roleId))))
				.fetch();
	}

}
