package com.gts.cms.common.student.repository.custom;

public interface StudentAppDocumentCollectionRepositoryCustom {
	Long updateStudentDocumentFiles(Long applicationId, String docRepId, String docPath);
}
