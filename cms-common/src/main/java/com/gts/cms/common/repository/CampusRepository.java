package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.CampusRepositoryCustom;
import com.gts.cms.entity.Campus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * created by Naveen(Auto) on 14/09/2018
 * 
 */
@Repository
public interface CampusRepository extends JpaRepository<Campus, Long>, CampusRepositoryCustom {

	List<Campus> findByIsActiveTrue();

	Optional<Campus> findByCampusIdAndIsActiveTrue(Long campusId);


}
