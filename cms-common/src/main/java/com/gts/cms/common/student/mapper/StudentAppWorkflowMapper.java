package com.gts.cms.common.student.mapper;

import com.gts.cms.common.dto.StudentAppWorkflowDTO;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.StudentAppWorkflow;
import com.gts.cms.entity.StudentApplication;
import com.gts.cms.entity.WorkflowStage;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class StudentAppWorkflowMapper implements BaseMapper<StudentAppWorkflow, StudentAppWorkflowDTO> {

	@Override
	public StudentAppWorkflowDTO convertEntityToDTO(StudentAppWorkflow studentAppWorkflow) {
		StudentAppWorkflowDTO studentAppWorkflowDTO = new StudentAppWorkflowDTO();
		studentAppWorkflowDTO.setStudentAppWorkflowId(studentAppWorkflow.getStudentAppWorkflowId());
		if (studentAppWorkflow.getFromAppStatus() != null) {
			studentAppWorkflowDTO
					.setCurrentWorkflowStatusId(studentAppWorkflow.getFromAppStatus().getWorkflowStageId());
			studentAppWorkflowDTO.setCurrentWorkflowName(studentAppWorkflow.getFromAppStatus().getWfName());
		}
		if (studentAppWorkflow.getToAppStatus() != null) {
			studentAppWorkflowDTO.setToAppWorkflowStatusId(studentAppWorkflow.getToAppStatus().getWorkflowStageId());
			studentAppWorkflowDTO.setToAppWorkflowName(studentAppWorkflow.getToAppStatus().getWfName());
		}
		studentAppWorkflowDTO.setComments(studentAppWorkflow.getComments());
		studentAppWorkflowDTO.setIsCurrentStatus(studentAppWorkflow.getIsCurrentStatus());
		studentAppWorkflowDTO.setIsActive(studentAppWorkflow.getIsActive());
		studentAppWorkflowDTO.setReason(studentAppWorkflow.getReason());
		if (studentAppWorkflow.getStudentApplication() != null) {
			studentAppWorkflowDTO.setStudentAppId(studentAppWorkflow.getStudentApplication().getStudentAppId());
		}

		return studentAppWorkflowDTO;
	}

	@Override
	public List<StudentAppWorkflowDTO> convertEntityListToDTOList(List<StudentAppWorkflow> studentAppWorkflowList) {
		List<StudentAppWorkflowDTO> studentAppWorkflowDTOList = new ArrayList<>();
		studentAppWorkflowList
				.forEach(studentAppWorkflow -> studentAppWorkflowDTOList.add(convertEntityToDTO(studentAppWorkflow)));
		return studentAppWorkflowDTOList;
	}

	@Override
	public List<StudentAppWorkflow> convertDTOListToEntityList(List<StudentAppWorkflowDTO> studentAppWorkflowDTOList) {
		List<StudentAppWorkflow> studentAppWorkflowList = new ArrayList<>();
		studentAppWorkflowDTOList.forEach(
				studentAppWorkflowDTO -> studentAppWorkflowList.add(convertDTOtoEntity(studentAppWorkflowDTO, null)));
		return studentAppWorkflowList;

	}

	@Override
	public StudentAppWorkflow convertDTOtoEntity(StudentAppWorkflowDTO studentAppWorkflowDTO,
												 StudentAppWorkflow studentAppWorkflow) {
		if (null == studentAppWorkflow) {
			studentAppWorkflow = new StudentAppWorkflow();
			studentAppWorkflow.setIsActive(studentAppWorkflowDTO.getIsActive());
			studentAppWorkflow.setCreatedDt(new Date());
			studentAppWorkflow.setCreatedUser(SecurityUtil.getCurrentUser());
		}
		if (studentAppWorkflowDTO.getCurrentWorkflowStatusId() != null) {
			WorkflowStage fromAppWorkflowStage = new WorkflowStage();
			fromAppWorkflowStage.setWorkflowStageId(studentAppWorkflowDTO.getCurrentWorkflowStatusId());
			fromAppWorkflowStage.setWfName(studentAppWorkflowDTO.getCurrentWorkflowName());
			studentAppWorkflow.setFromAppStatus(fromAppWorkflowStage);
		}
		WorkflowStage toAppWorkflowStage = null;
		if (studentAppWorkflowDTO.getToAppWorkflowStatusId() != null) {
			toAppWorkflowStage = new WorkflowStage();
			toAppWorkflowStage.setWorkflowStageId(studentAppWorkflowDTO.getToAppWorkflowStatusId());
			toAppWorkflowStage.setWfName(studentAppWorkflowDTO.getToAppWorkflowName());
			studentAppWorkflow.setToAppStatus(toAppWorkflowStage);
		}
		studentAppWorkflow.setComments(studentAppWorkflowDTO.getComments());
		studentAppWorkflow.setIsCurrentStatus(studentAppWorkflowDTO.getIsCurrentStatus());
		studentAppWorkflow.setIsActive(studentAppWorkflowDTO.getIsActive());
		if (studentAppWorkflow.getIsActive() == null) {
			studentAppWorkflow.setIsActive(Boolean.TRUE);

		}
		studentAppWorkflow.setReason(studentAppWorkflowDTO.getReason());
		if (studentAppWorkflowDTO.getStudentAppId() != null && toAppWorkflowStage != null) {
			StudentApplication studentApplication = new StudentApplication();
			studentApplication.setStudentAppId(studentAppWorkflowDTO.getStudentAppId());
			studentApplication.setUpdatedDt(new Date());
			studentApplication.setUpdatedUser(SecurityUtil.getCurrentUser());
			studentApplication.setWorkflowStage(toAppWorkflowStage);
			studentAppWorkflow.setStudentApplication(studentApplication);
		}
		return studentAppWorkflow;
	}

}
