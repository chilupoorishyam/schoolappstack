package com.gts.cms.common.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.Map;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SubjectStgDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long subjectId;
    private String school;
    private String course;
    private String subjectName;
    private String subjectCode;
    private String subjectShortName;
    private String subjectType;
    private String subjectCategory;
    private String creditHours;
    @JsonIgnore
    private Map<String, MultipartFile> files;
}
