package com.gts.cms.common.employee.dto;

import com.gts.cms.common.dto.EmployeeReportingDTO;
import com.gts.cms.common.dto.StaffCourseyrSubjectDTO;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

public class EmployeeDetailDTO {

	private Long employeeId;

	private String aadharNo;

	private String aadharPath;

	private String address;

	private String aicteRegNo;

	private String biometricCode;

	private Date createdDt;

	private Long createdUser;

	@NotNull(message = "Date of Birth is required.")
	private Date dateOfBirth;

	private Date dateOfRelieving;

	@NotNull(message = "Email is required.")
	private String email;

	private String emergencyMobile;

	private String empNumber;

	private String epfNo;

	private String esiRegNo;

	private String facebookUrl;

	private String fatherName;

	@NotNull(message = "First Name is required.")
	private String firstName;

	private Long biometricId;

	/*@NotNull(message = "Current Pay is required.")*/
	private Long currentPayId;

	/*@NotNull(message = "Payroll Pay is required.")*/
	private Long payrollPayId;

	/*@NotNull(message = "PayScale is required.")*/
	private Long payscaleId;

	private Long reportingManagerId;
	
	private String reportingManagerName;
	private String reportingManagerNumber;
	
	private String googleplusUrl;

	private Boolean isActive;

	private Boolean isManager;

	private Boolean isPtax;

	private Boolean isRatified;

	private Boolean isTds;

	private Boolean isUsingCampAccommodation;

	private Boolean isUsingTransport;

	private Date jntuDateOfJoining;

	private String jntuRegNo;

	@NotNull(message = "Joining Date is required.")
	private Date joiningDate;

	private String lastName;

	private String licNo;

	private String linkedinUrl;

	private String middleName;

	@NotNull(message = "Mobile is required.")
	private String mobile;

	private Integer monthlySalary;

	private String motherName;

	private Integer nonTeachingExp;

	private String officialMobile;

	private String pancard;

	private String pancardPath;

	private String passportNo;

	private String passportPath;

	private String permanentAddress;

	private String permanentMandal;

	private String permanentPincode;

	private String permanentStreet;

	private String photoPath;

	private String presentAddress;

	private String presentMandal;

	private String presentPincode;

	private String presentStreet;
	
	private Long permanentStateId;
	
	private String permanentStateName;

	private Long permanentCountryId;
	
	private String permanentCountryName;
	
	private Long presentStateId;
	
	private String presentStateName;

	private Long presentCountryId;
	
	private String presentCountryName;

	private Date promotedDate;

	private Long researchExp;

	private String residencePhone;

	private Date resignationDate;

	private Integer serviceBreakYrs;

	private String statusDescription;

	private Integer teachingExp;

	private Integer tenureDays;

	private String voterId;

	private String voterIdPath;

	private Date weddingDate;

	@NotNull(message = "Organization is required.")
	private Long organizationId;

	private Long maritalStatusId;

	private Long districtId;

	private Long empStatusId;
	private String empStatusCode;
	private String empStatusDisplayName;


	private Long empStateId;
	private String empStateCode;
	private String empDisplayName;


	private Long empTypeId;

	@NotNull(message = "Department is required.")
	private Long empDeptId;

	@NotNull(message = "Working Department is required.")
	private Long empWorkingDeptId;

	@NotNull(message = "Qualification is required.")
	private Long qualificationId;

	@NotNull(message = "Designation is required.")
	private Long designationId;
	
	

	@NotNull(message = "Working Designation is required.")
	private Long workingDesignationId;

	@NotNull(message = "SchoolId is required.")
	private Long schoolId;
	
	private String schoolName;
	private String schoolCode;

	@NotNull(message = "Category is required.")
	private Long empCategoryId;

	private Long empWrkCategoryId;

	private Long teachingforId;

	private Long appointmentId;

	private Long cityPresentId;

	private Long districtPresentId;

	private Long cityPermanentId;

	private Long districtPermanentId;

	private Long bloodgroupId;

	private Long paymodeId;

	private Long userId;

	private Long residentId;

	private Long accommodationId;

	private Long titleId;

	@NotNull(message = "Gender is required.")
	private Long genderId;
	
	private String gender;

	private Long religionId;

	private Long nationalityId;

	private Long casteId;

	private Long subCasteId;
	
	private Long empgrade;
	private String empGradeName;
	private String empGradeCode;
	private String deptName;
	
	private String designationName;
	
	private String empCategoryName;
	
	private String nationality;
	
	private String maritalStatus;
	
	private String reason;

	private List<StaffCourseyrSubjectDTO> staffCourseyrSubjects;

	private List<EmployeeDocumentCollectionDTO> employeeDocumentCollection;

	private List<EmployeeDocumentCollectionDTO> verifiedEmployeeDocumentCollection;

	private List<EmployeeBankDetailDTO> employeeBankDetails;

	private List<EmployeeReportingDTO> employeeReportings;

	private List<EmployeeReportingDTO> employeeReportingMangers;

	private List<EmployeeExperienceDetailDTO> empExperienceDetails;
	
	private List<EmployeeEducationDTO> employeeEducations;

	
	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getAadharNo() {
		return aadharNo;
	}

	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}

	public String getAadharPath() {
		return aadharPath;
	}

	public void setAadharPath(String aadharPath) {
		this.aadharPath = aadharPath;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAicteRegNo() {
		return aicteRegNo;
	}

	public void setAicteRegNo(String aicteRegNo) {
		this.aicteRegNo = aicteRegNo;
	}

	public String getBiometricCode() {
		return biometricCode;
	}

	public void setBiometricCode(String biometricCode) {
		this.biometricCode = biometricCode;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Date getDateOfRelieving() {
		return dateOfRelieving;
	}

	public void setDateOfRelieving(Date dateOfRelieving) {
		this.dateOfRelieving = dateOfRelieving;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmergencyMobile() {
		return emergencyMobile;
	}

	public void setEmergencyMobile(String emergencyMobile) {
		this.emergencyMobile = emergencyMobile;
	}

	public String getEmpNumber() {
		return empNumber;
	}

	public void setEmpNumber(String empNumber) {
		this.empNumber = empNumber;
	}

	public String getEpfNo() {
		return epfNo;
	}

	public void setEpfNo(String epfNo) {
		this.epfNo = epfNo;
	}

	public String getEsiRegNo() {
		return esiRegNo;
	}

	public void setEsiRegNo(String esiRegNo) {
		this.esiRegNo = esiRegNo;
	}

	public String getFacebookUrl() {
		return facebookUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Long getBiometricId() {
		return biometricId;
	}

	public void setBiometricId(Long biometricId) {
		this.biometricId = biometricId;
	}

	public Long getCurrentPayId() {
		return currentPayId;
	}

	public void setCurrentPayId(Long currentPayId) {
		this.currentPayId = currentPayId;
	}

	public Long getPayrollPayId() {
		return payrollPayId;
	}

	public void setPayrollPayId(Long payrollPayId) {
		this.payrollPayId = payrollPayId;
	}

	public Long getPayscaleId() {
		return payscaleId;
	}

	public void setPayscaleId(Long payscaleId) {
		this.payscaleId = payscaleId;
	}

	public Long getReportingManagerId() {
		return reportingManagerId;
	}

	public void setReportingManagerId(Long reportingManagerId) {
		this.reportingManagerId = reportingManagerId;
	}

	public String getGoogleplusUrl() {
		return googleplusUrl;
	}

	public void setGoogleplusUrl(String googleplusUrl) {
		this.googleplusUrl = googleplusUrl;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsManager() {
		return isManager;
	}

	public void setIsManager(Boolean isManager) {
		this.isManager = isManager;
	}

	public Boolean getIsPtax() {
		return isPtax;
	}

	public void setIsPtax(Boolean isPtax) {
		this.isPtax = isPtax;
	}

	public Boolean getIsRatified() {
		return isRatified;
	}

	public void setIsRatified(Boolean isRatified) {
		this.isRatified = isRatified;
	}

	public Boolean getIsTds() {
		return isTds;
	}

	public void setIsTds(Boolean isTds) {
		this.isTds = isTds;
	}

	public Boolean getIsUsingCampAccommodation() {
		return isUsingCampAccommodation;
	}

	public void setIsUsingCampAccommodation(Boolean isUsingCampAccommodation) {
		this.isUsingCampAccommodation = isUsingCampAccommodation;
	}

	public Boolean getIsUsingTransport() {
		return isUsingTransport;
	}

	public void setIsUsingTransport(Boolean isUsingTransport) {
		this.isUsingTransport = isUsingTransport;
	}

	public Date getJntuDateOfJoining() {
		return jntuDateOfJoining;
	}

	public void setJntuDateOfJoining(Date jntuDateOfJoining) {
		this.jntuDateOfJoining = jntuDateOfJoining;
	}

	public String getJntuRegNo() {
		return jntuRegNo;
	}

	public void setJntuRegNo(String jntuRegNo) {
		this.jntuRegNo = jntuRegNo;
	}

	public Date getJoiningDate() {
		return joiningDate;
	}

	public void setJoiningDate(Date joiningDate) {
		this.joiningDate = joiningDate;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLicNo() {
		return licNo;
	}

	public void setLicNo(String licNo) {
		this.licNo = licNo;
	}

	public String getLinkedinUrl() {
		return linkedinUrl;
	}

	public void setLinkedinUrl(String linkedinUrl) {
		this.linkedinUrl = linkedinUrl;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getMonthlySalary() {
		return monthlySalary;
	}

	public void setMonthlySalary(Integer monthlySalary) {
		this.monthlySalary = monthlySalary;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public Integer getNonTeachingExp() {
		return nonTeachingExp;
	}

	public void setNonTeachingExp(Integer nonTeachingExp) {
		this.nonTeachingExp = nonTeachingExp;
	}

	public String getOfficialMobile() {
		return officialMobile;
	}

	public void setOfficialMobile(String officialMobile) {
		this.officialMobile = officialMobile;
	}

	public String getPancard() {
		return pancard;
	}

	public void setPancard(String pancard) {
		this.pancard = pancard;
	}

	public String getPancardPath() {
		return pancardPath;
	}

	public void setPancardPath(String pancardPath) {
		this.pancardPath = pancardPath;
	}

	public String getPassportNo() {
		return passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public String getPassportPath() {
		return passportPath;
	}

	public void setPassportPath(String passportPath) {
		this.passportPath = passportPath;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getPermanentMandal() {
		return permanentMandal;
	}

	public void setPermanentMandal(String permanentMandal) {
		this.permanentMandal = permanentMandal;
	}

	public String getPermanentPincode() {
		return permanentPincode;
	}

	public void setPermanentPincode(String permanentPincode) {
		this.permanentPincode = permanentPincode;
	}

	public String getPermanentStreet() {
		return permanentStreet;
	}

	public void setPermanentStreet(String permanentStreet) {
		this.permanentStreet = permanentStreet;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getPresentMandal() {
		return presentMandal;
	}

	public void setPresentMandal(String presentMandal) {
		this.presentMandal = presentMandal;
	}

	public String getPresentPincode() {
		return presentPincode;
	}

	public void setPresentPincode(String presentPincode) {
		this.presentPincode = presentPincode;
	}

	public String getPresentStreet() {
		return presentStreet;
	}

	public void setPresentStreet(String presentStreet) {
		this.presentStreet = presentStreet;
	}

	public Date getPromotedDate() {
		return promotedDate;
	}

	public void setPromotedDate(Date promotedDate) {
		this.promotedDate = promotedDate;
	}

	public Long getResearchExp() {
		return researchExp;
	}

	public void setResearchExp(Long researchExp) {
		this.researchExp = researchExp;
	}

	public String getResidencePhone() {
		return residencePhone;
	}

	public void setResidencePhone(String residencePhone) {
		this.residencePhone = residencePhone;
	}

	public Date getResignationDate() {
		return resignationDate;
	}

	public void setResignationDate(Date resignationDate) {
		this.resignationDate = resignationDate;
	}

	public Integer getServiceBreakYrs() {
		return serviceBreakYrs;
	}

	public void setServiceBreakYrs(Integer serviceBreakYrs) {
		this.serviceBreakYrs = serviceBreakYrs;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public Integer getTeachingExp() {
		return teachingExp;
	}

	public void setTeachingExp(Integer teachingExp) {
		this.teachingExp = teachingExp;
	}

	public Integer getTenureDays() {
		return tenureDays;
	}

	public void setTenureDays(Integer tenureDays) {
		this.tenureDays = tenureDays;
	}

	public String getVoterId() {
		return voterId;
	}

	public void setVoterId(String voterId) {
		this.voterId = voterId;
	}

	public String getVoterIdPath() {
		return voterIdPath;
	}

	public void setVoterIdPath(String voterIdPath) {
		this.voterIdPath = voterIdPath;
	}

	public Date getWeddingDate() {
		return weddingDate;
	}

	public void setWeddingDate(Date weddingDate) {
		this.weddingDate = weddingDate;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public Long getMaritalStatusId() {
		return maritalStatusId;
	}

	public void setMaritalStatusId(Long maritalStatusId) {
		this.maritalStatusId = maritalStatusId;
	}

	public Long getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Long districtId) {
		this.districtId = districtId;
	}

	public Long getEmpStatusId() {
		return empStatusId;
	}

	public void setEmpStatusId(Long empStatusId) {
		this.empStatusId = empStatusId;
	}

	public Long getEmpStateId() {
		return empStateId;
	}

	public void setEmpStateId(Long empStateId) {
		this.empStateId = empStateId;
	}

	public Long getEmpTypeId() {
		return empTypeId;
	}

	public void setEmpTypeId(Long empTypeId) {
		this.empTypeId = empTypeId;
	}

	public Long getEmpDeptId() {
		return empDeptId;
	}

	public void setEmpDeptId(Long empDeptId) {
		this.empDeptId = empDeptId;
	}

	public Long getEmpWorkingDeptId() {
		return empWorkingDeptId;
	}

	public void setEmpWorkingDeptId(Long empWorkingDeptId) {
		this.empWorkingDeptId = empWorkingDeptId;
	}

	public Long getQualificationId() {
		return qualificationId;
	}

	public void setQualificationId(Long qualificationId) {
		this.qualificationId = qualificationId;
	}

	public Long getDesignationId() {
		return designationId;
	}

	public void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}

	public Long getWorkingDesignationId() {
		return workingDesignationId;
	}

	public void setWorkingDesignationId(Long workingDesignationId) {
		this.workingDesignationId = workingDesignationId;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public Long getEmpCategoryId() {
		return empCategoryId;
	}

	public void setEmpCategoryId(Long empCategoryId) {
		this.empCategoryId = empCategoryId;
	}

	public Long getEmpWrkCategoryId() {
		return empWrkCategoryId;
	}

	public void setEmpWrkCategoryId(Long empWrkCategoryId) {
		this.empWrkCategoryId = empWrkCategoryId;
	}

	public Long getTeachingforId() {
		return teachingforId;
	}

	public void setTeachingforId(Long teachingforId) {
		this.teachingforId = teachingforId;
	}

	public Long getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(Long appointmentId) {
		this.appointmentId = appointmentId;
	}

	public Long getCityPresentId() {
		return cityPresentId;
	}

	public void setCityPresentId(Long cityPresentId) {
		this.cityPresentId = cityPresentId;
	}

	public Long getDistrictPresentId() {
		return districtPresentId;
	}

	public void setDistrictPresentId(Long districtPresentId) {
		this.districtPresentId = districtPresentId;
	}

	public Long getCityPermanentId() {
		return cityPermanentId;
	}

	public void setCityPermanentId(Long cityPermanentId) {
		this.cityPermanentId = cityPermanentId;
	}

	public Long getDistrictPermanentId() {
		return districtPermanentId;
	}

	public void setDistrictPermanentId(Long districtPermanentId) {
		this.districtPermanentId = districtPermanentId;
	}

	public Long getBloodgroupId() {
		return bloodgroupId;
	}

	public void setBloodgroupId(Long bloodgroupId) {
		this.bloodgroupId = bloodgroupId;
	}

	public Long getPaymodeId() {
		return paymodeId;
	}

	public void setPaymodeId(Long paymodeId) {
		this.paymodeId = paymodeId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getResidentId() {
		return residentId;
	}

	public void setResidentId(Long residentId) {
		this.residentId = residentId;
	}

	public Long getAccommodationId() {
		return accommodationId;
	}

	public void setAccommodationId(Long accommodationId) {
		this.accommodationId = accommodationId;
	}

	public Long getTitleId() {
		return titleId;
	}

	public void setTitleId(Long titleId) {
		this.titleId = titleId;
	}

	public Long getGenderId() {
		return genderId;
	}

	public void setGenderId(Long genderId) {
		this.genderId = genderId;
	}

	public Long getReligionId() {
		return religionId;
	}

	public void setReligionId(Long religionId) {
		this.religionId = religionId;
	}

	public Long getNationalityId() {
		return nationalityId;
	}

	public void setNationalityId(Long nationalityId) {
		this.nationalityId = nationalityId;
	}

	public Long getCasteId() {
		return casteId;
	}

	public void setCasteId(Long casteId) {
		this.casteId = casteId;
	}

	public Long getSubCasteId() {
		return subCasteId;
	}

	public void setSubCasteId(Long subCasteId) {
		this.subCasteId = subCasteId;
	}

	public Long getEmpgrade() {
		return empgrade;
	}

	public void setEmpgrade(Long empgrade) {
		this.empgrade = empgrade;
	}

	public List<StaffCourseyrSubjectDTO> getStaffCourseyrSubjects() {
		return staffCourseyrSubjects;
	}

	public void setStaffCourseyrSubjects(List<StaffCourseyrSubjectDTO> staffCourseyrSubjects) {
		this.staffCourseyrSubjects = staffCourseyrSubjects;
	}

	
	public List<EmployeeDocumentCollectionDTO> getEmployeeDocumentCollection() {
		return employeeDocumentCollection;
	}

	public void setEmployeeDocumentCollection(List<EmployeeDocumentCollectionDTO> employeeDocumentCollection) {
		this.employeeDocumentCollection = employeeDocumentCollection;
	}

	public List<EmployeeDocumentCollectionDTO> getVerifiedEmployeeDocumentCollection() {
		return verifiedEmployeeDocumentCollection;
	}

	public void setVerifiedEmployeeDocumentCollection(
			List<EmployeeDocumentCollectionDTO> verifiedEmployeeDocumentCollection) {
		this.verifiedEmployeeDocumentCollection = verifiedEmployeeDocumentCollection;
	}

	public List<EmployeeBankDetailDTO> getEmployeeBankDetails() {
		return employeeBankDetails;
	}

	public void setEmployeeBankDetails(List<EmployeeBankDetailDTO> employeeBankDetails) {
		this.employeeBankDetails = employeeBankDetails;
	}

	public List<EmployeeReportingDTO> getEmployeeReportings() {
		return employeeReportings;
	}

	public void setEmployeeReportings(List<EmployeeReportingDTO> employeeReportings) {
		this.employeeReportings = employeeReportings;
	}

	public List<EmployeeReportingDTO> getEmployeeReportingMangers() {
		return employeeReportingMangers;
	}

	public void setEmployeeReportingMangers(List<EmployeeReportingDTO> employeeReportingMangers) {
		this.employeeReportingMangers = employeeReportingMangers;
	}

	public List<EmployeeExperienceDetailDTO> getEmpExperienceDetails() {
		return empExperienceDetails;
	}

	public void setEmpExperienceDetails(List<EmployeeExperienceDetailDTO> empExperienceDetails) {
		this.empExperienceDetails = empExperienceDetails;
	}

	public List<EmployeeEducationDTO> getEmployeeEducations() {
		return employeeEducations;
	}

	public void setEmployeeEducations(List<EmployeeEducationDTO> employeeEducations) {
		this.employeeEducations = employeeEducations;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Long getPermanentStateId() {
		return permanentStateId;
	}

	public String getPermanentStateName() {
		return permanentStateName;
	}

	public Long getPermanentCountryId() {
		return permanentCountryId;
	}

	public String getPermanentCountryName() {
		return permanentCountryName;
	}

	public Long getPresentStateId() {
		return presentStateId;
	}

	public String getPresentStateName() {
		return presentStateName;
	}

	public Long getPresentCountryId() {
		return presentCountryId;
	}

	public String getPresentCountryName() {
		return presentCountryName;
	}

	public void setPermanentStateId(Long permanentStateId) {
		this.permanentStateId = permanentStateId;
	}

	public void setPermanentStateName(String permanentStateName) {
		this.permanentStateName = permanentStateName;
	}

	public void setPermanentCountryId(Long permanentCountryId) {
		this.permanentCountryId = permanentCountryId;
	}

	public void setPermanentCountryName(String permanentCountryName) {
		this.permanentCountryName = permanentCountryName;
	}

	public void setPresentStateId(Long presentStateId) {
		this.presentStateId = presentStateId;
	}

	public void setPresentStateName(String presentStateName) {
		this.presentStateName = presentStateName;
	}

	public void setPresentCountryId(Long presentCountryId) {
		this.presentCountryId = presentCountryId;
	}

	public void setPresentCountryName(String presentCountryName) {
		this.presentCountryName = presentCountryName;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDesignationName() {
		return designationName;
	}

	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}

	public String getEmpCategoryName() {
		return empCategoryName;
	}

	public void setEmpCategoryName(String empCategoryName) {
		this.empCategoryName = empCategoryName;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getEmpGradeName() {
		return empGradeName;
	}

	public void setEmpGradeName(String empGradeName) {
		this.empGradeName = empGradeName;
	}

	public String getEmpGradeCode() {
		return empGradeCode;
	}

	public void setEmpGradeCode(String empGradeCode) {
		this.empGradeCode = empGradeCode;
	}

	public String getReportingManagerName() {
		return reportingManagerName;
	}

	public void setReportingManagerName(String reportingManagerName) {
		this.reportingManagerName = reportingManagerName;
	}

	public String getEmpStatusCode() {
		return empStatusCode;
	}

	public void setEmpStatusCode(String empStatusCode) {
		this.empStatusCode = empStatusCode;
	}

	public String getEmpStatusDisplayName() {
		return empStatusDisplayName;
	}

	public void setEmpStatusDisplayName(String empStatusDisplayName) {
		this.empStatusDisplayName = empStatusDisplayName;
	}

	public String getEmpStateCode() {
		return empStateCode;
	}

	public void setEmpStateCode(String empStateCode) {
		this.empStateCode = empStateCode;
	}

	public String getEmpDisplayName() {
		return empDisplayName;
	}

	public void setEmpDisplayName(String empDisplayName) {
		this.empDisplayName = empDisplayName;
	}

	public String getReportingManagerNumber() {
		return reportingManagerNumber;
	}

	public void setReportingManagerNumber(String reportingManagerNumber) {
		this.reportingManagerNumber = reportingManagerNumber;
	}
}
