package com.gts.cms.common.enums;

public enum ReportsEnum {
	
	BUS_FEE_PARTICULAR("Transport"),
	LIBRARY_FEE_PARTICULAR("LF"),
	STD_MANAGEMENT_QUOTA("MGMT"),
	TC_ISSUED("TCISSUED"),
	//TCWFSETTING("VCETCWFSETTING");
	TCWFSETTING("TC Workflow Setting"),
	NODUE("NO DUE");
	
	private String id;

	private ReportsEnum(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
