package com.gts.cms.common.student.service.impl;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.StudentbatchDTO;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.student.repository.StudentBatchRepository;
import com.gts.cms.common.student.service.StudentBatchService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentBatchServiceImpl implements StudentBatchService {

	private static final Logger LOGGER = Logger.getLogger(StudentBatchServiceImpl.class);
	
	/*@Autowired
	private StudentbatchMapper studentbatchMapper;*/

	@Autowired
    StudentBatchRepository studentBatchRepository;


	@Override
	public ApiResponse<List<StudentbatchDTO>> getStudentBatches(Boolean status, Long schoolId, Long subjecttypeId) {
		ApiResponse<List<StudentbatchDTO>> response = null;
		if (null == status) {
			status = Boolean.TRUE;
		}
		try {
			/*List<Studentbatch> studentbatchDetail = studentBatchRepository
					.findStudentBatches(status,schoolId, subjecttypeId);
			if (!studentbatchDetail.isEmpty()) {
				response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(),
						studentbatchMapper.convertEntityListToDTOList(studentbatchDetail), HttpStatus.OK.value());
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
						HttpStatus.OK.value());
			}*/
		} catch (Exception e) {
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;
	}

}
