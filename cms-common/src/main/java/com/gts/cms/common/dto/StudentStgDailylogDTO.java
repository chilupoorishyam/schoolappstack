package com.gts.cms.common.dto;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

public class StudentStgDailylogDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long stdStgDailylogId;
	private String biometricCode;
	private Date createdDt;
	private Long createdUser;
	private Date dateofdutyRaw;
	private Integer eventDate;
	private Long eventId;
	private Time eventTime;
	private Date eventdate;
	private Long biometricId;
	private String location;
	private String rfid;
	private Integer seqNo;
	private String servicetagId;
	private Long schoolId;
	private String schoolName;
	public Long getStdStgDailylogId() {
		return stdStgDailylogId;
	}
	public void setStdStgDailylogId(Long stdStgDailylogId) {
		this.stdStgDailylogId = stdStgDailylogId;
	}
	public String getBiometricCode() {
		return biometricCode;
	}
	public void setBiometricCode(String biometricCode) {
		this.biometricCode = biometricCode;
	}
	public Date getCreatedDt() {
		return createdDt;
	}
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	public Long getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}
	public Date getDateofdutyRaw() {
		return dateofdutyRaw;
	}
	public void setDateofdutyRaw(Date dateofdutyRaw) {
		this.dateofdutyRaw = dateofdutyRaw;
	}
	public Integer getEventDate() {
		return eventDate;
	}
	public void setEventDate(Integer eventDate) {
		this.eventDate = eventDate;
	}
	public Long getEventId() {
		return eventId;
	}
	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}
	public Time getEventTime() {
		return eventTime;
	}
	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}
	public Date getEventdate() {
		return eventdate;
	}
	public void setEventdate(Date eventdate) {
		this.eventdate = eventdate;
	}
	public Long getBiometricId() {
		return biometricId;
	}
	public void setBiometricId(Long biometricId) {
		this.biometricId = biometricId;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getRfid() {
		return rfid;
	}
	public void setRfid(String rfid) {
		this.rfid = rfid;
	}
	public Integer getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}
	public String getServicetagId() {
		return servicetagId;
	}
	public void setServicetagId(String servicetagId) {
		this.servicetagId = servicetagId;
	}
	public Long getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	
	
}
