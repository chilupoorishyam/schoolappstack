package com.gts.cms.common.repository.custom;

import com.gts.cms.entity.Subject;

import java.util.List;

public interface SubjectRepositoryCustom {
	//List<Subject> getSubjects(Long schoolId, Long courseId);
	List<Subject> getsubjects(Boolean isActive, Long schoolId, String subjectTypeCode);

	List<Subject> searchSubjectDetails(String subjectName, String subjectCode, String shortName, Long schoolId, Long courseId);
}
