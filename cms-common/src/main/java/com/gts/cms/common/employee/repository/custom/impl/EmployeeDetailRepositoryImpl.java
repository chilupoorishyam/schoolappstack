package com.gts.cms.common.employee.repository.custom.impl;

import com.gts.cms.common.employee.repository.custom.EmployeeDetailRepositoryCustom;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.entity.EmployeeDetail;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.dml.UpdateClause;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAUpdateClause;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static com.gts.cms.entity.QEmployeeDetail.employeeDetail;

/**
 * @author Genesis
 *
 */
@Repository
public class EmployeeDetailRepositoryImpl extends QuerydslRepositorySupport implements EmployeeDetailRepositoryCustom {

	public EmployeeDetailRepositoryImpl() {
		super(EmployeeDetailRepositoryImpl.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public Long deleteEmployeeDetail(Long employeeId) {

		UpdateClause<JPAUpdateClause> updateCount = update(employeeDetail)
				.where(employeeDetail.employeeId.eq(employeeId)).set(employeeDetail.isActive, false);
		Long parent = updateCount.execute();

		/*
		 * UpdateClause<JPAUpdateClause> updateChildCount1 = update(employeeBankDetail)
		 * .where(employeeBankDetail.employeeDetail.employeeId.eq(employeeId))
		 * .set(employeeBankDetail.isActive, false); Long child1 =
		 * updateChildCount1.execute();
		 * 
		 * UpdateClause<JPAUpdateClause> updateChildCount2 =
		 * update(employeeDocumentCollection)
		 * .where(employeeDocumentCollection.employeeDetail.employeeId.eq(employeeId))
		 * .set(employeeDocumentCollection.isActive, false); Long child2 =
		 * updateChildCount2.execute();
		 * 
		 * UpdateClause<JPAUpdateClause> updateChildCount3 = update(employeeReporting)
		 * .where(employeeReporting.employeeDetail.employeeId.eq(employeeId))
		 * .set(employeeReporting.isActive, false); Long child3 =
		 * updateChildCount3.execute();
		 * 
		 * UpdateClause<JPAUpdateClause> updateChildCount4 =
		 * update(employeeExperienceDetail)
		 * .where(employeeExperienceDetail.employeeDetail.employeeId.eq(employeeId))
		 * .set(employeeExperienceDetail.isActive, false); Long child4 =
		 * updateChildCount4.execute();
		 * 
		 * UpdateClause<JPAUpdateClause> updateChildCount5 = update(employeeEducation)
		 * .where(employeeEducation.empDetail.employeeId.eq(employeeId))
		 * .set(employeeEducation.isActive, false); Long child5 =
		 * updateChildCount5.execute();
		 */

		return parent;
	}

	@Override
	public Long uploadEmployeeApplicationFiles(Long employeeId, String aadharFileName, String pancardFileName,
			String passportFileName, String photoFileName, String voterIdFileName) {
		UpdateClause<JPAUpdateClause> query = update(employeeDetail).where(employeeDetail.employeeId.eq(employeeId));
		if (aadharFileName != null) {
			query.set(employeeDetail.aadharPath, aadharFileName);
		}
		if (pancardFileName != null) {
			query.set(employeeDetail.pancardPath, pancardFileName);
		}
		if (photoFileName != null) {
			query.set(employeeDetail.photoPath, photoFileName);
		}
		if (voterIdFileName != null) {
			query.set(employeeDetail.voterIdPath, voterIdFileName);
		}
		if (passportFileName != null) {
			query.set(employeeDetail.passportPath, passportFileName);
		}
		return query.execute();
	}

	public List<EmployeeDetail> findAllEmployeeDetail(Long schoolId, Long empDeptId, Boolean status, String aadharNo,
													  String empNumber, String firstName, String mobile) {
		JPQLQuery<EmployeeDetail> query = null;

		query = from(employeeDetail);
		
		if (status != null) {
			query.where(employeeDetail.isActive.eq(status));
		} else {
			query.where(employeeDetail.isActive.eq(Boolean.TRUE));
		}
		if (schoolId != null) {
			query.where(employeeDetail.school.schoolId.eq(schoolId));
		}
		if (empDeptId != null) {
			query.where(employeeDetail.employeeDepartment.departmentId.eq(empDeptId));
		}

		if (empNumber != null) {
			query.where(employeeDetail.empNumber.eq(empNumber));
		}
		if (aadharNo != null) {
			query.where(employeeDetail.aadharNo.eq(aadharNo));
		}
		if (firstName != null) {
			query.where(employeeDetail.firstName.contains(firstName));
		}
		if (mobile != null) {
			query.where(employeeDetail.mobile.eq(mobile));
		}
		query.where(employeeDetail.employeeStatus.generalDetailCode.eq(Messages.ACTIVE.getValue()));
		query.orderBy(employeeDetail.createdDt.asc());

		return query.fetch();
	}
	
	public List<EmployeeDetail> findEmployeeDetails(Long organizationId,Long schoolId, Long departmentId, Long designationId, Long employeeId, Boolean status) {
		JPQLQuery<EmployeeDetail> query = null;

		query = from(employeeDetail);
		
		if (status != null) {
			query.where(employeeDetail.isActive.eq(status));
		} else {
			query.where(employeeDetail.isActive.eq(Boolean.TRUE));
		}
		if(organizationId != null) {
			query.where(employeeDetail.organization.organizationId.eq(organizationId));
		}
		if (schoolId != null) {
			query.where(employeeDetail.school.schoolId.eq(schoolId));
		}
		if (departmentId != null) {
			query.where(employeeDetail.employeeDepartment.departmentId.eq(departmentId));
		}
		if(designationId != null) {
			query.where(employeeDetail.designation.designationId.eq(designationId));
		}
		if(employeeId != null) {
			query.where(employeeDetail.employeeId.eq(employeeId));
		}

		return query.fetch();
	}
	
	@Override
	public List<EmployeeDetail> searchEmployeeDetails(Long schoolId, String firstName, String lastName, String middleName,
			String employeeNo, String mobile,Long deptId,String empStatus) {
		JPQLQuery<EmployeeDetail> jpqlQuery = from(employeeDetail);

		BooleanBuilder builder = new BooleanBuilder();
		
		if (employeeNo != null) {
			builder.or(employeeDetail.empNumber.equalsIgnoreCase(employeeNo));
		}
		if (firstName != null) {
			builder.or(employeeDetail.firstName.like("%"+firstName+"%"));
		}
		if (lastName != null) {
			builder.or(employeeDetail.lastName.like("%"+lastName+"%"));
		}
		if (middleName != null) {
			builder.or(employeeDetail.middleName.like("%"+middleName+"%"));
		}
		if (mobile != null) {
			builder.or(employeeDetail.mobile.eq(mobile));
		}
		if (schoolId != null) {
			builder.and(employeeDetail.school.schoolId.eq(schoolId));
		}
		if(deptId!=null) {;
			builder.and(employeeDetail.employeeDepartment.departmentId.eq(deptId));
		}
		builder.and(employeeDetail.isActive.eq(true));
		//builder.and(employeeDetail.employeeStatus.generalDetailId.isNull());
		
		if(empStatus!=null) {
			builder.and(employeeDetail.employeeStatus.generalDetailCode.eq(empStatus));
		}
		jpqlQuery.where(builder).orderBy(employeeDetail.employeeId.asc());
		return jpqlQuery.fetch();
	}
	
}
