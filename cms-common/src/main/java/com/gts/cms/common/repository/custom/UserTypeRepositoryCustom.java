package com.gts.cms.common.repository.custom;

import com.gts.cms.entity.Usertype;

/**
 * created by Naveen on 04/09/2018 
 * 
*/
public interface UserTypeRepositoryCustom {
	
	Usertype getUserTypeByCode(String userTypeCode);
	

}
