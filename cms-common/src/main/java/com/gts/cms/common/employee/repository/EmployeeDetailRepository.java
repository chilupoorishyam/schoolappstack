package com.gts.cms.common.employee.repository;

import com.gts.cms.common.employee.repository.custom.EmployeeDetailRepositoryCustom;
import com.gts.cms.entity.EmployeeDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author Genesis
 *
 */
@Repository
public interface EmployeeDetailRepository extends JpaRepository<EmployeeDetail, Long>, EmployeeDetailRepositoryCustom {
	
	@Query("SELECT e FROM EmployeeDetail e"
			+ " WHERE (:employeeId is null or e.employeeId = :employeeId)"
			+ " AND (:userId is null or e.user.userId = :userId)"
			+ " AND e.isActive = true"
			)
	Optional<EmployeeDetail> findByEmployeeIdAndIsActiveTrue(@Param("employeeId") Long employeeId,
			@Param("userId") Long userId);

	@Query("SELECT DISTINCT ed FROM EmployeeDetail ed" + " JOIN FETCH ed.empAttendances1 ea "
			+ " WHERE 1=1 and (:schoolId is null or ed.school.schoolId = :schoolId)"
			+ " AND (:attendanceDate is null or ea.attendanceDate = :attendanceDate)"
			+ " AND (:isActive is null or ea.isActive = :isActive)" + " AND ed.isActive = true")
	List<EmployeeDetail> findEmployeeAttendanceList(@Param("isActive") Boolean status,
			@Param("schoolId") Long schoolId,@Param("attendanceDate") Date attendanceDate);
	
	
	
	@Query("SELECT e.firstName FROM  EmployeeDetail e" 
			+ " WHERE e.employeeId IN (:employeeIdList)"
			+ " AND e.isActive = true"
			)
	List<String> findEmpDetails(@Param("employeeIdList") List<Long> employeeIdList);
	
	
	
	/*
	 * @Query("SELECT e FROM EmployeeDetail e" +
	 * " WHERE ( e.departmentId in ( select distinct departmentId from EmpDeptHeads where courseGroupId = :courseGroupId) )"
	 * + " AND e.isActive = true" ) Optional<EmployeeDetail>
	 * findByEmployeeDetailsByCourseGroup(@Param("courseGroupId") Long
	 * courseGroupId);
	 */
	

	/*@Query("SELECT v From SpecialActivity sac,ViewGetStudentDetails v "
			+ "JOIN sac.spclActivityAttendees saa"
			+ " where 1=1"
			+ " AND  v.groupSectionId=saa.groupSection.groupSectionId"
			+"  AND (:spclActivityId is null or sac.spclActivityId=:spclActivityId)"
			)*/
	 
	@Query("SELECT DISTINCT ed  FROM  EmployeeDetail ed,EmpDeptHeads edh,CounselorMapping cm"
			+" where 1=1"
			+" AND(ed.employeeDepartment.departmentId=edh.department.departmentId) "
			+ "AND(cm.employeeDetail.employeeId=ed.employeeId) "
			+ "AND (cm.school.schoolId =:schoolId) "
			+ "AND (:empDeptId is null or edh.department.departmentId=:empDeptId) "
			+ " AND (:status is null or ed.isActive = :status) "
			)	
	List<EmployeeDetail> findByDeptWiseCounselors(@Param("schoolId") Long schoolId,@Param("empDeptId") Long empDeptId,@Param("status") Boolean status);

	
	
	@Query("SELECT ed  FROM  EmployeeDetail ed"
			+" where 1=1"
			+" AND(:userId is null or ed.user.userId=:userId)"
			+ "AND ed.isActive = true"
			)	
	EmployeeDetail findByUserId(@Param("userId") Long userId);
	

	@Query(" SELECT dept.deptCode FROM Department dept "
			+ " JOIN dept.empDetails1 emp "
			+ " WHERE 1=1 "
			+ " AND (emp.employeeDepartment.departmentId=dept.departmentId )"
			+ " AND (:employeeId is null or emp.employeeId = :employeeId ) "
			+ " AND (emp.isActive= true) "
			+ " AND (dept.isActive=true) "
			)
	
	String findByDeptCode(@Param("employeeId") Long employeeId);

	
	@Query("SELECT ed  FROM  EmployeeDetail ed"
			+" where 1=1"
			+" AND(:userId is null or ed.user.userId=:userId)"
			+ " AND (ed.employeeStatus.generalDetailCode=:value )"
			+ " AND (ed.isActive=true)"
			)
	EmployeeDetail findByUserId(@Param("userId")Long userId, @Param("value")String value);
	
	@Query("SELECT era.firstName FROM  EmployeeDetail era" 
			+ " WHERE era.employeeId IN (:invigilationEmpIds)"
			+ " AND era.isActive = true"
			)
	List<String> findEmplListDetails(@Param("invigilationEmpIds") List<Long> invigilationEmpIds);
	
	@Query("SELECT ed FROM EmployeeDetail ed"
			+ " WHERE 1=1"
			+ " AND ed.reportingManager.employeeId =:employeeId "
			+ " AND ed.isActive = true ")
	List<EmployeeDetail> findReportingDetails(@Param("employeeId") Long employeeId);
	
	
	@Query("SELECT e.user.userId FROM EmployeeDetail e "
			+ " WHERE 1=1"
			+ " AND (:generalDetailId is null or e.employeeCategory.generalDetailId =:generalDetailId)"
			+ " AND e.isActive=true ")
	List<Long> findByEmpCatId(@Param("generalDetailId") Long generalDetailId);
	
	/*SELECT emp.`fk_user_id`
	FROM 
	`t_emp_details` emp LEFT JOIN`t_m_general_details` gd
	    ON emp.fk_emp_category_catdet_id = gd.`pk_gd_id`
	       AND emp.`is_active` =1
	       AND gd.`is_active`=1
	 WHERE 1=1
	 AND gd.`gd_code`='NTCHNGSTF'
	*/
	
	@Query("SELECT emp.user.userId FROM "
			+ " EmployeeDetail emp LEFT JOIN GeneralDetail gd "
			+ "   ON emp.employeeCategory.generalDetailId = gd.generalDetailId "
			+ " WHERE 1=1"
			+ " AND(:generalDetailCode is null or gd.generalDetailCode =:generalDetailCode)"
			+ " AND(:schoolId is null or emp.school.schoolId=:schoolId) "
			 )
	List<Long> findByCode(@Param("generalDetailCode") String  generalDetailCode,@Param("schoolId") Long schoolId);

	@Query("SELECT ed FROM EmployeeDetail ed"
			+ " WHERE 1=1"
			+ " AND ed.firstName =:firstName "
			+ " AND ed.lastName =:lastName "
			+ " AND ed.isActive = true ")
	List<EmployeeDetail> checkEmployeeExists(@Param("firstName") String firstName,
											  @Param("lastName") String lastName);

	@Query("SELECT ed FROM EmployeeDetail ed"
			+ " WHERE 1=1"
			+ " AND ed.employeeId =:employeeId "
			+ " AND ed.isActive = true ")
	EmployeeDetail findByEmpId(@Param("employeeId") Long employeeId);

	@Query("SELECT ed FROM EmployeeDetail ed"
			+ " WHERE 1=1"
			+ " AND ed.empNumber =:empNumber "
			+ " AND ed.isActive = true ")
	EmployeeDetail findByEmpNumber(@Param("empNumber") String empNumber);

	@Query("SELECT e.user.userId FROM  EmployeeDetail e"
			+ " WHERE e.employeeId =:employeeId"
			+ " AND e.isActive = true"
	)
	Long findEmpUserId(@Param("employeeId") Long employeeId);

	@Query("SELECT e.user.userId FROM EmployeeDetail e "
			+ " WHERE 1=1"
			+ " AND (e.employeeId = :empMentorId)"
			+ " AND e.isActive=true "
	)
	Long fetchEmpUserIds(Long empMentorId);
}
