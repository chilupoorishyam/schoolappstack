package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.FinancialYearDTO;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.FinancialYear;
import com.gts.cms.entity.Organization;
import com.gts.cms.entity.School;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class FinancialYearMapper implements BaseMapper<FinancialYear, FinancialYearDTO> {
	@Override
	public FinancialYearDTO convertEntityToDTO(FinancialYear financialYear) {
		FinancialYearDTO financialYearDTO = null;
		if (financialYear != null) {
			financialYearDTO = new FinancialYearDTO();
			financialYearDTO.setFinancialYearId(financialYear.getFinancialYearId());
			if (financialYear.getOrganization() != null) {
				financialYearDTO.setOrganizationId(financialYear.getOrganization().getOrganizationId());
				financialYearDTO.setOrgName(financialYear.getOrganization().getOrgName());
				financialYearDTO.setOrgCode(financialYear.getOrganization().getOrgCode());
			}
			if (financialYear.getSchool() != null) {
				financialYearDTO.setSchoolId(financialYear.getSchool().getSchoolId());
				financialYearDTO.setSchoolName(financialYear.getSchool().getSchoolName());
				financialYearDTO.setSchoolCode(financialYear.getSchool().getSchoolCode());
			}
			financialYearDTO.setFinancialYear(financialYear.getFinancialYear());
			financialYearDTO.setFromDate(financialYear.getFromDate());
			financialYearDTO.setToDate(financialYear.getToDate());
			financialYearDTO.setIsDefault(financialYear.getIsDefault());
			financialYearDTO.setIsActive(financialYear.getIsActive());
			financialYearDTO.setReason(financialYear.getReason());
			financialYearDTO.setSortOrder(financialYear.getSortOrder());
		}
		return financialYearDTO;
	}

	@Override
	public FinancialYear convertDTOtoEntity(FinancialYearDTO financialYearDTO, FinancialYear financialYear) {
		if (null == financialYear) {
			financialYear = new FinancialYear();
			financialYear.setIsActive(Boolean.TRUE);
			financialYear.setCreatedDt(new Date());
			financialYear.setCreatedUser(SecurityUtil.getCurrentUser());

		} else {
			financialYear.setIsActive(financialYearDTO.getIsActive());
		}
		financialYear.setFinancialYearId(financialYearDTO.getFinancialYearId());
		if (financialYearDTO.getOrganizationId() != null) {
			Organization organization = new Organization();
			organization.setOrganizationId(financialYearDTO.getOrganizationId());
			organization.setOrgName(financialYearDTO.getOrgName());
			financialYear.setOrganization(organization);
		}
		if (financialYearDTO.getSchoolId() != null) {
			School school = new School();
			school.setSchoolId(financialYearDTO.getSchoolId());
			school.setSchoolName(financialYearDTO.getSchoolName());
			financialYear.setSchool(school);
		}
		financialYear.setFinancialYear(financialYearDTO.getFinancialYear());
		financialYear.setFromDate(financialYearDTO.getFromDate());
		financialYear.setToDate(financialYearDTO.getToDate());
		financialYear.setIsDefault(financialYearDTO.getIsDefault());
		financialYear.setReason(financialYearDTO.getReason());
		financialYear.setSortOrder(financialYearDTO.getSortOrder());
		financialYear.setUpdatedDt(new Date());
		financialYear.setUpdatedUser(SecurityUtil.getCurrentUser());
		return financialYear;
	}

	@Override
	public List<FinancialYear> convertDTOListToEntityList(List<FinancialYearDTO> financialYearDTOList) {
		List<FinancialYear> financialYearList = new ArrayList<>();
		financialYearDTOList
				.forEach(financialYearDTO -> financialYearList.add(convertDTOtoEntity(financialYearDTO, null)));
		return financialYearList;

	}

	@Override
	public List<FinancialYearDTO> convertEntityListToDTOList(List<FinancialYear> financialYearList) {
		List<FinancialYearDTO> financialYearDTOList = new ArrayList<>();
		financialYearList.forEach(financialYear -> financialYearDTOList.add(convertEntityToDTO(financialYear)));
		return financialYearDTOList;
	}

}
