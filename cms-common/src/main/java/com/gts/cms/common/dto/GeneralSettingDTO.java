package com.gts.cms.common.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class GeneralSettingDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long generalSettingId;
	private Long schoolId;
	private String schoolName;
	private String schoolCode;
	private String settingName;
	private String settingCode;
	private String settingValue;
	@NotNull(message = "IsActive is required.")
	private Boolean isActive;
	private String reason;

	public Long getGeneralSettingId() {
		return generalSettingId;
	}

	public void setGeneralSettingId(Long generalSettingId) {
		this.generalSettingId = generalSettingId;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolCode() {
		return this.schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getSettingName() {
		return settingName;
	}

	public void setSettingName(String settingName) {
		this.settingName = settingName;
	}

	public String getSettingCode() {
		return settingCode;
	}

	public void setSettingCode(String settingCode) {
		this.settingCode = settingCode;
	}

	public String getSettingValue() {
		return settingValue;
	}

	public void setSettingValue(String settingValue) {
		this.settingValue = settingValue;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
