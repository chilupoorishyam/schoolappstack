package com.gts.cms.common.employee.repository.custom;

/**
 * @author Genesis
 *
 */
public interface EmployeeDocumentCollectionRepositoryCustom {

	Long uploadEmployeeDocumentFiles(Long employeeId, String docRepId, String docName);

}
