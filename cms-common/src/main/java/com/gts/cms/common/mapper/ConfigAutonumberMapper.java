package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.ConfigAutonumberDTO;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.ConfigAutonumber;
import com.gts.cms.entity.Course;
import com.gts.cms.entity.Organization;
import com.gts.cms.entity.School;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

;

@Component
public class ConfigAutonumberMapper implements BaseMapper<ConfigAutonumber, ConfigAutonumberDTO> {
	@Override
	public ConfigAutonumberDTO convertEntityToDTO(ConfigAutonumber configAutoNumber) {
		ConfigAutonumberDTO configAutoNumberDTO = null;
		if (configAutoNumber != null) {
			configAutoNumberDTO = new ConfigAutonumberDTO();
			configAutoNumberDTO.setAutoconfigId(configAutoNumber.getAutoconfigId());
			if (configAutoNumber.getOrganization() != null) {
				configAutoNumberDTO.setOrganizationId(configAutoNumber.getOrganization().getOrganizationId());
				configAutoNumberDTO.setOrgName(configAutoNumber.getOrganization().getOrgName());
				configAutoNumberDTO.setOrgCode(configAutoNumber.getOrganization().getOrgCode());
			}
			/*if (configAutoNumber.getSchool() != null) {
				configAutoNumberDTO.setSchoolId(configAutoNumber.getSchool().getSchoolId());
				configAutoNumberDTO.setSchoolName(configAutoNumber.getSchool().getSchoolName());
				configAutoNumberDTO.setSchoolCode(configAutoNumber.getSchool().getSchoolCode());
			}*/
			if (configAutoNumber.getCourse() != null) {
				configAutoNumberDTO.setCourseId(configAutoNumber.getCourse().getCourseId());
				configAutoNumberDTO.setCourseName(configAutoNumber.getCourse().getCourseName());
				configAutoNumberDTO.setCourseCode(configAutoNumber.getCourse().getCourseCode());
			}

			configAutoNumberDTO.setConfigAttributeName(configAutoNumber.getConfigAttributeName());
			configAutoNumberDTO.setConfigAtttributeCode(configAutoNumber.getConfigAtttributeCode());
			configAutoNumberDTO.setPrefix(configAutoNumber.getPrefix());
			configAutoNumberDTO.setSuffix(configAutoNumber.getSuffix());
			configAutoNumberDTO.setIsAutoIncRequired(configAutoNumber.getIsAutoIncRequired());
			configAutoNumberDTO.setCurrentNumber(configAutoNumber.getCurrentNumber());
			configAutoNumberDTO.setResetBy(configAutoNumber.getResetBy());
			configAutoNumberDTO.setIsFormula(configAutoNumber.getIsFormula());
			configAutoNumberDTO.setIsActive(configAutoNumber.getIsActive());
			configAutoNumberDTO.setReason(configAutoNumber.getReason());
			configAutoNumberDTO.setCreatedDt(configAutoNumber.getCreatedDt());
			configAutoNumberDTO.setCreatedUser(configAutoNumber.getCreatedUser());

		}
		return configAutoNumberDTO;
	}

	@Override
	public ConfigAutonumber convertDTOtoEntity(ConfigAutonumberDTO configAutoNumberDTO,
			ConfigAutonumber configAutoNumber) {
		if (null == configAutoNumber) {
			configAutoNumber = new ConfigAutonumber();
			//configAutoNumber.setIsActive(Status.ACTIVE.getId());
			if(configAutoNumberDTO.getIsActive() != null) {
				configAutoNumber.setIsActive(configAutoNumberDTO.getIsActive());
			}else {
				configAutoNumber.setIsActive(Status.ACTIVE.getId());
			}
			configAutoNumber.setCreatedDt(new Date());
			configAutoNumber.setCreatedUser(SecurityUtil.getCurrentUser());
		}else{
			configAutoNumber.setIsActive(configAutoNumberDTO.getIsActive());
		}
		configAutoNumber.setAutoconfigId(configAutoNumberDTO.getAutoconfigId());
		if (configAutoNumberDTO.getOrganizationId() != null) {
			Organization organization = new Organization();
			organization.setOrganizationId(configAutoNumberDTO.getOrganizationId());
			organization.setOrgName(configAutoNumberDTO.getOrgName());
			configAutoNumber.setOrganization(organization);
		}

		if (configAutoNumberDTO.getSchoolId() != null) {
			School school = new School();
			school.setSchoolId(configAutoNumberDTO.getSchoolId());
			school.setSchoolName(configAutoNumberDTO.getSchoolName());
			configAutoNumber.setSchool(school);
		}
		if (configAutoNumberDTO.getCourseId() != null) {
			Course course = new Course();
			course.setCourseId(configAutoNumberDTO.getCourseId());
			course.setCourseName(configAutoNumberDTO.getCourseName());
			configAutoNumber.setCourse(course);
		}



		configAutoNumber.setConfigAttributeName(configAutoNumberDTO.getConfigAttributeName());
		configAutoNumber.setConfigAtttributeCode(configAutoNumberDTO.getConfigAtttributeCode());
		configAutoNumber.setPrefix(configAutoNumberDTO.getPrefix());
		configAutoNumber.setSuffix(configAutoNumberDTO.getSuffix());
		configAutoNumber.setIsAutoIncRequired(configAutoNumberDTO.getIsAutoIncRequired());
		configAutoNumber.setCurrentNumber(configAutoNumberDTO.getCurrentNumber());
		configAutoNumber.setResetBy(configAutoNumberDTO.getResetBy());
		configAutoNumber.setIsFormula(configAutoNumberDTO.getIsFormula());
		configAutoNumber.setReason(configAutoNumberDTO.getReason());
		configAutoNumber.setUpdatedDt(new Date());
		configAutoNumber.setUpdatedUser(SecurityUtil.getCurrentUser());

		return configAutoNumber;
	}

	@Override
	public List<ConfigAutonumber> convertDTOListToEntityList(List<ConfigAutonumberDTO> configAutoNumberDTOList) {
		List<ConfigAutonumber> configAutoNumberList = new ArrayList<>();
		configAutoNumberDTOList.forEach(
				configAutoNumberDTO -> configAutoNumberList.add(convertDTOtoEntity(configAutoNumberDTO, null)));
		return configAutoNumberList;

	}

	public List<ConfigAutonumberDTO> convertEntityListToDTOList(List<ConfigAutonumber> configAutoNumberList) {
		List<ConfigAutonumberDTO> configAutoNumberDTOList = new ArrayList<>();
		configAutoNumberList
				.forEach(configAutoNumber -> configAutoNumberDTOList.add(convertEntityToDTO(configAutoNumber)));
		return configAutoNumberDTOList;
	}

}
