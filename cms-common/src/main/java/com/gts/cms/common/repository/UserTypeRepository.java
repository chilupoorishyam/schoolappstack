package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.UserTypeRepositoryCustom;
import com.gts.cms.entity.Usertype;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * created by Naveen(Auto) on 02/09/2018
 * 
 */
@Repository
public interface UserTypeRepository extends JpaRepository<Usertype, Long>, UserTypeRepositoryCustom {

	@Query("SELECT u.userTypeId FROM Usertype u" 
			+ " WHERE u.userTypeCode = :userType"
			)
	Long findByUserName(@Param("userType") String userType);
}
