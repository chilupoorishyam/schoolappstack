package com.gts.cms.common.student.dto;

import com.gts.cms.common.dto.StudentAppActivityDTO;
import com.gts.cms.common.dto.StudentAppEducationDTO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class StudentDetailDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long studentId;
	private String aadharCardNo;
	private String aadharFilePath;
	private Date adminssionDate;
	private String admissionNumber;
	private String applicationNo;
	private String biometricCode;
	private Date createdDt;
	private Long createdUser;
	private Date dateOfBirth;
	private Date dateOfExpiry;
	private Date dateOfIssue;
	private Date dateOfRegistration;
	private String eamcetRank;
	private String entranceHtNo;
	private String fatherAddress;
	private String fatherEmailId;
	private String fatherMobileNo;
	private String fatherName;
	private String fatherOccupation;
	private String fatherPhotoPath;
	private String fatherQualification;
	private String firstName;
	private String folderPath;
	private String guardianAddress;
	private String guardianEmailId;
	private String guardianIncomePa;
	private String guardianMobileNo;
	private String guardianName;
	private String hallticketNumber;
	private String hobbies;
	private String identificationMarks;
	private String interests;
	private Boolean isActive;
	private Boolean isCurrentYear;
	private Boolean isLateral;
	private Boolean isLocal;
	private Boolean isScholorship;
	private Boolean isgovtempFather;
	private Boolean isgovtempMother;
	private Boolean isgovtempSpouse;
	private String langStatus1;
	private String langStatus2;
	private String langStatus3;
	private String langStatus4;
	private String langStatus5;
	private String lastName;
	private String middleName;
	private String mobile;
	private String motherAddress;
	private String motherEmailId;
	private String motherIncomePa;
	private String motherMobileNo;
	private String motherName;
	private String motherOccupation;
	private String motherPhotoPath;
	private String motherQualification;
	private String pancardFilePath;
	private String pancardNo;
	private String passportNo;
	private String permanentAddress;
	private String permanentPincode;
	private String permanentStreet;
	private String premanentMandal;
	private String presentAddress;
	private String presentMandal;
	private String presentPincode;
	private String presentStreet;
	private String primaryContact;
	private String reason;
	private String receiptNo;
	private String refApplicationNo;
	private String residencePhone;
	private String rfid;
	private String rollNumber;
	private String spouseAddress;
	private String spouseEmailId;
	private String spouseIncomePa;
	private String spouseMobileNo;
	private String spouseName;
	private String spouseOccupation;
	private String spousePhotoPath;
	private String spouseQualification;
	private String sscNo;
	private String stdEmailId;
	private String studentPhotoPath;
	private Date updatedDt;
	private Long updatedUser;
	private Date weddingDate;
	private Long batchId;
	private String batchName;
	private Long casteId;
	private String casteName;
	private Long cityId;
	private String cityCode;
	private String cityName;
	private Long schoolId;
	private String schoolCode;
	private String schoolName;
	private Long courseId;
	private String courseCode;
	private String courseName;
	private String groupCode;
	private String groupName;
	private String shortName;
	private Long courseYearId;
	private String courseYearCode;
	private String courseYearName;
	private Long districtId;
	private String districtCode;
	private String districtName;
	private Long disabilityId;
	private String disabilityDisplayName;
	private String disabilityCode;
	private Long nationalityId;
	private String nationalityDisplayName;
	private String nationalityCode;
	private Long religionId;
	private String religionDisplayName;
	private String religionCode;
	private Long bloodgroupId;
	private String bloodgroupDisplayName;
	private String bloodgroupCode;
	private Long maritalstatusId;
	private String maritalstatusDisplayName;
	private String maritalstatusCode;
	private Long language1Id;
	private String language1DisplayName;
	private String language1Code;
	private Long language2Id;
	private String language2DisplayName;
	private String language2Code;
	private Long language3Id;
	private String language3DisplayName;
	private String language3Code;
	private Long language4Id;
	private String language4DisplayName;
	private String language4Code;
	private Long language5Id;
	private String language5DisplayName;
	private String language5Code;
	private Long quotaId;
	private String quotaDisplayName;
	private String quotaCode;
	private Long studentTypeId;
	private String studentTypeDisplayName;
	private String studentTypeCode;
	private Long genderId;
	private String gender;
	private String genderCode;
	private Long organizationId;
	private String orgCode;
	private String orgName;
	private Long subCasteId;
	private String subCasteName;
	private Long userId;

	private Long userTypeId;
	private String resetPasswordCode;
	private String userName;

	private Long studentAppId;
	private String presentStreetName;
	private String studentEmailId;
	private Long permanentStateId;
	private String permanentStateName;
	private Long permanentCountryId;
	private String permanentCountryName;
	private Long presentCountryId;
	private String presentCountryName;

	private Long presentStateId;
	private String presentStateName;

	private Long groupSectionId;
	private String section;
	private Long academicYearId;
	private String academicYear;

	private List<StudentAppDocumentCollectionDTO> stdDocCollections;
	private List<StudentAppActivityDTO> stdActivities;
	private List<StudentAppEducationDTO> stdEducations;

	private String fathersIncomePa;
	private Long titleId;
	private String title;
	private String entranceHTNumber;
	private String permanentMandal;
	private Long presentDistrictId;
	private String presentDistrictName;
	private Long presentCityId;
	private String presentCityName;
	private Long permanentDistrictId;
	private String permanentDistrictName;
	private Long permanentCityId;
	private String permanentCityName;
	private Long qualifyingId;
	private String qualifyingName;
	private String qualifyingCode;
	private Boolean isMinority;
	private List<StudentDocumentCollectionDTO> studentDocumentCollections;
	private List<StudentActivitiesDetailDTO> studentActivitiesDetails;
	private List<StudentEducationDetailDTO> studentEducationDetails;


	private Long studentStatusId;
	private String studentStatusCode;
	private String studentStatusDisplayName;
	private Long parentUserId;
	private String parentFirstName;
	private Long parentId;

	public Long getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}

	//private List<FeeStudentDataDTO> feeStudentDataDTOs;

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getAadharCardNo() {
		return aadharCardNo;
	}

	public void setAadharCardNo(String aadharCardNo) {
		this.aadharCardNo = aadharCardNo;
	}

	public String getAadharFilePath() {
		return aadharFilePath;
	}

	public void setAadharFilePath(String aadharFilePath) {
		this.aadharFilePath = aadharFilePath;
	}

	public Date getAdminssionDate() {
		return adminssionDate;
	}

	public void setAdminssionDate(Date adminssionDate) {
		this.adminssionDate = adminssionDate;
	}

	public String getAdmissionNumber() {
		return admissionNumber;
	}

	public void setAdmissionNumber(String admissionNumber) {
		this.admissionNumber = admissionNumber;
	}

	public String getApplicationNo() {
		return applicationNo;
	}

	public void setApplicationNo(String applicationNo) {
		this.applicationNo = applicationNo;
	}

	public String getBiometricCode() {
		return biometricCode;
	}

	public void setBiometricCode(String biometricCode) {
		this.biometricCode = biometricCode;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Date getDateOfExpiry() {
		return dateOfExpiry;
	}

	public void setDateOfExpiry(Date dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}

	public Date getDateOfIssue() {
		return dateOfIssue;
	}

	public void setDateOfIssue(Date dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public Date getDateOfRegistration() {
		return dateOfRegistration;
	}

	public void setDateOfRegistration(Date dateOfRegistration) {
		this.dateOfRegistration = dateOfRegistration;
	}

	public String getEamcetRank() {
		return eamcetRank;
	}

	public void setEamcetRank(String eamcetRank) {
		this.eamcetRank = eamcetRank;
	}

	public String getEntranceHtNo() {
		return entranceHtNo;
	}

	public void setEntranceHtNo(String entranceHtNo) {
		this.entranceHtNo = entranceHtNo;
	}

	public String getFatherAddress() {
		return fatherAddress;
	}

	public void setFatherAddress(String fatherAddress) {
		this.fatherAddress = fatherAddress;
	}

	public String getFatherEmailId() {
		return fatherEmailId;
	}

	public void setFatherEmailId(String fatherEmailId) {
		this.fatherEmailId = fatherEmailId;
	}

	public String getFatherMobileNo() {
		return fatherMobileNo;
	}

	public void setFatherMobileNo(String fatherMobileNo) {
		this.fatherMobileNo = fatherMobileNo;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getFatherOccupation() {
		return fatherOccupation;
	}

	public void setFatherOccupation(String fatherOccupation) {
		this.fatherOccupation = fatherOccupation;
	}

	public String getFatherPhotoPath() {
		return fatherPhotoPath;
	}

	public void setFatherPhotoPath(String fatherPhotoPath) {
		this.fatherPhotoPath = fatherPhotoPath;
	}

	public String getFatherQualification() {
		return fatherQualification;
	}

	public void setFatherQualification(String fatherQualification) {
		this.fatherQualification = fatherQualification;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFolderPath() {
		return folderPath;
	}

	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}

	public String getGuardianAddress() {
		return guardianAddress;
	}

	public void setGuardianAddress(String guardianAddress) {
		this.guardianAddress = guardianAddress;
	}

	public String getGuardianEmailId() {
		return guardianEmailId;
	}

	public void setGuardianEmailId(String guardianEmailId) {
		this.guardianEmailId = guardianEmailId;
	}

	public String getGuardianIncomePa() {
		return guardianIncomePa;
	}

	public void setGuardianIncomePa(String guardianIncomePa) {
		this.guardianIncomePa = guardianIncomePa;
	}

	public String getGuardianMobileNo() {
		return guardianMobileNo;
	}

	public void setGuardianMobileNo(String guardianMobileNo) {
		this.guardianMobileNo = guardianMobileNo;
	}

	public String getGuardianName() {
		return guardianName;
	}

	public void setGuardianName(String guardianName) {
		this.guardianName = guardianName;
	}

	public String getHallticketNumber() {
		return hallticketNumber;
	}

	public void setHallticketNumber(String hallticketNumber) {
		this.hallticketNumber = hallticketNumber;
	}

	public String getHobbies() {
		return hobbies;
	}

	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}

	public String getIdentificationMarks() {
		return identificationMarks;
	}

	public void setIdentificationMarks(String identificationMarks) {
		this.identificationMarks = identificationMarks;
	}

	public String getInterests() {
		return interests;
	}

	public void setInterests(String interests) {
		this.interests = interests;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsCurrentYear() {
		return isCurrentYear;
	}

	public void setIsCurrentYear(Boolean isCurrentYear) {
		this.isCurrentYear = isCurrentYear;
	}

	public Boolean getIsLateral() {
		return isLateral;
	}

	public void setIsLateral(Boolean isLateral) {
		this.isLateral = isLateral;
	}

	public Boolean getIsLocal() {
		return isLocal;
	}

	public void setIsLocal(Boolean isLocal) {
		this.isLocal = isLocal;
	}

	public Boolean getIsScholorship() {
		return isScholorship;
	}

	public void setIsScholorship(Boolean isScholorship) {
		this.isScholorship = isScholorship;
	}

	public Boolean getIsgovtempFather() {
		return isgovtempFather;
	}

	public void setIsgovtempFather(Boolean isgovtempFather) {
		this.isgovtempFather = isgovtempFather;
	}

	public Boolean getIsgovtempMother() {
		return isgovtempMother;
	}

	public void setIsgovtempMother(Boolean isgovtempMother) {
		this.isgovtempMother = isgovtempMother;
	}

	public Boolean getIsgovtempSpouse() {
		return isgovtempSpouse;
	}

	public void setIsgovtempSpouse(Boolean isgovtempSpouse) {
		this.isgovtempSpouse = isgovtempSpouse;
	}

	public String getLangStatus1() {
		return langStatus1;
	}

	public void setLangStatus1(String langStatus1) {
		this.langStatus1 = langStatus1;
	}

	public String getLangStatus2() {
		return langStatus2;
	}

	public void setLangStatus2(String langStatus2) {
		this.langStatus2 = langStatus2;
	}

	public String getLangStatus3() {
		return langStatus3;
	}

	public void setLangStatus3(String langStatus3) {
		this.langStatus3 = langStatus3;
	}

	public String getLangStatus4() {
		return langStatus4;
	}

	public void setLangStatus4(String langStatus4) {
		this.langStatus4 = langStatus4;
	}

	public String getLangStatus5() {
		return langStatus5;
	}

	public void setLangStatus5(String langStatus5) {
		this.langStatus5 = langStatus5;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMotherAddress() {
		return motherAddress;
	}

	public void setMotherAddress(String motherAddress) {
		this.motherAddress = motherAddress;
	}

	public String getMotherEmailId() {
		return motherEmailId;
	}

	public void setMotherEmailId(String motherEmailId) {
		this.motherEmailId = motherEmailId;
	}

	public String getMotherIncomePa() {
		return motherIncomePa;
	}

	public void setMotherIncomePa(String motherIncomePa) {
		this.motherIncomePa = motherIncomePa;
	}

	public String getMotherMobileNo() {
		return motherMobileNo;
	}

	public void setMotherMobileNo(String motherMobileNo) {
		this.motherMobileNo = motherMobileNo;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getMotherOccupation() {
		return motherOccupation;
	}

	public void setMotherOccupation(String motherOccupation) {
		this.motherOccupation = motherOccupation;
	}

	public String getMotherPhotoPath() {
		return motherPhotoPath;
	}

	public void setMotherPhotoPath(String motherPhotoPath) {
		this.motherPhotoPath = motherPhotoPath;
	}

	public String getMotherQualification() {
		return motherQualification;
	}

	public void setMotherQualification(String motherQualification) {
		this.motherQualification = motherQualification;
	}

	public String getPancardFilePath() {
		return pancardFilePath;
	}

	public void setPancardFilePath(String pancardFilePath) {
		this.pancardFilePath = pancardFilePath;
	}

	public String getPancardNo() {
		return pancardNo;
	}

	public void setPancardNo(String pancardNo) {
		this.pancardNo = pancardNo;
	}

	public String getPassportNo() {
		return passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getPermanentPincode() {
		return permanentPincode;
	}

	public void setPermanentPincode(String permanentPincode) {
		this.permanentPincode = permanentPincode;
	}

	public String getPermanentStreet() {
		return permanentStreet;
	}

	public void setPermanentStreet(String permanentStreet) {
		this.permanentStreet = permanentStreet;
	}

	public String getPremanentMandal() {
		return premanentMandal;
	}

	public void setPremanentMandal(String premanentMandal) {
		this.premanentMandal = premanentMandal;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getPresentMandal() {
		return presentMandal;
	}

	public void setPresentMandal(String presentMandal) {
		this.presentMandal = presentMandal;
	}

	public String getPresentPincode() {
		return presentPincode;
	}

	public void setPresentPincode(String presentPincode) {
		this.presentPincode = presentPincode;
	}

	public String getPresentStreet() {
		return presentStreet;
	}

	public void setPresentStreet(String presentStreet) {
		this.presentStreet = presentStreet;
	}

	public String getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(String primaryContact) {
		this.primaryContact = primaryContact;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public String getRefApplicationNo() {
		return refApplicationNo;
	}

	public void setRefApplicationNo(String refApplicationNo) {
		this.refApplicationNo = refApplicationNo;
	}

	public String getResidencePhone() {
		return residencePhone;
	}

	public void setResidencePhone(String residencePhone) {
		this.residencePhone = residencePhone;
	}

	public String getRfid() {
		return rfid;
	}

	public void setRfid(String rfid) {
		this.rfid = rfid;
	}

	public String getRollNumber() {
		return rollNumber;
	}

	public void setRollNumber(String rollNumber) {
		this.rollNumber = rollNumber;
	}

	public String getSpouseAddress() {
		return spouseAddress;
	}

	public void setSpouseAddress(String spouseAddress) {
		this.spouseAddress = spouseAddress;
	}

	public String getSpouseEmailId() {
		return spouseEmailId;
	}

	public void setSpouseEmailId(String spouseEmailId) {
		this.spouseEmailId = spouseEmailId;
	}

	public String getSpouseIncomePa() {
		return spouseIncomePa;
	}

	public void setSpouseIncomePa(String spouseIncomePa) {
		this.spouseIncomePa = spouseIncomePa;
	}

	public String getSpouseMobileNo() {
		return spouseMobileNo;
	}

	public void setSpouseMobileNo(String spouseMobileNo) {
		this.spouseMobileNo = spouseMobileNo;
	}

	public String getSpouseName() {
		return spouseName;
	}

	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}

	public String getSpouseOccupation() {
		return spouseOccupation;
	}

	public void setSpouseOccupation(String spouseOccupation) {
		this.spouseOccupation = spouseOccupation;
	}

	public String getSpousePhotoPath() {
		return spousePhotoPath;
	}

	public void setSpousePhotoPath(String spousePhotoPath) {
		this.spousePhotoPath = spousePhotoPath;
	}

	public String getSpouseQualification() {
		return spouseQualification;
	}

	public void setSpouseQualification(String spouseQualification) {
		this.spouseQualification = spouseQualification;
	}

	public String getSscNo() {
		return sscNo;
	}

	public void setSscNo(String sscNo) {
		this.sscNo = sscNo;
	}

	public String getStdEmailId() {
		return stdEmailId;
	}

	public void setStdEmailId(String stdEmailId) {
		this.stdEmailId = stdEmailId;
	}

	public String getStudentPhotoPath() {
		return studentPhotoPath;
	}

	public void setStudentPhotoPath(String studentPhotoPath) {
		this.studentPhotoPath = studentPhotoPath;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Date getWeddingDate() {
		return weddingDate;
	}

	public void setWeddingDate(Date weddingDate) {
		this.weddingDate = weddingDate;
	}

	public Long getBatchId() {
		return batchId;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public Long getCasteId() {
		return casteId;
	}

	public void setCasteId(Long casteId) {
		this.casteId = casteId;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}


	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public Long getCourseYearId() {
		return courseYearId;
	}

	public void setCourseYearId(Long courseYearId) {
		this.courseYearId = courseYearId;
	}

	public String getCourseYearCode() {
		return courseYearCode;
	}

	public void setCourseYearCode(String courseYearCode) {
		this.courseYearCode = courseYearCode;
	}

	public String getCourseYearName() {
		return courseYearName;
	}

	public void setCourseYearName(String courseYearName) {
		this.courseYearName = courseYearName;
	}

	public Long getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Long districtId) {
		this.districtId = districtId;
	}

	public String getDistrictCode() {
		return districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public Long getDisabilityId() {
		return disabilityId;
	}

	public void setDisabilityId(Long disabilityId) {
		this.disabilityId = disabilityId;
	}

	public String getDisabilityDisplayName() {
		return disabilityDisplayName;
	}

	public void setDisabilityDisplayName(String disabilityDisplayName) {
		this.disabilityDisplayName = disabilityDisplayName;
	}

	public String getDisabilityCode() {
		return disabilityCode;
	}

	public void setDisabilityCode(String disabilityCode) {
		this.disabilityCode = disabilityCode;
	}

	public Long getNationalityId() {
		return nationalityId;
	}

	public void setNationalityId(Long nationalityId) {
		this.nationalityId = nationalityId;
	}

	public String getNationalityDisplayName() {
		return nationalityDisplayName;
	}

	public void setNationalityDisplayName(String nationalityDisplayName) {
		this.nationalityDisplayName = nationalityDisplayName;
	}

	public String getNationalityCode() {
		return nationalityCode;
	}

	public void setNationalityCode(String nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	public Long getReligionId() {
		return religionId;
	}

	public void setReligionId(Long religionId) {
		this.religionId = religionId;
	}

	public String getReligionDisplayName() {
		return religionDisplayName;
	}

	public void setReligionDisplayName(String religionDisplayName) {
		this.religionDisplayName = religionDisplayName;
	}

	public String getReligionCode() {
		return religionCode;
	}

	public void setReligionCode(String religionCode) {
		this.religionCode = religionCode;
	}

	public Long getBloodgroupId() {
		return bloodgroupId;
	}

	public void setBloodgroupId(Long bloodgroupId) {
		this.bloodgroupId = bloodgroupId;
	}

	public String getBloodgroupDisplayName() {
		return bloodgroupDisplayName;
	}

	public void setBloodgroupDisplayName(String bloodgroupDisplayName) {
		this.bloodgroupDisplayName = bloodgroupDisplayName;
	}

	public String getBloodgroupCode() {
		return bloodgroupCode;
	}

	public void setBloodgroupCode(String bloodgroupCode) {
		this.bloodgroupCode = bloodgroupCode;
	}

	public Long getMaritalstatusId() {
		return maritalstatusId;
	}

	public void setMaritalstatusId(Long maritalstatusId) {
		this.maritalstatusId = maritalstatusId;
	}

	public String getMaritalstatusDisplayName() {
		return maritalstatusDisplayName;
	}

	public void setMaritalstatusDisplayName(String maritalstatusDisplayName) {
		this.maritalstatusDisplayName = maritalstatusDisplayName;
	}

	public String getMaritalstatusCode() {
		return maritalstatusCode;
	}

	public void setMaritalstatusCode(String maritalstatusCode) {
		this.maritalstatusCode = maritalstatusCode;
	}

	public Long getLanguage1Id() {
		return language1Id;
	}

	public void setLanguage1Id(Long language1Id) {
		this.language1Id = language1Id;
	}

	public String getLanguage1DisplayName() {
		return language1DisplayName;
	}

	public void setLanguage1DisplayName(String language1DisplayName) {
		this.language1DisplayName = language1DisplayName;
	}

	public String getLanguage1Code() {
		return language1Code;
	}

	public void setLanguage1Code(String language1Code) {
		this.language1Code = language1Code;
	}

	public Long getLanguage2Id() {
		return language2Id;
	}

	public void setLanguage2Id(Long language2Id) {
		this.language2Id = language2Id;
	}

	public String getLanguage2DisplayName() {
		return language2DisplayName;
	}

	public void setLanguage2DisplayName(String language2DisplayName) {
		this.language2DisplayName = language2DisplayName;
	}

	public String getLanguage2Code() {
		return language2Code;
	}

	public void setLanguage2Code(String language2Code) {
		this.language2Code = language2Code;
	}

	public Long getLanguage3Id() {
		return language3Id;
	}

	public void setLanguage3Id(Long language3Id) {
		this.language3Id = language3Id;
	}

	public String getLanguage3DisplayName() {
		return language3DisplayName;
	}

	public void setLanguage3DisplayName(String language3DisplayName) {
		this.language3DisplayName = language3DisplayName;
	}

	public String getLanguage3Code() {
		return language3Code;
	}

	public void setLanguage3Code(String language3Code) {
		this.language3Code = language3Code;
	}

	public Long getLanguage4Id() {
		return language4Id;
	}

	public void setLanguage4Id(Long language4Id) {
		this.language4Id = language4Id;
	}

	public String getLanguage4DisplayName() {
		return language4DisplayName;
	}

	public void setLanguage4DisplayName(String language4DisplayName) {
		this.language4DisplayName = language4DisplayName;
	}

	public String getLanguage4Code() {
		return language4Code;
	}

	public void setLanguage4Code(String language4Code) {
		this.language4Code = language4Code;
	}

	public Long getLanguage5Id() {
		return language5Id;
	}

	public void setLanguage5Id(Long language5Id) {
		this.language5Id = language5Id;
	}

	public String getLanguage5DisplayName() {
		return language5DisplayName;
	}

	public void setLanguage5DisplayName(String language5DisplayName) {
		this.language5DisplayName = language5DisplayName;
	}

	public String getLanguage5Code() {
		return language5Code;
	}

	public void setLanguage5Code(String language5Code) {
		this.language5Code = language5Code;
	}

	public Long getQuotaId() {
		return quotaId;
	}

	public void setQuotaId(Long quotaId) {
		this.quotaId = quotaId;
	}

	public String getQuotaDisplayName() {
		return quotaDisplayName;
	}

	public void setQuotaDisplayName(String quotaDisplayName) {
		this.quotaDisplayName = quotaDisplayName;
	}

	public String getQuotaCode() {
		return quotaCode;
	}

	public void setQuotaCode(String quotaCode) {
		this.quotaCode = quotaCode;
	}

	public Long getStudentTypeId() {
		return studentTypeId;
	}

	public void setStudentTypeId(Long studentTypeId) {
		this.studentTypeId = studentTypeId;
	}

	public String getStudentTypeDisplayName() {
		return studentTypeDisplayName;
	}

	public void setStudentTypeDisplayName(String studentTypeDisplayName) {
		this.studentTypeDisplayName = studentTypeDisplayName;
	}

	public String getStudentTypeCode() {
		return studentTypeCode;
	}

	public void setStudentTypeCode(String studentTypeCode) {
		this.studentTypeCode = studentTypeCode;
	}

	public Long getGenderId() {
		return genderId;
	}

	public void setGenderId(Long genderId) {
		this.genderId = genderId;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getGenderCode() {
		return genderCode;
	}

	public void setGenderCode(String genderCode) {
		this.genderCode = genderCode;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}


	public Long getSubCasteId() {
		return subCasteId;
	}

	public void setSubCasteId(Long subCasteId) {
		this.subCasteId = subCasteId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getUserTypeId() {
		return userTypeId;
	}

	public void setUserTypeId(Long userTypeId) {
		this.userTypeId = userTypeId;
	}

	public String getResetPasswordCode() {
		return resetPasswordCode;
	}

	public void setResetPasswordCode(String resetPasswordCode) {
		this.resetPasswordCode = resetPasswordCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getStudentAppId() {
		return studentAppId;
	}

	public void setStudentAppId(Long studentAppId) {
		this.studentAppId = studentAppId;
	}

	public String getPresentStreetName() {
		return presentStreetName;
	}

	public void setPresentStreetName(String presentStreetName) {
		this.presentStreetName = presentStreetName;
	}

	public String getStudentEmailId() {
		return studentEmailId;
	}

	public void setStudentEmailId(String studentEmailId) {
		this.studentEmailId = studentEmailId;
	}

	public String getSubCasteName() {
		return subCasteName;
	}

	public void setSubCasteName(String subCasteName) {
		this.subCasteName = subCasteName;
	}

	public String getCasteName() {
		return casteName;
	}

	public void setCasteName(String casteName) {
		this.casteName = casteName;
	}

	public Long getPermanentStateId() {
		return permanentStateId;
	}

	public void setPermanentStateId(Long permanentStateId) {
		this.permanentStateId = permanentStateId;
	}

	public String getPermanentStateName() {
		return permanentStateName;
	}

	public void setPermanentStateName(String permanentStateName) {
		this.permanentStateName = permanentStateName;
	}

	public Long getPermanentCountryId() {
		return permanentCountryId;
	}

	public void setPermanentCountryId(Long permanentCountryId) {
		this.permanentCountryId = permanentCountryId;
	}

	public String getPermanentCountryName() {
		return permanentCountryName;
	}

	public void setPermanentCountryName(String permanentCountryName) {
		this.permanentCountryName = permanentCountryName;
	}

	public Long getPresentCountryId() {
		return presentCountryId;
	}

	public void setPresentCountryId(Long presentCountryId) {
		this.presentCountryId = presentCountryId;
	}

	public String getPresentCountryName() {
		return presentCountryName;
	}

	public void setPresentCountryName(String presentCountryName) {
		this.presentCountryName = presentCountryName;
	}

	public Long getPresentStateId() {
		return presentStateId;
	}

	public void setPresentStateId(Long presentStateId) {
		this.presentStateId = presentStateId;
	}

	public String getPresentStateName() {
		return presentStateName;
	}

	public void setPresentStateName(String presentStateName) {
		this.presentStateName = presentStateName;
	}

	/*public List<FeeStudentDataDTO> getFeeStudentDataDTOs() {
		return feeStudentDataDTOs;
	}

	public void setFeeStudentDataDTOs(List<FeeStudentDataDTO> feeStudentDataDTOs) {
		this.feeStudentDataDTOs = feeStudentDataDTOs;
	}*/

	public Long getGroupSectionId() {
		return groupSectionId;
	}

	public void setGroupSectionId(Long groupSectionId) {
		this.groupSectionId = groupSectionId;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}

	public List<StudentAppDocumentCollectionDTO> getStdDocCollections() {
		return stdDocCollections;
	}

	public void setStdDocCollections(List<StudentAppDocumentCollectionDTO> stdDocCollections) {
		this.stdDocCollections = stdDocCollections;
	}

	public String getFathersIncomePa() {
		return fathersIncomePa;
	}

	public void setFathersIncomePa(String fathersIncomePa) {
		this.fathersIncomePa = fathersIncomePa;
	}

	public Long getTitleId() {
		return titleId;
	}

	public void setTitleId(Long titleId) {
		this.titleId = titleId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEntranceHTNumber() {
		return entranceHTNumber;
	}

	public void setEntranceHTNumber(String entranceHTNumber) {
		this.entranceHTNumber = entranceHTNumber;
	}

	public String getPermanentMandal() {
		return permanentMandal;
	}

	public void setPermanentMandal(String permanentMandal) {
		this.permanentMandal = permanentMandal;
	}

	public Long getPresentDistrictId() {
		return presentDistrictId;
	}

	public void setPresentDistrictId(Long presentDistrictId) {
		this.presentDistrictId = presentDistrictId;
	}

	public String getPresentDistrictName() {
		return presentDistrictName;
	}

	public void setPresentDistrictName(String presentDistrictName) {
		this.presentDistrictName = presentDistrictName;
	}

	public Long getPresentCityId() {
		return presentCityId;
	}

	public void setPresentCityId(Long presentCityId) {
		this.presentCityId = presentCityId;
	}

	public String getPresentCityName() {
		return presentCityName;
	}

	public void setPresentCityName(String presentCityName) {
		this.presentCityName = presentCityName;
	}

	public Long getPermanentDistrictId() {
		return permanentDistrictId;
	}

	public void setPermanentDistrictId(Long permanentDistrictId) {
		this.permanentDistrictId = permanentDistrictId;
	}

	public String getPermanentDistrictName() {
		return permanentDistrictName;
	}

	public void setPermanentDistrictName(String permanentDistrictName) {
		this.permanentDistrictName = permanentDistrictName;
	}

	public Long getPermanentCityId() {
		return permanentCityId;
	}

	public void setPermanentCityId(Long permanentCityId) {
		this.permanentCityId = permanentCityId;
	}

	public String getPermanentCityName() {
		return permanentCityName;
	}

	public void setPermanentCityName(String permanentCityName) {
		this.permanentCityName = permanentCityName;
	}

	public List<StudentAppActivityDTO> getStdActivities() {
		return stdActivities;
	}

	public void setStdActivities(List<StudentAppActivityDTO> stdActivities) {
		this.stdActivities = stdActivities;
	}

	public List<StudentAppEducationDTO> getStdEducations() {
		return stdEducations;
	}

	public void setStdEducations(List<StudentAppEducationDTO> stdEducations) {
		this.stdEducations = stdEducations;
	}

	public List<StudentDocumentCollectionDTO> getStudentDocumentCollections() {
		return studentDocumentCollections;
	}

	public List<StudentActivitiesDetailDTO> getStudentActivitiesDetails() {
		return studentActivitiesDetails;
	}

	public List<StudentEducationDetailDTO> getStudentEducationDetails() {
		return studentEducationDetails;
	}

	public void setStudentDocumentCollections(List<StudentDocumentCollectionDTO> studentDocumentCollections) {
		this.studentDocumentCollections = studentDocumentCollections;
	}

	public void setStudentActivitiesDetails(List<StudentActivitiesDetailDTO> studentActivitiesDetails) {
		this.studentActivitiesDetails = studentActivitiesDetails;
	}

	public void setStudentEducationDetails(List<StudentEducationDetailDTO> studentEducationDetails) {
		this.studentEducationDetails = studentEducationDetails;
	}

	public Long getStudentStatusId() {
		return studentStatusId;
	}

	public void setStudentStatusId(Long studentStatusId) {
		this.studentStatusId = studentStatusId;
	}

	public String getStudentStatusCode() {
		return studentStatusCode;
	}

	public void setStudentStatusCode(String studentStatusCode) {
		this.studentStatusCode = studentStatusCode;
	}

	public String getStudentStatusDisplayName() {
		return studentStatusDisplayName;
	}

	public void setStudentStatusDisplayName(String studentStatusDisplayName) {
		this.studentStatusDisplayName = studentStatusDisplayName;
	}

	public Long getQualifyingId() {
		return qualifyingId;
	}

	public void setQualifyingId(Long qualifyingId) {
		this.qualifyingId = qualifyingId;
	}

	public String getQualifyingName() {
		return qualifyingName;
	}

	public void setQualifyingName(String qualifyingName) {
		this.qualifyingName = qualifyingName;
	}

	public String getQualifyingCode() {
		return qualifyingCode;
	}

	public void setQualifyingCode(String qualifyingCode) {
		this.qualifyingCode = qualifyingCode;
	}

	public Boolean getIsMinority() {
		return isMinority;
	}

	public void setIsMinority(Boolean isMinority) {
		this.isMinority = isMinority;
	}

	public Long getParentUserId() {
		return parentUserId;
	}

	public void setParentUserId(Long parentUserId) {
		this.parentUserId = parentUserId;
	}

	public String getParentFirstName() {
		return parentFirstName;
	}

	public void setParentFirstName(String parentFirstName) {
		this.parentFirstName = parentFirstName;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
}