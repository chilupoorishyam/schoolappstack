package com.gts.cms.common.repository.custom;

import com.gts.cms.entity.SmsTemplate;

/**
 * @author Genesis
 *
 */
public interface SmsTemplateRepositoryCustom {

	public SmsTemplate getSmsTemplateByCode(String templateCode);

}
