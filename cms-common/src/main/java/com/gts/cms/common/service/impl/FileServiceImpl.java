package com.gts.cms.common.service.impl;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.service.FileService;
import com.gts.cms.common.util.FileUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Service
public class FileServiceImpl implements FileService {

    private static final Logger LOGGER = Logger.getLogger(FileServiceImpl.class);

    @Value("${s3.url}")
    private String url;

    @Value("${s3.bucket-name}")
    private String bucketName;

    @Autowired
    AmazonS3 s3Client;

    private File convertMultiPartToFile(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    @Override
    public String uploadFile(MultipartFile multipartFile, String fileName) {
        //String fileSeparator = System.getProperty("file.separator");
        String fileSeparator = FileUtil.getFileSeparator();
        StringBuilder fileUrl = new StringBuilder();
        fileName = fileName.replaceAll("\\s+", "_");
        File file;
        try {
            file = convertMultiPartToFile(multipartFile);
            fileUrl = fileUrl.append(url).append(fileSeparator).append(bucketName).append(fileSeparator).append(fileName);
            LOGGER.info("File Url:" + fileUrl);
            uploadFileTos3bucket(fileName, file);
            file.delete();
        } catch (AmazonClientException | IOException e) {
            e.printStackTrace();
            LOGGER.error("Error occured while uploading a file: " + e.getMessage(), e);
            throw new ApiException("File upload Failed.  Please try again or contact system admin.");
        }
        return fileName;
    }

    @Override
    public String uploadFile(MultipartFile multipartFile, String fileName, Long pkId, String type) {
        //String fileSeparator = System.getProperty("file.separator");
        String fileSeparator = FileUtil.getFileSeparator();
        StringBuilder fileUrl = new StringBuilder();
        fileName = fileName.replaceAll("\\s+", "_");
        File file;
        try {
            file = convertMultiPartToFile(multipartFile);
            fileUrl = fileUrl.append(type).append(fileSeparator)
                    .append(pkId).append(fileSeparator).append(fileName);
            StringBuilder bucket = new StringBuilder(bucketName);
            bucket.append(fileSeparator)
                    .append(type).append(fileSeparator)
                    .append(pkId);
            LOGGER.info("File Url:" + fileUrl);
            s3Client.putObject(
                    new PutObjectRequest(bucket.toString(), fileName, file).withCannedAcl(CannedAccessControlList.PublicRead));
            file.delete();
        } catch (AmazonClientException | IOException e) {
            e.printStackTrace();
            LOGGER.error("Error occured while uploading a file: " + e.getMessage(), e);
            throw new ApiException("File upload Failed.  Please try again or contact system admin.");
        }
        return fileUrl.toString();
    }

    private void uploadFileTos3bucket(String fileName, File file) {
        s3Client.putObject(
                new PutObjectRequest(bucketName, fileName, file).withCannedAcl(CannedAccessControlList.PublicRead));
    }


}
