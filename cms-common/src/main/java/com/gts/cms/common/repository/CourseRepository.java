package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.CourseRepositoryCustom;
import com.gts.cms.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * created by Naveen(Auto) on 02/09/2018
 * 
 */
@Repository
public interface CourseRepository extends JpaRepository<Course, Long>, CourseRepositoryCustom {

    @Query("SELECT c FROM  Course c"
            + " WHERE 1=1 "
            + " AND c.school.schoolId = :schoolId "
            + " AND (:courseCode is null or upper(c.courseCode)=upper(:courseCode)) "
            + " AND c.isActive = true"
    )
    List<Course> findCourseByName(@Param("schoolId") Long schoolId,@Param("courseCode") String courseCode);
}

