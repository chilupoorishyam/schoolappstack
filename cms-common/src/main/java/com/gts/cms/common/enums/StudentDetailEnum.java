package com.gts.cms.common.enums;

public enum StudentDetailEnum {
	DTND("DTND"), 
	DTNDRECOMMENDED("DETAIN RECOMMENDED"), 
	INCOLLEGE("INCOLLEGE"),
	DISCONTINUED("DISCONTINUED");
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	private StudentDetailEnum(String value) {
		this.value = value;
	}

}
