package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.NetworkMemberDTO;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.AmsProfileDetail;
import com.gts.cms.entity.NetworkMember;
import com.gts.cms.entity.Organization;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class NetworkMemberMapper implements BaseMapper<NetworkMember, NetworkMemberDTO> {
	@Override
	public NetworkMemberDTO convertEntityToDTO(NetworkMember entityObject) {
		NetworkMemberDTO dtoObject = null;
		if (entityObject != null) {
			dtoObject = new NetworkMemberDTO();
			dtoObject.setUpdatedDt(entityObject.getUpdatedDt());
			dtoObject.setCreatedUser(entityObject.getCreatedUser());
			dtoObject.setCreatedDt(entityObject.getCreatedDt());
			dtoObject.setUpdatedUser(entityObject.getUpdatedUser());
			dtoObject.setIsActive(entityObject.getIsActive());
			dtoObject.setNetworkMemberId(entityObject.getNetworkMemberId());
			dtoObject.setMemberLeftDate(entityObject.getMemberLeftDate());
			dtoObject.setMemberJoinDate(entityObject.getMemberJoinDate());
			dtoObject.setReason(entityObject.getReason());
			if(entityObject.getProfileDetail()!=null) {
				dtoObject.setAmsProfileId(entityObject.getProfileDetail().getAmsProfileId());
				dtoObject.setAmsFirstName(entityObject.getProfileDetail().getAmsFirstName());
			}
			if(entityObject.getOrganization()!=null) {
				dtoObject.setOrganizationId(entityObject.getOrganization().getOrganizationId());
				dtoObject.setOrgCode(entityObject.getOrganization().getOrgCode());
				dtoObject.setOrgName(entityObject.getOrganization().getOrgName());
			}
		}
		return dtoObject;
	}

	@Override
	public NetworkMember convertDTOtoEntity(NetworkMemberDTO dtoObject, NetworkMember entityObject) {
		if (dtoObject != null) {
			if (entityObject == null) {
				entityObject = new NetworkMember();
				entityObject.setIsActive(Boolean.TRUE);
				entityObject.setCreatedDt(new Date());
				entityObject.setCreatedUser(SecurityUtil.getCurrentUser());
			}
			if (dtoObject.getNetworkMemberId() != null) {
				entityObject.setNetworkMemberId(dtoObject.getNetworkMemberId());
				entityObject.setCreatedDt(dtoObject.getCreatedDt());
				if (dtoObject.getCreatedUser() != null) {
					entityObject.setCreatedUser(dtoObject.getCreatedUser());
				} else {
					entityObject.setCreatedUser(SecurityUtil.getCurrentUser());
				}
				entityObject.setUpdatedDt(new Date());
				entityObject.setIsActive(dtoObject.getIsActive());
				entityObject.setUpdatedUser(SecurityUtil.getCurrentUser());
			}

			if (dtoObject.getAmsProfileId() != null) {
				AmsProfileDetail amsProfileDetail = new AmsProfileDetail();
				amsProfileDetail.setAmsProfileId(dtoObject.getAmsProfileId());
				entityObject.setProfileDetail(amsProfileDetail);
			}
			entityObject.setMemberLeftDate(dtoObject.getMemberLeftDate());
			entityObject.setMemberJoinDate(dtoObject.getMemberJoinDate());
			if (dtoObject.getOrganizationId() != null) {
				Organization organization = new Organization();
				organization.setOrganizationId(dtoObject.getOrganizationId());
				entityObject.setOrganization(organization);
			}
			entityObject.setReason(dtoObject.getReason());
		}
		return entityObject;
	}

	@Override
	public List<NetworkMember> convertDTOListToEntityList(List<NetworkMemberDTO> networkMemberDTOList) {
		List<NetworkMember> networkMemberList = new ArrayList<>();
		networkMemberDTOList.forEach(dtoObject -> networkMemberList.add(convertDTOtoEntity(dtoObject, null)));
		return networkMemberList;
	}

	@Override
	public List<NetworkMemberDTO> convertEntityListToDTOList(List<NetworkMember> networkMemberList) {
		List<NetworkMemberDTO> networkMemberDTOList = new ArrayList<>();
		networkMemberList.forEach(entityObject -> networkMemberDTOList.add(convertEntityToDTO(entityObject)));
		return networkMemberDTOList;
	}
}
