package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.SmsTemplateRepositoryCustom;
import com.gts.cms.entity.SmsTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SmsTemplateRepository extends JpaRepository<SmsTemplate, Long>, SmsTemplateRepositoryCustom {

	SmsTemplate findByTemplateCode(String string);
	

}
