package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.SubjectBookDTO;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.Book;
import com.gts.cms.entity.School;
import com.gts.cms.entity.SubjectBook;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class SubjectBookMapper implements BaseMapper<SubjectBook, SubjectBookDTO> {
	@Override
	public SubjectBookDTO convertEntityToDTO(SubjectBook subjectBook) {
		SubjectBookDTO subjectBookDTO = null;
		if (subjectBook != null) {
			subjectBookDTO = new SubjectBookDTO();
			subjectBookDTO.setCreatedUser(subjectBook.getCreatedUser());
			subjectBookDTO.setIsActive(subjectBook.getIsActive());
			subjectBookDTO.setUpdatedUser(subjectBook.getUpdatedUser());
			subjectBookDTO.setCreatedDt(subjectBook.getCreatedDt());
			subjectBookDTO.setUpdatedDt(subjectBook.getUpdatedDt());
			subjectBookDTO.setIsOnlinecourse(subjectBook.getIsOnlinecourse());
			subjectBookDTO.setIsReference(subjectBook.getIsReference());
			subjectBookDTO.setReason(subjectBook.getReason());
			subjectBookDTO.setSubBookId(subjectBook.getSubBookId());
			subjectBookDTO.setIsTextbook(subjectBook.getIsTextbook());
			/*if (subjectBook.getSubjectregulation() != null) {
				subjectBookDTO.setSubjectRegulationId(subjectBook.getSubjectregulation().getSubjectRegulationId());
			}*/
			if (subjectBook.getBook() != null) {
				subjectBookDTO.setBookId(subjectBook.getBook().getBookId());
				subjectBookDTO.setBooknumber(subjectBook.getBook().getBooknumber());
				subjectBookDTO.setTitle(subjectBook.getBook().getTitle());
				subjectBookDTO.setIsbn(subjectBook.getBook().getIsbn());
				subjectBookDTO.setEdition(subjectBook.getBook().getEdition());
			}
			if (subjectBook.getSchool() != null) {
				subjectBookDTO.setSchoolId(subjectBook.getSchool().getSchoolId());
				subjectBookDTO.setSchoolName(subjectBook.getSchool().getSchoolName());
			}
		}
		return subjectBookDTO;
	}

	@Override
	public SubjectBook convertDTOtoEntity(SubjectBookDTO subjectBookDTO, SubjectBook subjectBook) {
		if (null == subjectBook) {
			subjectBook = new SubjectBook();
			subjectBook.setIsActive(Status.ACTIVE.getId());
			subjectBook.setCreatedDt(new Date());
			subjectBook.setCreatedUser(SecurityUtil.getCurrentUser());
		}
		subjectBook.setIsActive(subjectBookDTO.getIsActive());

		if(subjectBook.getIsActive()==null) {
			subjectBook.setIsActive(Boolean.TRUE);
		}
		subjectBook.setUpdatedUser(SecurityUtil.getCurrentUser());
		subjectBook.setUpdatedDt(new Date());

		/*if (subjectBookDTO.getSubjectRegulationId() != null) {
			Subjectregulation subjectregulation = new Subjectregulation();
			subjectregulation.setSubjectRegulationId(subjectBookDTO.getSubjectRegulationId());
			subjectBook.setSubjectregulation(subjectregulation);
		}*/
		subjectBook.setIsOnlinecourse(subjectBookDTO.getIsOnlinecourse());
		subjectBook.setIsReference(subjectBookDTO.getIsReference());
		subjectBook.setReason(subjectBookDTO.getReason());
		if (subjectBookDTO.getBookId() != null) {
			Book book = new Book();
			book.setBookId(subjectBookDTO.getBookId());
			subjectBook.setBook(book);
		}
		subjectBook.setSubBookId(subjectBookDTO.getSubBookId());
		subjectBook.setIsTextbook(subjectBookDTO.getIsTextbook());
		if (subjectBookDTO.getSchoolId() != null) {
			School school = new School();
			school.setSchoolId(subjectBookDTO.getSchoolId());
			subjectBook.setSchool(school);
		}
		return subjectBook;
	}

	@Override
	public List<SubjectBook> convertDTOListToEntityList(List<SubjectBookDTO> subjectBookDTOList) {
		List<SubjectBook> subjectBookList = new ArrayList<>();
		subjectBookDTOList.forEach(subjectBookDTO -> subjectBookList.add(convertDTOtoEntity(subjectBookDTO, null)));
		return subjectBookList;
	}

	@Override
	public List<SubjectBookDTO> convertEntityListToDTOList(List<SubjectBook> subjectBookList) {
		List<SubjectBookDTO> subjectBookDTOList = new ArrayList<>();
		subjectBookList.forEach(subjectBook -> subjectBookDTOList.add(convertEntityToDTO(subjectBook)));
		return subjectBookDTOList;
	}
}