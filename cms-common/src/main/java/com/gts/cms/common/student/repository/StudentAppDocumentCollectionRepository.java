package com.gts.cms.common.student.repository;

import com.gts.cms.common.student.repository.custom.StudentAppDocumentCollectionRepositoryCustom;
import com.gts.cms.entity.StudentAppDocCollection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Genesis
 *
 */
@Repository
public interface StudentAppDocumentCollectionRepository
		extends JpaRepository<StudentAppDocCollection, Long>, StudentAppDocumentCollectionRepositoryCustom {

}
