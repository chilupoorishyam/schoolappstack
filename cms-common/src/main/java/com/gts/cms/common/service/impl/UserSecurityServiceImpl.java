package com.gts.cms.common.service.impl;

import com.gts.cms.common.dto.PasswordDTO;
import com.gts.cms.common.repository.PasswordResetTokenRepository;
import com.gts.cms.common.repository.UserRepository;
import com.gts.cms.common.service.UserSecurityService;
import com.gts.cms.entity.PasswordResetToken;
import com.gts.cms.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.List;

@Service
@Transactional
public class UserSecurityServiceImpl implements UserSecurityService {

    @Autowired
    private PasswordResetTokenRepository passwordTokenRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    public String validatePasswordResetToken(PasswordDTO passwordDto) {
        User user = userRepository.findByUserNameOrEmail(passwordDto.getUserName());
        final List<PasswordResetToken> passTokens = passwordTokenRepository.findTokenByUser(user.getUserId());
        if (!CollectionUtils.isEmpty(passTokens)) {
            return !isTokenFound(passTokens.get(0),passwordDto) ? "invalidToken"
                    : isTokenExpired(passTokens.get(0)) ? "expired"
                    : null;
        } else {
            return null;
        }
    }

    private boolean isTokenFound(PasswordResetToken passToken,PasswordDTO passwordDto) {
        boolean ifTokenValid = false;
        if (passToken != null) {
            if (passwordDto.getToken() != null && passToken.getToken() != null && passToken.getToken().equals(passwordDto.getToken())) {
                ifTokenValid = true;
            }
        }
        return ifTokenValid;
    }

    private boolean isTokenExpired(PasswordResetToken passToken) {
        final Calendar cal = Calendar.getInstance();
        return passToken.getExpiryDate().before(cal.getTime());
    }
}