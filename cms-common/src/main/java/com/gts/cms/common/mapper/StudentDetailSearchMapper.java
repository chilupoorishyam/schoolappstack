package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.StudentDetailSearchDTO;
import com.gts.cms.common.util.FileUtil;
import com.gts.cms.entity.StudentDetail;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StudentDetailSearchMapper implements BaseMapper<StudentDetail, StudentDetailSearchDTO> {

	/*@Autowired
	StudentAttendanceMapper studentAttendanceMapper;*/

	@Override
	public StudentDetailSearchDTO convertEntityToDTO(StudentDetail entity) {
		StudentDetailSearchDTO studentDetailSearchDto = null;
		if (entity != null) {
			studentDetailSearchDto = new StudentDetailSearchDTO();
			studentDetailSearchDto.setStudentId(entity.getStudentId());
			studentDetailSearchDto.setAdmissionNumber(entity.getAdmissionNumber());
			studentDetailSearchDto.setDateOfBirth(entity.getDateOfBirth());
			studentDetailSearchDto.setStdEmailId(entity.getStdEmailId());
			studentDetailSearchDto.setFatherName(entity.getFatherName());
			studentDetailSearchDto.setFirstName(entity.getFirstName());
			studentDetailSearchDto.setGuardianName(entity.getGuardianName());
			studentDetailSearchDto.setHallticketNumber(entity.getHallticketNumber());
			studentDetailSearchDto.setIsActive(entity.getIsActive());
			studentDetailSearchDto.setLastName(entity.getLastName());
			studentDetailSearchDto.setMiddleName(entity.getMiddleName());
			studentDetailSearchDto.setMobile(entity.getMobile());
			studentDetailSearchDto.setMotherName(entity.getMotherName());
			studentDetailSearchDto.setRollNumber(entity.getRollNumber());
			studentDetailSearchDto.setStudentPhotoPath(FileUtil.getAbsolutePath(entity.getStudentPhotoPath()));
			studentDetailSearchDto.setIsLateral(entity.getIsLateral());
			if(entity.getGroupSection() != null) {
			studentDetailSearchDto.setGroupSectionId(entity.getGroupSection().getGroupSectionId());
			studentDetailSearchDto.setSection(entity.getGroupSection().getSection());
			}

			if (entity.getBatch() != null) {
				studentDetailSearchDto.setBatchId(entity.getBatch().getBatchId());
			}
			
			if (entity.getAcademicYear() != null) {
				studentDetailSearchDto.setAcademicYearId(entity.getAcademicYear().getAcademicYearId());
				studentDetailSearchDto.setAcademicYear(entity.getAcademicYear().getAcademicYear());
			}
			if (entity.getQuota() != null) {
				studentDetailSearchDto.setQuotaId(entity.getQuota().getGeneralDetailId());
				studentDetailSearchDto.setQuotaDisplayName(entity.getQuota().getGeneralDetailDisplayName());
			}
			if (entity.getSchool() != null) {
				studentDetailSearchDto.setSchoolId(entity.getSchool().getSchoolId());
				studentDetailSearchDto.setSchoolCode(entity.getSchool().getSchoolCode());
				studentDetailSearchDto.setSchoolName(entity.getSchool().getSchoolName());
			}
			if (entity.getCourse() != null) {
				studentDetailSearchDto.setCourseId(entity.getCourse().getCourseId());
				studentDetailSearchDto.setCourseCode(entity.getCourse().getCourseCode());
			}

			if (entity.getCourseYear() != null) {
				studentDetailSearchDto.setCourseYearId(entity.getCourseYear().getCourseYearId());
				studentDetailSearchDto.setCourseYearCode(entity.getCourseYear().getCourseYearCode());
				studentDetailSearchDto.setCourseYearName(entity.getCourseYear().getCourseYearName());
			}
			if (entity.getUser() != null) {
				studentDetailSearchDto.setUserId(entity.getUser().getUserId());
			}
			if (entity.getStudentStatus() != null) {
				studentDetailSearchDto.setStudentStatusId(entity.getStudentStatus().getGeneralDetailId());
				studentDetailSearchDto.setStudentStatusDisplayName(entity.getStudentStatus().getGeneralDetailDisplayName());
				studentDetailSearchDto.setStudentStatusCode(entity.getStudentStatus().getGeneralDetailCode());
			}
			if(entity.getGender()!=null) {
				studentDetailSearchDto.setGenderId(entity.getGender().getGeneralDetailId());
				studentDetailSearchDto.setGenderDsiplayName(entity.getGender().getGeneralDetailDisplayName());
				studentDetailSearchDto.setGenderCode(entity.getGender().getGeneralDetailCode());
			}
		}
		return studentDetailSearchDto;
	}

	@Override
	public List<StudentDetailSearchDTO> convertEntityListToDTOList(List<StudentDetail> entityList) {
		List<StudentDetailSearchDTO> studentDetailDTOList = new ArrayList<>();
		entityList.forEach(studentDetail -> studentDetailDTOList.add(convertEntityToDTO(studentDetail)));
		return studentDetailDTOList;
	}

	@Override
	public List<StudentDetail> convertDTOListToEntityList(List<StudentDetailSearchDTO> dtoList) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StudentDetail convertDTOtoEntity(StudentDetailSearchDTO d, StudentDetail e) {
		// TODO Auto-generated method stub
		return null;
	}
}
