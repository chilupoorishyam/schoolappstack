package com.gts.cms.common.student.mapper;

import com.gts.cms.common.dto.StudentSubjectDTO;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class StudentSubjectMapper implements BaseMapper<StudentSubject, StudentSubjectDTO> {
	
	@Override
	public StudentSubject convertDTOtoEntity(StudentSubjectDTO dtoObject,StudentSubject entityObject) {
		if (null == entityObject) {
			entityObject = new StudentSubject();
			entityObject.setIsActive(Boolean.TRUE);
			entityObject.setCreatedDt(new Date());
			entityObject.setCreatedUser(SecurityUtil.getCurrentUser());
		} else {
			entityObject.setIsActive(dtoObject.getIsActive());
		}
		
		entityObject.setStudentSubjectId(dtoObject.getStudentSubjectId());
	
		if(dtoObject.getSchoolId() != null) {
		  School school = new School();
		  school.setSchoolId(dtoObject.getSchoolId());
		  school.setSchoolName(dtoObject.getSchoolName());
		  
		  entityObject.setSchool(school);
		}
		
		if(dtoObject.getCourseYearId() != null) {
		  CourseYear courseYear = new CourseYear();
		  courseYear.setCourseYearId(dtoObject.getCourseYearId());
		  courseYear.setCourseYearName(dtoObject.getCourseYearName());
		  
		  entityObject.setCourseYear(courseYear);
		}
		

		
		if(dtoObject.getAcademicYearId() != null) {
		  AcademicYear acadYear = new AcademicYear();
		  acadYear.setAcademicYearId(dtoObject.getAcademicYearId());
		  acadYear.setAcademicYear(dtoObject.getAcademicYear());
		  
		  entityObject.setAcademicYear(acadYear);
		}
		
		if(dtoObject.getSubjectId()!=null) {
		  Subject subject = new Subject();
		  subject.setSubjectId(dtoObject.getSubjectId());
		  subject.setSubjectName(dtoObject.getSubjectName());
		  subject.setSubCredits(dtoObject.getSubCredits());
		  entityObject.setSubject(subject);
		}
		
		if(dtoObject.getSubjectTypeId() != null) {
			GeneralDetail gd = new GeneralDetail();
			gd.setGeneralDetailId(dtoObject.getSubjectTypeId());
			gd.setGeneralDetailCode(dtoObject.getSubjectTypeCode());
			gd.setGeneralDetailDisplayName(dtoObject.getSubjectTypeName());

			entityObject.setSubjectType(gd);
		}
		
		/*
		 * if(dtoObject.getSectionId() != null) { GroupSection groupSection = new
		 * GroupSection(); groupSection.setGroupSectionId(dtoObject.getSectionId());
		 * groupSection.setSection(dtoObject.getSection());
		 * 
		 * entityObject.setGroupSection(groupSection); }
		 */
		
		if(dtoObject.getStudentId() != null) {
			StudentDetail studentDetail = new StudentDetail();
			studentDetail.setStudentId(dtoObject.getStudentId());
			studentDetail.setFirstName(dtoObject.getStdFirstName());
			entityObject.setStudentDetail(studentDetail);
		}
		
		/*
		 * if(dtoObject.getStudentbatchId() != null) { Studentbatch studentbatch = new
		 * Studentbatch();
		 * studentbatch.setStudentbatchId(dtoObject.getStudentbatchId());
		 * studentbatch.setBatchName(dtoObject.getBatchName());
		 * entityObject.setStudentbatch(studentbatch); }
		 */
		
		if(dtoObject.getCourseId() != null) {
			Course course = new Course();
			course.setCourseId(dtoObject.getCourseId());
			course.setCourseName(dtoObject.getCourseName());
			entityObject.setCourse(course);
		}
		
		entityObject.setReason(dtoObject.getReason());

		entityObject.setUpdatedDt(dtoObject.getUpdatedDt());

		entityObject.setUpdatedUser(SecurityUtil.getCurrentUser());
		
		return entityObject;
	}
	
	@Override
	public StudentSubjectDTO convertEntityToDTO(StudentSubject entityObject) {
		StudentSubjectDTO dtoObject = null;
		if(entityObject != null) {
			dtoObject = new StudentSubjectDTO();
			
			 dtoObject.setStudentSubjectId(entityObject.getStudentSubjectId());
			
			if(entityObject.getAcademicYear()  != null) {
			  dtoObject.setAcademicYearId(entityObject.getAcademicYear().getAcademicYearId());
			  dtoObject.setAcademicYear(entityObject.getAcademicYear().getAcademicYear());
			}


			if(entityObject.getSchool() != null) {
			  dtoObject.setSchoolId(entityObject.getSchool().getSchoolId());
			  dtoObject.setSchoolName(entityObject.getSchool().getSchoolName());
			  dtoObject.setSchoolCode(entityObject.getSchool().getSchoolCode());
			}

			if(entityObject.getCourseYear() != null) {
			  dtoObject.setCourseYearId(entityObject.getCourseYear().getCourseYearId());
			  dtoObject.setCourseYearCode(entityObject.getCourseYear().getCourseYearCode());
			  dtoObject.setCourseYearName(entityObject.getCourseYear().getCourseYearName());
			}
			
			/*if (entityObject.getRegulation() != null) {
			  dtoObject.setRegulationId(entityObject.getRegulation().getRegulationId());
			  dtoObject.setRegulationName(entityObject.getRegulation().getRegulationName());
			  dtoObject.setRegulationCode(entityObject.getRegulation().getRegulationCode());
			}*/

			if(entityObject.getSubject()!=null) {
			  dtoObject.setSubjectId(entityObject.getSubject().getSubjectId());
			  dtoObject.setSubjectCode(entityObject.getSubject().getSubjectCode());
			  dtoObject.setSubjectName(entityObject.getSubject().getSubjectName());
			  dtoObject.setSubCredits(entityObject.getSubject().getSubCredits());
			  dtoObject.setShortName(entityObject.getSubject().getShortName());
			}

			if(entityObject.getSubjectType() != null) {
				dtoObject.setSubjectTypeId(entityObject.getSubjectType().getGeneralDetailId());
				dtoObject.setSubjectTypeCode(entityObject.getSubjectType().getGeneralDetailCode());
				dtoObject.setSubjectTypeName(entityObject.getSubjectType().getGeneralDetailDisplayName());
			}
			
			/*
			 * if(entityObject.getGroupSection() != null) {
			 * dtoObject.setSectionId(entityObject.getGroupSection().getGroupSectionId());
			 * dtoObject.setSection(entityObject.getGroupSection().getSection()); }
			 */
			if(entityObject.getStudentDetail() != null) {
				dtoObject.setStudentId(entityObject.getStudentDetail().getStudentId());
				dtoObject.setStdFirstName(entityObject.getStudentDetail().getFirstName());
			}
			/*
			 * if(entityObject.getStudentbatch() != null) {
			 * dtoObject.setStudentbatchId(entityObject.getStudentbatch().getStudentbatchId(
			 * )); dtoObject.setBatchName(entityObject.getStudentbatch().getBatchName()); }
			 */
			if(entityObject.getCourse() != null) {
				dtoObject.setCourseId(entityObject.getCourse().getCourseId());
				dtoObject.setCourseName(entityObject.getCourse().getCourseName());
			}
			dtoObject.setReason(entityObject.getReason());
			
			dtoObject.setIsActive(entityObject.getIsActive());
		}
		return dtoObject;
	}
	
	@Override
	public List<StudentSubject> convertDTOListToEntityList(List<StudentSubjectDTO> dtoObjectList) {
		List<StudentSubject> entityObjectList = new ArrayList<>();
		dtoObjectList
				.forEach(dtoObject -> entityObjectList.add(convertDTOtoEntity(dtoObject, null)));
		return entityObjectList;
	}

	@Override
	public List<StudentSubjectDTO> convertEntityListToDTOList(List<StudentSubject> entityObjectList) {
		List<StudentSubjectDTO> dtoObjectList = new ArrayList<>();
		entityObjectList.forEach(entityObject -> dtoObjectList.add(convertEntityToDTO(entityObject)));
		return dtoObjectList;
	}
}