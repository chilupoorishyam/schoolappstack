package com.gts.cms.common.util;

import java.security.MessageDigest;

import com.gts.cms.common.enums.Messages;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.BadCredentialsException;

/**
 * @author shyamsunderreddy
 *
 */
public class MD5Hashing {

	private static final Logger LOGGER = Logger.getLogger(MD5Hashing.class);

	public static String md5EncyptionUsingByte(String password) {
		String encryptPassword = "";
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes());
			byte byteData[] = md.digest();
			// convert the byte to hex format method 1
			StringBuffer builder = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				builder.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
			encryptPassword = builder.toString();
		} catch (Exception e) {
			LOGGER.error("Error occured while encripting the data");
			throw new BadCredentialsException(Messages.EXCEPTION_MESSAGE.getValue());
		}
		return encryptPassword;
	}

	public static String md5EncyptionUsingHex(String password) {
		String encryptPassword = "";
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes());
			byte byteData[] = md.digest();
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				String hex = Integer.toHexString(0xff & byteData[i]);
				if (hex.length() == 1)
					hexString.append('0');
				hexString.append(hex);
			}
			encryptPassword = hexString.toString();
		} catch (Exception e) {
			LOGGER.error("Error occured while encripting the data");
			throw new BadCredentialsException(Messages.EXCEPTION_MESSAGE.getValue());
		}
		return encryptPassword;
	}
}
