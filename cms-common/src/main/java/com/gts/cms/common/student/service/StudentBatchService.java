package com.gts.cms.common.student.service;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.StudentbatchDTO;

import java.util.List;

/**
 * @author Genesis
 *
 */
public interface StudentBatchService {

	public ApiResponse<List<StudentbatchDTO>> getStudentBatches(Boolean status, Long schoolId, Long subjecttypeId);

}