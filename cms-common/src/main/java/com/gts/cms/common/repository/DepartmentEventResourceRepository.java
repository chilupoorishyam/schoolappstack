package com.gts.cms.common.repository;

import com.gts.cms.entity.DepartmentEventResource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentEventResourceRepository extends JpaRepository<DepartmentEventResource, Long> {
}