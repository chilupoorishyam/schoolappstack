package com.gts.cms.common.student.repository;

import com.gts.cms.entity.Studentbatch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentBatchRepository extends JpaRepository<Studentbatch, Long> {
	
	@Query("SELECT s FROM Studentbatch s"
			+ " WHERE (:schoolId is null or s.school.schoolId = :schoolId)"
			+ " AND (:subjecttypeId is null or s.subjecttype.generalDetailId = :subjecttypeId)"
			+ " AND (:isActive is null or s.isActive = :isActive)"
			)
	List<Studentbatch> findStudentBatches(
			@Param("isActive")Boolean isActive,
			@Param("schoolId") Long schoolId,
			@Param("subjecttypeId") Long subjecttypeId);

}
