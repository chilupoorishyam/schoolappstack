package com.gts.cms.common.repository.custom;
/**
 * created by Naveen on 04/09/2018 
 * 
*/
public interface SchoolRepositoryCustom {
	Long deleteSchool(Long schoolId);
	Long uploadSchoolLogo(Long schoolId,String logoName);
}
