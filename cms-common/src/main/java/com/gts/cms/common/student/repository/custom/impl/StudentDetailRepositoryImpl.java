package com.gts.cms.common.student.repository.custom.impl;

import com.gts.cms.common.enums.Status;
import com.gts.cms.common.student.repository.custom.StudentDetailRepositoryCustom;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.StudentDetail;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.dml.UpdateClause;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAUpdateClause;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

import static com.gts.cms.entity.QFeeStudentData.feeStudentData;
import static com.gts.cms.entity.QOrganization.organization;
import static com.gts.cms.entity.QStudentDetail.studentDetail;

@Repository
public class StudentDetailRepositoryImpl extends QuerydslRepositorySupport implements StudentDetailRepositoryCustom {

	public StudentDetailRepositoryImpl() {
		super(StudentDetailRepositoryImpl.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<StudentDetail> findByStudentData(String studentData) {
		JPQLQuery<StudentDetail> query = from(studentDetail)
				.where(studentDetail.admissionNumber.equalsIgnoreCase(studentData)
						.or(studentDetail.firstName.equalsIgnoreCase(studentData))
						.or(studentDetail.mobile.eq(studentData)).and(studentDetail.isActive.eq(true)))
				.orderBy(studentDetail.admissionNumber.asc());
		return query.fetch();
	}

	@Override
	public Long updateStudentAdmissionPhotFileNames(Long studentId, String studentAadharFilePath,
			String studentPancardFilePath, String studentPhotoFilePath, String fatherPhotoFilePath,
			String motherPhotoFilePath, String spousePhotoFileName) {
		return update(studentDetail).where(studentDetail.studentId.eq(studentId))
				.set(studentDetail.aadharFilePath, studentAadharFilePath)
				.set(studentDetail.pancardFilePath, studentPancardFilePath)
				.set(studentDetail.studentPhotoPath, studentPhotoFilePath)
				.set(studentDetail.fatherPhotoPath, fatherPhotoFilePath)
				.set(studentDetail.motherPhotoPath, motherPhotoFilePath)
				.set(studentDetail.spousePhotoPath, spousePhotoFileName).execute();
	}

	@Override
	public List<StudentDetail> findAllStudentsDetails(Long orgId, Boolean status) {

		if (status != null) {
			JPQLQuery<StudentDetail> query = from(studentDetail).join(studentDetail.organization, organization);

			query.where(organization.organizationId.eq(orgId).and(studentDetail.isActive.eq(status)))
					.orderBy(studentDetail.admissionNumber.asc());
			return query.fetch();
		} else {
			JPQLQuery<StudentDetail> query = from(studentDetail).join(studentDetail.organization, organization);
			query.where(organization.organizationId.eq(orgId)).orderBy(studentDetail.admissionNumber.asc());
			return query.fetch();
		}
	}

	@Override
	public Long deleteStudentAdmissionDetail(Long id) {
		return update(studentDetail).where(studentDetail.studentId.eq(id))
				.set(studentDetail.isActive, Status.INACTIVE.getId()).execute();
	}

	/*@Override
	public List<StudentDetail> findStudentList(Boolean status, Long schoolId, Long courseId, Long courseGroupId,
			Long courseYearId, Long groupSectionId, Long academicYearId) {
		JPQLQuery<StudentDetail> query = null;

		query = from(studentDetail);
		query.where(studentDetail.studentStatus.generalDetailCode.ne(StudentDetailEnum.DTND.getValue()));
		if (status != null) {
			query.where(studentDetail.isActive.eq(status));

		} else {
			query.where(studentDetail.isActive.eq(Boolean.TRUE));
		}
		if (schoolId != null) {
			query.where(studentDetail.school.schoolId.eq(schoolId));
		}
		if (courseId != null) {
			query.where(studentDetail.course.courseId.eq(courseId));
		}
		if (courseGroupId != null) {
			query.where(studentDetail.courseGroup.courseGroupId.eq(courseGroupId));
		}
		if (courseYearId != null) {
			query.where(studentDetail.courseYear.courseYearId.eq(courseYearId));
		}
		if (groupSectionId != null) {
			query.where(studentDetail.groupSection.groupSectionId.eq(groupSectionId));
		}
		if (academicYearId != null) {
			query.where(studentDetail.academicYear.academicYearId.eq(academicYearId));
		}

		query.orderBy(studentDetail.createdDt.asc());

		return query.fetch();
	}*/

	@Override
	public List<StudentDetail> findStudentListByStructure(Boolean status, Long schoolId, Long academicYearId,
			Long feeStructureId) {
		JPQLQuery<StudentDetail> query = null;

		query = from(studentDetail).join(studentDetail.feeStudentData, feeStudentData);
		if (status != null) {
			query.where(studentDetail.isActive.eq(status));

		} else {
			query.where(studentDetail.isActive.eq(Boolean.TRUE));
		}
		if (schoolId != null) {
			query.where(studentDetail.school.schoolId.eq(schoolId));
		}
		if (academicYearId != null) {
			query.where(studentDetail.academicYear.academicYearId.eq(academicYearId));
		}
		if (feeStructureId != null) {
			query.where(feeStudentData.feeStructure.feeStructureId.eq(feeStructureId));
		}

		query.orderBy(studentDetail.createdDt.asc());

		return query.fetch();
	}

	@Override
	public void updateRegulationAndGroupsection(List<StudentDetail> studentDetailList) {
		/*
		 * SQLUpdateClause update = new SQLUpdateClause(,SQLTemplates.DEFAULT
		 * ,studentDetail); update.set(survey.name,
		 * "AA").where(studentDetail.name.eq("A")).addBatch(); update.set(survey.name,
		 * "BB").where(surstudentDetailvey.name.eq("B")).addBatch(); update.execute();
		 */

		/*
		 * EntityTransaction transaction = em.getTransaction(); transaction.begin();
		 */
		try {
			for (StudentDetail studentDetail1 : studentDetailList) {

				JPAUpdateClause query = update(studentDetail)
						.where(studentDetail.studentId.eq(studentDetail1.getStudentId()));
				/*
				 * if(studentDetail1.getGroupSection().getGroupSectionId()!=null) {
				 * query.set(studentDetail.groupSection.groupSectionId,
				 * studentDetail1.getGroupSection().getGroupSectionId()); }
				 */
				if (studentDetail1.getGroupSection() != null) {
					query.set(studentDetail.groupSection.groupSectionId,
							studentDetail1.getGroupSection().getGroupSectionId());
				} else {
					query.setNull(studentDetail.groupSection.groupSectionId);
				}

				/*if (studentDetail1.getRegulation().getRegulationId() != null) {
					query.set(studentDetail.regulation.regulationId, studentDetail1.getRegulation().getRegulationId());
				}*/
				query.set(studentDetail.updatedDt, new Date());
				query.set(studentDetail.updatedUser, SecurityUtil.getCurrentUser());
				query.execute();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	/*
	 * @Override public List<StudentDetail> findStudentAttendanceList(Boolean
	 * status, Long schoolId, Long courseId, Long courseGroupId, Long courseYearId,
	 * Long groupSectionId, Date attendanceDate) { JPQLQuery<StudentDetail> query =
	 * null;
	 * 
	 * query =
	 * from(studentDetail).join(studentDetail.stdAttendances,studentAttendance);
	 * 
	 * if (status != null) { query.where(studentDetail.isActive.eq(status));
	 * 
	 * } else { query.where(studentDetail.isActive.eq(Boolean.TRUE)); } if
	 * (schoolId != null) {
	 * query.where(studentDetail.school.schoolId.eq(schoolId)); } if (courseId !=
	 * null) { query.where(studentDetail.course.courseId.eq(courseId)); }
	 * 
	 * if (courseGroupId != null) {
	 * query.where(studentDetail.courseGroup.courseGroupId.eq(courseGroupId)); } if
	 * (courseYearId != null) {
	 * query.where(studentDetail.courseYear.courseYearId.eq(courseYearId)); } if
	 * (groupSectionId != null) {
	 * query.where(studentDetail.groupSection.groupSectionId.eq(groupSectionId)); }
	 * if (attendanceDate != null) {
	 * query.where(studentAttendance.attendanceDate.eq(attendanceDate)); }
	 * 
	 * query.orderBy(studentDetail.createdDt.asc());
	 * 
	 * return query.fetch(); }
	 */
	@Override
	public Long updateStudentAadharFile(Long studentId, String studentAadharFilePath) {
		return update(studentDetail).where(studentDetail.studentId.eq(studentId))
				.set(studentDetail.aadharFilePath, studentAadharFilePath).execute();

	}

	@Override
	public Long updateStudentPanCard(Long studentId, String studentPancardFileName) {
		return update(studentDetail).where(studentDetail.studentId.eq(studentId))
				.set(studentDetail.pancardFilePath, studentPancardFileName).execute();
	}

	@Override
	public Long updateStudentMotherPhotoFile(Long studentId, String motherPhotoFileName) {
		return update(studentDetail).where(studentDetail.studentId.eq(studentId))
				.set(studentDetail.motherPhotoPath, motherPhotoFileName).execute();
	}

	@Override
	public Long updateStudentFatherPhotoFile(Long studentId, String fatherPhotoFileName) {
		return update(studentDetail).where(studentDetail.studentId.eq(studentId))
				.set(studentDetail.fatherPhotoPath, fatherPhotoFileName).execute();
	}

	@Override
	public Long updateStudentPhotoFile(Long studentId, String studentPhotoFileName) {
		return update(studentDetail).where(studentDetail.studentId.eq(studentId))
				.set(studentDetail.studentPhotoPath, studentPhotoFileName).execute();
	}

	@Override
	public List<StudentDetail> searchStudentDetails(String firstName, String lastName, String middleName,
			String admissionNo, String mobile, Long schoolId, Long academicYearId,Boolean status,Long courseId,Long courseYearId) {
		JPQLQuery<StudentDetail> jpqlQuery = from(studentDetail);

		BooleanBuilder builder = new BooleanBuilder();
		if (admissionNo != null) {
			builder.or(studentDetail.rollNumber.like("%"+ admissionNo + "%"));
		}
		if (firstName != null) {
			builder.or(studentDetail.firstName.like("%" + firstName + "%"));
		}
		if (lastName != null) {
			builder.or(studentDetail.lastName.like("%" + lastName + "%"));
		}
		if (middleName != null) {
			builder.or(studentDetail.middleName.like("%" + middleName + "%"));
		}
		if (mobile != null) {
			builder.or(studentDetail.mobile.eq(mobile));
		}
		if(status != null) {
			builder.and(studentDetail.isActive.eq(status));
		}
		if (schoolId != null) {
			builder.and(studentDetail.school.schoolId.eq(schoolId));
		}
		if (academicYearId != null) {
			builder.and(studentDetail.academicYear.academicYearId.eq(academicYearId));
		}
		if(courseId!=null) {
			builder.and(studentDetail.course.courseId.eq(courseId));
		}
		if(courseYearId!=null) {
			builder.and(studentDetail.courseYear.courseYearId.eq(courseYearId));
		}
		/*if(courseGroupId!=null && courseGroupId>0) {
			builder.and(studentDetail.courseGroup.courseGroupId.eq(courseGroupId));
		}*/
		
		// builder.and(studentDetail.studentStatus.generalDetailCode.eq("INCOLLEGE"));
		jpqlQuery.where(builder).orderBy(studentDetail.studentId.asc());
		jpqlQuery.limit(50);
		return jpqlQuery.fetch();
	}

	@Override
	public Long updateStudentStatus(Long generalDetailId, Long schoolId, Long studentId, String reason, Long academicYearId, Long courseYearId, Long groupSectionId) {
		return null;
	}

	public Long updateStudentStatus(Long generalDetailId, Long schoolId, Long studentId, String reason,
			Long academicYearId, Long courseId, Long courseYearId, Long groupSectionId) {
		UpdateClause<JPAUpdateClause> query = update(studentDetail)
				.where(studentDetail.studentId.eq(studentId).and(studentDetail.school.schoolId.eq(schoolId)));
		if (reason != null) {
			query.set(studentDetail.reason, reason);
		}
		query.set(studentDetail.studentStatus.generalDetailId, generalDetailId);
		if (academicYearId != null) {
			query.set(studentDetail.academicYear.academicYearId, academicYearId);
		}
		if (courseId != null) {
			query.set(studentDetail.course.courseId, courseId);
		}
		/*if (courseGroupId != null) {
			query.set(studentDetail.courseGroup.courseGroupId, courseGroupId);
		}*/
		if (courseYearId != null) {
			query.set(studentDetail.courseYear.courseYearId, courseYearId);
		}
		if (groupSectionId != null) {
			query.set(studentDetail.groupSection.groupSectionId, groupSectionId);
		}
		/*if(regulationId!=null) {
			query.set(studentDetail.regulation.regulationId, regulationId);
		}*/
		
		query.set(studentDetail.updatedDt, new Date());
		query.set(studentDetail.updatedUser, SecurityUtil.getCurrentUser());
		return query.execute();
	}

	@Override
	public List<StudentDetail> findDetainStudentsList(Boolean status, Long schoolId, Long courseId,
			Long courseYearId, Long groupSectionId, Long academicYearId, Long detailId) {
		JPQLQuery<StudentDetail> query = null;

		query = from(studentDetail);
		if (status != null) {
			query.where(studentDetail.isActive.eq(status));

		} else {
			query.where(studentDetail.isActive.eq(Boolean.TRUE));
		}
		query.where(studentDetail.studentStatus.generalDetailId.eq(detailId));
		if (schoolId != null) {
			query.where(studentDetail.school.schoolId.eq(schoolId));
		}
		if (courseId != null) {
			query.where(studentDetail.course.courseId.eq(courseId));
		}

		/*if (courseGroupId != null) {
			query.where(studentDetail.courseGroup.courseGroupId.eq(courseGroupId));
		}*/
		if (courseYearId != null) {
			query.where(studentDetail.courseYear.courseYearId.eq(courseYearId));
		}
		if (groupSectionId != null) {
			query.where(studentDetail.groupSection.groupSectionId.eq(groupSectionId));
		}
		if (academicYearId != null) {
			query.where(studentDetail.academicYear.academicYearId.eq(academicYearId));
		}

		query.orderBy(studentDetail.createdDt.asc());

		return query.fetch();
	}

	@Override
	public Long updateStudentSection(Long schoolId, Long studentId, Long groupSectionId) {
		return update(studentDetail)
				.where(studentDetail.studentId.eq(studentId).and(studentDetail.school.schoolId.eq(schoolId)))
				.set(studentDetail.groupSection.groupSectionId, groupSectionId).execute();
	}

	@Override
	public List<StudentDetail> getBirthdayList(Long organizationId, Long schoolId, Integer day, Integer month) {

		JPQLQuery<StudentDetail> query = null;

		query = from(studentDetail).where(studentDetail.isActive.eq(Boolean.TRUE)
				.and(studentDetail.organization.organizationId.eq(organizationId)
						.and(studentDetail.school.schoolId.eq(schoolId)).and(studentDetail.dateOfBirth.dayOfMonth()
								.eq(day).and(studentDetail.dateOfBirth.month().eq(month)))));
		return query.fetch();
	}

	@Override
	public List<StudentDetail> findFeeMappingStudentList(Long schoolId,
			Long courseYearId, Long academicYearId, Long quotaId) {
		JPQLQuery<StudentDetail> query = null;

		query = from(studentDetail).leftJoin(studentDetail.feeStudentData, feeStudentData)
				.on(feeStudentData.academicYear.academicYearId.ne(academicYearId));
		query.where(studentDetail.isActive.eq(Boolean.TRUE));

		if (schoolId != null) {
			query.where(studentDetail.school.schoolId.eq(schoolId));
		}

		/*if (courseGroupId != null) {
			query.where(studentDetail.courseGroup.courseGroupId.eq(courseGroupId));
		}*/

		if (courseYearId != null) {
			query.where(studentDetail.courseYear.courseYearId.in(courseYearId));
		}

		if (academicYearId != null) {
			query.where(studentDetail.academicYear.academicYearId.eq(academicYearId));
			//query.where(feeStudentData.academicYear.academicYearId.ne(academicYearId));
		}
		if (quotaId != null) {
			query.where(studentDetail.quota.generalDetailId.eq(quotaId));
		}

		//query.where(feeStudentData.studentDetail.studentId.isNull());

		query.orderBy(studentDetail.createdDt.asc());

		return query.fetch();
	}

	/*@Override
	public	Long updateSection(List<Long> studentsId, Long toGroupSectionId, Long toBatchId, Long toCourseYearId, Date toDate,
			Long academicYearId) {

		UpdateClause<JPAUpdateClause> query = update(studentDetail)
				.where(studentDetail.studentId.in(studentsId));
		if (toGroupSectionId != null) {
			query.set(studentDetail.groupSection.groupSectionId, toGroupSectionId);
		}
		if (toBatchId != null) {
			query.set(studentDetail.batch.batchId, toBatchId);
		}
		if (toCourseYearId != null) {
			query.set(studentDetail.courseYear.courseYearId, toCourseYearId);
		}
		if (academicYearId != null) {
			query.set(studentDetail.academicYear.academicYearId, academicYearId);
		}
		return  query.execute();
	}*/
	
	@Override
	public StudentDetail findByStudenAdmissionNo(String admissionNo) {
		JPQLQuery<StudentDetail> query = from(studentDetail)
				.where(studentDetail.admissionNumber.equalsIgnoreCase(admissionNo))
				.orderBy(studentDetail.admissionNumber.asc())
				.limit(1);
		return query.fetchOne();
	}
	
	@Override
	public StudentDetail findByStudentRollNo(String rollNo) {
		JPQLQuery<StudentDetail> query = from(studentDetail)
				.where(studentDetail.rollNumber.equalsIgnoreCase(rollNo))
				.orderBy(studentDetail.rollNumber.asc())
				.limit(1);
		return query.fetchOne();
	}
}
