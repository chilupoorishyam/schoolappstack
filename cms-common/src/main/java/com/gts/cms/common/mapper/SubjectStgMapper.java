package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.SubjectStgDTO;
import com.gts.cms.entity.SubjectStg;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SubjectStgMapper implements BaseMapper<SubjectStg, SubjectStgDTO> {
    private static final Logger LOGGER = Logger.getLogger(SubjectStgMapper.class);


    @Override
    public SubjectStgDTO convertEntityToDTO(SubjectStg subjectStg) {
        SubjectStgDTO subjectStgDTO = null;
        if (subjectStg != null) {
            subjectStgDTO = SubjectStgDTO.builder()
                    .school(subjectStg.getSchool())
                    .course(subjectStg.getCourse())
                    .subjectName(subjectStg.getSubjectName())
                    .subjectCode(subjectStg.getSubjectCode())
                    .subjectShortName(subjectStg.getSubjectShortName())
                    .subjectType(subjectStg.getSubjectType())
                    .subjectCategory(subjectStg.getSubjectCategory())
                    .creditHours(subjectStg.getCreditHours())
                    .build();
        }
        return subjectStgDTO;
    }

    @Override
    public List<SubjectStgDTO> convertEntityListToDTOList(List<SubjectStg> entityList) {
        List<SubjectStgDTO> detailStgDTOArrayList = new ArrayList<>();
        entityList.forEach(subjectStg -> detailStgDTOArrayList.add(convertEntityToDTO(subjectStg)));
        return detailStgDTOArrayList;
    }

    @Override
    public List<SubjectStg> convertDTOListToEntityList(List<SubjectStgDTO> dtoList) {
        return null;
    }

    @Override
    public SubjectStg convertDTOtoEntity(SubjectStgDTO subjectStgDTO, SubjectStg subjectStg) {
        return null;
    }
}
