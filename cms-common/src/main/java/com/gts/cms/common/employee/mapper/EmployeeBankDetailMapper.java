package com.gts.cms.common.employee.mapper;

import com.gts.cms.common.employee.dto.EmployeeBankDetailDTO;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.util.OptionalUtil;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.EmployeeBankDetail;
import com.gts.cms.entity.EmployeeDetail;
import com.gts.cms.entity.Organization;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Genesis
 *
 */
@Component
public class EmployeeBankDetailMapper implements BaseMapper<EmployeeBankDetail, EmployeeBankDetailDTO> {

	public EmployeeBankDetailDTO convertEntityToDTO(EmployeeBankDetail employeeBankDetail) {
		EmployeeBankDetailDTO employeeBankDetailDTO = null;
		if (employeeBankDetail != null) {
			employeeBankDetailDTO = new EmployeeBankDetailDTO();

			employeeBankDetailDTO.setEmployeeBankDetailId(employeeBankDetail.getEmployeeBankDetailId());
			employeeBankDetailDTO.setCreatedDt(employeeBankDetail.getCreatedDt());
			employeeBankDetailDTO.setCreatedUser(employeeBankDetail.getCreatedUser());
			employeeBankDetailDTO.setAccountNumber(employeeBankDetail.getAccountNumber());
			employeeBankDetailDTO.setBankAddress(employeeBankDetail.getBankAddress());
			employeeBankDetailDTO.setBankName(employeeBankDetail.getBankName());
			employeeBankDetailDTO.setBranchName(employeeBankDetail.getBranchName());
			employeeBankDetailDTO.setDdPayableAddress(employeeBankDetail.getDdPayableAddress());
			employeeBankDetailDTO.setIfscCode(employeeBankDetail.getIfscCode());
			employeeBankDetailDTO.setIsActive(employeeBankDetail.getIsActive());
			employeeBankDetailDTO.setReason(employeeBankDetail.getReason());
			employeeBankDetailDTO.setPhone(employeeBankDetail.getPhone());

			employeeBankDetailDTO.setEmployeeId(
					OptionalUtil.resolve(() -> employeeBankDetail.getEmployeeDetail().getEmployeeId()).orElse(null));
			employeeBankDetailDTO.setOrganizationId(
					OptionalUtil.resolve(() -> employeeBankDetail.getOrganization().getOrganizationId()).orElse(null));

		}
		return employeeBankDetailDTO;
	}

	public EmployeeBankDetail convertDTOtoEntity(EmployeeBankDetailDTO employeeBankDetailDTO,
			EmployeeBankDetail employeeBankDetail) {
		if (employeeBankDetailDTO != null) {
			if (employeeBankDetail == null) {
				employeeBankDetail = new EmployeeBankDetail();
				employeeBankDetail.setIsActive(Boolean.TRUE);
				employeeBankDetail.setCreatedDt(new Date());
				employeeBankDetail.setCreatedUser(SecurityUtil.getCurrentUser());
			}else {
				employeeBankDetail.setIsActive(employeeBankDetailDTO.getIsActive());
			}
			
			employeeBankDetail.setEmployeeBankDetailId(employeeBankDetailDTO.getEmployeeBankDetailId());
			employeeBankDetail.setAccountNumber(employeeBankDetailDTO.getAccountNumber());
			employeeBankDetail.setBankAddress(employeeBankDetailDTO.getBankAddress());
			employeeBankDetail.setBankName(employeeBankDetailDTO.getBankName());
			employeeBankDetail.setBranchName(employeeBankDetailDTO.getBranchName());
			employeeBankDetail.setUpdatedDt(new Date());
			employeeBankDetail.setUpdatedUser(SecurityUtil.getCurrentUser());
			employeeBankDetail.setDdPayableAddress(employeeBankDetailDTO.getDdPayableAddress());
			employeeBankDetail.setIfscCode(employeeBankDetailDTO.getIfscCode());
			employeeBankDetail.setReason(employeeBankDetailDTO.getReason());
			employeeBankDetail.setPhone(employeeBankDetailDTO.getPhone());

			EmployeeDetail employeeDetail = new EmployeeDetail();
			employeeDetail.setEmployeeId(employeeBankDetailDTO.getEmployeeId());
			employeeBankDetail.setEmployeeDetail(employeeDetail);

			Organization organization = new Organization();
			organization.setOrganizationId(employeeBankDetailDTO.getOrganizationId());
			employeeBankDetail.setOrganization(organization);
		}
		return employeeBankDetail;
	}

	public List<EmployeeBankDetail> convertDTOListToEntityList(
			List<EmployeeBankDetailDTO> employeeBankDetailDTOList) {
		List<EmployeeBankDetail> employeeBankDetailList = new ArrayList<>();
		if (employeeBankDetailDTOList != null) {
			employeeBankDetailDTOList.forEach(employeeBankDetail -> employeeBankDetailList
					.add(convertDTOtoEntity(employeeBankDetail, null)));
		}
		return employeeBankDetailList;

	}

	public List<EmployeeBankDetailDTO> convertEntityListToDTOList(
			List<EmployeeBankDetail> employeeBankDetailList) {
		List<EmployeeBankDetailDTO> employeeBankDetailDTOList = new ArrayList<>();
		employeeBankDetailList
				.forEach(employeeBankDetail -> employeeBankDetailDTOList.add(convertEntityToDTO(employeeBankDetail)));
		return employeeBankDetailDTOList;
	}

}
