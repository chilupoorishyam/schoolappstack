package com.gts.cms.common.repository.custom;
/**
 * created by Naveen on 04/09/2018 
 * 
*/
public interface OrganizationRepositoryCustom {
	Long deleteOrganization(Long organizationId);
	Long uploadOrganizationLogo(Long organizationId,String logoName);
}
