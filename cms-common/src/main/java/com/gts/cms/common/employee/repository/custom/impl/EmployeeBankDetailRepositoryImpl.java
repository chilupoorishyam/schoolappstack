package com.gts.cms.common.employee.repository.custom.impl;

import com.gts.cms.common.employee.repository.custom.EmployeeBankDetailRepositoryCustom;
import com.gts.cms.entity.EmployeeBankDetail;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static com.gts.cms.entity.QEmployeeBankDetail.employeeBankDetail;
/**
 * @author Genesis
 *
 */
@Repository
public class EmployeeBankDetailRepositoryImpl extends QuerydslRepositorySupport implements  EmployeeBankDetailRepositoryCustom{

	public EmployeeBankDetailRepositoryImpl() {
		super(EmployeeBankDetailRepositoryImpl.class);
	}

	@PersistenceContext
	private EntityManager em;
	
	public List<EmployeeBankDetail> findAllEmployeeBankDetail(Long empId, Boolean status) {
		JPQLQuery<EmployeeBankDetail> query = null;

		query = from(employeeBankDetail);
		
		if (status != null) {
			query.where(employeeBankDetail.isActive.eq(status));
		} else {
			query.where(employeeBankDetail.isActive.eq(Boolean.TRUE));
		}
		if (empId != null) {
			query.where(employeeBankDetail.employeeDetail.employeeId.eq(empId));
		}

		query.orderBy(employeeBankDetail.createdDt.asc());

		return query.fetch();
	}
	

}
