package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * The Class AcademicYear.
 */
public class GeneralMasterDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long generalMasterId;

	@NotNull(message = "General Master Code is required.")
	private String generalMasterCode;

	@NotNull(message = "General Master Description is required.")
	private String generalMasterDescription;

	@NotNull(message = "General Master DisplayName is required.")
	private String generalMasterDisplayName;
	
	@NotNull(message = "IsActive is required.")
	private Boolean isActive;
	
	@NotNull(message = "IsEditable is required.")
	private Boolean isEditable;

	private String reason;

	private Date createdDt;

	private Long createdUser;
	
	private List<GeneralDetailDTO> generalDetailDTOList;

	public Long getGeneralMasterId() {
		return generalMasterId;
	}

	public void setGeneralMasterId(Long generalMasterId) {
		this.generalMasterId = generalMasterId;
	}

	public String getGeneralMasterCode() {
		return generalMasterCode;
	}

	public void setGeneralMasterCode(String generalMasterCode) {
		this.generalMasterCode = generalMasterCode;
	}

	public String getGeneralMasterDescription() {
		return generalMasterDescription;
	}

	public void setGeneralMasterDescription(String generalMasterDescription) {
		this.generalMasterDescription = generalMasterDescription;
	}

	public String getGeneralMasterDisplayName() {
		return generalMasterDisplayName;
	}

	public void setGeneralMasterDisplayName(String generalMasterDisplayName) {
		this.generalMasterDisplayName = generalMasterDisplayName;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsEditable() {
		return isEditable;
	}

	public void setIsEditable(Boolean isEditable) {
		this.isEditable = isEditable;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public List<GeneralDetailDTO> getGeneralDetailDTOList() {
		return generalDetailDTOList;
	}

	public void setGeneralDetailDTOList(List<GeneralDetailDTO> generalDetailDTOList) {
		this.generalDetailDTOList = generalDetailDTOList;
	}

}
