package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.NetworkDTO;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class NetworkMapper implements BaseMapper<Network, NetworkDTO> {
	@Override
	public NetworkDTO convertEntityToDTO(Network entityObject) {
		NetworkDTO dtoObject = null;
		if (entityObject != null) {
			dtoObject = new NetworkDTO();
			dtoObject.setUpdatedDt(entityObject.getUpdatedDt());
			dtoObject.setCreatedUser(entityObject.getCreatedUser());
			dtoObject.setCreatedDt(entityObject.getCreatedDt());
			dtoObject.setUpdatedUser(entityObject.getUpdatedUser());
			dtoObject.setIsActive(entityObject.getIsActive());
			dtoObject.setLocation(entityObject.getLocation());
			dtoObject.setNetworkShortName(entityObject.getNetworkShortName());
			dtoObject.setEstablishedYear(entityObject.getEstablishedYear());
			dtoObject.setNetworkIcon(entityObject.getNetworkIcon());
			dtoObject.setNetworkId(entityObject.getNetworkId());
			dtoObject.setCallAlums(entityObject.getCallAlums());
			dtoObject.setNetworkName(entityObject.getNetworkName());
			dtoObject.setReason(entityObject.getReason());
			dtoObject.setWebsite(entityObject.getWebsite());
			
			if(entityObject.getNetworkTypeCat()!=null) {
				dtoObject.setNetworkTypeId(entityObject.getNetworkTypeCat().getGeneralDetailId());
				dtoObject.setNetworkTypeName(entityObject.getNetworkTypeCat().getGeneralDetailDisplayName());
				dtoObject.setNetworkTypeCode(entityObject.getNetworkTypeCat().getGeneralDetailCode());
			}
			
			if(entityObject.getNetworkCreatedProfile()!=null) {
				dtoObject.setNetworkCreatedProfileId(entityObject.getNetworkCreatedProfile().getAmsProfileId());
				dtoObject.setNetworkFirstName(entityObject.getNetworkCreatedProfile().getAmsFirstName());
				dtoObject.setNetworkCompanyName(entityObject.getNetworkCreatedProfile().getCompanyName());
				dtoObject.setNetworkCityName(entityObject.getNetworkCreatedProfile().getCityName());
			}
			if(entityObject.getSchool()!=null) {
				dtoObject.setSchoolId(entityObject.getSchool().getSchoolId());
				dtoObject.setSchoolName(entityObject.getSchool().getSchoolName());
				dtoObject.setSchoolCode(entityObject.getSchool().getSchoolCode());
			}
			if(entityObject.getOrganization()!=null) {
				dtoObject.setOrganizationId(entityObject.getOrganization().getOrganizationId());
				dtoObject.setOrgCode(entityObject.getOrganization().getOrgCode());
				dtoObject.setOrgName(entityObject.getOrganization().getOrgName());
			}
		}
		return dtoObject;
	}

	@Override
	public Network convertDTOtoEntity(NetworkDTO dtoObject, Network entityObject) {
		if (dtoObject != null) {
			if (entityObject == null) {
				entityObject = new Network();
				entityObject.setIsActive(Boolean.TRUE);
				entityObject.setCreatedDt(new Date());
				entityObject.setCreatedUser(SecurityUtil.getCurrentUser());
			}
			if (dtoObject.getNetworkId() != null) {
				entityObject.setCreatedDt(dtoObject.getCreatedDt());
				if (dtoObject.getCreatedUser() != null) {
					entityObject.setCreatedUser(dtoObject.getCreatedUser());
				} else {
					entityObject.setCreatedUser(SecurityUtil.getCurrentUser());
				}
				entityObject.setUpdatedDt(new Date());
				entityObject.setIsActive(dtoObject.getIsActive());
				entityObject.setUpdatedUser(SecurityUtil.getCurrentUser());
			}

			entityObject.setNetworkId(dtoObject.getNetworkId());
			entityObject.setLocation(dtoObject.getLocation());
			if (dtoObject.getNetworkTypeId() != null) {
				GeneralDetail generalDetail = new GeneralDetail();
				generalDetail.setGeneralDetailId(dtoObject.getNetworkTypeId());
				entityObject.setNetworkTypeCat(generalDetail);
			}
			entityObject.setNetworkShortName(dtoObject.getNetworkShortName());

			if (dtoObject.getNetworkCreatedProfileId() != null) {
				AmsProfileDetail amsProfileDetail = new AmsProfileDetail();
				amsProfileDetail.setAmsProfileId(dtoObject.getNetworkCreatedProfileId());
				entityObject.setNetworkCreatedProfile(amsProfileDetail);
			}
			entityObject.setEstablishedYear(dtoObject.getEstablishedYear());
			entityObject.setNetworkIcon(dtoObject.getNetworkIcon());
			if (dtoObject.getSchoolId() != null) {
				School school = new School();
				school.setSchoolId(dtoObject.getSchoolId());
				entityObject.setSchool(school);
			}
			if (dtoObject.getOrganizationId() != null) {
				Organization organization = new Organization();
				organization.setOrganizationId(dtoObject.getOrganizationId());
				entityObject.setOrganization(organization);
			}
			entityObject.setNetworkId(dtoObject.getNetworkId());
			entityObject.setCallAlums(dtoObject.getCallAlums());
			entityObject.setNetworkName(dtoObject.getNetworkName());
			entityObject.setReason(dtoObject.getReason());
			entityObject.setWebsite(dtoObject.getWebsite());
		}
		return entityObject;
	}

	@Override
	public List<Network> convertDTOListToEntityList(List<NetworkDTO> networkDTOList) {
		List<Network> networkList = new ArrayList<>();
		networkDTOList.forEach(dtoObject -> networkList.add(convertDTOtoEntity(dtoObject, null)));
		return networkList;
	}

	@Override
	public List<NetworkDTO> convertEntityListToDTOList(List<Network> networkList) {
		List<NetworkDTO> networkDTOList = new ArrayList<>();
		networkList.forEach(entityObject -> networkDTOList.add(convertEntityToDTO(entityObject)));
		return networkDTOList;
	}
}