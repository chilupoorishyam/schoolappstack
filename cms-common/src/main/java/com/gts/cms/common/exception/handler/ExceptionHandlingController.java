package com.gts.cms.common.exception.handler;

import java.util.ArrayList;
import java.util.List;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.FieldErrorResource;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.exception.BadRequestSearchException;
import com.gts.cms.common.exception.UserTokenExpiredException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.gts.cms.common.exception.BadRequestException;

@ControllerAdvice
@RestController
public class ExceptionHandlingController {
	private static Logger LOGGER = LogManager.getLogger();

	@ExceptionHandler(BadRequestException.class)
	protected ApiResponse<List<FieldErrorResource>> handleInvalidRequest(BadRequestException apiException) {
		LOGGER.error("Exception raised " + apiException);
		List<FieldErrorResource> fieldErrorResources = new ArrayList<>();

		if (null != apiException.getErrors()) {
			List<FieldError> fieldErrors = apiException.getErrors().getFieldErrors();
			for (FieldError fieldError : fieldErrors) {
				FieldErrorResource fieldErrorResource = new FieldErrorResource();
				fieldErrorResource.setField(fieldError.getField());
				fieldErrorResource.setCode(fieldError.getCode());
				fieldErrorResource.setMessage(fieldError.getDefaultMessage());
				fieldErrorResources.add(fieldErrorResource);
			}
		}
		if(apiException.getMessage() != null) {
			return new ApiResponse<List<FieldErrorResource>>(Boolean.FALSE,apiException.getMessage(), fieldErrorResources, HttpStatus.BAD_REQUEST.value());
		}else {
			return new ApiResponse<List<FieldErrorResource>>(Boolean.FALSE,"Invalid Request", fieldErrorResources, HttpStatus.BAD_REQUEST.value());
		}
		
	}

	@ExceptionHandler(BadRequestSearchException.class)
	protected ApiResponse<List<FieldErrorResource>> handleInvalidSearchRequest(BadRequestSearchException apiException) {
		LOGGER.error("Exception raised " + apiException);
		List<FieldErrorResource> fieldErrorResources = new ArrayList<>();

		if (null != apiException.getErrors()) {
			List<FieldError> fieldErrors = apiException.getErrors().getFieldErrors();
			for (FieldError fieldError : fieldErrors) {
				FieldErrorResource fieldErrorResource = new FieldErrorResource();
				fieldErrorResource.setField(fieldError.getField());
				fieldErrorResource.setCode(fieldError.getCode());
				fieldErrorResource.setMessage(fieldError.getDefaultMessage());
				fieldErrorResources.add(fieldErrorResource);
			}
		}
		if(apiException.getMessage() != null) {
			return new ApiResponse<List<FieldErrorResource>>(Boolean.FALSE,apiException.getMessage(), fieldErrorResources, HttpStatus.OK.value());
		}else {
			return new ApiResponse<List<FieldErrorResource>>(Boolean.FALSE,"Invalid Request", fieldErrorResources, HttpStatus.OK.value());
		}
		
	}
	
	
	
	@ExceptionHandler(ApiException.class)
	protected ApiResponse<?> handleApiException(ApiException exception) {
		return new ApiResponse<>(Boolean.FALSE,exception.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY.value());
	}
	
	@ExceptionHandler(NoHandlerFoundException.class)
	protected ApiResponse<?> handleNoHandlerFoundException(NoHandlerFoundException exception) {
		return new ApiResponse<>(Boolean.FALSE,"Resource Not Found!", HttpStatus.NOT_FOUND.value());
	}
	
	@ExceptionHandler(UserTokenExpiredException.class)
	public ApiResponse<?> handleUserTokenExpiredException(UserTokenExpiredException exception) {
		return new ApiResponse<>(Boolean.FALSE,"Token Expired,Login to continue.", HttpStatus.BAD_GATEWAY.value());
	}
	
	@ExceptionHandler(BadCredentialsException.class)
	public ApiResponse<?> handleBadCredentialsException(BadCredentialsException exception) {
		return new ApiResponse<>(Boolean.FALSE,"Invalid Username or Password.", HttpStatus.UNAUTHORIZED.value());
	}
	
	
	@ExceptionHandler(MultipartException.class)
	public ApiResponse<?> handleMultipartException(MultipartException exception) {
		return new ApiResponse<>(Boolean.FALSE,"Unable to upload file, please contact system admin.", HttpStatus.UNPROCESSABLE_ENTITY.value());
	}
	
	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	public ApiResponse<?> handleMultipartException(HttpRequestMethodNotSupportedException exception) {
		return new ApiResponse<>(Boolean.FALSE,"Request Method Not Supported.", HttpStatus.BAD_REQUEST.value());
	}
	
	@ExceptionHandler(MissingServletRequestParameterException.class)
	public ApiResponse<?> handleMultipartException(MissingServletRequestParameterException exception) {
		return new ApiResponse<>(Boolean.FALSE,"Missing Required Parameters.", HttpStatus.BAD_REQUEST.value());
	}
	
	@ExceptionHandler(Exception.class)
	protected ApiResponse<?> handleException(Exception exception) {
		LOGGER.error("Exception raised : "+exception);
		LOGGER.error("Exception occured while processing request.  Reason : "+exception.getLocalizedMessage(), exception);
		return new ApiResponse<>(Boolean.FALSE,"Internal Server error. Please contact system admin.", HttpStatus.INTERNAL_SERVER_ERROR.value());
	}
	
	
}
