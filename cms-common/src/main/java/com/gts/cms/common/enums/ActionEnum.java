package com.gts.cms.common.enums;

public enum ActionEnum {
	ACTION_SAVE("SAVE"), 
	ACTION_SUBMIT("SUBMIT");
	private String id;

	private ActionEnum(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
