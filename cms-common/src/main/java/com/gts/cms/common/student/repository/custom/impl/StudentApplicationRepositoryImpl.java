package com.gts.cms.common.student.repository.custom.impl;

import com.gts.cms.common.enums.Status;
import com.gts.cms.common.student.repository.custom.StudentApplicationRepositoryCustom;
import com.gts.cms.entity.StudentApplication;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

import static com.gts.cms.entity.QOrganization.organization;
import static com.gts.cms.entity.QStudentApplication.studentApplication;
import static com.gts.cms.entity.QWorkflowStage.workflowStage;

@Repository
public class StudentApplicationRepositoryImpl extends QuerydslRepositorySupport
		implements StudentApplicationRepositoryCustom {

	public StudentApplicationRepositoryImpl() {
		super(StudentApplicationRepositoryImpl.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<StudentApplication> findByStudentData(String studentData, Long schoolId) {
		JPQLQuery<StudentApplication> query = from(studentApplication).where(studentApplication.applicationNo
				.equalsIgnoreCase(studentData).or(studentApplication.firstName.equalsIgnoreCase(studentData))
							.or(studentApplication.mobile.eq(studentData))
							.and(studentApplication.isActive.eq(true))
							.and(studentApplication.school.schoolId.eq(schoolId)))
			.orderBy(studentApplication.applicationNo.asc());
		return query.fetch();
	}

	@Override
	public Long updateStudentApplicationPhotFileNames(Long applicationId, String studentAadharFilePath,
			String studentPancardFilePath, String studentPhotoFilePath, String fatherPhotoFilePath,
			String motherPhotoFilePath) {
		return update(studentApplication).where(studentApplication.studentAppId.eq(applicationId))
				.set(studentApplication.aadharFilePath, studentAadharFilePath)
				.set(studentApplication.pancardFilePath, studentPancardFilePath)
				.set(studentApplication.studentPhotoPath, studentPhotoFilePath)
				.set(studentApplication.fatherPhotoPath, fatherPhotoFilePath)
				.set(studentApplication.motherPhotoPath, motherPhotoFilePath).execute();
	}

	@Override
	public List<StudentApplication> findAllStudentsApplicationForms(Long orgId, String role, Integer workFlowStep) {
		JPQLQuery<StudentApplication> query = from(studentApplication).join(studentApplication.organization, organization)
				.join(studentApplication.workflowStage, workflowStage);
		query.where(organization.organizationId.eq(orgId).and(workflowStage.wfStage.eq(workFlowStep)).and(studentApplication.isActive.eq(Status.ACTIVE.getId()))).orderBy(studentApplication.createdDt.asc());
		return query.fetch();
	}

	@Override
	public List<StudentApplication> findAllStudentsApplicationForms(Long orgId, Boolean status) {

		if (status != null) {
			JPQLQuery<StudentApplication> query = from(studentApplication).join(studentApplication.organization,organization);
					
			query.where(organization.organizationId.eq(orgId).and(studentApplication.isActive.eq(status)))
					.orderBy(studentApplication.createdDt.desc());
			return query.fetch();
		} else {
			JPQLQuery<StudentApplication> query = from(studentApplication).join(studentApplication.organization,organization);
			query.where(organization.organizationId.eq(orgId)).orderBy(studentApplication.createdDt.desc());
			return query.fetch();
		}
	}
	@Override
	public Long deleteStudentApplicationDetail(Long id) {
		return update(studentApplication).where(studentApplication.studentAppId.eq(id))
				.set(studentApplication.isActive, Status.INACTIVE.getId()).execute();
	}

	@Override
	public Long updateStudentAdmissionNumber(Long studentAppId, Date adminssionDate, String admissionNumber) {
		return update(studentApplication).where(studentApplication.studentAppId.eq(studentAppId))
				.set(studentApplication.admissionNumber, admissionNumber)
				.set(studentApplication.adminssionDate, adminssionDate)
				.execute();
		}

	@Override
	public Long updateStudentAadharFile(Long applicationId, String studentAadharFilePath) {
		return update(studentApplication).where(studentApplication.studentAppId.eq(applicationId))
				.set(studentApplication.aadharFilePath, studentAadharFilePath).execute();
	}

	@Override
	public Long updateStudentPancardFile(Long applicationId, String studentPancardFileName) {
		return update(studentApplication).where(studentApplication.studentAppId.eq(applicationId))
				.set(studentApplication.pancardFilePath, studentPancardFileName).execute();
	}

	@Override
	public Long updateStudentMotherPhotoFile(Long applicationId, String motherPhotoFileName) {
		return update(studentApplication).where(studentApplication.studentAppId.eq(applicationId))
				.set(studentApplication.motherPhotoPath, motherPhotoFileName).execute();
	}

	@Override
	public Long updateStudentFatherPhotoFile(Long applicationId, String fatherPhotoFileName) {
		return update(studentApplication).where(studentApplication.studentAppId.eq(applicationId))
				.set(studentApplication.fatherPhotoPath, fatherPhotoFileName).execute();
	}

	@Override
	public Long updateStudentPhotoFile(Long applicationId, String studentPhotoFileName) {
		return update(studentApplication).where(studentApplication.studentAppId.eq(applicationId))
				.set(studentApplication.studentPhotoPath, studentPhotoFileName).execute();
	}
}
