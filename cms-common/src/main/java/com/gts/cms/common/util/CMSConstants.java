package com.gts.cms.common.util;

/**
 * @author Shyamsunder
 */
public final class CMSConstants {

    /**
     * Private constructor prevents instantiation.
     */
    private CMSConstants() {
    }

    public static final String HYPHEN = " - ";
    public static final String DEFAULT_TIME = "00:00";
    public static final String EMPTY_STRING = "";
    public static final String NEW_LINE = "\r\n";
    public static final String LINE_SEPARATOR = "\n";

    public static final Integer ZERO_INT = 0;

    public static final String FILE_ORG_UNDER_SCORE = "org_";
    public static final String FILE_COLLEGE_UNDER_SCORE = "school_";
    public static final String FILE_TXN_UNDER_SCORE = "payments_txn_";
    public static final String FILE_PAYMENTS = "payments";

}