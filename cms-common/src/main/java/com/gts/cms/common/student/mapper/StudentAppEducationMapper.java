package com.gts.cms.common.student.mapper;

import com.gts.cms.common.dto.StudentAppEducationDTO;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.StudentAppEducation;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Component
public class StudentAppEducationMapper implements BaseMapper<StudentAppEducation, StudentAppEducationDTO> {

	@Override
	public StudentAppEducationDTO convertEntityToDTO(StudentAppEducation studentAppEducation) {
		StudentAppEducationDTO studentAppEducationDTO = null;
		if (studentAppEducation != null) {
			studentAppEducationDTO = new StudentAppEducationDTO();
			studentAppEducationDTO.setAppEducationId(studentAppEducation.getAppEducationId());
			studentAppEducationDTO.setBoard(studentAppEducation.getBoard());
			studentAppEducationDTO.setMedium(studentAppEducation.getMedium());
			studentAppEducationDTO.setNameOfInstitution(studentAppEducation.getNameOfInstitution());
			studentAppEducationDTO.setAddress(studentAppEducation.getAddress());
			studentAppEducationDTO.setMajorSubjects(studentAppEducation.getMajorSubjects());
			studentAppEducationDTO.setGradeClassSecured(studentAppEducation.getGradeClassSecured());
			studentAppEducationDTO.setYearOfCompletion(studentAppEducation.getYearOfCompletion());
			if(studentAppEducation.getPrecentage()!=null) {
				studentAppEducationDTO.setPrecentage(studentAppEducation.getPrecentage().doubleValue());	
			}
			studentAppEducationDTO.setIsActive(studentAppEducation.getIsActive());
		}
		return studentAppEducationDTO;
	}

	@Override
	public List<StudentAppEducationDTO> convertEntityListToDTOList(List<StudentAppEducation> studentAppEducationList) {
		List<StudentAppEducationDTO> studentAppEducationDTOList = new ArrayList<>();
		studentAppEducationList.forEach(
				studentAppEducation -> studentAppEducationDTOList.add(convertEntityToDTO(studentAppEducation)));
		return studentAppEducationDTOList;
	}

	@Override
	public List<StudentAppEducation> convertDTOListToEntityList(
			List<StudentAppEducationDTO> studentAppEducationDTOList) {
		List<StudentAppEducation> studentAppEducationList = new ArrayList<>();
		studentAppEducationDTOList.forEach(studentAppEducationDTO -> studentAppEducationList
				.add(convertDTOtoEntity(studentAppEducationDTO, null)));
		return studentAppEducationList;
	}

	@Override
	public StudentAppEducation convertDTOtoEntity(StudentAppEducationDTO studentAppEducationDTO,
			StudentAppEducation studentAppEducation) {
		if (null == studentAppEducation) {
			studentAppEducation = new StudentAppEducation();
			studentAppEducation.setCreatedDt(new Date());
			studentAppEducation.setCreatedUser(SecurityUtil.getCurrentUser());
		}
		studentAppEducation.setAppEducationId(studentAppEducation.getAppEducationId());
		studentAppEducation.setIsActive(studentAppEducationDTO.getIsActive());
		studentAppEducation.setBoard(studentAppEducationDTO.getBoard());
		studentAppEducation.setMedium(studentAppEducationDTO.getMedium());
		studentAppEducation.setNameOfInstitution(studentAppEducation.getNameOfInstitution());
		studentAppEducation.setAddress(studentAppEducationDTO.getAddress());
		studentAppEducation.setMajorSubjects(studentAppEducationDTO.getMajorSubjects());
		studentAppEducation.setGradeClassSecured(studentAppEducationDTO.getGradeClassSecured());
		studentAppEducation.setYearOfCompletion(studentAppEducation.getYearOfCompletion());
		studentAppEducation.setPrecentage(BigDecimal.valueOf(studentAppEducationDTO.getPrecentage()));
		return studentAppEducation;
	}
}
