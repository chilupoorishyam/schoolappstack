package com.gts.cms.common.repository;

import com.gts.cms.entity.AmsPost;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AmsPostRepository extends  JpaRepository<AmsPost, Long>{

}
