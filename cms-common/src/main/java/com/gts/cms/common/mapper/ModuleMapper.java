package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.ModuleDTO;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.Module;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class ModuleMapper implements BaseMapper<Module, ModuleDTO> {
@Override
public ModuleDTO convertEntityToDTO(Module module) {
	ModuleDTO moduleDTO = null;
	if (module != null) {
	moduleDTO = new ModuleDTO();
	  moduleDTO.setCreatedDt(module.getCreatedDt());
	  moduleDTO.setUpdatedDt(module.getUpdatedDt());
	  moduleDTO.setUpdatedUser(module.getUpdatedUser());
	  moduleDTO.setIsActive(module.getIsActive());
	  moduleDTO.setCreatedUser(module.getCreatedUser());
	  moduleDTO.setModuleId(module.getModuleId());
	  moduleDTO.setUrl(module.getUrl());
	  moduleDTO.setModuleName(module.getModuleName());
	  moduleDTO.setReason(module.getReason());
	  moduleDTO.setIconName(module.getIconName());
	  moduleDTO.setSortOrder(module.getSortOrder());
	  moduleDTO.setDisplayName(module.getDisplayName());
	}
	return moduleDTO;
}
@Override
public Module convertDTOtoEntity(ModuleDTO moduleDTO,Module module){
	if ( null == module ) {
	 module = new Module();
	 module.setIsActive(Status.ACTIVE.getId());
	 module.setCreatedDt(new Date());
	 module.setCreatedUser(SecurityUtil.getCurrentUser());
	}else{
	 module.setIsActive(moduleDTO.getIsActive());
	}
	  module.setUpdatedDt(new Date());
	  module.setUpdatedUser(SecurityUtil.getCurrentUser());
	  module.setModuleId(moduleDTO.getModuleId());
	  module.setUrl(moduleDTO.getUrl());
	  module.setModuleName(moduleDTO.getModuleName());
	  module.setReason(moduleDTO.getReason());
	  module.setIconName(moduleDTO.getIconName());
	  module.setSortOrder(moduleDTO.getSortOrder());
	  module.setDisplayName(moduleDTO.getDisplayName());
	return module;
}
@Override
public List<Module> convertDTOListToEntityList(List<ModuleDTO> moduleDTOList) {
	List<Module> moduleList = new ArrayList<>();
	 moduleDTOList.forEach(moduleDTO -> moduleList.add(convertDTOtoEntity(moduleDTO, null)));
	return moduleList;
}
@Override
public List<ModuleDTO> convertEntityListToDTOList(List<Module> moduleList) {
	List<ModuleDTO> moduleDTOList = new ArrayList<>();
	 moduleList.forEach(module -> moduleDTOList.add(convertEntityToDTO(module)));
	return moduleDTOList;
}
}