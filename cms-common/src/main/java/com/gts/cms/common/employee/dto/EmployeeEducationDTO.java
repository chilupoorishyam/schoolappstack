package com.gts.cms.common.employee.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Genesis
 *
 */
public class EmployeeEducationDTO {

	private static final long serialVersionUID = 1L;
	
	private Long empEducationId;

	private String address;

	private String board;

	private Date createdDt;

	private Long createdUser;

	private String gradeClassSecured;

	private Boolean isActive;

	private String majorSubjects;

	private String medium;

	private String nameOfInstitution;

	private BigDecimal precentage;

	private String reason;

	private String yearOfCompletion;

	private Long employeeId;

	private Long modeofstudy;

	public Long getEmpEducationId() {
		return empEducationId;
	}

	public void setEmpEducationId(Long empEducationId) {
		this.empEducationId = empEducationId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getGradeClassSecured() {
		return gradeClassSecured;
	}

	public void setGradeClassSecured(String gradeClassSecured) {
		this.gradeClassSecured = gradeClassSecured;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getMajorSubjects() {
		return majorSubjects;
	}

	public void setMajorSubjects(String majorSubjects) {
		this.majorSubjects = majorSubjects;
	}

	public String getMedium() {
		return medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

	public String getNameOfInstitution() {
		return nameOfInstitution;
	}

	public void setNameOfInstitution(String nameOfInstitution) {
		this.nameOfInstitution = nameOfInstitution;
	}

	public BigDecimal getPrecentage() {
		return precentage;
	}

	public void setPrecentage(BigDecimal precentage) {
		this.precentage = precentage;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getYearOfCompletion() {
		return yearOfCompletion;
	}

	public void setYearOfCompletion(String yearOfCompletion) {
		this.yearOfCompletion = yearOfCompletion;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Long getModeofstudy() {
		return modeofstudy;
	}

	public void setModeofstudy(Long modeofstudy) {
		this.modeofstudy = modeofstudy;
	}
	
	

}
