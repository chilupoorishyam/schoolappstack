
package com.gts.cms.common.student.service.impl;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.StudentAppDocCollectionDTO;
import com.gts.cms.common.dto.StudentAppWorkflowDTO;
import com.gts.cms.common.enums.*;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.repository.*;
import com.gts.cms.common.service.FileService;
import com.gts.cms.common.student.dto.ApplicationDTO;
import com.gts.cms.common.student.dto.StudentApplicationDTO;
import com.gts.cms.common.student.dto.StudentApplicationImagesDTO;
import com.gts.cms.common.student.dto.StudentDocumentCollectionDTO;
import com.gts.cms.common.student.mapper.StudentAppWorkflowMapper;
import com.gts.cms.common.student.mapper.StudentApplicationMapper;
import com.gts.cms.common.student.mapper.StudentDetailMapper;
import com.gts.cms.common.student.repository.*;
import com.gts.cms.common.student.service.StudentDetailService;
import com.gts.cms.common.student.service.StudentService;
import com.gts.cms.common.util.FileUtil;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.emailandsms.service.SmsService;
import com.gts.cms.entity.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;
import java.util.Map.Entry;

@Service
public class StudentServiceImpl implements StudentService {
	private static final Logger LOGGER = Logger.getLogger(StudentServiceImpl.class);
	private static final String FILE_ORG = "org_";
	private static final String FILE_COLLEGE = "school_";
	private static final String FILE_APP = "std_";

	private static final String FILE_AADHAR = "aadhar.";
	private static final String FILE_PAN = "pan.";
	private static final String FILE_PASSPORT = "passport.";
	private static final String FILE_PHOTO = "photo.";
	private static final String FILE_DOCUMENTS = "documents";

	private static final String fileSeparator = FileUtil.getFileSeparator();

	@Autowired
	private StudentApplicationMapper studentApplicationMapper;

	@Autowired
	StudentApplicationRepository studentApplicationRepository;

	@Autowired
	private StudentDetailMapper studentDetailMapper;
	@Autowired
	StudentAppWorkflowMapper studentAppWorkflowMapper;

	@Autowired
	StudentAppWorkflowRepository studentAppWorkflowRepository;

	@Autowired
	StudentAppDocumentCollectionRepository studentAppDocumentCollectionRepository;

	@Autowired
	private StudentDocumentCollectionRepository studentDocumentCollectionRepository;
	@Autowired
	FileService fileService;
	@Autowired
	private WorkflowStageRepository workflowStageRepository;

	@Autowired
	private ConfigAutonumberRepository configAutoNumberRepository;

	@Autowired
	private UserRoleRepository userRoleRepository;

	@Autowired
	private StudentDetailRepository studentDetailRepository;
	
	@Autowired
	private StudentDetailService studentDetailService;

	/*@Autowired
	FeeStudentDataService feeStudentDataService;

	@Autowired
	FeeStudentDataMapper feeStudentDataMapper;
*/
	/*@Autowired
	FeeStructureRepository feeStructureRepository;
*/
	@Autowired
	GeneralDetailRepository generalDetailRepository;

	@Autowired
	private StudentAcademicBatchesRepository studentAcademicBatchesRepository;

	/*@Autowired
	FeeStudentDataParticularService feeStudentDataParticularService;

	@Autowired
	FeeStudentDataRepository feeStudentDataRepository;

	@Autowired
	FeeStudentDataParticularMapper feeStudentDataParticularMapper;

	@Autowired
	FeeStudentDataParticularRepository feeStudentDataParticularRepository;
*/
	@Autowired
	SmsService smsService;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	StudentDocumentCollectionRepository stdDocCollectionRepository;

	public String getApplicationNo(Long schoolId) {
		String applicationNumber = null;
		try {
			ConfigAutonumber configAutoNumber = configAutoNumberRepository.findSchoolConfigAutoNumbers(schoolId,
					ConfigAutoNumbersCodeEnum.STD_APP.getId());
			if (configAutoNumber != null && configAutoNumber.getPrefix() != null && configAutoNumber.getSuffix() != null
					&& configAutoNumber.getCurrentNumber() != null) {
				applicationNumber = configAutoNumber.getPrefix() + configAutoNumber.getSuffix()
						+ configAutoNumber.getCurrentNumber();
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occured while gettingApplicationNo : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}
		return applicationNumber;
	}
	/*
	 * 27-04-2020
	 * regulation added 
	 */
	@Override
	@Transactional
	public ApiResponse<?> addStudentApplicationForm(StudentApplicationDTO studentApplicationDTO) {

		ApiResponse<Object> response = null;
		
		try {
			List<StudentApplication> list = studentApplicationRepository.isStudentDuplicate(studentApplicationDTO.getFirstName(), studentApplicationDTO.getFatherName());
			if (CollectionUtils.isEmpty(list)) {
				StudentApplication studentApplication = studentApplicationMapper
						.convertStudentApplicationDTOToStudentApplication(studentApplicationDTO, true);
			/*if (studentApplication.getQuota() != null && studentApplication.getQuota().getGeneralDetailCode() != null
					&& studentApplication.getQuota().getGeneralDetailCode().equals("MGMT")) {*/
				LOGGER.info("StudentServiceImpl.addStudentApplicationForm()");
				Long currentNumber = null;
				ConfigAutonumber configAutoNumber = configAutoNumberRepository.findSchoolConfigAutoNumbers(
						studentApplicationDTO.getSchoolId(), ConfigAutoNumbersCodeEnum.STD_APP.getId());
				String applicationNumber = null;
				if (configAutoNumber != null && configAutoNumber.getAutoconfigId() != null
						&& configAutoNumber.getPrefix() != null && configAutoNumber.getSuffix() != null
						&& configAutoNumber.getCurrentNumber() != null) {
					LOGGER.info("StudentServiceImpl.addStudentApplicationForm()" + configAutoNumber + configAutoNumber.getAutoconfigId());
					currentNumber = Long.parseLong(configAutoNumber.getCurrentNumber()) + 1;
					if (configAutoNumber.getSchool() != null) {
						applicationNumber = configAutoNumber.getSchool().getSchoolCode() + "/" +  configAutoNumber.getPrefix() + "/" + currentNumber + "/" + configAutoNumber.getSuffix();
					} else {
						applicationNumber = configAutoNumber.getPrefix() + "/" + currentNumber + "/" + configAutoNumber.getSuffix();
					}
					configAutoNumber.setCurrentNumber(currentNumber.toString());
					configAutoNumber.setUpdatedDt(new Date());
					configAutoNumber.setUpdatedUser(SecurityUtil.getCurrentUser());

				}
				if (applicationNumber != null) {
					LOGGER.info("StudentServiceImpl.addStudentApplicationForm()" + "if(applicationNumber!=null)" + applicationNumber);
					studentApplication.setApplicationNo(applicationNumber);
					setStudentAppWorkflows(studentApplicationDTO, studentApplication, true);
					studentApplication = studentApplicationRepository.save(studentApplication);
					studentApplicationRepository.flush();
					try {
						Long count = configAutoNumberRepository.updateCurrentNumber(configAutoNumber);
						LOGGER.info("StudentServiceImpl.addStudentApplicationForm()" + count);
					} catch (Exception e) {

						LOGGER.error("Exception Occured while updating currentnumber in ConfigAutoNumber  : " + e);
					}
					if (studentApplication != null && studentApplication.getStudentAppId() != null
							&& studentApplication.getWorkflowStage() != null && WorkFlowNameEnum.ADMIT.name()
							.equals(studentApplication.getWorkflowStage().getWfName())) {

						StudentDetail studentDetail = studentDetailMapper
								.convertStudentApplicationToStudentDetails(studentApplication, true);
						String status = StudentDetailEnum.INCOLLEGE.getValue();
						GeneralDetail generalDetail = generalDetailRepository.getGeneralDetailByCode(status);
						studentDetail.setStudentStatus(generalDetail);

						configAutoNumber = configAutoNumberRepository.findSchoolConfigAutoNumbers(
								studentApplication.getSchool().getSchoolId(),
								ConfigAutoNumbersCodeEnum.STD_ADMISSION.getId());
						if (configAutoNumber != null && configAutoNumber.getAutoconfigId() != null
								&& configAutoNumber.getPrefix() != null && configAutoNumber.getSuffix() != null
								&& configAutoNumber.getCurrentNumber() != null) {
							currentNumber = Long.parseLong(configAutoNumber.getCurrentNumber()) + 1;
							String admissionNumber = configAutoNumber.getPrefix() + currentNumber
									+ configAutoNumber.getSuffix();
							configAutoNumber.setCurrentNumber(currentNumber.toString());
							configAutoNumber.setUpdatedDt(new Date());
							configAutoNumber.setUpdatedUser(SecurityUtil.getCurrentUser());
							studentDetail.setAdminssionDate(new Date());
							studentDetail.setAdmissionNumber(admissionNumber);
						}
						if (studentDetail.getStudentStatus() != null) {
							LOGGER.info("StudentStatus  " + studentDetail.getStudentStatus().getGeneralDetailCode());
						}
						studentDetail = studentDetailRepository.save(studentDetail);
						studentDetailRepository.flush();

						// adding student academic batch details
						StudentAcademicbatch studentAcademicBatch = new StudentAcademicbatch();
						studentAcademicBatch.setSchool(studentDetail.getSchool());
						studentAcademicBatch.setStudentDetail(studentDetail);
						studentAcademicBatch.setCourse(studentDetail.getCourse());
						studentAcademicBatch.setAcademicYear(studentDetail.getAcademicYear());
						studentAcademicBatch.setFromCourseYear(studentDetail.getCourseYear());
						studentAcademicBatch.setFromBatch(studentDetail.getBatch());
						studentAcademicBatch.setToBatch(studentDetail.getBatch());
						studentAcademicBatch.setFromGroupSection(studentDetail.getGroupSection());
						studentAcademicBatch.setIsActive(Boolean.TRUE);
						studentAcademicBatch.setCreatedUser(SecurityUtil.getCurrentUser());
						studentAcademicBatch.setStudentStatus(studentDetail.getStudentStatus());
						Calendar calendar = Calendar.getInstance();
						Date date = new Date();
						calendar.setTime(date);
						//calendar.set(Calendar.HOUR, 0);
						calendar.set(Calendar.MINUTE, 0);
						calendar.set(Calendar.SECOND, 0);
						calendar.set(Calendar.HOUR_OF_DAY, 0);
						studentAcademicBatch.setFromDate(calendar.getTime());
						//studentAcademicBatch.setFromDate(new Date());
						studentAcademicBatch.setCreatedDt(new Date());
						studentAcademicBatch.setCreatedUser(SecurityUtil.getCurrentUser());



						studentAcademicBatchesRepository.save(studentAcademicBatch);
						studentAcademicBatchesRepository.flush();

						// end of student academic batch
						// TODO call service feestudent data
						// Long schoolId, Long academicYearId, Long courseYearID, Long
						// groupSectionId,Long quotaId, Long studentId
						/*
						 * feeStudentDataService.saveAdmitStudentFeeDetail(studentDetail.getSchool().
						 * getSchoolId(), studentDetail.getAcademicYear().getAcademicYearId(),
						 * studentDetail.getCourseYear().getCourseYearId(), null,
						 * studentDetail.getQuota().getGeneralDetailId(), studentDetail.getStudentId());
						 */
						/*List<FeeStudentDataParticularDTO> feeStudentDataParticularDTOs = new ArrayList<>();
						List<FeeStudentDataDTO> feeStudentDataDTOs = new ArrayList<>();
						List<FeeStudentDataParticular> feeStudentDataParticular = null;
						List<FeeStudentData> feeStudentDatas = null;*/
						/*feeStudentDataService.saveAdmitStudentFeeDetail(studentDetail.getSchool().getSchoolId(),
								studentDetail.getAcademicYear().getAcademicYearId(),
								studentDetail.getCourseYear().getCourseYearId(), null,
								studentDetail.getQuota().getGeneralDetailId(), studentDetail.getStudentId(),
								feeStudentDataParticularDTOs, feeStudentDataDTOs);

						if (!CollectionUtils.isEmpty(feeStudentDataDTOs)) {
							feeStudentDatas = feeStudentDataMapper.convertDTOListToEntityList(feeStudentDataDTOs);

							feeStudentDatas = feeStudentDataRepository.saveAll(feeStudentDatas);
							feeStudentDataRepository.flush();
						}
						if (!CollectionUtils.isEmpty(feeStudentDataParticularDTOs)) {
							feeStudentDataParticular = feeStudentDataParticularMapper
									.convertDTOListToEntityList(feeStudentDataParticularDTOs);

							feeStudentDataParticular = feeStudentDataParticularRepository
									.saveAll(feeStudentDataParticular);
							feeStudentDataParticularRepository.flush();
						}*/

						try {
							Long count = studentApplicationRepository.updateStudentAdmissionNumber(
									studentApplication.getStudentAppId(), studentDetail.getAdminssionDate(),
									studentDetail.getAdmissionNumber());
							count = configAutoNumberRepository.updateCurrentNumber(configAutoNumber);
						} catch (Exception e) {
							LOGGER.error("Exception Occured while updating currentnumber in ConfigAutoNumber  : " + e);
						}
						if (studentDetail != null && studentDetail.getAdmissionNumber() != null) {
							ApplicationDTO applicationDTO = new ApplicationDTO();
							applicationDTO.setStudentAppId(studentApplication.getStudentAppId());
							applicationDTO.setAdmissionNumber(studentDetail.getAdmissionNumber());
							applicationDTO.setAdmissionId(studentDetail.getStudentId());
							applicationDTO.setApplicationNumber(studentDetail.getApplicationNo());
							List<StudentAppDocCollection> studentAppDocCollections = studentApplication
									.getStdAppDocCollections();
							List<StudentAppDocCollectionDTO> studentAppDocCollectionDTOs = new ArrayList<>();
							setStudentAppDocCollectionDTOs(studentAppDocCollections, studentAppDocCollectionDTOs);
							applicationDTO.setStudentAppDocCollectionDTO(studentAppDocCollectionDTOs);
							List<StudentDocumentCollection> studentDocumentCollections = studentDetail
									.getStdDocumentCollections();
							List<StudentDocumentCollectionDTO> studentDocumentCollectionDTOs = new ArrayList<>();

							setStudentDocumentCollectionDTOs(studentDocumentCollections, studentDocumentCollectionDTOs);
							applicationDTO.setStudentDocumentCollectionDTO(studentDocumentCollectionDTOs);

							response = new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(), applicationDTO);
						}

					} else {

						ApplicationDTO applicationDTO = new ApplicationDTO();

						applicationDTO.setStudentAppId(studentApplication.getStudentAppId());
						applicationDTO.setApplicationNumber(studentApplication.getApplicationNo());
						List<StudentAppDocCollection> studentAppDocCollections = studentApplication
								.getStdAppDocCollections();
						List<StudentAppDocCollectionDTO> studentAppDocCollectionDTOs = new ArrayList<>();
						setStudentAppDocCollectionDTOs(studentAppDocCollections, studentAppDocCollectionDTOs);
						applicationDTO.setStudentAppDocCollectionDTO(studentAppDocCollectionDTOs);
						response = new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(), applicationDTO,
								HttpStatus.OK.value());
					}

				} else {
					throw new ApiException("Unable to generate ApplicationNumber");
				}
			} else {
				return new ApiResponse<>(false, "(" +
						studentApplicationDTO.getFirstName() + " and "
						+ studentApplicationDTO.getFatherName() + ") is Duplicate!", null, HttpStatus.OK.value());
			}
	/*		} else {
				Long currentNumber = null;
				ConfigAutonumber configAutoNumber = configAutoNumberRepository.findSchoolConfigAutoNumbers(
						studentApplicationDTO.getSchoolId(), ConfigAutoNumbersCodeEnum.STD_APP.getId());
				LOGGER.info("ConfigAutonumber==>"+configAutoNumber);
				String applicationNumber = null;
				if (configAutoNumber != null && configAutoNumber.getAutoconfigId() != null
						&& configAutoNumber.getPrefix() != null && configAutoNumber.getSuffix() != null
						&& configAutoNumber.getCurrentNumber() != null) {
					currentNumber = Long.parseLong(configAutoNumber.getCurrentNumber()) + 1;
					if(configAutoNumber.getSchool() != null) {
						applicationNumber = configAutoNumber.getSchool().getSchoolCode() + configAutoNumber.getPrefix() + currentNumber + configAutoNumber.getSuffix();
					}else {
						applicationNumber = configAutoNumber.getPrefix() + currentNumber + configAutoNumber.getSuffix();
					}
					//applicationNumber = configAutoNumber.getPrefix() + currentNumber + configAutoNumber.getSuffix();
					configAutoNumber.setCurrentNumber(currentNumber.toString());
					configAutoNumber.setUpdatedDt(new Date());
					configAutoNumber.setUpdatedUser(SecurityUtil.getCurrentUser());

				}
				studentApplication.setApplicationNo(applicationNumber);
				setStudentAppWorkflows(studentApplicationDTO, studentApplication, true);

				studentApplication = studentApplicationRepository.save(studentApplication);
				studentApplicationRepository.flush();
				if (studentApplication != null && studentApplication.getStudentAppId() != null) {
					try {
						Long count = configAutoNumberRepository.updateCurrentNumber(configAutoNumber);
					} catch (Exception e) {
						LOGGER.error("Error Whileupdating current Number in AutoConfig ");
						throw new ApiException("Configurate autoNumber not updated successfully");
					}

					
					 * smsService.sendApplicationFormSms(studentApplication);
					 

					ApplicationDTO applicationDTO = new ApplicationDTO();

					applicationDTO.setStudentAppId(studentApplication.getStudentAppId());
					applicationDTO.setApplicationNumber(studentApplication.getApplicationNo());
					List<StudentAppDocCollection> studentAppDocCollections = studentApplication
							.getStdAppDocCollections();
					List<StudentAppDocCollectionDTO> studentAppDocCollectionDTOs = new ArrayList<>();
					setStudentAppDocCollectionDTOs(studentAppDocCollections, studentAppDocCollectionDTOs);
					applicationDTO.setStudentAppDocCollectionDTO(studentAppDocCollectionDTOs);
					response = new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(), applicationDTO,
							HttpStatus.OK.value());
					LOGGER.info("StudentServiceImpl.addStudentApplicationForm()==exit");
				}

			}*/
		} catch (Exception e) {
			LOGGER.error("Exception Occured while adding student : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;

	}

	private void setStudentDocumentCollectionDTOs(List<StudentDocumentCollection> studentDocumentCollections,
			List<StudentDocumentCollectionDTO> studentDocumentCollectionDTOs) {
		if (!CollectionUtils.isEmpty(studentDocumentCollections)) {
			for (StudentDocumentCollection studentDocumentCollection : studentDocumentCollections) {
				StudentDocumentCollectionDTO studentDocumentCollectionDTO = new StudentDocumentCollectionDTO();
				studentDocumentCollectionDTO.setAppDocCollId(studentDocumentCollection.getStudentDocCollId());
				studentDocumentCollectionDTO.setDocumentRepositoryId(
						studentDocumentCollection.getDocumentRepository().getDocumentRepositoryId());
				studentDocumentCollectionDTO.setFileName(studentDocumentCollection.getFileName());
				studentDocumentCollectionDTO.setIsSoftCopy(studentDocumentCollection.getIsSoftCopy());
				studentDocumentCollectionDTOs.add(studentDocumentCollectionDTO);
			}
		}

	}

	private void setStudentAppDocCollectionDTOs(List<StudentAppDocCollection> studentAppDocCollections,
			List<StudentAppDocCollectionDTO> studentAppDocCollectionDTOs) {
		if (!CollectionUtils.isEmpty(studentAppDocCollections)) {
			for (StudentAppDocCollection studentAppDocCollection : studentAppDocCollections) {
				StudentAppDocCollectionDTO studentAppDocCollectionDTO = new StudentAppDocCollectionDTO();
				studentAppDocCollectionDTO.setAppDocCollId(studentAppDocCollection.getAppDocCollId());
				studentAppDocCollectionDTO.setDocumentRepositoryId(
						studentAppDocCollection.getDocumentRepository().getDocumentRepositoryId());
				studentAppDocCollectionDTO.setFileName(studentAppDocCollection.getFileName());
				studentAppDocCollectionDTO.setIsSoftCopy(studentAppDocCollection.getIsSoftCopy());
				studentAppDocCollectionDTOs.add(studentAppDocCollectionDTO);
			}
		}

	}

	@Override
	@Transactional
	public ApiResponse<StudentApplicationImagesDTO> uploadStudentApplicationFiles(
			StudentApplicationImagesDTO studentApplicationImages) {
		ApiResponse<StudentApplicationImagesDTO> response = null;
		try {
			if (null == studentApplicationImages || null == studentApplicationImages.getApplicationNumber()) {
				throw new ApiException(Messages.RECORD_ID_MUST_NOT_NULL.getValue());
			}

			if (studentApplicationImages.getFiles() == null) {
				throw new ApiException(Messages.NO_FILES_FOUND.getValue());
			}
			String studentAadharFileName = null;
			String studentPancardFileName = null;
			String studentPhotoFileName = null;
			String fatherPhotoFileName = null;
			String motherPhotoFileName = null;
			String spousePhotoFileName = null;

			MultipartFile studentAadhar = studentApplicationImages.getFiles().get("studentAadhar");
			MultipartFile studentPanCard = studentApplicationImages.getFiles().get("studentPanCard");
			MultipartFile studentPhoto = studentApplicationImages.getFiles().get("studentPhoto");
			MultipartFile fatherPhoto = studentApplicationImages.getFiles().get("fatherPhoto");
			MultipartFile motherPhoto = studentApplicationImages.getFiles().get("motherPhoto");
			MultipartFile spousePhoto = studentApplicationImages.getFiles().get("spousePhoto");

			String orgCode = SecurityUtil.getOrgCode();
			String schoolCode = SecurityUtil.getSchoolCode();
			if (studentAadhar != null) {
				try {
					LOGGER.info("Ented into std_aadhar" + studentAadhar);
					studentAadharFileName = fileService.uploadFile(studentAadhar,
							"org_" + orgCode + fileSeparator + "school_" + schoolCode + fileSeparator + "std_"
									+ studentApplicationImages.getApplicationNumber() + fileSeparator + "std_aadhar."
									+ FilenameUtils.getExtension(studentAadhar.getOriginalFilename()));
					studentApplicationImages.setStudentAadharFileStatus(Status.SUCESS.getName());
					LOGGER.info("Ented into std_aadhar" + studentAadharFileName);
				} catch (Exception e) {
					studentApplicationImages.setStudentAadharFileStatus("Failed to upload Student Aadhar");
					LOGGER.info("Error While uploading aadhar : " + studentAadhar);
				}
			}

			if (studentPanCard != null) {
				try {
					studentPancardFileName = fileService.uploadFile(studentPanCard,
							"org_" + orgCode + fileSeparator + "school_" + schoolCode + fileSeparator + "std_"
									+ studentApplicationImages.getApplicationNumber() + fileSeparator + "std_pan."
									+ FilenameUtils.getExtension(studentPanCard.getOriginalFilename()));
					studentApplicationImages.setStudentPanCardFileStatus(Status.SUCESS.getName());
				} catch (Exception e) {
					LOGGER.error("Error occured in pan card upload"+e);
					studentApplicationImages.setStudentPanCardFileStatus("Failed to upload Student Pancard");
				}
			}

			if (studentPhoto != null) {
				try {
					studentPhotoFileName = fileService.uploadFile(studentPhoto,
							"org_" + orgCode + fileSeparator + "school_" + schoolCode + fileSeparator + "std_"
									+ studentApplicationImages.getApplicationNumber() + fileSeparator + "std_photo."
									+ FilenameUtils.getExtension(studentPhoto.getOriginalFilename()));
					/*
					 * studentPhotoFileName = fileService.uploadFile(studentPhoto, "org_" + orgCode
					 * + "/" + "school_" + schoolCode + "/" + "std_" +
					 * studentApplicationImages.getApplicationNumber() + "/" + "std_photo." +
					 * FilenameUtils.getExtension( studentPhoto.getOriginalFilename()));
					 */
					studentApplicationImages.setStudentPhotoFileStatus(Status.SUCESS.getName());
				} catch (Exception e) {
					studentApplicationImages.setStudentPanCardFileStatus("Failed to upload Student Photo ");
				}
			}

			if (fatherPhoto != null) {
				try {
					fatherPhotoFileName = fileService.uploadFile(fatherPhoto,
							"org_" + orgCode + fileSeparator + "school_" + schoolCode + fileSeparator + "std_"
									+ studentApplicationImages.getApplicationNumber() + fileSeparator
									+ "std_father_photo."
									+ FilenameUtils.getExtension(fatherPhoto.getOriginalFilename()));
					studentApplicationImages.setFatherPhotoFileStatus(Status.SUCESS.getName());
				} catch (Exception e) {
					
					studentApplicationImages.setFatherPhotoFileStatus("Failed to upload Father Photo ");
				}
			}

			if (motherPhoto != null) {
				try {
					motherPhotoFileName = fileService.uploadFile(motherPhoto,
							"org_" + orgCode + fileSeparator + "school_" + schoolCode + fileSeparator + "std_"
									+ studentApplicationImages.getApplicationNumber() + fileSeparator
									+ "std_mother_photo."
									+ FilenameUtils.getExtension(motherPhoto.getOriginalFilename()));
					studentApplicationImages.setMotherPhotoFileStatus(Status.SUCESS.getName());
				} catch (Exception e) {
			
					studentApplicationImages.setMotherPhotoFileStatus("Failed to upload Mother Photo ");
				}
			}

			if (spousePhoto != null) {
				try {
					spousePhotoFileName = fileService.uploadFile(spousePhoto,
							FILE_ORG + orgCode + fileSeparator + FILE_COLLEGE + schoolCode + fileSeparator + FILE_APP
									+ studentApplicationImages.getApplicationNumber() + fileSeparator
									+ "std_spouse_photo."
									+ FilenameUtils.getExtension(spousePhoto.getOriginalFilename()));
					studentApplicationImages.setSpousePhotoFileStatus(Status.SUCESS.getName());
				} catch (Exception e) {
					LOGGER.error("Error While uploading Spouse Photo : " + spousePhoto);
					studentApplicationImages.setSpousePhotoFileStatus("Failed to upload Spouse Photo ");
				}
			}

			if (studentAadharFileName != null || studentPancardFileName != null || studentPhotoFileName != null
					|| motherPhotoFileName != null || fatherPhotoFileName != null || spousePhotoFileName != null) {

				try {
					Long updatedCount = studentApplicationRepository.updateStudentApplicationPhotFileNames(
							studentApplicationImages.getApplicationId(), studentAadharFileName, studentPancardFileName,
							studentPhotoFileName, fatherPhotoFileName, motherPhotoFileName);

					if (updatedCount != null && updatedCount != 0) {
						if (studentApplicationImages.getAdmissionId() != null
								&& studentApplicationImages.getAdmissionId() != 0) {
							updatedCount = studentDetailRepository.updateStudentAdmissionPhotFileNames(
									studentApplicationImages.getAdmissionId(), studentAadharFileName,
									studentPancardFileName, studentPhotoFileName, fatherPhotoFileName,
									motherPhotoFileName, spousePhotoFileName);
						}

						response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), studentApplicationImages,
								HttpStatus.OK.value());
					} else {
						response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), studentApplicationImages,
								HttpStatus.OK.value());
					}
				} catch (Exception e) {
					response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), studentApplicationImages,
							HttpStatus.OK.value());
				}
			} else {
				response = new ApiResponse<>(Messages.FAILED_TO_UPLOAD_IMAGES.getValue(), studentApplicationImages,
						HttpStatus.OK.value());
			}
			String docPath = null;
			try {
				Map<String, MultipartFile> files = studentApplicationImages.getFiles();
				for (Entry<String, MultipartFile> entry : files.entrySet()) {
					String docRepId = entry.getKey();
					if (docRepId.matches("[0-9]+")) {
						MultipartFile appFile = entry.getValue();
						try {
							docPath = fileService.uploadFile(appFile,
									FILE_ORG + orgCode + fileSeparator + FILE_COLLEGE + schoolCode + fileSeparator
											+ FILE_APP + studentApplicationImages.getApplicationNumber() + fileSeparator
											+ "std_documents" + fileSeparator + docRepId + "."
											+ FilenameUtils.getExtension(entry.getValue().getOriginalFilename()));
							studentApplicationImages.setStudentDocumentsStatus("SUCCESS");
							if (docPath != null) {
								Long updatedCount = studentAppDocumentCollectionRepository.updateStudentDocumentFiles(
										studentApplicationImages.getApplicationId(), docRepId, docPath);
								if (updatedCount == null || updatedCount == 0) {

									response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(),
											studentApplicationImages, HttpStatus.OK.value());

								} else {

									updatedCount = studentAppDocumentCollectionRepository.updateStudentDocumentFiles(
											studentApplicationImages.getApplicationId(), docRepId, docPath);

									response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(),
											studentApplicationImages, HttpStatus.OK.value());
								}

							} else {
								response = new ApiResponse<>(Messages.FAILED_TO_UPLOAD_DOCUMENTS.getValue(),
										studentApplicationImages, HttpStatus.OK.value());
							}
						} catch (Exception e) {
							studentApplicationImages.setFiles(null);
							studentApplicationImages.setStudentDocumentsStatus("Failed to upload Student Documents");
							response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), studentApplicationImages,
									HttpStatus.INTERNAL_SERVER_ERROR.value());
						}
					}
				}

			} catch (Exception e) {
				studentApplicationImages.setFiles(null);
				studentApplicationImages.setStudentDocumentsStatus("Failed to upload Student Documents");
				response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), studentApplicationImages,
						HttpStatus.INTERNAL_SERVER_ERROR.value());
			}

		} catch (Exception e) {
			studentApplicationImages.setFiles(null);
			response = new ApiResponse<>(e.getMessage(), studentApplicationImages,
					HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		studentApplicationImages.setFiles(null);
		//studentDetailRepository.flush();
		studentApplicationRepository.flush();
		updateStudentDetails(studentApplicationImages);
		return response;
	}

	@Override

	public ApiResponse<List<StudentApplicationDTO>> getStudentApplicationForm(String studentData, Long schoolId) {
		ApiResponse<List<StudentApplicationDTO>> response = null;
		try {
			List<StudentApplication> studentApplicationList = studentApplicationRepository
					.findByStudentData(studentData,schoolId);
			if (!CollectionUtils.isEmpty(studentApplicationList)) {
				List<StudentApplicationDTO> studentApplicationListDto = studentApplicationMapper
						.convertStudentApplicationListToStudentApplicationDTOList(studentApplicationList);
				response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), studentApplicationListDto);
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
						HttpStatus.OK.value());
			}
		} catch (Exception e) {
			
			LOGGER.error("Exception Occured while getting StudentApplication : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;
	}

	@Override
	public ApiResponse<List<StudentApplicationDTO>> getAllApplicationForms(String status) {
		ApiResponse<List<StudentApplicationDTO>> response = null;
		Long userId = SecurityUtil.getCurrentUser();
		User user= userRepository.findById(userId).get();
		//UserRole userRole = userRoleRepository.getOne(userId);
		List<UserRole> roles =  user.getUserRoles();
		boolean hasPermission = false;
		for (UserRole role : roles) {
			if(role.getRole() != null && role.getRole().getRoleName().equals(UserRolesEnum.ROLE_VERIFICATION_OFFICER.getRoleName())) {
				hasPermission = true;
			}
		}

		// we need to get role from SecurityUtil.
		// But temporarly am getting the role from DB
		/*
userRole != null && userRole.getRole() != null
				&& userRole.getRole().getRoleName().equals(UserRolesEnum.ROLE_VERIFICATION_OFFICER.getRoleName()
*/
		if (user != null && hasPermission) {
			List<StudentApplication> studentApplicationList = studentApplicationRepository
					.findAllStudentsApplicationForms(SecurityUtil.getOrganizationId(), null,
							WorkFlowStagesEnum.STAGE2.getId());
			if (!CollectionUtils.isEmpty(studentApplicationList)) {
				List<StudentApplicationDTO> studentApplicationListDto = studentApplicationMapper
						.convertStudentApplicationListToStudentApplicationDTOList(studentApplicationList);
				response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), studentApplicationListDto);
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
						HttpStatus.OK.value());
			}
		} else {
			Boolean isActive = null;
			if (status != null) {
				isActive = new Boolean(status);
			}
			List<StudentApplication> studentApplicationList = studentApplicationRepository
					.findAllStudentsApplicationForms(SecurityUtil.getOrganizationId(), isActive);
			if (!CollectionUtils.isEmpty(studentApplicationList)) {
				List<StudentApplicationDTO> studentApplicationListDto = studentApplicationMapper
						.convertStudentApplicationListToStudentApplicationDTOList(studentApplicationList);
				response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), studentApplicationListDto);
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
						HttpStatus.OK.value());
			}
		}
		return response;
	}

	@Override
	@Transactional
	public ApiResponse<?> updateStudentApplicationFormWorkFlow(StudentAppWorkflowDTO studentAppWorkflowDTO) {
		ApiResponse<Object> response = null;
		try {
			StudentAppWorkflow studentAppWorkflow = studentAppWorkflowMapper.convertDTOtoEntity(studentAppWorkflowDTO,
					null);

			// studentApp
			studentAppWorkflow = studentAppWorkflowRepository.save(studentAppWorkflow);
			if (studentAppWorkflow != null && studentAppWorkflow.getStudentAppWorkflowId() != null) {
				ApplicationDTO applicationDTO = new ApplicationDTO();
				applicationDTO.setStudentAppId(studentAppWorkflowDTO.getStudentAppId());
				response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), applicationDTO);
				if (studentAppWorkflow.getToAppStatus() != null
						&& WorkFlowNameEnum.ADMIT.name().equals(studentAppWorkflow.getToAppStatus().getWfName())) {
					Optional<StudentApplication> studentApplicationOp = studentApplicationRepository
							.findById(studentAppWorkflow.getStudentApplication().getStudentAppId());
					if (studentApplicationOp.isPresent()) {
						StudentApplication studentApplication = studentApplicationOp.get();
						StudentDetail studentDetail = studentDetailMapper
								.convertStudentApplicationToStudentDetails(studentApplication, false);

						Long currentNumber = null;
						ConfigAutonumber configAutoNumber = configAutoNumberRepository.findSchoolConfigAutoNumbers(
								studentApplication.getSchool().getSchoolId(),
								ConfigAutoNumbersCodeEnum.STD_ADMISSION.getId());
						if (configAutoNumber != null && configAutoNumber.getAutoconfigId() != null
								&& configAutoNumber.getPrefix() != null && configAutoNumber.getSuffix() != null
								&& configAutoNumber.getCurrentNumber() != null) {
							currentNumber = Long.parseLong(configAutoNumber.getCurrentNumber()) + 1;
							String admissionNumber = configAutoNumber.getPrefix() + currentNumber
									+ configAutoNumber.getSuffix();
							configAutoNumber.setCurrentNumber(currentNumber.toString());
							configAutoNumber.setUpdatedDt(new Date());
							configAutoNumber.setUpdatedUser(SecurityUtil.getCurrentUser());
							studentDetail.setAdmissionNumber(admissionNumber);
							studentDetail.setAdminssionDate(new Date());
						}
						studentDetail = studentDetailRepository.save(studentDetail);
						studentDetailRepository.flush();
						try {
							Long count = studentApplicationRepository.updateStudentAdmissionNumber(
									studentApplication.getStudentAppId(), studentDetail.getAdminssionDate(),
									studentDetail.getAdmissionNumber());

							count = configAutoNumberRepository.updateCurrentNumber(configAutoNumber);
						} catch (Exception e) {
							LOGGER.error("Error occured while updating admission no"+e);
						}
						if (studentDetail != null && studentDetail.getAdmissionNumber() != null) {
							applicationDTO.setStudentAppId(studentAppWorkflowDTO.getStudentAppId());
							applicationDTO.setAdmissionNumber(studentDetail.getAdmissionNumber());
							applicationDTO.setApplicationNumber(studentDetail.getApplicationNo());
							response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), applicationDTO);
						}

					}
				}
			}
		} catch (Exception e) {
		
			LOGGER.error("Exception Occured while updating studentApplicationFormWorkFlow : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}
		return response;
	}

	@Override
	@Transactional
	public ApiResponse<?> updateStudentApplicationForm(StudentApplicationDTO studentApplicationDTO) {
		ApiResponse<ApplicationDTO> response = null;
		try {
			StudentApplication studentApplication = studentApplicationMapper
					.convertStudentApplicationDTOToStudentApplication(studentApplicationDTO, false);
			/*
			 * // fromDate Calendar calendarFromDate = Calendar.getInstance();
			 * calendarFromDate.set(Calendar.HOUR, 0); calendarFromDate.set(Calendar.MINUTE,
			 * 0); calendarFromDate.set(Calendar.SECOND, 0);
			 * calendarFromDate.set(Calendar.HOUR_OF_DAY, 0);
			 * calendarFromDate.add(Calendar.DATE, 0);
			 */
			
			// we need to get SchoolId based on user_id
			if (studentApplication != null) {
				setStudentAppWorkflows(studentApplicationDTO, studentApplication, true);
				studentApplication = studentApplicationRepository.save(studentApplication);
				studentApplicationRepository.flush();
				if (studentApplication != null && studentApplication.getStudentAppId() != null) {
					if (studentApplicationDTO.getCurrentWorkflowStatusName() != null && studentApplicationDTO
							.getCurrentWorkflowStatusName().equals(WorkFlowNameEnum.ADMIT.name())) {
						// studentDetailRepostroty.update(studentDetail);
						StudentDetail studentDetail = studentDetailMapper
								.convertStudentApplicationToStudentDetails(studentApplication, false);
						studentDetailRepository.save(studentDetail);
						studentDetailRepository.flush();
						try {
							Long count = studentApplicationRepository.updateStudentAdmissionNumber(
									studentApplication.getStudentAppId(), studentDetail.getAdminssionDate(),
									studentDetail.getAdmissionNumber());
						} catch (Exception e) {
							//e.printStackTrace();
							LOGGER.error("Exception Occured while updating student admission number  : " + e);
						}
						if (studentDetail != null && studentDetail.getAdmissionNumber() != null) {
							ApplicationDTO applicationDTO = new ApplicationDTO();
							applicationDTO.setStudentAppId(studentApplication.getStudentAppId());
							applicationDTO.setAdmissionNumber(studentDetail.getAdmissionNumber());
							applicationDTO.setAdmissionId(studentDetail.getStudentId());
							applicationDTO.setApplicationNumber(studentDetail.getApplicationNo());
							List<StudentAppDocCollection> studentAppDocCollections = studentApplication
									.getStdAppDocCollections();
							List<StudentAppDocCollectionDTO> studentAppDocCollectionDTOs = new ArrayList<>();
							setStudentAppDocCollectionDTOs(studentAppDocCollections, studentAppDocCollectionDTOs);
							applicationDTO.setStudentAppDocCollectionDTO(studentAppDocCollectionDTOs);
							List<StudentDocumentCollection> studentDocumentCollections = studentDetail
									.getStdDocumentCollections();
							List<StudentDocumentCollectionDTO> studentDocumentCollectionDTOs = new ArrayList<>();

							setStudentDocumentCollectionDTOs(studentDocumentCollections, studentDocumentCollectionDTOs);
							applicationDTO.setStudentDocumentCollectionDTO(studentDocumentCollectionDTOs);

							return response = new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(), applicationDTO);

						}

					} else if (!studentApplicationDTO.getCurrentWorkflowStatusName()
							.equals(WorkFlowNameEnum.ADMIT.name())
							&& studentApplicationDTO.getToAppWorkflowStatusName()
									.equals(WorkFlowNameEnum.ADMIT.name())) {
						StudentDetail studentDetail = studentDetailMapper
								.convertStudentApplicationToStudentDetails(studentApplication, true);
						Long currentNumber = null;
						ConfigAutonumber configAutoNumber = configAutoNumberRepository.findSchoolConfigAutoNumbers(
								studentApplication.getSchool().getSchoolId(),
								ConfigAutoNumbersCodeEnum.STD_ADMISSION.getId());
						if (configAutoNumber != null && configAutoNumber.getAutoconfigId() != null
								&& configAutoNumber.getPrefix() != null && configAutoNumber.getSuffix() != null
								&& configAutoNumber.getCurrentNumber() != null) {
							currentNumber = Long.parseLong(configAutoNumber.getCurrentNumber()) + 1;
							String admissionNumber = configAutoNumber.getPrefix() + currentNumber
									+ configAutoNumber.getSuffix();
							configAutoNumber.setCurrentNumber(currentNumber.toString());
							configAutoNumber.setUpdatedDt(new Date());
							configAutoNumber.setUpdatedUser(SecurityUtil.getCurrentUser());
							studentDetail.setAdminssionDate(new Date());
							studentDetail.setAdmissionNumber(admissionNumber);
						}
						String status = StudentDetailEnum.INCOLLEGE.getValue();
						GeneralDetail generalDetail = generalDetailRepository.getGeneralDetailByCode(status);
						studentDetail.setStudentStatus(generalDetail);

						studentDetail = studentDetailRepository.save(studentDetail);
						studentDetailRepository.flush();
						try {
							Long count = studentApplicationRepository.updateStudentAdmissionNumber(
									studentApplication.getStudentAppId(), studentDetail.getAdminssionDate(),
									studentDetail.getAdmissionNumber());
							count = configAutoNumberRepository.updateCurrentNumber(configAutoNumber);
						} catch (Exception e) {
							LOGGER.error("Exception Occured while updating currentnumber in ConfigAutoNumber  : " + e);
						}
						if (studentDetail != null && studentDetail.getAdmissionNumber() != null) {
							ApplicationDTO applicationDTO = new ApplicationDTO();
							applicationDTO.setStudentAppId(studentApplication.getStudentAppId());
							applicationDTO.setAdmissionNumber(studentDetail.getAdmissionNumber());
							applicationDTO.setAdmissionId(studentDetail.getStudentId());
							applicationDTO.setStudentId(studentDetail.getStudentId());
							applicationDTO.setStudentDetailMotherPhotoPath(studentDetail.getMotherPhotoPath());
							applicationDTO.setStudentDetailFatherPhotoPath(studentDetail.getFatherPhotoPath());
							applicationDTO.setStudentDetailPhotoPath(studentDetail.getStudentPhotoPath());
							applicationDTO.setApplicationNumber(studentDetail.getApplicationNo());
							List<StudentAppDocCollection> studentAppDocCollections = studentApplication
									.getStdAppDocCollections();
							List<StudentAppDocCollectionDTO> studentAppDocCollectionDTOs = new ArrayList<>();
							setStudentAppDocCollectionDTOs(studentAppDocCollections, studentAppDocCollectionDTOs);
							applicationDTO.setStudentAppDocCollectionDTO(studentAppDocCollectionDTOs);
							List<StudentDocumentCollection> studentDocumentCollections = studentDetail
									.getStdDocumentCollections();
							List<StudentDocumentCollectionDTO> studentDocumentCollectionDTOs = new ArrayList<>();

							setStudentDocumentCollectionDTOs(studentDocumentCollections, studentDocumentCollectionDTOs);
							applicationDTO.setStudentDocumentCollectionDTO(studentDocumentCollectionDTOs);
							
							StudentAcademicbatch studentAcademicbatch = new StudentAcademicbatch();
							studentAcademicbatch.setSchool(studentDetail.getSchool());
							studentAcademicbatch.setCourse(studentDetail.getCourse());
							studentAcademicbatch.setStudentDetail(studentDetail);
							studentAcademicbatch.setAcademicYear(studentDetail.getAcademicYear());
							
							studentAcademicbatch.setStudentStatus(generalDetail);
							studentAcademicbatch.setFromBatch(studentDetail.getBatch());
							studentAcademicbatch.setToBatch(studentDetail.getBatch());
							// fromDate
							Calendar calendarFromDate = Calendar.getInstance();
							calendarFromDate.set(Calendar.HOUR, 0);
							calendarFromDate.set(Calendar.MINUTE, 0);
							calendarFromDate.set(Calendar.SECOND, 0);
							calendarFromDate.set(Calendar.HOUR_OF_DAY, 0);
							calendarFromDate.add(Calendar.DATE, 0);
							LOGGER.info("setFromDate()"+calendarFromDate.getTime());
							studentAcademicbatch.setFromDate(calendarFromDate.getTime());
							LOGGER.info("setFromDate()"+calendarFromDate.getTime());
							studentAcademicbatch.setFromCourseYear(studentDetail.getCourseYear());
							studentAcademicbatch.setFromGroupSection(studentDetail.getGroupSection());
							studentAcademicbatch.setIsActive(Boolean.TRUE);
							studentAcademicbatch.setCreatedDt(new Date());
							studentAcademicbatch.setCreatedUser(SecurityUtil.getCurrentUser());
							studentAcademicbatch = studentAcademicBatchesRepository.save(studentAcademicbatch);
							
							studentAcademicBatchesRepository.flush();
							return  new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(), applicationDTO);

						}

					}
					ApplicationDTO applicationDTO = new ApplicationDTO();
					applicationDTO.setStudentAppId(studentApplication.getStudentAppId());
					applicationDTO.setAdmissionNumber(studentApplication.getAdmissionNumber());
					applicationDTO.setApplicationNumber(studentApplication.getApplicationNo());
					List<StudentAppDocCollection> studentAppDocCollections = studentApplication
							.getStdAppDocCollections();
					List<StudentAppDocCollectionDTO> studentAppDocCollectionDTOs = new ArrayList<>();
					setStudentAppDocCollectionDTOs(studentAppDocCollections, studentAppDocCollectionDTOs);
					applicationDTO.setStudentAppDocCollectionDTO(studentAppDocCollectionDTOs);

					return new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), applicationDTO);

				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occured while updateStudentApplicationForm : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;

	}

	private void setStudentAppWorkflows(StudentApplicationDTO studentApplicationDTO,
			StudentApplication studentApplication, boolean isCreate) {
		if (studentApplicationDTO.getCurrentWorkflowStatusId() != null
				&& studentApplicationDTO.getToAppWorkflowStatusId() != null) {
			StudentAppWorkflow appWorkflow = new StudentAppWorkflow();
			appWorkflow.setComments(studentApplicationDTO.getStatusComments());
			appWorkflow.setIsActive(studentApplicationDTO.getIsActive());
			appWorkflow.setIsCurrentStatus(studentApplicationDTO.isCurrentStatus());
			appWorkflow.setCreatedDt(studentApplicationDTO.getCreatedDate());
			appWorkflow.setCreatedUser(studentApplicationDTO.getCreatedUser());
			if (isCreate) {
				appWorkflow.setCreatedDt(new Date());
				appWorkflow.setCreatedUser(SecurityUtil.getCurrentUser());
			}

			appWorkflow.setIsCurrentStatus(studentApplicationDTO.isCurrentStatus());
			appWorkflow.setReason(studentApplicationDTO.getReason());
			appWorkflow.setUpdatedDt(new Date());
			appWorkflow.setUpdatedUser(SecurityUtil.getCurrentUser());
			WorkflowStage toAppStatus = new WorkflowStage();
			toAppStatus.setWorkflowStageId(studentApplicationDTO.getToAppWorkflowStatusId());
			toAppStatus.setWfName(studentApplicationDTO.getToAppWorkflowStatusName());
			appWorkflow.setToAppStatus(toAppStatus);

			WorkflowStage fromAppStatus = new WorkflowStage();
			fromAppStatus.setWorkflowStageId(studentApplicationDTO.getCurrentWorkflowStatusId());
			fromAppStatus.setWfName(studentApplicationDTO.getCurrentWorkflowStatusName());

			appWorkflow.setFromAppStatus(fromAppStatus);
			appWorkflow.setStudentApplication(studentApplication);
			studentApplication.setWorkflowStage(toAppStatus);
			studentApplication.setStdAppWorkflows(Collections.singletonList(appWorkflow));
		}
	}

	@Override
	@Transactional
	public ApiResponse<?> deleteStudentApplicationForm(Long id) {
		ApiResponse<Object> response = null;
		if (null == id) {
			throw new ApiException(Messages.RECORD_ID_MUST_NOT_NULL.getValue());
		}

		try {

			Long deleteCount = studentApplicationRepository.deleteStudentApplicationDetail(id);
			if (deleteCount > 0) {
				response = new ApiResponse<>(Messages.DELETE_SUCCESS.getValue(), id, HttpStatus.OK.value());
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.DELETE_FAILURE.getValue(), HttpStatus.OK.value());
			}

		} catch (Exception e) {
			
			LOGGER.error("Exception Occured while deleting studentApplicationform : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}
		return response;
	}

	@Override
	@Transactional
	public ApiResponse<StudentApplicationImagesDTO> updateStudentDocumentsAndImages(
			StudentApplicationImagesDTO studentApplicationImages) {
		ApiResponse<StudentApplicationImagesDTO> response = null;
		try {
			if (null == studentApplicationImages || null == studentApplicationImages.getApplicationNumber()) {
				throw new ApiException(Messages.RECORD_ID_MUST_NOT_NULL.getValue());
			}

			if (studentApplicationImages.getFiles() == null) {
				throw new ApiException(Messages.NO_FILES_FOUND.getValue());
			}
			String studentAadharFileName = null;
			String studentPancardFileName = null;
			String studentPhotoFileName = null;
			String fatherPhotoFileName = null;
			String motherPhotoFileName = null;
			MultipartFile studentAadhar = studentApplicationImages.getFiles().get("studentAadhar");
			MultipartFile studentPanCard = studentApplicationImages.getFiles().get("studentPanCard");
			MultipartFile studentPhoto = studentApplicationImages.getFiles().get("studentPhoto");
			MultipartFile fatherPhoto = studentApplicationImages.getFiles().get("fatherPhoto");
			MultipartFile motherPhoto = studentApplicationImages.getFiles().get("motherPhoto");
			String orgCode = SecurityUtil.getOrgCode();
			String schoolCode = SecurityUtil.getSchoolCode();
			if (studentAadhar != null) {
				try {
					studentAadharFileName = fileService.uploadFile(studentAadhar,
							"org_" + orgCode + fileSeparator + "school_" + schoolCode + fileSeparator + "std_"
									+ studentApplicationImages.getApplicationNumber() + fileSeparator + "std_aadhar."
									+ FilenameUtils.getExtension(studentAadhar.getOriginalFilename()));
					if (studentAadharFileName != null) {
						response = updateStudentAadharFilePath(studentApplicationImages, studentAadharFileName);
					} else {
						response = new ApiResponse<>(Messages.FAILED_TO_UPLOAD_AADHAR.getValue(),
								studentApplicationImages, HttpStatus.OK.value());
					}

				} catch (Exception e) {
				
					studentApplicationImages.setStudentAadharFileStatus(Messages.FAILED_TO_UPLOAD_AADHAR.getValue());
					LOGGER.info("Error While uploading aadhar : " + studentAadhar);
				}
			}

			if (studentPanCard != null) {
				try {

					studentPancardFileName = fileService.uploadFile(studentPanCard,
							"org_" + orgCode + fileSeparator + "school_" + schoolCode + fileSeparator + "std_"
									+ studentApplicationImages.getApplicationNumber() + fileSeparator + "std_pan."
									+ FilenameUtils.getExtension(studentPanCard.getOriginalFilename()));
					if (studentPancardFileName != null) {
						response = updateStudentPancardFilePath(studentApplicationImages, studentPancardFileName);
					} else {
						response = new ApiResponse<>(Messages.FAILED_TO_UPLOAD_PANCARD.getValue(),
								studentApplicationImages, HttpStatus.OK.value());
					}

				} catch (Exception e) {
					
					studentApplicationImages.setStudentPanCardFileStatus(Messages.FAILED_TO_UPLOAD_PANCARD.getValue());
					LOGGER.info("Error While updating PANCARD : " + studentPancardFileName);
				}
			}

			if (studentPhoto != null) {
				try {

					studentPhotoFileName = fileService.uploadFile(studentPhoto,
							"org_" + orgCode + fileSeparator + "school_" + schoolCode + fileSeparator + "std_"
									+ studentApplicationImages.getApplicationNumber() + fileSeparator + "std_photo."
									+ FilenameUtils.getExtension(studentPhoto.getOriginalFilename()));
					if (studentPhotoFileName != null) {
						response = updateStudentPhotoFilePath(studentApplicationImages, studentPhotoFileName);
					} else {
						response = new ApiResponse<>(Messages.FAILED_TO_UPLOAD_STUDENT_PHOTO.getValue(),
								studentApplicationImages, HttpStatus.OK.value());
					}

				} catch (Exception e) {
				
					studentApplicationImages
							.setStudentPhotoFileStatus(Messages.FAILED_TO_UPLOAD_STUDENT_PHOTO.getValue());
					LOGGER.info("Error While updating student Photo : " + studentPhotoFileName);
				}
			}

			if (fatherPhoto != null) {
				try {

					fatherPhotoFileName = fileService.uploadFile(fatherPhoto,
							"org_" + orgCode + fileSeparator + "school_" + schoolCode + fileSeparator + "std_"
									+ studentApplicationImages.getApplicationNumber() + fileSeparator
									+ "std_father_photo."
									+ FilenameUtils.getExtension(fatherPhoto.getOriginalFilename()));
					if (fatherPhotoFileName != null) {
						response = updateStudentFatherPhotoFilePath(studentApplicationImages, fatherPhotoFileName);
					} else {
						response = new ApiResponse<>(Messages.FAILED_TO_UPLOAD_STUDENT_FATHER_PHOTO.getValue(),
								studentApplicationImages, HttpStatus.OK.value());
					}

				} catch (Exception e) {
					
					studentApplicationImages
							.setFatherPhotoFileStatus(Messages.FAILED_TO_UPLOAD_STUDENT_FATHER_PHOTO.getValue());
					LOGGER.info("Error While updating father Photo : " + fatherPhotoFileName);
				}
			}

			if (motherPhoto != null) {
				try {

					motherPhotoFileName = fileService.uploadFile(motherPhoto,
							"org_" + orgCode + fileSeparator + "school_" + schoolCode + fileSeparator + "std_"
									+ studentApplicationImages.getApplicationNumber() + fileSeparator
									+ "std_mother_photo."
									+ FilenameUtils.getExtension(motherPhoto.getOriginalFilename()));
					if (motherPhotoFileName != null) {
						response = updateStudentMotherPhotoFilePath(studentApplicationImages, motherPhotoFileName);
					} else {
						response = new ApiResponse<>(Messages.FAILED_TO_UPLOAD_STUDENT_MOTHER_PHOTO.getValue(),
								studentApplicationImages, HttpStatus.OK.value());
					}

				} catch (Exception e) {
					studentApplicationImages
							.setMotherPhotoFileStatus(Messages.FAILED_TO_UPLOAD_STUDENT_MOTHER_PHOTO.getValue());
					LOGGER.info("Error While updating mother Photo : " + motherPhotoFileName);
				}
			}

			String docPath = null;
			try {
				Map<String, MultipartFile> files = studentApplicationImages.getFiles();
				for (Entry<String, MultipartFile> entry : files.entrySet()) {
					String docRepId = entry.getKey();
					if (docRepId.matches("[0-9]+")) {
						MultipartFile appFile = entry.getValue();
						try {
							docPath = fileService.uploadFile(appFile,
									FILE_ORG + orgCode + fileSeparator + FILE_COLLEGE + schoolCode + fileSeparator
											+ FILE_APP + studentApplicationImages.getApplicationNumber() + fileSeparator
											+ "std_documents" + fileSeparator + docRepId + "."
											+ FilenameUtils.getExtension(entry.getValue().getOriginalFilename()));
							if (docPath != null && docRepId != null
									&& studentApplicationImages.getApplicationId() != null) {
								Long updatedCount = studentAppDocumentCollectionRepository.updateStudentDocumentFiles(
										studentApplicationImages.getApplicationId(), docRepId, docPath);
								if (updatedCount == null || updatedCount == 0) {
									studentApplicationImages.setStudentDocumentsStatus("SUCCESS");

									response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(),
											studentApplicationImages, HttpStatus.OK.value());

								} else {
									if (studentApplicationImages.getAdmissionId() != null && docRepId != null
											&& docPath != null) {
										updatedCount = studentDocumentCollectionRepository.updateStudentDocumentFiles(
												studentApplicationImages.getAdmissionId(), docRepId, docPath);
										if (updatedCount != null && updatedCount != 0) {
											studentApplicationImages.setStudentDocumentsStatus("SUCCESS");

											response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(),
													studentApplicationImages, HttpStatus.OK.value());
										} else {
											response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(),
													studentApplicationImages, HttpStatus.OK.value());

										}
									} else {
										studentApplicationImages.setStudentDocumentsStatus("SUCCESS");

										response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(),
												studentApplicationImages, HttpStatus.OK.value());
									}

								} /*
									 * else { response = new
									 * ApiResponse<>(Messages.FAILED_TO_UPLOAD_DOCUMENTS.getValue(),
									 * studentApplicationImages, HttpStatus.OK.value()); }
									 */
							}
						} catch (Exception e) {
							LOGGER.error("Unable to upload doc"+e);
							studentApplicationImages.setFiles(null);
							studentApplicationImages.setStudentDocumentsStatus("Failed to upload Student Documents");
							response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), studentApplicationImages,
									HttpStatus.INTERNAL_SERVER_ERROR.value());
						}
					}
				}

			} catch (Exception e) {
			
				studentApplicationImages.setFiles(null);
				studentApplicationImages.setStudentDocumentsStatus("Failed to upload Student Documents");
				response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), studentApplicationImages,
						HttpStatus.INTERNAL_SERVER_ERROR.value());
			}

		} catch (Exception e) {
			
			studentApplicationImages.setFiles(null);
			response = new ApiResponse<>(e.getMessage(), studentApplicationImages,
					HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		studentApplicationImages.setFiles(null);
		studentAppDocumentCollectionRepository.flush();
		updateStudentDetails(studentApplicationImages);
		return response;

	}
	
	
	private void updateStudentDetails(StudentApplicationImagesDTO studentApplicationImages) {
		if(studentApplicationImages.getAdmissionId()!= null) {
			if(studentApplicationImages.getApplicationId() != null) {
				StudentDetail std = studentDetailRepository.findByStudenAdmissionNo(studentApplicationImages.getAdmisssionNo());
				StudentApplication stdApp = studentApplicationRepository.findById(studentApplicationImages.getApplicationId()).get();
				std.setFatherPhotoPath(stdApp.getFatherPhotoPath());
				std.setMotherPhotoPath(stdApp.getMotherPhotoPath());
				std.setStudentPhotoPath(stdApp.getStudentPhotoPath());
				std.setPancardFilePath(stdApp.getPancardFilePath());
				std.setAadharFilePath(stdApp.getAadharFilePath());
				studentDetailRepository.save(std);
			}
		
		//StudentDetail std = studentDetailRepository.findById(studentApplicationImages.getAdmissionId()).get();
		List<StudentDocumentCollection> docCollections = stdDocCollectionRepository.findDocCollectionByStdId(studentApplicationImages.getAdmissionId());
		for (StudentDocumentCollection docCollection : docCollections) {
			if(docCollection.getStudentAppDocCollection() != null) {
				StudentAppDocCollection studentAppDocCollection = docCollection.getStudentAppDocCollection();
				docCollection.setFilePath(studentAppDocCollection.getFilePath());
			}
		}
		stdDocCollectionRepository.saveAll(docCollections);
		}
	}

	private ApiResponse<StudentApplicationImagesDTO> updateStudentMotherPhotoFilePath(
			StudentApplicationImagesDTO studentApplicationImages, String motherPhotoFileName) {
		ApiResponse<StudentApplicationImagesDTO> response;
		try {
			Long updatedCount = studentApplicationRepository
					.updateStudentMotherPhotoFile(studentApplicationImages.getApplicationId(), motherPhotoFileName);

			if (updatedCount != null && updatedCount != 0) {
				if (studentApplicationImages.getAdmissionId() != null
						&& studentApplicationImages.getAdmissionId() != 0) {
					updatedCount = studentDetailRepository.updateStudentMotherPhotoFile(
							studentApplicationImages.getAdmissionId(), motherPhotoFileName);
				}

				response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), studentApplicationImages,
						HttpStatus.OK.value());
			} else {
				response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), studentApplicationImages,
						HttpStatus.OK.value());
			}
		} catch (Exception e) {
			
			response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), studentApplicationImages,
					HttpStatus.OK.value());
		}
		return response;
	}

	private ApiResponse<StudentApplicationImagesDTO> updateStudentFatherPhotoFilePath(
			StudentApplicationImagesDTO studentApplicationImages, String fatherPhotoFileName) {
		ApiResponse<StudentApplicationImagesDTO> response;
		try {
			Long updatedCount = studentApplicationRepository
					.updateStudentFatherPhotoFile(studentApplicationImages.getApplicationId(), fatherPhotoFileName);

			if (updatedCount != null && updatedCount != 0) {
				if (studentApplicationImages.getAdmissionId() != null
						&& studentApplicationImages.getAdmissionId() != 0) {
					updatedCount = studentDetailRepository.updateStudentFatherPhotoFile(
							studentApplicationImages.getAdmissionId(), fatherPhotoFileName);
				}

				response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), studentApplicationImages,
						HttpStatus.OK.value());
			} else {
				response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), studentApplicationImages,
						HttpStatus.OK.value());
			}
		} catch (Exception e) {
		
			response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), studentApplicationImages,
					HttpStatus.OK.value());
		}
		return response;
	}

	private ApiResponse<StudentApplicationImagesDTO> updateStudentPhotoFilePath(
			StudentApplicationImagesDTO studentApplicationImages, String studentPhotoFileName) {
		ApiResponse<StudentApplicationImagesDTO> response;
		try {
			Long updatedCount = studentApplicationRepository
					.updateStudentPhotoFile(studentApplicationImages.getApplicationId(), studentPhotoFileName);

			if (updatedCount != null && updatedCount != 0) {
				if (studentApplicationImages.getAdmissionId() != null
						&& studentApplicationImages.getAdmissionId() != 0) {
					updatedCount = studentDetailRepository
							.updateStudentPhotoFile(studentApplicationImages.getAdmissionId(), studentPhotoFileName);
				}

				response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), studentApplicationImages,
						HttpStatus.OK.value());
			} else {
				response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), studentApplicationImages,
						HttpStatus.OK.value());
			}
		} catch (Exception e) {
			
			response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), studentApplicationImages,
					HttpStatus.OK.value());
		}
		return response;
	}

	private ApiResponse<StudentApplicationImagesDTO> updateStudentPancardFilePath(
			StudentApplicationImagesDTO studentApplicationImages, String studentPancardFileName) {
		ApiResponse<StudentApplicationImagesDTO> response;
		try {
			Long updatedCount = studentApplicationRepository
					.updateStudentPancardFile(studentApplicationImages.getApplicationId(), studentPancardFileName);

			if (updatedCount != null && updatedCount != 0) {
				if (studentApplicationImages.getAdmissionId() != null
						&& studentApplicationImages.getAdmissionId() != 0) {
					updatedCount = studentDetailRepository
							.updateStudentPanCard(studentApplicationImages.getAdmissionId(), studentPancardFileName);
				}

				response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), studentApplicationImages,
						HttpStatus.OK.value());
			} else {
				response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), studentApplicationImages,
						HttpStatus.OK.value());
			}
		} catch (Exception e) {
			
			response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), studentApplicationImages,
					HttpStatus.OK.value());
		}
		return response;
	}

	private ApiResponse<StudentApplicationImagesDTO> updateStudentAadharFilePath(
			StudentApplicationImagesDTO studentApplicationImages, String studentAadharFileName) {
		ApiResponse<StudentApplicationImagesDTO> response;
		try {
			Long updatedCount = studentApplicationRepository
					.updateStudentAadharFile(studentApplicationImages.getApplicationId(), studentAadharFileName);

			if (updatedCount != null && updatedCount != 0) {
				if (studentApplicationImages.getAdmissionId() != null
						&& studentApplicationImages.getAdmissionId() != 0) {
					updatedCount = studentDetailRepository
							.updateStudentAadharFile(studentApplicationImages.getAdmissionId(), studentAadharFileName);
				}

				response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), studentApplicationImages,
						HttpStatus.OK.value());
			} else {
				response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), studentApplicationImages,
						HttpStatus.OK.value());
			}
		} catch (Exception e) {
		
			response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), studentApplicationImages,
					HttpStatus.OK.value());
		}
		return response;
	}
}