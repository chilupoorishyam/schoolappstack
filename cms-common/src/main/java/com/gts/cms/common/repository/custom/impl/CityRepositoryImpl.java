package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.enums.Status;
import com.gts.cms.common.repository.custom.CityRepositoryCustom;
import com.gts.cms.entity.City;
import com.gts.cms.entity.QCity;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * created by Naveen on 04/09/2018
 * 
 */

public class CityRepositoryImpl extends QuerydslRepositorySupport implements CityRepositoryCustom {
	public CityRepositoryImpl()	{
		super(City.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<City> getCitiesByDistrictId(Long districtId) {
		JPQLQuery<City> query = from(QCity.city)
				.where(QCity.city.district.districtId.eq(districtId).and(QCity.city.isActive.eq(Status.ACTIVE.getId())));
		return query.fetch();

	}

}
