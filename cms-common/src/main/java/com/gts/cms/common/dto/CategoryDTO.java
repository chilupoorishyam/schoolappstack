package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

public class CategoryDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long categoryId;
	private String achievementCategory;
	private String achievementCategoryCode;
	private String categoryLogoPath;
	private Date createdDt;
	private Long createdUser;
	private Boolean isActive;
	private String reason;
	private Date updatedDt;
	private Long updatedUser;
	private Long organizationId;
	private String orgCode;
	private String orgName;
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setAchievementCategory(String achievementCategory) {
		this.achievementCategory = achievementCategory;
	}

	public String getAchievementCategory() {
		return achievementCategory;
	}

	public void setAchievementCategoryCode(String achievementCategoryCode) {
		this.achievementCategoryCode = achievementCategoryCode;
	}

	public String getAchievementCategoryCode() {
		return achievementCategoryCode;
	}

	public void setCategoryLogoPath(String categoryLogoPath) {
		this.categoryLogoPath = categoryLogoPath;
	}

	public String getCategoryLogoPath() {
		return categoryLogoPath;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReason() {
		return reason;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgName() {
		return orgName;
	}
}