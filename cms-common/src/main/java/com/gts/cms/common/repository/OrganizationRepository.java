package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.OrganizationRepositoryCustom;
import com.gts.cms.entity.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * created by Naveen(Auto) on 02/09/2018
 * 
 */
@Repository
public interface OrganizationRepository extends JpaRepository<Organization, Long>, OrganizationRepositoryCustom {

	public List<Organization> findByIsActiveTrue();
	
	public Optional<Organization> findByOrganizationIdAndIsActiveTrue(Long organizationId);

	public Optional<Organization> findByOrgCode(String orgCode);



}
