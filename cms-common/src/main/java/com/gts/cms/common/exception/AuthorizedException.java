package com.gts.cms.common.exception;
/**
 * created by sathish on 02/09/2018.
 */
public class AuthorizedException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new authorized exception.
	 */
	public AuthorizedException() {
		super();
	}

	/**
	 * Instantiates a new authorized exception.
	 *
	 * @param msg the msg
	 */
	public AuthorizedException(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new authorized exception.
	 *
	 * @param t the t
	 */
	public AuthorizedException(Throwable t) {
		super(t);
	}
}
