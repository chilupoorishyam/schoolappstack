package com.gts.cms.common.employee.mapper;

import com.gts.cms.common.employee.dto.EmployeeEducationDTO;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.EmployeeDetail;
import com.gts.cms.entity.EmployeeEducation;
import com.gts.cms.entity.GeneralDetail;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Genesis
 *
 */
@Component
public class EmployeeEducationMapper implements BaseMapper<EmployeeEducation, EmployeeEducationDTO> {

	public EmployeeEducationDTO convertEntityToDTO(EmployeeEducation employeeEducation) {
		EmployeeEducationDTO employeeEducationDTO = null;
		if (employeeEducation != null) {
			employeeEducationDTO = new EmployeeEducationDTO();

			employeeEducationDTO.setEmpEducationId(employeeEducation.getEmpEducationId());
			employeeEducationDTO.setAddress(employeeEducation.getAddress());
			employeeEducationDTO.setBoard(employeeEducation.getBoard());
			employeeEducationDTO.setCreatedDt(employeeEducation.getCreatedDt());
			employeeEducationDTO.setCreatedUser(employeeEducation.getCreatedUser());
			employeeEducationDTO.setGradeClassSecured(employeeEducation.getGradeClassSecured());
			employeeEducationDTO.setIsActive(employeeEducation.getIsActive());
			employeeEducationDTO.setMajorSubjects(employeeEducation.getMajorSubjects());
			employeeEducationDTO.setMedium(employeeEducation.getMedium());
			employeeEducationDTO.setNameOfInstitution(employeeEducation.getNameOfInstitution());
			employeeEducationDTO.setPrecentage(employeeEducation.getPrecentage());
			employeeEducationDTO.setReason(employeeEducation.getReason());
			employeeEducationDTO.setYearOfCompletion(employeeEducation.getYearOfCompletion());

			if (employeeEducation.getEmpDetail() != null) {
				employeeEducationDTO.setEmployeeId(employeeEducation.getEmpDetail().getEmployeeId());
			}

			if (employeeEducation.getModeofstudy() != null) {
				employeeEducationDTO.setModeofstudy(employeeEducation.getModeofstudy().getGeneralDetailId());
			}

		}
		return employeeEducationDTO;
	}

	public EmployeeEducation convertDTOtoEntity(EmployeeEducationDTO employeeEducationDTO,
			EmployeeEducation employeeEducation) {
		if (employeeEducationDTO != null) {

			if (employeeEducation == null) {
				employeeEducation = new EmployeeEducation();
				employeeEducation.setIsActive(Boolean.TRUE);
				employeeEducation.setCreatedDt(new Date());
				employeeEducation.setCreatedUser(SecurityUtil.getCurrentUser());
			} else {
				employeeEducation.setCreatedDt(employeeEducationDTO.getCreatedDt());
				employeeEducation.setCreatedUser(employeeEducationDTO.getCreatedUser());
				employeeEducation.setIsActive(employeeEducationDTO.getIsActive());
			}

			employeeEducation.setAddress(employeeEducationDTO.getAddress());
			employeeEducation.setBoard(employeeEducationDTO.getBoard());
			employeeEducation.setGradeClassSecured(employeeEducationDTO.getGradeClassSecured());

			employeeEducation.setMajorSubjects(employeeEducationDTO.getMajorSubjects());
			employeeEducation.setMedium(employeeEducationDTO.getMedium());
			employeeEducation.setNameOfInstitution(employeeEducationDTO.getNameOfInstitution());
			employeeEducation.setPrecentage(employeeEducationDTO.getPrecentage());
			employeeEducation.setReason(employeeEducationDTO.getReason());
			employeeEducation.setYearOfCompletion(employeeEducationDTO.getYearOfCompletion());

			EmployeeDetail employeeDetail = new EmployeeDetail();
			employeeDetail.setEmployeeId(employeeEducationDTO.getEmployeeId());
			employeeEducation.setEmpDetail(employeeDetail);

			GeneralDetail modeofstudy = new GeneralDetail();
			modeofstudy.setGeneralDetailId(employeeEducationDTO.getModeofstudy());
			employeeEducation.setModeofstudy(modeofstudy);

		}
		return employeeEducation;
	}

	public List<EmployeeEducation> convertDTOListToEntityList(List<EmployeeEducationDTO> employeeEducationDTOList) {
		List<EmployeeEducation> employeeEducationList = new ArrayList<>();
		if (employeeEducationDTOList != null) {
			employeeEducationDTOList.forEach(
					employeeEducation -> employeeEducationList.add(convertDTOtoEntity(employeeEducation, null)));
		}
		return employeeEducationList;

	}

	public List<EmployeeEducationDTO> convertEntityListToDTOList(List<EmployeeEducation> employeeEducationList) {
		List<EmployeeEducationDTO> employeeEducationDTOList = new ArrayList<>();
		employeeEducationList
				.forEach(employeeEducation -> employeeEducationDTOList.add(convertEntityToDTO(employeeEducation)));
		return employeeEducationDTOList;
	}

}
