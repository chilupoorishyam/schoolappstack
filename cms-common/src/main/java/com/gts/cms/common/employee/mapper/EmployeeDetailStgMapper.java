package com.gts.cms.common.employee.mapper;

import com.gts.cms.common.employee.dto.EmployeeDetailStgDTO;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.entity.EmployeeDetailStg;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EmployeeDetailStgMapper implements BaseMapper<EmployeeDetailStg, EmployeeDetailStgDTO> {


    @Override
    public EmployeeDetailStgDTO convertEntityToDTO(EmployeeDetailStg employeeDetailStg) {
        EmployeeDetailStgDTO employeeDetailStgDTO = null;
        if (employeeDetailStg != null) {
            employeeDetailStgDTO = EmployeeDetailStgDTO.builder()
                    .organization(employeeDetailStg.getOrganization())
                    .school(employeeDetailStg.getSchool())
                    .empNo(employeeDetailStg.getEmpNo())
                    .dateOfJoin(employeeDetailStg.getDateOfJoin())
                    .firstName(employeeDetailStg.getFirstName())
                    .middleName(employeeDetailStg.getMiddleName())
                    .lastName(employeeDetailStg.getLastName())
                    .gender(employeeDetailStg.getGender())
                    .dateOfBirth(employeeDetailStg.getDateOfBirth())
                    .mobileNumber(employeeDetailStg.getMobileNumber())
                    .email(employeeDetailStg.getEmail())
                    .aadhaarNo(employeeDetailStg.getAadhaarNo())
                    .panNo(employeeDetailStg.getPanNo())
                    .department(employeeDetailStg.getDepartment())
                    .workingDepartment(employeeDetailStg.getWorkingDepartment())
                    .qualification(employeeDetailStg.getWorkingDepartment())
                    .designation(employeeDetailStg.getDesignation())
                    .employeeCategory(employeeDetailStg.getEmployeeCategory())
                    .address(employeeDetailStg.getAddress())
                    .city(employeeDetailStg.getCity())
                    .district(employeeDetailStg.getDistrict())
                    .build();
        }
        return employeeDetailStgDTO;
    }

    @Override
    public List<EmployeeDetailStgDTO> convertEntityListToDTOList(List<EmployeeDetailStg> entityList) {
        List<EmployeeDetailStgDTO> examStgMarksDTOList = new ArrayList<>();
        entityList.forEach(employeeDetailStg -> examStgMarksDTOList.add(convertEntityToDTO(employeeDetailStg)));
        return examStgMarksDTOList;
    }

    @Override
    public List<EmployeeDetailStg> convertDTOListToEntityList(List<EmployeeDetailStgDTO> dtoList) {
        return null;
    }

    @Override
    public EmployeeDetailStg convertDTOtoEntity(EmployeeDetailStgDTO employeeDetailStgDTO, EmployeeDetailStg employeeDetailStg) {
        return null;
    }
}
