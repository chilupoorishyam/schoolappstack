package com.gts.cms.common.employee.mapper;

import com.gts.cms.common.dto.EmployeeReportingDTO;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.util.OptionalUtil;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.Designation;
import com.gts.cms.entity.EmployeeDetail;
import com.gts.cms.entity.EmployeeReporting;
import com.gts.cms.entity.Organization;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Genesis
 *
 */
@Component
public class EmployeeReportingMapper implements BaseMapper<EmployeeReporting, EmployeeReportingDTO> {

	public EmployeeReportingDTO convertEntityToDTO(EmployeeReporting employeeReporting) {
		EmployeeReportingDTO employeeReportingDTO = null;
		if (employeeReporting != null) {
			employeeReportingDTO = new EmployeeReportingDTO();

			employeeReportingDTO.setEmpReportingId(employeeReporting.getEmployeeReportingId());
			employeeReportingDTO.setCreatedDt(employeeReporting.getCreatedDt());
			employeeReportingDTO.setCreatedUser(employeeReporting.getCreatedUser());
			employeeReportingDTO.setFromDate(employeeReporting.getFromDate());
			employeeReportingDTO.setToDate(employeeReporting.getToDate());
			employeeReportingDTO.setIsActive(employeeReporting.getIsActive());
			employeeReportingDTO.setReason(employeeReporting.getReason());

			employeeReportingDTO.setEmpId(
					OptionalUtil.resolve(() -> employeeReporting.getEmployeeDetail().getEmployeeId()).orElse(null));
			employeeReportingDTO.setEmpName(
					OptionalUtil.resolve(() -> employeeReporting.getEmployeeDetail().getFirstName()).orElse(null));
			employeeReportingDTO.setEmpNumber(
					OptionalUtil.resolve(() -> employeeReporting.getEmployeeDetail().getEmpNumber()).orElse(null));
			
			employeeReportingDTO.setManagerEmpId(OptionalUtil
					.resolve(() -> employeeReporting.getManagerEmployeeDetail().getEmployeeId()).orElse(null));
			employeeReportingDTO.setManagerEmpName(OptionalUtil
					.resolve(() -> employeeReporting.getManagerEmployeeDetail().getFirstName()).orElse(null));
			employeeReportingDTO.setManagerEmpNumber(
					OptionalUtil.resolve(() -> employeeReporting.getManagerEmployeeDetail().getEmpNumber()).orElse(null));
			
			/*employeeReportingDTO.setEmpDesignationId(
					OptionalUtil.resolve(() -> employeeReporting.getDesignation().getDesignationId()).orElse(null));*/
			
			if(employeeReporting.getDesignation()!=null) {
				employeeReportingDTO.setEmpDesignationId(employeeReporting.getDesignation().getDesignationId());
				employeeReportingDTO.setDesEmpName(employeeReporting.getDesignation().getDesignationName());
			}
			
			/*employeeReportingDTO.setOrganizationId(
					OptionalUtil.resolve(() -> employeeReporting.getOrganization().getOrganizationId()).orElse(null));*/
			if (employeeReporting.getOrganization()!= null) {
				employeeReportingDTO.setOrganizationId(employeeReporting.getOrganization().getOrganizationId());
				employeeReportingDTO.setOrgName(employeeReporting.getOrganization().getOrgName());
				employeeReportingDTO.setOrgCode(employeeReporting.getOrganization().getOrgCode());
			}

		}
		return employeeReportingDTO;
	}

	public EmployeeReporting convertDTOtoEntity(EmployeeReportingDTO employeeReportingDTO, EmployeeReporting employeeReporting) {
		if (employeeReportingDTO != null) {
			if (employeeReporting == null) {
				employeeReporting = new EmployeeReporting();
				employeeReporting.setCreatedDt(new Date());
				employeeReporting.setIsActive(Boolean.TRUE);
				employeeReporting.setCreatedUser(SecurityUtil.getCurrentUser());
			}else {
				employeeReporting.setIsActive(employeeReportingDTO.getIsActive());
			}
			
			employeeReporting.setEmployeeReportingId(employeeReportingDTO.getEmpReportingId());
			employeeReporting.setUpdatedDt(new Date());
			employeeReporting.setUpdatedUser(SecurityUtil.getCurrentUser());
			employeeReporting.setFromDate(employeeReportingDTO.getFromDate());
			employeeReporting.setToDate(employeeReportingDTO.getToDate());
			employeeReporting.setReason(employeeReportingDTO.getReason());
			if(employeeReportingDTO.getEmpId()!=null) {
			EmployeeDetail employeeDetail = new EmployeeDetail();
			employeeDetail.setEmployeeId(employeeReportingDTO.getEmpId());
			employeeReporting.setEmployeeDetail(employeeDetail);
			}
			if(employeeReportingDTO.getManagerEmpId()!=null) {
			EmployeeDetail managerEmployeeDetail = new EmployeeDetail();
			managerEmployeeDetail.setEmployeeId(employeeReportingDTO.getManagerEmpId());
			employeeReporting.setManagerEmployeeDetail(managerEmployeeDetail);
			}
			if(employeeReportingDTO.getEmpDesignationId()!=null) {
			Designation designation = new Designation();
			designation.setDesignationId(employeeReportingDTO.getEmpDesignationId());
			employeeReporting.setDesignation(designation);
			}
			if(employeeReportingDTO.getOrganizationId()!=null) {
			Organization organization = new Organization();
			organization.setOrganizationId(employeeReportingDTO.getOrganizationId());
			employeeReporting.setOrganization(organization);
			}
		}
		return employeeReporting;
	}

	public List<EmployeeReporting> convertDTOListToEntityList(
			List<EmployeeReportingDTO> employeeBankDetailDTOList) {
		List<EmployeeReporting> employeeBankDetailList = new ArrayList<>();
		if (employeeBankDetailDTOList != null) {
			employeeBankDetailDTOList.forEach(employeeReporting -> employeeBankDetailList
					.add(convertDTOtoEntity(employeeReporting, null)));
		}
		return employeeBankDetailList;

	}

	public List<EmployeeReportingDTO> convertEntityListToDTOList(
			List<EmployeeReporting> employeeBankDetailList) {
		List<EmployeeReportingDTO> employeeBankDetailDTOList = new ArrayList<>();
		employeeBankDetailList
				.forEach(employeeReporting -> employeeBankDetailDTOList.add(convertEntityToDTO(employeeReporting)));
		return employeeBankDetailDTOList;
	}

}
