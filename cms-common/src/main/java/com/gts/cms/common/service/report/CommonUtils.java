package com.gts.cms.common.service.report;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

public class CommonUtils {
private static final Logger LOGGER=Logger.getLogger(CommonUtils.class);
	@SuppressWarnings("rawtypes")
	public static synchronized String getClassName(Object object) throws Exception {

		if (object==null)
			throw new NullPointerException("\"given object can not be null\"");

		Class clazz = object.getClass();

		String str = clazz.getName();
		String className = "";

		int index = str.lastIndexOf(".");

		if (index!=-1)
			className = str.substring(index+1);

		return className;
	}
	
	public static synchronized String getFileExtension(String fileName) {

		String extension = "";
		int index = fileName.lastIndexOf(".")+1;
		if (index!=-1)
			extension = fileName.substring(index);

		return extension;
	}
	
	public static synchronized String getFileExtension(File file) {
		String extension = "";

		if(file!=null)
			extension = getFileExtension(file.getName());

		return extension;
	}
	
    public static String getClientIp(HttpServletRequest req) {
        String remoteAddr = "";
        if (req != null) {
            remoteAddr = req.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = req.getRemoteAddr();
            }
        }
        return remoteAddr;
    }
	

	private static final String[] lowNames = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight",
			"nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen",
			"nineteen" };

	private static final String[] tensNames = { "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty",
			"Ninety" };

	private static final String[] bigNames = { "Thousand", "million", "billion" };

	/**
	 * Converts an integer number into words (american english).
	 **/
	public static String convertNumberTowords(int n) {
		if (n < 0) {
			return "minus " + convertNumberTowords(-n);
		}
		if (n <= 999) {
			return convert999(n);
		}
		String s = null;
		int t = 0;
		while (n > 0) {
			if (n % 1000 != 0) {
				String s2 = convert999(n % 1000);
				if (t > 0) {
					s2 = s2 + " " + bigNames[t - 1];
				}
				if (s == null) {
					s = s2;
				} else {
					s = s2 + ", " + s;
				}
			}
			n /= 1000;
			t++;
		}
		return s;
	}

	// Range 0 to 999.
	private static String convert999(int n) {
		String s1 = lowNames[n / 100] + " hundred";
		String s2 = convert99(n % 100);
		if (n <= 99) {
			return s2;
		} else if (n % 100 == 0) {
			return s1;
		} else {
			return s1 + " " + s2;
		}
	}

	// Range 0 to 99.
	private static String convert99(int n) {
		if (n < 20) {
			return lowNames[n];
		}
		String s = tensNames[n / 10 - 2];
		if (n % 10 == 0) {
			return s;
		}
		return s + "-" + lowNames[n % 10];
	}
	
	public static String getDateInFormat(Date date) {
		if(date != null) {
		String pattern = "dd-MM-yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		return simpleDateFormat.format(date);
		}else {
			return "";
		}
	}
	
	 public static String convertToIndianCurrency(String num) {
	        BigDecimal bd = new BigDecimal(num);
	        long number = bd.longValue();
	        long no = bd.longValue();
	        int decimal = (int) (bd.remainder(BigDecimal.ONE).doubleValue() * 100);
	        int digits_length = String.valueOf(no).length();
	        int i = 0;
	        ArrayList<String> str = new ArrayList<>();
	        HashMap<Integer, String> words = new HashMap<>();
	        words.put(0, "");
	        words.put(1, "One");
	        words.put(2, "Two");
	        words.put(3, "Three");
	        words.put(4, "Four");
	        words.put(5, "Five");
	        words.put(6, "Six");
	        words.put(7, "Seven");
	        words.put(8, "Eight");
	        words.put(9, "Nine");
	        words.put(10, "Ten");
	        words.put(11, "Eleven");
	        words.put(12, "Twelve");
	        words.put(13, "Thirteen");
	        words.put(14, "Fourteen");
	        words.put(15, "Fifteen");
	        words.put(16, "Sixteen");
	        words.put(17, "Seventeen");
	        words.put(18, "Eighteen");
	        words.put(19, "Nineteen");
	        words.put(20, "Twenty");
	        words.put(30, "Thirty");
	        words.put(40, "Forty");
	        words.put(50, "Fifty");
	        words.put(60, "Sixty");
	        words.put(70, "Seventy");
	        words.put(80, "Eighty");
	        words.put(90, "Ninety");
	        String digits[] = {"", "Hundred", "Thousand", "Lakh", "Crore"};
	        while (i < digits_length) {
	            int divider = (i == 2) ? 10 : 100;
	            number = no % divider;
	            no = no / divider;
	            i += divider == 10 ? 1 : 2;
	            if (number > 0) {
	                int counter = str.size();
	                String plural = (counter > 0 && number > 9) ? "s" : "";
	                String tmp = (number < 21) ? words.get(Integer.valueOf((int) number)) + " " + digits[counter] + plural : words.get(Integer.valueOf((int) Math.floor(number / 10) * 10)) + " " + words.get(Integer.valueOf((int) (number % 10))) + " " + digits[counter] + plural;                
	                str.add(tmp);
	            } else {
	                str.add("");
	            }
	        }
	 
	        Collections.reverse(str);
	        String Rupees = String.join(" ", str).trim();
	 
	        String paise = (decimal) > 0 ? " And Paise " + words.get(Integer.valueOf((int) (decimal - decimal % 10))) + " " + words.get(Integer.valueOf((int) (decimal % 10))) : "";
	        return "In words: Rupees " + Rupees + " Only." + " ";
	    }
	
	public static Date getLessThanOneDate(Date date) {
		LOGGER.info("CommonUtils.getLessThanOneDate()==>given date"+date);
		Calendar calendar = Calendar.getInstance();
		if (date != null) {
			calendar.setTime(date);
		}
		calendar.set(Calendar.HOUR, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 40);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.add(Calendar.DATE, -1);
		LOGGER.info("CommonUtils.getLessThanOneDate()"+calendar.getTime());
		return calendar.getTime();
	}
	
	public static Date getDateAndTime(Date date) {
		LOGGER.info("CommonUtils.getDateAndTime()==>given date"+date);
		Calendar calendarObj = Calendar.getInstance();
		calendarObj.setTime(date);
		calendarObj.set(Calendar.MINUTE, 0);
		calendarObj.set(Calendar.SECOND, 0);
		calendarObj.set(Calendar.HOUR_OF_DAY, 0);
		LOGGER.info("CommonUtils.getLessThanOneDate()==>result date"+calendarObj.getTime());
		return calendarObj.getTime();
	}
	
	public static Integer compareDate(Date fromDate,Date toDate) {
		LOGGER.info("CommonUtils.compareDate()==>fromDate==>"+fromDate+"toDate"+toDate);
		Integer value = null;
		if (fromDate!=null &&fromDate.compareTo(toDate) > 0) {
			value= -1;
        } else if (fromDate!=null && fromDate.compareTo(toDate) < 0) {
        	value= 1;
        } else if (fromDate!=null && fromDate.compareTo(toDate) == 0) {
        	value= 0;
        }
		LOGGER.info("CommonUtils.compareDate()==>value==>"+value);
		return value; 
	}
	
	public static String replaceLastCharacter(String str) {
		LOGGER.info("CommonUtils.replaceLastCharacter()");
		if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == ',') {
			str = str.substring(0, str.length() - 1);
		}
		return str;
	}

	public static char[] generateOTP(int len)
	{
		/*String Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String Small_chars = "abcdefghijklmnopqrstuvwxyz";*/
		// Using numeric values
		String numbers = "0123456789";
		/*String values = Capital_chars + Small_chars +
				numbers;*/

		// Using random method
		Random rndm_method = new Random();

		char[] otp = new char[len];

		for (int i = 0; i < len; i++)
		{
			// Use of charAt() method : to get character value
			// Use of nextInt() as it is scanning the value as int
			otp[i] =
					numbers.charAt(rndm_method.nextInt(numbers.length()));
		}
		return otp;
	}
}
