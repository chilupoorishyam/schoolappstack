package com.gts.cms.common.querydsl.support;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import static java.lang.String.format;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.Type;

import com.gts.cms.common.exception.MalformedQueryException;
import org.apache.commons.beanutils.ConstructorUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.util.StringUtils;

import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.gts.cms.common.querydsl.support.EntityDescriptorFactory.EntityDescriptor;
import com.gts.cms.common.querydsl.support.QDslFilterPredicate.Junction;
import com.gts.cms.common.querydsl.support.QDslFilterPredicate.Operator;
import com.querydsl.core.JoinExpression;
import com.querydsl.core.support.PathsExtractor;
import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Ops;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.QBean;
import com.querydsl.core.types.dsl.BeanPath;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.core.types.dsl.PathBuilderFactory;
import com.querydsl.core.types.dsl.SimpleOperation;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.jpa.JPAQueryBase;
import com.querydsl.jpa.JPQLQuery;

public final class QDslFilterBuilder {
	private static final String NULL_STRING = "null";
	private static final Set<Operator> LIST_OPERATORS = Sets.newHashSet(Operator.BETWEEN, Operator.IN);
	private static final String ORDER_PREFIX = "\\.?order\\(";
	private static final String ORDER_POSTFIX = "\\)";
	private static final Pattern ORDER_PATTERN = Pattern.compile(ORDER_PREFIX + ".+" + ORDER_POSTFIX,
			Pattern.CASE_INSENSITIVE);
	public static final String UNPARSABLE_QUERY = "The query is malformed and not parsable: %s";
	private static final int PREDICATE_DIVISOR = 4;
	private static final int VALID_PREDICATE_REMAINDER = 3;
	private static final int PATH_OFFSET = 1;
	private static final int OPERATOR_OFFSET = 2;
	private static final int VALUE_OFFSET = 3;
	private static final int NO_JUNCTION_BOUNDARY = -1;
	private static final Map<Class<?>, Class<?>> PRIMITIVE_CONVERTERS = Maps.newHashMap();
	static {
		PRIMITIVE_CONVERTERS.put(boolean.class, Boolean.class);
		PRIMITIVE_CONVERTERS.put(byte.class, Byte.class);
		PRIMITIVE_CONVERTERS.put(short.class, Short.class);
		PRIMITIVE_CONVERTERS.put(char.class, Character.class);
		PRIMITIVE_CONVERTERS.put(int.class, Integer.class);
		PRIMITIVE_CONVERTERS.put(long.class, Long.class);
		PRIMITIVE_CONVERTERS.put(float.class, Float.class);
		PRIMITIVE_CONVERTERS.put(double.class, Double.class);
	}

	public QDslFilterBuilder() {

	}

	private static final QDslFilterPathAdapter IDENTITY_ADAPTER = new QDslFilterPathAdapter() {

		@Override
		public String adaptPath(String path) {
			return path;
		}
	};

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static boolean joinExists(JPQLQuery query, EntityPath path, PathBuilder alias) {
		final Expression<?> aliasedJoin = Expressions.as(path, alias);
		List<JoinExpression> joinExpressions = ((JPAQueryBase) query).getMetadata().getJoins();
		for (JoinExpression joinExpression : joinExpressions) {
			if (joinExpression.getTarget().equals(aliasedJoin)) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static Path<?> buildPath(JPQLQuery query, Class<?> entityClazz, final String flatPath) {
		final List<String> nodes = Lists.newArrayList(flatPath.split("\\."));
		EntityDescriptorFactory.validatePath(entityClazz, nodes);
		EntityDescriptor entityDescriptor = EntityDescriptorFactory.describe(entityClazz);
		PathBuilder<?> pathBuilder = new PathBuilderFactory().create(entityClazz);
		if (!nodes.isEmpty()) {
			final String currentNode = nodes.remove(0);
			PropertyDescriptor propertyDescriptor = entityDescriptor.getProperty(currentNode);
			checkNotNull(propertyDescriptor, format("'%s' is not navigable from %s", currentNode, entityClazz));
			if (entityDescriptor.isEntityDesriptor(propertyDescriptor)
					|| entityDescriptor.isCollectiondescriptor(propertyDescriptor)) {
				Class<?> joinClass = EntityDescriptorFactory.getType(propertyDescriptor);
				PathBuilder<?> alias = new PathBuilderFactory().create(joinClass);
				EntityPath joinPath = (EntityPath<?>) pathBuilder.get(propertyDescriptor.getName(), joinClass);
				if (!joinExists(query, joinPath, alias)) {
					query.leftJoin(joinPath, alias);
				}
				return buildPath(query, joinClass, Joiner.on(".").join(nodes));
			}
			if (String.class.equals(propertyDescriptor.getPropertyType())) {
				return pathBuilder.getString(propertyDescriptor.getName());
			}
			return pathBuilder.get(propertyDescriptor.getName(), propertyDescriptor.getPropertyType());
		}
		return null;
	}

	private static Path<?> findPath(final Expression<?> projection, final Class<?> entityClass, String stringPath,
			final JPQLQuery query) {
		List<Path<?>> paths = Lists.newArrayList();
		if (projection instanceof QBean) {
			PathsExtractor.DEFAULT.visit((QBean<?>) projection, paths);
		} else if (projection instanceof BeanPath) {
			PathsExtractor.DEFAULT.visit((BeanPath<?>) projection, paths);
		} else {
			throw new IllegalArgumentException("Don't know how to process the provided type of projection");
		}
		for (Path<?> path : paths) {
			if (stringPath.equals(path.toString())) {
				return path;
			}
		}
		return buildPath(query, entityClass, stringPath);

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static String processOrderClause(final Expression<?> projection, final Class<?> entityClass,
			final JPQLQuery query, final String filterAndSort, final QDslFilterPathAdapter pathAdapter) {
		Matcher matcher = ORDER_PATTERN.matcher(filterAndSort);
		if (matcher.find()) {
			final String orderClause = matcher.group().replaceAll(ORDER_PREFIX, "").replaceAll(ORDER_POSTFIX, "");
			for (final String orderToken : orderClause.split(",")) {
				String[] columnAndDirection = orderToken.split("=");
				Direction direction = null;
				if (columnAndDirection.length > 1) {
					direction = Direction.fromString(columnAndDirection[1].trim().toLowerCase());
				}
				final String stringPath = pathAdapter.adaptPath(columnAndDirection[0].trim());
				Path<?> path = findPath(projection, entityClass, stringPath, query);
				checkNotNull(path, format("'%s' is an invalid path.", stringPath));
				Order order = Order.ASC;
				if (Direction.DESC.equals(direction)) {
					order = Order.DESC;
				}
				query.orderBy(new OrderSpecifier(order,
						path instanceof StringPath ? ((StringPath) path).toLowerCase() : path));
			}

			String[] parts = ORDER_PATTERN.split(filterAndSort);
			StringBuilder sb = new StringBuilder();
			for (final String part : parts) {
				sb.append(part);
			}
			return sb.toString();

		}

		return filterAndSort;
	}

	private static BooleanExpression buildPredicate(Expression<?> expression, Operator operator, Object value) {
		List<Expression<?>> parameters = Lists.<Expression<?>>newArrayList(expression);
		if (Optional.fromNullable(value).isPresent()) {
			if (Operator.BETWEEN.equals(operator) && value instanceof Collection) {
				for (Object v : (Collection<?>) value) {
					parameters.add(Expressions.constant(v));
				}
			} else {
				parameters.add(Expressions.constant(value));
			}
		}
		Expression<?>[] params = parameters.toArray(new Expression<?>[parameters.size()]);
		final boolean isString = expression.getType().isAssignableFrom(String.class);
		switch (operator) {
		case EQUAL:
			if (value == null) {
				return Expressions.predicate(Ops.IS_NULL, expression);
			} else {
				if (isString) {
					return Expressions.predicate(Ops.EQ_IGNORE_CASE, params);
				}
				return Expressions.predicate(Ops.EQ, params);
			}
		case NOTEQUAL:
			if (value == null) {
				return Expressions.predicate(Ops.IS_NOT_NULL, params);
			} else {
				if (isString) {
					return Expressions.predicate(Ops.EQ_IGNORE_CASE, params).not();
				}
				return Expressions.predicate(Ops.NE, params);
			}
		case LIKE:
		case CONTAINS:
			if (value == null) {
				return Expressions.predicate(Ops.IS_NULL, expression);
			}
			return Expressions.predicate(Ops.STRING_CONTAINS_IC, params);
		case STARTSWITH:
			if (value == null) {
				return Expressions.predicate(Ops.IS_NULL, expression);
			}
			return Expressions.predicate(Ops.STARTS_WITH_IC, params);
		case ENDSWITH:
			if (value == null) {
				return Expressions.predicate(Ops.IS_NULL, expression);
			}
			return Expressions.predicate(Ops.ENDS_WITH_IC, params);
		case GREATER:
			return Expressions.predicate(Ops.GT, params);
		case GREATEROREQUAL:
			return Expressions.predicate(Ops.GOE, params);
		case LESS:
			return Expressions.predicate(Ops.LT, params);
		case LESSOREQUAL:
			return Expressions.predicate(Ops.LOE, params);
		case BETWEEN:
			return Expressions.predicate(Ops.BETWEEN, params);
		case IN:
			return Expressions.predicate(Ops.IN, params);
		default:
			throw new IllegalStateException(
					"This shouldn't have happend.  If it did, the place to fix it is in the FilterBulder class");
		}
	}

	private static Expression<?> findAliasedExpression(Expression<?> expression, Path<?> potentialAlias) {
		if (expression instanceof QBean) {
			for (Expression<?> op : ((QBean<?>) expression).getArgs()) {
				if (op instanceof SimpleOperation) {
					List<?> args = ((SimpleOperation<?>) op).getArgs();
					if (args.size() > 1) {
						if (potentialAlias.equals(args.get(1))) {
							return (Expression<?>) args.get(0);
						}
					}
				}
			}
		}
		return potentialAlias;
	}

	@SuppressWarnings("rawtypes")
	public static JPQLQuery buildQuery(Class<?> entityClass, EntityManager em, String filterAndSort,
			QDslFilterPathAdapter originalPathAdapter) {
		final PathBuilder<?> rootPath = new PathBuilderFactory().create(entityClass);
		final JPQLQuery query = new Querydsl(em, rootPath).createQuery(rootPath);
		final PathBuilder<?> pathBuilder = new PathBuilderFactory().create(entityClass);
		return appendQuery(pathBuilder, entityClass, query, filterAndSort, originalPathAdapter);
	}

	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes" })
	public static JPQLQuery applyPagination(final JPQLQuery query, final EntityManager em, final EntityPath<?> ep,
			Pageable pageable) {
		final Querydsl querydsl = new Querydsl(em, new PathBuilder(ep.getType(), ep.getMetadata()));
		final EntityType<?> entityType = em.getMetamodel().entity(ep.getType());
		final Type<?> idType = entityType.getIdType();
		if (idType != null) {
			final SingularAttribute<?, ?> id = entityType.getId(idType.getJavaType());
			if (id != null) {
				final Sort defaultPaginationOrder =Sort.by(id.getName());
				if (pageable.getSort() != null) {
					pageable.getSort().and(defaultPaginationOrder);
				} else {
					pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
							defaultPaginationOrder);
				}
			}
		}
		return querydsl.applyPagination(pageable, query);
	}

	@SuppressWarnings("rawtypes")
	private static JPQLQuery appendQuery(final Expression<?> projection, Class<?> entityClass, JPQLQuery query,
			String filterAndSort, final QDslFilterPathAdapter originalPathAdapter) {
		if (StringUtils.isEmpty(filterAndSort)) {
			return query;
		}
		QDslFilterPathAdapter pathAdapter = originalPathAdapter != null ? originalPathAdapter : IDENTITY_ADAPTER;
		String[] tokens = parse(processOrderClause(projection, entityClass, query, filterAndSort.trim(), pathAdapter));
		if (tokens.length == 0) {
			return query;
		}
		if (tokens.length % PREDICATE_DIVISOR != VALID_PREDICATE_REMAINDER) {
			throw new MalformedQueryException(format(UNPARSABLE_QUERY, filterAndSort));
		}
		BooleanExpression predicate = null;
		for (int i = NO_JUNCTION_BOUNDARY; i < tokens.length; i = i + PREDICATE_DIVISOR) {
			Operator operator = Operator.get(tokens[i + OPERATOR_OFFSET]);
			String value = tokens[i + VALUE_OFFSET];
			Junction junction = null;
			if (i > -1) {
				junction = Junction.get(tokens[i]);
			}
			final String targetPath = pathAdapter.adaptPath(tokens[i + PATH_OFFSET]);
			Path<?> path = findPath(projection, entityClass, targetPath, query);
			checkNotNull(path, format("'%s' pah not found", targetPath));

			final Expression<?> expression = findAliasedExpression(projection, path);
			final BooleanExpression currentPredicate = buildPredicate(expression, operator,
					extractOperand(operator, expression.getType(), value));
			if (predicate != null) {
				if (Junction.OR.equals(junction)) {
					predicate = predicate.or(currentPredicate);
				} else {
					predicate = predicate.and(currentPredicate);
				}

			} else {
				predicate = currentPredicate;
			}
		}
		if (predicate != null) {
			query.where(predicate);
		}
		return query;
	}

	private static Object extractOperand(final Operator op, Class<?> type, String inputValue) {
		Object result = null;
		if (LIST_OPERATORS.contains(op)) {
			List<Object> values = Lists.newArrayList();
			result = values;
			for (String v : inputValue.split(QDslFilterPredicate.LIST_SEPARATOR)) {
				values.add(newInstance(type, v));
			}
		} else {
			result = newInstance(type, inputValue);
		}
		return result;
	}

	private static List<String> split(final List<String> expressions,
			final QDslFilterPredicate.FilterOperator operator) {
		List<String> result = newArrayList();
		for (final String expression : expressions) {
			String[] pairs = Pattern.compile(operator.regex(), Pattern.CASE_INSENSITIVE).split(expression);
			boolean first = true;
			for (String operand : pairs) {
				if (!first) {
					result.add(operator.value());
				}
				first = false;
				result.add(operand.trim());
			}

		}
		return result;
	}

	private static String[] parse(final String queryParam) {
		if ("".equals(queryParam.trim())) {
			return new String[] {};
		}
		List<QDslFilterPredicate.FilterOperator> operators = Lists.newArrayList();
		for (Operator filter : Operator.values()) {
			operators.add(filter);
		}
		for (Junction joinBy : Junction.values()) {
			operators.add(joinBy);
		}
		List<String> result = Lists.newArrayList();
		result.add(queryParam);
		for (final QDslFilterPredicate.FilterOperator operator : operators) {
			result = split(result, operator);
		}
		return result.toArray(new String[result.size()]);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static Object newInstance(Class<?> type, String value) {
		if (NULL_STRING.equalsIgnoreCase(value)) {
			return null;
		}
		if (type.isEnum()) {
			return Enum.valueOf((Class) type, value);
		}
		if (type.isPrimitive()) {
			type = PRIMITIVE_CONVERTERS.get(type);
		}
		Constructor<?> ctor = ConstructorUtils.getAccessibleConstructor(type, String.class);
		if (ctor == null) {
			ctor = ConstructorUtils.getAccessibleConstructor(type, Object.class);
			if (ctor == null) {
				throw new IllegalArgumentException(String.format("No single argumnt constructor for: %s", type));
			}
		}
		try {
			return ctor.newInstance(value);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}