package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

public class StudentDetailListDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long studentId;
	private String admissionNumber;
	private String applicationNo;
	private Date dateOfBirth;
	private String fatherAddress;
	private String fatherEmailId;
	private String fatherMobileNo;
	private String fatherName;
	private String fatherQualification;
	private String firstName;
	private String guardianAddress;
	private String guardianEmailId;
	private String guardianMobileNo;
	private String guardianName;
	private String hallticketNumber;
	private Boolean isActive;
	private String lastName;
	private String middleName;
	private String mobile;
	private String motherEmailId;
	private String motherMobileNo;
	private String motherName;
	private String permanentAddress;
	private String permanentPincode;
	private String permanentStreet;
	private String premanentMandal;
	private String presentAddress;
	private String presentMandal;
	private String presentPincode;
	private String presentStreet;
	private String primaryContact;
	private String rfid;
	private String rollNumber;
	private String sscNo;
	private String stdEmailId;
	private Long studentAppId;
	private String studentEmailId;
	private Long groupSectionId;
	private Long batchId;
	private Long academicYearId;
	private String academicYear;

	private Long genderId;
	private String genderDisplayName;
	private Long quotaId;
	private String quotaDisplayName;

	private String studentPhotoPath;
	private Long schoolId;
	private String schoolCode;
	private String schoolName;
	private Long courseId;
	private String courseCode;
	private String groupCode;
	private Long courseYearId;
	private String courseYearCode;
	private String courseYearName;
	private String section;

	private Long userId;
	/* private List<StudentAttendanceDTO> studentAttendances; */

	private Long studentStatusId;
	private String studentStatusCode;
	private String studentStatusDisplayName;
	private String reason;
	private Boolean isScholarship;
	private Boolean isLateral;

	private Long qualifyingId;
	private String qualifyingName;
	private String qualifyingCode;

	private Boolean isMinority;
	private Date fromDate;
	private Date createdDt;
	private Long createdUser;
	private Date toDate;
	private Long organizationId;

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getAdmissionNumber() {
		return admissionNumber;
	}

	public void setAdmissionNumber(String admissionNumber) {
		this.admissionNumber = admissionNumber;
	}

	public String getApplicationNo() {
		return applicationNo;
	}

	public void setApplicationNo(String applicationNo) {
		this.applicationNo = applicationNo;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getFatherAddress() {
		return fatherAddress;
	}

	public void setFatherAddress(String fatherAddress) {
		this.fatherAddress = fatherAddress;
	}

	public String getFatherEmailId() {
		return fatherEmailId;
	}

	public void setFatherEmailId(String fatherEmailId) {
		this.fatherEmailId = fatherEmailId;
	}

	public String getFatherMobileNo() {
		return fatherMobileNo;
	}

	public void setFatherMobileNo(String fatherMobileNo) {
		this.fatherMobileNo = fatherMobileNo;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getFatherQualification() {
		return fatherQualification;
	}

	public void setFatherQualification(String fatherQualification) {
		this.fatherQualification = fatherQualification;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGuardianAddress() {
		return guardianAddress;
	}

	public void setGuardianAddress(String guardianAddress) {
		this.guardianAddress = guardianAddress;
	}

	public String getGuardianEmailId() {
		return guardianEmailId;
	}

	public void setGuardianEmailId(String guardianEmailId) {
		this.guardianEmailId = guardianEmailId;
	}

	public String getGuardianMobileNo() {
		return guardianMobileNo;
	}

	public void setGuardianMobileNo(String guardianMobileNo) {
		this.guardianMobileNo = guardianMobileNo;
	}

	public String getGuardianName() {
		return guardianName;
	}

	public void setGuardianName(String guardianName) {
		this.guardianName = guardianName;
	}

	public String getHallticketNumber() {
		return hallticketNumber;
	}

	public void setHallticketNumber(String hallticketNumber) {
		this.hallticketNumber = hallticketNumber;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMotherEmailId() {
		return motherEmailId;
	}

	public void setMotherEmailId(String motherEmailId) {
		this.motherEmailId = motherEmailId;
	}

	public String getMotherMobileNo() {
		return motherMobileNo;
	}

	public void setMotherMobileNo(String motherMobileNo) {
		this.motherMobileNo = motherMobileNo;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getPermanentPincode() {
		return permanentPincode;
	}

	public void setPermanentPincode(String permanentPincode) {
		this.permanentPincode = permanentPincode;
	}

	public String getPermanentStreet() {
		return permanentStreet;
	}

	public void setPermanentStreet(String permanentStreet) {
		this.permanentStreet = permanentStreet;
	}

	public String getPremanentMandal() {
		return premanentMandal;
	}

	public void setPremanentMandal(String premanentMandal) {
		this.premanentMandal = premanentMandal;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getPresentMandal() {
		return presentMandal;
	}

	public void setPresentMandal(String presentMandal) {
		this.presentMandal = presentMandal;
	}

	public String getPresentPincode() {
		return presentPincode;
	}

	public void setPresentPincode(String presentPincode) {
		this.presentPincode = presentPincode;
	}

	public String getPresentStreet() {
		return presentStreet;
	}

	public void setPresentStreet(String presentStreet) {
		this.presentStreet = presentStreet;
	}

	public String getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(String primaryContact) {
		this.primaryContact = primaryContact;
	}

	public String getRfid() {
		return rfid;
	}

	public void setRfid(String rfid) {
		this.rfid = rfid;
	}

	public String getRollNumber() {
		return rollNumber;
	}

	public void setRollNumber(String rollNumber) {
		this.rollNumber = rollNumber;
	}

	public String getSscNo() {
		return sscNo;
	}

	public void setSscNo(String sscNo) {
		this.sscNo = sscNo;
	}

	public String getStdEmailId() {
		return stdEmailId;
	}

	public void setStdEmailId(String stdEmailId) {
		this.stdEmailId = stdEmailId;
	}

	public Long getStudentAppId() {
		return studentAppId;
	}

	public void setStudentAppId(Long studentAppId) {
		this.studentAppId = studentAppId;
	}

	public String getStudentEmailId() {
		return studentEmailId;
	}

	public void setStudentEmailId(String studentEmailId) {
		this.studentEmailId = studentEmailId;
	}

	public Long getGroupSectionId() {
		return groupSectionId;
	}

	public void setGroupSectionId(Long groupSectionId) {
		this.groupSectionId = groupSectionId;
	}


	/*
	 * public List<StudentAttendanceDTO> getStudentAttendances() { return
	 * studentAttendances; }
	 * 
	 * public void setStudentAttendances(List<StudentAttendanceDTO>
	 * studentAttendances) { this.studentAttendances = studentAttendances; }
	 */
	public Long getBatchId() {
		return batchId;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public Long getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}

	public String getStudentPhotoPath() {
		return studentPhotoPath;
	}

	public void setStudentPhotoPath(String studentPhotoPath) {
		this.studentPhotoPath = studentPhotoPath;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}



	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public Long getCourseYearId() {
		return courseYearId;
	}

	public void setCourseYearId(Long courseYearId) {
		this.courseYearId = courseYearId;
	}

	public String getCourseYearCode() {
		return courseYearCode;
	}

	public void setCourseYearCode(String courseYearCode) {
		this.courseYearCode = courseYearCode;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getCourseYearName() {
		return courseYearName;
	}

	public void setCourseYearName(String courseYearName) {
		this.courseYearName = courseYearName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getGenderId() {
		return genderId;
	}

	public String getGenderDisplayName() {
		return genderDisplayName;
	}

	public Long getQuotaId() {
		return quotaId;
	}

	public String getQuotaDisplayName() {
		return quotaDisplayName;
	}

	public void setGenderId(Long genderId) {
		this.genderId = genderId;
	}

	public void setGenderDisplayName(String genderDisplayName) {
		this.genderDisplayName = genderDisplayName;
	}

	public void setQuotaId(Long quotaId) {
		this.quotaId = quotaId;
	}

	public void setQuotaDisplayName(String quotaDisplayName) {
		this.quotaDisplayName = quotaDisplayName;
	}

	public String getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}

	public Long getStudentStatusId() {
		return studentStatusId;
	}

	public void setStudentStatusId(Long studentStatusId) {
		this.studentStatusId = studentStatusId;
	}

	public String getStudentStatusCode() {
		return studentStatusCode;
	}

	public void setStudentStatusCode(String studentStatusCode) {
		this.studentStatusCode = studentStatusCode;
	}

	public String getStudentStatusDisplayName() {
		return studentStatusDisplayName;
	}

	public void setStudentStatusDisplayName(String studentStatusDisplayName) {
		this.studentStatusDisplayName = studentStatusDisplayName;
	}

	public Boolean getIsScholarship() {
		return isScholarship;
	}

	public void setIsScholarship(Boolean isScholarship) {
		this.isScholarship = isScholarship;
	}

	public Boolean getIsLateral() {
		return isLateral;
	}

	public void setIsLateral(Boolean isLateral) {
		this.isLateral = isLateral;
	}

	public Long getQualifyingId() {
		return qualifyingId;
	}

	public void setQualifyingId(Long qualifyingId) {
		this.qualifyingId = qualifyingId;
	}

	public String getQualifyingName() {
		return qualifyingName;
	}

	public void setQualifyingName(String qualifyingName) {
		this.qualifyingName = qualifyingName;
	}

	public String getQualifyingCode() {
		return qualifyingCode;
	}

	public void setQualifyingCode(String qualifyingCode) {
		this.qualifyingCode = qualifyingCode;
	}

	public Boolean getIsMinority() {
		return isMinority;
	}

	public void setIsMinority(Boolean isMinority) {
		this.isMinority = isMinority;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

}