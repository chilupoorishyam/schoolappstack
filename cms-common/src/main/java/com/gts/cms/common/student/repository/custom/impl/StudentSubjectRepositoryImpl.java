package com.gts.cms.common.student.repository.custom.impl;

import com.gts.cms.common.student.repository.custom.StudentSubjectRepositoryCustom;
import com.gts.cms.entity.StudentSubject;
import org.apache.log4j.Logger;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

/**
 * @author Genesis
 *
 */
@Repository
public class StudentSubjectRepositoryImpl extends QuerydslRepositorySupport implements StudentSubjectRepositoryCustom {
	private static final Logger LOGGER = Logger.getLogger(StudentSubjectRepositoryImpl.class);

	public StudentSubjectRepositoryImpl() {
		super(StudentSubject.class);
	}

}
