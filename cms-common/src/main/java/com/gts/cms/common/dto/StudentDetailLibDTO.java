package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

public class StudentDetailLibDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Long studentId;
	private String studentName;
	private String rollNumber;
	private Long academicYearId;
	private String academicYear;
	private Long courseId;
	private String courseName;
	private String courseCode;
	private String groupCode;
	private String groupName;
	private Long courseYearId;
	private String courseYearName;
	private String courseYearCode;
	private Long groupSectionId;
	private String section;
	private Long orgId;
	private String orgName;
	private String orgCode;
	private Long schoolId;
	private String schoolName;
	private String schoolCode;
	
	public Long getStudentId() {
		return studentId;
	}
	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getRollNumber() {
		return rollNumber;
	}
	public void setRollNumber(String rollNumber) {
		this.rollNumber = rollNumber;
	}
	public Long getAcademicYearId() {
		return academicYearId;
	}
	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}
	public String getAcademicYear() {
		return academicYear;
	}
	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}
	public Long getCourseId() {
		return courseId;
	}
	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getCourseCode() {
		return courseCode;
	}
	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Long getCourseYearId() {
		return courseYearId;
	}
	public void setCourseYearId(Long courseYearId) {
		this.courseYearId = courseYearId;
	}
	public String getCourseYearName() {
		return courseYearName;
	}
	public void setCourseYearName(String courseYearName) {
		this.courseYearName = courseYearName;
	}
	public String getCourseYearCode() {
		return courseYearCode;
	}
	public void setCourseYearCode(String courseYearCode) {
		this.courseYearCode = courseYearCode;
	}
	public Long getGroupSectionId() {
		return groupSectionId;
	}
	public void setGroupSectionId(Long groupSectionId) {
		this.groupSectionId = groupSectionId;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public Long getOrgId() {
		return orgId;
	}
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	public Long getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getSchoolCode() {
		return schoolCode;
	}
	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}
}