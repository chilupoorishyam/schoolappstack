package com.gts.cms.common.student.service;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.StudentAcademicbatchDTO;
import com.gts.cms.common.student.dto.PromotionDTO;

import java.util.List;

public interface StudentAcademicBatchesService {

	ApiResponse<?> promoteStudent(PromotionDTO promotionDTO);

	ApiResponse<?> academicBatchUpdate(List<StudentAcademicbatchDTO> studentAcademicBatchDTO);
	
}
