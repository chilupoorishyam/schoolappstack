package com.gts.cms.common.service;

import java.io.ByteArrayInputStream;
import java.util.List;

public interface StudentReportService {

	public ByteArrayInputStream getStudentAdmissionReport(String admissionId);

	public ByteArrayInputStream getTCReport(String admissionId);

	public ByteArrayInputStream getBonafideReport(String admissionId,String certificateFor,Long certificateReqId, String coursestatus);
	
	public ByteArrayInputStream getTcBonafideReport(String admissionId,String certificateFor,Long certificateReqId, String coursestatus);

	/*public ByteArrayInputStream getITBonafideReport(String admissionId,String certificateFor,Long certificateReqId);
*/
	public ByteArrayInputStream getPPBonafideReport(String admissionId,String certificateFor,Long certificateReqId);
/*
	public ByteArrayInputStream getCustodianReport(String admissionId,String certificateFor,List<String> certificatesList,Long certificateReqId);*/

	public ByteArrayInputStream getCourseCompletionReport(String admissionId,String certificateFor,String coursestatus,Long certificateReqId, String month);

	public ByteArrayInputStream getMediumOfInstructions(String admissionId,Long certificateReqId, String coursestatus);

	public ByteArrayInputStream getNoObjection(Long empId);

	public ByteArrayInputStream getStaffSalary(Long empId);

	


	public ByteArrayInputStream getCustodianReport(String admissionId, String certificateFor,
			List<String> certificatesList, Long certificateReqId, List<String> certificateList, String coursestatus);

	public ByteArrayInputStream getITBonafideReport(String admissionId, String certificateFor, Long certificateReqId,
                                                    List<String> feeList, String coursestatus, List<Long> financialYearId, Long academicYearId);

	
	public ByteArrayInputStream getBankBonafideReport(String admissionId, String certificateFor, Long certificateReqId,
			List<String> feeList, String coursestatus);
	
	// public ByteArrayInputStream getNewBonafideReport(BonafideCertificateDTO certificate);

}
