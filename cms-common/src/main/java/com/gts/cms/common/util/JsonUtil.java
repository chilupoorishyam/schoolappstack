package com.gts.cms.common.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gts.cms.common.exception.JsonException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * created by sathish on 02/09/2018
 */
public class JsonUtil {
	private static final Logger LOGGER = LogManager.getLogger(JsonUtil.class);
	private static ObjectMapper mapper;
	static {
		mapper = new ObjectMapper();
	}

	public static String javaToJson(Object obj) {
		String jsonString = null;
		try {
			jsonString = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			LOGGER.error("Exception while converting the java to json : "+e);
			throw new JsonException(e);
		}
		return jsonString;
	}
	public static <T> T jsonToJava(String jsonString,Class<T> cls) {
		T object = null;
		try {
			object=mapper.readValue(jsonString,cls);
		} catch (IOException e) {
			LOGGER.error("Exception while converting the json to java : "+e);	
			//throw new JsonException(e);
		}
		return object;
	}
	/*public static List<StudentAttendanceDetailDTO> jsonToJavaList(String jsonString,
			TypeReference<List<StudentAttendanceDetailDTO>> typeReference) {
		List<StudentAttendanceDetailDTO> object = null;
		try {
			object=mapper.readValue(jsonString,typeReference);
		} catch (IOException e) {
			LOGGER.error("Exception while converting the json to java : "+e);	
			throw new JsonException(e);
		}
		return object;
	}*/
	
}
