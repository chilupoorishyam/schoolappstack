package com.gts.cms.common.repository.custom;

import com.gts.cms.entity.CourseYear;

import java.util.List;

/**
 * created by Naveen on 04/09/2018 
 * 
*/
public interface CourseYearRepositoryCustom {
	List<CourseYear> findByCourseId(Long courseId);
	Long deleteCourseYear(Long courseYearId);
}
