package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.GeneralSettingRepositoryCustom;
import com.gts.cms.entity.GeneralSetting;
import com.gts.cms.entity.QGeneralSetting;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class GeneralSettingRepositoryImpl extends QuerydslRepositorySupport implements GeneralSettingRepositoryCustom {

	public GeneralSettingRepositoryImpl() {
		super(GeneralSetting.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public GeneralSetting getGeneralSettingByCode(String generalSettingCode) {
		JPQLQuery<GeneralSetting> query = from(QGeneralSetting.generalSetting)
				.where(QGeneralSetting.generalSetting.settingCode.eq(generalSettingCode).and(QGeneralSetting.generalSetting.isActive.eq(true)));
		return query.fetchOne();
	}

	@Override
	public GeneralSetting getGeneralSettingName(Long schoolId,String generalSettingName) {
		JPQLQuery<GeneralSetting> query = from(QGeneralSetting.generalSetting)
				.where(QGeneralSetting.generalSetting.settingCode.eq(generalSettingName)
						.and(QGeneralSetting.generalSetting.school.schoolId.eq(schoolId))
						.and(QGeneralSetting.generalSetting.isActive.eq(true)));
		return query.fetchOne();
	}
}
