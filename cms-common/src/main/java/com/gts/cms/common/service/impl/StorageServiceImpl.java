package com.gts.cms.common.service.impl;

import com.gts.cms.entity.LiveClassSchedule;
import com.gts.cms.entity.LiveClassStorage;
import com.gts.cms.videozoomapi.config.AppProperties;
import com.gts.cms.videozoomapi.repository.LiveClassStorageRepo;
import io.minio.GetPresignedObjectUrlArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.errors.*;
import io.minio.http.Method;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.tika.Tika;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class StorageServiceImpl {

    private final AppProperties appProperties;
    private final MinioClient minioClient;

    private final LiveClassStorageRepo liveClassStorageRepo;

    public LiveClassStorage uploadMeetingFile(LiveClassSchedule liveClassSchedule, MultipartFile file) throws IOException, InvalidKeyException, InvalidResponseException, InsufficientDataException, NoSuchAlgorithmException, ServerException, InternalException, XmlParserException, InvalidBucketNameException, ErrorResponseException {
        Tika tika = new Tika();
        String mime = tika.detect(file.getOriginalFilename());
        long millis = instantFromLocalDate(liveClassSchedule.getScheduledOnDate()).toEpochMilli();

        String bucket = appProperties.getStorage().getBucketRoot();
        String object = "recording_files/" + millis + "-" + liveClassSchedule.getLiveClsScheduleId() + "/" + file.getOriginalFilename();

        minioClient.putObject(
                PutObjectArgs
                        .builder()
                        .bucket(bucket)
                        .object(object)
                        .stream(file.getInputStream(), file.getSize(), -1)
                        .contentType(mime)
                        .build());

        return liveClassStorageRepo.save(
                LiveClassStorage
                        .builder()
                        .name(file.getOriginalFilename())
                        .bucket(bucket)
                        .object(object)
                        .groupUid(Long.valueOf(liveClassSchedule.getZoomMeetingId()))
                        .isOriginal(true)
                        .mime(mime)
                        .build());
    }

    public LiveClassStorage uploadMeetingFileFromZoom(LiveClassSchedule liveClassSchedule, String uri, String fileType) {
        try {
            String fileName = invokeShort() + "." + fileType;

            log.info("[::] Download file from Zoom: START");
            String tmpPath = System.getProperty("java.io.tmpdir") + UUID.randomUUID().toString();
            File file = new File(tmpPath + "/" + fileName);
            FileUtils.copyURLToFile(new URL(uri), file);
            log.info("[::] Download file from Zoom: DONE");

            Tika tika = new Tika();
            String mime = tika.detect(file);
            long millis = instantFromLocalDate(liveClassSchedule.getScheduledOnDate()).toEpochMilli();

            String bucket = appProperties.getStorage().getBucketRoot();
            String object = "recording_files/" + millis + "-" + liveClassSchedule.getLiveClsScheduleId() + "/" + fileName;

            log.info("[::] Upload file to storage: START");
            minioClient.putObject(
                    PutObjectArgs
                            .builder()
                            .bucket(bucket)
                            .object(object)
                            .stream(FileUtils.openInputStream(file), file.length(), -1)
                            .contentType(mime)
                            .build());
            log.info("[::] Upload file to storage: DONE");

            LiveClassStorage storage =
                    liveClassStorageRepo.save(
                            LiveClassStorage
                                    .builder()
                                    .name(fileName)
                                    .bucket(bucket)
                                    .object(object)
                                    .groupUid(Long.valueOf(liveClassSchedule.getZoomMeetingId()))
                                    .isOriginal(true)
                                    .mime(mime)
                                    .build());

            log.info("[::] Delete temporary file: START");
            Files
                    .walk(Paths.get(tmpPath))
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
            log.info("[::] Delete temporary file: DONE");
            return storage;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Exception uploadMeetingFileFromZoom :: " + e.getMessage());
        }
    }

    public List<LiveClassStorage> findByGroupUid(String groupUid) {
        return liveClassStorageRepo.findByGroupUid(groupUid);
    }

    public String generateObjectUri(String object) throws IOException, InvalidKeyException, InvalidResponseException, InsufficientDataException, InvalidExpiresRangeException, ServerException, InternalException, NoSuchAlgorithmException, XmlParserException, InvalidBucketNameException, ErrorResponseException {
        return minioClient.getPresignedObjectUrl(
                GetPresignedObjectUrlArgs.builder()
                        .method(Method.GET)
                        .bucket(appProperties.getStorage().getBucketRoot())
                        .object(object)
                        .expiry(1, TimeUnit.DAYS)
                        .build());
    }

    public static Instant instantFromLocalDate(LocalDate localDate) {
        return localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
    }

    public static String invokeShort() {
        return UUID.randomUUID().toString().substring(0, 8);
    }
}
