package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;

public class OrganizationDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long organizationId;

	@NotNull(message = "Organization Name is required.")
	// @Size(max=10,message="Organization Name can not be more than 100 characters")
	// @Pattern(regexp="^[A-Za-z]+$",message="Organization Name Should contain only
	// Alphabets")
	private String orgName;

	@NotNull(message = "Organization code is required.")
	private String orgCode;

	@NotNull(message = "District Id is required.")
	private Long districtId;

	private String districtName;

	private Long stateId;

	private Long countryId;

	private String stateName;

	private String countryName;

	private String cityName;

	private Long cityId;
	@NotNull(message = "Is Active is required.")
	private Boolean isActive;

	@NotNull(message = "Address is required")
	private String address;

	@NotNull(message = "Pincode is required.")
	private Integer pincode;

	@NotNull(message = "Mandal is required.")
	private String mandal;

	private String logoPath;

	private String logoFilename;

	private String landlineNumber;

	// @Pattern(regexp="/^(\\+\\d{1,3}[- ]?)?\\d{10}$/",message="Mobile Number is
	// not valid")
	private String mobileNumber;

	private String fax;

	// @Pattern(regexp="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$",message="Email
	// Address is not valid")
	private String email;

	private String facebookUrl;

	private String googleUrl;

	private String linkedinUrl;

	private Date licenseFdate;

	private Date licenseTdate;

	private Integer noIssuedLicenses;

	private String url;

	private String reason;
	
	private Map<String, MultipartFile> files;
	
	private String logoUploadStatus;

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getPincode() {
		return pincode;
	}

	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}

	public String getMandal() {
		return mandal;
	}

	public void setMandal(String mandal) {
		this.mandal = mandal;
	}

	public String getLogoPath() {
		return logoPath;
	}

	public void setLogoPath(String logoPath) {
		this.logoPath = logoPath;
	}

	public String getLogoFilename() {
		return logoFilename;
	}

	public void setLogoFilename(String logoFilename) {
		this.logoFilename = logoFilename;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFacebookUrl() {
		return facebookUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}

	public String getGoogleUrl() {
		return googleUrl;
	}

	public void setGoogleUrl(String googleUrl) {
		this.googleUrl = googleUrl;
	}

	public String getLinkedinUrl() {
		return linkedinUrl;
	}

	public void setLinkedinUrl(String linkedinUrl) {
		this.linkedinUrl = linkedinUrl;
	}

	public Date getLicenseFdate() {
		return licenseFdate;
	}

	public void setLicenseFdate(Date licenseFdate) {
		this.licenseFdate = licenseFdate;
	}

	public Date getLicenseTdate() {
		return licenseTdate;
	}

	public void setLicenseTdate(Date licenseTdate) {
		this.licenseTdate = licenseTdate;
	}

	public Integer getNoIssuedLicenses() {
		return noIssuedLicenses;
	}

	public void setNoIssuedLicenses(Integer noIssuedLicenses) {
		this.noIssuedLicenses = noIssuedLicenses;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getLandlineNumber() {
		return landlineNumber;
	}

	public void setLandlineNumber(String landlineNumber) {
		this.landlineNumber = landlineNumber;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Long getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Long districtId) {
		this.districtId = districtId;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public Map<String, MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(Map<String, MultipartFile> files) {
		this.files = files;
	}

	public String getLogoUploadStatus() {
		return logoUploadStatus;
	}

	public void setLogoUploadStatus(String logoUploadStatus) {
		this.logoUploadStatus = logoUploadStatus;
	}
	
	

}
