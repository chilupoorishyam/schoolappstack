package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.DepartmentRepositoryCustom;
import com.gts.cms.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * created by Naveen(Auto) on 02/09/2018
 * 
 */
@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long>, DepartmentRepositoryCustom {

    List<Department> findByDeptCode(String deptCode);
}
