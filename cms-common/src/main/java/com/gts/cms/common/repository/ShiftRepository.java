package com.gts.cms.common.repository;

import com.gts.cms.entity.Shift;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ShiftRepository  extends  JpaRepository<Shift, Long>{

	
	@Query("SELECT emp,es FROM EmpAttendanceEmployee emp"
			+ " LEFT JOIN  EmployeeShift es "
			+ " WITH(es.employeeDetail.employeeId=emp.employeeDetail.employeeId  AND (es.fromDate<=CURRENT_DATE and es.toDate>=CURRENT_DATE) AND es.isActive = true)"
			+ " LEFT JOIN  Shift sh "
			+ " WITH(sh.shiftId=es.shift.shiftId)"
			+ " WHERE(:status is null or emp.isActive = :status)"
			+ " AND (:schoolId is null or emp.school.schoolId = :schoolId)"
	)
	Page<Object[]> getShiftDetails(Boolean status, Long schoolId,Pageable pageable);
}
	
/*	SELECT * -- emp.*,es.*
	FROM `t_emp_attendance_employees` emp
	LEFT JOIN `t_emp_employee_shifts` es ON es.fk_emp_id=emp.fk_emp_id
	AND es.from_date<=CURRENT_DATE  AND es.to_date>=CURRENT_DATE
	AND es.is_active=TRUE
	LEFT JOIN `t_m_shifts` sh ON sh.pk_shift_id=es.fk_shift_id
*/