package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.SubjectRepositoryCustom;
import com.gts.cms.entity.Subject;
import com.gts.cms.entity.SubjectResource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Long>, SubjectRepositoryCustom {

    @Query("SELECT DISTINCT s FROM Subject s" + " JOIN FETCH s.batchwiseStudents bs" + " WHERE s.isActive = :isActive"
            + " AND bs.school.schoolId = :schoolId" + " AND bs.academicYear.academicYearId = :academicYearId"
            + " AND bs.groupSection.groupSectionId = :groupSectionId" + " AND bs.studentDetail.studentId = :studentId "
            + " AND bs.subjectType.generalDetailCode = :subjectTypeEle")
    List<Subject> getStudentSubjects(@Param("isActive") Boolean isActive, @Param("schoolId") Long schoolId,
                                     @Param("academicYearId") Long academicYearId, @Param("groupSectionId") Long groupSectionId,
                                     @Param("studentId") Long studentId, @Param("subjectTypeEle") String subjectTypeEle);


    @Modifying
    @Query("UPDATE Subject sd SET sd.isActive = false"
            + " WHERE sd.subjectId = :subjectId")
    void setIsActiveFalse(@Param("subjectId") Long subjectId);


    @Query("SELECT s.subjectName FROM Subject s"
            + " WHERE s.subjectId IN (:subjectIdList)"
            + " AND s.isActive = true"
    )
    List<String> findSubjectDetails(@Param("subjectIdList") List<Long> subjectIdList);

    /*
     SELECT
        * -- sub.*, etd.*

    FROM
        t_m_subjects sub  INNER JOIN t_exam_timetable_details etd
            ON sub.pk_subject_id=etd.fk_subject_id
               AND etd.is_active=1

    INNER JOIN t_exam_timetable et
            ON et.pk_exam_timetable_id=etd.fk_exam_timetable_id
                AND et.`is_active`=1
   --  LEFT join `t_cm_subjectregulation` sr
-- 	    on sr.`fk_subject_id`=sub.`pk_subject_id`
-- 		and sr.`fk_course_group_id`= etd.`fk_course_group_id`
-- 		and sr.`fk_course_year_id` = etd.`fk_course_year_id`
-- 		and sr.`fk_subject_id` = etd.`fk_subject_id`
--
 WHERE
        1=1
        AND  et.fk_school_id=17
        AND  et.fk_course_id=23
        AND  et.fk_exam_id=128
        AND  et.exam_date='2020-06-13'
        AND  etd.fk_course_group_id=6
        AND  etd.fk_course_year_id=29
        AND sub.is_active=1

     */
    @Query("SELECT s,etd FROM Subject s"
            + " JOIN ExamTimetableDetail etd"
            + " on s.subjectId=etd.subject.subjectId"
            + " AND etd.isActive=true"
            + " JOIN ExamTimetable et"
            + " on et.examTimetableId=etd.examTimetable.examTimetableId"
            + " WHERE 1=1"
            + " AND (:schoolId is null or et.school.schoolId = :schoolId ) "
            + " AND (:courseId is null or et.course.courseId = :courseId )"
            + " AND (:examId is null or et.examMaster.examId = :examId )"
            + " AND (:examDate is null or et.examDate = DATE(:examDate))"
            + " AND (:courseYearId is null or etd.courseYear.courseYearId = :courseYearId ) "
            + " AND s.isActive = true"
    )
    List<Object[]> getSubjectDetails(@Param("schoolId") Long schoolId,
                                     @Param("courseId") Long courseId,
                                     @Param("examId") Long examId,
                                     @Param("examDate") Date examDate,
                                     @Param("courseYearId") Long courseYearId);

    @Query("SELECT DISTINCT sr FROM Subject s "
            + " INNER JOIN SubjectResource sr "
            + " 	ON s.subjecttype.generalDetailId = sr.subjecttype.generalDetailId "
            + " INNER JOIN SubjectCourseyear scy "
            + "     ON scy.subjectCourseyearId = sr.subjectCourseyear.subjectCourseyearId "
            + " 	AND s.subjectId= scy.subject.subjectId "
            + " WHERE 1=1 "
            + " AND s.subjectId=:subjectId "
            + " AND sr.isActive=true"
    )
    List<SubjectResource> findStaffSubjects(@Param("subjectId") Long subjectId);

    Subject findBySubjectId(@Param("subjectId") Long subjectId);
}
