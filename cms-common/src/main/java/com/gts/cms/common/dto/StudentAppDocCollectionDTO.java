package com.gts.cms.common.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

public class StudentAppDocCollectionDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long appDocCollId;

	private Date createdDt;

	private Long createdUser;

	private String fileName;

	private String filePath;

	private Boolean isActive;

	private Boolean isHardCopy;

	private Boolean isOriginal;

	private Boolean isSoftCopy;

	private Boolean isVerified;

	private String rackNumber;
	@NotNull(message="Reason is required.")
	private String reason;

	private Long studentAppId;
	private String studentFirstName;
	private String verifiedEmployeeName;
	private Long documentRepositoryId;
	private String docName;
	private Long verfiedEmployeeId;

	
	public Long getAppDocCollId() {
		return appDocCollId;
	}

	public void setAppDocCollId(Long appDocCollId) {
		this.appDocCollId = appDocCollId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsHardCopy() {
		return isHardCopy;
	}

	public void setIsHardCopy(Boolean isHardCopy) {
		this.isHardCopy = isHardCopy;
	}

	public Boolean getIsOriginal() {
		return isOriginal;
	}

	public void setIsOriginal(Boolean isOriginal) {
		this.isOriginal = isOriginal;
	}

	public Boolean getIsSoftCopy() {
		return isSoftCopy;
	}

	public void setIsSoftCopy(Boolean isSoftCopy) {
		this.isSoftCopy = isSoftCopy;
	}

	public Boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	public String getRackNumber() {
		return rackNumber;
	}

	public void setRackNumber(String rackNumber) {
		this.rackNumber = rackNumber;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Long getStudentAppId() {
		return studentAppId;
	}

	public void setStudentAppId(Long studentAppId) {
		this.studentAppId = studentAppId;
	}

	public Long getDocumentRepositoryId() {
		return documentRepositoryId;
	}

	public void setDocumentRepositoryId(Long documentRepositoryId) {
		this.documentRepositoryId = documentRepositoryId;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	


	public String getStudentFirstName() {
		return studentFirstName;
	}

	public void setStudentFirstName(String studentFirstName) {
		this.studentFirstName = studentFirstName;
	}

	public String getVerifiedEmployeeName() {
		return verifiedEmployeeName;
	}

	public void setVerifiedEmployeeName(String verifiedEmployeeName) {
		this.verifiedEmployeeName = verifiedEmployeeName;
	}

	public Long getVerfiedEmployeeId() {
		return verfiedEmployeeId;
	}

	public void setVerfiedEmployeeId(Long verfiedEmployeeId) {
		this.verfiedEmployeeId = verfiedEmployeeId;
	}


}
