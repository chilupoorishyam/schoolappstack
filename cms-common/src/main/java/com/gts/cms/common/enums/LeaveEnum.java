package com.gts.cms.common.enums;

public enum LeaveEnum {
	YEAR(2019);

	private final Integer value;

	LeaveEnum(final Integer newValue) {
		value = newValue;
	}

	public Integer getValue() {
		return value;
	}
}
