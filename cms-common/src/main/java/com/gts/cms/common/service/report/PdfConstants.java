package com.gts.cms.common.service.report;

public enum PdfConstants {
	PDF_PATH("C:\\Users\\GenesisTechSystems\\Downloads\\pdf (2)\\"),
	PDF_STYLE_PATH("C:\\Users\\GenesisTechSystems\\Downloads\\pdf (2)\\pdf"),
	LOGO("https://schoolerp.skolo.co.in/assets/images/logos/logo.jpg");

	private String value;

	private PdfConstants(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
