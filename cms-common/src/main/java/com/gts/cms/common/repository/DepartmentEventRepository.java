package com.gts.cms.common.repository;

import com.gts.cms.entity.DepartmentEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentEventRepository extends JpaRepository<DepartmentEvent, Long> {
}