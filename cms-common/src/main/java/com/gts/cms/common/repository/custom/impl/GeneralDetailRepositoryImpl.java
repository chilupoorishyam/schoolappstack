package com.gts.cms.common.repository.custom.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.gts.cms.entity.GeneralDetail;
import com.gts.cms.entity.QGeneralDetail;
import com.gts.cms.common.repository.custom.GeneralDetailRepositoryCustom;
import com.gts.cms.entity.GeneralDetail;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.querydsl.jpa.JPQLQuery;

@Repository
public class GeneralDetailRepositoryImpl extends QuerydslRepositorySupport implements GeneralDetailRepositoryCustom {

	public GeneralDetailRepositoryImpl() {
		super(GeneralDetail.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public GeneralDetail findByGeneralDetailIdAndIsActiveTrue(Long generalDetailId) {
		JPQLQuery<GeneralDetail> query = from(QGeneralDetail.generalDetail)
				.where(QGeneralDetail.generalDetail.generalDetailId.eq(generalDetailId).and(QGeneralDetail.generalDetail.isActive.eq(true)));
		return query.fetchOne();
	}

	@Override
	public List<GeneralDetail> findByGeneralMasterIdAndIsActiveTrue(Long generalMasterId) {
	JPQLQuery<GeneralDetail> query = from(QGeneralDetail.generalDetail)
				.where(QGeneralDetail.generalDetail.generalMaster.generalMasterId.eq(generalMasterId).and(QGeneralDetail.generalDetail.isActive.eq(true)));
		return query.fetch();
	}
	
	@Override
	public List<GeneralDetail> findByGeneralMasterCode(String generalMasterCode) {
	JPQLQuery<GeneralDetail> query = from(QGeneralDetail.generalDetail)
				.where(QGeneralDetail.generalDetail.generalMaster.generalMasterCode.eq(generalMasterCode).and(QGeneralDetail.generalDetail.isActive.eq(true)));
		return query.fetch();
	}

	@Override
	public Long deleteGeneralDetail(Long generalDetailId) {
		return update(QGeneralDetail.generalDetail).where(QGeneralDetail.generalDetail.generalDetailId.eq(generalDetailId))
				.set(QGeneralDetail.generalDetail.isActive, false).execute();
	}

	@Override
	public GeneralDetail getFeeDataTypeByCode(String generalDetailCode) {
		JPQLQuery<GeneralDetail> query = from(QGeneralDetail.generalDetail)
				.where(QGeneralDetail.generalDetail.generalDetailCode.eq(generalDetailCode).and(QGeneralDetail.generalDetail.isActive.eq(true)));
		return query.fetchOne();
	}
	
	@Override
	public GeneralDetail getGeneralDetailByCode(String generalDetailCode) {
		JPQLQuery<GeneralDetail> query = from(QGeneralDetail.generalDetail)
				.where(QGeneralDetail.generalDetail.generalDetailCode.eq(generalDetailCode).and(QGeneralDetail.generalDetail.isActive.eq(true)));
		return query.fetchOne();
	}
	
	@Override
	public GeneralDetail getGeneralMasterDetailByCodes(String generalDetailCode,String generalMasterCode) {
		JPQLQuery<GeneralDetail> query = from(QGeneralDetail.generalDetail)
				.where(QGeneralDetail.generalDetail.generalDetailCode.eq(generalDetailCode).and(QGeneralDetail.generalDetail.generalMaster.generalMasterCode.eq(generalMasterCode)).
						and(QGeneralDetail.generalDetail.isActive.eq(true)));
		return query.fetchOne();
	}

}
