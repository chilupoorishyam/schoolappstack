package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;


public class SmsTagDTO implements Serializable {
private static final long serialVersionUID = 1L;
private Long smsTagId;
private Date createdDt;
private Long createdUser;
private Boolean isActive;
private String reason;
private String tag;
private String tagDes;
private Date updatedDt;
private Long updatedUser;
private Long organizationId;
private String orgCode;
private String orgName;

public void setSmsTagId(Long smsTagId) {
 this.smsTagId = smsTagId;
}
public Long getSmsTagId(){
 return smsTagId;
}
public void setCreatedDt(Date createdDt) {
 this.createdDt = createdDt;
}
public Date getCreatedDt(){
 return createdDt;
}
public void setCreatedUser(Long createdUser) {
 this.createdUser = createdUser;
}
public Long getCreatedUser(){
 return createdUser;
}
public void setIsActive(Boolean isActive) {
 this.isActive = isActive;
}
public Boolean getIsActive(){
 return isActive;
}
public void setReason(String reason) {
 this.reason = reason;
}
public String getReason(){
 return reason;
}
public void setTag(String tag) {
 this.tag = tag;
}
public String getTag(){
 return tag;
}
public void setTagDes(String tagDes) {
 this.tagDes = tagDes;
}
public String getTagDes(){
 return tagDes;
}
public void setUpdatedDt(Date updatedDt) {
 this.updatedDt = updatedDt;
}
public Date getUpdatedDt(){
 return updatedDt;
}
public void setUpdatedUser(Long updatedUser) {
 this.updatedUser = updatedUser;
}
public Long getUpdatedUser(){
 return updatedUser;
}
public void setOrganizationId(Long organizationId) {
 this.organizationId = organizationId;
}
public Long getOrganizationId(){
 return organizationId;
}
public void setOrgCode(String orgCode) {
 this.orgCode = orgCode;
}
public String getOrgCode(){
 return orgCode;
}
public void setOrgName(String orgName) {
 this.orgName = orgName;
}
public String getOrgName(){
 return orgName;
}
}