package com.gts.cms.common.employee.dto;

import com.gts.cms.common.dto.*;
import com.gts.cms.common.dto.EmployeeReportingDTO;
import com.gts.cms.common.dto.StaffCourseyrSubjectDTO;
import com.gts.cms.common.dto.SubjectResourceDTO;

import java.io.Serializable;
import java.util.List;

public class EmployeeResignedDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<StaffCourseyrSubjectDTO> staffCourseYrSubjectDTOs;

	private List<SubjectResourceDTO> subjectResourceDTOs;

	//private List<CounselorMappingDTO> counselorMappingDTOs;
	
	private List<EmployeeReportingDTO> employeeReportingDTOs;
	
	//private List<StudentAttendanceDTO> studentAttendanceDTOs;

	public List<StaffCourseyrSubjectDTO> getStaffCourseYrSubjectDTOs() {
		return staffCourseYrSubjectDTOs;
	}

	public void setStaffCourseYrSubjectDTOs(List<StaffCourseyrSubjectDTO> staffCourseYrSubjectDTOs) {
		this.staffCourseYrSubjectDTOs = staffCourseYrSubjectDTOs;
	}

	public List<SubjectResourceDTO> getSubjectResourceDTOs() {
		return subjectResourceDTOs;
	}

	public void setSubjectResourceDTOs(List<SubjectResourceDTO> subjectResourceDTOs) {
		this.subjectResourceDTOs = subjectResourceDTOs;
	}

	public List<EmployeeReportingDTO> getEmployeeReportingDTOs() {
		return employeeReportingDTOs;
	}

	public void setEmployeeReportingDTOs(List<EmployeeReportingDTO> employeeReportingDTOs) {
		this.employeeReportingDTOs = employeeReportingDTOs;
	}
}
