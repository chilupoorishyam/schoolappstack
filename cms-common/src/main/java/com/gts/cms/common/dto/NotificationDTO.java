package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

public class NotificationDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long notificationId;

	private Long schoolId;

	private Long academicYearId;

	private String notificationTitle;

	private String description;

	private Date publishDate;

	private Boolean isPublished;

	private Date notificationEnddate;

	private Boolean isAnnouncement;

	private String reason;

	@NotNull(message = "isActive is required")
	private Boolean isActive;

	private Date createdDt;

	private Long createdUser;

	private Date updatedDt;

	private Long updatedUser;

	private String schoolName;

	private String schoolCode;

	private String academicYear;

	private String notificationDocPath;

	private Long userId;

	private List<NotificationAudienceDTO> notificationAudiences;

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public Long getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}

	public String getNotificationTitle() {
		return notificationTitle;
	}

	public void setNotificationTitle(String notificationTitle) {
		this.notificationTitle = notificationTitle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public Boolean getIsPublished() {
		return isPublished;
	}

	public void setIsPublished(Boolean isPublished) {
		this.isPublished = isPublished;
	}

	public Date getNotificationEnddate() {
		return notificationEnddate;
	}

	public void setNotificationEnddate(Date notificationEnddate) {
		this.notificationEnddate = notificationEnddate;
	}

	public Boolean getIsAnnouncement() {
		return isAnnouncement;
	}

	public void setIsAnnouncement(Boolean isAnnouncement) {
		this.isAnnouncement = isAnnouncement;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}

	public List<NotificationAudienceDTO> getNotificationAudiences() {
		return notificationAudiences;
	}

	public void setNotificationAudiences(List<NotificationAudienceDTO> notificationAudiences) {
		this.notificationAudiences = notificationAudiences;
	}

	public String getNotificationDocPath() {
		return notificationDocPath;
	}

	public void setNotificationDocPath(String notificationDocPath) {
		this.notificationDocPath = notificationDocPath;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}