package com.gts.cms.common.student.mapper;

import com.gts.cms.common.dto.StudentMedicalCertificateDTO;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.*;
import com.gts.cms.entity.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class StudentMedicalCertificateMapper implements BaseMapper<StudentMedicalCertificate, StudentMedicalCertificateDTO> {
@Override
public StudentMedicalCertificateDTO convertEntityToDTO(StudentMedicalCertificate studentMedicalCertificate) {
	StudentMedicalCertificateDTO studentMedicalCertificateDTO = null;
	if (studentMedicalCertificate != null) {
	studentMedicalCertificateDTO = new StudentMedicalCertificateDTO();
	  studentMedicalCertificateDTO.setIsActive(studentMedicalCertificate.getIsActive());
	  studentMedicalCertificateDTO.setUpdatedDt(studentMedicalCertificate.getUpdatedDt());
	  studentMedicalCertificateDTO.setCreatedDt(studentMedicalCertificate.getCreatedDt());
	  studentMedicalCertificateDTO.setApprovalComments(studentMedicalCertificate.getApprovalComments());
	  studentMedicalCertificateDTO.setStudentMedicalCertificateId(studentMedicalCertificate.getStudentMedicalCertificateId());
	  studentMedicalCertificateDTO.setApprovalDate(studentMedicalCertificate.getApprovalDate());
	  studentMedicalCertificateDTO.setFromDate(studentMedicalCertificate.getFromDate());
	  studentMedicalCertificateDTO.setToDate(studentMedicalCertificate.getToDate());
	  studentMedicalCertificateDTO.setCertificateNo(studentMedicalCertificate.getCertificateNo());
	  studentMedicalCertificateDTO.setDoctorName(studentMedicalCertificate.getDoctorName());
	  studentMedicalCertificateDTO.setReason(studentMedicalCertificate.getReason());
	  studentMedicalCertificateDTO.setFilepath2(studentMedicalCertificate.getFilepath2());
	  studentMedicalCertificateDTO.setFilepath3(studentMedicalCertificate.getFilepath3());
	  studentMedicalCertificateDTO.setFilepath1(studentMedicalCertificate.getFilepath1());
	  studentMedicalCertificateDTO.setMedicalReason(studentMedicalCertificate.getMedicalReason());
	 if(studentMedicalCertificate.getApprovedByEmployeeDetail() != null){
	  studentMedicalCertificateDTO.setApprovedByEmployeeId(studentMedicalCertificate.getApprovedByEmployeeDetail().getEmployeeId());
	 }
	 if(studentMedicalCertificate.getAcademicYear() != null){
	  studentMedicalCertificateDTO.setAcademicYearId(studentMedicalCertificate.getAcademicYear().getAcademicYearId());
	 }
	 if(studentMedicalCertificate.getSchool() != null){
	  studentMedicalCertificateDTO.setSchoolId(studentMedicalCertificate.getSchool().getSchoolId());
	  studentMedicalCertificateDTO.setSchoolCode(studentMedicalCertificate.getSchool().getSchoolCode());
	  studentMedicalCertificateDTO.setSchoolName(studentMedicalCertificate.getSchool().getSchoolName());
	 }
	 if(studentMedicalCertificate.getCourseYear() != null){
	  studentMedicalCertificateDTO.setCourseYearId(studentMedicalCertificate.getCourseYear().getCourseYearId());
	  studentMedicalCertificateDTO.setCourseYearName(studentMedicalCertificate.getCourseYear().getCourseYearName());
	 }
	 if(studentMedicalCertificate.getApprovalStatus() != null){
	  studentMedicalCertificateDTO.setApprovalStatusId(studentMedicalCertificate.getApprovalStatus().getGeneralDetailId());
	  studentMedicalCertificateDTO.setApprovalStatusDisplayName(studentMedicalCertificate.getApprovalStatus().getGeneralDetailDisplayName());
	 }
	 if(studentMedicalCertificate.getGroupSection() != null){
	  studentMedicalCertificateDTO.setGroupSectionId(studentMedicalCertificate.getGroupSection().getGroupSectionId());
	 }
	 if(studentMedicalCertificate.getStudentDetail() != null){
	  studentMedicalCertificateDTO.setStudentId(studentMedicalCertificate.getStudentDetail().getStudentId());
	  studentMedicalCertificateDTO.setStudentFirstName(studentMedicalCertificate.getStudentDetail().getFirstName());
	  studentMedicalCertificateDTO.setStudentLastName(studentMedicalCertificate.getStudentDetail().getLastName());
	  studentMedicalCertificateDTO.setStudentMiddleName(studentMedicalCertificate.getStudentDetail().getMiddleName());
	 }
	}
	return studentMedicalCertificateDTO;
}
@Override
public StudentMedicalCertificate convertDTOtoEntity(StudentMedicalCertificateDTO studentMedicalCertificateDTO,StudentMedicalCertificate studentMedicalCertificate) {
	if ( null == studentMedicalCertificate ) {
	 studentMedicalCertificate = new StudentMedicalCertificate();
	 studentMedicalCertificate.setIsActive(Status.ACTIVE.getId());
	 studentMedicalCertificate.setCreatedDt(new Date());
	 studentMedicalCertificate.setCreatedUser(SecurityUtil.getCurrentUser());
	}else{
	 studentMedicalCertificate.setIsActive(studentMedicalCertificateDTO.getIsActive());
	}
	 if(studentMedicalCertificateDTO.getCreatedUser()!= null){
		 studentMedicalCertificate.setCreatedUser(studentMedicalCertificateDTO.getCreatedUser());
	 }
	  studentMedicalCertificate.setUpdatedDt(new Date());
	 if(studentMedicalCertificateDTO.getUpdatedUser()!= null){
		 studentMedicalCertificate.setUpdatedUser(studentMedicalCertificateDTO.getUpdatedUser());
	 }
	 if(studentMedicalCertificateDTO.getApprovedByEmployeeId()!= null){
		 EmployeeDetail employeeDetail = new EmployeeDetail();
		 employeeDetail.setEmployeeId(studentMedicalCertificateDTO.getApprovedByEmployeeId());
		 studentMedicalCertificate.setApprovedByEmployeeDetail(employeeDetail);
	 }
	  studentMedicalCertificate.setApprovalComments(studentMedicalCertificateDTO.getApprovalComments());
	 if(studentMedicalCertificateDTO.getApprovalStatusId()!= null){
		 GeneralDetail generalDetail = new GeneralDetail();
		 generalDetail.setGeneralDetailId(studentMedicalCertificateDTO.getApprovalStatusId());
		 studentMedicalCertificate.setApprovalStatus(generalDetail);
	 }
	  studentMedicalCertificate.setStudentMedicalCertificateId(studentMedicalCertificateDTO.getStudentMedicalCertificateId());
	 if(studentMedicalCertificateDTO.getStudentId()!= null){
		 StudentDetail studentDetail = new StudentDetail();
		 studentDetail.setStudentId(studentMedicalCertificateDTO.getStudentId());
		 studentMedicalCertificate.setStudentDetail(studentDetail);
	 }
	  studentMedicalCertificate.setApprovalDate(studentMedicalCertificateDTO.getApprovalDate());
	  studentMedicalCertificate.setFromDate(studentMedicalCertificateDTO.getFromDate());
	  studentMedicalCertificate.setToDate(studentMedicalCertificateDTO.getToDate());
	  studentMedicalCertificate.setCertificateNo(studentMedicalCertificateDTO.getCertificateNo());
	  studentMedicalCertificate.setDoctorName(studentMedicalCertificateDTO.getDoctorName());
	  studentMedicalCertificate.setReason(studentMedicalCertificateDTO.getReason());
	  studentMedicalCertificate.setMedicalReason(studentMedicalCertificateDTO.getMedicalReason());
	 if(studentMedicalCertificateDTO.getAcademicYearId()!= null){
		 AcademicYear academicYear = new AcademicYear();
		 academicYear.setAcademicYearId(studentMedicalCertificateDTO.getAcademicYearId());
		 studentMedicalCertificate.setAcademicYear(academicYear);
	 }
	  studentMedicalCertificate.setFilepath2(studentMedicalCertificateDTO.getFilepath2());
	 if(studentMedicalCertificateDTO.getSchoolId()!= null){
		 School school = new School();
		 school.setSchoolId(studentMedicalCertificateDTO.getSchoolId());
		 studentMedicalCertificate.setSchool(school);
	 }
	  studentMedicalCertificate.setFilepath3(studentMedicalCertificateDTO.getFilepath3());
	  studentMedicalCertificate.setFilepath1(studentMedicalCertificateDTO.getFilepath1());
	 if(studentMedicalCertificateDTO.getCourseYearId()!= null){
		 CourseYear courseYear = new CourseYear();
		 courseYear.setCourseYearId(studentMedicalCertificateDTO.getCourseYearId());
		 studentMedicalCertificate.setCourseYear(courseYear);
	 }
	 if(studentMedicalCertificateDTO.getGroupSectionId()!= null){
		 GroupSection groupSection = new GroupSection();
		 groupSection.setGroupSectionId(studentMedicalCertificateDTO.getGroupSectionId());
		 studentMedicalCertificate.setGroupSection(groupSection);
	 }
	return studentMedicalCertificate;
}
@Override
public List<StudentMedicalCertificate> convertDTOListToEntityList(List<StudentMedicalCertificateDTO> studentMedicalCertificateDTOList) {
	List<StudentMedicalCertificate> studentMedicalCertificateList = new ArrayList<>();
	 studentMedicalCertificateDTOList.forEach(studentMedicalCertificateDTO -> studentMedicalCertificateList.add(convertDTOtoEntity(studentMedicalCertificateDTO, null)));
	return studentMedicalCertificateList;
}
@Override
public List<StudentMedicalCertificateDTO> convertEntityListToDTOList(List<StudentMedicalCertificate> studentMedicalCertificateList) {
	List<StudentMedicalCertificateDTO> studentMedicalCertificateDTOList = new ArrayList<>();
	 studentMedicalCertificateList.forEach(studentMedicalCertificate -> studentMedicalCertificateDTOList.add(convertEntityToDTO(studentMedicalCertificate)));
	return studentMedicalCertificateDTOList;
}
}