package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.ProfileVisitDTO;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.AmsProfileDetail;
import com.gts.cms.entity.ProfileVisit;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class ProfileVisitMapper implements BaseMapper<ProfileVisit, ProfileVisitDTO> {
	@Override
	public ProfileVisitDTO convertEntityToDTO(ProfileVisit entityObject) {
		ProfileVisitDTO dtoObject = null;
		if (entityObject != null) {
			dtoObject = new ProfileVisitDTO();
			dtoObject.setUpdatedDt(entityObject.getUpdatedDt());
			dtoObject.setCreatedUser(entityObject.getCreatedUser());
			dtoObject.setUpdatedUser(entityObject.getUpdatedUser());
			dtoObject.setIsActive(entityObject.getIsActive());
			dtoObject.setCreatedDt(entityObject.getCreatedDt());
			dtoObject.setReason(entityObject.getReason());
			dtoObject.setProfileVisitId(entityObject.getProfileVisitId());
			dtoObject.setVisitedTime(entityObject.getVisitedTime());

			if (entityObject.getProfile() != null) {
				dtoObject.setProfileId(entityObject.getProfile().getAmsProfileId());
				dtoObject.setCityName(entityObject.getProfile().getCityName());
				dtoObject.setCompanyName(entityObject.getProfile().getCompanyName());
				dtoObject.setFirstName(entityObject.getProfile().getAmsFirstName());
			}
			if (entityObject.getVisitorProfile() != null) {
				dtoObject.setVisitorProfileId(entityObject.getVisitorProfile().getAmsProfileId());
				dtoObject.setVisitorCityName(entityObject.getVisitorProfile().getCityName());
				dtoObject.setVisitorCompanyName(entityObject.getVisitorProfile().getCompanyName());
				dtoObject.setVisitorFirstName(entityObject.getVisitorProfile().getAmsFirstName());
			}

		}
		return dtoObject;
	}

	@Override
	public ProfileVisit convertDTOtoEntity(ProfileVisitDTO dtoObject, ProfileVisit entityObject) {
		if (dtoObject != null) {
			if (entityObject == null) {
				entityObject = new ProfileVisit();
				entityObject.setIsActive(Boolean.TRUE);
				entityObject.setCreatedDt(new Date());
				entityObject.setCreatedUser(SecurityUtil.getCurrentUser());
			}
			if (dtoObject.getProfileVisitId() != null) {
				entityObject.setProfileVisitId(dtoObject.getProfileVisitId());
				entityObject.setCreatedDt(dtoObject.getCreatedDt());
				if (dtoObject.getCreatedUser() != null) {
					entityObject.setCreatedUser(dtoObject.getCreatedUser());
				} else {
					entityObject.setCreatedUser(SecurityUtil.getCurrentUser());
				}
				entityObject.setUpdatedDt(new Date());
				entityObject.setIsActive(dtoObject.getIsActive());
				entityObject.setUpdatedUser(SecurityUtil.getCurrentUser());
			}

			entityObject.setReason(dtoObject.getReason());
			if (dtoObject.getProfileId() != null) {
				AmsProfileDetail amsProfileDetail = new AmsProfileDetail();
				amsProfileDetail.setAmsProfileId(dtoObject.getProfileId());
				entityObject.setProfile(amsProfileDetail);
			}
			if (dtoObject.getVisitorProfileId() != null) {
				AmsProfileDetail amsProfileDetail = new AmsProfileDetail();
				amsProfileDetail.setAmsProfileId(dtoObject.getVisitorProfileId());
				entityObject.setVisitorProfile(amsProfileDetail);
			}

			entityObject.setVisitedTime(dtoObject.getVisitedTime());
		}
		return entityObject;
	}

	@Override
	public List<ProfileVisit> convertDTOListToEntityList(List<ProfileVisitDTO> profileVisitDTOList) {
		List<ProfileVisit> profileVisitList = new ArrayList<>();
		profileVisitDTOList.forEach(dtoObject -> profileVisitList.add(convertDTOtoEntity(dtoObject, null)));
		return profileVisitList;
	}

	@Override
	public List<ProfileVisitDTO> convertEntityListToDTOList(List<ProfileVisit> profileVisitList) {
		List<ProfileVisitDTO> profileVisitDTOList = new ArrayList<>();
		profileVisitList.forEach(entityObject -> profileVisitDTOList.add(convertEntityToDTO(entityObject)));
		return profileVisitDTOList;
	}
}