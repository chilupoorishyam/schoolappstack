package com.gts.cms.common.enums;
public enum PaymentMode {

    NB,DC,CC,CASH,CLEMI,IVR,COD,BALANCE,UPI, UPI_INTENT, CREDIT_CARD, DEBIT_CARD, NET_BANKING, EMI
}