package com.gts.cms.common.employee.dto;

import java.util.Date;

public class EmployeeBankDetailDTO {

	private Long employeeBankDetailId;

	private String accountNumber;

	private String bankAddress;

	private String bankName;

	private String branchName;

	private Date createdDt;

	private Long createdUser;

	private String ddPayableAddress;

	private String ifscCode;

	private Boolean isActive;

	private String phone;

	private String reason;

	private Long organizationId;

	private Long employeeId;

	public Long getEmployeeBankDetailId() {
		return employeeBankDetailId;
	}

	public void setEmployeeBankDetailId(Long employeeBankDetailId) {
		this.employeeBankDetailId = employeeBankDetailId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getBankAddress() {
		return bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDdPayableAddress() {
		return ddPayableAddress;
	}

	public void setDdPayableAddress(String ddPayableAddress) {
		this.ddPayableAddress = ddPayableAddress;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}


}
