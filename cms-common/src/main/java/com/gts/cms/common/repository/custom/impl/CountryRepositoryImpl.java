package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.CountryRepositoryCustom;
import com.gts.cms.entity.Country;
import com.gts.cms.entity.QCountry;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * created by Naveen on 04/09/2018
 * 
 */
@Repository
public class CountryRepositoryImpl extends QuerydslRepositorySupport implements CountryRepositoryCustom {
	public CountryRepositoryImpl() {
		super(Country.class);
	}
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public Long deleteCountryById(Long countryId) {
		return update(QCountry.country).where(QCountry.country.countryId.eq(countryId))
				.set(QCountry.country.isActive, false).execute();
	}
	
}
