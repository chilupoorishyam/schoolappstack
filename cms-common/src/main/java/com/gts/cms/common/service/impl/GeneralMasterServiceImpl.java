package com.gts.cms.common.service.impl;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.GeneralMasterDTO;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.mapper.GeneralMasterMapper;
import com.gts.cms.common.repository.GeneralMasterRepository;
import com.gts.cms.common.service.GeneralMasterService;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.GeneralMaster;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

/**
 * The Class GeneralMasterServiceImpl.
 */
@Service
public class GeneralMasterServiceImpl implements GeneralMasterService {

	private static final Logger LOGGER = Logger.getLogger(GeneralMasterServiceImpl.class);

	@Autowired
    GeneralMasterRepository generalMasterRepository;

	@Autowired
	private GeneralMasterMapper generalMasterMapper;

	@Override
	@Transactional
	public ApiResponse<Long> addGeneralMaster(GeneralMasterDTO generalMasterDTO) {
		ApiResponse<Long> response = null;
		GeneralMaster generalMaster = null;
		try {
			generalMaster = generalMasterMapper.convertDTOtoEntity(generalMasterDTO,
					generalMaster);
			generalMaster.setCreatedDt(new Date());
			generalMaster.setCreatedUser(SecurityUtil.getCurrentUser());
			generalMaster = generalMasterRepository.save(generalMaster);
			generalMasterRepository.flush();
			if (generalMaster != null && generalMaster.getGeneralMasterId() != 0) {
				response = new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(), generalMaster.getGeneralMasterId(),
						HttpStatus.OK.value());
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occured while adding GeneralMaster : " + e);
			e.printStackTrace();
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;
	}

	@Override
	public ApiResponse<GeneralMasterDTO> getGeneralMasterDetails(Long generalMasterId) {
		ApiResponse<GeneralMasterDTO> response = null;
		if (null == generalMasterId) {
			throw new ApiException(Messages.RECORD_ID_MUST_NOT_NULL.getValue());
		}
		try {
			GeneralMaster generalMaster = generalMasterRepository.findByGeneralMasterIdAndIsActiveTrue(generalMasterId);
			if (null != generalMaster) {
				GeneralMasterDTO generalMasterDto = generalMasterMapper.convertEntityToDTO(generalMaster);
				response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), generalMasterDto);
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
						HttpStatus.OK.value());
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occured while getting generalMasterDetails : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;
	}

	@Override
	public ApiResponse<Long> updateGeneralMaster(GeneralMasterDTO generalMasterDTO) {
		ApiResponse<Long> response = null;
		GeneralMaster generalMaster = null;
		try {
			generalMaster = generalMasterMapper.convertDTOtoEntity(generalMasterDTO,
					generalMaster);
			generalMaster = generalMasterRepository.save(generalMaster);
			generalMasterRepository.flush();
			if (generalMaster != null && generalMaster.getGeneralMasterId() != 0) {
				response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), generalMaster.getGeneralMasterId(),
						HttpStatus.OK.value());
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.UPDATE_FAILURE.getValue(), HttpStatus.OK.value());
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occured while updating GeneralMaster : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;

	}

	@Override
	@Transactional
	public ApiResponse<Long> deleteGeneralMaster(Long generalMasterId) {
		ApiResponse<Long> response = null;
		if (null == generalMasterId) {
			throw new ApiException(Messages.RECORD_ID_MUST_NOT_NULL.getValue());
		}
		try {
			Long deleteCount = generalMasterRepository.deleteGeneralMaster(generalMasterId);
			if (deleteCount > 0) {
				response = new ApiResponse<>(Messages.DELETE_SUCCESS.getValue(), generalMasterId,
						HttpStatus.OK.value());
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.DELETE_FAILURE.getValue(), HttpStatus.OK.value());
			}

		} catch (Exception e) {
			LOGGER.error("Exception Occured while deleting GeneralMaster : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;
	}

	@Override
	public ApiResponse<List<GeneralMasterDTO>> getAllGeneralMasterDetails() {
		ApiResponse<List<GeneralMasterDTO>> response = null;
		try {
			List<GeneralMaster> generalMasterList = generalMasterRepository.findByIsActiveTrue();
			if (!CollectionUtils.isEmpty(generalMasterList)) {
				List<GeneralMasterDTO> generalMasterListDto = generalMasterMapper
						.convertEntityListToDTOList(generalMasterList);
				response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), generalMasterListDto);
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
						HttpStatus.OK.value());
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occured while getting GeneralMaster : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;
	}
}
