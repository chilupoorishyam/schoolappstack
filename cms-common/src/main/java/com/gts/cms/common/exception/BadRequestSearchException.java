package com.gts.cms.common.exception;

import org.springframework.validation.Errors;

/**
 * created by sathish on 02/09/2018.
 */
public class BadRequestSearchException extends RuntimeException {

	private Errors errors;

	public BadRequestSearchException(String message) {
		super(message);
	}

	public BadRequestSearchException(String message, Errors errors) {
		super(message);
		this.errors = errors;
	}
	public BadRequestSearchException(Errors errors) {
		this.errors = errors;
	}

	public Errors getErrors() {
		return errors;
	}
}
