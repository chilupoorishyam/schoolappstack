package com.gts.cms.common.dto;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

public class StudentDailylogDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long dailylogId;
	private Long biometricId;
	private Long schoolId;
	private String schoolName;
	private Long studentId;
	private String biometricCode;
	private Date createdDt;
	private Long createdUser;
	private Date dateofdutyRaw;
	private Integer eventDate;
	private Integer eventId;
	private Time eventTime;
	private Date eventdate;
	private Boolean isActive;
	private String reason;
	private String rfid;
	private Integer seqNo;
	private String servicetagId;
	private Date updatedDt;
	private Long updatedUser;
	public Long getDailylogId() {
		return dailylogId;
	}
	public void setDailylogId(Long dailylogId) {
		this.dailylogId = dailylogId;
	}
	public Long getBiometricId() {
		return biometricId;
	}
	public void setBiometricId(Long biometricId) {
		this.biometricId = biometricId;
	}
	public Long getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public Long getStudentId() {
		return studentId;
	}
	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}
	public String getBiometricCode() {
		return biometricCode;
	}
	public void setBiometricCode(String biometricCode) {
		this.biometricCode = biometricCode;
	}
	public Date getCreatedDt() {
		return createdDt;
	}
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	public Long getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}
	public Date getDateofdutyRaw() {
		return dateofdutyRaw;
	}
	public void setDateofdutyRaw(Date dateofdutyRaw) {
		this.dateofdutyRaw = dateofdutyRaw;
	}
	public Integer getEventDate() {
		return eventDate;
	}
	public void setEventDate(Integer eventDate) {
		this.eventDate = eventDate;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public Time getEventTime() {
		return eventTime;
	}
	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}
	public Date getEventdate() {
		return eventdate;
	}
	public void setEventdate(Date eventdate) {
		this.eventdate = eventdate;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getRfid() {
		return rfid;
	}
	public void setRfid(String rfid) {
		this.rfid = rfid;
	}
	public Integer getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}
	public String getServicetagId() {
		return servicetagId;
	}
	public void setServicetagId(String servicetagId) {
		this.servicetagId = servicetagId;
	}
	public Date getUpdatedDt() {
		return updatedDt;
	}
	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}
	public Long getUpdatedUser() {
		return updatedUser;
	}
	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}


}
