package com.gts.cms.common.student.dto;

import com.gts.cms.common.dto.StudentDetailListDTO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class PromotionDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Date fromDate;

	private Date toDate;
	
	private Long schoolId;
	
	private Long courseId;
	
	private Long academicYearId;
	
	private Long fromCourseYearId;
	
	private Long toCourseYearId;
	
	private Long fromBatchId;

	private Long toBatchId;
	
	private Long fromGroupSectionId;
	
	private Long toGroupSectionId;
	
	private List<StudentDetailListDTO> studentIdsList;

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public Long getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}

	public Long getFromCourseYearId() {
		return fromCourseYearId;
	}

	public void setFromCourseYearId(Long fromCourseYearId) {
		this.fromCourseYearId = fromCourseYearId;
	}

	public Long getToCourseYearId() {
		return toCourseYearId;
	}

	public void setToCourseYearId(Long toCourseYearId) {
		this.toCourseYearId = toCourseYearId;
	}

	public Long getFromBatchId() {
		return fromBatchId;
	}

	public void setFromBatchId(Long fromBatchId) {
		this.fromBatchId = fromBatchId;
	}

	public Long getToBatchId() {
		return toBatchId;
	}

	public void setToBatchId(Long toBatchId) {
		this.toBatchId = toBatchId;
	}

	public Long getFromGroupSectionId() {
		return fromGroupSectionId;
	}

	public void setFromGroupSectionId(Long fromGroupSectionId) {
		this.fromGroupSectionId = fromGroupSectionId;
	}

	public Long getToGroupSectionId() {
		return toGroupSectionId;
	}

	public void setToGroupSectionId(Long toGroupSectionId) {
		this.toGroupSectionId = toGroupSectionId;
	}

	public List<StudentDetailListDTO> getStudentIdsList() {
		return studentIdsList;
	}

	public void setStudentIdsList(List<StudentDetailListDTO> studentIdsList) {
		this.studentIdsList = studentIdsList;
	}


}
