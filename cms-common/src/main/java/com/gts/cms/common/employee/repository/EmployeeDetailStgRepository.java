package com.gts.cms.common.employee.repository;

import com.gts.cms.entity.EmployeeDetailStg;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeDetailStgRepository extends JpaRepository<EmployeeDetailStg, Long> {


}
