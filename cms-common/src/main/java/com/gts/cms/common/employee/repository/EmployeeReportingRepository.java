package com.gts.cms.common.employee.repository;

import com.gts.cms.entity.EmployeeDetail;
import com.gts.cms.entity.EmployeeReporting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @author Genesis
 *
 */
@Repository
public interface EmployeeReportingRepository extends JpaRepository<EmployeeReporting, Long>{
	
//	@Query("SELECT e FROM EmployeeDetail e"
//			+ " WHERE (:employeeId is null or e.employeeId = :employeeId)"
//			+ " AND (:userId is null or e.user.userId = :userId)"
//			+ " AND e.isActive = true"
//			)
//	Optional<EmployeeDetail> findByEmployeeIdAndIsActiveTrue(@Param("employeeId") Long employeeId,
//			@Param("userId") Long userId);

	@Query("SELECT DISTINCT ed FROM EmployeeDetail ed" + " JOIN FETCH ed.empAttendances1 ea "
			+ " WHERE 1=1 and (:schoolId is null or ed.school.schoolId = :schoolId)"
			+ " AND (:attendanceDate is null or ea.attendanceDate = :attendanceDate)"
			+ " AND (:isActive is null or ea.isActive = :isActive)" + " AND ed.isActive = true")
	List<EmployeeDetail> findEmployeeAttendanceList(@Param("isActive") Boolean status,
													@Param("schoolId") Long schoolId, @Param("attendanceDate") Date attendanceDate);
	
	
	
	@Query("SELECT e.firstName FROM  EmployeeDetail e" 
			+ " WHERE e.employeeId IN (:employeeIdList)"
			+ " AND e.isActive = true"
			)
	List<String> findEmpDetails(@Param("employeeIdList") List<Long> employeeIdList);
	
	
	@Query(" SELECT er FROM EmployeeReporting er "
			+ " WHERE 1=1 "
			+ " AND DATE(er.toDate) >= :dateOfRelieving "
			+ " AND er.employeeDetail.employeeId= :employeeId "
			+ " AND er.isActive= true ")
	List<EmployeeReporting> findReportingDetails(@Param("employeeId") Long employeeId,@Param("dateOfRelieving") Date dateOfRelieving);

}
