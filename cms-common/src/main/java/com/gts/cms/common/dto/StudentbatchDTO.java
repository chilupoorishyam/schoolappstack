package com.gts.cms.common.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

public class StudentbatchDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long studentbatchId;

	private String batchName;

	private Integer capacity;

	private Date createdDt;

	private Long createdUser;

	private Boolean isActive;

	private BigInteger sortOrder;

	private String reason;

	private Date updatedDt;

	private Long updatedUser;

	private Long courseId;

	private String courseName;

	private String courseCode;

	private Long subtypeId;

	private String subjecttypeName;

	private Long schoolId;
	private String schoolName;
	private String schoolCode;

	public Long getStudentbatchId() {
		return studentbatchId;
	}

	public void setStudentbatchId(Long studentbatchId) {
		this.studentbatchId = studentbatchId;
	}

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public BigInteger getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(BigInteger sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public Long getSubtypeId() {
		return subtypeId;
	}

	public void setSubtypeId(Long subtypeId) {
		this.subtypeId = subtypeId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public String getSubjecttypeName() {
		return subjecttypeName;
	}

	public void setSubjecttypeName(String subjecttypeName) {
		this.subjecttypeName = subjecttypeName;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

}
