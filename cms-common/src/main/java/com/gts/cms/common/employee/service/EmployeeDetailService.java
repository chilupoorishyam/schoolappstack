package com.gts.cms.common.employee.service;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.EmployeeListDTO;
import com.gts.cms.common.dto.EmployeeReportingDTO;
import com.gts.cms.common.employee.dto.EmployeeDetailDTO;
import com.gts.cms.common.employee.dto.EmployeeDetailUploadDTO;

import java.util.List;

public interface EmployeeDetailService {

	public ApiResponse<?> addEmployeeDetailForm(EmployeeDetailDTO employeeDetailDTO);

	public ApiResponse<EmployeeDetailDTO> getEmployeeDetail(Long employeeId, Long userId);

	public ApiResponse<?> updateEmployeeDetail(EmployeeDetailDTO employeeDetailDTO);

	public ApiResponse<Long> deleteEmployeeDetail(Long employeeId);

	public ApiResponse<EmployeeDetailUploadDTO> uploadEmployeeApplicationFiles(
			EmployeeDetailUploadDTO employeeDetailUploadDTO);

	public ApiResponse<List<EmployeeDetailDTO>>
	getAllEmployeesDetails(Long schoolId, Long empDeptId,Boolean status,String aadharNo,String empNumber,String firstName,String mobile);

	public ApiResponse<List<EmployeeListDTO>> getEmployeesList(Long schoolId, Long empDeptId, Boolean status,
			String aadharNo, String empNumber, String firstName, String mobile);
	
	public ApiResponse<List<EmployeeListDTO>> searchEmployeeDetail(Long schoolId,String query,Long deptId,String empStatus);
	
	public ApiResponse<?> addEmployeeManager(EmployeeReportingDTO employeeReportingDTO);
	
	public ApiResponse<List<EmployeeDetailDTO>> getAllEmployeesDetailsByCourseGroup(Boolean status, Long schoolId);

	public ApiResponse<List<EmployeeDetailDTO>> getDeptWiseCounsellor(Long schoolId, Long empDeptId,Boolean status);

	public ApiResponse<?> addEmployeesManager(List<EmployeeReportingDTO> employeeReportingDTOs);
	
}
