package com.gts.cms.common.repository.custom;

public interface QualificationRepositoryCustom {
	Long deleteQualification(Long qualificationId);
}
