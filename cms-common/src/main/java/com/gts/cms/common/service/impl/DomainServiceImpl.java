package com.gts.cms.common.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.PageResponse;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.exception.BadRequestException;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.repository.DomainRepository;
import com.gts.cms.common.service.DomainService;
import com.gts.cms.common.util.DomainUtil;
import com.gts.cms.common.util.SecurityUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class DomainServiceImpl implements DomainService,ApplicationContextAware {
	private static final Logger LOGGER = Logger.getLogger(DomainServiceImpl.class);
	@Autowired
	DomainRepository domainRepository;

	@Autowired
	ObjectMapper objectMapper;
	
	private ApplicationContext applicationContext;

	@Autowired
	org.springframework.validation.Validator validator;

	@Transactional
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	@Cacheable(value="domain", key="#params")
	public ApiResponse<?> findOneEntity(String entityName, Map<String, String> params) {
		Class<?> entityClass = DomainUtil.getEntityClazz(entityName);
		LOGGER.info("findOneEntity method execution started for : "+entityName+" "+params);
		String queryString = getQuery(params, entityName, true);
		Object domainEntity = domainRepository.findOneEntity(entityClass, queryString);
		if (domainEntity == null) {
			//throw new ApiException("No Records(s) Found with given criteria!");
			return new ApiResponse<>(Boolean.FALSE,"No Records(s) Found with given criteria!",HttpStatus.OK.value());
		}
		BaseMapper mapper = getBaseMapper(entityName);
		LOGGER.info("Execution completed on findOneEntity : "+entityName);
		return new ApiResponse<>("Data retrieved successfully!", mapper.convertEntityToDTO(domainEntity));
	}

	@Transactional
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	@Cacheable(value="domain",key="#entityName")
	public ApiResponse<PageResponse> findEntityList(String entityName, Map<String, String> params) {
		long time1=System.currentTimeMillis();
		LOGGER.info("findEntityList method execution started for : "+entityName+" "+params);
		Class<?> entityClass = DomainUtil.getEntityClazz(entityName);
		String query = getQuery(params, entityName, false);
		int pageNumber = getPageNumber(params);
		int pageSize = getPageSize(params);

		PageResponse pageResponse = domainRepository.findEntityList(entityClass, query, pageNumber, pageSize);
		BaseMapper mapper = getBaseMapper(entityName);
		pageResponse.setResultList(mapper.convertEntityListToDTOList(pageResponse.getResultList()));
		long time2=System.currentTimeMillis();
		LOGGER.info("Execution completed on findEntityList : "+entityName+" Total Time token :  "+(time2-time1));
		return new ApiResponse<PageResponse>("Data retrieved successfully!", pageResponse, Boolean.TRUE);
	}

	@Transactional
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	@CacheEvict(value = "domain",allEntries=true)
	public ApiResponse<?> createEntity(String entityName, Map<String, Object> wrappedRequestBody) {
		LOGGER.info("createEntity method execution started for : "+entityName+" "+wrappedRequestBody);
		
		ApiResponse<Object> response = null;
		Object object = null;
		try {
			Class<?> entityClass = DomainUtil.getDTOClazz(entityName + "DTO");
			object = objectMapper.convertValue(wrappedRequestBody, entityClass);
			validate(object);
			BaseMapper mapper = getBaseMapper(entityName);
			object = domainRepository.create(mapper.convertDTOtoEntity(object, null));
			response = new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(), mapper.convertEntityToDTO(object),
					HttpStatus.OK.value());
		} catch (BadRequestException be) {
			throw be;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception Occured while creating entity : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}
		LOGGER.info("Execution completed on createEntity : "+entityName);
		
		return response;
	}
	
	@Transactional
	@Override
	@CachePut(value = "domain",key="#params")
	public ApiResponse<?> updateEntity(String entityName, Map<String, Object> wrappedRequestBody,Map<String, String> params) {
		LOGGER.info("updateEntity method execution started for : "+entityName+" "+wrappedRequestBody);
		
		ApiResponse<Object> response = null;
		Object object = null;
		String queryString = getQuery(params, entityName, true);
		Class<?> entityClass = DomainUtil.getEntityClazz(entityName);
		Object domainEntity = domainRepository.findOneEntity(entityClass, queryString);
		if (domainEntity == null) {
			//throw new ApiException("No Records(s) Found with given criteria!");
			return new ApiResponse<>(Boolean.FALSE,"No Records(s) Found with given criteria!",HttpStatus.OK.value());
		}
		try {
			Class<?> dtoClass = DomainUtil.getDTOClazz(entityName + "DTO");
			object = objectMapper.convertValue(wrappedRequestBody, dtoClass);
			validate(object);
			BaseMapper mapper = getBaseMapper(entityName);
			object = domainRepository.create(mapper.convertDTOtoEntity(object, domainEntity));
			response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), mapper.convertEntityToDTO(object),
					HttpStatus.OK.value());
		} catch (BadRequestException be) {
			throw be;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Exception Occured while updating entity : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}
		LOGGER.info("Execution completed on updateEntity : "+entityName);

		return response;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Transactional
	@Override
	@CacheEvict(value = "domain",key="#params")
	public ApiResponse<?> deleteOneEntity(String entityName, Map<String, String> params) {
		LOGGER.info("deleteOneEntity method execution started for : "+entityName+" "+params);
		
		ApiResponse<Object> response = null;
		Object entityPatch = null;
		Object entity = null;
		String queryString = getQuery(params, entityName, true);
		try {
			Class<?> entityClass = DomainUtil.getEntityClazz(entityName);
			Map<String, Object> requestBody = new HashMap<>();
			requestBody.put("isActive", Boolean.FALSE);
			requestBody.put("updatedDt", new Date());
			requestBody.put("updatedUser", SecurityUtil.getCurrentUser());
			entityPatch = objectMapper.convertValue(requestBody, entityClass);
			entity = domainRepository.deleteOneEntity(entityPatch, queryString);
			BaseMapper mapper = getBaseMapper(entityName);
			response = new ApiResponse<>(Messages.DELETE_SUCCESS.getValue(), mapper.convertEntityToDTO(entity),
					HttpStatus.OK.value());
		} catch (Exception e) {
			LOGGER.error("Exception Occured while deleting entity : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}
		LOGGER.info("Execution completed on deleteOneEntity : "+entityName);

		return response;
	}

	private void validate(Object object) {
		Errors result = new BeanPropertyBindingResult(object, "obj");
		validator.validate(object, result);
		if (result.hasErrors()) {
			throw new BadRequestException(result);
		}

	}

	private int getPageSize(Map<String, String> params) {
		String pageSizeStr = params.get("size");
		if (pageSizeStr == null) {
			return 100;
		}
		try {
			return Integer.parseInt(pageSizeStr);
		} catch (NumberFormatException e) {
			throw new BadRequestException("");
		}
	}

	private int getPageNumber(Map<String, String> params) {
		String pageNumberStr = params.get("page");
		if (pageNumberStr == null) {
			return 0;
		}
		try {
			return Integer.parseInt(pageNumberStr);
		} catch (NumberFormatException e) {
			throw new BadRequestException("");
		}
	}

	private String getQuery(Map<String, String> params, String entityName, boolean required) {
		String queryString = params.get("query");
		if (queryString == null && required) {
			throw new ApiException("Query String is required.");
		}
		return queryString;
	}

	@SuppressWarnings("rawtypes")
	private BaseMapper getBaseMapper(String entityName) {
		BaseMapper mapper = null;
			entityName=entityName.substring(0,1).toLowerCase()+entityName.substring(1)+"Mapper";
			mapper=(BaseMapper)applicationContext.getBean(entityName);
		return mapper;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext=applicationContext;
		
	}

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ApiResponse<PageResponse> searchEntityList(String entityName, Map<String, String> params) {
		long time1=System.currentTimeMillis();
		LOGGER.info("searchEntityList method execution started for : "+entityName+" "+params);
		Class<?> entityClass = DomainUtil.getEntityClazz(entityName);
		String query = params.get("q");
		String fields = params.get("fields");
		int pageNumber = getPageNumber(params);
		int pageSize = getPageSize(params);

		PageResponse pageResponse = domainRepository.searchEntityList(entityClass, query,fields, pageNumber, pageSize);
		BaseMapper mapper = getBaseMapper(entityName);
		pageResponse.setResultList(mapper.convertEntityListToDTOList(pageResponse.getResultList()));
		long time2=System.currentTimeMillis();
		LOGGER.info("Execution completed on searchEntityList : "+entityName+" Total Time token :  "+(time2-time1));
		return new ApiResponse<PageResponse>("Data retrieved successfully!", pageResponse, Boolean.TRUE);
	}
	

}
