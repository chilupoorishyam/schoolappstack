package com.gts.cms.common.repository;

import com.gts.cms.entity.GeneralPaymentSetting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GeneralPaymentSettingRepository extends JpaRepository<GeneralPaymentSetting, Long> {
    @Query("SELECT g FROM  GeneralPaymentSetting g"
            + " WHERE g.school.schoolId=:schoolId"
    )
    GeneralPaymentSetting findBySchoolId(@Param("schoolId") Long schoolId);
}
