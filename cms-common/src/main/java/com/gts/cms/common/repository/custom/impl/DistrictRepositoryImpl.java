package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.DistrictRepositoryCustom;
import com.gts.cms.entity.District;
import com.gts.cms.entity.QDistrict;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * created by Naveen on 04/09/2018
 * 
 */
@Repository
public class DistrictRepositoryImpl extends QuerydslRepositorySupport implements DistrictRepositoryCustom {

	public DistrictRepositoryImpl() {
		super(District.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<District> findAllByStateId(Long stateId) {
		JPQLQuery<District> query = from(QDistrict.district)
				.where(QDistrict.district.state.stateId.eq(stateId).and(QDistrict.district.isActive.eq(true)));
		return query.fetch();
	}

}
