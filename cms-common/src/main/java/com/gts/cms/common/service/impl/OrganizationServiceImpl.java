package com.gts.cms.common.service.impl;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.OrganizationDTO;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.repository.OrganizationRepository;
import com.gts.cms.common.service.FileService;
import com.gts.cms.common.service.OrganizationService;
import com.gts.cms.common.util.FileUtil;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;


@Service
public class OrganizationServiceImpl implements OrganizationService {

	private static final Logger LOGGER = Logger.getLogger(OrganizationServiceImpl.class);

	private static final String FILE_ORG = "org_";
	private static final String FILE_NAME = "logo.";

	private static final String fileSeparator = FileUtil.getFileSeparator();
	
	@Autowired
	OrganizationRepository organizationRepository;
	
	@Autowired
	FileService fileService;

	
	
	@Override
	@Transactional
	public ApiResponse<OrganizationDTO> uploadOrganizationLogo(OrganizationDTO organizationDTO) {
		ApiResponse<OrganizationDTO> response = null;

		if (null == organizationDTO || null == organizationDTO.getOrganizationId()) {
			throw new ApiException(Messages.RECORD_ID_MUST_NOT_NULL.getValue());
		}

		try {
			String collegeLogoName = null;
			if (CollectionUtils.isEmpty(organizationDTO.getFiles())) {
				throw new ApiException(Messages.NO_FILES_FOUND.getValue());
			} else {

				MultipartFile logoFile = organizationDTO.getFiles().get("organizationLogo");

				if (logoFile != null) {
					try {
						collegeLogoName = fileService.uploadFile(logoFile,
								FILE_ORG + organizationDTO.getOrgCode()  + fileSeparator + FILE_NAME
										+ FilenameUtils.getExtension(logoFile.getOriginalFilename()));
						organizationDTO.setLogoUploadStatus(Status.SUCESS.getName());

					} catch (Exception e) {
						organizationDTO.setLogoUploadStatus(Messages.FAILED_TO_UPLOAD_IMAGES.getValue());
					}
				}


				if (collegeLogoName != null ) {
					try {
						Long updatedCount = organizationRepository.uploadOrganizationLogo(organizationDTO.getOrganizationId(), collegeLogoName);
						organizationDTO.setFiles(null);
						if (updatedCount != null && updatedCount != 0) {
							response = new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(), organizationDTO,
									HttpStatus.OK.value());
						} else {
							throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue());
						}
					} catch (Exception e) {
						response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(),HttpStatus.OK.value());
					}
				} else {
					response = new ApiResponse<>(Messages.FAILED_TO_UPLOAD_IMAGES.getValue(), organizationDTO,
							HttpStatus.OK.value());
				}

			}

		} catch (Exception e) {
			LOGGER.error("Exception Occured while uploading college logo: " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}
		return response;
	}
}
