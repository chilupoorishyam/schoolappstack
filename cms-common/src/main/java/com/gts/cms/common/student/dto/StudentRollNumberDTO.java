package com.gts.cms.common.student.dto;

import java.io.Serializable;

public class StudentRollNumberDTO implements Serializable{
	private static final long serialVersionUID = 1L;

	private Long studentId;
	
	private String rollNumber;

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getRollNumber() {
		return rollNumber;
	}

	public void setRollNumber(String rollNumber) {
		this.rollNumber = rollNumber;
	}
	
	
}
