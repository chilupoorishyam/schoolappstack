package com.gts.cms.common.exception;
/**
 * created by sathish on 02/09/2018.
 */
public class AuthenticationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new authentication exception.
	 */
	public AuthenticationException() {
		super();
	}

	/**
	 * Instantiates a new authentication exception.
	 *
	 * @param msg the msg
	 */
	public AuthenticationException(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new authentication exception.
	 *
	 * @param th the th
	 */
	public AuthenticationException(Throwable th) {
		super(th);
	}
}
