package com.gts.cms.common.repository.custom;

import com.gts.cms.entity.FinancialYear;

import java.util.List;

/**
 * created by Naveen on 04/09/2018
 * 
 */
public interface FinancialYearRepositoryCustom {
	public Long deleteFinancialYear(Long financialYearId);
	public FinancialYear findByIdAndIsActiveTrue(Long financialYearId);
	public List<FinancialYear> findByOrganizationIdAndSchoolIdAndIsActiveTrue(Long schoolId);
	public FinancialYear findFinancialYearBySchoolId(Long schoolId);

}
