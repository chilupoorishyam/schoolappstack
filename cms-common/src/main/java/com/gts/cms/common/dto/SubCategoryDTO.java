package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

public class SubCategoryDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long subCategoryId;
	
	private String achievementSubcategory;
	
	private String achievementSubcategoryCode;
	
	private Date createdDt;
	
	private Long createdUser;
	
	private Boolean isActive;
	
	private String reason;
	
	private String subcategoryLogoPath;
	
	private Date updatedDt;
	
	private Long updatedUser;
	
	private Long categoryId;
	private String achievementCategoryCode;
	
	private Long organizationId;
	private String orgCode;
	private String orgName;
	public Long getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(Long subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	public String getAchievementSubcategory() {
		return achievementSubcategory;
	}
	public void setAchievementSubcategory(String achievementSubcategory) {
		this.achievementSubcategory = achievementSubcategory;
	}
	public String getAchievementSubcategoryCode() {
		return achievementSubcategoryCode;
	}
	public void setAchievementSubcategoryCode(String achievementSubcategoryCode) {
		this.achievementSubcategoryCode = achievementSubcategoryCode;
	}
	public Date getCreatedDt() {
		return createdDt;
	}
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	public Long getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getSubcategoryLogoPath() {
		return subcategoryLogoPath;
	}
	public void setSubcategoryLogoPath(String subcategoryLogoPath) {
		this.subcategoryLogoPath = subcategoryLogoPath;
	}
	public Date getUpdatedDt() {
		return updatedDt;
	}
	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}
	public Long getUpdatedUser() {
		return updatedUser;
	}
	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public String getAchievementCategoryCode() {
		return achievementCategoryCode;
	}
	public void setAchievementCategoryCode(String achievementCategoryCode) {
		this.achievementCategoryCode = achievementCategoryCode;
	}
	public Long getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}
	public String getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	
}