package com.gts.cms.common.student.repository.custom;

import com.gts.cms.entity.StudentMedicalCertificate;

import java.util.List;

/**
 * @author Genesis
 *
 */
public interface StudentMedicalCertificateRepositoryCustom {

	List<StudentMedicalCertificate> findAllEnquiryForms(Long schoolId);

	List<StudentMedicalCertificate> findAllEnquiryForms(Long schoolId, Long academicYearId, Long courseYearId,
														Long groupSectionId, Long studentId, Boolean status);

	Long uploadMedicalCertificateFiles(Long studentMedicalCertificateId, String fileName1, String fileName2,
			String fileName3);
}
