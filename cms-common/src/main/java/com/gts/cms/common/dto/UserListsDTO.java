package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.List;

public class UserListsDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long userId;
	private String firstName;
	private String lastName;
	private String userName;
	private String userRole;
	private String email;
	private Long organizationId;
	private String orgName;
	private String orgCode;
	private Long schoolId;
	private String schoolName;
	private String schoolCode;
	// private Long roleId;
	private Long userTypeId;
	private Boolean isActive;
	private Boolean isEditable;
	private Boolean isReset;
	private String mobileNumber;
	private Long academicYearId;
	private String password;
	private List<StudentDetailListDTO> studentDetailListDTOs;
	private List<UserRoleDTO> userRolesDTOs;

	public Long getUserId() {
		return userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getUserName() {
		return userName;
	}

	public String getUserRole() {
		return userRole;
	}

	public String getEmail() {
		return email;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	/*
	 * public Long getRoleId() { return roleId; }
	 */

	public Long getUserTypeId() {
		return userTypeId;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public Boolean getIsEditable() {
		return isEditable;
	}

	public Boolean getIsReset() {
		return isReset;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public Long getAcademicYearId() {
		return academicYearId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}
	/*
	 * public void setRoleId(Long roleId) { this.roleId = roleId; }
	 */

	public void setUserTypeId(Long userTypeId) {
		this.userTypeId = userTypeId;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public void setIsEditable(Boolean isEditable) {
		this.isEditable = isEditable;
	}

	public void setIsReset(Boolean isReset) {
		this.isReset = isReset;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}


	public List<StudentDetailListDTO> getStudentDetailListDTOs() {
		return studentDetailListDTOs;
	}


	public void setStudentDetailListDTOs(List<StudentDetailListDTO> studentDetailListDTOs) {
		this.studentDetailListDTOs = studentDetailListDTOs;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<UserRoleDTO> getUserRolesDTOs() {
		return userRolesDTOs;
	}

	public void setUserRolesDTOs(List<UserRoleDTO> userRolesDTOs) {
		this.userRolesDTOs = userRolesDTOs;
	}

}
