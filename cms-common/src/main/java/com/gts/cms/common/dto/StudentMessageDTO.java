package com.gts.cms.common.dto;

import java.io.Serializable;

public class StudentMessageDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long studentId;
	private Long schoolId;
	private String message;
	private Boolean messageStatus;

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(Boolean messageStatus) {
		this.messageStatus = messageStatus;
	}
	
}
