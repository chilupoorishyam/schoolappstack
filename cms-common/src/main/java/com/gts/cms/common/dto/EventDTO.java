package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;


public class EventDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	
	private Long eventId;
	
	private Long schoolId;
	
	private Long academicYearId;
	
	private Long employeeId;
	
	private String eventName;
	
	private Boolean isHoliday;
	
	private Long eventTypeId;
	
	private String description;
	
	private Date startDate;
	
	private Date endDate;
	
	private String organizerDetails;
	
	private Date publishDate;
	
	private Boolean isPublished;
	
	private Boolean isWeekOff;
	
	private Long eventStatusId;
	
	private Boolean canEnableReminders;
	
	private Integer remainder1BeforeXDays;
	
	private Integer remainder2BeforeXDays;
	
	private Integer remainder3BeforeXDays;
	
	private Boolean remainderModeEmail;
	
	private String emailMessage;
	
	private Boolean remainderModeSmsPopup;
	
	private String smsPopupMessage;
	
	private String reason;

	@NotNull(message="isActive is required")
	private Boolean isActive;

	
	private Date createdDt;

	private Long createdUser;

	private Date updatedDt;

	private Long updatedUser;

	private String schoolName;
	
	private String schoolCode;
	
	private String academicYear;
	
	private String firstName;

	private String middleName;
	
	private String lastName;
	
	private String eventTypeName;
	
	private String eventStatusCode;
	
	private String eventStatusDisplayName;
	
	private List<EventAudienceDTO> EventAudiences;
	
	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public Long getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}

	public Long getEmployeeId() {
		return employeeId;
	}
	
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	public Boolean getIsHoliday() {
		return isHoliday;
	}

	public void setIsHoliday(Boolean isHoliday) {
		this.isHoliday = isHoliday;
	}

	public Long getEventTypeId() {
		return eventTypeId;
	}
	
	public void setEventTypeId(Long eventTypeId) {
		this.eventTypeId = eventTypeId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getOrganizerDetails() {
		return organizerDetails;
	}

	public void setOrganizerDetails(String organizerDetails) {
		this.organizerDetails = organizerDetails;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public Boolean getIsPublished() {
		return isPublished;
	}

	public void setIsPublished(Boolean isPublished) {
		this.isPublished = isPublished;
	}

	public Long getEventStatusId() {
		return eventStatusId;
	}
	
	public void setEventStatusId(Long eventStatusId) {
		this.eventStatusId = eventStatusId;
	}

	public Boolean getCanEnableReminders() {
		return canEnableReminders;
	}

	public void setCanEnableReminders(Boolean canEnableReminders) {
		this.canEnableReminders = canEnableReminders;
	}

	public Integer getRemainder1BeforeXDays() {
		return remainder1BeforeXDays;
	}

	public void setRemainder1BeforeXDays(Integer remainder1BeforeXDays) {
		this.remainder1BeforeXDays = remainder1BeforeXDays;
	}

	public Integer getRemainder2BeforeXDays() {
		return remainder2BeforeXDays;
	}

	public void setRemainder2BeforeXDays(Integer remainder2BeforeXDays) {
		this.remainder2BeforeXDays = remainder2BeforeXDays;
	}

	public Integer getRemainder3BeforeXDays() {
		return remainder3BeforeXDays;
	}

	public void setRemainder3BeforeXDays(Integer remainder3BeforeXDays) {
		this.remainder3BeforeXDays = remainder3BeforeXDays;
	}

	public Boolean getRemainderModeEmail() {
		return remainderModeEmail;
	}

	public void setRemainderModeEmail(Boolean remainderModeEmail) {
		this.remainderModeEmail = remainderModeEmail;
	}

	public String getEmailMessage() {
		return emailMessage;
	}

	public void setEmailMessage(String emailMessage) {
		this.emailMessage = emailMessage;
	}

	public Boolean getRemainderModeSmsPopup() {
		return remainderModeSmsPopup;
	}

	public void setRemainderModeSmsPopup(Boolean remainderModeSmsPopup) {
		this.remainderModeSmsPopup = remainderModeSmsPopup;
	}

	public String getSmsPopupMessage() {
		return smsPopupMessage;
	}

	public void setSmsPopupMessage(String smsPopupMessage) {
		this.smsPopupMessage = smsPopupMessage;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEventTypeName() {
		return eventTypeName;
	}

	public void setEventTypeName(String eventTypeName) {
		this.eventTypeName = eventTypeName;
	}

	public String getEventStatusCode() {
		return eventStatusCode;
	}

	public void setEventStatusCode(String eventStatusCode) {
		this.eventStatusCode = eventStatusCode;
	}

	public String getEventStatusDisplayName() {
		return eventStatusDisplayName;
	}

	public void setEventStatusDisplayName(String eventStatusDisplayName) {
		this.eventStatusDisplayName = eventStatusDisplayName;
	}

	public List<EventAudienceDTO> getEventAudiences() {
		return EventAudiences;
	}

	public void setEventAudiences(List<EventAudienceDTO> eventAudiences) {
		EventAudiences = eventAudiences;
	}

	public Boolean getIsWeekOff() {
		return isWeekOff;
	}

	public void setIsWeekOff(Boolean isWeekOff) {
		this.isWeekOff = isWeekOff;
	}
}