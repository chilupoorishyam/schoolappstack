package com.gts.cms.common.student.service;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.student.dto.StudentDetailDTO;
import com.gts.cms.common.student.dto.StudentDetailStgDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StudentDetailStgService {
    ApiResponse<List<StudentDetailStgDTO>> uploadExcel(StudentDetailStgDTO studentDetailStgDTO);

    ApiResponse<List<StudentDetailStgDTO>> getStgStudentDetails();

    ApiResponse<List<StudentDetailDTO>> processStgStudentDetails();
}
