package com.gts.cms.common.util;

public enum PdfConstants {
	PDF_PATH("C:\\Users\\Genesis\\Desktop\\pdf"),
	PDF_STYLE_PATH("C:\\SchoolVDC\\nov\\schoolAppStack\\cms-common\\src\\main\\java\\com\\gts\\cms\\pdf\\"),
	LOGO("C:\\Users\\Genesis\\Desktop\\pdf\\vaagdevi.JPG");

	private String value;

	private PdfConstants(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
