
package com.gts.cms.common.exception.handler;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.exception.AuthenticationException;
import com.gts.cms.common.exception.InternalServerProblemException;
import com.gts.cms.common.exception.AuthorizedException;
import com.gts.cms.common.exception.BadRequestException;
import com.gts.cms.common.exception.JsonException;
import com.gts.cms.common.exception.UnknownException;

/**
 * created by sathish on 01/09/2018.
 */
public class ExceptionHandler {
	
	private ExceptionHandler() {
		
	}
	
	/**
	 * Handle.
	 *
	 * @param exception the exception
	 * @return the api response
	 */
	public static ApiResponse<?> handle(Exception exception) {
		ApiResponse<?> response=new ApiResponse<>();
		  response.setSuccess(Boolean.FALSE);
		  response.setMessage(exception.getMessage());
		return response;
	}
	
	/**
	 * Handle.
	 *
	 * @param exception the exception
	 * @return the api response
	 */
	public static ApiResponse<?> handle(JsonException exception) {
		ApiResponse<?> response=new ApiResponse<>();
		  response.setSuccess(Boolean.FALSE);
		  response.setMessage("Unable to process your request!Please try again");
		return response;
	}
	
	/**
	 * Handle.
	 *
	 * @param exception the exception
	 * @return the api response
	 */
	public static ApiResponse<?> handle(BadRequestException exception) {
		ApiResponse<?> response=new ApiResponse<>();
		  response.setSuccess(Boolean.FALSE);
		  response.setMessage("Bad Request");
		return response;
	}
	
	/**
	 * Handle.
	 *
	 * @param exception the exception
	 * @return the api response
	 */
	public static ApiResponse<?> handle(AuthenticationException exception) {
		ApiResponse<?> response=new ApiResponse<>();
		  response.setSuccess(Boolean.FALSE);
		  response.setMessage("Authentication Failure!Please try again");
		return response;
	}

	/**
	 * Handle.
	 *
	 * @param exception the exception
	 * @return the api response
	 */
	public static ApiResponse<?> handle(AuthorizedException exception) {
		ApiResponse<?> response=new ApiResponse<>();
		  response.setSuccess(Boolean.FALSE);
		  response.setMessage("You are not authorized user");
		return response;
	}
	
	/**
	 * Handle.
	 *
	 * @param exception the exception
	 * @return the api response
	 */
	public static ApiResponse<?> handle(InternalServerProblemException exception) {
		ApiResponse<?> response=new ApiResponse<>();
		  response.setSuccess(Boolean.FALSE);
		  response.setMessage("Unable to process your request!Please try again");
		return response;
	}
	
	/**
	 * Handle.
	 *
	 * @param exception the exception
	 * @return the api response
	 */
	public static ApiResponse<?> handle(UnknownException exception) {
		ApiResponse<?> response=new ApiResponse<>();
		  response.setSuccess(Boolean.FALSE);
		  response.setMessage("Unknown problem");
		return response;
	}
	
	
}
