package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.SubCasteDTO;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.Caste;
import com.gts.cms.entity.SubCaste;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class SubCasteMapper implements BaseMapper<SubCaste, SubCasteDTO> {
	@Override
	public SubCasteDTO convertEntityToDTO(SubCaste subCaste) {
		SubCasteDTO subCasteDTO = null;
		if (subCaste != null) {
			subCasteDTO = new SubCasteDTO();

			subCasteDTO.setSubCasteId(subCaste.getSubCasteId());

			if (subCaste.getCaste() != null) {
				subCasteDTO.setCasteId(subCaste.getCaste().getCasteId());
				subCasteDTO.setCaste(subCaste.getCaste().getCaste());
			}

			subCasteDTO.setSubCaste(subCaste.getSubCaste());
			subCasteDTO.setSortOrder(subCaste.getSortOrder());
			subCasteDTO.setIsEligibleForReservation(subCaste.getIsEligibleForReservation());
			subCasteDTO.setIsActive(subCaste.getIsActive());

		}
		return subCasteDTO;
	}

	@Override
	public SubCaste convertDTOtoEntity(SubCasteDTO subCasteDto, SubCaste subCaste) {
		if (null == subCaste) {
			subCaste = new SubCaste();
			subCaste.setCreatedDt(new Date());
			subCaste.setCreatedUser(SecurityUtil.getCurrentUser());
			subCaste.setIsActive(Status.ACTIVE.getId());
		} else {
			subCaste.setIsActive(subCasteDto.getIsActive());
		}

		subCaste.setSubCasteId(subCasteDto.getSubCasteId());
		if (subCasteDto.getCasteId() != null) {
			Caste caste = new Caste();
			caste.setCasteId(subCasteDto.getCasteId());
			caste.setCaste(subCasteDto.getCaste());
			subCaste.setCaste(caste);
		}
		subCaste.setSubCaste(subCasteDto.getSubCaste());
		subCaste.setSortOrder(subCasteDto.getSortOrder());
		subCaste.setIsEligibleForReservation(subCasteDto.getIsEligibleForReservation());
		subCaste.setUpdatedDt(new Date());
		subCaste.setUpdatedUser(SecurityUtil.getCurrentUser());
		return subCaste;
	}

	@Override
	public List<SubCaste> convertDTOListToEntityList(List<SubCasteDTO> subCasteDTOList) {
		List<SubCaste> subCasteList = new ArrayList<>();
		subCasteDTOList.forEach(subCasteDTO -> subCasteList.add(convertDTOtoEntity(subCasteDTO, null)));
		return subCasteList;

	}

	@Override
	public List<SubCasteDTO> convertEntityListToDTOList(List<SubCaste> subCasteList) {
		List<SubCasteDTO> subCasteDTOList = new ArrayList<>();
		subCasteList.forEach(subCaste -> subCasteDTOList.add(convertEntityToDTO(subCaste)));
		return subCasteDTOList;
	}

}
