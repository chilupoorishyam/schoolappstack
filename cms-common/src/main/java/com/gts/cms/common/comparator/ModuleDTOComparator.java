package com.gts.cms.common.comparator;

import java.util.Comparator;

import com.gts.cms.common.dto.ModuleDTO;

public class ModuleDTOComparator implements Comparator<ModuleDTO>{

	@Override
	public int compare(ModuleDTO o1, ModuleDTO o2) {
		 //return 
				 int x = o1.getSortOrder().compareTo(o2.getSortOrder());
				 
				 if (x == 0) {
					if(!o1.getModuleId().equals(o2.getModuleId())) {
						x=1;
					}
				 }
		 
		 return x;
	}

}
