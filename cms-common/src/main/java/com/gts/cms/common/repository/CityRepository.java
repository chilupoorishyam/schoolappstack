package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.CityRepositoryCustom;
import com.gts.cms.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * created by Naveen(Auto) on 02/09/2018
 * 
 */
@Repository
public interface CityRepository extends JpaRepository<City, Long>, CityRepositoryCustom {

    Optional<City> findByCityName(String cityCode);


}
