package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;

public class UserDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long userId;
	@NotNull(message = "firstName is required.")
	private String firstName;
	private String lastName;
	@NotNull(message = "UserName is required.")
	private String userName;
	private String userRole;
	@NotNull(message = "email is required.")
	private String email;
	@NotNull(message = "Organization Id is required.")
	private Long organizationId;
	private String organizationName;
	private Long schoolId;
	private String schoolName;
	
	private Long countryId;
	private String countryName;
	private String countryCode;
	
	private Long stateId;
	private String stateName;
	private String stateCode;
	
	private Long districtId;
	private String districtName;
	private String districtCode;
	
	private Long cityId;
	private String cityName;
	private String cityCode;
	
	private Long roleId;
	private Set<ModuleDTO> modules;
	
	private Date createdDt;
	private Long createdUser;
	private Long userTypeId;
	@NotNull(message = "isActive is required.")
	private Boolean isActive;
	@NotNull(message = "isEditable is required.")
	private Boolean isEditable;
	@NotNull(message = "isReset is required.")
	private Boolean isReset;
	@NotNull(message = "mobileNumber is required.")
	private String mobileNumber;
	@NotNull(message = "password is required.")
	private String password;
	private Integer passwordAttempts;
	private Date passwordExpDate;
	private String reason;
	private String resetPasswordCode;
	private Date updatedDt;
	private Long updatedUser;
	private String schoolCode;
	private String organizationCode;
	private Long academicYearId;
	private String academicYear;
	private List<UserRoleDTO> userRoles;
	
	private Set<PageDTO> pages;
	
	private String userTypeCode;
	private String userTypeName;
	private Long studentId;
	private Long employeeId;
	
	private String latestAndroidVersion;
	private String latestIOSVersion;
	private String forceLatestAndroidVersion;
	private String forceLatestIOSVersion;
	private String iosVersion;
	private String androidVersion;
	
	

	public Set<ModuleDTO> getModules() {
		return modules;
	}

	public void setModules(Set<ModuleDTO> modules) {
		this.modules = modules;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public String getOrganizationCode() {
		return organizationCode;
	}

	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public Long getRoleId() {
		return roleId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Set<PageDTO> getPages() {
		return pages;
	}

	public void setPages(Set<PageDTO> pages) {
		this.pages = pages;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Long getUserTypeId() {
		return userTypeId;
	}

	public void setUserTypeId(Long userTypeId) {
		this.userTypeId = userTypeId;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsEditable() {
		return isEditable;
	}

	public void setIsEditable(Boolean isEditable) {
		this.isEditable = isEditable;
	}

	public Boolean getIsReset() {
		return isReset;
	}

	public void setIsReset(Boolean isReset) {
		this.isReset = isReset;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getPasswordAttempts() {
		return passwordAttempts;
	}

	public void setPasswordAttempts(Integer passwordAttempts) {
		this.passwordAttempts = passwordAttempts;
	}

	public Date getPasswordExpDate() {
		return passwordExpDate;
	}

	public void setPasswordExpDate(Date passwordExpDate) {
		this.passwordExpDate = passwordExpDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getResetPasswordCode() {
		return resetPasswordCode;
	}

	public void setResetPasswordCode(String resetPasswordCode) {
		this.resetPasswordCode = resetPasswordCode;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public Long getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}

	public String getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}

	public List<UserRoleDTO> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(List<UserRoleDTO> userRoles) {
		this.userRoles = userRoles;
	}

	public String getUserTypeCode() {
		return userTypeCode;
	}

	public void setUserTypeCode(String userTypeCode) {
		this.userTypeCode = userTypeCode;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getLatestAndroidVersion() {
		return latestAndroidVersion;
	}

	public void setLatestAndroidVersion(String latestAndroidVersion) {
		this.latestAndroidVersion = latestAndroidVersion;
	}

	public String getLatestIOSVersion() {
		return latestIOSVersion;
	}

	public void setLatestIOSVersion(String latestIOSVersion) {
		this.latestIOSVersion = latestIOSVersion;
	}

	public String getForceLatestAndroidVersion() {
		return forceLatestAndroidVersion;
	}

	public void setForceLatestAndroidVersion(String forceLatestAndroidVersion) {
		this.forceLatestAndroidVersion = forceLatestAndroidVersion;
	}

	public String getForceLatestIOSVersion() {
		return forceLatestIOSVersion;
	}

	public void setForceLatestIOSVersion(String forceLatestIOSVersion) {
		this.forceLatestIOSVersion = forceLatestIOSVersion;
	}

	public String getUserTypeName() {
		return userTypeName;
	}

	public void setUserTypeName(String userTypeName) {
		this.userTypeName = userTypeName;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public Long getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Long districtId) {
		this.districtId = districtId;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getDistrictCode() {
		return districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getIosVersion() {
		return iosVersion;
	}

	public void setIosVersion(String iosVersion) {
		this.iosVersion = iosVersion;
	}

	public String getAndroidVersion() {
		return androidVersion;
	}

	public void setAndroidVersion(String androidVersion) {
		this.androidVersion = androidVersion;
	}
}
