package com.gts.cms.common.enums;

public enum FCMActions {
	PAGE_LEAVES("Leaves"),
	PAGE_EVENTS("Events"),
	PAGE_FEES("Fees"),
	PAGE_WORKLOAD("Workload"),
	ANNOUNCEMENT("Announcements"),
	ABSENT("Absent");
	private String value;

	private FCMActions(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
