package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

public class NetworkMemberDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long networkMemberId;
	private Date createdDt;
	private Long createdUser;
	private Long amsProfileId;
	private String cityName;
	private String companyName;
	private String amsFirstName;
	private String lastName;
	private Long organizationId;
	private String orgCode;
	private String orgName;
	private Boolean isActive;
	private Date memberJoinDate;
	private Date memberLeftDate;
	private String reason;
	private Date updatedDt;
	private Long updatedUser;

	public Long getNetworkMemberId() {
		return networkMemberId;
	}

	public void setNetworkMemberId(Long networkMemberId) {
		this.networkMemberId = networkMemberId;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setAmsProfileId(Long amsProfileId) {
		this.amsProfileId = amsProfileId;
	}

	public Long getAmsProfileId() {
		return amsProfileId;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setAmsFirstName(String amsFirstName) {
		this.amsFirstName = amsFirstName;
	}

	public String getAmsFirstName() {
		return amsFirstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setMemberJoinDate(Date memberJoinDate) {
		this.memberJoinDate = memberJoinDate;
	}

	public Date getMemberJoinDate() {
		return memberJoinDate;
	}

	public void setMemberLeftDate(Date memberLeftDate) {
		this.memberLeftDate = memberLeftDate;
	}

	public Date getMemberLeftDate() {
		return memberLeftDate;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReason() {
		return reason;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}
}