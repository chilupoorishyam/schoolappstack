package com.gts.cms.common.service.impl;

import com.gts.cms.common.comparator.ModuleDTOComparator;
import com.gts.cms.common.comparator.PageDTOComparator;
import com.gts.cms.common.comparator.SubModuleDTOComparator;
import com.gts.cms.common.dto.*;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.exception.BadRequestSearchException;
import com.gts.cms.common.mapper.UserListMapper;
import com.gts.cms.common.mapper.UserMapper;
import com.gts.cms.common.mapper.UserRoleMapper;
import com.gts.cms.common.repository.*;
import com.gts.cms.common.service.UserService;
import com.gts.cms.common.service.report.CommonUtils;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * The Class UserServiceImpl.
 */
@Service
public class UserServiceImpl implements UserService {

    /**
     * The Constant USER_DETAILS_RETERIVED_SUCCESS.
     */
    private static final String USER_DETAILS_RETERIVED_SUCCESS = "User Details Retrived Successfully";

    /**
     * The Constant USER_DETAILS_RETERIVED_FAILURE.
     */
    private static final String USER_DETAILS_RETERIVED_FAILURE = "You are not Authorized";

    @Value("${spring.mail.username}")
    private String username;

    /**
     * The user repository.
     */
    @Autowired
    UserRepository userRepository;

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserListMapper userListMapper;

    @Autowired
    UserTypeRepository userTypeRepository;

    @Autowired
    UserRoleRepository userRoleRepository;

    @Autowired
    UserRoleMapper userRoleMapper;


    @Autowired
    GeneralDetailRepository generalDetailRepository;

    @Autowired
    RoleRepository roleRepository;

    /*@Autowired
    StudentDetailListMapper studentDetailListMapper;*/


    @Autowired
    PasswordResetTokenRepository passwordTokenRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;



    @Value("${appconfig.latest-android-version}")
    private String latestAndroidVersion;
    @Value("${appconfig.latest-ios-version}")
    private String latestIOSVersion;
    @Value("${appconfig.force-latest-android-version}")
    private String forceLatestAndroidVersion;
    @Value("${appconfig.force-latest-ios-version}")
    private String forceLatestIOSVersion;
    @Value("${appconfig.iosVersion}")
    private String iosVersion;
    @Value("${appconfig.androidVersion}")
    private String androidVersion;

    @Value("${spring.datasource.url}")
    private String dbHost;
    
    /*@Autowired
    private SchoolRepository schoolRepo;*/


    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);

    /*
     * (non-Javadoc)
     *
     * @see com.gts.cms.common.service.UserService#getUserDetails(java.lang.Long)
     */
    @Override
    @Transactional
    public ApiResponse<UserDTO> getUserDetails(Long id, Boolean status, Boolean isMobile) {
        long time1 = System.currentTimeMillis();
        LOGGER.debug("entered into getUserDetails() " + id);
        ApiResponse<UserDTO> response = null;
        try {
            User user = userRepository.getUserDetails(id, status);
            if (user != null && !CollectionUtils.isEmpty(user.getUserRoles())) {
                UserDTO userDTO = new UserDTO();
                userDTO.setLatestAndroidVersion(latestAndroidVersion);
                userDTO.setForceLatestAndroidVersion(forceLatestAndroidVersion);
                userDTO.setLatestIOSVersion(latestIOSVersion);
                userDTO.setForceLatestIOSVersion(forceLatestIOSVersion);
                userDTO.setIosVersion(iosVersion);
                userDTO.setAndroidVersion(androidVersion);
                userDTO.setUserId(user.getUserId());
                userDTO.setUserName(user.getUserName());
                userDTO.setFirstName(user.getFirstName());
                if (user.getUserType() != null) {
                    userDTO.setUserTypeId(user.getUserType().getUserTypeId());
                    userDTO.setUserTypeName(user.getUserType().getUserTypeName());
                    userDTO.setUserTypeCode(user.getUserType().getUserTypeCode());
                }
                userDTO.setLastName(user.getLastName());
                userDTO.setMobileNumber(user.getMobileNumber());
                userDTO.setEmail(user.getEmail());
                userDTO.setSchoolCode(SecurityUtil.getSchoolCode());
                userDTO.setSchoolId(SecurityUtil.getSchoolId());
                userDTO.setSchoolName(SecurityUtil.getSchoolName());
               
                /*if(SecurityUtil.getSchoolId()!=null) {
                	School school=schoolRepo.findBySchool(SecurityUtil.getSchoolId());
                	if(school!=null) {
                		SchoolDTO schoolDTO=schoolMapper.convertEntityToDTO(school);
                		
                		if(schoolDTO.getCountryId()!=null) {
                			userDTO.setCountryId(schoolDTO.getCountryId());
                			userDTO.setCountryName(schoolDTO.getCountryName());
                			userDTO.setCountryCode(schoolDTO.getCountryCode());
                		}
                		
                		if(schoolDTO.getStateId()!=null) {
                			userDTO.setStateId(schoolDTO.getStateId());
                			userDTO.setStateName(schoolDTO.getStateName());
                			userDTO.setStateCode(schoolDTO.getStateCode());
                		}
                		
                		if(schoolDTO.getDistrictId()!=null) {
                			userDTO.setDistrictId(schoolDTO.getDistrictId());
                			userDTO.setDistrictName(schoolDTO.getDistrictName());
                			userDTO.setDistrictCode(schoolDTO.getDisctrictCode());
                		}
                		
                		if(schoolDTO.getCityId()!=null) {
                			userDTO.setCityId(schoolDTO.getCityId());
                			userDTO.setCityName(schoolDTO.getCityName());
                			userDTO.setCityCode(schoolDTO.getCityCode());
                		}
                	}
                }*/
                
                userDTO.setOrganizationId(SecurityUtil.getOrganizationId());
                userDTO.setOrganizationCode(SecurityUtil.getOrgCode());
                userDTO.setOrganizationName(SecurityUtil.getOrgName());
                userDTO.setRoleId(SecurityUtil.getRoleId());
                userDTO.setUserRole(SecurityUtil.getRoleName());
                userDTO.setAcademicYearId(SecurityUtil.getAcademicYearId());
                userDTO.setAcademicYear(SecurityUtil.getAcademicYear());
                List<UserRoleDTO> userRoleDto = new ArrayList<>();
                for (UserRole role : user.getUserRoles()) {
                    if (role.getIsActive()) {
                        userRoleDto.add(userRoleMapper.convertEntityToDTO(role));
                    }
                }
                userDTO.setUserRoles(userRoleDto);

                List<RolePrivilege> rolePrivileges = new ArrayList<>();
                List<UserRole> userRoles = user.getUserRoles();

                for (UserRole userRole : userRoles) {
                    if (userRole.getIsActive()) {
                        List<RolePrivilege> rolePrivilegesList = userRole.getRole().getRolePrivileges();
                        for (RolePrivilege rolePrivilege : rolePrivilegesList) {
                            if (rolePrivilege.getIsActive() && rolePrivilege.getRole().getMobile().equals(isMobile)) {
                                rolePrivileges.add(rolePrivilege);
                            }
                        }
                    }
                }

                Set<ModuleDTO> moduleDTOSet = getModules(rolePrivileges, isMobile);
                if (moduleDTOSet != null && !moduleDTOSet.isEmpty()) {
                    setModulePages(rolePrivileges, moduleDTOSet);

                    setSubModules(rolePrivileges, moduleDTOSet);

                    setSubModulePages(rolePrivileges, moduleDTOSet);

                    userDTO.setModules(moduleDTOSet);
                }

                setPages(userDTO, rolePrivileges);

                response = new ApiResponse<>(USER_DETAILS_RETERIVED_SUCCESS, userDTO, HttpStatus.OK.value());
            } else {
                response = new ApiResponse<>(Boolean.FALSE, USER_DETAILS_RETERIVED_FAILURE, HttpStatus.OK.value());

            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Exception Occured while getting user details : " + e);
            throw new ApiException("Unable to process your request at this time, please try again!" + e, e);
        }

        long time2 = System.currentTimeMillis();
        LOGGER.debug("getUserDetails() executed " + " Total Time Taken : " + (time2 - time1));
        return response;

    }

    @Transactional
    private void setPages(UserDTO userDTO, List<RolePrivilege> rolePrivileges) {
        Set<PageDTO> pageDTOSet = new TreeSet<>(new PageDTOComparator());
        if (rolePrivileges != null && rolePrivileges.size() > 0) {
            for (RolePrivilege rolePrivilege : rolePrivileges) {
                Page page = rolePrivilege.getPage();
                if (page != null && rolePrivilege.getModule() == null && rolePrivilege.getSubmodule() == null) {
                    PageDTO pageDTO = setPageDetails(rolePrivilege, page);
                    if (pageDTO != null) {
                        pageDTOSet.add(pageDTO);
                    }
                }
            }
        }
        userDTO.setPages(pageDTOSet);

    }

    /**
     * Sets the sub module pages.
     *
     * @param rolePrivileges the role privileges
     * @param moduleDTOSet   the module DTO set
     */

    private void setSubModulePages(List<RolePrivilege> rolePrivileges, Set<ModuleDTO> moduleDTOSet) {
        for (ModuleDTO moduleDTO : moduleDTOSet) {
            Set<SubmoduleDTO> subModulesDTOList = moduleDTO.getSubModules();
            if (!CollectionUtils.isEmpty(subModulesDTOList)) {
                for (SubmoduleDTO subModuleDTO : subModulesDTOList) {
                    Set<PageDTO> pageDTOSet = new TreeSet<>(new PageDTOComparator());
                    for (RolePrivilege rolePrivilege : rolePrivileges) {
                        Page page = rolePrivilege.getPage();

                        if (moduleDTO != null && page != null && page.getSubmodule() != null
                                && moduleDTO.getModuleId().equals(page.getModule().getModuleId())
                                && page.getSubmodule().getSubmoduleId().equals(subModuleDTO.getSubModuleId())) {

                            PageDTO pageDTO = setPageDetails(rolePrivilege, page);
                            if (pageDTO != null) {
                                pageDTOSet.add(pageDTO);
                            }

                        }
                    }
                    subModuleDTO.setPages(pageDTOSet);
                }
            }
        }
    }

    private PageDTO setPageDetails(RolePrivilege rolePrivilege, Page page) {
        PageDTO pageDTO = null;
        if (rolePrivilege != null && page != null && page.getIsActive() != null && page.getIsActive()) {
            pageDTO = new PageDTO();
            pageDTO.setPageId(page.getPageId());
            pageDTO.setPageName(page.getPageName());
            pageDTO.setDisplayName(page.getDisplayName());
            pageDTO.setPageNo(page.getPageNo());
            pageDTO.setIconName(page.getIconName());
            pageDTO.setUrl(page.getUrl());
            pageDTO.setCanAdd(rolePrivilege.getCanAdd());
            pageDTO.setCanDelete(rolePrivilege.getCanDelete());
            pageDTO.setCanEdit(rolePrivilege.getCanEdit());
            pageDTO.setCanView(rolePrivilege.getCanView());
            pageDTO.setSortOrder(page.getSortOrder());
            pageDTO.setIsActive(page.getIsActive());
            if (page.getModule() != null) {
                pageDTO.setModuleId(page.getModule().getModuleId());
                pageDTO.setModuleName(page.getModule().getDisplayName());
            }
            if (page.getSubmodule() != null) {
                pageDTO.setSubmoduleId(page.getSubmodule().getSubmoduleId());
                pageDTO.setSubmoduleName(page.getSubmodule().getDisplayName());
            }


        }
        return pageDTO;

    }

    /**
     * Sets the sub modules.
     *
     * @param rolePrivileges the role privileges
     * @param moduleDTOSet   the module DTO set
     */
    private void setSubModules(List<RolePrivilege> rolePrivileges, Set<ModuleDTO> moduleDTOSet) {
        if (!CollectionUtils.isEmpty(moduleDTOSet)) {
            for (ModuleDTO moduleDTO : moduleDTOSet) {
                Set<SubmoduleDTO> subModulesDTOList = new TreeSet<>(new SubModuleDTOComparator());
                for (RolePrivilege rolePrivilege : rolePrivileges) {
                    Submodule subModule = rolePrivilege.getSubmodule();
                    if (subModule != null && subModule.getIsActive() != null && subModule.getIsActive()
                            && subModule.getModule() != null && moduleDTO != null
                            && moduleDTO.getModuleId().equals(subModule.getModule().getModuleId())) {
                        SubmoduleDTO subModuleDTO = new SubmoduleDTO();
                        subModuleDTO.setSubModuleId(subModule.getSubmoduleId());
                        subModuleDTO.setSubModuleName(subModule.getSubmoduleName());
                        subModuleDTO.setDisplayName(subModule.getDisplayName());
                        subModuleDTO.setSortOrder(subModule.getSortOrder());
                        subModuleDTO.setIconName(subModule.getIconName());
                        subModulesDTOList.add(subModuleDTO);
                    }
                }
                moduleDTO.setSubModules(subModulesDTOList);
            }
        }
    }

    /**
     * Sets the module pages.
     *
     * @param rolePrivileges the role privileges
     * @param moduleDTOSet   the module DTO set
     */

    private void setModulePages(List<RolePrivilege> rolePrivileges, Set<ModuleDTO> moduleDTOSet) {
        for (ModuleDTO moduleDTO : moduleDTOSet) {
            Set<PageDTO> pageDTOList = new TreeSet<>(new PageDTOComparator());
            if (rolePrivileges != null && rolePrivileges.size() > 0) {
                for (RolePrivilege rolePrivilege : rolePrivileges) {
                    Page page = rolePrivilege.getPage();
                    if (page != null && moduleDTO != null && page.getModule() != null
                            && moduleDTO.getModuleId().equals(page.getModule().getModuleId())
                            && page.getSubmodule() == null) {

                        PageDTO pageDTO = setPageDetails(rolePrivilege, page);

                        if (pageDTO != null) {
                            pageDTOList.add(pageDTO);
                        }
                    }
                }
            }
            moduleDTO.setPages(pageDTOList);
        }
    }

    /**
     * Gets the modules.
     *
     * @param rolePrivileges the role privileges
     * @param isMobile
     * @return the modules
     */
    private Set<ModuleDTO> getModules(List<RolePrivilege> rolePrivileges, Boolean isMobile) {
        Set<ModuleDTO> moduleDTOSet = new TreeSet<>(new ModuleDTOComparator());
        for (RolePrivilege rolePrivilege : rolePrivileges) {
            if (rolePrivilege.getModule() != null && rolePrivilege.getModule().getIsActive() != null
                    && rolePrivilege.getModule().getIsActive()
                    && rolePrivilege.getRole().getMobile() == isMobile) {
                Module module = rolePrivilege.getModule();
                ModuleDTO moduleDTO = new ModuleDTO();
                moduleDTO.setModuleId(module.getModuleId());
                moduleDTO.setDisplayName(module.getDisplayName());
                moduleDTO.setModuleName(module.getModuleName());
                moduleDTO.setUrl(module.getUrl());
                moduleDTO.setSortOrder(module.getSortOrder());
                moduleDTO.setIconName(module.getIconName());
                moduleDTOSet.add(moduleDTO);
            }
        }
        return moduleDTOSet;
    }

    @Override
    public ApiResponse<List<UserListsDTO>> getUserDetailsbyType(Long userTypeId, String userTypeCode, Integer page, Integer size) {

        ApiResponse<List<UserListsDTO>> response = null;
        org.springframework.data.domain.Page<User> users = null;
        Long pageIndex = null, pageSize = null;
        Pageable pageable = null;

        try {
            if (page == null) {
                page = 0;
            }
            if (size == null) {
                size = 50;
            }
            pageIndex = page.longValue();
            pageSize = size.longValue();

            pageable = PageRequest.of(page, size);

            Usertype usertype = userTypeRepository.getUserTypeByCode(userTypeCode);

            if (usertype != null) {
                userTypeId = usertype.getUserTypeId();
            }

            users = userRepository.findByUserDetailsPagination(null, userTypeId, pageable);

            if (!CollectionUtils.isEmpty(users.getContent())) {
                List<UserListsDTO> userDTOs = userListMapper.convertEntityListToDTOList(users.getContent());
                response = new ApiResponse<>(Boolean.TRUE, Messages.RETRIEVED_SUCCESS.getValue(), userDTOs, Boolean.TRUE, pageIndex, pageSize, users.getTotalElements());
            } else {
                response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(), HttpStatus.OK.value());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;

    }

    @Override
    public ApiResponse<?> createUser(UserDTO userDto) {

        ApiResponse<?> response = null;

        if (userDto.getUserName() != null) {
            User userName = userRepository.findByUserName(userDto.getUserName());

            if (userName != null && userName.getUserName().equalsIgnoreCase(userDto.getUserName())) {
            	response = new ApiResponse<>(Boolean.FALSE, Messages.MESSAGE_FOR_USER.getValue(), HttpStatus.OK.value());
            } else {
                try {
                    User user = userMapper.convertDTOtoEntity(userDto, null);

                    user = userRepository.save(user);

                    if (user.getUserId() != null) {

                        Usertype userType = userTypeRepository.findById(user.getUserType().getUserTypeId()).get();
                        /*if (userType.getUserTypeCode().equals("STUDENT")) {

                            StudentDetail std = studentDetailRepository.findById(userDto.getStudentId()).get();
                            std.setUser(user);
                            studentDetailRepository.save(std);
                        }
                        if (userType.getUserTypeCode().equals("PARENT")) {
                            StudentDetail std = studentDetailRepository.findById(userDto.getStudentId()).get();
                            std.setParentUser(user);
                            studentDetailRepository.save(std);
                        }
                        if (userType.getUserTypeCode().equals("STAFF")) {
                            EmployeeDetail emp = employeeDetailRepository.findById(userDto.getEmployeeId()).get();
                            emp.setUser(user);
                            employeeDetailRepository.save(emp);
                        }*/
                        userDto = userMapper.convertEntityToDTO(user);
                        response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), userDto,
                                HttpStatus.OK.value());
                    } else {
                        response = new ApiResponse<>(Boolean.FALSE, Messages.UNABLE_ADD_USER.getValue(),
                                HttpStatus.OK.value());
                    }
                } catch (Exception e) {
                	LOGGER.error("UserServiceImpl.createUser()" +e);;
                    throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
                }

            }

        }
        return response;
    }

    @Override
    @Transactional
    public ApiResponse<Set<PageDTO>> getUserPageDetails(Long userId, Boolean status, Boolean isMobile) {
        long time1 = System.currentTimeMillis();
        LOGGER.debug("entered into getUserPageDetails() " + userId);
        ApiResponse<Set<PageDTO>> response = null;
        try {
            User user = userRepository.getUserDetails(userId, status);
            if (user != null && !CollectionUtils.isEmpty(user.getUserRoles())) {

                List<RolePrivilege> rolePrivileges = new ArrayList<>();
                List<UserRole> userRoles = user.getUserRoles();

                for (UserRole userRole : userRoles) {
                    List<RolePrivilege> rolePrivilegesList = userRole.getRole().getRolePrivileges();
                    for (RolePrivilege rolePrivilege : rolePrivilegesList) {
                        if (rolePrivilege.getIsActive() && rolePrivilege.getRole().getMobile().equals(isMobile)) {
                            rolePrivileges.add(rolePrivilege);
                        }
                    }
                }

                Set<PageDTO> pageDTOList = new TreeSet<>(new PageDTOComparator());

                Set<ModuleDTO> moduleDTOSet = getModules(rolePrivileges, isMobile);
                if (moduleDTOSet != null && !moduleDTOSet.isEmpty()) {

                    for (ModuleDTO moduleDTO : moduleDTOSet) {
                        // Set<PageDTO> pageDTOList = new TreeSet<>(new PageDTOComparator());
                        if (rolePrivileges != null && rolePrivileges.size() > 0) {
                            for (RolePrivilege rolePrivilege : rolePrivileges) {
                                Page page = rolePrivilege.getPage();
                                if (page != null && moduleDTO != null && page.getModule() != null
                                        && moduleDTO.getModuleId().equals(page.getModule().getModuleId())
                                        && page.getSubmodule() == null) {

                                    PageDTO pageDTO = setPageDetails(rolePrivilege, page);

                                    if (pageDTO != null) {
                                        pageDTOList.add(pageDTO);
                                    }
                                }
                            }
                        }
                        // moduleDTO.setPages(pageDTOList);
                    }

                    for (ModuleDTO moduleDTO : moduleDTOSet) {
                        Set<SubmoduleDTO> subModulesDTOList = moduleDTO.getSubModules();
                        if (!CollectionUtils.isEmpty(subModulesDTOList)) {
                            for (SubmoduleDTO subModuleDTO : subModulesDTOList) {
                                // pageDTOSet = new TreeSet<>(new PageDTOComparator());
                                for (RolePrivilege rolePrivilege : rolePrivileges) {
                                    Page page = rolePrivilege.getPage();

                                    if (moduleDTO != null && page != null && page.getSubmodule() != null
                                            && moduleDTO.getModuleId().equals(page.getModule().getModuleId())
                                            && page.getSubmodule().getSubmoduleId()
                                            .equals(subModuleDTO.getSubModuleId())) {

                                        PageDTO pageDTO = setPageDetails(rolePrivilege, page);
                                        if (pageDTO != null) {
                                            pageDTOList.add(pageDTO);
                                        }

                                    }
                                }
                                // subModuleDTO.setPages(pageDTOSet);
                            }
                        }
                    }

                    // userDTO.setModules(moduleDTOSet);
                }

                if (rolePrivileges != null && rolePrivileges.size() > 0) {
                    for (RolePrivilege rolePrivilege : rolePrivileges) {
                        Page page = rolePrivilege.getPage();
                        if (page != null && rolePrivilege.getModule() == null && rolePrivilege.getSubmodule() == null) {
                            PageDTO pageDTO = setPageDetails(rolePrivilege, page);
                            if (pageDTO != null) {
                                pageDTOList.add(pageDTO);
                            }
                        }
                    }
                }

                response = new ApiResponse<>(USER_DETAILS_RETERIVED_SUCCESS, pageDTOList, HttpStatus.OK.value());
            } else {
                response = new ApiResponse<>(Boolean.FALSE, USER_DETAILS_RETERIVED_FAILURE, HttpStatus.OK.value());

            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Exception Occured while getting user details : " + e);
            throw new ApiException("Unable to process your request at this time, please try again!" + e, e);
        }

        long time2 = System.currentTimeMillis();
        LOGGER.debug("getUserDetails() executed " + " Total Time Taken : " + (time2 - time1));
        return response;

    }

    @Override
    public Object getBuildInfo() {
        Map<String, String> mapInfo = new HashMap<String, String>();
        RuntimeMXBean rb = ManagementFactory.getRuntimeMXBean();
        Long uptime = rb.getUptime();
        URL url_name;
        try {
            url_name = new URL("http://bot.whatismyipaddress.com");
            BufferedReader sc = new BufferedReader(new InputStreamReader(url_name.openStream()));
            String systemipaddress = sc.readLine().trim();
            mapInfo.put("IP", systemipaddress);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mapInfo.put("DBHOST", dbHost);
        String time = String.format("%02d min,%02d sec",
                TimeUnit.MILLISECONDS.toMinutes(uptime),
                TimeUnit.MILLISECONDS.toSeconds(uptime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(uptime))
        );
        mapInfo.put("UP-TIME", time);
        return mapInfo;
    }

    @Override
    public ApiResponse<?> getcreatingUserForStudents() {
        ApiResponse<?> response = null;
        LOGGER.info("UserServiceImpl.getcreatingUserForStudents() --- execution started");
        List<StudentDetail> studentDetailObj1 = new ArrayList<StudentDetail>();
        List<StudentDetail> studentDetailObj2 = new ArrayList<StudentDetail>();

        try {
            int count = 0;
          /*  List<StudentDetail> studentDetailObj3 = studentDetailRepository.getFindByRollNo("INCOLLEGE");
            List<StudentDetail> studentDetailObj4 = studentDetailRepository.getRollNumDuplicates("INCOLLEGE");
            for (StudentDetail studentDetailObj : studentDetailObj4) {
                studentDetailObj3.add(studentDetailObj);
            }*/

            /*List<StudentDetail> studentDetail = studentDetailRepository.getFindByUserId("INCOLLEGE");
            for (StudentDetail studentDetailList : studentDetail) {
                User user = userRepository.findByRollNumber(studentDetailList.getRollNumber());
                if (user != null) {
                    studentDetailObj1.add(studentDetailList);
                } else {
                    studentDetailObj2.add(studentDetailList);
                }
            }*/

            List<User> userList = new ArrayList<User>();

            for (StudentDetail studentDetailObj : studentDetailObj2) {
                User user = new User();
                user.setIsActive(Status.ACTIVE.getId());
                user.setCreatedDt(new Date());
                user.setCreatedUser(SecurityUtil.getCurrentUser());
                user.setUpdatedDt(new Date());
                user.setUpdatedUser(SecurityUtil.getCurrentUser());
                if (studentDetailObj.getFirstName() != null) {
                    user.setFirstName(studentDetailObj.getFirstName());
                }
                //user.setPassword(MD5Hashing.md5EncyptionUsingHex(userDTO.getPassword()));
                user.setPassword(studentDetailObj.getRollNumber());
                user.setUserName(studentDetailObj.getRollNumber());
                user.setIsReset(true);
                if (studentDetailObj.getLastName() != null) {
                    user.setLastName(studentDetailObj.getLastName());
                }
			  
			/* user.setPasswordExpDate(userDTO.getPasswordExpDate());
			  user.setResetPasswordCode(userDTO.getResetPasswordCode());
			  user.setPasswordAttempts(userDTO.getPasswordAttempts());*/
                user.setIsEditable(true);
                Long userTypeId = userTypeRepository.findByUserName("STUDENT");

                Usertype usertype = new Usertype();
                usertype.setUserTypeId(userTypeId);
                user.setUserType(usertype);

                if (studentDetailObj.getSchool().getSchoolId() != null) {
                    School school = new School();
                    school.setSchoolId(studentDetailObj.getSchool().getSchoolId());
                    user.setSchool(school);
                }
                if (studentDetailObj.getOrganization().getOrganizationId() != null) {
                    Organization organization = new Organization();
                    organization.setOrganizationId(studentDetailObj.getOrganization().getOrganizationId());
                    user.setOrganization(organization);
                }
                if (studentDetailObj.getReason() != null) {
                    user.setReason(studentDetailObj.getReason());
                }
                if (studentDetailObj.getMobile() != null) {
                    user.setMobileNumber(studentDetailObj.getMobile());
                }
                if (studentDetailObj.getStdEmailId() != null) {
                    user.setEmail(studentDetailObj.getStdEmailId());
                } else {
                    user.setEmail("NULL");
                }


                userList.add(user);

                count = count + 1;
                //LOGGER.info(count + "--" + studentDetailObj.getStudentId());


            }

            List<User> usersList = userRepository.saveAll(userList);
            List<UserRole> userRoleList = new ArrayList<UserRole>();

            for (User userObj : usersList) {
                StudentDetail studentDetail1 =null;// studentDetailRepository.findByStudentRollNo(userObj.getUserName());
                User user1 = new User();
                user1.setUserId(userObj.getUserId());
                studentDetail1.setUser(user1);
                UserRole userRole = new UserRole();


                userRole.setIsActive(Status.ACTIVE.getId());

                userRole.setCreatedDt(new Date());
                userRole.setCreatedUser(SecurityUtil.getCurrentUser());

                userRole.setUpdatedDt(new Date());
                userRole.setUpdatedUser(SecurityUtil.getCurrentUser());

                if (userObj.getUserId() != null) {
                    User user = new User();
                    user.setUserId(userObj.getUserId());
                    userRole.setUser(user);
                }

                Long roleId = roleRepository.findByRollName("STUDENT");
                Role role = new Role();
                role.setRoleId(roleId);
                userRole.setRole(role);
                userRoleList.add(userRole);
            }
            userRoleRepository.saveAll(userRoleList);
            /*if (studentDetailObj3.size() > 0) {
                response = new ApiResponse<>(Messages.USERS.getValue(), studentDetailListMapper.convertEntityListToDTOList(studentDetailObj3), HttpStatus.OK.value());
            } else {
                response = new ApiResponse<>(Messages.USERS_EXISTS.getValue(), HttpStatus.OK.value());
            }*/

            LOGGER.info("UserServiceImpl.getcreatingUserForStudents() --- execution completed");
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage());
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }
        return response;
    }

    @Override
    public ApiResponse<List<SmsToUsersDTO>> getsmsToLimitUser(Long userTypeId) {
        LOGGER.info("UserServiceImpl.getsmsToLimitUser()");
        ApiResponse<List<SmsToUsersDTO>> response = null;
        List<SmsToUsersDTO> smsToUsersDTOs = null;
        if (null == userTypeId) {
            throw new ApiException(Messages.RECORD_ID_MUST_NOT_NULL.getValue());
        }
        try {

            List<Object[]> objects = userRepository.smsToLimitUser(userTypeId);

            if (!(CollectionUtils.isEmpty(objects))) {
                smsToUsersDTOs = new ArrayList<>();

                for (Object[] object : objects) {
                    SmsToUsersDTO smsToUsersDTO = new SmsToUsersDTO();
                    smsToUsersDTO.setUserId((Long) object[0]);
                    smsToUsersDTO.setFirstName((String) object[1]);
                    smsToUsersDTO.setEmail((String) object[2]);
                    smsToUsersDTO.setMobileNumber((String) object[3]);
                    smsToUsersDTOs.add(smsToUsersDTO);
                }
            }

            if (!(CollectionUtils.isEmpty(smsToUsersDTOs))) {

                response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), (smsToUsersDTOs),
                        HttpStatus.OK.value());
            } else {
                response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
                        HttpStatus.OK.value());
            }

        } catch (Exception e) {
            LOGGER.error("UserServiceImpl.getsmsToLimitUser" + e);
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }

    @Override
    public PasswordResetToken createPasswordResetTokenForUser(User user) {
        //TODO OTP create
        String token = String.valueOf(CommonUtils.generateOTP(6));
        LOGGER.info("Generated OTP :" + token);
        PasswordResetToken passwordResetToken = new PasswordResetToken(token, user);
        passwordResetToken.setActive(Boolean.TRUE);
        passwordResetToken.setCreatedDt(new Date());
        passwordResetToken = passwordTokenRepository.save(passwordResetToken);


        StringBuilder mailBody = new StringBuilder();
        mailBody.append(passwordResetToken.getToken());
        mailBody.append(" is OTP for reset password of the user ");
        mailBody.append(passwordResetToken.getUser().getUserName());
        List<String> emailIds = new ArrayList<>();
        emailIds.add(user.getEmail());
        //emailIds.add("chilupoorishyam@gmail.com");
        String fromUser = "dev@gentechsys.com";
        if (username != null) {
            fromUser = username;
        }
        /*SendEmailResult sendEmailResult = bulkMailService.sendEmail(fromUser, emailIds,
                "Reset Password", mailBody.toString(), mailBody.toString());*/

        return passwordResetToken;
    }

    @Override
    public User getUserByPasswordResetToken(PasswordDTO passwordDto) {
        User resetPassworUser = null;
        User user = userRepository.findByUserNameOrEmail(passwordDto.getUserName());
        List<PasswordResetToken> passwordResetTokens = passwordTokenRepository.findByUserAndToken(user.getUserId(), passwordDto.getToken());
        if (!CollectionUtils.isEmpty(passwordResetTokens)) {
            resetPassworUser = passwordResetTokens.get(0).getUser();
        }
        return resetPassworUser;
    }

    @Override
    public void changeUserPassword(User user, String newPassword) {
        user.setPassword(newPassword);
        user.setUpdatedDt(new Date());
        userRepository.save(user);

    }

	@Override
	public ApiResponse<?> searchUserDetails(Long schoolId, Long userTypeId, Boolean status, String query) {
		LOGGER.info("UserServiceImpl.searchUserDetails()");
		ApiResponse<?> response = null;
		List<User> users = null;
		List<UserDTO> userDTO = null;

		if ((query == null) || (query != null && query.length() < 5)) {
			throw new BadRequestSearchException("Query String length cannot be lessthan 4 characters.");
		}

		try {
			String firstName = query, lastName = query, userName=query;

			if (query.matches("[0-9]+")) {
				users = userRepository.searchUserDetails(schoolId, userTypeId, firstName, lastName,userName,status);
			} 
			else {
				users = userRepository.searchUserDetails(schoolId, userTypeId, firstName, lastName, userName,status);
			}

			if (!CollectionUtils.isEmpty(users)) {
				userDTO = userMapper.convertEntityListToDTOList(users);
				response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), userDTO);
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
						HttpStatus.OK.value());
			}

		} catch (Exception e) {
			LOGGER.info("UserServiceImpl.searchUserDetails()");
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue());
		}
		return response;
	}
}
	

	