package com.gts.cms.common.enums;

public enum WorkFlowStagesEnum {

	/*Draft,
	SubimmitedToBSH,
	ApprovedByBSH,*/
	
	STAGE1(1,"STAGE1"),
	STAGE2(2,"STAGE2"),
	STAGE3(3,"STAGE3"),
	STAGE4(4,"STAGE4"),
	STAGE5(5,"STAGE5"),
	STAGE6(6,"STAGE6"),
	STAGE7(7,"STAGE7"),
	STAGE8(8,"STAGE8"),
	STAGE9(9,"STAGE9"),
	STAGE10(10,"STAGE10"),
	OPEN_STAGE(11,"OPEN"),
		
	ASSIGNED_STAGE(12,"ASSIGNED"),
	ACCEPT_STAGE(13,"ACCEPT"),
	REJECT_STAGE(14,"REJECT"),
	RESOLVED_STAGE(15,"RESOLVED"),
	CLOSED_STAGE(16,"CLOSED"),
	ASSIGNMENT(17,"ASSIGNMENT"),
	WORK_FLOW_FOR_CODE(18,"COMPLAINT");
	private int id;
	private String name;
	private WorkFlowStagesEnum(int id,String name) {
		this.id=id;
		this.name=name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
