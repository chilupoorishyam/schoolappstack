package com.gts.cms.common.service.impl;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.AuthorizationDTO;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.mapper.AuthorizationMapper;
import com.gts.cms.common.repository.AuthorizationRepository;
import com.gts.cms.common.service.AuthorizationService;
import com.gts.cms.entity.Authorization;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

/**
 * The Class AuthorizationServiceImpl.
 */
@Service
public class AuthorizationServiceImpl implements AuthorizationService {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger(AuthorizationServiceImpl.class);

	/** The authorization repository. */
	@Autowired
    AuthorizationRepository authorizationRepository;

	@Autowired
    AuthorizationMapper authMapper;

	@Override
	public ApiResponse<?> getAuthDetails(Date loginTime, Date logoutTime, Long userId, Boolean isActive, Integer page, Integer size) {
		LOGGER.info("AuthorizationServiceImpl.getAuthDetails()");
		ApiResponse<?> response = null;
		List<AuthorizationDTO> authDTOs = null;
		Long	pageIndex =null,pageSize =null;
		Page<Authorization> auth=null;
		Pageable pageable=null;
		
		try {
			
			if (page == null) {
				page = 0;
			}
			if (size == null) {
				size = 50;
			}
			
			pageIndex = page.longValue();
			pageSize = size.longValue();
			pageable=PageRequest.of(page, size);
			
			auth=authorizationRepository.fetchAuthDetails(loginTime, logoutTime,userId, isActive,pageable);
			
			if (!(CollectionUtils.isEmpty(auth.getContent()))) {
				authDTOs = authMapper.convertEntityListToDTOList(auth.getContent());
				
				if (!(CollectionUtils.isEmpty(authDTOs))) {
					response = new ApiResponse<>(Boolean.TRUE, Messages.RETRIEVED_SUCCESS.getValue(), authDTOs,Boolean.TRUE, pageIndex, pageSize, auth.getTotalElements(),HttpStatus.OK.value());
				}
				
				else {
					response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),HttpStatus.OK.value());
				}
			}

			 else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
						HttpStatus.OK.value());
			}

		} catch (Exception e) {
		}

		return response;
	}

}
