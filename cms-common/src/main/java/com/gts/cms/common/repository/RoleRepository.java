package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.RoleRepositoryCustom;
import com.gts.cms.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * created by Naveen(Auto) on 02/09/2018
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long>, RoleRepositoryCustom {

    @Query("SELECT r.roleId FROM  Role r"
            + " WHERE r.roleName = :roleName"
    )
    Long findByRollName(@Param("roleName") String string);

    @Query("SELECT r.roleId FROM  Role r"
            + " WHERE r.roleName = :roleName"
            + " AND r.school.schoolId = :schoolId" +
            " AND r.isActive = true"
    )
    Long findByRollNameAndSchoolId(@Param("roleName") String string,
                                    @Param("schoolId") Long schoolId);


}
