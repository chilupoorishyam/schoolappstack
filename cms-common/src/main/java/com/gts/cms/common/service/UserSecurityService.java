package com.gts.cms.common.service;

import com.gts.cms.common.dto.PasswordDTO;

public interface UserSecurityService {
    String validatePasswordResetToken(PasswordDTO passwordDto);
}
