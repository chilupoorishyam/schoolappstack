package com.gts.cms.common.student.repository.custom.impl;

import com.gts.cms.common.enums.Status;
import com.gts.cms.common.student.repository.custom.StudentEnquiryCustomRepository;
import com.gts.cms.entity.StudentEnquiry;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.gts.cms.entity.QSchool.school;
import static com.gts.cms.entity.QStudentEnquiry.studentEnquiry;

@Repository
public class StudentEnquiryCustomRepositoryImpl extends QuerydslRepositorySupport
		implements StudentEnquiryCustomRepository {

	public StudentEnquiryCustomRepositoryImpl() {
		super(StudentEnquiryCustomRepositoryImpl.class);
	}

	@Override
	public List<StudentEnquiry> findByStudentData(String studentData) {
		JPQLQuery<StudentEnquiry> query = from(studentEnquiry).where(studentEnquiry.studentName.containsIgnoreCase(studentData)
				.or(studentEnquiry.mobileNumber.containsIgnoreCase(studentData)).or(studentEnquiry.isActive.eq(true)))
				.orderBy(studentEnquiry.createdDt.asc());
		return query.fetch();
	}


	@Override
	public List<StudentEnquiry> findAllEnquiryForms(Long schoolId) {
		JPQLQuery<StudentEnquiry> query = from(studentEnquiry).join(studentEnquiry.school, school);
		query.where(school.schoolId.eq(schoolId).and(studentEnquiry.isActive.eq(Status.ACTIVE.getId())))
		.orderBy(studentEnquiry.createdDt.asc());
		return query.fetch();
	}

	@Override
	public List<StudentEnquiry> findAllEnquiryForms(Long schoolId, String status) {
		JPQLQuery<StudentEnquiry> query = null;
		if (Status.ALL.getName().equalsIgnoreCase(status)) {
			query = from(studentEnquiry).join(studentEnquiry.school, school);
			query.where(school.schoolId.eq(schoolId)).orderBy(studentEnquiry.enquiryId.asc());
		} else if(Status.INACTIVE.getName().equalsIgnoreCase(status)) {
			query = from(studentEnquiry).join(studentEnquiry.school, school);
			query.where(school.schoolId.eq(schoolId).and
			(studentEnquiry.isActive.eq(Boolean.FALSE))).orderBy(studentEnquiry.enquiryId.asc());
		}else if(status==null || 
				Status.ACTIVE.getName().equalsIgnoreCase(status)){
			query = from(studentEnquiry).join(studentEnquiry.school, school);
			query.where(school.schoolId.eq(schoolId).and(studentEnquiry.isActive.eq(Boolean.TRUE)))
			.orderBy(studentEnquiry.enquiryId.asc());
		}
		/*if (status != null) {
			JPQLQuery<StudentEnquiry> query = from(studentEnquiry).join(studentEnquiry.school,school);
					
			query.where(school.schoolId.eq(schoolId).and(studentEnquiry.isActive.eq(status)))
					.orderBy(studentApplication.createdDt.asc());
			return query.fetch();
		} else {
			JPQLQuery<StudentEnquiry> query = from(studentEnquiry).join(studentEnquiry.school,school);
			query.where(school.schoolId.eq(schoolId)).orderBy(studentEnquiry.createdDt.asc());
			
		}*/
		
		return query.fetch();
	}
}
