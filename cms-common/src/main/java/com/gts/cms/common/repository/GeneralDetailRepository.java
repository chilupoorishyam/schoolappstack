package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.GeneralDetailRepositoryCustom;
import com.gts.cms.entity.GeneralDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GeneralDetailRepository extends JpaRepository<GeneralDetail, Long>, GeneralDetailRepositoryCustom {

	@Query("SELECT g.generalDetailId FROM  GeneralDetail g" 
			+ " WHERE g.generalDetailCode = :generalDetailCode"
			)
	Long findByGeneralDetailCode(@Param("generalDetailCode") String generalDetailCode );

	@Query("SELECT g.generalDetailCode FROM  GeneralDetail g "
			+ "WHERE g.generalDetailId = :payerTypeId")
	String findCode(@Param("payerTypeId") Long payerTypeId);
	
	
	/*SELECT gd.`pk_gd_id`, gm.`gm_code` FROM 
	`t_m_general_master` gm INNER JOIN `t_m_general_details`gd
	ON gm.`pk_gm_id` =  gd.`fk_gm_id`
	 WHERE 1=1
	 AND gd.`gd_code` = 'APPROVED'
	 AND gm.`gm_code` = 'CERTWFSTAGE'*/
	
	@Query("SELECT gd.generalDetailId FROM GeneralDetail gd "
			+ " INNER JOIN GeneralMaster gm "
			+ " ON gd.generalMaster.generalMasterId = gm.generalMasterId"
			+ " AND gd.isActive =true "
			+ " AND gm.isActive =true "
			+ " WHERE 1=1"
			+ " AND gd.generalDetailCode = :generalDetailCode"
			+ " AND gm.generalMasterCode = :generalMasterCode")
	Long fetchGeneralDetails(String generalDetailCode, String generalMasterCode);



	@Query("SELECT g FROM  GeneralDetail g"
			+ " WHERE g.generalDetailId = :generalDetailId"
	)
	GeneralDetail findByGeneralDetailId(@Param("generalDetailId") Long generalDetailId );
	 
}

