package com.gts.cms.common.enums;

public enum PaymentTypeEnum {
	CASH("CASH"), 
	SCHOLARSHIP("SCHOLARSHIP"),
	CARD("CARD"),
	CONVREMIT("CONVREMIT"),
	DD("DD");
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	private PaymentTypeEnum(String value) {
		this.value = value;
	}

}
