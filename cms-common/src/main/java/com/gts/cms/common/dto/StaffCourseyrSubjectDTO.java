package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

public class StaffCourseyrSubjectDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long staffCourseyrSubjectId;

	private Date createdDt;

	private Long createdUser;

	private Date fromDate;

	private Boolean isActive;

	private String reason;

	private Date toDate;

	private Long subjectCourseyearId;

	private Long employeeId;

	private Long academicYearId;
	private Long subjectRegulationId;
	private Long schoolId;

	private String schoolCode;
	private String schoolName;

	private String academicYear;

	private String firstName;
	private String empNumber;
	private String middleName;

	private String lastName;

	private String deptName;

	private String designationName;

	private Long subjectId;

	private String subjectCode;
	private String subjectName;

	private String subjectType;
	private Long subjectTypeId;
	private Integer subCredits;

	private Long groupSectionId;
	private String section;

	private Long courseYearId;
	private String courseYearName;

	private Long courseId;
	private String courseCode;
	private String courseName;
	
	private Long studentbatchId;
	private String batchName;

	public Long getStaffCourseyrSubjectId() {
		return staffCourseyrSubjectId;
	}

	public void setStaffCourseyrSubjectId(Long staffCourseyrSubjectId) {
		this.staffCourseyrSubjectId = staffCourseyrSubjectId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Long getSubjectCourseyearId() {
		return subjectCourseyearId;
	}

	public void setSubjectCourseyearId(Long subjectCourseyearId) {
		this.subjectCourseyearId = subjectCourseyearId;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Long getAcademicYearId() {
		return academicYearId;
	}

	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}

	public Long getSubjectRegulationId() {
		return subjectRegulationId;
	}

	public void setSubjectRegulationId(Long subjectRegulationId) {
		this.subjectRegulationId = subjectRegulationId;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDesignationName() {
		return designationName;
	}

	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}

	public Long getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubjectCode() {
		return subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getSubjectType() {
		return subjectType;
	}

	public void setSubjectType(String subjectType) {
		this.subjectType = subjectType;
	}

	public Integer getSubCredits() {
		return subCredits;
	}

	public void setSubCredits(Integer subCredits) {
		this.subCredits = subCredits;
	}

	public Long getGroupSectionId() {
		return groupSectionId;
	}

	public void setGroupSectionId(Long groupSectionId) {
		this.groupSectionId = groupSectionId;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public Long getCourseYearId() {
		return courseYearId;
	}

	public void setCourseYearId(Long courseYearId) {
		this.courseYearId = courseYearId;
	}

	public String getCourseYearName() {
		return courseYearName;
	}

	public void setCourseYearName(String courseYearName) {
		this.courseYearName = courseYearName;
	}

	public Long getSubjectTypeId() {
		return subjectTypeId;
	}

	public void setSubjectTypeId(Long subjectTypeId) {
		this.subjectTypeId = subjectTypeId;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public Long getStudentbatchId() {
		return studentbatchId;
	}

	public void setStudentbatchId(Long studentbatchId) {
		this.studentbatchId = studentbatchId;
	}

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public String getEmpNumber() {
		return empNumber;
	}

	public void setEmpNumber(String empNumber) {
		this.empNumber = empNumber;
	}
}
