package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

public class EmpAttendanceEmployeeDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long attendanceEmpsId;
	private String categoryName;
	private String companyName;
	private Date createdDt;
	private Long createdUser;
	private String employeeCode;
	private Long empId;
	private String userName;
	private String empName;
	private String empNumber;
	private Boolean isActive;
	private String numericCode;
	private String reason;
	private Boolean recordStatus;
	private String stringCode;
	private Date updatedDt;
	private Long updatedUser;
	private Long schoolId;
	private String schoolCode;
	private String schoolName;
	private Long employeeId;
	private String employeeName;
	private Date startAttendanceDate;
	private Date latestattendanceDate;

	public Long getAttendanceEmpsId() {
		return attendanceEmpsId;
	}

	public void setAttendanceEmpsId(Long attendanceEmpsId) {
		this.attendanceEmpsId = attendanceEmpsId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public Long getEmpId() {
		return empId;
	}

	public void setEmpId(Long empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpNumber() {
		return empNumber;
	}

	public void setEmpNumber(String empNumber) {
		this.empNumber = empNumber;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getNumericCode() {
		return numericCode;
	}

	public void setNumericCode(String numericCode) {
		this.numericCode = numericCode;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Boolean getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(Boolean recordStatus) {
		this.recordStatus = recordStatus;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getStringCode() {
		return stringCode;
	}

	public void setStringCode(String stringCode) {
		this.stringCode = stringCode;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Date getStartAttendanceDate() {
		return startAttendanceDate;
	}

	public void setStartAttendanceDate(Date startAttendanceDate) {
		this.startAttendanceDate = startAttendanceDate;
	}

	public Date getLatestattendanceDate() {
		return latestattendanceDate;
	}

	public void setLatestattendanceDate(Date latestattendanceDate) {
		this.latestattendanceDate = latestattendanceDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}