package com.gts.cms.common.student.mapper;

import com.gts.cms.common.dto.StudentAppActivityDTO;
import com.gts.cms.common.dto.StudentAppEducationDTO;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.student.dto.*;
import com.gts.cms.common.student.dto.*;
import com.gts.cms.common.util.FileUtil;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.*;
import com.gts.cms.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class StudentDetailMapper implements BaseMapper<StudentDetail, StudentDetailDTO> {
	@Value("${s3.url}")
	private String url;

	@Value("${s3.bucket-name}")
	private String bucketName;
	private static final String fileSeparator = FileUtil.getFileSeparator();

	@Autowired
	StudentDocumentCollectionMapper studentDocumentCollectionMapper;

	@Autowired
	StudentActivitiesDetailMapper studentActivitiesDetailMapper;

	@Autowired
	StudentEducationDetailMapper studentEducationDetailMapper;

	public StudentDetail convertStudentApplicationToStudentDetails(StudentApplication studentApplication,
																   boolean isCreate) {
		StudentDetail studentDetail = null;
		if (studentApplication != null) {
			studentDetail = new StudentDetail();
			studentDetail.setApplicationNo(studentApplication.getApplicationNo());
			studentDetail.setBiometricCode(studentApplication.getBiometricNo());
			studentDetail.setAadharCardNo(studentApplication.getAadharCardNo());
			studentDetail.setAadharFilePath(FileUtil.getRelativePath(studentApplication.getAadharFilePath()));

			studentDetail.setCreatedDt(studentApplication.getCreatedDt());
			studentDetail.setCreatedUser(studentApplication.getCreatedUser());
			studentDetail.setUpdatedDt(new Date());
			studentDetail.setUpdatedUser(SecurityUtil.getCurrentUser());
			studentDetail.setIsMinority(studentApplication.getIsMinority());
			if (studentApplication.getBatch() != null) {

				studentDetail.setBatch(studentApplication.getBatch());

			}
			if (studentApplication.getBloodGroup() != null) {
				GeneralDetail bDetail = new GeneralDetail();
				bDetail.setGeneralDetailId(studentApplication.getBloodGroup().getGeneralDetailId());
				studentDetail.setBloodgroup(bDetail);
			}
			if (studentApplication.getCourse() != null) {
				Course course = new Course();
				course.setCourseId(studentApplication.getCourse().getCourseId());
				studentDetail.setCourse(course);
			}
			if (studentApplication.getSchool() != null) {
				School school = new School();
				school.setSchoolId(studentApplication.getSchool().getSchoolId());
				studentDetail.setSchool(school);
			}

			if (studentApplication.getOrganization() != null) {
				Organization organization = new Organization();
				organization.setOrganizationId(studentApplication.getOrganization().getOrganizationId());
				studentDetail.setOrganization(organization);
			}

			if (studentApplication.getCourseYear() != null) {
				CourseYear courseYear = new CourseYear();
				courseYear.setCourseYearId(studentApplication.getCourseYear().getCourseYearId());
				studentDetail.setCourseYear(courseYear);
			}

			if (studentApplication.getBatch() != null) {
				Batch batch = new Batch();
				batch.setBatchId(studentApplication.getBatch().getBatchId());
				studentDetail.setBatch(batch);
			}

			if (studentApplication.getLanguage1() != null) {
				GeneralDetail lang1 = new GeneralDetail();
				lang1.setGeneralDetailId(studentApplication.getLanguage1().getGeneralDetailId());
				studentDetail.setLanguage1(lang1);
			}

			if (studentApplication.getLanguage2() != null) {
				GeneralDetail lang2 = new GeneralDetail();
				lang2.setGeneralDetailId(studentApplication.getLanguage2().getGeneralDetailId());
				studentDetail.setLanguage2(lang2);
			}

			if (studentApplication.getLanguage3() != null) {
				GeneralDetail lang3 = new GeneralDetail();
				lang3.setGeneralDetailId(studentApplication.getLanguage3().getGeneralDetailId());
				studentDetail.setLanguage3(lang3);
			}
			if (studentApplication.getLanguage4() != null) {
				GeneralDetail lang4 = new GeneralDetail();
				lang4.setGeneralDetailId(studentApplication.getLanguage4().getGeneralDetailId());
				studentDetail.setLanguage4(lang4);
			}

			if (studentApplication.getLanguage5() != null) {
				GeneralDetail lang5 = new GeneralDetail();
				lang5.setGeneralDetailId(studentApplication.getLanguage5().getGeneralDetailId());
				studentDetail.setLanguage5(lang5);
			}

			if (studentApplication.getAcademicYear() != null) {
				AcademicYear academicYear = new AcademicYear();
				academicYear.setAcademicYearId(studentApplication.getAcademicYear().getAcademicYearId());
				studentDetail.setAcademicYear(academicYear);
			}

			if (studentApplication.getCaste() != null) {

				studentDetail.setCaste(studentApplication.getCaste());
			}
			if (studentApplication.getSubCaste() != null) {

				studentDetail.setSubCaste(studentApplication.getSubCaste());
			}



			if (studentApplication.getDisability() != null) {
				studentDetail.setDisability(studentApplication.getDisability());
			}

			if (studentApplication.getGender() != null) {

				studentDetail.setGender(studentApplication.getGender());
			}

			if (studentApplication.getQuota() != null) {

				studentDetail.setQuota(studentApplication.getQuota());
			}
			if (studentApplication.getTitle() != null) {

				studentDetail.setTitle(studentApplication.getTitle());
			}

			if (studentApplication.getReligion() != null) {

				studentDetail.setReligion(studentApplication.getReligion());
			}

			if (studentApplication.getStudenttype() != null) {

				studentDetail.setStudentType(studentApplication.getStudenttype());
			}

			if (studentApplication.getBloodGroup() != null) {

				studentDetail.setBloodgroup(studentApplication.getBloodGroup());
			}

			if (studentApplication.getNationality() != null) {

				studentDetail.setNationality(studentApplication.getNationality());
			}

			/*if (studentApplication.getCourseGroup() != null) {

				studentDetail.setCourseGroup(studentApplication.getCourseGroup());
			}*/

			if (studentApplication.getQualifyingCat() != null) {

				studentDetail.setQualifying(studentApplication.getQualifyingCat());
			}

			studentDetail.setDateOfExpiry(studentApplication.getDateOfExpiry());
			studentDetail.setDateOfIssue(studentApplication.getDateOfIssue());
			studentDetail.setDateOfRegistration(studentApplication.getDateOfRegistration());

			studentDetail.setDateOfBirth(studentApplication.getDateOfBirth());
			studentDetail.setEamcetRank(studentApplication.getEamcetRank());
			studentDetail.setStdEmailId(studentApplication.getStudentEmailId());
			studentDetail.setEntranceHtNo(studentApplication.getEntranceHtNo());
			studentDetail.setFatherEmailId(studentApplication.getFatherEmailId());
			studentDetail.setFatherAddress(studentApplication.getFatherAddress());
			studentDetail.setFatherMobileNo(studentApplication.getFatherMobileNo());
			studentDetail.setFatherName(studentApplication.getFatherName());
			studentDetail.setFatherOccupation(studentApplication.getFatherOccupation());
			// need
			studentDetail.setFatherPhotoPath(FileUtil.getRelativePath(studentApplication.getFatherPhotoPath()));
			studentDetail.setFatherQualification(studentApplication.getFatherQualification());
			studentDetail.setFathersIncomePa(studentApplication.getFathersIncomePa());
			studentDetail.setFirstName(studentApplication.getFirstName());
			studentDetail.setFolderPath(studentApplication.getFolderPath());

			studentDetail.setGuardianAddress(studentApplication.getGuardianAddress());
			studentDetail.setGuardianEmailId(studentApplication.getGuardianEmailId());
			studentDetail.setGuardianIncomePa(studentApplication.getGuardianIncomePa());
			studentDetail.setGuardianName(studentApplication.getGuardianName());
			studentDetail.setHallticketNumber(studentApplication.getHallticketNumber());
			studentDetail.setHobbies(studentApplication.getHobbies());
			studentDetail.setIdentificationMarks(studentApplication.getIdentificationMarks());
			studentDetail.setInterests(studentApplication.getInterests());
			studentDetail.setIsActive(studentApplication.getIsActive());
			studentDetail.setIsgovtempFather(studentApplication.getIsgovtempFather());
			studentDetail.setIsgovtempMother(studentApplication.getIsgovtempMother());
			studentDetail.setIsLocal(studentApplication.getIsLocal());
			studentDetail.setLangStatus1(studentApplication.getLangStatus1());
			studentDetail.setLangStatus2(studentApplication.getLangStatus2());
			studentDetail.setLangStatus3(studentApplication.getLangStatus3());
			studentDetail.setLangStatus4(studentApplication.getLangStatus4());
			studentDetail.setLangStatus5(studentApplication.getLangStatus5());
			studentDetail.setLastName(studentApplication.getLastName());
			studentDetail.setMiddleName(studentApplication.getMiddleName());
			studentDetail.setMobile(studentApplication.getMobile());
			studentDetail.setMotherAddress(studentApplication.getMotherAddress());
			studentDetail.setMotherEmailId(studentApplication.getMotherEmailId());
			studentDetail.setMotherIncomePa(studentApplication.getMotherIncomePa());
			studentDetail.setMotherMobileNo(studentApplication.getMotherMobileNo());
			studentDetail.setMotherName(studentApplication.getMotherName());
			studentDetail.setMotherOccupation(studentApplication.getMotherOccupation());
			// need
			studentDetail.setMotherPhotoPath(FileUtil.getRelativePath(studentApplication.getMotherPhotoPath()));
			studentDetail.setMotherQualification(studentApplication.getMotherQualification());
			// need
			studentDetail.setPancardFilePath(FileUtil.getRelativePath(studentApplication.getPancardFilePath()));
			studentDetail.setPancardNo(studentApplication.getPancardNo());
			studentDetail.setPassportNo(studentApplication.getPassportNo());
			studentDetail.setPermanentAddress(studentApplication.getPermanentAddress());
			studentDetail.setPermanentPincode(studentApplication.getPermanentPincode());
			studentDetail.setPermanentStreet(studentApplication.getPermanentStreet());
			studentDetail.setPremanentMandal(studentApplication.getPermanentMandal());
			studentDetail.setPermanentDistrict(studentApplication.getPermanentDistrict());
			studentDetail.setPresentAddress(studentApplication.getPresentAddress());
			studentDetail.setPresentMandal(studentApplication.getPresentMandal());
			studentDetail.setPresentPincode(studentApplication.getPresentPincode());
			studentDetail.setPresentStreet(studentApplication.getPresentStreetName());
			studentDetail.setPresentDistrict(studentApplication.getPresentDistrict());
			studentDetail.setPrimaryContact(studentApplication.getPrimaryContact());
			studentDetail.setReason(studentApplication.getReason());
			studentDetail.setReceiptNo(studentApplication.getReceiptNo());
			studentDetail.setRefApplicationNo(studentApplication.getRefApplicationNo());

			studentDetail.setResidencePhone(studentApplication.getResidencePhone());
			studentDetail.setRollNumber(studentApplication.getRollNumber());
			studentDetail.setSscNo(studentApplication.getSscNo());
			// need
			studentDetail.setStudentPhotoPath(FileUtil.getRelativePath(studentApplication.getStudentPhotoPath()));
			studentDetail.setStudentApplication(studentApplication);

			studentDetail.setPremenentCity(studentApplication.getPermenentCity());
			studentDetail.setPresentCity(studentApplication.getPresentCity());

			studentDetail.setIsLateral(studentApplication.getIsLateral());

			setStudentDocumentCollection(studentDetail, studentApplication, isCreate);

			setStudentActivities(studentDetail, studentApplication, isCreate);
			setStudentEducations(studentDetail, studentApplication, isCreate);

			// add studentEducationDetails, appActitivites,appDocumentCollection,appWorkflow
			//
		}
		return studentDetail;
	}

	private void setStudentEducations(StudentDetail studentDetail, StudentApplication studentApplication,
									  boolean isCreate) {
		if (!CollectionUtils.isEmpty(studentApplication.getStdAppEducations())) {
			List<StudentEducationDetail> studentEducationDetails = new ArrayList<>();
			for (StudentAppEducation studentAppEducation : studentApplication.getStdAppEducations()) {

				StudentEducationDetail studentEducationDetail = new StudentEducationDetail();
				studentEducationDetail.setCreatedDt(studentAppEducation.getCreatedDt());
				studentEducationDetail.setCreatedUser(studentAppEducation.getCreatedUser());
				/*
				 * if(!isCreate) {
				 * studentEducationDetail.setStudentEducationId(studentAppEducation.
				 * getAppEducationId()); }
				 */
				if (isCreate) {
					studentEducationDetail.setCreatedDt(new Date());
					studentEducationDetail.setCreatedUser(SecurityUtil.getCurrentUser());
				}
				studentEducationDetail.setAddress(studentAppEducation.getAddress());
				studentEducationDetail.setBoard(studentAppEducation.getBoard());
				studentEducationDetail.setGradeClassSecured(studentAppEducation.getGradeClassSecured());
				studentEducationDetail.setUpdatedDt(new Date());
				studentEducationDetail.setUpdatedUser(SecurityUtil.getCurrentUser());
				studentEducationDetail.setIsActive(studentAppEducation.getIsActive());
				studentEducationDetail.setMajorSubjects(studentAppEducation.getMajorSubjects());
				studentEducationDetail.setMedium(studentAppEducation.getMedium());
				studentEducationDetail.setNameOfInstitution(studentAppEducation.getNameOfInstitution());
				studentEducationDetail.setPrecentage(studentAppEducation.getPrecentage());
				studentEducationDetail.setReason(studentAppEducation.getReason());
				studentEducationDetail.setYearOfCompletion(studentAppEducation.getYearOfCompletion());
				studentEducationDetail.setStudentAppEducation(studentAppEducation);
				// studentEducationDetail.setTMGeneralDetail(studentAppEducation.get);
				studentEducationDetail.setStudentDetail(studentDetail);
				studentEducationDetails.add(studentEducationDetail);
			}
			if (!CollectionUtils.isEmpty(studentEducationDetails)) {
				studentDetail.setStdEducationDetails(studentEducationDetails);
			}

		}

	}

	private void setStudentActivities(StudentDetail studentDetail, StudentApplication studentApplication,
									  boolean isCreate) {

		if (!CollectionUtils.isEmpty(studentApplication.getStdAppActivities())) {
			List<StudentActivitiesDetail> studentActivitiesDetails = new ArrayList<>();
			for (StudentAppActivity studentAppActivity : studentApplication.getStdAppActivities()) {

				StudentActivitiesDetail studentActivitiesDetail = new StudentActivitiesDetail();
				/*
				 * if(!isCreate) {
				 * studentActivitiesDetail.setStudentActivityId(studentAppActivity.
				 * getStudentAppActivityId()); }
				 */
				studentActivitiesDetail.setCreatedDt(studentAppActivity.getCreatedDt());
				studentActivitiesDetail.setCreatedUser(studentAppActivity.getCreatedUser());

				if (isCreate) {
					studentActivitiesDetail.setCreatedDt(new Date());
					studentActivitiesDetail.setCreatedUser(SecurityUtil.getCurrentUser());
				}
				studentActivitiesDetail.setLevel(studentAppActivity.getLevel());
				studentActivitiesDetail.setParticulars(studentAppActivity.getParticulars());
				studentActivitiesDetail.setIsActive(studentAppActivity.getIsActive());
				studentActivitiesDetail.setSponsoredBy(studentAppActivity.getSponsoredBy());
				studentActivitiesDetail.setUpdatedDt(new Date());
				studentActivitiesDetail.setUpdatedUser(SecurityUtil.getCurrentUser());
				studentActivitiesDetail.setAppActivity(studentAppActivity);
				studentActivitiesDetail.setStudentDetail(studentDetail);
				studentActivitiesDetails.add(studentActivitiesDetail);
			}
			if (!CollectionUtils.isEmpty(studentActivitiesDetails)) {
				studentDetail.setActivitiesDetails(studentActivitiesDetails);
			}

		}

	}

	private void setStudentDocumentCollection(StudentDetail studentDetail, StudentApplication studentApplication,
											  boolean isCreate) {
		if (!CollectionUtils.isEmpty(studentApplication.getStdAppDocCollections())) {
			List<StudentDocumentCollection> studentDocCollectionList = new ArrayList<>();
			for (StudentAppDocCollection studentAppDocumentCollection : studentApplication.getStdAppDocCollections()) {
				StudentDocumentCollection studentDocumentCollection = new StudentDocumentCollection();
				studentDocumentCollection.setCreatedDt(studentAppDocumentCollection.getCreatedDt());
				studentDocumentCollection.setCreatedUser(studentAppDocumentCollection.getCreatedUser());
				studentDocumentCollection.setUpdatedDt(new Date());
				studentDocumentCollection.setUpdatedUser(SecurityUtil.getCurrentUser());
				if (isCreate) {
					studentDocumentCollection.setCreatedDt(new Date());
					studentDocumentCollection.setCreatedUser(SecurityUtil.getCurrentUser());

				}
				/*
				 * if (!isCreate) {
				 * studentDocumentCollection.setStudentDocCollId(studentAppDocumentCollection.
				 * getAppDocCollId());
				 *
				 * }
				 */
				studentDocumentCollection.setIsActive(studentAppDocumentCollection.getIsActive());
				studentDocumentCollection.setFileName(studentAppDocumentCollection.getFileName());
				studentDocumentCollection
						.setFilePath(FileUtil.getRelativePath(studentAppDocumentCollection.getFilePath()));
				studentDocumentCollection.setIsHardCopy(studentAppDocumentCollection.getIsHardCopy());
				studentDocumentCollection.setIsSoftCopy(studentAppDocumentCollection.getIsSoftCopy());
				studentDocumentCollection.setIsOriginal(studentAppDocumentCollection.getIsOriginal());
				studentDocumentCollection.setIsVerified(studentAppDocumentCollection.getIsVerified());
				studentDocumentCollection.setRackNumber(studentAppDocumentCollection.getRackNumber());
				studentDocumentCollection.setReason(studentAppDocumentCollection.getReason());

				studentDocumentCollection.setDocumentRepository(studentAppDocumentCollection.getDocumentRepository());
				studentDocumentCollection.setStudentAppDocCollection(studentAppDocumentCollection);
				studentDocumentCollection.setStudentDetail(studentDetail);
				studentDocCollectionList.add(studentDocumentCollection);
			}
			if (!CollectionUtils.isEmpty(studentDocCollectionList)) {
				studentDetail.setStdDocumentCollections(studentDocCollectionList);
			}
		}
	}

	public StudentDetailDTO convertStudentDetailToStudentDetailDTO(StudentDetail studentDetail) {
		StudentDetailDTO studentDetailDTO = null;
		if (studentDetail != null) {
			studentDetailDTO = new StudentDetailDTO();
			studentDetailDTO.setStudentId(studentDetail.getStudentId());
			studentDetailDTO.setApplicationNo(studentDetail.getApplicationNo());
			studentDetailDTO.setBiometricCode(studentDetail.getBiometricCode());
			studentDetailDTO.setAadharCardNo(studentDetail.getAadharCardNo());
			// need abslou
			if (studentDetail.getAadharFilePath() != null) {
				/*
				 * StringBuilder aadharFilePath=new StringBuilder(); aadharFilePath =
				 * aadharFilePath.append(url).append(fileSeparator).append(bucketName).append(
				 * fileSeparator) .append(studentDetail.getAadharFilePath());
				 */
				studentDetailDTO.setAadharFilePath(FileUtil.getAbsolutePath(studentDetail.getAadharFilePath()));
			}

			studentDetailDTO.setAdminssionDate(studentDetail.getAdminssionDate());
			studentDetailDTO.setAdmissionNumber(studentDetail.getAdmissionNumber());
			studentDetailDTO.setCreatedDt(studentDetail.getCreatedDt());
			studentDetailDTO.setCreatedUser(studentDetail.getCreatedUser());
			studentDetailDTO.setIsMinority(studentDetail.getIsMinority());

			if (studentDetail.getQualifying() != null) {
				studentDetailDTO.setQualifyingId(studentDetail.getQualifying().getGeneralDetailId());
				studentDetailDTO.setQualifyingName(studentDetail.getQualifying().getGeneralDetailDisplayName());
				studentDetailDTO.setQualifyingCode(studentDetail.getQualifying().getGeneralDetailCode());
			}
			if (studentDetail.getGroupSection() != null) {
				studentDetailDTO.setGroupSectionId(studentDetail.getGroupSection().getGroupSectionId());
				studentDetailDTO.setSection(studentDetail.getGroupSection().getSection());
			}
			if (studentDetail.getAcademicYear() != null) {
				studentDetailDTO.setAcademicYearId(studentDetail.getAcademicYear().getAcademicYearId());
				studentDetailDTO.setAcademicYear(studentDetail.getAcademicYear().getAcademicYear());
			}
			if (studentDetail.getBatch() != null) {
				studentDetailDTO.setBatchId(studentDetail.getBatch().getBatchId());
				studentDetailDTO.setBatchName(studentDetail.getBatch().getBatchName());
			}
			if (studentDetail.getBloodgroup() != null) {
				studentDetailDTO.setBloodgroupId(studentDetail.getBloodgroup().getGeneralDetailId());
				studentDetailDTO.setBloodgroupDisplayName(studentDetail.getBloodgroup().getGeneralDetailDisplayName());
			}
			if (studentDetail.getCourse() != null) {
				studentDetailDTO.setCourseId(studentDetail.getCourse().getCourseId());
				studentDetailDTO.setCourseName(studentDetail.getCourse().getCourseName());
			}
			if (studentDetail.getCourseYear() != null) {
				studentDetailDTO.setCourseYearId(studentDetail.getCourseYear().getCourseYearId());
				studentDetailDTO.setCourseYearName(studentDetail.getCourseYear().getCourseYearName());
			}

			if (studentDetail.getUser() != null) {
				studentDetailDTO.setUserId(studentDetail.getUser().getUserId());
				studentDetailDTO.setUserName(studentDetail.getUser().getUserName());
			}

			if (studentDetail.getLanguage1() != null) {
				studentDetailDTO.setLanguage1Id(studentDetail.getLanguage1().getGeneralDetailId());
			}

			if (studentDetail.getLanguage2() != null) {
				studentDetailDTO.setLanguage2Id(studentDetail.getLanguage2().getGeneralDetailId());
			}

			if (studentDetail.getLanguage3() != null) {
				studentDetailDTO.setLanguage3Id(studentDetail.getLanguage3().getGeneralDetailId());
			}

			if (studentDetail.getLanguage4() != null) {
				studentDetailDTO.setLanguage4Id(studentDetail.getLanguage4().getGeneralDetailId());
			}

			if (studentDetail.getLanguage5() != null) {
				studentDetailDTO.setLanguage5Id(studentDetail.getLanguage5().getGeneralDetailId());
			}

			studentDetailDTO.setDateOfExpiry(studentDetail.getDateOfExpiry());
			studentDetailDTO.setDateOfIssue(studentDetail.getDateOfIssue());
			studentDetailDTO.setDateOfRegistration(studentDetail.getDateOfRegistration());
			if (studentDetail.getDisability() != null) {
				studentDetailDTO.setDisabilityId(studentDetail.getDisability().getGeneralDetailId());
				studentDetailDTO.setDisabilityDisplayName(studentDetail.getDisability().getGeneralDetailDisplayName());
			}
			studentDetailDTO.setDateOfBirth(studentDetail.getDateOfBirth());
			studentDetailDTO.setEamcetRank(studentDetail.getEamcetRank());
			studentDetailDTO.setStdEmailId(studentDetail.getStdEmailId());
			studentDetailDTO.setEntranceHtNo(studentDetail.getEntranceHtNo());
			studentDetailDTO.setFatherEmailId(studentDetail.getFatherEmailId());
			studentDetailDTO.setFatherAddress(studentDetail.getFatherAddress());
			studentDetailDTO.setFatherMobileNo(studentDetail.getFatherMobileNo());
			studentDetailDTO.setFatherName(studentDetail.getFatherName());
			studentDetailDTO.setFatherOccupation(studentDetail.getFatherOccupation());
			if (studentDetail.getFatherPhotoPath() != null) {
				/*
				 * StringBuilder fatherPhotoPath=new StringBuilder(); fatherPhotoPath =
				 * fatherPhotoPath.append(url).append(fileSeparator).append(bucketName).append(
				 * fileSeparator) .append(studentDetail.getFatherPhotoPath());
				 */

				studentDetailDTO.setFatherPhotoPath(FileUtil.getAbsolutePath(studentDetail.getFatherPhotoPath()));
			}

			studentDetailDTO.setFatherQualification(studentDetail.getFatherQualification());
			studentDetailDTO.setFirstName(studentDetail.getFirstName());
			studentDetailDTO.setFolderPath(studentDetail.getFolderPath());
			if (studentDetail.getGender() != null) {
				studentDetailDTO.setGenderId(studentDetail.getGender().getGeneralDetailId());
				studentDetailDTO.setGender(studentDetail.getGender().getGeneralDetailDisplayName());
			}
			studentDetailDTO.setGuardianAddress(studentDetail.getGuardianAddress());
			studentDetailDTO.setGuardianEmailId(studentDetail.getGuardianEmailId());
			studentDetailDTO.setGuardianIncomePa(studentDetail.getGuardianIncomePa());
			studentDetailDTO.setGuardianName(studentDetail.getGuardianName());
			studentDetailDTO.setHallticketNumber(studentDetail.getHallticketNumber());
			studentDetailDTO.setHobbies(studentDetail.getHobbies());
			studentDetailDTO.setIdentificationMarks(studentDetail.getIdentificationMarks());
			studentDetailDTO.setInterests(studentDetail.getInterests());
			studentDetailDTO.setIsActive(studentDetail.getIsActive());
			studentDetailDTO.setIsgovtempFather(studentDetail.getIsgovtempFather());
			studentDetailDTO.setIsgovtempMother(studentDetail.getIsgovtempMother());
			studentDetailDTO.setIsLocal(studentDetail.getIsLocal());
			studentDetailDTO.setLangStatus1(studentDetail.getLangStatus1());
			studentDetailDTO.setLangStatus2(studentDetail.getLangStatus2());
			studentDetailDTO.setLangStatus3(studentDetail.getLangStatus3());
			studentDetailDTO.setLangStatus4(studentDetail.getLangStatus4());
			studentDetailDTO.setLangStatus5(studentDetail.getLangStatus5());
			studentDetailDTO.setLastName(studentDetail.getLastName());
			studentDetailDTO.setMiddleName(studentDetail.getMiddleName());
			studentDetailDTO.setMobile(studentDetail.getMobile());
			studentDetailDTO.setMotherAddress(studentDetail.getMotherAddress());
			studentDetailDTO.setMotherEmailId(studentDetail.getMotherEmailId());
			studentDetailDTO.setMotherIncomePa(studentDetail.getMotherIncomePa());
			studentDetailDTO.setMotherMobileNo(studentDetail.getMotherMobileNo());
			studentDetailDTO.setMotherName(studentDetail.getMotherName());
			studentDetailDTO.setMotherOccupation(studentDetail.getMotherOccupation());

			if (studentDetail.getMotherPhotoPath() != null) {
				/*
				 * StringBuilder motherPhotoPath=new StringBuilder(); motherPhotoPath =
				 * motherPhotoPath.append(url).append(fileSeparator).append(bucketName).append(
				 * fileSeparator) .append(studentDetail.getMotherPhotoPath());
				 */
				studentDetailDTO.setMotherPhotoPath(FileUtil.getAbsolutePath(studentDetail.getMotherPhotoPath()));

			}

			studentDetailDTO.setMotherQualification(studentDetail.getMotherQualification());

			if (studentDetail.getNationality() != null) {
				studentDetailDTO.setNationalityId(studentDetail.getNationality().getGeneralDetailId());
				studentDetailDTO
						.setNationalityDisplayName(studentDetail.getNationality().getGeneralDetailDisplayName());
			}

			if (studentDetail.getPancardFilePath() != null) {
//			StringBuilder pancardPhotoPath=new StringBuilder();
//			pancardPhotoPath = pancardPhotoPath.append(url).append(fileSeparator).append(bucketName).append(fileSeparator)
//					.append(studentDetail.getPancardFilePath());

				studentDetailDTO.setPancardFilePath(FileUtil.getAbsolutePath(studentDetail.getPancardFilePath()));
			}

			studentDetailDTO.setPancardNo(studentDetail.getPancardNo());
			studentDetailDTO.setPassportNo(studentDetail.getPassportNo());
			studentDetailDTO.setPermanentAddress(studentDetail.getPermanentAddress());
			studentDetailDTO.setPermanentPincode(studentDetail.getPermanentPincode());
			studentDetailDTO.setPermanentStreet(studentDetail.getPermanentStreet());
			studentDetailDTO.setPresentAddress(studentDetail.getPresentAddress());
			studentDetailDTO.setPresentMandal(studentDetail.getPresentMandal());
			studentDetailDTO.setPresentPincode(studentDetail.getPresentPincode());
			studentDetailDTO.setPresentStreetName(studentDetail.getPresentStreet());

			if (studentDetail.getPresentCity() != null) {
				studentDetailDTO.setPresentCityId(studentDetail.getPresentCity().getCityId());
				studentDetailDTO.setPresentCityName(studentDetail.getPresentCity().getCityName());
				if (studentDetail.getPresentDistrict() != null) {
					studentDetailDTO.setPresentDistrictId(studentDetail.getPresentDistrict().getDistrictId());
					studentDetailDTO.setPresentDistrictName(studentDetail.getPresentDistrict().getDistrictName());
					if (studentDetail.getPresentDistrict().getState() != null) {
						studentDetailDTO.setPresentStateId(studentDetail.getPresentDistrict().getState().getStateId());
						studentDetailDTO
								.setPresentStateName(studentDetail.getPresentDistrict().getState().getStateName());
						if (studentDetail.getPresentDistrict().getState().getCountry() != null) {
							studentDetailDTO.setPresentCountryId(
									studentDetail.getPresentDistrict().getState().getCountry().getCountryId());
							studentDetailDTO.setPresentCountryName(
									studentDetail.getPresentDistrict().getState().getCountry().getCountryName());

						}
					}

				}

			}

			if (studentDetail.getPremenentCity() != null) {
				studentDetailDTO.setPermanentCityId(studentDetail.getPremenentCity().getCityId());
				studentDetailDTO.setPermanentCityName(studentDetail.getPremenentCity().getCityName());
				if (studentDetail.getPermanentDistrict() != null) {
					studentDetailDTO.setPermanentDistrictId(studentDetail.getPermanentDistrict().getDistrictId());
					studentDetailDTO.setPermanentDistrictName(studentDetail.getPermanentDistrict().getDistrictName());
					if (studentDetail.getPermanentDistrict().getState() != null) {
						studentDetailDTO
								.setPermanentStateId(studentDetail.getPermanentDistrict().getState().getStateId());
						studentDetailDTO
								.setPermanentStateName(studentDetail.getPermanentDistrict().getState().getStateName());
						if (studentDetail.getPermanentDistrict().getState().getCountry() != null) {
							studentDetailDTO.setPermanentCountryId(
									studentDetail.getPermanentDistrict().getState().getCountry().getCountryId());
							studentDetailDTO.setPermanentCountryName(
									studentDetail.getPermanentDistrict().getState().getCountry().getCountryName());

						}
					}

				}
			}

			studentDetailDTO.setPrimaryContact(studentDetail.getPrimaryContact());
			if (studentDetail.getQuota() != null) {
				studentDetailDTO.setQuotaId(studentDetail.getQuota().getGeneralDetailId());
				studentDetailDTO.setQuotaDisplayName(studentDetail.getQuota().getGeneralDetailDisplayName());
				studentDetailDTO.setQuotaCode(studentDetail.getQuota().getGeneralDetailCode());
			}
			if (studentDetail.getParentUser() != null) {
				studentDetailDTO.setParentUserId(studentDetail.getParentUser().getUserId());
				studentDetailDTO.setParentFirstName(studentDetail.getParentUser().getFirstName());
			}
			studentDetailDTO.setReason(studentDetail.getReason());
			studentDetailDTO.setReceiptNo(studentDetail.getReceiptNo());
			studentDetailDTO.setRefApplicationNo(studentDetail.getRefApplicationNo());

			if (studentDetail.getReligion() != null) {
				studentDetailDTO.setReligionId(studentDetail.getReligion().getGeneralDetailId());
				studentDetailDTO.setReligionDisplayName(studentDetail.getReligion().getGeneralDetailDisplayName());
			}
			studentDetailDTO.setResidencePhone(studentDetail.getResidencePhone());
			studentDetailDTO.setRollNumber(studentDetail.getRollNumber());
			studentDetailDTO.setSscNo(studentDetail.getSscNo());
			studentDetailDTO.setStudentPhotoPath(FileUtil.getAbsolutePath(studentDetail.getStudentPhotoPath()));
			if (studentDetail.getStudentType() != null) {
				studentDetailDTO.setStudentTypeId(studentDetail.getStudentType().getGeneralDetailId());
				studentDetailDTO
						.setStudentTypeDisplayName(studentDetail.getStudentType().getGeneralDetailDisplayName());
			}
			if (studentDetail.getSubCaste() != null) {
				studentDetailDTO.setSubCasteId(studentDetail.getSubCaste().getSubCasteId());
				studentDetailDTO.setSubCasteName(studentDetail.getSubCaste().getSubCaste());
			}
			if (studentDetail.getCaste() != null) {
				studentDetailDTO.setCasteId(studentDetail.getCaste().getCasteId());
				studentDetailDTO.setCasteName(studentDetail.getCaste().getCaste());
			}
			/*if (studentDetail.getCourseGroup() != null) {
				studentDetailDTO.setCourseGroupId(studentDetail.getCourseGroup().getCourseGroupId());
				studentDetailDTO.setCourseGroupName(studentDetail.getCourseGroup().getGroupName());
				studentDetailDTO.setGroupCode(studentDetail.getCourseGroup().getGroupCode());
			}*/

			if (studentDetail.getSchool() != null) {
				studentDetailDTO.setSchoolId(studentDetail.getSchool().getSchoolId());
				studentDetailDTO.setSchoolCode(studentDetail.getSchool().getSchoolCode());
				studentDetailDTO.setSchoolName(studentDetail.getSchool().getSchoolName());

			}
			if (studentDetail.getOrganization() != null) {
				studentDetailDTO.setOrganizationId(studentDetail.getOrganization().getOrganizationId());
				studentDetailDTO.setOrgCode(studentDetail.getOrganization().getOrgCode());
				studentDetailDTO.setOrgName(studentDetail.getOrganization().getOrgName());

			}

			if (studentDetail.getIsCurrentYear() != null) {
				studentDetailDTO.setIsCurrentYear(studentDetail.getIsCurrentYear());
			}
			if (studentDetail.getIsLateral() != null) {
				studentDetailDTO.setIsLateral(studentDetail.getIsLateral());
			}
			if (studentDetail.getIsLateral() != null) {
				studentDetailDTO.setIsScholorship(studentDetail.getIsScholorship());
			}

			// setStudentAppDocumentCollection(studentDetailDTO, studentDetail);
			// setStudentAppActivities(studentDetailDTO, studentDetail);
			// setStudentAppEducations(studentDetailDTO, studentDetail);

			if (studentDetail.getStdDocumentCollections() != null) {
				studentDetailDTO.setStudentDocumentCollections(studentDocumentCollectionMapper
						.convertStudentDocumentCollectionListToStudentDocumentCollectionDTOList(
								studentDetail.getStdDocumentCollections()));
			}
			if (studentDetail.getActivitiesDetails() != null) {
				studentDetailDTO.setStudentActivitiesDetails(
						studentActivitiesDetailMapper.convertEntityListToDTOList(studentDetail.getActivitiesDetails()));
			}
			if (studentDetail.getStdEducationDetails() != null) {
				studentDetailDTO.setStudentEducationDetails(
						studentEducationDetailMapper.convertEntityListToDTOList(studentDetail.getStdEducationDetails()));
			}

			if (studentDetail.getTitle() != null) {
				studentDetailDTO.setTitleId(studentDetail.getTitle().getGeneralDetailId());
				studentDetailDTO.setTitle(studentDetail.getTitle().getGeneralDetailDisplayName());
			}
			studentDetailDTO.setEntranceHTNumber(studentDetail.getEntranceHtNo());
			studentDetailDTO.setFathersIncomePa(studentDetail.getFathersIncomePa());
			studentDetailDTO.setLangStatus1(studentDetail.getLangStatus1());
			studentDetailDTO.setLangStatus2(studentDetail.getLangStatus2());
			studentDetailDTO.setLangStatus3(studentDetail.getLangStatus3());
			studentDetailDTO.setLangStatus4(studentDetail.getLangStatus4());
			studentDetailDTO.setLangStatus5(studentDetail.getLangStatus5());
			studentDetailDTO.setPermanentMandal(studentDetail.getPremanentMandal());


			if (studentDetail.getStudentStatus() != null) {
				studentDetailDTO.setStudentStatusId(studentDetail.getStudentStatus().getGeneralDetailId());
				studentDetailDTO.setStudentStatusDisplayName(studentDetail.getStudentStatus().getGeneralDetailDisplayName());
				studentDetailDTO.setStudentStatusCode(studentDetail.getStudentStatus().getGeneralDetailCode());
			}
		}
		return studentDetailDTO;
	}


	private void setStudentAppDocumentCollection(StudentDetailDTO studentApplicationDTO,
												 StudentDetail studentApplication) {
		if (!CollectionUtils.isEmpty(studentApplication.getStdDocumentCollections())) {
			List<StudentAppDocumentCollectionDTO> studentAppDocCollections = new ArrayList<>();
			for (StudentDocumentCollection studentAppDocCollection : studentApplication.getStdDocumentCollections()) {
				StudentAppDocumentCollectionDTO studentAppDocCollectionDTO = new StudentAppDocumentCollectionDTO();
				studentAppDocCollectionDTO.setAppDocCollId(studentAppDocCollection.getStudentDocCollId());
				// studentAppDocCollectionDTO.setCreatedDt(studentAppDocCollection.getCreatedDt());
				studentAppDocCollectionDTO.setCreatedUser(studentAppDocCollection.getCreatedUser());
				studentAppDocCollectionDTO.setIsActive(studentAppDocCollection.getIsActive());
				studentAppDocCollectionDTO.setFileName(studentAppDocCollection.getFileName());
				if (studentAppDocCollection.getFilePath() != null) {
					// StringBuilder filePath = new StringBuilder();

					// need modifications -abs path
					/*
					 * filePath =
					 * filePath.append(url).append(fileSeparator).append(bucketName).append(
					 * fileSeparator) .append(studentAppDocCollection.getFilePath());
					 */
					studentAppDocCollectionDTO
							.setFilePath(FileUtil.getAbsolutePath(studentAppDocCollection.getFilePath()));
				}

				studentAppDocCollectionDTO.setIsHardCopy(studentAppDocCollection.getIsHardCopy());
				studentAppDocCollectionDTO.setIsSoftCopy(studentAppDocCollection.getIsSoftCopy());
				studentAppDocCollectionDTO.setIsOriginal(studentAppDocCollection.getIsOriginal());
				studentAppDocCollectionDTO.setIsVerified(studentAppDocCollection.getIsVerified());
				studentAppDocCollectionDTO.setRackNumber(studentAppDocCollection.getRackNumber());
				studentAppDocCollectionDTO.setReason(studentAppDocCollection.getReason());
				if (studentAppDocCollection.getDocumentRepository() != null) {
					studentAppDocCollectionDTO.setDocumentRepositoryId(
							studentAppDocCollection.getDocumentRepository().getDocumentRepositoryId());
				}
				studentAppDocCollections.add(studentAppDocCollectionDTO);
			}
			if (!CollectionUtils.isEmpty(studentAppDocCollections)) {
				studentApplicationDTO.setStdDocCollections(studentAppDocCollections);
			}
		}
	}

	private void setStudentAppActivities(StudentDetailDTO studentApplicationDTO, StudentDetail studentApplication) {

		if (!CollectionUtils.isEmpty(studentApplication.getActivitiesDetails())) {
			List<StudentAppActivityDTO> studentAppActivities = new ArrayList<>();
			for (StudentActivitiesDetail studentAppActivity : studentApplication.getActivitiesDetails()) {

				StudentAppActivityDTO studentAppActivityDTO = new StudentAppActivityDTO();
				studentAppActivityDTO.setStudentAppActivityId(studentAppActivity.getStudentActivityId());
				studentAppActivityDTO.setLevel(studentAppActivity.getLevel());
				studentAppActivityDTO.setParticulars(studentAppActivity.getParticulars());
				studentAppActivityDTO.setSponsoredBy(studentAppActivity.getSponsoredBy());
				studentAppActivityDTO.setUpdatedDt(studentAppActivity.getUpdatedDt());
				studentAppActivityDTO.setUpdatedUser(studentAppActivity.getUpdatedUser());
				studentAppActivityDTO.setCreatedUser(SecurityUtil.getCurrentUser());
				studentAppActivityDTO.setIsActive(studentAppActivity.getIsActive());
				studentAppActivities.add(studentAppActivityDTO);
			}
			if (!CollectionUtils.isEmpty(studentAppActivities)) {
				studentApplicationDTO.setStdActivities(studentAppActivities);
			}
		}
	}

	private void setStudentAppEducations(StudentDetailDTO studentApplicationDTO, StudentDetail studentApplication) {

		if (!CollectionUtils.isEmpty(studentApplication.getStdEducationDetails())) {
			List<StudentAppEducationDTO> studentAppEducations = new ArrayList<>();
			for (StudentEducationDetail studentAppEducation : studentApplication.getStdEducationDetails()) {
				StudentAppEducationDTO studentAppEducationDTO = new StudentAppEducationDTO();
				studentAppEducationDTO.setAppEducationId(studentAppEducation.getStudentEducationId());
				studentAppEducationDTO.setAddress(studentAppEducation.getAddress());
				studentAppEducationDTO.setBoard(studentAppEducation.getBoard());
				studentAppEducationDTO.setCreatedUser(studentAppEducation.getCreatedUser());
				studentAppEducationDTO.setGradeClassSecured(studentAppEducation.getGradeClassSecured());
				studentAppEducationDTO.setIsActive(studentAppEducation.getIsActive());
				studentAppEducationDTO.setMajorSubjects(studentAppEducation.getMajorSubjects());
				studentAppEducationDTO.setMedium(studentAppEducation.getMedium());
				studentAppEducationDTO.setNameOfInstitution(studentAppEducation.getNameOfInstitution());
				studentAppEducationDTO.setYearOfCompletion(studentAppEducation.getYearOfCompletion());
				if (studentAppEducation.getPrecentage() != null) {
					studentAppEducationDTO.setPrecentage(studentAppEducation.getPrecentage().doubleValue());
				}
				studentAppEducationDTO.setReason(studentAppEducation.getReason());
				studentAppEducationDTO.setUpdatedDt(new Date());
				studentAppEducationDTO.setUpdatedUser(studentAppEducation.getUpdatedUser());
				studentAppEducations.add(studentAppEducationDTO);

			}
			if (!CollectionUtils.isEmpty(studentAppEducations)) {
				studentApplicationDTO.setStdEducations(studentAppEducations);
			}

		}
	}

	public StudentDetail convertStudentDetailDTOToStudentDetail(StudentDetailDTO studentDetailDTO,
																StudentDetail studentDetail) {
		if (null == studentDetail) {
			studentDetail = new StudentDetail();
		}
		if (studentDetailDTO.getStudentId() == null) {
			studentDetail.setCreatedDt(new Date());
			studentDetail.setCreatedUser(SecurityUtil.getCurrentUser());
			studentDetail.setIsActive(Boolean.TRUE);
			studentDetail.setAdminssionDate(new Date());
		} else {
			studentDetail.setCreatedDt(studentDetailDTO.getCreatedDt());
			studentDetail.setCreatedUser(studentDetailDTO.getCreatedUser());
			studentDetail.setAdminssionDate(studentDetailDTO.getAdminssionDate());
			studentDetail.setIsActive(studentDetailDTO.getIsActive());
		}
		studentDetail.setUpdatedDt(new Date());
		studentDetail.setUpdatedUser(SecurityUtil.getCurrentUser());
		if (studentDetailDTO.getStudentAppId() != null) {
			StudentApplication stdApp = new StudentApplication();
			stdApp.setStudentAppId(studentDetailDTO.getStudentAppId());
			studentDetail.setStudentApplication(stdApp);
		}
		if (studentDetailDTO.getQualifyingId() != null) {
			GeneralDetail generalDetail = new GeneralDetail();
			generalDetail.setGeneralDetailId(studentDetailDTO.getQualifyingId());
			studentDetail.setQualifying(generalDetail);
		}
		studentDetail.setIsMinority(studentDetailDTO.getIsMinority());

		studentDetail.setStudentId(studentDetailDTO.getStudentId());
		studentDetail.setIsLateral(studentDetailDTO.getIsLateral());
		studentDetail.setIsScholorship(studentDetailDTO.getIsScholorship());
		studentDetail.setApplicationNo(studentDetailDTO.getApplicationNo());
		studentDetail.setBiometricCode(studentDetailDTO.getBiometricCode());
		studentDetail.setAadharCardNo(studentDetailDTO.getAadharCardNo());
		studentDetail.setAadharFilePath(FileUtil.getRelativePath(studentDetailDTO.getAadharFilePath()));
		studentDetail.setAdmissionNumber(studentDetailDTO.getAdmissionNumber());
		studentDetail.setIsCurrentYear(studentDetailDTO.getIsCurrentYear());
		studentDetail.setDateOfExpiry(studentDetailDTO.getDateOfExpiry());
		studentDetail.setDateOfIssue(studentDetailDTO.getDateOfIssue());
		studentDetail.setDateOfRegistration(studentDetailDTO.getDateOfRegistration());
		studentDetail.setEamcetRank(studentDetailDTO.getEamcetRank());
		studentDetail.setFatherEmailId(studentDetailDTO.getFatherEmailId());
		studentDetail.setFatherAddress(studentDetailDTO.getFatherAddress());
		studentDetail.setFatherMobileNo(studentDetailDTO.getFatherMobileNo());
		studentDetail.setFatherName(studentDetailDTO.getFatherName());
		studentDetail.setFatherOccupation(studentDetailDTO.getFatherOccupation());
		studentDetail.setFatherPhotoPath(FileUtil.getRelativePath(studentDetailDTO.getFatherPhotoPath()));
		studentDetail.setFatherQualification(studentDetailDTO.getFatherQualification());
		studentDetail.setFathersIncomePa(studentDetailDTO.getFathersIncomePa());
		studentDetail.setFirstName(studentDetailDTO.getFirstName());
		studentDetail.setFolderPath(studentDetailDTO.getFolderPath());
		studentDetail.setGuardianAddress(studentDetailDTO.getGuardianAddress());
		studentDetail.setGuardianEmailId(studentDetailDTO.getGuardianEmailId());
		studentDetail.setGuardianIncomePa(studentDetailDTO.getGuardianIncomePa());
		studentDetail.setGuardianName(studentDetailDTO.getGuardianName());
		studentDetail.setHallticketNumber(studentDetailDTO.getHallticketNumber());
		studentDetail.setHobbies(studentDetailDTO.getHobbies());
		studentDetail.setIdentificationMarks(studentDetailDTO.getIdentificationMarks());
		studentDetail.setInterests(studentDetailDTO.getInterests());
		studentDetail.setIsgovtempFather(studentDetailDTO.getIsgovtempFather());
		studentDetail.setIsgovtempMother(studentDetailDTO.getIsgovtempMother());
		studentDetail.setIsLocal(studentDetailDTO.getIsLocal());
		studentDetail.setLangStatus1(studentDetailDTO.getLangStatus1());
		studentDetail.setLangStatus2(studentDetailDTO.getLangStatus2());
		studentDetail.setLangStatus3(studentDetailDTO.getLangStatus3());
		studentDetail.setLangStatus4(studentDetailDTO.getLangStatus4());
		studentDetail.setLangStatus5(studentDetailDTO.getLangStatus5());
		studentDetail.setWeddingDate(studentDetailDTO.getWeddingDate());

		if (studentDetailDTO.getLanguage1Id() != null) {
			GeneralDetail lang1 = new GeneralDetail();
			lang1.setGeneralDetailId(studentDetailDTO.getLanguage1Id());
			studentDetail.setLanguage1(lang1);
		}

		if (studentDetailDTO.getLanguage2Id() != null) {
			GeneralDetail lang2 = new GeneralDetail();
			lang2.setGeneralDetailId(studentDetailDTO.getLanguage2Id());
			studentDetail.setLanguage2(lang2);
		}

		if (studentDetailDTO.getLanguage3Id() != null) {
			GeneralDetail lang3 = new GeneralDetail();
			lang3.setGeneralDetailId(studentDetailDTO.getLanguage3Id());
			studentDetail.setLanguage3(lang3);
		}

		if (studentDetailDTO.getLanguage4Id() != null) {
			GeneralDetail lang4 = new GeneralDetail();
			lang4.setGeneralDetailId(studentDetailDTO.getLanguage4Id());
			studentDetail.setLanguage4(lang4);
		}

		if (studentDetailDTO.getLanguage5Id() != null) {
			GeneralDetail lang5 = new GeneralDetail();
			lang5.setGeneralDetailId(studentDetailDTO.getLanguage5Id());
			studentDetail.setLanguage5(lang5);
		}
		if (studentDetailDTO.getTitleId() != null) {
			GeneralDetail title = new GeneralDetail();
			title.setGeneralDetailId(studentDetailDTO.getTitleId());
			studentDetail.setTitle(title);
		}
		if (studentDetailDTO.getStudentStatusId() != null) {
			GeneralDetail studentStatus = new GeneralDetail();
			studentStatus.setGeneralDetailId(studentDetailDTO.getStudentStatusId());
			studentDetail.setStudentStatus(studentStatus);
		}
		if (studentDetailDTO.getUserId() != null) {
			User user = new User();
			user.setUserId(studentDetailDTO.getUserId());
			studentDetail.setUser(user);
		}
		if (studentDetailDTO.getParentId() != null){
			User user = new User();
			user.setUserId(studentDetailDTO.getParentId());
			studentDetail.setParentUser(user);
		}
		studentDetail.setLastName(studentDetailDTO.getLastName());
		studentDetail.setMiddleName(studentDetailDTO.getMiddleName());
		studentDetail.setMobile(studentDetailDTO.getMobile());
		studentDetail.setMotherAddress(studentDetailDTO.getMotherAddress());
		studentDetail.setMotherEmailId(studentDetailDTO.getMotherEmailId());
		studentDetail.setMotherIncomePa(studentDetailDTO.getMotherIncomePa());
		studentDetail.setMotherMobileNo(studentDetailDTO.getMotherMobileNo());
		studentDetail.setMotherName(studentDetailDTO.getMotherName());
		studentDetail.setMotherOccupation(studentDetailDTO.getMotherOccupation());
		studentDetail.setMotherPhotoPath(FileUtil.getRelativePath(studentDetailDTO.getMotherPhotoPath()));
		studentDetail.setMotherQualification(studentDetailDTO.getMotherQualification());
		studentDetail.setPancardFilePath(FileUtil.getRelativePath(studentDetailDTO.getPancardFilePath()));
		studentDetail.setPancardNo(studentDetailDTO.getPancardNo());
		studentDetail.setPassportNo(studentDetailDTO.getPassportNo());
		studentDetail.setPermanentAddress(studentDetailDTO.getPermanentAddress());
		studentDetail.setPermanentPincode(studentDetailDTO.getPermanentPincode());
		studentDetail.setPermanentStreet(studentDetailDTO.getPermanentStreet());
		studentDetail.setPremanentMandal(studentDetailDTO.getPermanentMandal());

		if (studentDetailDTO.getPermanentCityId() != null) {
			City permanentCity = new City();
			permanentCity.setCityId(studentDetailDTO.getPermanentCityId());
			studentDetail.setPremenentCity(permanentCity);
			if (studentDetailDTO.getPermanentDistrictId() != null) {
				District district = new District();
				district.setDistrictId(studentDetailDTO.getPermanentDistrictId());
				district.setDistrictName(studentDetailDTO.getPermanentDistrictName());
				permanentCity.setDistrict(district);
				studentDetail.setPermanentDistrict(district);

				if (studentDetailDTO.getPermanentStateId() != null) {
					State state = new State();
					state.setStateId(studentDetailDTO.getPermanentStateId());
					state.setStateName(studentDetailDTO.getPermanentStateName());
					district.setState(state);

					if (studentDetailDTO.getPermanentCountryId() != null) {
						Country country = new Country();
						country.setCountryId(studentDetailDTO.getPermanentCountryId());
						country.setCountryName(studentDetailDTO.getPermanentCountryName());
						state.setCountry(country);
					}
				}
			}

		}

		if (studentDetailDTO.getPresentCityId() != null) {
			City presentCity = new City();
			presentCity.setCityId(studentDetailDTO.getPresentCityId());
			studentDetail.setPresentCity(presentCity);
			if (studentDetailDTO.getPresentDistrictId() != null) {
				District district = new District();
				district.setDistrictId(studentDetailDTO.getPresentDistrictId());
				district.setDistrictName(studentDetailDTO.getPresentDistrictName());
				presentCity.setDistrict(district);
				studentDetail.setPresentDistrict(district);

				if (studentDetailDTO.getPresentStateId() != null) {
					State state = new State();
					state.setStateId(studentDetailDTO.getPresentStateId());
					state.setStateName(studentDetailDTO.getPresentStateName());
					district.setState(state);

					if (studentDetailDTO.getPresentCountryId() != null) {
						Country country = new Country();
						country.setCountryId(studentDetailDTO.getPresentCountryId());
						country.setCountryName(studentDetailDTO.getPresentCountryName());
						state.setCountry(country);
					}
				}
			}
		}

		studentDetail.setPresentAddress(studentDetailDTO.getPresentAddress());
		studentDetail.setPresentMandal(studentDetailDTO.getPresentMandal());
		studentDetail.setPresentPincode(studentDetailDTO.getPresentPincode());
		studentDetail.setPresentStreet(studentDetailDTO.getPresentStreetName());
		studentDetail.setPrimaryContact(studentDetailDTO.getPrimaryContact());
		studentDetail.setReason(studentDetailDTO.getReason());
		studentDetail.setReceiptNo(studentDetailDTO.getReceiptNo());
		studentDetail.setRefApplicationNo(studentDetailDTO.getRefApplicationNo());
		studentDetail.setResidencePhone(studentDetailDTO.getResidencePhone());
		studentDetail.setRollNumber(studentDetailDTO.getRollNumber());
		studentDetail.setSscNo(studentDetailDTO.getSscNo());
		studentDetail.setStudentPhotoPath(FileUtil.getRelativePath(studentDetailDTO.getStudentPhotoPath()));

		studentDetail.setDateOfBirth(studentDetailDTO.getDateOfBirth());
		studentDetail.setStdEmailId(studentDetailDTO.getStdEmailId());
		studentDetail.setEntranceHtNo(studentDetailDTO.getEntranceHTNumber());
		if (studentDetailDTO.getCourseId() != null) {
			Course course = new Course();
			course.setCourseId(studentDetailDTO.getCourseId());
			studentDetail.setCourse(course);
		}
		if (studentDetailDTO.getSchoolId() != null) {
			School school = new School();
			school.setSchoolId(studentDetailDTO.getSchoolId());
			studentDetail.setSchool(school);
		}

		if (studentDetailDTO.getOrganizationId() != null) {
			Organization org = new Organization();
			org.setOrganizationId(studentDetailDTO.getOrganizationId());
			studentDetail.setOrganization(org);
		}

		if (studentDetailDTO.getCourseYearId() != null) {
			CourseYear courseYear = new CourseYear();
			courseYear.setCourseYearId(studentDetailDTO.getCourseYearId());
			studentDetail.setCourseYear(courseYear);
		}

		if (studentDetailDTO.getBatchId() != null) {
			Batch batch = new Batch();
			batch.setBatchId(studentDetailDTO.getBatchId());
			studentDetail.setBatch(batch);
		}
		if (studentDetailDTO.getCasteId() != null) {
			Caste caste = new Caste();
			caste.setCasteId(studentDetailDTO.getCasteId());
			studentDetail.setCaste(caste);
		}
		if (studentDetailDTO.getSubCasteId() != null) {
			SubCaste subCaste = new SubCaste();
			subCaste.setSubCasteId(studentDetailDTO.getSubCasteId());
			studentDetail.setSubCaste(subCaste);
		}



		if (studentDetailDTO.getDisabilityId() != null) {
			GeneralDetail disability = new GeneralDetail();
			disability.setGeneralDetailId(studentDetailDTO.getDisabilityId());
			studentDetail.setDisability(disability);
		}

		if (studentDetailDTO.getGenderId() != null) {
			GeneralDetail gender = new GeneralDetail();
			gender.setGeneralDetailId(studentDetailDTO.getGenderId());
			studentDetail.setGender(gender);
		}

		if (studentDetailDTO.getQuotaId() != null) {
			GeneralDetail quota = new GeneralDetail();
			quota.setGeneralDetailId(studentDetailDTO.getQuotaId());
			quota.setGeneralDetailDisplayName(studentDetailDTO.getQuotaDisplayName());
			quota.setGeneralDetailCode(studentDetailDTO.getQuotaCode());
			studentDetail.setQuota(quota);
		}
		if (studentDetailDTO.getReligionId() != null) {
			GeneralDetail religion = new GeneralDetail();
			religion.setGeneralDetailId(studentDetailDTO.getReligionId());
			studentDetail.setReligion(religion);
		}
		if (studentDetailDTO.getStudentTypeId() != null) {
			GeneralDetail studentType = new GeneralDetail();
			studentType.setGeneralDetailId(studentDetailDTO.getStudentTypeId());
			studentDetail.setStudentType(studentType);
		}

		if (studentDetailDTO.getBloodgroupId() != null) {
			GeneralDetail bloodGroup = new GeneralDetail();
			bloodGroup.setGeneralDetailId(studentDetailDTO.getBloodgroupId());
			studentDetail.setBloodgroup(bloodGroup);
		}
		if (studentDetailDTO.getNationalityId() != null) {
			GeneralDetail nationality = new GeneralDetail();
			nationality.setGeneralDetailId(studentDetailDTO.getNationalityId());
			studentDetail.setNationality(nationality);
		}

		if (studentDetailDTO.getGroupSectionId() != null) {
			GroupSection groupSection = new GroupSection();
			groupSection.setGroupSectionId(studentDetailDTO.getGroupSectionId());
			studentDetail.setGroupSection(groupSection);
		}
		if (studentDetailDTO.getAcademicYearId() != null) {
			AcademicYear academicYear = new AcademicYear();
			academicYear.setAcademicYearId(studentDetailDTO.getAcademicYearId());
			studentDetail.setAcademicYear(academicYear);
		}
		// Set the StudentDocumentCollection details
		if (!CollectionUtils.isEmpty(studentDetailDTO.getStudentDocumentCollections())) {
			List<StudentDocumentCollection> studentDocCollectionList = new ArrayList<>();
			for (StudentDocumentCollectionDTO studentDocumentCollectionDTO : studentDetailDTO
					.getStudentDocumentCollections()) {
				StudentDocumentCollection studentDocumentCollection = new StudentDocumentCollection();

				if (studentDocumentCollectionDTO.getStdDocCollId() == null) {
					studentDocumentCollection.setCreatedDt(new Date());
					studentDocumentCollection.setCreatedUser(SecurityUtil.getCurrentUser());
					studentDocumentCollection.setIsActive(Boolean.TRUE);
				} else {
					studentDocumentCollection.setStudentDocCollId(studentDocumentCollectionDTO.getStdDocCollId());
					studentDocumentCollection.setCreatedDt(studentDocumentCollectionDTO.getCreatedDt());
					studentDocumentCollection.setCreatedUser(studentDocumentCollectionDTO.getCreatedUser());
					studentDocumentCollection.setIsActive(studentDocumentCollectionDTO.getIsActive());
					studentDocumentCollection.setUpdatedDt(new Date());
					studentDocumentCollection.setUpdatedUser(SecurityUtil.getCurrentUser());
				}
				studentDocumentCollection.setFileName(studentDocumentCollectionDTO.getFileName());
				studentDocumentCollection
						.setFilePath(FileUtil.getRelativePath(studentDocumentCollectionDTO.getFilePath()));
				studentDocumentCollection.setIsHardCopy(studentDocumentCollectionDTO.getIsHardCopy());
				studentDocumentCollection.setIsSoftCopy(studentDocumentCollectionDTO.getIsSoftCopy());
				studentDocumentCollection.setIsOriginal(studentDocumentCollectionDTO.getIsOriginal());
				studentDocumentCollection.setIsVerified(studentDocumentCollectionDTO.getIsVerified());
				studentDocumentCollection.setRackNumber(studentDocumentCollectionDTO.getRackNumber());
				studentDocumentCollection.setReason(studentDocumentCollectionDTO.getReason());

				if (studentDocumentCollectionDTO.getDocRepId() != null) {
					DocumentRepository documentRepository = new DocumentRepository();
					documentRepository.setDocumentRepositoryId(studentDocumentCollectionDTO.getDocRepId());
					studentDocumentCollection.setDocumentRepository(documentRepository);
				}

				if (studentDocumentCollectionDTO.getAppDocCollId() != null) {
					StudentAppDocCollection studentAppDocCollection = new StudentAppDocCollection();
					studentAppDocCollection.setAppDocCollId(studentDocumentCollectionDTO.getAppDocCollId());
					studentDocumentCollection.setStudentAppDocCollection(studentAppDocCollection);
				}

				if (studentDocumentCollectionDTO.getVerifiedByEmpId() != null) {
					EmployeeDetail verifiedbyEmployeeDetail = new EmployeeDetail();
					verifiedbyEmployeeDetail.setEmployeeId(studentDocumentCollectionDTO.getVerifiedByEmpId());
					studentDocumentCollection.setVerifiedbyEmployeeDetail(verifiedbyEmployeeDetail);
				}

				if (studentDocumentCollectionDTO.getStdDocCollId() == null) {
					studentDocumentCollection.setStudentDetail(studentDetail);
				} else {
					StudentDetail eduStudentDetail = new StudentDetail();
					eduStudentDetail.setStudentId(studentDetail.getStudentId());
					studentDocumentCollection.setStudentDetail(studentDetail);
				}

				studentDocCollectionList.add(studentDocumentCollection);
			}
			if (!CollectionUtils.isEmpty(studentDocCollectionList)) {
				studentDetail.setStdDocumentCollections(studentDocCollectionList);
			}
		}

		// Set the StudentActivitiesDetails details
		if (!CollectionUtils.isEmpty(studentDetailDTO.getStudentActivitiesDetails())) {
			List<StudentActivitiesDetail> studentActivitiesDetailsList = new ArrayList<>();
			for (StudentActivitiesDetailDTO studentActivitiesDetailDTO : studentDetailDTO
					.getStudentActivitiesDetails()) {
				StudentActivitiesDetail studentActivitiesDetail = new StudentActivitiesDetail();

				if (studentActivitiesDetailDTO.getStudentActivityId() == null) {
					studentActivitiesDetail.setCreatedDt(new Date());
					studentActivitiesDetail.setCreatedUser(SecurityUtil.getCurrentUser());
					studentActivitiesDetail.setIsActive(Boolean.TRUE);
				} else {
					studentActivitiesDetail.setStudentActivityId(studentActivitiesDetailDTO.getStudentActivityId());
					studentActivitiesDetail.setCreatedDt(studentActivitiesDetailDTO.getCreatedDt());
					studentActivitiesDetail.setCreatedUser(studentActivitiesDetailDTO.getCreatedUser());
					studentActivitiesDetail.setIsActive(studentActivitiesDetailDTO.getIsActive());
					studentActivitiesDetail.setUpdatedDt(new Date());
					studentActivitiesDetail.setUpdatedUser(SecurityUtil.getCurrentUser());
				}

				studentActivitiesDetail.setStudentActivityId(studentActivitiesDetailDTO.getStudentActivityId());
				studentActivitiesDetail.setLevel(studentActivitiesDetailDTO.getLevel());
				studentActivitiesDetail.setParticulars(studentActivitiesDetailDTO.getParticulars());
				studentActivitiesDetail.setSponsoredBy(studentActivitiesDetailDTO.getSponsoredBy());
				studentActivitiesDetail.setReason(studentActivitiesDetailDTO.getReason());

				if (studentActivitiesDetailDTO.getStudentActivityId() == null) {
					studentActivitiesDetail.setStudentDetail(studentDetail);
				} else {
					StudentDetail actStudentDetail = new StudentDetail();
					actStudentDetail.setStudentId(studentDetail.getStudentId());
					studentActivitiesDetail.setStudentDetail(actStudentDetail);
				}

				studentActivitiesDetailsList.add(studentActivitiesDetail);
			}
			if (!CollectionUtils.isEmpty(studentActivitiesDetailsList)) {
				studentDetail.setActivitiesDetails(studentActivitiesDetailsList);
			}
		}

		// Set the StudentEducations details
		if (!CollectionUtils.isEmpty(studentDetailDTO.getStudentEducationDetails())) {
			List<StudentEducationDetail> studentEducationDetailList = new ArrayList<>();
			for (StudentEducationDetailDTO studentEducationDetailDTO : studentDetailDTO.getStudentEducationDetails()) {
				StudentEducationDetail studentEducationDetail = new StudentEducationDetail();

				if (studentEducationDetailDTO.getStudentEducationId() == null) {
					studentEducationDetail.setCreatedDt(new Date());
					studentEducationDetail.setCreatedUser(SecurityUtil.getCurrentUser());
					studentEducationDetail.setIsActive(Boolean.TRUE);
				} else {
					studentEducationDetail.setStudentEducationId(studentEducationDetailDTO.getStudentEducationId());
					studentEducationDetail.setCreatedDt(studentEducationDetailDTO.getCreatedDt());
					studentEducationDetail.setCreatedUser(studentEducationDetailDTO.getCreatedUser());
					studentEducationDetail.setIsActive(studentEducationDetailDTO.getIsActive());
					studentEducationDetail.setUpdatedDt(new Date());
					studentEducationDetail.setUpdatedUser(SecurityUtil.getCurrentUser());
				}

				studentEducationDetail.setAddress(studentEducationDetailDTO.getAddress());
				studentEducationDetail.setBoard(studentEducationDetailDTO.getBoard());
				studentEducationDetail.setGradeClassSecured(studentEducationDetailDTO.getGradeClassSecured());
				studentEducationDetail.setMajorSubjects(studentEducationDetailDTO.getMajorSubjects());
				studentEducationDetail.setMedium(studentEducationDetailDTO.getMedium());
				studentEducationDetail.setNameOfInstitution(studentEducationDetailDTO.getNameOfInstitution());
				studentEducationDetail.setPrecentage(studentEducationDetailDTO.getPrecentage());
				studentEducationDetail.setYearOfCompletion(studentEducationDetailDTO.getYearOfCompletion());
				studentEducationDetail.setReason(studentEducationDetailDTO.getReason());

				if (studentEducationDetailDTO.getGeneralDetailId() != null) {
					GeneralDetail gd = new GeneralDetail();
					gd.setGeneralDetailId(studentEducationDetailDTO.getGeneralDetailId());
					studentEducationDetail.setTMGeneralDetail(gd);
				}

				if (studentEducationDetailDTO.getAppEducationId() != null) {
					StudentAppEducation studentAppEducation = new StudentAppEducation();
					studentAppEducation.setAppEducationId(studentEducationDetailDTO.getAppEducationId());
					studentEducationDetail.setStudentAppEducation(studentAppEducation);
				}

				if (studentEducationDetailDTO.getStudentEducationId() == null) {
					studentEducationDetail.setStudentDetail(studentDetail);
				} else {
					StudentDetail eduStudentDetail = new StudentDetail();
					eduStudentDetail.setStudentId(studentDetail.getStudentId());
					studentEducationDetail.setStudentDetail(eduStudentDetail);
				}

				studentEducationDetailList.add(studentEducationDetail);
			}
			if (!CollectionUtils.isEmpty(studentEducationDetailList)) {
				studentDetail.setStdEducationDetails(studentEducationDetailList);
			}
		}

		return studentDetail;
	}


	@Override
	public StudentDetailDTO convertEntityToDTO(StudentDetail studentDetail) {
		StudentDetailDTO studentDetailDTO = null;
		if (studentDetail != null) {
			studentDetailDTO = new StudentDetailDTO();
			studentDetailDTO.setStudentId(studentDetail.getStudentId());
			studentDetailDTO.setApplicationNo(studentDetail.getApplicationNo());
			studentDetailDTO.setBiometricCode(studentDetail.getBiometricCode());
			studentDetailDTO.setAadharCardNo(studentDetail.getAadharCardNo());
			// need abslou
			if (studentDetail.getAadharFilePath() != null) {
				/*
				 * StringBuilder aadharFilePath=new StringBuilder(); aadharFilePath =
				 * aadharFilePath.append(url).append(fileSeparator).append(bucketName).append(
				 * fileSeparator) .append(studentDetail.getAadharFilePath());
				 */
				studentDetailDTO.setAadharFilePath(FileUtil.getAbsolutePath(studentDetail.getAadharFilePath()));
			}

			studentDetailDTO.setAdminssionDate(studentDetail.getAdminssionDate());
			studentDetailDTO.setAdmissionNumber(studentDetail.getAdmissionNumber());
			studentDetailDTO.setCreatedDt(studentDetail.getCreatedDt());
			studentDetailDTO.setCreatedUser(studentDetail.getCreatedUser());
			studentDetailDTO.setIsMinority(studentDetail.getIsMinority());

			if (studentDetail.getQualifying() != null) {
				studentDetailDTO.setQualifyingId(studentDetail.getQualifying().getGeneralDetailId());
				studentDetailDTO.setQualifyingName(studentDetail.getQualifying().getGeneralDetailDisplayName());
				studentDetailDTO.setQualifyingCode(studentDetail.getQualifying().getGeneralDetailCode());
			}
			if (studentDetail.getGroupSection() != null) {
				studentDetailDTO.setGroupSectionId(studentDetail.getGroupSection().getGroupSectionId());
				studentDetailDTO.setSection(studentDetail.getGroupSection().getSection());
			}
			if (studentDetail.getAcademicYear() != null) {
				studentDetailDTO.setAcademicYearId(studentDetail.getAcademicYear().getAcademicYearId());
				studentDetailDTO.setAcademicYear(studentDetail.getAcademicYear().getAcademicYear());
			}
			if (studentDetail.getBatch() != null) {
				studentDetailDTO.setBatchId(studentDetail.getBatch().getBatchId());
				studentDetailDTO.setBatchName(studentDetail.getBatch().getBatchName());
			}
			if (studentDetail.getBloodgroup() != null) {
				studentDetailDTO.setBloodgroupId(studentDetail.getBloodgroup().getGeneralDetailId());
				studentDetailDTO.setBloodgroupDisplayName(studentDetail.getBloodgroup().getGeneralDetailDisplayName());
			}
			if (studentDetail.getCourse() != null) {
				studentDetailDTO.setCourseId(studentDetail.getCourse().getCourseId());
				studentDetailDTO.setCourseName(studentDetail.getCourse().getCourseName());
			}
			if (studentDetail.getCourseYear() != null) {
				studentDetailDTO.setCourseYearId(studentDetail.getCourseYear().getCourseYearId());
				studentDetailDTO.setCourseYearName(studentDetail.getCourseYear().getCourseYearName());
			}

			if (studentDetail.getUser() != null) {
				studentDetailDTO.setUserId(studentDetail.getUser().getUserId());
			}

			if (studentDetail.getLanguage1() != null) {
				studentDetailDTO.setLanguage1Id(studentDetail.getLanguage1().getGeneralDetailId());
			}

			if (studentDetail.getLanguage2() != null) {
				studentDetailDTO.setLanguage2Id(studentDetail.getLanguage2().getGeneralDetailId());
			}

			if (studentDetail.getLanguage3() != null) {
				studentDetailDTO.setLanguage3Id(studentDetail.getLanguage3().getGeneralDetailId());
			}

			if (studentDetail.getLanguage4() != null) {
				studentDetailDTO.setLanguage4Id(studentDetail.getLanguage4().getGeneralDetailId());
			}

			if (studentDetail.getLanguage5() != null) {
				studentDetailDTO.setLanguage5Id(studentDetail.getLanguage5().getGeneralDetailId());
			}

			studentDetailDTO.setDateOfExpiry(studentDetail.getDateOfExpiry());
			studentDetailDTO.setDateOfIssue(studentDetail.getDateOfIssue());
			studentDetailDTO.setDateOfRegistration(studentDetail.getDateOfRegistration());
			if (studentDetail.getDisability() != null) {
				studentDetailDTO.setDisabilityId(studentDetail.getDisability().getGeneralDetailId());
				studentDetailDTO.setDisabilityDisplayName(studentDetail.getDisability().getGeneralDetailDisplayName());
			}
			studentDetailDTO.setDateOfBirth(studentDetail.getDateOfBirth());
			studentDetailDTO.setEamcetRank(studentDetail.getEamcetRank());
			studentDetailDTO.setStdEmailId(studentDetail.getStdEmailId());
			studentDetailDTO.setEntranceHtNo(studentDetail.getEntranceHtNo());
			studentDetailDTO.setFatherEmailId(studentDetail.getFatherEmailId());
			studentDetailDTO.setFatherAddress(studentDetail.getFatherAddress());
			studentDetailDTO.setFatherMobileNo(studentDetail.getFatherMobileNo());
			studentDetailDTO.setFatherName(studentDetail.getFatherName());
			studentDetailDTO.setFatherOccupation(studentDetail.getFatherOccupation());
			if (studentDetail.getFatherPhotoPath() != null) {
				/*
				 * StringBuilder fatherPhotoPath=new StringBuilder(); fatherPhotoPath =
				 * fatherPhotoPath.append(url).append(fileSeparator).append(bucketName).append(
				 * fileSeparator) .append(studentDetail.getFatherPhotoPath());
				 */

				studentDetailDTO.setFatherPhotoPath(FileUtil.getAbsolutePath(studentDetail.getFatherPhotoPath()));
			}

			studentDetailDTO.setFatherQualification(studentDetail.getFatherQualification());
			studentDetailDTO.setFirstName(studentDetail.getFirstName());
			studentDetailDTO.setFolderPath(studentDetail.getFolderPath());
			if (studentDetail.getGender() != null) {
				studentDetailDTO.setGenderId(studentDetail.getGender().getGeneralDetailId());
				studentDetailDTO.setGender(studentDetail.getGender().getGeneralDetailDisplayName());
			}
			studentDetailDTO.setGuardianAddress(studentDetail.getGuardianAddress());
			studentDetailDTO.setGuardianEmailId(studentDetail.getGuardianEmailId());
			studentDetailDTO.setGuardianIncomePa(studentDetail.getGuardianIncomePa());
			studentDetailDTO.setGuardianName(studentDetail.getGuardianName());
			studentDetailDTO.setHallticketNumber(studentDetail.getHallticketNumber());
			studentDetailDTO.setHobbies(studentDetail.getHobbies());
			studentDetailDTO.setIdentificationMarks(studentDetail.getIdentificationMarks());
			studentDetailDTO.setInterests(studentDetail.getInterests());
			studentDetailDTO.setIsActive(studentDetail.getIsActive());
			studentDetailDTO.setIsgovtempFather(studentDetail.getIsgovtempFather());
			studentDetailDTO.setIsgovtempMother(studentDetail.getIsgovtempMother());
			studentDetailDTO.setIsLocal(studentDetail.getIsLocal());
			studentDetailDTO.setLangStatus1(studentDetail.getLangStatus1());
			studentDetailDTO.setLangStatus2(studentDetail.getLangStatus2());
			studentDetailDTO.setLangStatus3(studentDetail.getLangStatus3());
			studentDetailDTO.setLangStatus4(studentDetail.getLangStatus4());
			studentDetailDTO.setLangStatus5(studentDetail.getLangStatus5());
			studentDetailDTO.setLastName(studentDetail.getLastName());
			studentDetailDTO.setMiddleName(studentDetail.getMiddleName());
			studentDetailDTO.setMobile(studentDetail.getMobile());
			studentDetailDTO.setMotherAddress(studentDetail.getMotherAddress());
			studentDetailDTO.setMotherEmailId(studentDetail.getMotherEmailId());
			studentDetailDTO.setMotherIncomePa(studentDetail.getMotherIncomePa());
			studentDetailDTO.setMotherMobileNo(studentDetail.getMotherMobileNo());
			studentDetailDTO.setMotherName(studentDetail.getMotherName());
			studentDetailDTO.setMotherOccupation(studentDetail.getMotherOccupation());

			if (studentDetail.getMotherPhotoPath() != null) {
				/*
				 * StringBuilder motherPhotoPath=new StringBuilder(); motherPhotoPath =
				 * motherPhotoPath.append(url).append(fileSeparator).append(bucketName).append(
				 * fileSeparator) .append(studentDetail.getMotherPhotoPath());
				 */
				studentDetailDTO.setMotherPhotoPath(FileUtil.getAbsolutePath(studentDetail.getMotherPhotoPath()));

			}

			studentDetailDTO.setMotherQualification(studentDetail.getMotherQualification());

			if (studentDetail.getNationality() != null) {
				studentDetailDTO.setNationalityId(studentDetail.getNationality().getGeneralDetailId());
				studentDetailDTO
						.setNationalityDisplayName(studentDetail.getNationality().getGeneralDetailDisplayName());
			}

			if (studentDetail.getPancardFilePath() != null) {
//			StringBuilder pancardPhotoPath=new StringBuilder();
//			pancardPhotoPath = pancardPhotoPath.append(url).append(fileSeparator).append(bucketName).append(fileSeparator)
//					.append(studentDetail.getPancardFilePath());

				studentDetailDTO.setPancardFilePath(FileUtil.getAbsolutePath(studentDetail.getPancardFilePath()));
			}

			studentDetailDTO.setPancardNo(studentDetail.getPancardNo());
			studentDetailDTO.setPassportNo(studentDetail.getPassportNo());
			studentDetailDTO.setPermanentAddress(studentDetail.getPermanentAddress());
			studentDetailDTO.setPermanentPincode(studentDetail.getPermanentPincode());
			studentDetailDTO.setPermanentStreet(studentDetail.getPermanentStreet());
			studentDetailDTO.setPresentAddress(studentDetail.getPresentAddress());
			studentDetailDTO.setPresentMandal(studentDetail.getPresentMandal());
			studentDetailDTO.setPresentPincode(studentDetail.getPresentPincode());
			studentDetailDTO.setPresentStreetName(studentDetail.getPresentStreet());

			if (studentDetail.getPresentCity() != null) {
				studentDetailDTO.setPresentCityId(studentDetail.getPresentCity().getCityId());
				studentDetailDTO.setPresentCityName(studentDetail.getPresentCity().getCityName());
				if (studentDetail.getPresentDistrict() != null) {
					studentDetailDTO.setPresentDistrictId(studentDetail.getPresentDistrict().getDistrictId());
					studentDetailDTO.setPresentDistrictName(studentDetail.getPresentDistrict().getDistrictName());
					if (studentDetail.getPresentDistrict().getState() != null) {
						studentDetailDTO.setPresentStateId(studentDetail.getPresentDistrict().getState().getStateId());
						studentDetailDTO
								.setPresentStateName(studentDetail.getPresentDistrict().getState().getStateName());
						if (studentDetail.getPresentDistrict().getState().getCountry() != null) {
							studentDetailDTO.setPresentCountryId(
									studentDetail.getPresentDistrict().getState().getCountry().getCountryId());
							studentDetailDTO.setPresentCountryName(
									studentDetail.getPresentDistrict().getState().getCountry().getCountryName());

						}
					}

				}

			}

			if (studentDetail.getPremenentCity() != null) {
				studentDetailDTO.setPermanentCityId(studentDetail.getPremenentCity().getCityId());
				studentDetailDTO.setPermanentCityName(studentDetail.getPremenentCity().getCityName());
				if (studentDetail.getPermanentDistrict() != null) {
					studentDetailDTO.setPermanentDistrictId(studentDetail.getPermanentDistrict().getDistrictId());
					studentDetailDTO.setPermanentDistrictName(studentDetail.getPermanentDistrict().getDistrictName());
					if (studentDetail.getPermanentDistrict().getState() != null) {
						studentDetailDTO
								.setPermanentStateId(studentDetail.getPermanentDistrict().getState().getStateId());
						studentDetailDTO
								.setPermanentStateName(studentDetail.getPermanentDistrict().getState().getStateName());
						if (studentDetail.getPermanentDistrict().getState().getCountry() != null) {
							studentDetailDTO.setPermanentCountryId(
									studentDetail.getPermanentDistrict().getState().getCountry().getCountryId());
							studentDetailDTO.setPermanentCountryName(
									studentDetail.getPermanentDistrict().getState().getCountry().getCountryName());

						}
					}

				}
			}

			studentDetailDTO.setPrimaryContact(studentDetail.getPrimaryContact());
			if (studentDetail.getQuota() != null) {
				studentDetailDTO.setQuotaId(studentDetail.getQuota().getGeneralDetailId());
				studentDetailDTO.setQuotaDisplayName(studentDetail.getQuota().getGeneralDetailDisplayName());
				studentDetailDTO.setQuotaCode(studentDetail.getQuota().getGeneralDetailCode());
			}
			if (studentDetail.getParentUser() != null) {
				studentDetailDTO.setParentUserId(studentDetail.getParentUser().getUserId());
				studentDetailDTO.setParentFirstName(studentDetail.getParentUser().getFirstName());
			}
			studentDetailDTO.setReason(studentDetail.getReason());
			studentDetailDTO.setReceiptNo(studentDetail.getReceiptNo());
			studentDetailDTO.setRefApplicationNo(studentDetail.getRefApplicationNo());

			if (studentDetail.getReligion() != null) {
				studentDetailDTO.setReligionId(studentDetail.getReligion().getGeneralDetailId());
				studentDetailDTO.setReligionDisplayName(studentDetail.getReligion().getGeneralDetailDisplayName());
			}
			studentDetailDTO.setResidencePhone(studentDetail.getResidencePhone());
			studentDetailDTO.setRollNumber(studentDetail.getRollNumber());
			studentDetailDTO.setSscNo(studentDetail.getSscNo());
			studentDetailDTO.setStudentPhotoPath(FileUtil.getAbsolutePath(studentDetail.getStudentPhotoPath()));
			if (studentDetail.getStudentType() != null) {
				studentDetailDTO.setStudentTypeId(studentDetail.getStudentType().getGeneralDetailId());
				studentDetailDTO
						.setStudentTypeDisplayName(studentDetail.getStudentType().getGeneralDetailDisplayName());
			}
			if (studentDetail.getSubCaste() != null) {
				studentDetailDTO.setSubCasteId(studentDetail.getSubCaste().getSubCasteId());
				studentDetailDTO.setSubCasteName(studentDetail.getSubCaste().getSubCaste());
			}
			if (studentDetail.getCaste() != null) {
				studentDetailDTO.setCasteId(studentDetail.getCaste().getCasteId());
				studentDetailDTO.setCasteName(studentDetail.getCaste().getCaste());
			}
			/*if (studentDetail.getCourseGroup() != null) {
				studentDetailDTO.setCourseGroupId(studentDetail.getCourseGroup().getCourseGroupId());
				studentDetailDTO.setCourseGroupName(studentDetail.getCourseGroup().getGroupName());
				studentDetailDTO.setGroupCode(studentDetail.getCourseGroup().getGroupCode());
			}*/

			if (studentDetail.getSchool() != null) {
				studentDetailDTO.setSchoolId(studentDetail.getSchool().getSchoolId());
				studentDetailDTO.setSchoolCode(studentDetail.getSchool().getSchoolCode());
				studentDetailDTO.setSchoolName(studentDetail.getSchool().getSchoolName());

			}
			if (studentDetail.getOrganization() != null) {
				studentDetailDTO.setOrganizationId(studentDetail.getOrganization().getOrganizationId());
				studentDetailDTO.setOrgCode(studentDetail.getOrganization().getOrgCode());
				studentDetailDTO.setOrgName(studentDetail.getOrganization().getOrgName());

			}

			if (studentDetail.getIsCurrentYear() != null) {
				studentDetailDTO.setIsCurrentYear(studentDetail.getIsCurrentYear());
			}
			if (studentDetail.getIsLateral() != null) {
				studentDetailDTO.setIsLateral(studentDetail.getIsLateral());
			}
			if (studentDetail.getIsLateral() != null) {
				studentDetailDTO.setIsScholorship(studentDetail.getIsScholorship());
			}

			// setStudentAppDocumentCollection(studentDetailDTO, studentDetail);
			// setStudentAppActivities(studentDetailDTO, studentDetail);
			// setStudentAppEducations(studentDetailDTO, studentDetail);

			if (studentDetail.getStdDocumentCollections() != null) {
				studentDetailDTO.setStudentDocumentCollections(studentDocumentCollectionMapper
						.convertStudentDocumentCollectionListToStudentDocumentCollectionDTOList(
								studentDetail.getStdDocumentCollections()));
			}
			if (studentDetail.getActivitiesDetails() != null) {
				studentDetailDTO.setStudentActivitiesDetails(
						studentActivitiesDetailMapper.convertEntityListToDTOList(studentDetail.getActivitiesDetails()));
			}
			if (studentDetail.getStdEducationDetails() != null) {
				studentDetailDTO.setStudentEducationDetails(
						studentEducationDetailMapper.convertEntityListToDTOList(studentDetail.getStdEducationDetails()));
			}

			if (studentDetail.getTitle() != null) {
				studentDetailDTO.setTitleId(studentDetail.getTitle().getGeneralDetailId());
				studentDetailDTO.setTitle(studentDetail.getTitle().getGeneralDetailDisplayName());
			}
			studentDetailDTO.setEntranceHTNumber(studentDetail.getEntranceHtNo());
			studentDetailDTO.setFathersIncomePa(studentDetail.getFathersIncomePa());
			studentDetailDTO.setLangStatus1(studentDetail.getLangStatus1());
			studentDetailDTO.setLangStatus2(studentDetail.getLangStatus2());
			studentDetailDTO.setLangStatus3(studentDetail.getLangStatus3());
			studentDetailDTO.setLangStatus4(studentDetail.getLangStatus4());
			studentDetailDTO.setLangStatus5(studentDetail.getLangStatus5());
			studentDetailDTO.setPermanentMandal(studentDetail.getPremanentMandal());


			if (studentDetail.getStudentStatus() != null) {
				studentDetailDTO.setStudentStatusId(studentDetail.getStudentStatus().getGeneralDetailId());
				studentDetailDTO.setStudentStatusDisplayName(studentDetail.getStudentStatus().getGeneralDetailDisplayName());
				studentDetailDTO.setStudentStatusCode(studentDetail.getStudentStatus().getGeneralDetailCode());
			}
		}
		return studentDetailDTO;
	}

	@Override
	public List<StudentDetailDTO> convertEntityListToDTOList(List<StudentDetail> entityList) {
		List<StudentDetailDTO> dtoObjectList = new ArrayList<>();
		entityList.forEach(entityObject -> dtoObjectList.add(convertStudentDetailToStudentDetailDTO(entityObject)));
		return dtoObjectList;
	}

	@Override
	public List<StudentDetail> convertDTOListToEntityList(List<StudentDetailDTO> dtoList) {
		List<StudentDetail> studentDetailList = new ArrayList<>();
		dtoList.forEach(studentDetailDTOTO -> studentDetailList.add(convertStudentDetailDTOToStudentDetail(studentDetailDTOTO, null)));
		return studentDetailList;
	}

	public List<StudentDetailDTO> convertStudentDetailToStudentDetailDTO(List<StudentDetail> studentDetail) {
		List<StudentDetailDTO> dtoObjectList = new ArrayList<>();
		studentDetail.forEach(entityObject -> dtoObjectList.add(convertStudentDetailToStudentDetailDTO(entityObject)));
		return dtoObjectList;
	}


	public List<StudentDetail> convertStudentDetailDTOToStudentDetail(List<StudentDetailDTO> studentDetailDtoList) {

		List<StudentDetail> studentDetailList = new ArrayList<>();
		studentDetailDtoList.forEach(studentDetailDTOTO -> studentDetailList.add(convertStudentDetailDTOToStudentDetail(studentDetailDTOTO, null)));
		return studentDetailList;

	}

	@Override
	public StudentDetail convertDTOtoEntity(StudentDetailDTO studentDetailDTO, StudentDetail studentDetail) {
		if (null == studentDetail) {
			studentDetail = new StudentDetail();
		}
		if (studentDetailDTO.getStudentId() == null) {
			studentDetail.setCreatedDt(new Date());
			studentDetail.setCreatedUser(SecurityUtil.getCurrentUser());
			studentDetail.setIsActive(Boolean.TRUE);
			studentDetail.setAdminssionDate(new Date());
		} else {
			studentDetail.setCreatedDt(studentDetailDTO.getCreatedDt());
			studentDetail.setCreatedUser(studentDetailDTO.getCreatedUser());
			studentDetail.setAdminssionDate(studentDetailDTO.getAdminssionDate());
			studentDetail.setIsActive(studentDetailDTO.getIsActive());
		}
		studentDetail.setUpdatedDt(new Date());
		studentDetail.setUpdatedUser(SecurityUtil.getCurrentUser());
		if (studentDetailDTO.getStudentAppId() != null) {
			StudentApplication stdApp = new StudentApplication();
			stdApp.setStudentAppId(studentDetailDTO.getStudentAppId());
			studentDetail.setStudentApplication(stdApp);
		}
		if (studentDetailDTO.getQualifyingId() != null) {
			GeneralDetail generalDetail = new GeneralDetail();
			generalDetail.setGeneralDetailId(studentDetailDTO.getQualifyingId());
			studentDetail.setQualifying(generalDetail);
		}
		studentDetail.setIsMinority(studentDetailDTO.getIsMinority());

		studentDetail.setStudentId(studentDetailDTO.getStudentId());
		studentDetail.setIsLateral(studentDetailDTO.getIsLateral());
		studentDetail.setIsScholorship(studentDetailDTO.getIsScholorship());
		studentDetail.setApplicationNo(studentDetailDTO.getApplicationNo());
		studentDetail.setBiometricCode(studentDetailDTO.getBiometricCode());
		studentDetail.setAadharCardNo(studentDetailDTO.getAadharCardNo());
		studentDetail.setAadharFilePath(FileUtil.getRelativePath(studentDetailDTO.getAadharFilePath()));
		studentDetail.setAdmissionNumber(studentDetailDTO.getAdmissionNumber());
		studentDetail.setIsCurrentYear(studentDetailDTO.getIsCurrentYear());
		studentDetail.setDateOfExpiry(studentDetailDTO.getDateOfExpiry());
		studentDetail.setDateOfIssue(studentDetailDTO.getDateOfIssue());
		studentDetail.setDateOfRegistration(studentDetailDTO.getDateOfRegistration());
		studentDetail.setEamcetRank(studentDetailDTO.getEamcetRank());
		studentDetail.setFatherEmailId(studentDetailDTO.getFatherEmailId());
		studentDetail.setFatherAddress(studentDetailDTO.getFatherAddress());
		studentDetail.setFatherMobileNo(studentDetailDTO.getFatherMobileNo());
		studentDetail.setFatherName(studentDetailDTO.getFatherName());
		studentDetail.setFatherOccupation(studentDetailDTO.getFatherOccupation());
		studentDetail.setFatherPhotoPath(FileUtil.getRelativePath(studentDetailDTO.getFatherPhotoPath()));
		studentDetail.setFatherQualification(studentDetailDTO.getFatherQualification());
		studentDetail.setFathersIncomePa(studentDetailDTO.getFathersIncomePa());
		studentDetail.setFirstName(studentDetailDTO.getFirstName());
		studentDetail.setFolderPath(studentDetailDTO.getFolderPath());
		studentDetail.setGuardianAddress(studentDetailDTO.getGuardianAddress());
		studentDetail.setGuardianEmailId(studentDetailDTO.getGuardianEmailId());
		studentDetail.setGuardianIncomePa(studentDetailDTO.getGuardianIncomePa());
		studentDetail.setGuardianName(studentDetailDTO.getGuardianName());
		studentDetail.setHallticketNumber(studentDetailDTO.getHallticketNumber());
		studentDetail.setHobbies(studentDetailDTO.getHobbies());
		studentDetail.setIdentificationMarks(studentDetailDTO.getIdentificationMarks());
		studentDetail.setInterests(studentDetailDTO.getInterests());
		studentDetail.setIsgovtempFather(studentDetailDTO.getIsgovtempFather());
		studentDetail.setIsgovtempMother(studentDetailDTO.getIsgovtempMother());
		studentDetail.setIsLocal(studentDetailDTO.getIsLocal());
		studentDetail.setLangStatus1(studentDetailDTO.getLangStatus1());
		studentDetail.setLangStatus2(studentDetailDTO.getLangStatus2());
		studentDetail.setLangStatus3(studentDetailDTO.getLangStatus3());
		studentDetail.setLangStatus4(studentDetailDTO.getLangStatus4());
		studentDetail.setLangStatus5(studentDetailDTO.getLangStatus5());
		studentDetail.setWeddingDate(studentDetailDTO.getWeddingDate());

		if (studentDetailDTO.getLanguage1Id() != null) {
			GeneralDetail lang1 = new GeneralDetail();
			lang1.setGeneralDetailId(studentDetailDTO.getLanguage1Id());
			studentDetail.setLanguage1(lang1);
		}

		if (studentDetailDTO.getLanguage2Id() != null) {
			GeneralDetail lang2 = new GeneralDetail();
			lang2.setGeneralDetailId(studentDetailDTO.getLanguage2Id());
			studentDetail.setLanguage2(lang2);
		}

		if (studentDetailDTO.getLanguage3Id() != null) {
			GeneralDetail lang3 = new GeneralDetail();
			lang3.setGeneralDetailId(studentDetailDTO.getLanguage3Id());
			studentDetail.setLanguage3(lang3);
		}

		if (studentDetailDTO.getLanguage4Id() != null) {
			GeneralDetail lang4 = new GeneralDetail();
			lang4.setGeneralDetailId(studentDetailDTO.getLanguage4Id());
			studentDetail.setLanguage4(lang4);
		}

		if (studentDetailDTO.getLanguage5Id() != null) {
			GeneralDetail lang5 = new GeneralDetail();
			lang5.setGeneralDetailId(studentDetailDTO.getLanguage5Id());
			studentDetail.setLanguage5(lang5);
		}
		if (studentDetailDTO.getTitleId() != null) {
			GeneralDetail title = new GeneralDetail();
			title.setGeneralDetailId(studentDetailDTO.getTitleId());
			studentDetail.setTitle(title);
		}
		if (studentDetailDTO.getStudentStatusId() != null) {
			GeneralDetail studentStatus = new GeneralDetail();
			studentStatus.setGeneralDetailId(studentDetailDTO.getStudentStatusId());
			studentDetail.setStudentStatus(studentStatus);
		}
		if (studentDetailDTO.getUserId() != null) {
			User user = new User();
			user.setUserId(studentDetailDTO.getUserId());
			studentDetail.setUser(user);
		}
		studentDetail.setLastName(studentDetailDTO.getLastName());
		studentDetail.setMiddleName(studentDetailDTO.getMiddleName());
		studentDetail.setMobile(studentDetailDTO.getMobile());
		studentDetail.setMotherAddress(studentDetailDTO.getMotherAddress());
		studentDetail.setMotherEmailId(studentDetailDTO.getMotherEmailId());
		studentDetail.setMotherIncomePa(studentDetailDTO.getMotherIncomePa());
		studentDetail.setMotherMobileNo(studentDetailDTO.getMotherMobileNo());
		studentDetail.setMotherName(studentDetailDTO.getMotherName());
		studentDetail.setMotherOccupation(studentDetailDTO.getMotherOccupation());
		studentDetail.setMotherPhotoPath(FileUtil.getRelativePath(studentDetailDTO.getMotherPhotoPath()));
		studentDetail.setMotherQualification(studentDetailDTO.getMotherQualification());
		studentDetail.setPancardFilePath(FileUtil.getRelativePath(studentDetailDTO.getPancardFilePath()));
		studentDetail.setPancardNo(studentDetailDTO.getPancardNo());
		studentDetail.setPassportNo(studentDetailDTO.getPassportNo());
		studentDetail.setPermanentAddress(studentDetailDTO.getPermanentAddress());
		studentDetail.setPermanentPincode(studentDetailDTO.getPermanentPincode());
		studentDetail.setPermanentStreet(studentDetailDTO.getPermanentStreet());
		studentDetail.setPremanentMandal(studentDetailDTO.getPermanentMandal());

		if (studentDetailDTO.getPermanentCityId() != null) {
			City permanentCity = new City();
			permanentCity.setCityId(studentDetailDTO.getPermanentCityId());
			studentDetail.setPremenentCity(permanentCity);
			if (studentDetailDTO.getPermanentDistrictId() != null) {
				District district = new District();
				district.setDistrictId(studentDetailDTO.getPermanentDistrictId());
				district.setDistrictName(studentDetailDTO.getPermanentDistrictName());
				permanentCity.setDistrict(district);
				studentDetail.setPermanentDistrict(district);

				if (studentDetailDTO.getPermanentStateId() != null) {
					State state = new State();
					state.setStateId(studentDetailDTO.getPermanentStateId());
					state.setStateName(studentDetailDTO.getPermanentStateName());
					district.setState(state);

					if (studentDetailDTO.getPermanentCountryId() != null) {
						Country country = new Country();
						country.setCountryId(studentDetailDTO.getPermanentCountryId());
						country.setCountryName(studentDetailDTO.getPermanentCountryName());
						state.setCountry(country);
					}
				}
			}

		}

		if (studentDetailDTO.getPresentCityId() != null) {
			City presentCity = new City();
			presentCity.setCityId(studentDetailDTO.getPresentCityId());
			studentDetail.setPresentCity(presentCity);
			if (studentDetailDTO.getPresentDistrictId() != null) {
				District district = new District();
				district.setDistrictId(studentDetailDTO.getPresentDistrictId());
				district.setDistrictName(studentDetailDTO.getPresentDistrictName());
				presentCity.setDistrict(district);
				studentDetail.setPresentDistrict(district);

				if (studentDetailDTO.getPresentStateId() != null) {
					State state = new State();
					state.setStateId(studentDetailDTO.getPresentStateId());
					state.setStateName(studentDetailDTO.getPresentStateName());
					district.setState(state);

					if (studentDetailDTO.getPresentCountryId() != null) {
						Country country = new Country();
						country.setCountryId(studentDetailDTO.getPresentCountryId());
						country.setCountryName(studentDetailDTO.getPresentCountryName());
						state.setCountry(country);
					}
				}
			}
		}

		studentDetail.setPresentAddress(studentDetailDTO.getPresentAddress());
		studentDetail.setPresentMandal(studentDetailDTO.getPresentMandal());
		studentDetail.setPresentPincode(studentDetailDTO.getPresentPincode());
		studentDetail.setPresentStreet(studentDetailDTO.getPresentStreetName());
		studentDetail.setPrimaryContact(studentDetailDTO.getPrimaryContact());
		studentDetail.setReason(studentDetailDTO.getReason());
		studentDetail.setReceiptNo(studentDetailDTO.getReceiptNo());
		studentDetail.setRefApplicationNo(studentDetailDTO.getRefApplicationNo());
		studentDetail.setResidencePhone(studentDetailDTO.getResidencePhone());
		studentDetail.setRollNumber(studentDetailDTO.getRollNumber());
		studentDetail.setSscNo(studentDetailDTO.getSscNo());
		studentDetail.setStudentPhotoPath(FileUtil.getRelativePath(studentDetailDTO.getStudentPhotoPath()));

		studentDetail.setDateOfBirth(studentDetailDTO.getDateOfBirth());
		studentDetail.setStdEmailId(studentDetailDTO.getStdEmailId());
		studentDetail.setEntranceHtNo(studentDetailDTO.getEntranceHTNumber());
		if (studentDetailDTO.getCourseId() != null) {
			Course course = new Course();
			course.setCourseId(studentDetailDTO.getCourseId());
			studentDetail.setCourse(course);
		}
		if (studentDetailDTO.getSchoolId() != null) {
			School school = new School();
			school.setSchoolId(studentDetailDTO.getSchoolId());
			studentDetail.setSchool(school);
		}

		if (studentDetailDTO.getOrganizationId() != null) {
			Organization org = new Organization();
			org.setOrganizationId(studentDetailDTO.getOrganizationId());
			studentDetail.setOrganization(org);
		}

		if (studentDetailDTO.getCourseYearId() != null) {
			CourseYear courseYear = new CourseYear();
			courseYear.setCourseYearId(studentDetailDTO.getCourseYearId());
			studentDetail.setCourseYear(courseYear);
		}

		if (studentDetailDTO.getBatchId() != null) {
			Batch batch = new Batch();
			batch.setBatchId(studentDetailDTO.getBatchId());
			studentDetail.setBatch(batch);
		}
		if (studentDetailDTO.getCasteId() != null) {
			Caste caste = new Caste();
			caste.setCasteId(studentDetailDTO.getCasteId());
			studentDetail.setCaste(caste);
		}
		if (studentDetailDTO.getSubCasteId() != null) {
			SubCaste subCaste = new SubCaste();
			subCaste.setSubCasteId(studentDetailDTO.getSubCasteId());
			studentDetail.setSubCaste(subCaste);
		}


		if (studentDetailDTO.getDisabilityId() != null) {
			GeneralDetail disability = new GeneralDetail();
			disability.setGeneralDetailId(studentDetailDTO.getDisabilityId());
			studentDetail.setDisability(disability);
		}

		if (studentDetailDTO.getGenderId() != null) {
			GeneralDetail gender = new GeneralDetail();
			gender.setGeneralDetailId(studentDetailDTO.getGenderId());
			studentDetail.setGender(gender);
		}

		if (studentDetailDTO.getQuotaId() != null) {
			GeneralDetail quota = new GeneralDetail();
			quota.setGeneralDetailId(studentDetailDTO.getQuotaId());
			quota.setGeneralDetailDisplayName(studentDetailDTO.getQuotaDisplayName());
			quota.setGeneralDetailCode(studentDetailDTO.getQuotaCode());
			studentDetail.setQuota(quota);
		}
		if (studentDetailDTO.getReligionId() != null) {
			GeneralDetail religion = new GeneralDetail();
			religion.setGeneralDetailId(studentDetailDTO.getReligionId());
			studentDetail.setReligion(religion);
		}
		if (studentDetailDTO.getStudentTypeId() != null) {
			GeneralDetail studentType = new GeneralDetail();
			studentType.setGeneralDetailId(studentDetailDTO.getStudentTypeId());
			studentDetail.setStudentType(studentType);
		}

		if (studentDetailDTO.getBloodgroupId() != null) {
			GeneralDetail bloodGroup = new GeneralDetail();
			bloodGroup.setGeneralDetailId(studentDetailDTO.getBloodgroupId());
			studentDetail.setBloodgroup(bloodGroup);
		}
		if (studentDetailDTO.getNationalityId() != null) {
			GeneralDetail nationality = new GeneralDetail();
			nationality.setGeneralDetailId(studentDetailDTO.getNationalityId());
			studentDetail.setNationality(nationality);
		}

		if (studentDetailDTO.getGroupSectionId() != null) {
			GroupSection groupSection = new GroupSection();
			groupSection.setGroupSectionId(studentDetailDTO.getGroupSectionId());
			studentDetail.setGroupSection(groupSection);
		}
		if (studentDetailDTO.getAcademicYearId() != null) {
			AcademicYear academicYear = new AcademicYear();
			academicYear.setAcademicYearId(studentDetailDTO.getAcademicYearId());
			studentDetail.setAcademicYear(academicYear);
		}
		// Set the StudentDocumentCollection details
		if (!CollectionUtils.isEmpty(studentDetailDTO.getStudentDocumentCollections())) {
			List<StudentDocumentCollection> studentDocCollectionList = new ArrayList<>();
			for (StudentDocumentCollectionDTO studentDocumentCollectionDTO : studentDetailDTO
					.getStudentDocumentCollections()) {
				StudentDocumentCollection studentDocumentCollection = new StudentDocumentCollection();

				if (studentDocumentCollectionDTO.getStdDocCollId() == null) {
					studentDocumentCollection.setCreatedDt(new Date());
					studentDocumentCollection.setCreatedUser(SecurityUtil.getCurrentUser());
					studentDocumentCollection.setIsActive(Boolean.TRUE);
				} else {
					studentDocumentCollection.setStudentDocCollId(studentDocumentCollectionDTO.getStdDocCollId());
					studentDocumentCollection.setCreatedDt(studentDocumentCollectionDTO.getCreatedDt());
					studentDocumentCollection.setCreatedUser(studentDocumentCollectionDTO.getCreatedUser());
					studentDocumentCollection.setIsActive(studentDocumentCollectionDTO.getIsActive());
					studentDocumentCollection.setUpdatedDt(new Date());
					studentDocumentCollection.setUpdatedUser(SecurityUtil.getCurrentUser());
				}
				studentDocumentCollection.setFileName(studentDocumentCollectionDTO.getFileName());
				studentDocumentCollection
						.setFilePath(FileUtil.getRelativePath(studentDocumentCollectionDTO.getFilePath()));
				studentDocumentCollection.setIsHardCopy(studentDocumentCollectionDTO.getIsHardCopy());
				studentDocumentCollection.setIsSoftCopy(studentDocumentCollectionDTO.getIsSoftCopy());
				studentDocumentCollection.setIsOriginal(studentDocumentCollectionDTO.getIsOriginal());
				studentDocumentCollection.setIsVerified(studentDocumentCollectionDTO.getIsVerified());
				studentDocumentCollection.setRackNumber(studentDocumentCollectionDTO.getRackNumber());
				studentDocumentCollection.setReason(studentDocumentCollectionDTO.getReason());

				if (studentDocumentCollectionDTO.getDocRepId() != null) {
					DocumentRepository documentRepository = new DocumentRepository();
					documentRepository.setDocumentRepositoryId(studentDocumentCollectionDTO.getDocRepId());
					studentDocumentCollection.setDocumentRepository(documentRepository);
				}

				if (studentDocumentCollectionDTO.getAppDocCollId() != null) {
					StudentAppDocCollection studentAppDocCollection = new StudentAppDocCollection();
					studentAppDocCollection.setAppDocCollId(studentDocumentCollectionDTO.getAppDocCollId());
					studentDocumentCollection.setStudentAppDocCollection(studentAppDocCollection);
				}

				if (studentDocumentCollectionDTO.getVerifiedByEmpId() != null) {
					EmployeeDetail verifiedbyEmployeeDetail = new EmployeeDetail();
					verifiedbyEmployeeDetail.setEmployeeId(studentDocumentCollectionDTO.getVerifiedByEmpId());
					studentDocumentCollection.setVerifiedbyEmployeeDetail(verifiedbyEmployeeDetail);
				}

				if (studentDocumentCollectionDTO.getStdDocCollId() == null) {
					studentDocumentCollection.setStudentDetail(studentDetail);
				} else {
					StudentDetail eduStudentDetail = new StudentDetail();
					eduStudentDetail.setStudentId(studentDetail.getStudentId());
					studentDocumentCollection.setStudentDetail(studentDetail);
				}

				studentDocCollectionList.add(studentDocumentCollection);
			}
			if (!CollectionUtils.isEmpty(studentDocCollectionList)) {
				studentDetail.setStdDocumentCollections(studentDocCollectionList);
			}
		}

		// Set the StudentActivitiesDetails details
		if (!CollectionUtils.isEmpty(studentDetailDTO.getStudentActivitiesDetails())) {
			List<StudentActivitiesDetail> studentActivitiesDetailsList = new ArrayList<>();
			for (StudentActivitiesDetailDTO studentActivitiesDetailDTO : studentDetailDTO
					.getStudentActivitiesDetails()) {
				StudentActivitiesDetail studentActivitiesDetail = new StudentActivitiesDetail();

				if (studentActivitiesDetailDTO.getStudentActivityId() == null) {
					studentActivitiesDetail.setCreatedDt(new Date());
					studentActivitiesDetail.setCreatedUser(SecurityUtil.getCurrentUser());
					studentActivitiesDetail.setIsActive(Boolean.TRUE);
				} else {
					studentActivitiesDetail.setStudentActivityId(studentActivitiesDetailDTO.getStudentActivityId());
					studentActivitiesDetail.setCreatedDt(studentActivitiesDetailDTO.getCreatedDt());
					studentActivitiesDetail.setCreatedUser(studentActivitiesDetailDTO.getCreatedUser());
					studentActivitiesDetail.setIsActive(studentActivitiesDetailDTO.getIsActive());
					studentActivitiesDetail.setUpdatedDt(new Date());
					studentActivitiesDetail.setUpdatedUser(SecurityUtil.getCurrentUser());
				}

				studentActivitiesDetail.setStudentActivityId(studentActivitiesDetailDTO.getStudentActivityId());
				studentActivitiesDetail.setLevel(studentActivitiesDetailDTO.getLevel());
				studentActivitiesDetail.setParticulars(studentActivitiesDetailDTO.getParticulars());
				studentActivitiesDetail.setSponsoredBy(studentActivitiesDetailDTO.getSponsoredBy());
				studentActivitiesDetail.setReason(studentActivitiesDetailDTO.getReason());

				if (studentActivitiesDetailDTO.getStudentActivityId() == null) {
					studentActivitiesDetail.setStudentDetail(studentDetail);
				} else {
					StudentDetail actStudentDetail = new StudentDetail();
					actStudentDetail.setStudentId(studentDetail.getStudentId());
					studentActivitiesDetail.setStudentDetail(actStudentDetail);
				}

				studentActivitiesDetailsList.add(studentActivitiesDetail);
			}
			if (!CollectionUtils.isEmpty(studentActivitiesDetailsList)) {
				studentDetail.setActivitiesDetails(studentActivitiesDetailsList);
			}
		}

		// Set the StudentEducations details
		if (!CollectionUtils.isEmpty(studentDetailDTO.getStudentEducationDetails())) {
			List<StudentEducationDetail> studentEducationDetailList = new ArrayList<>();
			for (StudentEducationDetailDTO studentEducationDetailDTO : studentDetailDTO.getStudentEducationDetails()) {
				StudentEducationDetail studentEducationDetail = new StudentEducationDetail();

				if (studentEducationDetailDTO.getStudentEducationId() == null) {
					studentEducationDetail.setCreatedDt(new Date());
					studentEducationDetail.setCreatedUser(SecurityUtil.getCurrentUser());
					studentEducationDetail.setIsActive(Boolean.TRUE);
				} else {
					studentEducationDetail.setStudentEducationId(studentEducationDetailDTO.getStudentEducationId());
					studentEducationDetail.setCreatedDt(studentEducationDetailDTO.getCreatedDt());
					studentEducationDetail.setCreatedUser(studentEducationDetailDTO.getCreatedUser());
					studentEducationDetail.setIsActive(studentEducationDetailDTO.getIsActive());
					studentEducationDetail.setUpdatedDt(new Date());
					studentEducationDetail.setUpdatedUser(SecurityUtil.getCurrentUser());
				}

				studentEducationDetail.setAddress(studentEducationDetailDTO.getAddress());
				studentEducationDetail.setBoard(studentEducationDetailDTO.getBoard());
				studentEducationDetail.setGradeClassSecured(studentEducationDetailDTO.getGradeClassSecured());
				studentEducationDetail.setMajorSubjects(studentEducationDetailDTO.getMajorSubjects());
				studentEducationDetail.setMedium(studentEducationDetailDTO.getMedium());
				studentEducationDetail.setNameOfInstitution(studentEducationDetailDTO.getNameOfInstitution());
				studentEducationDetail.setPrecentage(studentEducationDetailDTO.getPrecentage());
				studentEducationDetail.setYearOfCompletion(studentEducationDetailDTO.getYearOfCompletion());
				studentEducationDetail.setReason(studentEducationDetailDTO.getReason());

				if (studentEducationDetailDTO.getGeneralDetailId() != null) {
					GeneralDetail gd = new GeneralDetail();
					gd.setGeneralDetailId(studentEducationDetailDTO.getGeneralDetailId());
					studentEducationDetail.setTMGeneralDetail(gd);
				}

				if (studentEducationDetailDTO.getAppEducationId() != null) {
					StudentAppEducation studentAppEducation = new StudentAppEducation();
					studentAppEducation.setAppEducationId(studentEducationDetailDTO.getAppEducationId());
					studentEducationDetail.setStudentAppEducation(studentAppEducation);
				}

				if (studentEducationDetailDTO.getStudentEducationId() == null) {
					studentEducationDetail.setStudentDetail(studentDetail);
				} else {
					StudentDetail eduStudentDetail = new StudentDetail();
					eduStudentDetail.setStudentId(studentDetail.getStudentId());
					studentEducationDetail.setStudentDetail(eduStudentDetail);
				}

				studentEducationDetailList.add(studentEducationDetail);
			}
			if (!CollectionUtils.isEmpty(studentEducationDetailList)) {
				studentDetail.setStdEducationDetails(studentEducationDetailList);
			}
		}

		return studentDetail;
	}
}
