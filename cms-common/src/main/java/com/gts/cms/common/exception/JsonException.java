package com.gts.cms.common.exception;
/**
 * created by sathish on 02/09/2018.
 */
public class JsonException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new json exception.
	 */
	public JsonException() {
		super();
	}

	/**
	 * Instantiates a new json exception.
	 *
	 * @param msg the msg
	 */
	public JsonException(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new json exception.
	 *
	 * @param t the t
	 */
	public JsonException(Throwable t) {
		super(t);
	}
}
