package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.EmployeeListDTO;
import com.gts.cms.common.util.FileUtil;
import com.gts.cms.common.util.OptionalUtil;
import com.gts.cms.entity.EmployeeDetail;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EmployeeListMapper implements BaseMapper<EmployeeDetail, EmployeeListDTO> {

	public EmployeeListDTO convertEntityToDTO(EmployeeDetail empDetail) {
		EmployeeListDTO empDetailDTO = null;
		if (empDetail != null) {
			empDetailDTO = new EmployeeListDTO();
			empDetailDTO.setEmployeeId(empDetail.getEmployeeId());
			empDetailDTO.setEmail(empDetail.getEmail());
			empDetailDTO.setEmpNumber(empDetail.getEmpNumber());
			empDetailDTO.setFirstName(empDetail.getFirstName());
			// empDetailDTO.setReportingManagerName(empDetail.getEmpEmployeeReportings2().ge);
			empDetailDTO.setLastName(empDetail.getLastName());
			empDetailDTO.setMiddleName(empDetail.getMiddleName());
			empDetailDTO.setPhotoPath(FileUtil.getAbsolutePath(empDetail.getPhotoPath()));
			empDetailDTO.setMobile(empDetail.getMobile());
			empDetailDTO.setDateOfBirth(empDetail.getDateOfBirth());
			empDetailDTO.setDateOfRelieving(empDetail.getDateOfRelieving());
			empDetailDTO.setIsActive(empDetail.getIsActive());
			if (empDetail.getEmployeeCategory() != null) {
				empDetailDTO.setEmpCategoryId(empDetail.getEmployeeCategory().getGeneralDetailId());
			}
			if(empDetail.getEmployeeWorkingDepartment()!=null) {
				empDetailDTO.setEmpWorkingDeptId(empDetail.getEmployeeWorkingDepartment().getDepartmentId());
			}
			empDetailDTO.setJoiningDate(empDetail.getJoiningDate());
			if(empDetail.getOrganization()!=null) {
				empDetailDTO.setOrganizationId(empDetail.getOrganization().getOrganizationId());
			}
			if(empDetail.getQualification()!=null) {
				empDetailDTO.setQualificationId(empDetail.getQualification().getQualificationId());
			}
			
			if(empDetail.getWorkingDesignation()!=null) {
				empDetailDTO.setWorkingDesignationId(empDetail.getWorkingDesignation().getDesignationId());
			}
			if(empDetail.getEmployeeWorkCategory()!=null) {
				empDetailDTO.setEmpWorkingDeptId(empDetail.getEmployeeWorkCategory().getGeneralDetailId());
			}
			
			empDetailDTO.setEmpDeptId(
					OptionalUtil.resolve(() -> empDetail.getEmployeeDepartment().getDepartmentId()).orElse(null));
			if (empDetail.getEmployeeDepartment() != null) {
				empDetailDTO.setEmpDeptName(empDetail.getEmployeeDepartment().getDeptName());
			}

			empDetailDTO.setDesignationId(
					OptionalUtil.resolve(() -> empDetail.getDesignation().getDesignationId()).orElse(null));

			if (empDetail.getDesignation() != null) {
				empDetailDTO.setDesignation(empDetail.getDesignation().getDesignationName());
			}
			empDetailDTO
					.setGenderId(OptionalUtil.resolve(() -> empDetail.getGender().getGeneralDetailId()).orElse(null));
			if (empDetail.getGender() != null) {
				empDetailDTO.setGender(empDetail.getGender().getGeneralDetailDisplayName());
			}
			
			if (empDetail.getUser() != null) {
				empDetailDTO.setUserId(empDetail.getUser().getUserId());
				empDetailDTO.setUserName(empDetail.getUser().getUserName());
				
			}
			
			if(empDetail.getSchool()!=null) {
				empDetailDTO.setSchoolId(empDetail.getSchool().getSchoolId());
				empDetailDTO.setSchoolName(empDetail.getSchool().getSchoolName());
				empDetailDTO.setSchoolCode(empDetail.getSchool().getSchoolCode());
				
				
			}
			
			if(empDetail.getReportingManager()!=null) {
				empDetailDTO.setReportingManagerId(empDetail.getReportingManager().getEmployeeId());
				empDetailDTO.setReportingManagerName(empDetail.getReportingManager().getFirstName());
			}
			if(empDetail.getEmployeeStatus()!=null) {
				empDetailDTO.setEmpStatusId(empDetail.getEmployeeStatus().getGeneralDetailId());
				empDetailDTO.setEmpStatus(empDetail.getEmployeeStatus().getGeneralDetailCode());
			}
			if(empDetail.getEmployeeState()!=null) {
				empDetailDTO.setEmpStateId(empDetail.getEmployeeState().getGeneralDetailId());
				empDetailDTO.setEmpState(empDetail.getEmployeeState().getGeneralDetailCode());
			}
			
			
			empDetailDTO.setReason(empDetail.getReason());
		}
		return empDetailDTO;
	}


	public List<EmployeeListDTO> convertEntityListToDTOList(List<EmployeeDetail> empDetailList) {
		List<EmployeeListDTO> empDetailDTOList = new ArrayList<>();
		empDetailList.forEach(empDetail -> empDetailDTOList.add(convertEntityToDTO(empDetail)));
		return empDetailDTOList;
	}

	@Override
	public List<EmployeeDetail> convertDTOListToEntityList(List<EmployeeListDTO> dtoList) {
		return null;
	}

	@Override
	public EmployeeDetail convertDTOtoEntity(EmployeeListDTO d, EmployeeDetail e) {
		return null;
	}

}
