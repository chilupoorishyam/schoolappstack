package com.gts.cms.common.service;

/**
 * created by sathish on 04/09/2018
 */
public interface CountryService {
	
/*
*//**
 * Gets the country details.
 *
 * @param countryId the country id
 * @return the country details
 *//*
public ApiResponse<CountryDTO> getCountryDetails(Long countryId);


*//**
 * Gets the all countries.
 *
 * @return the all countries
 *//*
public ApiResponse<List<CountryDTO>> getAllCountries();

*//**
 * Adds the country.
 *
 * @param country the country
 * @return the api response
 *//*
public ApiResponse<Long> addCountry(CountryDTO country);

*//**
 * Update the country.
 *
 * @param country the country
 * @return the api response
 *//*
public ApiResponse<Long> updateCountry(CountryDTO country);

*//**
 * Delete the country.
 *
 * @param country the country
 * @return the api response
 *//*
ApiResponse<Long> deleteCountryById(Long countryId);*/
}

