package com.gts.cms.common.service;

import java.util.Date;

import com.gts.cms.common.dto.ApiResponse;

public interface AuthorizationService {

	default ApiResponse<?> getAuthDetails(Date loginTime, Date logoutTime, Long userId, Boolean isActive, Integer page, Integer size) {
		return null;

	}

}
