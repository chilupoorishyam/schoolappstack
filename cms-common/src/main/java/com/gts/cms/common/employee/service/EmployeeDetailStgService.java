package com.gts.cms.common.employee.service;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.employee.dto.EmployeeDetailDTO;
import com.gts.cms.common.employee.dto.EmployeeDetailStgDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface EmployeeDetailStgService {
    ApiResponse<List<EmployeeDetailStgDTO>> uploadExcel(EmployeeDetailStgDTO employeeDetailStgDTO);

    ApiResponse<List<EmployeeDetailStgDTO>> getStgEmployeeDetails();

    ApiResponse<List<EmployeeDetailDTO>> processStgEmployeeDetails();
}
