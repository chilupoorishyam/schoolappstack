package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.StateRepositoryCustom;
import com.gts.cms.entity.State;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * created by Naveen(Auto) on 02/09/2018
 * 
 */
@Repository
public interface StateRepository extends JpaRepository<State, Long>, StateRepositoryCustom {

}
