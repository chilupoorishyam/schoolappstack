package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.DesignationRepositoryCustom;
import com.gts.cms.entity.Designation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * created by Naveen(Auto) on 02/09/2018
 * 
 */
@Repository
public interface DesignationRepository extends JpaRepository<Designation, Long>, DesignationRepositoryCustom {

    Optional<Designation> findByDesignationName(String designationName);
}
