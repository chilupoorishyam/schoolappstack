package com.gts.cms.common.dto;

import java.io.Serializable;

public class SmsLoginDetailDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long userId;
	private Long organizationId;
	private Long schoolId;
	private Long roleId;
	private Long userTypeId;
	private Long academicYearId;
	private Boolean messageStatus;

	public Long getUserId() {
		return userId;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public Long getUserTypeId() {
		return userTypeId;
	}

	public Long getAcademicYearId() {
		return academicYearId;
	}

	public Boolean getMessageStatus() {
		return messageStatus;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public void setUserTypeId(Long userTypeId) {
		this.userTypeId = userTypeId;
	}

	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}

	public void setMessageStatus(Boolean messageStatus) {
		this.messageStatus = messageStatus;
	}
}
