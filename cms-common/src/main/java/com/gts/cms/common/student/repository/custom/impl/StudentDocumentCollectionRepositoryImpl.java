package com.gts.cms.common.student.repository.custom.impl;

import com.gts.cms.common.student.repository.custom.StudentDocumentCollectionRepositoryCustom;
import com.gts.cms.entity.StudentDocumentCollection;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static com.gts.cms.entity.QStudentDocumentCollection.studentDocumentCollection;
@Repository
public  class StudentDocumentCollectionRepositoryImpl extends QuerydslRepositorySupport implements StudentDocumentCollectionRepositoryCustom{
	public StudentDocumentCollectionRepositoryImpl() {
		super(StudentDocumentCollectionRepositoryImpl.class);
	}
	@PersistenceContext
	private EntityManager em;
	@Override
	public Long updateStudentDocumentFiles(Long studentId, String docRepId, String docPath) {
		Long id = Long.parseLong(docRepId);
		return update(studentDocumentCollection)
				.where(studentDocumentCollection.studentDetail.studentId.eq(studentId)
						.and(studentDocumentCollection.studentDocCollId.eq(id)))
				.set(studentDocumentCollection.filePath, docPath).execute();
	}

	public List<StudentDocumentCollection> findDocCollectionByStdId(Long studentId) {
		JPQLQuery query = from(studentDocumentCollection);
		query.where(studentDocumentCollection.studentDetail.studentId.eq(studentId));
		return query.fetch();
	}

	
}
