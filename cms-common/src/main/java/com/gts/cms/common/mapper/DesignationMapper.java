package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.DesignationDTO;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.Designation;
import com.gts.cms.entity.Organization;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class DesignationMapper implements BaseMapper<Designation, DesignationDTO> {

	@Override
	public DesignationDTO convertEntityToDTO(Designation designation) {
		DesignationDTO designationDTO = null;

		if (designation != null) {
			designationDTO = new DesignationDTO();
			designationDTO.setDesignationId(designation.getDesignationId());
			designationDTO.setDesignationName(designation.getDesignationName());

			if (designation.getOrganization() != null) {
				designationDTO.setOrganizationId(designation.getOrganization().getOrganizationId());
				designationDTO.setOrgName(designation.getOrganization().getOrgName());
				designationDTO.setOrgCode(designation.getOrganization().getOrgCode());
			}

			designationDTO.setDesignationName(designation.getDesignationName());
			designationDTO.setReason(designation.getReason());
			designationDTO.setIsActive(designation.getIsActive());
		}

		return designationDTO;
	}

	@Override
	public List<DesignationDTO> convertEntityListToDTOList(List<Designation> designationList) {
		List<DesignationDTO> designationDTOList = new ArrayList<>();
		designationList.forEach(designation -> designationDTOList.add(convertEntityToDTO(designation)));
		return designationDTOList;
	}

	@Override
	public Designation convertDTOtoEntity(DesignationDTO designationDTO, Designation designation) {
		if (null == designation) {
			designation = new Designation();
			designation.setIsActive(Status.ACTIVE.getId());
			designation.setCreatedDt(new Date());
			designation.setCreatedUser(SecurityUtil.getCurrentUser());
		}
		else {
			designation.setIsActive(designationDTO.getIsActive());
		}
		
		designation.setDesignationId(designation.getDesignationId());

		if (designationDTO.getOrganizationId() != null) {
			Organization organization = new Organization();
			organization.setOrganizationId(designationDTO.getOrganizationId());
			organization.setOrgName(designationDTO.getOrgName());
			designation.setOrganization(organization);
		}

		designation.setDesignationName(designationDTO.getDesignationName());

		designation.setUpdatedDt(new Date());
		designation.setUpdatedUser(SecurityUtil.getCurrentUser());
		designation.setReason(designationDTO.getReason());
		return designation;
	}

	@Override
	public List<Designation> convertDTOListToEntityList(List<DesignationDTO> designationDTOList) {
		List<Designation> designationList = new ArrayList<>();
		designationDTOList.forEach(designationDTO -> designationList.add(convertDTOtoEntity(designationDTO, null)));
		return designationList;
	}
}
