package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.SubjectRepositoryCustom;
import com.gts.cms.entity.QSubject;
import com.gts.cms.entity.Subject;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQuery;
import org.apache.log4j.Logger;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SubjectRepositoryCustomImpl extends QuerydslRepositorySupport implements SubjectRepositoryCustom {
	private static final Logger LOGGER = Logger.getLogger(SubjectRepositoryCustomImpl.class);
	public SubjectRepositoryCustomImpl() {
		super(Subject.class);
	}

	/*@Override
	public List<Subject> getSubjects(Long schoolId, Long courseId) {
		JPQLQuery<Subject> query = from(subject)
				.where(subject.school.schoolId.eq(schoolId)
						.and(subject.course.courseId.eq(courseId)));
		List<Subject> subjectList = query.fetch();
		return subjectList;
	}*/

	@Override
	public List<Subject> getsubjects(Boolean isActive, Long schoolId, String subjectTypeCode) {
		JPQLQuery<Subject> query = from(QSubject.subject);
		if (isActive != null) {
			query.where(QSubject.subject.isActive.eq(isActive));

		} else {
			query.where(QSubject.subject.isActive.eq(Boolean.TRUE));
		}
		if(schoolId != null) {
			query.where(QSubject.subject.school.schoolId.eq(schoolId));
		}
		if(subjectTypeCode != null) {
			query.where(QSubject.subject.subjecttype.generalDetailCode.eq(subjectTypeCode));
		}
		LOGGER.info("query  "+query);
		LOGGER.info("RESULT COUNT"+query.fetchCount());
		return query.fetch();
	}

	@Override
	public List<Subject> searchSubjectDetails(String subjectName, String subjectCode, String shortName,Long schoolId,Long courseId) {
		JPQLQuery<Subject> jpqlQuery = from(QSubject.subject);

		BooleanBuilder builder = new BooleanBuilder();
		if (subjectName != null) {
			builder.or(QSubject.subject.subjectName.like("%"+ subjectName + "%"));
		}
		if (subjectCode != null) {
			builder.or(QSubject.subject.subjectCode.like("%" + subjectCode + "%"));
		}
		if (shortName != null) {
			builder.or(QSubject.subject.subjectName.like("%" + shortName + "%"));
		}
		if(schoolId != null) {
			builder.and(QSubject.subject.school.schoolId.eq(schoolId));
		}
		if(courseId!=null)
		{
			builder.and(QSubject.subject.course.courseId.eq(courseId));
		}
		
		builder.and(QSubject.subject.isActive.eq(Boolean.TRUE));
		
		
		jpqlQuery.where(builder);
		jpqlQuery.limit(50);
		return jpqlQuery.fetch();
	}
}
