package com.gts.cms.common.student.dto;

public class StudentAppDocumentCollectionDTO {
		private Long appDocCollId;

		private Long createdUser;
		private String fileName;

		private String filePath;

		private Boolean isActive;
		private Boolean isHardCopy;

		private Boolean isOriginal;

		private Boolean isSoftCopy;

		private Boolean isVerified;

		private String rackNumber;

		private String reason;

		private Long documentRepositoryId;
		private Long studentApplicationId;

	
		public Long getAppDocCollId() {
			return appDocCollId;
		}
		public void setAppDocCollId(Long appDocCollId) {
			this.appDocCollId = appDocCollId;
		}
		public Long getCreatedUser() {
			return createdUser;
		}
		public void setCreatedUser(Long createdUser) {
			this.createdUser = createdUser;
		}
		public String getFileName() {
			return fileName;
		}
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}
		public String getFilePath() {
			return filePath;
		}
		public void setFilePath(String filePath) {
			this.filePath = filePath;
		}
		public Boolean getIsActive() {
			return isActive;
		}
		public void setIsActive(Boolean isActive) {
			this.isActive = isActive;
		}
		public Boolean getIsHardCopy() {
			return isHardCopy;
		}
		public void setIsHardCopy(Boolean isHardCopy) {
			this.isHardCopy = isHardCopy;
		}
		public Boolean getIsOriginal() {
			return isOriginal;
		}
		public void setIsOriginal(Boolean isOriginal) {
			this.isOriginal = isOriginal;
		}
		public Boolean getIsSoftCopy() {
			return isSoftCopy;
		}
		public void setIsSoftCopy(Boolean isSoftCopy) {
			this.isSoftCopy = isSoftCopy;
		}
		public Boolean getIsVerified() {
			return isVerified;
		}
		public void setIsVerified(Boolean isVerified) {
			this.isVerified = isVerified;
		}
		public String getRackNumber() {
			return rackNumber;
		}
		public void setRackNumber(String rackNumber) {
			this.rackNumber = rackNumber;
		}
		public String getReason() {
			return reason;
		}
		public void setReason(String reason) {
			this.reason = reason;
		}
		public Long getDocumentRepositoryId() {
			return documentRepositoryId;
		}
		public void setDocumentRepositoryId(Long documentRepositoryId) {
			this.documentRepositoryId = documentRepositoryId;
		}
		public Long getStudentApplicationId() {
			return studentApplicationId;
		}
		public void setStudentApplicationId(Long studentApplicationId) {
			this.studentApplicationId = studentApplicationId;
		}
	

}
