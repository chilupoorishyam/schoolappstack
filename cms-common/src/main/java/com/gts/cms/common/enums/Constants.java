package com.gts.cms.common.enums;

public enum Constants {
	LEAVESTATUS_COMPLETE("LPSCOMPLETE"),
	LEAVESTATUS_CANCEL("LPSCANCEL"),
	LEAVESTATUS_RECOMMENDED("LPSRECOMMENDED"),
	LEAVESTATUS_APPROVED("LPSAPPROVED"),
	LEAVESTATUS_REJECTED("LPSREJECTED"),
	LEAVESTATUS_APPLIED("LPSAPPLIED"),
	STAFF_PROXY_INITIATED("INITIATED"),
	STAFF_PROXY_ACCEPTED("ACCEPTED"),
	STAFF_PROXY_REJECTED("REJECTED"),
	STAFF_PROXY_COMPLETED("COMPLETED");

	private String value;

	private Constants(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
