package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

public class PublisherDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long publisherId;
	private Date createdDt;
	private Long createdUser;
	private String date;
	@NotNull(message = "isActive is required")
	private Boolean isActive;
	@NotNull(message = "Publisher name is required")
	private String publishername;
	private String reason;
	private String shortName;
	private Date updatedDt;
	private Long updatedUser;
	@NotNull(message = "Library Id is required")
	private Long libraryId;
	private String libraryCode;
	private String libraryName;
	private String contactPersonName;
	private String address;
	private String emailId;
	private String phonenumber;
	private String mobile;
	
	public void setPublisherId(Long publisherId) {
		this.publisherId = publisherId;
	}

	public Long getPublisherId() {
		return publisherId;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDate() {
		return date;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setPublishername(String publishername) {
		this.publishername = publishername;
	}

	public String getPublishername() {
		return publishername;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReason() {
		return reason;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setLibraryId(Long libraryId) {
		this.libraryId = libraryId;
	}

	public Long getLibraryId() {
		return libraryId;
	}

	public void setLibraryCode(String libraryCode) {
		this.libraryCode = libraryCode;
	}

	public String getLibraryCode() {
		return libraryCode;
	}

	public void setLibraryName(String libraryName) {
		this.libraryName = libraryName;
	}

	public String getLibraryName() {
		return libraryName;
	}

	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
}