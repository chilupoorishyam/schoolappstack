package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

public class StudentMedicalCertificateDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long studentMedicalCertificateId;
	private String approvalComments;
	private Date approvalDate;
	private String certificateNo;
	private Date createdDt;
	private String doctorName;
	private String filepath1;
	private String filepath2;
	private String filepath3;
	private Date fromDate;
	private Boolean isActive;
	private String reason;
	private Date toDate;
	private Date updatedDt;
	private Long approvedByEmployeeId;
	@NotNull(message="Academic Year Id is required.")
	private Long academicYearId;
	
	private Long schoolId;
	private String schoolCode;
	private String schoolName;
	@NotNull(message="Course Year Id is required.")
	private Long courseYearId;
	private String courseYearCode;
	private String courseYearName;
	private Long approvalStatusId;
	private String approvalStatusDisplayName;
	private String approvalStatusCode;
	@NotNull(message="Group Section Id is required.")
	private Long groupSectionId;
	@NotNull(message="Student Id is required.")
	private Long studentId;
	private String studentFirstName;
	private String studentLastName;
	private String studentMiddleName;
	private String medicalReason;
	
	private Long createdUser;
	private Long updatedUser;
	
	public Long getStudentMedicalCertificateId() {
		return studentMedicalCertificateId;
	}
	public void setStudentMedicalCertificateId(Long studentMedicalCertificateId) {
		this.studentMedicalCertificateId = studentMedicalCertificateId;
	}
	public String getApprovalComments() {
		return approvalComments;
	}
	public void setApprovalComments(String approvalComments) {
		this.approvalComments = approvalComments;
	}
	public Date getApprovalDate() {
		return approvalDate;
	}
	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	public String getCertificateNo() {
		return certificateNo;
	}
	public void setCertificateNo(String certificateNo) {
		this.certificateNo = certificateNo;
	}
	public Date getCreatedDt() {
		return createdDt;
	}
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	public String getDoctorName() {
		return doctorName;
	}
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	public String getFilepath1() {
		return filepath1;
	}
	public void setFilepath1(String filepath1) {
		this.filepath1 = filepath1;
	}
	public String getFilepath2() {
		return filepath2;
	}
	public void setFilepath2(String filepath2) {
		this.filepath2 = filepath2;
	}
	public String getFilepath3() {
		return filepath3;
	}
	public void setFilepath3(String filepath3) {
		this.filepath3 = filepath3;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Date getUpdatedDt() {
		return updatedDt;
	}
	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}
	public Long getApprovedByEmployeeId() {
		return approvedByEmployeeId;
	}
	public void setApprovedByEmployeeId(Long approvedByEmployeeId) {
		this.approvedByEmployeeId = approvedByEmployeeId;
	}
	public Long getAcademicYearId() {
		return academicYearId;
	}
	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}
	public Long getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}
	public String getSchoolCode() {
		return schoolCode;
	}
	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public Long getCourseYearId() {
		return courseYearId;
	}
	public void setCourseYearId(Long courseYearId) {
		this.courseYearId = courseYearId;
	}
	public String getCourseYearCode() {
		return courseYearCode;
	}
	public void setCourseYearCode(String courseYearCode) {
		this.courseYearCode = courseYearCode;
	}
	public String getCourseYearName() {
		return courseYearName;
	}
	public void setCourseYearName(String courseYearName) {
		this.courseYearName = courseYearName;
	}
	public Long getApprovalStatusId() {
		return approvalStatusId;
	}
	public void setApprovalStatusId(Long approvalStatusId) {
		this.approvalStatusId = approvalStatusId;
	}
	public String getApprovalStatusDisplayName() {
		return approvalStatusDisplayName;
	}
	public void setApprovalStatusDisplayName(String approvalStatusDisplayName) {
		this.approvalStatusDisplayName = approvalStatusDisplayName;
	}
	public String getApprovalStatusCode() {
		return approvalStatusCode;
	}
	public void setApprovalStatusCode(String approvalStatusCode) {
		this.approvalStatusCode = approvalStatusCode;
	}
	public Long getGroupSectionId() {
		return groupSectionId;
	}
	public void setGroupSectionId(Long groupSectionId) {
		this.groupSectionId = groupSectionId;
	}
	public Long getStudentId() {
		return studentId;
	}
	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}
	public String getStudentFirstName() {
		return studentFirstName;
	}
	public void setStudentFirstName(String studentFirstName) {
		this.studentFirstName = studentFirstName;
	}
	public String getStudentLastName() {
		return studentLastName;
	}
	public void setStudentLastName(String studentLastName) {
		this.studentLastName = studentLastName;
	}
	public String getStudentMiddleName() {
		return studentMiddleName;
	}
	public void setStudentMiddleName(String studentMiddleName) {
		this.studentMiddleName = studentMiddleName;
	}
	public Long getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}
	public Long getUpdatedUser() {
		return updatedUser;
	}
	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}
	public String getMedicalReason() {
		return medicalReason;
	}
	public void setMedicalReason(String medicalReason) {
		this.medicalReason = medicalReason;
	}
	
	
}