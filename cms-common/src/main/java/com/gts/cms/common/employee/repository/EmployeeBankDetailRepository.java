package com.gts.cms.common.employee.repository;

import com.gts.cms.common.employee.repository.custom.EmployeeBankDetailRepositoryCustom;
import com.gts.cms.entity.EmployeeBankDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Genesis
 *
 */
@Repository
public interface EmployeeBankDetailRepository extends JpaRepository<EmployeeBankDetail, Long>, EmployeeBankDetailRepositoryCustom{

}
