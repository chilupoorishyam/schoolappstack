package com.gts.cms.common.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class SmsMessageDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@NotNull(message="Mobile Number is required")
	private String mobileNumber;
	private String senderId;
	@NotNull(message="Message is required")
	private String message;
	
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getSenderId() {
		return senderId;
	}
	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	

}
