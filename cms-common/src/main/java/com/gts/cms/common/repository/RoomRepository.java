package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.RoomRepositoryCustom;
import com.gts.cms.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * created by Naveen(Auto) on 02/09/2018
 * 
 */
@Repository
public interface RoomRepository extends JpaRepository<Room, Long>, RoomRepositoryCustom {


}
