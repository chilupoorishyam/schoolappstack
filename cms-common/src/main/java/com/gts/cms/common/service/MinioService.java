package com.gts.cms.common.service;

import com.gts.cms.videozoomapi.config.AppProperties;
import com.gts.cms.videozoomapi.util.tribute.request.PostObjectRequest;
import com.gts.cms.videozoomapi.util.tribute.response.GetObjectResponse;
import com.gts.cms.videozoomapi.util.tribute.response.PostObjectResponse;
import io.minio.*;
import io.minio.http.Method;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.apache.tika.Tika;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
public class MinioService {

    private final AppProperties appProperties;
    private final MinioClient minioClient;

    @SneakyThrows
    public GetObjectResponse uploadFile(String subject, MultipartFile file) {
        Tika tika = new Tika();
        String mime = tika.detect(file.getOriginalFilename());

        String bucket = appProperties.getStorage().getBucketRoot();
        String object = "video/" + subject + "/" + file.getOriginalFilename();

        String uri = appProperties.getStorage().getHost() + "/" + bucket + "/" + object;

        minioClient.putObject(
                PutObjectArgs
                        .builder()
                        .bucket(bucket)
                        .object(object)
                        .stream(file.getInputStream(), file.getSize(), -1)
                        .contentType(mime)
                        .build());

        return new GetObjectResponse(uri);
    }
    
    @SneakyThrows
    public GetObjectResponse uploadFile(String subject, String unit, MultipartFile file) {
        Tika tika = new Tika();
        String mime = tika.detect(file.getOriginalFilename());

        String bucket = appProperties.getStorage().getBucketRoot();
       // String object = "video/" + subject + "/" + file.getOriginalFilename();
        String object = "video/" + subject + "/" + unit + "/"  + file.getOriginalFilename();
        String uri = appProperties.getStorage().getHost() + "/" + bucket + "/" + object;

        minioClient.putObject(
                PutObjectArgs
                        .builder()
                        .bucket(bucket)
                        .object(object)
                        .stream(file.getInputStream(), file.getSize(), -1)
                        .contentType(mime)
                        .build());

        return new GetObjectResponse(uri);
    }
    
    @SneakyThrows
    public GetObjectResponse uploadFile(String subject, String unit, String topic, MultipartFile file) {
        Tika tika = new Tika();
        String mime = tika.detect(file.getOriginalFilename());

        String bucket = appProperties.getStorage().getBucketRoot();
       // String object = "video/" + subject + "/" + file.getOriginalFilename();
        String object = "video/" + subject + "/" + unit + "/" + topic + "/" + file.getOriginalFilename();
        String uri = appProperties.getStorage().getHost() + "/" + bucket + "/" + object;

        minioClient.putObject(
                PutObjectArgs
                        .builder()
                        .bucket(bucket)
                        .object(object)
                        .stream(file.getInputStream(), file.getSize(), -1)
                        .contentType(mime)
                        .build());

        return new GetObjectResponse(uri);
    }
    
	
    @SneakyThrows
    public String generateGetObjectRequest(String uri) {
        // dl-edu/video/Physics 101/some-video.mp4
        String object = generateObjectPathFromUri(uri);

        return minioClient.getPresignedObjectUrl(
                GetPresignedObjectUrlArgs.builder()
                        .method(Method.GET)
                        .bucket(appProperties.getStorage().getBucketRoot())
                        .object(object)
                        .expiry(1, TimeUnit.DAYS)
                        .build());
    }

    @SneakyThrows
    public void deleteObject(String uri) {
        // dl-edu/video/Physics 101/some-video.mp4
        String object = generateObjectPathFromUri(uri);

        minioClient.removeObject(
                RemoveObjectArgs
                        .builder()
                        .bucket(appProperties.getStorage().getBucketRoot())
                        .object(object)
                        .build());
    }

    @SneakyThrows
    public PostObjectResponse generatePutObjectRequest(PostObjectRequest request) {
        // dl-edu/video/Physics 101/some-video.mp4
        String bucket = appProperties.getStorage().getBucketRoot();
        String rootDirectory = "video";
        String object = rootDirectory + "/" + request.getSubject() + "/" + request.getFilename();
        PostPolicy policy = new PostPolicy(bucket, object, ZonedDateTime.now().plusDays(1));

        policy.setContentType("video/mp4");
        policy.setSuccessActionStatus(200);

        String uploadUri = appProperties.getStorage().getUploadUri();
        String staticUri = appProperties.getStorage().getHost() + "/" + bucket + "/" + object;
        Map<String, String> formData = minioClient.presignedPostPolicy(policy);

        return new PostObjectResponse(uploadUri, staticUri, formData);
    }

    private String generateObjectPathFromUri(String uri) {
        // dl-edu/video/Physics 101/some-video.mp4
        String host = appProperties.getStorage().getHost();
        String bucket = appProperties.getStorage().getBucketRoot();
        String root = host + "/" + bucket + "/";

        return StringUtils.difference(root, uri);
    }


}
