package com.gts.cms.common.repository;

import com.gts.cms.entity.AmsProfileDetail;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AmsProfileDetailRepository  extends  JpaRepository<AmsProfileDetail, Long>{

}
