package com.gts.cms.common.student.service.impl;

import com.gts.cms.common.dto.*;
import com.gts.cms.common.enums.ConfigAutoNumbersCodeEnum;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.enums.StudentDetailEnum;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.exception.BadRequestSearchException;
import com.gts.cms.common.mapper.StudentDetailListMapper;
import com.gts.cms.common.mapper.StudentDetailSearchMapper;
import com.gts.cms.common.repository.ConfigAutonumberRepository;
import com.gts.cms.common.repository.GeneralDetailRepository;
import com.gts.cms.common.service.FileService;
import com.gts.cms.common.student.dto.StudentDetailDTO;
import com.gts.cms.common.student.dto.StudentDetailFileUploadDTO;
import com.gts.cms.common.student.dto.StudentRollNumberDTO;
import com.gts.cms.common.student.mapper.StudentDetailMapper;
import com.gts.cms.common.student.repository.StudentAcademicBatchesRepository;
import com.gts.cms.common.student.repository.StudentDetailRepository;
import com.gts.cms.common.student.repository.StudentDocumentCollectionRepository;
import com.gts.cms.common.student.service.StudentDetailService;
import com.gts.cms.common.util.FileUtil;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.*;
import java.util.Map.Entry;

@Service
public class StudentDetailServiceImpl implements StudentDetailService {

    private static final Logger LOGGER = Logger.getLogger(StudentDetailServiceImpl.class);

    private static final String FILE_ORG = "org_";
    private static final String FILE_COLLEGE = "school_";
    private static final String FILE_APP = "stddetail_";

    private static final String FILE_AADHAR = "std_aadhar.";
    private static final String FILE_PAN = "std_pan.";
    private static final String FILE_PASSPORT = "passport.";
    private static final String FILE_PHOTO = "std_photo.";

    private static final String FILE_FATHER_PHOTO = "std_father_photo.";
    private static final String FILE_MOTHER_PHOTO = "std_mother_photo.";
    private static final String FILE_SPOUSE_PHOTO = "std_spouse_photo.";

    private static final String FILE_DOCUMENTS = "std_documents";

    private static final String fileSeparator = FileUtil.getFileSeparator();

    @Autowired
    StudentDetailRepository studentDetailRepository;

    @Autowired
    StudentDetailMapper studentDetailMapper;

    @Autowired
    StudentDetailSearchMapper studentDetailSearchMapper;

    @Autowired
    StudentDetailListMapper studentDetailListMapper;

    @Autowired
    ConfigAutonumberRepository configAutoNumberRepository;

    @Autowired
    FileService fileService;

    @Autowired
    StudentDocumentCollectionRepository studentDocumentCollectionRepository;

	/*@Autowired
	FeeStructureRepository feeStructureRepository;*/

    @Autowired
    GeneralDetailRepository generalDetailRepository;

	/*@Autowired
	FeeStudentDataService feeStudentDataService;*/

    @Autowired
    StudentAcademicBatchesRepository studentAcademicBatchesRepository;

	/*@Autowired
	FeeStudentDataMapper feeStudentDataMapper;*/

    /*@Autowired
    FeeStudentDataRepository feeStudentDataRepository;*/

	/*@Autowired
	FeeStudentDataParticularMapper feeStudentDataParticularMapper;

	@Autowired
	FeeStudentDataParticularRepository feeStudentDataParticularRepository;*/

    /*@Autowired
    BatchwiseStudentRepository batchwiseStudentRepository;

    @Autowired
    StudentAttendanceRepository studentAttendanceRepository;
    @Autowired
    StudentAttendanceMapper studentAttendanceMapper;*/


    @Override
    @Transactional(readOnly = true)
    public ApiResponse<List<StudentDetailListDTO>> getStudentsList(Boolean status, Long schoolId, Long courseId,
                                                                    Long courseYearId, Long groupSectionId, Long academicYearId, String statusCode) {
        LOGGER.info("StudentDetailServiceImpl.getStudentsList");
        ApiResponse<List<StudentDetailListDTO>> response = null;
        try {
            //String generalDetailCode = StudentDetailEnum.DTND.getValue();
            if (statusCode == null) {
                statusCode = StudentDetailEnum.INCOLLEGE.getValue();
            }
            if (status == null) {
                status = Boolean.TRUE;
            }
            List<StudentDetail> studentDetailsList = studentDetailRepository.findStudentList(status, schoolId,
                    courseId, courseYearId, groupSectionId, academicYearId, statusCode);

            if (!CollectionUtils.isEmpty(studentDetailsList)) {
                List<StudentDetailListDTO> studentDetailDTOs = studentDetailListMapper
                        .convertEntityListToDTOList(studentDetailsList);
                response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), studentDetailDTOs);
            } else {
                response = new ApiResponse<>(Boolean.FALSE, Messages.NO_STUDENTS.getValue(),
                        HttpStatus.OK.value());
            }
        } catch (Exception e) {
            LOGGER.error("StudentDetailServiceImpl.getStudentsList" + e);
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }

    @Override
    public ApiResponse<List<StudentDetailListDTO>> getStudentsListByStructure(Boolean status, Long schoolId,
                                                                              Long academicYearId, Long feeStructureId) {
        ApiResponse<List<StudentDetailListDTO>> response = null;
        try {
            List<StudentDetail> studentDetailsList = studentDetailRepository.findStudentListByStructure(status,
                    schoolId, academicYearId, feeStructureId);

            if (!CollectionUtils.isEmpty(studentDetailsList)) {
                List<StudentDetailListDTO> studentDetailDTOs = studentDetailListMapper
                        .convertEntityListToDTOList(studentDetailsList);
                response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), studentDetailDTOs);
            } else {
                response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
                        HttpStatus.OK.value());
            }
        } catch (Exception e) {
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }

    @Override
    @Transactional
    public ApiResponse<StudentDetailDTO> getStudentDetails(Long studentId, Long userId) {
        ApiResponse<StudentDetailDTO> response = null;
        try {
            StudentDetail studentDetail = studentDetailRepository.getOneByIds(studentId, userId);

            if (!StringUtils.isEmpty(studentDetail)) {
                StudentDetailDTO studentDetailDTO = studentDetailMapper
                        .convertStudentDetailToStudentDetailDTO(studentDetail);
                response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), studentDetailDTO);
            } else {
                response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
                        HttpStatus.OK.value());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }
        return response;
    }

    @Override
    @Transactional
    public ApiResponse<List<StudentDetailDTO>> getParentDetails(Long parentId) {
        ApiResponse<List<StudentDetailDTO>> response = null;
        try {
            List<StudentDetail> studentDetail = studentDetailRepository.findByParentId(parentId);

            if (!CollectionUtils.isEmpty(studentDetail)) {
                List<StudentDetailDTO> studentDetailDTO = studentDetailMapper.convertStudentDetailToStudentDetailDTO(studentDetail);

                response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), studentDetailDTO);
            } else {
                response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
                        HttpStatus.OK.value());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }
        return response;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public ApiResponse<?> updateStudentsList(List<StudentDetailListDTO> studentApplicationDTOs) {
        LOGGER.info("StudentDetailServiceImpl.updateStudentsList() -- execution started");
        ApiResponse<?> response = null;
        List<StudentAttendance> studentAttendanceList = null;
        List<StudentAttendance> studentAttendanceListofList = new ArrayList<>();
        List<StudentDetailListDTO> studentDetailListDTOs = new ArrayList<StudentDetailListDTO>();
        try {
            if (studentApplicationDTOs != null) {
                for (StudentDetailListDTO studentDetailListDTOObj : studentApplicationDTOs) {
                    studentAttendanceList = null;//studentAttendanceRepository.findByAbsentList(studentDetailListDTOObj.getToDate(), studentDetailListDTOObj.getStudentId());
                    if (studentAttendanceList.isEmpty()) {
                        studentDetailListDTOs.add(studentDetailListDTOObj);
                    }
                    if (studentAttendanceList.size() > 0) {
                        studentAttendanceListofList.addAll(studentAttendanceList);
                    }
                }
            }
            List<StudentDetail> studentDetailList = studentDetailListMapper.convertDTOListToEntityList(studentDetailListDTOs);
            if (!studentDetailList.isEmpty()) {
                studentDetailRepository.updateRegulationAndGroupsection(studentDetailList);

                for (StudentDetailListDTO studentDetailListDTO : studentDetailListDTOs) {

                    //studentAttendanceList=studentAttendanceRepository.findByAbsentList(studentDetailListDTO.getToDate(),studentDetailListDTO.getStudentId());

                    StudentAcademicbatch studentAcademicbatch = studentAcademicBatchesRepository.getDetails(studentDetailListDTO.getSchoolId(),
                            studentDetailListDTO.getStudentId());
                    if (studentAcademicbatch != null) {
                        studentAcademicbatch.setIsActive(Boolean.TRUE);
                        Calendar calendar = Calendar.getInstance();
                        if (studentDetailListDTO.getToDate() != null) {
                            calendar.setTime(studentDetailListDTO.getToDate());
                        }
                        calendar.set(Calendar.HOUR, 23);
                        calendar.set(Calendar.MINUTE, 59);
                        calendar.set(Calendar.SECOND, 59);
                        calendar.set(Calendar.MILLISECOND, 40);
                        calendar.set(Calendar.HOUR_OF_DAY, 23);
                        calendar.add(Calendar.DATE, -1);
                        //Date dateTimee=calendar.getTime();

                        studentAcademicbatch.setToDate(calendar.getTime());
                        LOGGER.info("ToDate" + calendar.getTime());
                        //studentAcademicbatch.setToDate(new Date());
                        GroupSection groupSection = new GroupSection();
                        if (studentDetailListDTO.getGroupSectionId() != null) {
                            groupSection.setGroupSectionId(studentDetailListDTO.getGroupSectionId());
                            studentAcademicbatch.setToGroupSection(groupSection);
                        } else {
                            studentAcademicbatch.setToGroupSection(null);
                        }
                       /* studentAcademicbatch.setCourseGroup(studentAcademicbatch.getCourseGroup());
                        if (studentDetailListDTO.getRegulationId() != null) {
                            Regulation regulation = new Regulation();
                            regulation.setRegulationId(studentDetailListDTO.getRegulationId());
                            studentAcademicbatch.setRegulation(regulation);
                        }*/
                        studentAcademicbatch.setUpdatedDt(new Date());
                        studentAcademicbatch.setUpdatedUser(SecurityUtil.getCurrentUser());
                        studentAcademicBatchesRepository.save(studentAcademicbatch);
                        StudentAcademicbatch studentAcademicbatchNew = new StudentAcademicbatch();
							/*Long updateCount = studentAcademicBatchesRepository
									.updateSection(studentAcademicbatch.getStudentAcademicbatchId(), groupSectionId);*/


                        //studentAcademicbatchNew.setStudentAcademicbatchId(studentAcademicbatch.getStudentAcademicbatchId());
                        studentAcademicbatchNew.setSchool(studentAcademicbatch.getSchool());
                        studentAcademicbatchNew.setStudentDetail(studentAcademicbatch.getStudentDetail());
                        studentAcademicbatchNew.setCourse(studentAcademicbatch.getCourse());
                        studentAcademicbatchNew.setAcademicYear(studentAcademicbatch.getAcademicYear());

                        studentAcademicbatchNew.setStudentStatus(studentAcademicbatch.getStudentStatus());

                        studentAcademicbatchNew.setFromBatch(studentAcademicbatch.getFromBatch());
                        studentAcademicbatchNew.setFromCourseYear(studentAcademicbatch.getFromCourseYear());

                        if (studentDetailListDTO.getGroupSectionId() != null) {
                            studentAcademicbatchNew.setFromGroupSection(groupSection);
                        } else {
                            studentAcademicbatchNew.setFromGroupSection(null);
                        }

                        studentAcademicbatchNew.setToBatch(studentAcademicbatch.getToBatch());
                        studentAcademicbatchNew.setToCourseYear(studentAcademicbatch.getToCourseYear());
                        studentAcademicbatchNew.setToGroupSection(null);

                        studentAcademicbatchNew.setReason("Section update");
                        studentAcademicbatchNew.setIsActive(Boolean.TRUE);
                        //studentAcademicbatchNew.setFromDate(studentAcademicbatch.getFromDate());
                        Calendar calendarObj = Calendar.getInstance();
                        if (studentDetailListDTO.getToDate() != null) {
                            calendarObj.setTime(studentDetailListDTO.getToDate());
                        }
                        calendarObj.set(Calendar.MINUTE, 0);
                        calendarObj.set(Calendar.SECOND, 0);
                        calendarObj.set(Calendar.HOUR_OF_DAY, 0);
                        studentAcademicbatchNew.setFromDate(calendarObj.getTime());
                        studentAcademicbatchNew.setToDate(null);
                        studentAcademicbatchNew.setCreatedDt(new Date());
                        studentAcademicbatchNew.setCreatedUser(SecurityUtil.getCurrentUser());

                        studentAcademicBatchesRepository.save(studentAcademicbatchNew);
                        studentAcademicBatchesRepository.flush();
                    }


                }
                ///response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), studentAttendanceMapper.convertEntityListToDTOList(studentAttendanceListofList), HttpStatus.OK.value());
            } else {
                //response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), studentAttendanceMapper.convertEntityListToDTOList(studentAttendanceListofList), HttpStatus.OK.value());
            }
            LOGGER.info("StudentDetailServiceImpl.updateStudentsList() -- execution ended");
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }

    @Override
    @Transactional
    public ApiResponse<StudentDetailDTO> updateStudentDetail(StudentDetailDTO studentDetailDTO) {
        ApiResponse<StudentDetailDTO> response = null;
        LOGGER.info("StudentDetailServiceImpl.updateStudentDetail()-- execution started ");
        try {
            ConfigAutonumber configAutoNumber = null;
            Long currentNumber = null;
            if (!StringUtils.isEmpty(studentDetailDTO)) {
                StudentDetail studentDetail = null;
                studentDetail = studentDetailMapper.convertStudentDetailDTOToStudentDetail(studentDetailDTO,
                        studentDetail);

                if (studentDetail.getStudentId() == null) {
                    configAutoNumber = configAutoNumberRepository.findSchoolConfigAutoNumbers(
                            studentDetail.getSchool().getSchoolId(), ConfigAutoNumbersCodeEnum.STD_ADMISSION.getId());
                    if (configAutoNumber != null && configAutoNumber.getAutoconfigId() != null
                            && configAutoNumber.getPrefix() != null && configAutoNumber.getSuffix() != null
                            && configAutoNumber.getCurrentNumber() != null) {
                        currentNumber = Long.parseLong(configAutoNumber.getCurrentNumber()) + 1;
                        String admissionNumber = configAutoNumber.getPrefix() + currentNumber
                                + configAutoNumber.getSuffix();
                        configAutoNumber.setCurrentNumber(currentNumber.toString());
                        configAutoNumber.setUpdatedDt(new Date());
                        configAutoNumber.setUpdatedUser(SecurityUtil.getCurrentUser());
                        studentDetail.setAdminssionDate(new Date());
                        studentDetail.setAdmissionNumber(admissionNumber);
                    }
                }

                studentDetail = studentDetailRepository.save(studentDetail);
                studentDetailRepository.flush();
                //added student subjects

                //studentSubjectServiceImpl.setStudentSubjects(studentDetailList, subjectregulation);

                //add student to structure removed
			/*	if (studentDetailDTO.getStudentId() == null) {
					List<FeeStudentDataParticularDTO> feeStudentDataParticularDTOs = new ArrayList<>();
					List<FeeStudentDataDTO> feeStudentDataDTOs = new ArrayList<>();
					List<FeeStudentDataParticular> feeStudentDataParticular = null;
					List<FeeStudentData> feeStudentDatas = null;
					feeStudentDataService.saveAdmitStudentFeeDetail(studentDetail.getSchool().getSchoolId(),
							studentDetail.getAcademicYear().getAcademicYearId(),
							studentDetail.getCourseYear().getCourseYearId(), null,
							studentDetail.getQuota().getGeneralDetailId(), studentDetail.getStudentId(),
							feeStudentDataParticularDTOs, feeStudentDataDTOs);

					if (!CollectionUtils.isEmpty(feeStudentDataDTOs)) {
						feeStudentDatas = feeStudentDataMapper.convertDTOListToEntityList(feeStudentDataDTOs);

						feeStudentDatas = feeStudentDataRepository.saveAll(feeStudentDatas);
						feeStudentDataRepository.flush();
						if (feeStudentDatas.get(0).getFeeStructure() != null) {
							feeStructureRepository
									.setIsEditableFalse(feeStudentDatas.get(0).getFeeStructure().getFeeStructureId());
						}

					}
					if (!CollectionUtils.isEmpty(feeStudentDataParticularDTOs)) {
						feeStudentDataParticular = feeStudentDataParticularMapper
								.convertDTOListToEntityList(feeStudentDataParticularDTOs);

						feeStudentDataParticular = feeStudentDataParticularRepository.saveAll(feeStudentDataParticular);
						feeStudentDataParticularRepository.flush();
					}

				}*/

                if (configAutoNumber != null && currentNumber != null) {
                    try {
                        configAutoNumberRepository.updateCurrentNumber(configAutoNumber);
                    } catch (Exception e) {
                        LOGGER.error("Exception Occured while updating currentnumber in ConfigAutoNumber  : "
                                + e.getMessage());
                    }
                }
                studentDetailDTO = studentDetailMapper.convertStudentDetailToStudentDetailDTO(studentDetail);
                if (studentDetailDTO.getStudentId() == null) {

                    studentDetailDTO.setStudentId(studentDetail.getStudentId());
                    studentDetailDTO.setAdmissionNumber(studentDetail.getAdmissionNumber());
                    response = new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(), studentDetailDTO);
                } else {
                    response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), studentDetailDTO);
                }
            } else {
                response = new ApiResponse<>(Boolean.FALSE, Messages.EXCEPTION_MESSAGE.getValue(),
                        HttpStatus.OK.value());
            }
            LOGGER.info("StudentDetailServiceImpl.updateStudentDetail()-- execution ended ");
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Exception Occured while adding Employee : " + e);
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }
        return response;
    }

    @Override
    public ApiResponse<List<StudentDetailSearchDTO>> searchStudentDetails(String query, Long schoolId,
                                                                          Long academicYearId, Boolean status, Long courseId, Long courseYearId) {
        ApiResponse<List<StudentDetailSearchDTO>> response = null;
        if ((query == null) || (query != null && query.length() < 5)) {
            throw new BadRequestSearchException("Query String length cannot be lessthan 4 characters.");
        }
        try {
            String firstName = query, lastName = query, middleName = query, admissionNo = query, mobile = query;
            List<StudentDetail> studentDetailsList;
            /*
             * if (query.matches("[0-9]+")) { if (query.length() < 10) { throw new
             * BadRequestException("Mobile Number cannot be lessthan 10 characters."); }
             * studentDetailsList = studentDetailRepository.searchStudentDetails(null, null,
             * null, null, mobile,schoolId, academicYearId); } else if
             * (query.chars().allMatch(Character::isLetter)) { studentDetailsList =
             * studentDetailRepository.searchStudentDetails(firstName, lastName, middleName,
             * null, null,schoolId, academicYearId); } else { studentDetailsList =
             * studentDetailRepository.searchStudentDetails(null, null, null, admissionNo,
             * null,schoolId, academicYearId); }
             */
            if (query.matches("[0-9]+")) {
                /*
                 * if (query.length() < 10) { throw new
                 * BadRequestException("Mobile Number cannot be lessthan 10 characters."); }
                 */
                studentDetailsList = studentDetailRepository.searchStudentDetails(null, null, null, admissionNo, mobile,
                        schoolId, academicYearId, status, courseId, courseYearId);
            } else {
                studentDetailsList = studentDetailRepository.searchStudentDetails(firstName, lastName, middleName,
                        admissionNo, null, schoolId, academicYearId, status, courseId, courseYearId);
            } /*
             * else {
             *
             * check for lenght 17UK1A0501 if()) { }
             *
             * studentDetailsList = studentDetailRepository.searchStudentDetails(null, null,
             * null, admissionNo, null,schoolId, academicYearId); }
             */
            // List<StudentDetail> studentDetailsList =
            // studentDetailRepository.searchStudentDetails(query);
            if (!CollectionUtils.isEmpty(studentDetailsList)) {
                List<StudentDetailSearchDTO> studentDetailDTOs = studentDetailSearchMapper
                        .convertEntityListToDTOList(studentDetailsList);
                response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), studentDetailDTOs);
            } else {
                response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
                        HttpStatus.OK.value());
            }
        } catch (Exception e) {
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }

    @Override
    @Transactional
    public ApiResponse<StudentDetailFileUploadDTO> uploadStudentDetailUploadFiles(
            StudentDetailFileUploadDTO studentDetailFileUploadDTO) {
        ApiResponse<StudentDetailFileUploadDTO> response = null;
        LOGGER.info("StudentDetailServiceImpl.uploadStudentDetailUploadFiles() -- execution started");
        try {
            if (null == studentDetailFileUploadDTO || null == studentDetailFileUploadDTO.getStudentId()) {
                throw new ApiException(Messages.RECORD_ID_MUST_NOT_NULL.getValue());
            }

            if (studentDetailFileUploadDTO.getFiles() == null) {
                throw new ApiException(Messages.NO_FILES_FOUND.getValue());
            }
//			String studentAadharFileName = null;
//			String studentPancardFileName = null;
//			String studentPhotoFileName = null;
//			String fatherPhotoFileName = null;
//			String motherPhotoFileName = null;
//			String spousePhotoFileName = null;

            String studentAadharFileName = FileUtil.getRelativePath(studentDetailFileUploadDTO.getStudentAadharFileName());
            String studentPancardFileName = FileUtil.getRelativePath(studentDetailFileUploadDTO.getStudentPancardFileName());
            String studentPhotoFileName = FileUtil.getRelativePath(studentDetailFileUploadDTO.getStudentPhotoFileName());
            String fatherPhotoFileName = FileUtil.getRelativePath(studentDetailFileUploadDTO.getFatherPhotoFileName());
            String motherPhotoFileName = FileUtil.getRelativePath(studentDetailFileUploadDTO.getMotherPhotoFileNamel());
            String spousePhotoFileName = FileUtil.getRelativePath(studentDetailFileUploadDTO.getSpousePhotoFileName());

            MultipartFile studentAadhar = studentDetailFileUploadDTO.getFiles().get("studentAadhar");
            MultipartFile studentPanCard = studentDetailFileUploadDTO.getFiles().get("studentPanCard");
            MultipartFile studentPhoto = studentDetailFileUploadDTO.getFiles().get("studentPhoto");
            MultipartFile fatherPhoto = studentDetailFileUploadDTO.getFiles().get("fatherPhoto");
            MultipartFile motherPhoto = studentDetailFileUploadDTO.getFiles().get("motherPhoto");
            MultipartFile spousePhoto = studentDetailFileUploadDTO.getFiles().get("spousePhoto");

            String orgCode = SecurityUtil.getOrgCode();
            String schoolCode = SecurityUtil.getSchoolCode();

            if (studentAadhar != null) {
                try {
                    LOGGER.info("Ented into std_aadhar" + studentAadhar);
                    studentAadharFileName = fileService.uploadFile(studentAadhar,
                            FILE_ORG + orgCode + fileSeparator + FILE_COLLEGE + schoolCode + fileSeparator + FILE_APP
                                    + studentDetailFileUploadDTO.getStudentId() + fileSeparator + FILE_AADHAR
                                    + FilenameUtils.getExtension(studentAadhar.getOriginalFilename()));
                    studentDetailFileUploadDTO.setStudentAadharFileStatus(Status.SUCESS.getName());
                } catch (Exception e) {
                    LOGGER.error("Error While uploading aadhar : " + studentAadhar);
                    studentDetailFileUploadDTO.setStudentAadharFileStatus("Failed to upload Student Aadhar");
                }
            }

            if (studentPanCard != null) {
                try {
                    studentPancardFileName = fileService.uploadFile(studentPanCard,
                            FILE_ORG + orgCode + fileSeparator + FILE_COLLEGE + schoolCode + fileSeparator + FILE_APP
                                    + studentDetailFileUploadDTO.getStudentId() + fileSeparator + FILE_PAN
                                    + FilenameUtils.getExtension(studentPanCard.getOriginalFilename()));
                    studentDetailFileUploadDTO.setStudentPanCardFileStatus(Status.SUCESS.getName());
                } catch (Exception e) {
                    LOGGER.error("Error While uploading Pancard : " + studentPanCard);
                    studentDetailFileUploadDTO.setStudentPanCardFileStatus("Failed to upload Student Pancard");
                }
            }

            if (studentPhoto != null) {
                try {
                    studentPhotoFileName = fileService.uploadFile(studentPhoto,
                            FILE_ORG + orgCode + fileSeparator + FILE_COLLEGE + schoolCode + fileSeparator + FILE_APP
                                    + studentDetailFileUploadDTO.getStudentId() + fileSeparator + FILE_PHOTO
                                    + FilenameUtils.getExtension(studentPhoto.getOriginalFilename()));
                    studentDetailFileUploadDTO.setStudentPhotoFileStatus(Status.SUCESS.getName());
                } catch (Exception e) {
                    LOGGER.error("Error While uploading Photo : " + studentPhoto + e);
                    studentDetailFileUploadDTO.setStudentPhotoFileStatus("Failed to upload Student Photo ");
                }
            }

            if (fatherPhoto != null) {
                try {
                    fatherPhotoFileName = fileService.uploadFile(fatherPhoto,
                            FILE_ORG + orgCode + fileSeparator + FILE_COLLEGE + schoolCode + fileSeparator + FILE_APP
                                    + studentDetailFileUploadDTO.getStudentId() + fileSeparator + FILE_FATHER_PHOTO
                                    + FilenameUtils.getExtension(fatherPhoto.getOriginalFilename()));
                    studentDetailFileUploadDTO.setFatherPhotoFileStatus(Status.SUCESS.getName());
                } catch (Exception e) {
                    LOGGER.error("Error While uploading Father Photo : " + fatherPhoto);
                    studentDetailFileUploadDTO.setFatherPhotoFileStatus("Failed to upload Father Photo ");
                }
            }

            if (motherPhoto != null) {
                try {
                    motherPhotoFileName = fileService.uploadFile(motherPhoto,
                            FILE_ORG + orgCode + fileSeparator + FILE_COLLEGE + schoolCode + fileSeparator + FILE_APP
                                    + studentDetailFileUploadDTO.getStudentId() + fileSeparator + FILE_MOTHER_PHOTO
                                    + FilenameUtils.getExtension(motherPhoto.getOriginalFilename()));
                    studentDetailFileUploadDTO.setMotherPhotoFileStatus(Status.SUCESS.getName());
                } catch (Exception e) {
                    LOGGER.error("Error While uploading Mother Photo : " + motherPhoto);
                    studentDetailFileUploadDTO.setMotherPhotoFileStatus("Failed to upload Mother Photo ");
                }
            }

            if (spousePhoto != null) {
                try {
                    spousePhotoFileName = fileService.uploadFile(spousePhoto,
                            FILE_ORG + orgCode + fileSeparator + FILE_COLLEGE + schoolCode + fileSeparator + FILE_APP
                                    + studentDetailFileUploadDTO.getStudentId() + fileSeparator + FILE_SPOUSE_PHOTO
                                    + FilenameUtils.getExtension(spousePhoto.getOriginalFilename()));
                    studentDetailFileUploadDTO.setSpousePhotoFileStatus(Status.SUCESS.getName());
                } catch (Exception e) {
                    LOGGER.error("Error While uploading Spouse Photo : " + spousePhoto);
                    studentDetailFileUploadDTO.setSpousePhotoFileStatus("Failed to upload Spouse Photo ");
                }
            }

            if (studentAadharFileName != null || studentPancardFileName != null || studentPhotoFileName != null
                    || motherPhotoFileName != null || fatherPhotoFileName != null || spousePhotoFileName != null) {

                try {
                    studentDetailRepository.updateStudentAdmissionPhotFileNames(
                            studentDetailFileUploadDTO.getStudentId(), studentAadharFileName, studentPancardFileName,
                            studentPhotoFileName, fatherPhotoFileName, motherPhotoFileName, spousePhotoFileName);
                    response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), studentDetailFileUploadDTO,
                            HttpStatus.OK.value());
                } catch (Exception e) {
                    response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), studentDetailFileUploadDTO,
                            HttpStatus.OK.value());
                }
            } else {
                response = new ApiResponse<>(Messages.FAILED_TO_UPLOAD_IMAGES.getValue(), studentDetailFileUploadDTO,
                        HttpStatus.OK.value());
            }
            String docPath = null;
            try {
                Map<String, MultipartFile> files = studentDetailFileUploadDTO.getFiles();
                for (Entry<String, MultipartFile> entry : files.entrySet()) {
                    String docRepId = entry.getKey();
                    if (docRepId.matches("[0-9]+")) {
                        MultipartFile appFile = entry.getValue();
                        try {
                            docPath = fileService.uploadFile(appFile,
                                    FILE_ORG + orgCode + fileSeparator + FILE_COLLEGE + schoolCode + fileSeparator
                                            + FILE_APP + studentDetailFileUploadDTO.getStudentId() + fileSeparator
                                            + FILE_DOCUMENTS + fileSeparator + docRepId + "."
                                            + FilenameUtils.getExtension(entry.getValue().getOriginalFilename()));
                            studentDetailFileUploadDTO.setStudentDocumentsStatus(Status.SUCESS.getName());
                            if (docPath != null) {
                                studentDocumentCollectionRepository.updateStudentDocumentFiles(
                                        studentDetailFileUploadDTO.getStudentId(), docRepId, docPath);

                                response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(),
                                        studentDetailFileUploadDTO, HttpStatus.OK.value());
                            }

                        } catch (Exception e) {
                            studentDetailFileUploadDTO.setFiles(null);
                            studentDetailFileUploadDTO.setStudentDocumentsStatus("Failed to upload Student Documents");
                            response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), studentDetailFileUploadDTO,
                                    HttpStatus.INTERNAL_SERVER_ERROR.value());
                        }
                    }
                }

            } catch (Exception e) {
                studentDetailFileUploadDTO.setStudentDocumentsStatus("Failed to upload Student Documents");
                response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), studentDetailFileUploadDTO,
                        HttpStatus.INTERNAL_SERVER_ERROR.value());
            }
            LOGGER.info("StudentDetailServiceImpl.uploadStudentDetailUploadFiles() -- execution ended");
        } catch (Exception e) {
            LOGGER.error("StudentDetailServiceImpl.uploadStudentDetailUploadFiles()" + e);
            response = new ApiResponse<>(e.getMessage(), studentDetailFileUploadDTO,
                    HttpStatus.INTERNAL_SERVER_ERROR.value());
        } finally {
            LOGGER.info("StudentDetailServiceImpl.uploadStudentDetailUploadFiles() -- finally block executed");
            if (studentDetailFileUploadDTO != null) {
                studentDetailFileUploadDTO.setFiles(null);
            }
        }
        return response;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApiResponse<?> detainStudentDetail(List<StudentDetainRequestDTO> studentDetainRequestDTOs) {
        ApiResponse<Long> response = null;
        try {
            // String detain = StudentDetailEnum.DTND.getValue();
            // GeneralDetail generalDetail =
            // generalDetailRepository.getGeneralDetailByCode(detain);
            // if (generalDetail != null) {
            for (StudentDetainRequestDTO studentDetainRequestDTO : studentDetainRequestDTOs) {
                Long count = studentDetailRepository.updateStudentStatus(studentDetainRequestDTO.getStudentStatusId(),
                        studentDetainRequestDTO.getSchoolId(), studentDetainRequestDTO.getStudentId(),
                        studentDetainRequestDTO.getReason(), null, null, null);
                if (count > 0) {
                    StudentAcademicbatch studentAcademicbatch = studentAcademicBatchesRepository
                            .getDetails(studentDetainRequestDTO.getSchoolId(), studentDetainRequestDTO.getStudentId());
                    if (studentAcademicbatch != null) {
						/*if (studentDetainRequestDTO.getStudentStatusId() != null) {
							GeneralDetail studentStatus = new GeneralDetail();
							studentStatus.setGeneralDetailId(studentDetainRequestDTO.getStudentStatusId());
							studentAcademicbatch.setStudentStatus(studentStatus);
						}*/
                        Calendar calendarToDate = Calendar.getInstance();
                        if (studentDetainRequestDTO.getToDate() != null) {
                            Date toDate = studentDetainRequestDTO.getToDate();
                            calendarToDate.setTime(toDate);
                        }
                        calendarToDate.set(Calendar.HOUR, 23);
                        calendarToDate.set(Calendar.MINUTE, 59);
                        calendarToDate.set(Calendar.SECOND, 59);
                        calendarToDate.set(Calendar.MILLISECOND, 55);
                        calendarToDate.set(Calendar.HOUR_OF_DAY, 23);
                        calendarToDate.add(Calendar.DATE, -1);
                        //
                        //calendarToDate.setTime(calendarToDate.getTime());
                        /// Date dateTimee=calendarToDate.getTime();
                        studentAcademicbatch.setToDate(calendarToDate.getTime());
                        LOGGER.info("setToDate" + calendarToDate.getTime());
                        //studentAcademicbatch.setFromCourseYear(studentAcademicbatch.getFromCourseYear());
                        //studentAcademicbatch.setToBatch(studentAcademicbatch.getFromBatch());
                        //studentAcademicbatch.setFromBatch(studentAcademicbatch.getFromBatch());
                        //studentAcademicbatch.setToCourseYear(studentAcademicbatch.getFromCourseYear());
                        //studentAcademicbatch.setCourseGroup(studentAcademicbatch.getCourseGroup());
                        studentAcademicbatch.setToGroupSection(null);
                        studentAcademicbatch.setReason(studentDetainRequestDTO.getReason());
                        studentAcademicbatch.setUpdatedDt(new Date());
                        studentAcademicbatch.setUpdatedUser(SecurityUtil.getCurrentUser());
                        studentAcademicbatch = studentAcademicBatchesRepository.save(studentAcademicbatch);
                        studentAcademicBatchesRepository.flush();
                        //new record insert
                        if (studentAcademicbatch != null) {
                            StudentAcademicbatch studentAcademicbatchObj = new StudentAcademicbatch();
                            studentAcademicbatchObj.setSchool(studentAcademicbatch.getSchool());
                            studentAcademicbatchObj.setStudentDetail(studentAcademicbatch.getStudentDetail());
                            studentAcademicbatchObj.setCourse(studentAcademicbatch.getCourse());
                            studentAcademicbatchObj.setAcademicYear(studentAcademicbatch.getAcademicYear());
                            studentAcademicbatchObj.setFromCourseYear(studentAcademicbatch.getFromCourseYear());
                            studentAcademicbatchObj.setFromBatch(studentAcademicbatch.getFromBatch());
                            studentAcademicbatchObj.setFromGroupSection(studentAcademicbatch.getFromGroupSection());
                            if (studentDetainRequestDTO.getStudentStatusId() != null) {
                                GeneralDetail studentStatus = new GeneralDetail();
                                studentStatus.setGeneralDetailId(studentDetainRequestDTO.getStudentStatusId());
                                studentAcademicbatchObj.setStudentStatus(studentStatus);
                            }
                            studentAcademicbatchObj.setIsPromoted(Boolean.FALSE);
                            studentAcademicbatchObj.setIsAlmuni(studentAcademicbatch.getIsAlmuni());
                            studentAcademicbatchObj.setIsActive(studentAcademicbatch.getIsActive());
                            Calendar calendarFromDate = Calendar.getInstance();
                            if (studentDetainRequestDTO.getFromDate() != null) {
                                Date fromDate = studentDetainRequestDTO.getFromDate();
                                calendarFromDate.setTime(fromDate);
                            }
                            //calendarFromDate.set(Calendar.HOUR, 0);
                            calendarFromDate.set(Calendar.MINUTE, 0);
                            calendarFromDate.set(Calendar.SECOND, 0);
                            calendarFromDate.set(Calendar.MILLISECOND, 0);
                            calendarFromDate.set(Calendar.HOUR_OF_DAY, 0);
                            studentAcademicbatchObj.setFromDate(calendarFromDate.getTime());
                            //	studentAcademicbatchObj.setToDate(studentAcademicbatch.getToDate());
                            studentAcademicbatchObj.setToDate(null);
                            studentAcademicbatchObj.setCreatedDt(studentAcademicbatch.getCreatedDt());
                            studentAcademicbatchObj.setCreatedUser(studentAcademicbatch.getCreatedUser());
                            studentAcademicbatchObj.setUpdatedDt(studentAcademicbatch.getUpdatedDt());
                            studentAcademicbatchObj.setUpdatedUser(studentAcademicbatch.getUpdatedUser());

                            studentAcademicbatchObj.setToBatch(studentAcademicbatch.getFromBatch());
                            studentAcademicbatchObj.setToCourseYear(studentAcademicbatch.getFromCourseYear());
                            studentAcademicbatchObj.setToGroupSection(null);
                            studentAcademicbatchObj.setReason(studentDetainRequestDTO.getReason());
                            studentAcademicBatchesRepository.save(studentAcademicbatchObj);
                            studentAcademicBatchesRepository.flush();

                        }
                        List<Long> nos = new ArrayList<>();
                        nos.add(studentDetainRequestDTO.getStudentId());
                        /*Integer updateCount = batchwiseStudentRepository.updateBatchwiseStudents(nos,
                                studentDetainRequestDTO.getGroupSectionId(), new Date(), SecurityUtil.getCurrentUser());
                        LOGGER.debug("Student batches updated count : " + updateCount);*/

                        response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), HttpStatus.OK.value());
                    } else {

                        LOGGER.debug("StudentAcademicbatch not updated");
                        throw new ApiException(Messages.UPDATE_FAILURE.getValue());
                        //response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), HttpStatus.OK.value());
                    }

                } else {
                    throw new ApiException(Messages.UPDATE_FAILURE.getValue());
                    //response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), HttpStatus.OK.value());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }


    @Override
    public ApiResponse<List<StudentDetailListDTO>> getDetainStudentsList(Boolean status, Long schoolId, Long courseId,
                                                                         Long courseYearId, Long groupSectionId, Long academicYearId) {
        ApiResponse<List<StudentDetailListDTO>> response = null;
        try {
            String detain = StudentDetailEnum.DTND.getValue();
            GeneralDetail generalDetail = generalDetailRepository.getGeneralDetailByCode(detain);

            List<StudentDetail> studentDetailsList = studentDetailRepository.findDetainStudentsList(status, schoolId,
                    courseId, courseYearId, groupSectionId, academicYearId,
                    generalDetail.getGeneralDetailId());

            if (!CollectionUtils.isEmpty(studentDetailsList)) {
                List<StudentDetailListDTO> studentDetailDTOs = studentDetailListMapper
                        .convertEntityListToDTOList(studentDetailsList);
                response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), studentDetailDTOs);
            } else {
                response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
                        HttpStatus.OK.value());
            }
        } catch (Exception e) {
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }

    /*
     * 	27-04-2020
     *  regulationId updated in studentdetails and unwanted code commented (@rif)
     *  requirement need to update toDate,CourseYear,GroupSection
     */
    @Override
    @Transactional
    public ApiResponse<?> reAdmissionStudent(StudentDetainRequestDTO studentDetainRequestDTO) {
        ApiResponse<?> response = null;
        //List<StudentAttendance> studentAttendanceList=null;
        List<StudentAttendance> studentAttendanceListofList = new ArrayList<>();
        try {
            String readmission = StudentDetailEnum.INCOLLEGE.getValue();
            GeneralDetail generalDetail = generalDetailRepository.getGeneralDetailByCode(readmission);

            /*studentAttendanceList=studentAttendanceRepository.findByAbsentList(studentDetainRequestDTO.getToDate(),studentDetainRequestDTO.getStudentId());*/
			
			/*if(studentAttendanceList.size()>0) {
				studentAttendanceListofList.addAll(studentAttendanceList);
			}*/

            //if(studentAttendanceList.isEmpty()) {}
            if (generalDetail != null) {
                Long count = studentDetailRepository.updateStudentStatus(generalDetail.getGeneralDetailId(),
                        studentDetainRequestDTO.getSchoolId(), studentDetainRequestDTO.getStudentId(),
                        studentDetainRequestDTO.getReason(), studentDetainRequestDTO.getAcademicYearId(),
                        studentDetainRequestDTO.getCourseYearId(), studentDetainRequestDTO.getGroupSectionId());
                if (count > 0) {
                    StudentAcademicbatch studentAcademicbatch = studentAcademicBatchesRepository
                            .getDetails(studentDetainRequestDTO.getSchoolId(), studentDetainRequestDTO.getStudentId());

                    //	studentAcademicbatch.setToDate(dateTimee);
                    //	LOGGER.info("setToDate"+dateTimee);
                    if (studentAcademicbatch != null) {
                        Calendar calendarToDate = Calendar.getInstance();
                        if (studentDetainRequestDTO.getToDate() != null) {
                            Date toDate = studentDetainRequestDTO.getToDate();
                            calendarToDate.setTime(toDate);
                        }
                        calendarToDate.set(Calendar.MINUTE, 59);
                        calendarToDate.set(Calendar.SECOND, 59);
                        calendarToDate.set(Calendar.MILLISECOND, 59);
                        calendarToDate.set(Calendar.HOUR_OF_DAY, 23);
                        calendarToDate.add(Calendar.DATE, -1);
                        //
                        //calendarToDate.setTime(calendarToDate.getTime());
                        //   Date toDateTime=calendarToDate.getTime();
				           
				           /* studentAcademicbatch.setSchool(studentAcademicbatch.getSchool());
				            studentAcademicbatch.setStudentDetail(studentAcademicbatch.getStudentDetail());
				            studentAcademicbatch.setCourse(studentAcademicbatch.getCourse());
				            studentAcademicbatch.setAcademicYear(studentAcademicbatch.getAcademicYear());
				            studentAcademicbatch.setFromCourseYear(studentAcademicbatch.getFromCourseYear());*/

                        CourseYear courseYear = new CourseYear();
                        courseYear.setCourseYearId(studentDetainRequestDTO.getCourseYearId());
                        studentAcademicbatch.setToCourseYear(courseYear);

                        // studentAcademicbatch.setFromBatch(studentAcademicbatch.getFromBatch());
                        // studentAcademicbatch.setToBatch(studentAcademicbatch.getToBatch());
                        //  studentAcademicbatch.setFromGroupSection(studentAcademicbatch.getFromGroupSection());

                        GroupSection groupSection = new GroupSection();
                        groupSection.setGroupSectionId(studentDetainRequestDTO.getGroupSectionId());
                        studentAcademicbatch.setToGroupSection(groupSection);
				            
				         /* studentAcademicbatch.setStudentStatus(studentAcademicbatch.getStudentStatus());
				            studentAcademicbatch.setIsPromoted(studentAcademicbatch.getIsPromoted());
				            studentAcademicbatch.setIsAlmuni(studentAcademicbatch.getIsAlmuni());
				            studentAcademicbatch.setReason(studentAcademicbatch.getReason());
				            studentAcademicbatch.setIsActive(studentAcademicbatch.getIsActive());
				            studentAcademicbatch.setFromDate(studentAcademicbatch.getFromDate());*/

                        studentAcademicbatch.setToDate(calendarToDate.getTime());

                        LOGGER.info("ToDate" + calendarToDate.getTime());
				         /* studentAcademicbatch.setCreatedDt(studentAcademicbatch.getCreatedDt());
				            studentAcademicbatch.setCreatedUser(studentAcademicbatch.getCreatedUser());
				            studentAcademicbatch.setCourseGroup(studentAcademicbatch.getCourseGroup());*/

                        studentAcademicbatch.setUpdatedDt(new Date());
                        studentAcademicbatch.setUpdatedUser(SecurityUtil.getCurrentUser());
                        studentAcademicbatch = studentAcademicBatchesRepository.save(studentAcademicbatch);
                        studentAcademicBatchesRepository.flush();
                        //Integer toDateUpdate=studentAcademicBatchesRepository.updateToDateForDetainRecord(studentDetainRequestDTO.getSchoolId(), studentDetainRequestDTO.getStudentId(),toDateTime);
                    }
                    if (studentAcademicbatch != null) {
                        StudentAcademicbatch studentAcademicbatchNew = new StudentAcademicbatch();
							/*Long updateCount = studentAcademicBatchesRepository
									.updateIsActive(studentAcademicbatch.getStudentAcademicbatchId(), Boolean.FALSE);*/

                        studentAcademicbatchNew.setStudentAcademicbatchId(null);
                        School school = new School();
                        school.setSchoolId(studentDetainRequestDTO.getSchoolId());
                        studentAcademicbatchNew.setSchool(school);

                        StudentDetail student = new StudentDetail();
                        student.setStudentId(studentDetainRequestDTO.getStudentId());
                        studentAcademicbatchNew.setStudentDetail(student);

                        Course course = new Course();
                        course.setCourseId(studentDetainRequestDTO.getCourseId());
                        studentAcademicbatchNew.setCourse(course);

                        AcademicYear academicYear = new AcademicYear();
                        academicYear.setAcademicYearId(studentDetainRequestDTO.getAcademicYearId());
                        studentAcademicbatchNew.setAcademicYear(academicYear);
							
							/*if(studentDetainRequestDTO.getBatchId()!=null) {
								Batch batch = new Batch();
								batch.setBatchId(studentDetainRequestDTO.getBatchId());
								studentAcademicbatchNew.setFromBatch(batch);
								//studentAcademicbatchNew.setToBatch(batch);
							}		*/
                        //studentAcademicbatchNew.setFromBatch(studentAcademicbatch.getFromBatch());
                        CourseYear courseYear = new CourseYear();
                        courseYear.setCourseYearId(studentDetainRequestDTO.getCourseYearId());
                        studentAcademicbatchNew.setFromCourseYear(courseYear);

                        GroupSection groupSection = new GroupSection();
                        groupSection.setGroupSectionId(studentDetainRequestDTO.getGroupSectionId());
                        studentAcademicbatchNew.setFromGroupSection(groupSection);
                        Calendar calendarFromDate = Calendar.getInstance();
                        if (studentDetainRequestDTO.getFromDate() != null) {
                            Date fromDate = studentDetainRequestDTO.getFromDate();
                            calendarFromDate.setTime(fromDate);
                        }
                        calendarFromDate.set(Calendar.MINUTE, 0);
                        calendarFromDate.set(Calendar.SECOND, 0);
                        calendarFromDate.set(Calendar.MILLISECOND, 0);
                        calendarFromDate.set(Calendar.HOUR_OF_DAY, 0);
                        calendarFromDate.add(Calendar.DATE, 0);
                        studentAcademicbatchNew.setFromDate(calendarFromDate.getTime());

                        if (generalDetail.getGeneralDetailId() != null) {
                            GeneralDetail studentStatus = new GeneralDetail();
                            studentStatus.setGeneralDetailId(generalDetail.getGeneralDetailId());
                            studentAcademicbatchNew.setStudentStatus(studentStatus);
                        }




                        studentAcademicbatchNew.setToCourseYear(null);
                        studentAcademicbatchNew.setToGroupSection(null);
                        studentAcademicbatchNew.setToDate(null);
                        studentAcademicbatchNew.setReason(studentDetainRequestDTO.getReason());
                        studentAcademicbatchNew.setIsActive(Boolean.TRUE);
                        studentAcademicbatchNew.setCreatedDt(new Date());
                        studentAcademicbatchNew.setCreatedUser(SecurityUtil.getCurrentUser());
                        studentAcademicbatchNew.setUpdatedDt(new Date());
                        studentAcademicbatchNew.setUpdatedUser(SecurityUtil.getCurrentUser());

                        studentAcademicBatchesRepository.save(studentAcademicbatchNew);
                        studentAcademicBatchesRepository.flush();
                    }

                    //TODO Need to clarification about detain studetn feee details.
                    //TODO  Here shell we create New records as new students are any other scenario
						 /* List<FeeStudentDataParticularDTO> feeStudentDataParticularDTOs = new ArrayList<>();
						List<FeeStudentDataDTO> feeStudentDataDTOs = new ArrayList<>();
						List<FeeStudentDataParticular> feeStudentDataParticular = null;
						List<FeeStudentData> feeStudentDatas = null;
						feeStudentDataService.saveAdmitStudentFeeDetail(studentDetainRequestDTO.getSchoolId(),
								studentDetainRequestDTO.getAcademicYearId(),
								studentDetainRequestDTO.getCourseYearId(), null,
								studentDetainRequestDTO.getQuotaId(), studentDetainRequestDTO.getStudentId(),
								feeStudentDataParticularDTOs, feeStudentDataDTOs);

						if (!CollectionUtils.isEmpty(feeStudentDataDTOs)) {
							feeStudentDatas = feeStudentDataMapper.convertDTOListToEntityList(feeStudentDataDTOs);

							feeStudentDatas = feeStudentDataRepository.saveAll(feeStudentDatas);
							feeStudentDataRepository.flush();
						}
						if (!CollectionUtils.isEmpty(feeStudentDataParticularDTOs)) {
							feeStudentDataParticular = feeStudentDataParticularMapper
									.convertDTOListToEntityList(feeStudentDataParticularDTOs);

							feeStudentDataParticular = feeStudentDataParticularRepository
									.saveAll(feeStudentDataParticular);
							feeStudentDataParticularRepository.flush();
						}*/

                    //response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), HttpStatus.OK.value());
                    //response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), studentAttendanceMapper.convertEntityListToDTOList(studentAttendanceListofList), HttpStatus.OK.value());
                } else {
                    response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), HttpStatus.OK.value());
                }
            } else {
                response = new ApiResponse<>("Detain Id not found", HttpStatus.OK.value());
            }
			
			/*else {
				response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), studentAttendanceMapper.convertEntityListToDTOList(studentAttendanceListofList), HttpStatus.OK.value());
			}*/

        } catch (Exception e) {
            e.printStackTrace();
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }

    @Override
    @Transactional
    public ApiResponse<?> changeStudentSection(Long schoolId, Long studentId, Long groupSectionId) {
        ApiResponse<Long> response = null;
        try {
            Long count = studentDetailRepository.updateStudentSection(schoolId, studentId, groupSectionId);
            if (count > 0) {
                StudentAcademicbatch studentAcademicbatch = studentAcademicBatchesRepository.getDetails(schoolId,
                        studentId);
                if (studentAcademicbatch != null) {
                    studentAcademicbatch.setIsActive(Boolean.FALSE);
                    studentAcademicbatch.setToDate(new Date());
                    studentAcademicbatch.setToBatch(studentAcademicbatch.getFromBatch());
                    studentAcademicbatch.setToCourseYear(studentAcademicbatch.getFromCourseYear());
                    studentAcademicbatch.setToGroupSection(studentAcademicbatch.getFromGroupSection());
                    studentAcademicBatchesRepository.save(studentAcademicbatch);
                    StudentAcademicbatch studentAcademicbatchNew = new StudentAcademicbatch();
					/*Long updateCount = studentAcademicBatchesRepository
							.updateSection(studentAcademicbatch.getStudentAcademicbatchId(), groupSectionId);*/


                    studentAcademicbatchNew.setStudentAcademicbatchId(studentAcademicbatch.getStudentAcademicbatchId());
                    studentAcademicbatchNew.setSchool(studentAcademicbatch.getSchool());
                    studentAcademicbatchNew.setStudentDetail(studentAcademicbatch.getStudentDetail());
                    studentAcademicbatchNew.setCourse(studentAcademicbatch.getCourse());
                    studentAcademicbatchNew.setAcademicYear(studentAcademicbatch.getAcademicYear());

                    studentAcademicbatchNew.setStudentStatus(studentAcademicbatch.getStudentStatus());

                    studentAcademicbatchNew.setFromBatch(studentAcademicbatch.getFromBatch());
                    studentAcademicbatchNew.setFromCourseYear(studentAcademicbatch.getFromCourseYear());

                    GroupSection groupSection = new GroupSection();
                    groupSection.setGroupSectionId(groupSectionId);
                    studentAcademicbatchNew.setFromGroupSection(groupSection);

                    studentAcademicbatchNew.setToBatch(null);
                    studentAcademicbatchNew.setToCourseYear(null);
                    studentAcademicbatchNew.setToGroupSection(null);

                    studentAcademicbatchNew.setReason("Section change");
                    studentAcademicbatchNew.setIsActive(Boolean.TRUE);
                    studentAcademicbatchNew.setFromDate(studentAcademicbatch.getFromDate());
                    studentAcademicbatchNew.setToDate(null);
                    studentAcademicbatchNew.setCreatedDt(new Date());
                    studentAcademicbatchNew.setCreatedUser(SecurityUtil.getCurrentUser());

                    studentAcademicBatchesRepository.save(studentAcademicbatchNew);
                    studentAcademicBatchesRepository.flush();
                }

                response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), HttpStatus.OK.value());
            } else {
                response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), HttpStatus.OK.value());
            }
        } catch (Exception e) {
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }

    /*@Override
    public ApiResponse<List<StudentDetailListDTO>> getFeeMappingStudentsList(
            StudentFeeRequestDTO studentFeeRequestDTO) {
        ApiResponse<List<StudentDetailListDTO>> response = null;
        List<StudentDetailListDTO> studentDetailDTOList = new ArrayList<>();
        List<StudentDetailListDTO> studentDetailDTOs = null;
        try {
            Long schoolId = studentFeeRequestDTO.getSchoolId();
            Long academicYearId = studentFeeRequestDTO.getAcademicYearId();
            Map<Long, ArrayList<Long>> courseGroupYearsIds = studentFeeRequestDTO.getCourseGroupYearsIds();
            Iterator<Map.Entry<Long, ArrayList<Long>>> itr = courseGroupYearsIds.entrySet().iterator();

            while (itr.hasNext()) {
                Entry<Long, ArrayList<Long>> hm = itr.next();
                List<StudentDetail> studentDetailsList = studentDetailRepository.findFeeMappingStudentList(schoolId,
                        null,hm.getKey(), hm.getValue(), academicYearId);
                if (!CollectionUtils.isEmpty(studentDetailsList)) {
                    studentDetailDTOs = studentDetailListMapper.convertEntityListToDTOList(studentDetailsList);
                }
                if (!CollectionUtils.isEmpty(studentDetailDTOs)) {
                    for (StudentDetailListDTO studentDetailListDTO : studentDetailDTOs) {
                        studentDetailDTOList.add(studentDetailListDTO);
                    }
                }

            }

            if (!CollectionUtils.isEmpty(studentDetailDTOList)) {
                response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), studentDetailDTOList);
            } else {
                response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
                        HttpStatus.OK.value());
            }
        } catch (

        Exception e) {
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }
*/


    @Override
    @Transactional
    public ApiResponse<?> updateRollno(@Valid List<StudentRollNumberDTO> studentRollNumberDTOList) {
        ApiResponse<String> response = null;
        try {
            List<String> duplicateRollnumbers = new ArrayList<>();
            for (StudentRollNumberDTO studentRollNumberDTO : studentRollNumberDTOList) {
                StudentDetail studentDetail = studentDetailRepository.checkRollNumberIsDuplicate(studentRollNumberDTO.getRollNumber());
                if (studentDetail == null) {
                    studentDetailRepository.updateRollNumber(studentRollNumberDTO.getStudentId(), studentRollNumberDTO.getRollNumber());
                } else {
                    if (studentDetail.getStudentId().equals(studentRollNumberDTO.getStudentId())) {
                        studentDetailRepository.updateRollNumber(studentRollNumberDTO.getStudentId(), studentRollNumberDTO.getRollNumber());
                    } else {
                        duplicateRollnumbers.add(studentRollNumberDTO.getRollNumber());
                    }
                }
            }
            if (CollectionUtils.isEmpty(duplicateRollnumbers)) {
                response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), null);
            } else {
                response = new ApiResponse<>(false, Messages.UPDATE_FAILURE.getValue(),
                        duplicateRollnumbers + " are duplicate RollNumbers", HttpStatus.OK.value());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }

    @Override
    public ApiResponse<List<StudentDetailListDTO>> getStudentsScholarshipList(Boolean status, Long schoolId,
                                                                              Long courseId, Long courseYearId, Long groupSectionId, Long academicYearId,
                                                                              Boolean isScholarship) {
        ApiResponse<List<StudentDetailListDTO>> response = null;
        try {
            String generalDetailCode = StudentDetailEnum.DTND.getValue();
            if (status == null) {
                status = Boolean.TRUE;
            }
            List<StudentDetail> studentDetailsList = studentDetailRepository.findStudentScholarshipList(status, schoolId,
                    courseId,  courseYearId, groupSectionId, academicYearId, generalDetailCode, isScholarship);

            if (!CollectionUtils.isEmpty(studentDetailsList)) {
                List<StudentDetailListDTO> studentDetailDTOs = studentDetailListMapper
                        .convertEntityListToDTOList(studentDetailsList);
                response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), studentDetailDTOs);
            } else {
                response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
                        HttpStatus.OK.value());
            }
        } catch (Exception e) {
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }


    @Override
    @Transactional
    public ApiResponse<?> discontinueStudentDetail(@Valid List<StudentDiscontinueRequestDTO> studentDiscontinueRequestDTOs) {
        ApiResponse<Long> response = null;
        try {
            String discontinue = StudentDetailEnum.DISCONTINUED.getValue();
            GeneralDetail generalDetail = generalDetailRepository.getGeneralDetailByCode(discontinue);
            if (generalDetail != null) {
                for (StudentDiscontinueRequestDTO studentDiscontinueRequestDTO : studentDiscontinueRequestDTOs) {


                    Long count = studentDetailRepository.updateStudentStatus(generalDetail.getGeneralDetailId(),
                            studentDiscontinueRequestDTO.getSchoolId(), studentDiscontinueRequestDTO.getStudentId(),
                            studentDiscontinueRequestDTO.getReason(), null, null, null);
                    if (count > 0) {
                        StudentAcademicbatch studentAcademicbatch = studentAcademicBatchesRepository.getDetails(
                                studentDiscontinueRequestDTO.getSchoolId(), studentDiscontinueRequestDTO.getStudentId());

                        if (studentAcademicbatch != null) {
							/*if (generalDetail.getGeneralDetailId() != null) {
								GeneralDetail studentStatus = new GeneralDetail();
								studentStatus.setGeneralDetailId(generalDetail.getGeneralDetailId());
								studentAcademicbatch.setStudentStatus(studentStatus);
							}*/
                            //studentAcademicbatch.setStudentStatus(studentAcademicbatch.getStudentStatus());
                            Calendar calendarToDate = Calendar.getInstance();
                            if (studentDiscontinueRequestDTO.getToDate() != null) {
                                Date toDate = studentDiscontinueRequestDTO.getToDate();
                                calendarToDate.setTime(toDate);
                            }
                            calendarToDate.set(Calendar.HOUR, 23);
                            calendarToDate.set(Calendar.MINUTE, 59);
                            calendarToDate.set(Calendar.SECOND, 59);
                            calendarToDate.set(Calendar.HOUR_OF_DAY, 23);
                            calendarToDate.add(Calendar.DATE, -1);
                            LOGGER.info("discontinueStudentDetail-->ToDate-->" + calendarToDate.getTime());
                            studentAcademicbatch.setToDate(calendarToDate.getTime());
                            studentAcademicbatch.setToBatch(studentAcademicbatch.getFromBatch());
                            studentAcademicbatch.setToCourseYear(null);
                            studentAcademicbatch.setToGroupSection(null);
                            studentAcademicbatch.setReason(studentDiscontinueRequestDTO.getReason());
                            //studentAcademicbatch.setCourseGroup(studentAcademicbatch.getCourseGroup());

                            studentAcademicBatchesRepository.save(studentAcademicbatch);
                            studentAcademicBatchesRepository.flush();

                            //new record insert for discontinue
                            if (studentAcademicbatch != null) {

                                StudentAcademicbatch studentAcademicbatchObj = new StudentAcademicbatch();

                                studentAcademicbatchObj.setSchool(studentAcademicbatch.getSchool());
                                studentAcademicbatchObj.setStudentDetail(studentAcademicbatch.getStudentDetail());
                                studentAcademicbatchObj.setCourse(studentAcademicbatch.getCourse());
                                studentAcademicbatchObj.setAcademicYear(studentAcademicbatch.getAcademicYear());
                                studentAcademicbatchObj.setFromCourseYear(studentAcademicbatch.getFromCourseYear());
                                studentAcademicbatchObj.setFromBatch(studentAcademicbatch.getFromBatch());
                                studentAcademicbatchObj.setFromGroupSection(studentAcademicbatch.getFromGroupSection());


                                if (generalDetail.getGeneralDetailId() != null) {
                                    GeneralDetail studentStatus = new GeneralDetail();
                                    studentStatus.setGeneralDetailId(generalDetail.getGeneralDetailId());
                                    studentAcademicbatchObj.setStudentStatus(studentStatus);
                                }
                                studentAcademicbatchObj.setIsPromoted(Boolean.FALSE);
                                studentAcademicbatchObj.setIsAlmuni(studentAcademicbatch.getIsAlmuni());
                                studentAcademicbatchObj.setIsActive(studentAcademicbatch.getIsActive());

                                Calendar calendarFromDate = Calendar.getInstance();
                                if (studentDiscontinueRequestDTO.getFromDate() != null) {
                                    Date fromDate = studentDiscontinueRequestDTO.getFromDate();
                                    calendarFromDate.setTime(fromDate);
                                }
                                //calendarFromDate.set(Calendar.HOUR, 0);
                                calendarFromDate.set(Calendar.MINUTE, 0);
                                calendarFromDate.set(Calendar.SECOND, 0);
                                calendarFromDate.set(Calendar.MILLISECOND, 0);
                                calendarFromDate.set(Calendar.HOUR_OF_DAY, 0);
                                studentAcademicbatchObj.setFromDate(calendarFromDate.getTime());
                                studentAcademicbatchObj.setToDate(null);
                                studentAcademicbatchObj.setCreatedDt(new Date());
                                studentAcademicbatchObj.setCreatedUser(SecurityUtil.getCurrentUser());
                                //studentAcademicbatchObj.setUpdatedDt(studentAcademicbatch.getUpdatedDt());
                                //studentAcademicbatchObj.setUpdatedUser(studentAcademicbatch.getUpdatedUser());

                                studentAcademicbatchObj.setToBatch(studentAcademicbatch.getFromBatch());
                                studentAcademicbatchObj.setToCourseYear(null);
                                studentAcademicbatchObj.setToGroupSection(null);
                                studentAcademicbatchObj.setReason(studentDiscontinueRequestDTO.getReason());

                                studentAcademicBatchesRepository.save(studentAcademicbatchObj);
                                studentAcademicBatchesRepository.flush();


                                List<Long> nos = new ArrayList<>();
                                nos.add(studentDiscontinueRequestDTO.getStudentId());
                                /*Integer updateCount = batchwiseStudentRepository.updateBatchwiseStudents(nos, studentDiscontinueRequestDTO.getGroupSectionId(), new Date(), SecurityUtil.getCurrentUser());
                                LOGGER.debug("Student batches updated count : " + updateCount);*/
                            }

                            response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), HttpStatus.OK.value());
                        } else {
                            response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), HttpStatus.OK.value());
                        }
                    }
                }
            } else {
                response = new ApiResponse<>("Discontinue Id not found", HttpStatus.OK.value());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }

    @Override
    @Transactional
    public ApiResponse<?> detainRecommended(@Valid List<StudentDetainRequestDTO> studentDetainRequestDTOs) {
        ApiResponse<?> response = null;
        try {
            Long records = 0L;
            for (StudentDetainRequestDTO studentDetainRequestDTO : studentDetainRequestDTOs) {
                String detain = StudentDetailEnum.DTNDRECOMMENDED.getValue();

                if (studentDetainRequestDTO.getStudentStatusId() == null) {
                    GeneralDetail generalDetail = generalDetailRepository.getGeneralDetailByCode(detain);
                    if (generalDetail != null) {
                        studentDetainRequestDTO.setStudentStatusId(generalDetail.getGeneralDetailId());
                    }
                }
                Long count = studentDetailRepository.updateStudentStatus(
                        studentDetainRequestDTO.getStudentStatusId(), studentDetainRequestDTO.getSchoolId(),
                        studentDetainRequestDTO.getStudentId(), studentDetainRequestDTO.getReason(), null, null,  null);
                records = records + count;


            }
            if (records > 0) {
                response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), HttpStatus.OK.value());
                //response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(),studentAttendanceMapper.convertEntityListToDTOList(studentAttendanceListofList), HttpStatus.OK.value());
            } else {
                response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), HttpStatus.OK.value());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }

    /**
     * @author Arif
     * when student is assigned to section the record which is
     * created at the time of admission is updated
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public ApiResponse<?> assignSectionToStudent(List<StudentDetailListDTO> studentApplicationDTOs) {
        ApiResponse<Long> response = null;
        try {
            List<StudentDetail> studentDetailList = studentDetailListMapper
                    .convertDTOListToEntityList(studentApplicationDTOs);
            if (!studentDetailList.isEmpty()) {

                studentDetailRepository.updateRegulationAndGroupsection(studentDetailList);

                for (StudentDetailListDTO studentDetailListDTO : studentApplicationDTOs) {
                    StudentAcademicbatch studentAcademicbatch = studentAcademicBatchesRepository
                            .getDetails(studentDetailListDTO.getSchoolId(), studentDetailListDTO.getStudentId());
                    if (studentAcademicbatch != null) {
                        studentAcademicbatch.setIsActive(Boolean.TRUE);
                        studentAcademicbatch.setToDate(null);
                        studentAcademicbatch.setFromBatch(studentAcademicbatch.getFromBatch());
                        studentAcademicbatch.setFromCourseYear(studentAcademicbatch.getFromCourseYear());

                        GroupSection groupSection = new GroupSection();
                        if (studentDetailListDTO.getGroupSectionId() != null) {
                            groupSection.setGroupSectionId(studentDetailListDTO.getGroupSectionId());
                            studentAcademicbatch.setFromGroupSection(groupSection);
                        }
                        studentAcademicbatch.setToBatch(studentAcademicbatch.getToBatch());
                        studentAcademicbatch.setToCourseYear(studentAcademicbatch.getToCourseYear());
                        studentAcademicbatch.setToGroupSection(null);
                        studentAcademicbatch.setReason("Section update");
                        studentAcademicbatch.setIsActive(Boolean.TRUE);
                        studentAcademicbatch.setUpdatedDt(new Date());
                        studentAcademicbatch.setUpdatedUser(SecurityUtil.getCurrentUser());
                        studentAcademicBatchesRepository.save(studentAcademicbatch);
                    }
                }
                response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), HttpStatus.OK.value());
            }
        } catch (Exception e) {
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }

    @Override
    public ApiResponse<?> passedOutStudent(@Valid List<StudentDetainRequestDTO> studentDetainRequestDTO) {
        ApiResponse<Long> response = null;
        int flag = 0;
        try {

            for (StudentDetainRequestDTO studentPassedOutDTO : studentDetainRequestDTO) {
                StudentAcademicbatch studentAcademicbatch = studentAcademicBatchesRepository
                        .getDetails(studentPassedOutDTO.getSchoolId(), studentPassedOutDTO.getStudentId());


                if (studentAcademicbatch != null) {
                    Calendar calendarToDate = Calendar.getInstance();
                    if (studentPassedOutDTO.getToDate() != null) {
                        Date toDate = studentPassedOutDTO.getToDate();
                        calendarToDate.setTime(toDate);
                    }
                    calendarToDate.set(Calendar.MINUTE, 59);
                    calendarToDate.set(Calendar.SECOND, 59);
                    calendarToDate.set(Calendar.MILLISECOND, 59);
                    calendarToDate.set(Calendar.HOUR_OF_DAY, 23);

                    studentAcademicbatch.setSchool(studentAcademicbatch.getSchool());
                    studentAcademicbatch.setStudentDetail(studentAcademicbatch.getStudentDetail());
                    studentAcademicbatch.setCourse(studentAcademicbatch.getCourse());
                    studentAcademicbatch.setAcademicYear(studentAcademicbatch.getAcademicYear());
                    studentAcademicbatch.setFromCourseYear(studentAcademicbatch.getFromCourseYear());
                    studentAcademicbatch.setToCourseYear(studentAcademicbatch.getToCourseYear());
                    studentAcademicbatch.setFromBatch(studentAcademicbatch.getFromBatch());
                    studentAcademicbatch.setToBatch(studentAcademicbatch.getToBatch());
                    studentAcademicbatch.setFromGroupSection(studentAcademicbatch.getFromGroupSection());
                    studentAcademicbatch.setToGroupSection(studentAcademicbatch.getToGroupSection());
                    GeneralDetail studentStatus = new GeneralDetail();
                    studentStatus.setGeneralDetailId(studentPassedOutDTO.getStudentStatusId());
                    studentAcademicbatch.setStudentStatus(studentStatus);
                    studentAcademicbatch.setIsPromoted(studentAcademicbatch.getIsPromoted());
                    studentAcademicbatch.setIsAlmuni(studentAcademicbatch.getIsAlmuni());
                    studentAcademicbatch.setReason(studentAcademicbatch.getReason());
                    studentAcademicbatch.setIsActive(studentAcademicbatch.getIsActive());
                    studentAcademicbatch.setFromDate(studentAcademicbatch.getFromDate());
                    studentAcademicbatch.setToDate(calendarToDate.getTime());
                    studentAcademicbatch.setCreatedDt(studentAcademicbatch.getCreatedDt());
                    studentAcademicbatch.setCreatedUser(studentAcademicbatch.getCreatedUser());
                    studentAcademicBatchesRepository.save(studentAcademicbatch);
                    //Integer toDateUpdate=studentAcademicBatchesRepository.updateToDateForDetainRecord(studentDetainRequestDTO.getSchoolId(), studentDetainRequestDTO.getStudentId(),toDateTime);

                    /*  Long count = studentDetailRepository.updateStudentStatusAndDate(studentDetainRequestDTO.getStudentStatusId(),studentDetainRequestDTO.getSchoolId(),studentDetainRequestDTO.getStudentId(),studentDetainRequestDTO.getReason());*/

                    StudentDetail studentDetail = studentDetailRepository.findByStudentId(studentPassedOutDTO.getStudentId());
                    GeneralDetail studentStatus1 = new GeneralDetail();
                    studentStatus1.setGeneralDetailId(studentPassedOutDTO.getStudentStatusId());
                    studentDetail.setStudentStatus(studentStatus);
                    studentDetail.setUpdatedDt(new Date());
                    studentDetailRepository.save(studentDetail);
                    flag = flag + 1;
                }


            }


        } catch (Exception e) {
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }
        if (flag > 0) {
            response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), HttpStatus.OK.value());
        }
        return response;
    }

    @Override
    public ApiResponse<List<StudentDetailDTO>> addStudentsList(List<StudentDetailDTO> studentDetailDTO) {
        ApiResponse<List<StudentDetailDTO>> response = null;

        LOGGER.info("StudentDetailServiceImpl.addStudentsList()");

        try {
            List<StudentDetail> studentDetailList = studentDetailMapper.convertStudentDetailDTOToStudentDetail(studentDetailDTO);
            for (StudentDetail studentDetail : studentDetailList) {
                studentDetail = studentDetailRepository.save(studentDetail);
                //update Student Academic batches too
                List<StudentAcademicbatch> studentAcademicbatches = studentAcademicBatchesRepository.findByStudentId(studentDetail.getStudentId());
                for (StudentAcademicbatch studentAcademicbatch : studentAcademicbatches) {
                    studentAcademicBatchesRepository.save(studentAcademicbatch);
                }
            }
            response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(),
                    studentDetailMapper.convertStudentDetailToStudentDetailDTO(studentDetailList), HttpStatus.OK.value());
        } catch (Exception e) {
            LOGGER.error("StudentDetailServiceImpl.addStudentsList()" + e);
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }
}


