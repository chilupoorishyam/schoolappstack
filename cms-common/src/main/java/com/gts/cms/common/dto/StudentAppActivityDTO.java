package com.gts.cms.common.dto;

import java.util.Date;

public class StudentAppActivityDTO {
	private Long studentAppActivityId;

	private Long createdUser;

	private String level;

	private String particulars;
	private String sponsoredBy;

	private Date updatedDt;

	private Long updatedUser;
	
	private Boolean isActive;

	public Long getStudentAppActivityId() {
		return studentAppActivityId;
	}

	public void setStudentAppActivityId(Long studentAppActivityId) {
		this.studentAppActivityId = studentAppActivityId;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getParticulars() {
		return particulars;
	}

	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}

	public String getSponsoredBy() {
		return sponsoredBy;
	}

	public void setSponsoredBy(String sponsoredBy) {
		this.sponsoredBy = sponsoredBy;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}
