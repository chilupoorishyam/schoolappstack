package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.StudentCategoryRepositoryCustom;
import com.gts.cms.entity.StudentCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * created by Naveen(Auto) on 02/09/2018
 * 
 */
@Repository
public interface StudentCategoryRepository extends JpaRepository<StudentCategory, Long>, StudentCategoryRepositoryCustom {


}
