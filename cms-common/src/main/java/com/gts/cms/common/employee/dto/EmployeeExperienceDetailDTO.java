package com.gts.cms.common.employee.dto;

import java.util.Date;

/**
 * @author Genesis
 *
 */
public class EmployeeExperienceDetailDTO {

	private static final long serialVersionUID = 1L;

	private Long empExperiencedetailId;

	private Date createdDt;

	private Long createdUser;

	private String designation;

	private String experienceDetail;

	private Long experienceMonth;

	private Long experienceYear;

	private Date fromYrMonth;

	private Boolean isActive;

	private String prevoiusInstitutions;

	private String reason;

	private String subjects;

	private Date toYrMonth;

	private Long empId;

	private Long organizationId;

	public Long getEmpExperiencedetailId() {
		return empExperiencedetailId;
	}

	public void setEmpExperiencedetailId(Long empExperiencedetailId) {
		this.empExperiencedetailId = empExperiencedetailId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getExperienceDetail() {
		return experienceDetail;
	}

	public void setExperienceDetail(String experienceDetail) {
		this.experienceDetail = experienceDetail;
	}

	public Long getExperienceMonth() {
		return experienceMonth;
	}

	public void setExperienceMonth(Long experienceMonth) {
		this.experienceMonth = experienceMonth;
	}

	public Long getExperienceYear() {
		return experienceYear;
	}

	public void setExperienceYear(Long experienceYear) {
		this.experienceYear = experienceYear;
	}

	public Date getFromYrMonth() {
		return fromYrMonth;
	}

	public void setFromYrMonth(Date fromYrMonth) {
		this.fromYrMonth = fromYrMonth;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getPrevoiusInstitutions() {
		return prevoiusInstitutions;
	}

	public void setPrevoiusInstitutions(String prevoiusInstitutions) {
		this.prevoiusInstitutions = prevoiusInstitutions;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSubjects() {
		return subjects;
	}

	public void setSubjects(String subjects) {
		this.subjects = subjects;
	}

	public Date getToYrMonth() {
		return toYrMonth;
	}

	public void setToYrMonth(Date toYrMonth) {
		this.toYrMonth = toYrMonth;
	}

	public Long getEmpId() {
		return empId;
	}

	public void setEmpId(Long empId) {
		this.empId = empId;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

}
