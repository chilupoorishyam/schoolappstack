package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.SmsPatternRepositoryCustom;
import com.gts.cms.entity.QSmsPattern;
import com.gts.cms.entity.SmsPattern;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Genesis
 *
 */
@Repository
public class SmsPatternRepositoryImpl extends QuerydslRepositorySupport implements SmsPatternRepositoryCustom {

	public SmsPatternRepositoryImpl() {
		super(SmsPatternRepositoryImpl.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public SmsPattern getSmsPatternByCode(Long schoolId, String patternCode) {
		JPQLQuery<SmsPattern> query = from(QSmsPattern.smsPattern)
				.where(QSmsPattern.smsPattern.school.schoolId.eq(schoolId), QSmsPattern.smsPattern.messagepatternfor.eq(patternCode).and(QSmsPattern.smsPattern.isActive.eq(true).and(QSmsPattern.smsPattern.isSmsAlert.eq(true))));
		return query.fetchOne();
	}
	
	@Override
	public SmsPattern getEmailPatternByCode(Long schoolId,String patternCode) {
		JPQLQuery<SmsPattern> query = from(QSmsPattern.smsPattern)
				.where(QSmsPattern.smsPattern.school.schoolId.eq(schoolId), QSmsPattern.smsPattern.messagepatternfor.eq(patternCode).and(QSmsPattern.smsPattern.isActive.eq(true).and(QSmsPattern.smsPattern.isEmailAlert.eq(true))));
		return query.fetchOne();
	}

	@Override
	public SmsPattern getSmsPatternByCode(String patternCode) {
		JPQLQuery<SmsPattern> query = from(QSmsPattern.smsPattern)
				.where(QSmsPattern.smsPattern.messagepatternfor.eq(patternCode).and(QSmsPattern.smsPattern.isActive.eq(true)));
		return query.fetchOne();
	}
	
	
	 @Override
	public SmsPattern getSmsPatternByCodeAndClgId(String patternCode,Long schoolId) {
		JPQLQuery<SmsPattern> query = from(QSmsPattern.smsPattern)
				.where(QSmsPattern.smsPattern.messagepatternfor.eq(patternCode).and(QSmsPattern.smsPattern.school.schoolId.eq(schoolId).and(QSmsPattern.smsPattern.isActive.eq(true))));
		return query.fetchOne();
	}
	 
	/*public SmsPattern getSmsPatternByCode(String patternCode,Long schoolId) {
		JPQLQuery<SmsPattern> query = null;
		query= from(smsPattern);
		if(patternCode!=null) {
			query.where(smsPattern.messagepatternfor.eq(patternCode));
		}
		if(schoolId!=null) {
			query.where(smsPattern.school.schoolId.eq(schoolId));
		}
		return query.fetchOne();
	}
	*/
}
