package com.gts.cms.common.exception;

/**
 * The Class UnknownException.
 */
public class UnknownException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new unknown exception.
	 */
	public UnknownException() {
		super();
	}

	/**
	 * Instantiates a new unknown exception.
	 *
	 * @param s the s
	 */
	public UnknownException(String s) {
		super(s);
	}

	/**
	 * Instantiates a new unknown exception.
	 *
	 * @param t the t
	 */
	public UnknownException(Throwable t) {
		super(t);
	}

}
