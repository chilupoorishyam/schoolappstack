package com.gts.cms.common.student.repository.custom.impl;

import com.gts.cms.common.student.repository.custom.StudentMedicalCertificateRepositoryCustom;
import com.gts.cms.entity.StudentMedicalCertificate;
import com.querydsl.core.dml.UpdateClause;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAUpdateClause;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.gts.cms.entity.QStudentMedicalCertificate.studentMedicalCertificate;

/**
 * @author Genesis
 *
 */
@Repository
public class StudentMedicalCertificateRepositoryImpl extends QuerydslRepositorySupport
		implements StudentMedicalCertificateRepositoryCustom {

	public StudentMedicalCertificateRepositoryImpl() {
		super(StudentMedicalCertificateRepositoryImpl.class);
	}

	@Override
	public List<StudentMedicalCertificate> findAllEnquiryForms(Long schoolId) {
		JPQLQuery<StudentMedicalCertificate> query = null;
		query = from(studentMedicalCertificate);

		query.where(studentMedicalCertificate.isActive.eq(Boolean.TRUE));

		if (schoolId != null) {
			query.where(studentMedicalCertificate.school.schoolId.eq(schoolId));
		}
		query.orderBy(studentMedicalCertificate.createdDt.asc());
		return query.fetch();
	}

	@Override
	public List<StudentMedicalCertificate> findAllEnquiryForms(Long schoolId, Long academicYearId, Long courseYearId,
			Long groupSectionId, Long studentId, Boolean status) {
		JPQLQuery<StudentMedicalCertificate> query = null;
		query = from(studentMedicalCertificate);

		if (status != null) {
			query.where(studentMedicalCertificate.isActive.eq(status));
		} else {
			query.where(studentMedicalCertificate.isActive.eq(Boolean.TRUE));
		}
		if (schoolId != null) {
			query.where(studentMedicalCertificate.school.schoolId.eq(schoolId));
		}
		if (academicYearId != null) {
			query.where(studentMedicalCertificate.academicYear.academicYearId.eq(academicYearId));
		}
		if (courseYearId != null) {
			query.where(studentMedicalCertificate.courseYear.courseYearId.eq(courseYearId));
		}
		if (groupSectionId != null) {
			query.where(studentMedicalCertificate.groupSection.groupSectionId.eq(groupSectionId));
		}
		if (studentId != null) {
			query.where(studentMedicalCertificate.studentDetail.studentId.eq(studentId));
		}
		query.orderBy(studentMedicalCertificate.createdDt.asc());

		return query.fetch();
	}

	@Override
	public Long uploadMedicalCertificateFiles(Long studentMedicalCertificateId, String fileName1, String fileName2,
			String fileName3) {
		UpdateClause<JPAUpdateClause> query = update(studentMedicalCertificate)
				.where(studentMedicalCertificate.studentMedicalCertificateId.eq(studentMedicalCertificateId));
		if (fileName1 != null) {
			query.set(studentMedicalCertificate.filepath1, fileName1);
		}
		if (fileName2 != null) {
			query.set(studentMedicalCertificate.filepath2, fileName2);
		}
		if (fileName3 != null) {
			query.set(studentMedicalCertificate.filepath3, fileName3);
		}
		return query.execute();
	}
}
