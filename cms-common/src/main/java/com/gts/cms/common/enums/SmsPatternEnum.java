package com.gts.cms.common.enums;

public enum SmsPatternEnum {
	STAFF_SMS("Staff_SMS"), 
	ADMIN_SMS("Admin_SMS"),
	STUDENT_SMS("Student_SMS"),
	FEEDUEREMINDER_SMS("FeeDueReminder_SMS"),
	OTHERS_SMS("Others_SMS"),
	SMS_USER_TYPE_STUDENT("STUDENT"),
	SMS_USER_TYPE_ADMIN("ADMIN"),
	SMS_USER_TYPE_STAFF("STAFF"),
	SMS_LOGIN_SMS("Login_SMS");
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	private SmsPatternEnum(String value) {
		this.value = value;
	}

}
