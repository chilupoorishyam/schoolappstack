package com.gts.cms.common.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;

public class HelperDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long helperId;
	private Date dateOfBirth;
	private Date dateOfJoining;
	private String emailId;
	private BigDecimal experience;
	private String helperName;
	@NotNull(message = "IsActive is required.")
	private Boolean isActive;
	private String mobileNumber;
	private String permanentAddress;
	private String phone;
	private String presentAddress;
	private String reason;
	private Long genderId;
	private String genderDisplayName;
	private String genderCode;
	private Long maritalStatusId;
	private String maritalStatusDisplayName;
	private String maritalStatusCode;
	private Long bloodgroupId;
	private String bloodgroupDisplayName;
	private String bloodgroupCode;
	private Long organizationId;
	private String orgName;
	private Long transportDetailId;
	private Date createdDt;
	private Long createdUser;
	private Date updatedDt;
	private Long updatedUser;

	public void setHelperId(Long helperId) {
		this.helperId = helperId;
	}

	public Long getHelperId() {
		return helperId;
	}
	
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfJoining(Date dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}

	public Date getDateOfJoining() {
		return dateOfJoining;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setExperience(BigDecimal experience) {
		this.experience = experience;
	}

	public BigDecimal getExperience() {
		return experience;
	}

	public void setHelperName(String helperName) {
		this.helperName = helperName;
	}

	public String getHelperName() {
		return helperName;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone() {
		return phone;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReason() {
		return reason;
	}

	
	public void setGenderId(Long genderId) {
		this.genderId = genderId;
	}

	public Long getGenderId() {
		return genderId;
	}

	public void setGenderDisplayName(String genderDisplayName) {
		this.genderDisplayName = genderDisplayName;
	}

	public String getGenderDisplayName() {
		return genderDisplayName;
	}

	public void setGenderCode(String genderCode) {
		this.genderCode = genderCode;
	}

	public String getGenderCode() {
		return genderCode;
	}

	public void setMaritalStatusId(Long maritalStatusId) {
		this.maritalStatusId = maritalStatusId;
	}

	public Long getMaritalStatusId() {
		return maritalStatusId;
	}

	public void setMaritalStatusDisplayName(String maritalStatusDisplayName) {
		this.maritalStatusDisplayName = maritalStatusDisplayName;
	}

	public String getMaritalStatusDisplayName() {
		return maritalStatusDisplayName;
	}

	public void setMaritalStatusCode(String maritalStatusCode) {
		this.maritalStatusCode = maritalStatusCode;
	}

	public String getMaritalStatusCode() {
		return maritalStatusCode;
	}

	public void setBloodgroupId(Long bloodgroupId) {
		this.bloodgroupId = bloodgroupId;
	}

	public Long getBloodgroupId() {
		return bloodgroupId;
	}

	public void setBloodgroupDisplayName(String bloodgroupDisplayName) {
		this.bloodgroupDisplayName = bloodgroupDisplayName;
	}

	public String getBloodgroupDisplayName() {
		return bloodgroupDisplayName;
	}

	public void setBloodgroupCode(String bloodgroupCode) {
		this.bloodgroupCode = bloodgroupCode;
	}

	public String getBloodgroupCode() {
		return bloodgroupCode;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public Long getOrganizationId() {
		return organizationId;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setTransportDetailId(Long transportDetailId) {
		this.transportDetailId = transportDetailId;
	}

	public Long getTransportDetailId() {
		return transportDetailId;
	}
	public Date getCreatedDt() {
	      return createdDt;
	}
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	public Long getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}
	public Date getUpdatedDt() {
		return updatedDt;
	}
	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}
	public Long getUpdatedUser() {
		return updatedUser;
	}
	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

}
