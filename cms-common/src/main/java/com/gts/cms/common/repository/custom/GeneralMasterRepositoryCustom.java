package com.gts.cms.common.repository.custom;

import com.gts.cms.entity.GeneralMaster;

import java.util.List;


public interface GeneralMasterRepositoryCustom {

	public GeneralMaster findByGeneralMasterIdAndIsActiveTrue(Long generalMasterId);

	public List<GeneralMaster> findByIsActiveTrue();

	public Long deleteGeneralMaster(Long generalMasterId);

}
