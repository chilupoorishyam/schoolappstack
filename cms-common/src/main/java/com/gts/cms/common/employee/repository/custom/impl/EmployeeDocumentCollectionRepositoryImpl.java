package com.gts.cms.common.employee.repository.custom.impl;

import com.gts.cms.common.employee.repository.custom.EmployeeDocumentCollectionRepositoryCustom;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static com.gts.cms.entity.QEmployeeDocumentCollection.employeeDocumentCollection;

/**
 * @author Genesis
 *
 */
@Repository
public class EmployeeDocumentCollectionRepositoryImpl extends QuerydslRepositorySupport
		implements EmployeeDocumentCollectionRepositoryCustom {

	public EmployeeDocumentCollectionRepositoryImpl() {
		super(EmployeeDocumentCollectionRepositoryImpl.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public Long uploadEmployeeDocumentFiles(Long employeeId, String docRepId, String docName) {
		Long count=0L;
		update(employeeDocumentCollection)
				.where(employeeDocumentCollection.employeeDetail.employeeId.eq(employeeId)
						.and(employeeDocumentCollection.employeeDocCollId
								.eq(Long.parseLong(docRepId))))
				.set(employeeDocumentCollection.filePath, docName).execute();
		count=count+1;
		return  count;
	}

}
