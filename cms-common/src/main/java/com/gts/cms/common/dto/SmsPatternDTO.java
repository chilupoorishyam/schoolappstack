package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;


public class SmsPatternDTO implements Serializable {
private static final long serialVersionUID = 1L;
private Long messagePatternId;
private Date createdDt;
private Long createdUser;
private Boolean isActive;
private Boolean isEmailAlert;
private Boolean isSmsAlert;
private String messageContent;
private String messageContentHtml;
private String messageSignature;
private String messageSubject;
private String messagepatternfor;
private String reason;
private String senderCode;
private String smsTextFormat;
private Date updatedDt;
private Long updatedUser;
private Long schoolId;
private String schoolCode;
private String schoolName;

public void setMessagePatternId(Long messagePatternId) {
 this.messagePatternId = messagePatternId;
}
public Long getMessagePatternId(){
 return messagePatternId;
}
public void setCreatedDt(Date createdDt) {
 this.createdDt = createdDt;
}
public Date getCreatedDt(){
 return createdDt;
}
public void setCreatedUser(Long createdUser) {
 this.createdUser = createdUser;
}
public Long getCreatedUser(){
 return createdUser;
}
public void setIsActive(Boolean isActive) {
 this.isActive = isActive;
}
public Boolean getIsActive(){
 return isActive;
}
public void setIsEmailAlert(Boolean isEmailAlert) {
 this.isEmailAlert = isEmailAlert;
}
public Boolean getIsEmailAlert(){
 return isEmailAlert;
}
public void setIsSmsAlert(Boolean isSmsAlert) {
 this.isSmsAlert = isSmsAlert;
}
public Boolean getIsSmsAlert(){
 return isSmsAlert;
}
public void setMessageContent(String messageContent) {
 this.messageContent = messageContent;
}
public String getMessageContent(){
 return messageContent;
}
public void setMessageContentHtml(String messageContentHtml) {
 this.messageContentHtml = messageContentHtml;
}
public String getMessageContentHtml(){
 return messageContentHtml;
}
public void setMessageSignature(String messageSignature) {
 this.messageSignature = messageSignature;
}
public String getMessageSignature(){
 return messageSignature;
}
public void setMessageSubject(String messageSubject) {
 this.messageSubject = messageSubject;
}
public String getMessageSubject(){
 return messageSubject;
}
public void setMessagepatternfor(String messagepatternfor) {
 this.messagepatternfor = messagepatternfor;
}
public String getMessagepatternfor(){
 return messagepatternfor;
}
public void setReason(String reason) {
 this.reason = reason;
}
public String getReason(){
 return reason;
}
public void setSenderCode(String senderCode) {
 this.senderCode = senderCode;
}
public String getSenderCode(){
 return senderCode;
}
public void setSmsTextFormat(String smsTextFormat) {
 this.smsTextFormat = smsTextFormat;
}
public String getSmsTextFormat(){
 return smsTextFormat;
}
public void setUpdatedDt(Date updatedDt) {
 this.updatedDt = updatedDt;
}
public Date getUpdatedDt(){
 return updatedDt;
}
public void setUpdatedUser(Long updatedUser) {
 this.updatedUser = updatedUser;
}
public Long getUpdatedUser(){
 return updatedUser;
}
public void setSchoolId(Long schoolId) {
 this.schoolId = schoolId;
}
public Long getSchoolId(){
 return schoolId;
}
public void setSchoolCode(String schoolCode) {
 this.schoolCode = schoolCode;
}
public String getSchoolCode(){
 return schoolCode;
}
public void setSchoolName(String schoolName) {
 this.schoolName = schoolName;
}
public String getSchoolName(){
 return schoolName;
}
}