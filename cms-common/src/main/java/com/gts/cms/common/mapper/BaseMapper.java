package com.gts.cms.common.mapper;

import java.util.List;

public interface BaseMapper<E, D> {
	public D convertEntityToDTO(E e);

	public List<D> convertEntityListToDTOList(List<E> entityList);

	public List<E> convertDTOListToEntityList(List<D> dtoList);

	public E convertDTOtoEntity(D d, E e);
	
}
