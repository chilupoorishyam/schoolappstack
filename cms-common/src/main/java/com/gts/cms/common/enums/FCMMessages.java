package com.gts.cms.common.enums;

public enum FCMMessages {
	TITLE_LEAVE("Leave Notification"),
	STUDENT_ABSENT("Student Absent"),
	MSG_LEAVE_RECOMMENDED("Leave Recommended!"),
	MSG_LEAVE_APPLIED("Leave Applied!"),
	MSG_LEAVE_APPROVED("Leave Approved!"),
	MSG_LEAVE_REJECTED("Leave Rejected!"),
	MSG_LEAVE_CANCEL("Leave Canceled!"),
	TITLE_WLA("Workload Notification"),
	WLA_ACCEPT("Workload Accepted!"),
	WLA_REJECT("Workload Rejected!"),
	NO_DUE_APPLIED("No Due certificate Applied!"),
	MARK_ATTENDANCE("Attendance Not Marked!");
	private String value;

	private FCMMessages(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
