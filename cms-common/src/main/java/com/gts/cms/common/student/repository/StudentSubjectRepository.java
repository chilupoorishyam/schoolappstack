package com.gts.cms.common.student.repository;

import com.gts.cms.entity.StudentDetail;
import com.gts.cms.common.student.repository.custom.StudentSubjectRepositoryCustom;
import com.gts.cms.entity.StudentSubject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Genesis
 *
 */
@Repository
public interface StudentSubjectRepository extends JpaRepository<StudentSubject, Long>, StudentSubjectRepositoryCustom {


	
	@Query("SELECT ss FROM StudentSubject ss "
			+ " INNER JOIN StudentDetail sd "
			+ " ON sd.studentId=ss.studentDetail.studentId "
			+ " WHERE 1=1"
			+ " AND(:studentStatusCode is null or sd.studentStatus.generalDetailCode=:studentStatusCode) "
			+ " AND (:schoolId is null or ss.school.schoolId=:schoolId)"
			+ " AND (:academicYearId is null or ss.academicYear.academicYearId=:academicYearId)"
			+ " AND (:courseYearId is null or ss.courseYear.courseYearId=:courseYearId)"
			+ " AND (:subjectId is null or ss.subject.subjectId=:subjectId)"
			+ " AND (ss.isActive=true) "
			)
	List<StudentSubject> getStudentSubject(@Param("schoolId") Long schoolId,
		       @Param("academicYearId") Long academicYearId,
			   @Param("courseYearId") Long courseYearId,
			   @Param("subjectId") Long subjectId,
			   @Param("studentStatusCode") String studentStatusCode);
	
	@Query("SELECT DISTINCT sd FROM  StudentSubject ss "
			+ " INNER JOIN StudentDetail sd "
			+ " ON sd.studentId = ss.studentDetail.studentId "
			+ " AND sd.isActive=true "
			+ " AND ss.isActive=true "
			+ " WHERE 1=1"
			+ " AND(:schoolId is null or ss.school.schoolId=:schoolId) "
			+ " AND(:academicYearId is null or ss.academicYear.academicYearId=:academicYearId) "
			+ " AND(:courseId is null or ss.course.courseId=:courseId) "
			+ " AND(:courseYearId is null or sd.courseYear.courseYearId=:courseYearId) "
			+ " AND(sd.studentStatus.generalDetailCode=:studentStatusCode) "
			+ " AND(:subjectId is null or ss.subject.subjectId=:subjectId)"
			+ " AND(:subjectTypeId is null or ss.subjectType.generalDetailId=:subjectTypeId)"
			)
	List<StudentDetail> fetchExamSubjectStudents(@Param("schoolId") Long schoolId, 
												 @Param("academicYearId") Long academicYearId,
												 @Param("courseId") Long courseId, 
												 @Param("courseYearId")Long courseYearId, 
												 @Param("studentStatusCode") String studentStatusCode,
												 @Param("subjectId") Long subjectId,
												 @Param("subjectTypeId") Long subjectTypeId);

}


