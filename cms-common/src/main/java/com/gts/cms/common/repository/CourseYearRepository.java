package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.CourseYearRepositoryCustom;
import com.gts.cms.entity.CourseYear;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * created by Naveen(Auto) on 02/09/2018
 * 
 */
@Repository
public interface CourseYearRepository extends JpaRepository<CourseYear, Long>, CourseYearRepositoryCustom {
	
	@Query("SELECT cy FROM CourseYear cy"
			//+ " JOIN FETCH cg.course course JOIN FETCH course.courseYears cy "
			+ " JOIN FETCH cy.groupSections gs"
			+ " JOIN FETCH cy.subjectregulations sr"
			//+ " JOIN gs.academicYear a WITH a.academicYearId = :academicyearid "
			+ " WHERE cy.courseYearId = :courseYearId "
			+ " AND cy.school.schoolId = :schoolId "
			+ " AND cy.isActive = :isActive"
			+ " AND gs.academicYear.academicYearId = :academicYearId"
			+ " AND sr.academicYear.academicYearId = :academicYearId"

			)
	CourseYear findBySchoolIdCourseYearIdAndIsActive(@Param("isActive")Boolean isActive,
			@Param("schoolId") Long schoolId,
			@Param("courseYearId") Long courseYearId,
			@Param("academicYearId") Long academicYearId);

	
	@Query("SELECT c.courseYearName FROM  CourseYear c"
			+ " WHERE c.courseYearId IN (:courseYearIdList)"
			+ " AND c.isActive = true"
			)

	List<String> findCourseYearDetails(@Param("courseYearIdList") List<Long> courseYearIdList);
	
	@Query("SELECT DISTINCT cy FROM CourseYear cy "
			+ " JOIN  cy.course c "
			+ " JOIN  c.subjects s"
			+ " WHERE 1=1 "
			+ " AND cy.school.schoolId = :schoolId "
			+ " AND cy.isActive is true "
			+ " AND s.subjectId = :subjectId"
			)
	/*List<CourseYear> getCourseYearsBySubject(@Param("schoolId") Long schoolId);*/
	List<CourseYear> getCourseYearsBySubject(@Param("schoolId") Long schoolId,
			@Param("subjectId") Long subjectId);


	
	
	/*public List<CourseYear> findByIsActiveTrue();

	public Optional<CourseYear> findByCourseYearIdAndIsActiveTrue(Long courseYearId);*/

	@Query("SELECT c.courseYearName FROM  CourseYear c" 
			+ " WHERE c.courseYearId IN (:courseYearId)"
			+ " AND c.isActive = true"
			)

	String findCourseYearName(@Param("courseYearId") Long courseYearId );

	@Query("SELECT DISTINCT cy FROM CourseYear cy "
			+ " WHERE 1=1 "
			+ " AND cy.school.schoolId = :schoolId "
			+ " AND cy.course.courseId = :courseId "
			+ " AND (:courseYearCode is null or upper(cy.courseYearCode)=upper(:courseYearCode)) "
			+ " AND cy.isActive is true "
	)
	List<CourseYear> getCourseYearsByName(@Param("schoolId") Long schoolId,
										  @Param("courseId") Long courseId,
										  @Param("courseYearCode") String courseYearCode);
	
}
