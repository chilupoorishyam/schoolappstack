package com.gts.cms.common.student.repository.custom.impl;

import com.gts.cms.common.student.repository.custom.StudentAppDocumentCollectionRepositoryCustom;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static com.gts.cms.entity.QStudentAppDocCollection.studentAppDocCollection;
@Repository
public  class StudentAppDocumentCollectionRepositoryImpl extends QuerydslRepositorySupport implements StudentAppDocumentCollectionRepositoryCustom{
	public StudentAppDocumentCollectionRepositoryImpl() {
		super(StudentAppDocumentCollectionRepositoryImpl.class);
	}
	@PersistenceContext
	private EntityManager em;
	@Override
	public Long updateStudentDocumentFiles(Long applicationId, String docRepId, String docPath) {
		Long docRepId1 = Long.parseLong(docRepId);
		return update(studentAppDocCollection)
				.where(studentAppDocCollection.studentApplication.studentAppId.eq(applicationId)
						.and(studentAppDocCollection.appDocCollId.eq(docRepId1)))
				.set(studentAppDocCollection.filePath, docPath).execute();
	}

	

	
}
