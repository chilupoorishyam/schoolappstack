package com.gts.cms.common.dto;

import java.io.Serializable;

public class DistrictDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long districtId;
	private String districtName;

	private String districtCode;

	private Long stateId;
	private String stateName;
	

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public Long getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Long districtId) {
		this.districtId = districtId;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getDistrictCode() {
		return districtCode;
	}

	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

	/*
	 * public List<StateDTO> getStates() { return states; }
	 * 
	 * public void setStates(List<StateDTO> states) { this.states = states; }
	 */

}
