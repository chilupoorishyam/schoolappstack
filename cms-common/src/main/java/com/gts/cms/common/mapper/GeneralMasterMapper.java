package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.GeneralDetailDTO;
import com.gts.cms.common.dto.GeneralMasterDTO;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.GeneralDetail;
import com.gts.cms.entity.GeneralMaster;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class GeneralMasterMapper implements BaseMapper<GeneralMaster, GeneralMasterDTO> {

	@Autowired
	GeneralDetailMapper generalDetailMapper;

	public GeneralMasterDTO convertEntityToDTO(GeneralMaster generalMaster) {
		GeneralMasterDTO generalMasterDTO = null;
		if (generalMaster != null) {
			generalMasterDTO = new GeneralMasterDTO();
			generalMasterDTO.setGeneralMasterId(generalMaster.getGeneralMasterId());
			generalMasterDTO.setGeneralMasterDisplayName(generalMaster.getGeneralMasterDisplayName());
			generalMasterDTO.setGeneralMasterDescription(generalMaster.getGeneralMasterDescription());
			generalMasterDTO.setGeneralMasterCode(generalMaster.getGeneralMasterCode());
			generalMasterDTO.setCreatedDt(generalMaster.getCreatedDt());
			generalMasterDTO.setCreatedUser(generalMaster.getCreatedUser());
			generalMasterDTO.setIsActive(generalMaster.getIsActive());
			generalMasterDTO.setIsEditable(generalMaster.getIsEditable());
			generalMasterDTO.setReason(generalMaster.getReason());

			generalMasterDTO.setGeneralDetailDTOList(
					generalDetailMapper.convertEntityListToDTOList(generalMaster.getGeneralDetails()));
		}
		return generalMasterDTO;
	}

	public GeneralMaster convertDTOtoEntity(GeneralMasterDTO generalMasterDTO, GeneralMaster generalMaster) {
		if (null == generalMaster) {
			generalMaster = new GeneralMaster();
			generalMaster.setIsActive(Boolean.TRUE);
			generalMaster.setCreatedDt(new Date());
			generalMaster.setCreatedUser(SecurityUtil.getCurrentUser());
		} else {
			generalMaster.setUpdatedDt(new Date());
			generalMaster.setUpdatedUser(SecurityUtil.getCurrentUser());
		}
		generalMaster.setGeneralMasterId(generalMasterDTO.getGeneralMasterId());
		generalMaster.setGeneralMasterDisplayName(generalMasterDTO.getGeneralMasterDisplayName());
		generalMaster.setGeneralMasterDescription(generalMasterDTO.getGeneralMasterDescription());
		generalMaster.setGeneralMasterCode(generalMasterDTO.getGeneralMasterCode());
		generalMaster.setIsEditable(generalMasterDTO.getIsEditable());
		generalMaster.setReason(generalMasterDTO.getReason());
		generalMaster.setIsActive(generalMasterDTO.getIsActive());
		setGeneralDetails(generalMasterDTO, generalMaster);

		return generalMaster;
	}

	private void setGeneralDetails(GeneralMasterDTO generalMasterDTO, GeneralMaster generalMaster) {
		if (!CollectionUtils.isEmpty(generalMasterDTO.getGeneralDetailDTOList())) {
			List<GeneralDetail> generalDetailList = new ArrayList<>();
			for (GeneralDetailDTO generalDetailDTO : generalMasterDTO.getGeneralDetailDTOList()) {
				GeneralDetail generalDetail = new GeneralDetail();

				if (generalDetailDTO.getGeneralDetailId() == null) {
					generalDetail.setCreatedDt(new Date());
					generalDetail.setCreatedUser(SecurityUtil.getCurrentUser());
				} else {
					generalDetail.setCreatedDt(generalDetailDTO.getCreatedDt());
				}
				generalDetail.setIsActive(generalDetailDTO.getIsActive());
				if(generalDetail.getIsActive()==null) {
					generalDetail.setIsActive(Boolean.TRUE);
				}
				generalDetail.setUpdatedDt(new Date());
				generalDetail.setUpdatedUser(SecurityUtil.getCurrentUser());
				generalDetail.setGeneralDetailId(generalDetailDTO.getGeneralDetailId());
				generalDetail.setGeneralMaster(generalMaster);
				generalDetail.setGeneralDetailDisplayName(generalDetailDTO.getGeneralDetailDisplayName());
				generalDetail.setGeneralDetailCode(generalDetailDTO.getGeneralDetailCode());
				generalDetail.setGeneralDetailDescription(generalDetailDTO.getGeneralDetaildescription());
				generalDetail.setGeneralDetailSortOrder(generalDetailDTO.getGeneralDetailSortOrder());
				generalDetail.setIsEditable(generalDetailDTO.getIsEditable());
				generalDetail.setReason(generalDetailDTO.getReason());
				generalDetailList.add(generalDetail);
				
			}
			if (!CollectionUtils.isEmpty(generalDetailList)) {
				generalMaster.setGeneralDetails(generalDetailList);
			}
		}
	}

	public List<GeneralMaster> convertDTOListToEntityList(List<GeneralMasterDTO> generalMasterDTOList) {
		List<GeneralMaster> generalMasterList = new ArrayList<>();
		generalMasterDTOList
				.forEach(generalMasterDTO -> generalMasterList.add(convertDTOtoEntity(generalMasterDTO, null)));
		return generalMasterList;

	}

	public List<GeneralMasterDTO> convertEntityListToDTOList(List<GeneralMaster> generalMasterList) {
		List<GeneralMasterDTO> generalMasterDTOList = new ArrayList<>();
		generalMasterList.forEach(generalMaster -> generalMasterDTOList.add(convertEntityToDTO(generalMaster)));
		return generalMasterDTOList;
	}

}
