package com.gts.cms.common.service;

import java.util.List;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.UserRoleDTO;

public interface UserRoleService {

	ApiResponse<Long> addUserRole(List<UserRoleDTO> userRoleDTOs);
}
