package com.gts.cms.common.student.service.impl;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.StudentAcademicbatchDTO;
import com.gts.cms.common.dto.StudentDetainRequestDTO;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.service.report.CommonUtils;
import com.gts.cms.common.student.repository.StudentAcademicBatchesRepository;
import com.gts.cms.common.student.repository.StudentDetailRepository;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.GeneralDetail;
import com.gts.cms.entity.StudentAcademicbatch;
import com.gts.cms.entity.StudentAttendance;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class StudentDetain {
	@Autowired
	StudentDetailRepository studentDetailRepository;
	@Autowired
	StudentAcademicBatchesRepository studentAcademicBatchesRepository;
	/*@Autowired
	BatchwiseStudentRepository batchwiseStudentRepository;
	@Autowired
	StudentAttendanceRepository studentAttendanceRepository;
	@Autowired
	StudentAttendanceMapper studentAttendanceMapper;
	@Autowired
	StudentAcademicbatchMapper studentAcademicbatchMapper;*/
	
	private static final Logger LOGGER = Logger.getLogger(StudentDetain.class);

	@Transactional(rollbackFor = Exception.class)
	public ApiResponse<?> detainStudentDetail(List<StudentDetainRequestDTO> studentDetainRequestDTOs) {
		LOGGER.info("StudentDetain.detainStudentDetail()");
		ApiResponse<?> response = null;
		List<StudentAttendance> studentAttendanceList = null;
		//List<StudentAttendanceDTO> studentAttendanceDTOs=null;
		List<StudentAttendance> studentAttendancesExist = new ArrayList<>();
		List<StudentAcademicbatch> studentAcademicbatchs = null;
		List<StudentAcademicbatchDTO> studentAcademicbatchDTOs = null;
		StudentAcademicbatch studentAcademicbatch = null;
		List<StudentAcademicbatch> stuAcademicbatchsExist = new ArrayList<>();
		Integer updateCount=null;
		Integer value =null;
		int success=0;
		try {
			for (StudentDetainRequestDTO studentDetainRequestDTO : studentDetainRequestDTOs) {
				// for attendance check
				/*studentAttendanceList = studentAttendanceRepository
						.findByAbsentList(studentDetainRequestDTO.getToDate(), studentDetainRequestDTO.getStudentId());*/
				Date date = CommonUtils.getLessThanOneDate(studentDetainRequestDTO.getFromDate());
				// fromDate and toDate exist
				studentAcademicbatchs = studentAcademicBatchesRepository
						.findAlreadyExistWithDate(studentDetainRequestDTO.getStudentId(), date);
				// old record
				studentAcademicbatch = studentAcademicBatchesRepository
						.getDetails(studentDetainRequestDTO.getSchoolId(), studentDetainRequestDTO.getStudentId());
				value = CommonUtils.compareDate(studentAcademicbatch.getFromDate(), date);
				// student academic batch exist
				if (value == -1) {
					stuAcademicbatchsExist.add(studentAcademicbatch);
				}
				// student academic batch exist
				if (!(CollectionUtils.isEmpty(studentAcademicbatchs))) {
					stuAcademicbatchsExist.addAll(studentAcademicbatchs);
				}
				// attendance exist
				if (!(CollectionUtils.isEmpty(studentAttendanceList))) {
					studentAttendancesExist.addAll(studentAttendanceList);
				}

				if (CollectionUtils.isEmpty(studentAttendanceList) && CollectionUtils.isEmpty(studentAcademicbatchs)
						&& studentAcademicbatch != null && ((value == 0) || (value == 1))) {
					Long count = studentDetailRepository.updateStudentStatus(
							studentDetainRequestDTO.getStudentStatusId(), studentDetainRequestDTO.getSchoolId(),
							studentDetainRequestDTO.getStudentId(), studentDetainRequestDTO.getReason(), null, null, null);
					if (count > 0) {
						Date toDate = null;
						if (studentDetainRequestDTO.getToDate() != null) {
							toDate = CommonUtils.getLessThanOneDate(studentDetainRequestDTO.getToDate());
						}

						studentAcademicbatch.setToDate(toDate);
						LOGGER.info("setToDate==>" + toDate);

						studentAcademicbatch.setToGroupSection(null);
						studentAcademicbatch.setReason(studentDetainRequestDTO.getReason());
						studentAcademicbatch.setUpdatedDt(new Date());
						studentAcademicbatch.setUpdatedUser(SecurityUtil.getCurrentUser());
						studentAcademicbatch = studentAcademicBatchesRepository.save(studentAcademicbatch);
						studentAcademicBatchesRepository.flush();

						// new record insert
						StudentAcademicbatch studentAcademicbatchObj = new StudentAcademicbatch();
						studentAcademicbatchObj.setSchool(studentAcademicbatch.getSchool());
						studentAcademicbatchObj.setStudentDetail(studentAcademicbatch.getStudentDetail());
						studentAcademicbatchObj.setCourse(studentAcademicbatch.getCourse());
						studentAcademicbatchObj.setAcademicYear(studentAcademicbatch.getAcademicYear());
						studentAcademicbatchObj.setFromCourseYear(studentAcademicbatch.getFromCourseYear());
						studentAcademicbatchObj.setFromBatch(studentAcademicbatch.getFromBatch());
						studentAcademicbatchObj.setFromGroupSection(studentAcademicbatch.getFromGroupSection());
						if (studentDetainRequestDTO.getStudentStatusId() != null) {
							GeneralDetail studentStatus = new GeneralDetail();
							studentStatus.setGeneralDetailId(studentDetainRequestDTO.getStudentStatusId());
							studentAcademicbatchObj.setStudentStatus(studentStatus);
						}
						studentAcademicbatchObj.setIsPromoted(Boolean.FALSE);
						studentAcademicbatchObj.setIsAlmuni(studentAcademicbatch.getIsAlmuni());
						studentAcademicbatchObj.setIsActive(studentAcademicbatch.getIsActive());
						Date fromDate = null;
						if (studentDetainRequestDTO.getFromDate() != null) {
							fromDate = CommonUtils.getDateAndTime(studentDetainRequestDTO.getFromDate());
						}

						studentAcademicbatchObj.setFromDate(fromDate);
						studentAcademicbatchObj.setToDate(null);
						studentAcademicbatchObj.setCreatedDt(studentAcademicbatch.getCreatedDt());
						studentAcademicbatchObj.setCreatedUser(studentAcademicbatch.getCreatedUser());
						studentAcademicbatchObj.setUpdatedDt(studentAcademicbatch.getUpdatedDt());
						studentAcademicbatchObj.setUpdatedUser(studentAcademicbatch.getUpdatedUser());

						studentAcademicbatchObj.setToBatch(studentAcademicbatch.getFromBatch());
						studentAcademicbatchObj.setToCourseYear(studentAcademicbatch.getFromCourseYear());
						studentAcademicbatchObj.setToGroupSection(null);
						studentAcademicbatchObj.setReason(studentDetainRequestDTO.getReason());
						/*studentAcademicbatchObj.setCourseGroup(studentAcademicbatch.getCourseGroup());
						if (studentDetainRequestDTO.getRegulationId() != null) {
							Regulation regulation = new Regulation();
							regulation.setRegulationId(studentDetainRequestDTO.getRegulationId());
							studentAcademicbatchObj.setRegulation(regulation);
						}*/
						studentAcademicBatchesRepository.save(studentAcademicbatchObj);
						studentAcademicBatchesRepository.flush();

						List<Long> nos = new ArrayList<>();
						nos.add(studentDetainRequestDTO.getStudentId());
						/*updateCount = batchwiseStudentRepository.updateBatchwiseStudents(nos,
								studentDetainRequestDTO.getGroupSectionId(), new Date(), SecurityUtil.getCurrentUser());
						LOGGER.debug("Student batches updated count : " + updateCount);*/
					}
					success+=1;
				}
			}
			//StudentAttendanceAndDates studentAttendanceAndDates=new StudentAttendanceAndDates();
			//studentAttendanceDTOs=studentAttendanceMapper.convertEntityListToDTOList(studentAttendancesExist);
			//studentAcademicbatchDTOs=studentAcademicbatchMapper.convertEntityListToDTOList(stuAcademicbatchsExist);
			//studentAttendanceAndDates.setStudenAttendanceDTOs(studentAttendanceDTOs);
			//studentAttendanceAndDates.setStudentAcademicbatchDTOs(studentAcademicbatchDTOs);
			/*if(success>0) {
				response =new ApiResponse<>(Boolean.TRUE, Messages.UPDATE_SUCCESS.getValue(),studentAttendanceAndDates, HttpStatus.OK.value());
			}
			else {
				response =new ApiResponse<>(Boolean.FALSE, Messages.UPDATE_FAILURE.getValue(),studentAttendanceAndDates, HttpStatus.OK.value());

			}*/
			LOGGER.info("StudentDetain.detainStudentDetail()===>executed");
		} catch (Exception e) {
			LOGGER.error("StudentDetain.detainStudentDetail()" + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;
	}

}
