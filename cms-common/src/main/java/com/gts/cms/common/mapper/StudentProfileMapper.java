package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.StudentProfileDTO;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.*;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class StudentProfileMapper implements BaseMapper<StudentProfile, StudentProfileDTO> {

    private static final Logger LOGGER = Logger.getLogger(StudentProfileMapper.class);


    @Override
    public StudentProfileDTO convertEntityToDTO(StudentProfile studentProfile) {
        StudentProfileDTO dtoObject = null;
        if (studentProfile != null) {
            dtoObject = new StudentProfileDTO();
            dtoObject.setStudentProfileId(studentProfile.getStudentProfileId());
            if (studentProfile.getStudentDetail() != null) {
                dtoObject.setStudentName(studentProfile.getStudentDetail().getFirstName());
                dtoObject.setStudentId(studentProfile.getStudentDetail().getStudentId());
            }
            if (studentProfile.getEventMasterCatdetId() != null) {
                dtoObject.setEventMasterCatdetCode(studentProfile.getEventMasterCatdetId().getGeneralMasterCode());
                dtoObject.setEventMasterCatdetId(studentProfile.getEventMasterCatdetId().getGeneralMasterId());
                dtoObject.setEventMasterCatdetName(studentProfile.getEventMasterCatdetId().getGeneralMasterDisplayName());
            }
            if (studentProfile.getEventTitleCatdetId() != null) {
                dtoObject.setEventTitleCatdetCode(studentProfile.getEventTitleCatdetId().getGeneralDetailCode());
                dtoObject.setEventTitleCatdetId(studentProfile.getEventTitleCatdetId().getGeneralDetailId());
                dtoObject.setEventTitleCatdetName(studentProfile.getEventTitleCatdetId().getGeneralDetailDisplayName());
            }
            if (studentProfile.getCourseYear() != null) {
                dtoObject.setCourseYearId(studentProfile.getCourseYear().getCourseYearId());
                dtoObject.setCourseYearCode(studentProfile.getCourseYear().getCourseYearCode());
                dtoObject.setCourseYearName(studentProfile.getCourseYear().getCourseYearName());
            }
            dtoObject.setCertificate(studentProfile.getCertificate());
            dtoObject.setVideosPhotos(studentProfile.getVideosPhotos());
            dtoObject.setReason(studentProfile.getReason());
            dtoObject.setCreatedDt(studentProfile.getCreatedDt());
            dtoObject.setUpdatedDt(studentProfile.getUpdatedDt());
            dtoObject.setUpdatedUser(studentProfile.getUpdatedUser());
            dtoObject.setIsActive(studentProfile.getIsActive());
            dtoObject.setCreatedUser(studentProfile.getCreatedUser());
        }
        return dtoObject;
    }

    @Override
    public List<StudentProfileDTO> convertEntityListToDTOList(List<StudentProfile> entityList) {
        List<StudentProfileDTO> studentProfileDTOList = new ArrayList<>();
        entityList.forEach(entityObject -> studentProfileDTOList.add(convertEntityToDTO(entityObject)));
        return studentProfileDTOList;
    }

    @Override
    public List<StudentProfile> convertDTOListToEntityList(List<StudentProfileDTO> dtoList) {
        List<StudentProfile> entityList = new ArrayList<>();
        dtoList.forEach(dtoObject -> entityList.add(convertDTOtoEntity(dtoObject, null)));
        return entityList;
    }

    @Override
    public StudentProfile convertDTOtoEntity(StudentProfileDTO studentProfileDTO, StudentProfile studentProfile) {
        if (studentProfileDTO != null) {
            if (studentProfile == null) {
                studentProfile = new StudentProfile();
                studentProfile.setIsActive(Boolean.TRUE);
                studentProfile.setCreatedDt(new Date());
                studentProfile.setCreatedUser(SecurityUtil.getCurrentUser());
            } else {
                studentProfile.setIsActive(studentProfileDTO.getIsActive());
                studentProfile.setCreatedDt(new Date());
                studentProfile.setCreatedUser(SecurityUtil.getCurrentUser());
            }
            if (studentProfileDTO.getStudentId() != null) {
                StudentDetail studentDetail = new StudentDetail();
                studentDetail.setStudentId(studentProfileDTO.getStudentId());
                studentProfile.setStudentDetail(studentDetail);
            }
            if (studentProfileDTO.getEventMasterCatdetId() != null) {
                GeneralMaster generalMaster = new GeneralMaster();
                generalMaster.setGeneralMasterId(studentProfileDTO.getEventMasterCatdetId());
                studentProfile.setEventMasterCatdetId(generalMaster);
            }
            if (studentProfileDTO.getEventTitleCatdetId() != null) {
                GeneralDetail generalDetail = new GeneralDetail();
                generalDetail.setGeneralDetailId(studentProfileDTO.getEventTitleCatdetId());
                studentProfile.setEventTitleCatdetId(generalDetail);
            }
            if (studentProfileDTO.getCourseYearId() != null) {
                CourseYear courseYear = new CourseYear();
                courseYear.setCourseYearId(studentProfileDTO.getCourseYearId());
                studentProfile.setCourseYear(courseYear);
            }
            studentProfile.setCertificate(studentProfileDTO.getCertificate());
            studentProfile.setVideosPhotos(studentProfileDTO.getVideosPhotos());
        }
        return studentProfile;
    }
}