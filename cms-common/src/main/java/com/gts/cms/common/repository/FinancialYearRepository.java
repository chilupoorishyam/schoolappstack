package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.FinancialYearRepositoryCustom;
import com.gts.cms.entity.FinancialYear;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * created by Naveen(Auto) on 02/09/2018
 * 
 */
@Repository
public interface FinancialYearRepository extends JpaRepository<FinancialYear, Long>, FinancialYearRepositoryCustom {

	/*SELECT * FROM `t_m_financial_year`
	 WHERE 1=1
	 AND `fk_school_id`=16
	 AND '2020-03-04' BETWEEN  `from_date` AND `to_date`;*/
	
	@Query("SELECT fy FROM FinancialYear fy "
			+ " WHERE 1=1 "
			+ " AND (:schoolId is null or fy.school.schoolId=:schoolId) "
			+ " AND (:checkDate) BETWEEN fy.fromDate AND fy.toDate "
			+ " AND  fy.isActive = true"
			)
	List<FinancialYear> getFinancialYearDate(@Param("schoolId") Long schoolId,
			                                 @Param("checkDate") Date checkDate);
	
	
}
