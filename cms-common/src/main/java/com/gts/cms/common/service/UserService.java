package com.gts.cms.common.service;

import com.gts.cms.common.dto.*;
import com.gts.cms.entity.PasswordResetToken;
import com.gts.cms.entity.User;

import java.util.List;
import java.util.Set;

public interface UserService {

    ApiResponse<UserDTO> getUserDetails(Long id, Boolean status, Boolean isMobile);

    ApiResponse<?> createUser(UserDTO createUser);

    ApiResponse<List<UserListsDTO>> getUserDetailsbyType(Long userTypeId, String userTypeCode, Integer page, Integer size);

    ApiResponse<Set<PageDTO>> getUserPageDetails(Long userId, Boolean status, Boolean isMobile);

    Object getBuildInfo();

    ApiResponse<?> getcreatingUserForStudents();

    ApiResponse<List<SmsToUsersDTO>> getsmsToLimitUser(Long userTypeId);

    PasswordResetToken createPasswordResetTokenForUser(User user);

    User getUserByPasswordResetToken(PasswordDTO passwordDto);

    void changeUserPassword(User user, String newPassword);

	default ApiResponse<?> searchUserDetails(Long schoolId, Long userTypeId, Boolean status,String query){
		return null;
		
	}
}
