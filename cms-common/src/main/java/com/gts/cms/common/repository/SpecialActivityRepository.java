package com.gts.cms.common.repository;

import com.gts.cms.entity.SpecialActivity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface SpecialActivityRepository extends  JpaRepository<SpecialActivity, Long>{
	

}
