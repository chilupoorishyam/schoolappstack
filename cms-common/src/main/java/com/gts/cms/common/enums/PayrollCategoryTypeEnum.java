package com.gts.cms.common.enums;

public enum PayrollCategoryTypeEnum {
	EARNINGS("E"),
	DEDUCTIONS("D"); 
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	private PayrollCategoryTypeEnum(String value) {
		this.value = value;
	}

}
