package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.BlockRepositoryCustom;
import com.gts.cms.entity.Block;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * created by Naveen(Auto) on 02/09/2018
 * 
 */
@Repository
public interface BlockRepository extends JpaRepository<Block, Long>, BlockRepositoryCustom {


}
