package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.GeneralSettingRepositoryCustom;
import com.gts.cms.entity.GeneralSetting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GeneralSettingRepository extends JpaRepository<GeneralSetting, Long>, GeneralSettingRepositoryCustom {

}
