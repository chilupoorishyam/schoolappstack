package com.gts.cms.common.repository;

import com.gts.cms.entity.DepartmentEventAudience;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentEventAudienceRepository extends JpaRepository<DepartmentEventAudience, Long> {
}