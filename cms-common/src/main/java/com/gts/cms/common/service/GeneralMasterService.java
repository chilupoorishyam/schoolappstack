package com.gts.cms.common.service;

import java.util.List;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.GeneralMasterDTO;

public interface GeneralMasterService {

	ApiResponse<Long> addGeneralMaster(GeneralMasterDTO generalMasterDTO);

	ApiResponse<GeneralMasterDTO> getGeneralMasterDetails(Long generalMasterId);

	ApiResponse<Long> updateGeneralMaster(GeneralMasterDTO generalMasterDTO);

	ApiResponse<Long> deleteGeneralMaster(Long generalMasterId);

	ApiResponse<List<GeneralMasterDTO>> getAllGeneralMasterDetails();

}
