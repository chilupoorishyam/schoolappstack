package com.gts.cms.common.repository;

import com.gts.cms.entity.DepartmentEventPhoto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentEventPhotoRepository extends JpaRepository<DepartmentEventPhoto, Long> {
}