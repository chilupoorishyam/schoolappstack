package com.gts.cms.common.student.mapper;

import com.gts.cms.common.dto.ReferralDetailDTO;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Component
public class ReferralDetailMapper implements BaseMapper<ReferralDetail, ReferralDetailDTO> {
@Override
public ReferralDetailDTO convertEntityToDTO(ReferralDetail referralDetail) {
	ReferralDetailDTO referralDetailDTO = null;
	if (referralDetail != null) {
	referralDetailDTO = new ReferralDetailDTO();
	  referralDetailDTO.setCreatedUser(referralDetail.getCreatedUser());
	  referralDetailDTO.setCreatedDt(referralDetail.getCreatedDt());
	  referralDetailDTO.setUpdatedDt(referralDetail.getUpdatedDt());
	  referralDetailDTO.setUpdatedUser(referralDetail.getUpdatedUser());
	  referralDetailDTO.setIsActive(referralDetail.getIsActive());
	  referralDetailDTO.setNotes(referralDetail.getNotes());
	  referralDetailDTO.setStatusDate(referralDetail.getStatusDate());
	  referralDetailDTO.setPaidAmount(referralDetail.getPaidAmount());
	  referralDetailDTO.setReason(referralDetail.getReason());
	  referralDetailDTO.setIsTransactionSettled(referralDetail.getIsTransactionSettled());
	  referralDetailDTO.setIsstudentadmitted(referralDetail.getIsstudentadmitted());
	  referralDetailDTO.setReferralDetailId(referralDetail.getReferralDetailId());
	  referralDetailDTO.setStatusComments(referralDetail.getStatusComments());
	  referralDetailDTO.setReferralAmount(referralDetail.getReferralAmount());
	  referralDetailDTO.setTransactionCompletedDate(referralDetail.getTransactionCompletedDate());
	  
	  if(referralDetail.getSchool()!= null){
		  referralDetailDTO.setSchoolId(referralDetail.getSchool().getSchoolId());
		  referralDetailDTO.setSchoolCode(referralDetail.getSchool().getSchoolCode());
		  referralDetailDTO.setSchoolName(referralDetail.getSchool().getSchoolName());
	  }
	  
	  if (referralDetail.getAcademicYear() != null) {
		  referralDetailDTO.setAcademicYearId(referralDetail.getAcademicYear().getAcademicYearId());
		  referralDetailDTO.setAcademicYear(referralDetail.getAcademicYear().getAcademicYear());
	  }
	  
	  if (referralDetail.getStudentEnquiry() != null) {
		  referralDetailDTO.setEnquiryId(referralDetail.getStudentEnquiry().getEnquiryId());
	  }
	  
	  if (referralDetail.getAgentdetail() != null) {
		  referralDetailDTO.setAgentdetialId(referralDetail.getAgentdetail().getAgentdetialId());
		  referralDetailDTO.setAgentName(referralDetail.getAgentdetail().getAgentname());
	  }
	  
	  if (referralDetail.getEmployeeDetail() != null) {
		  referralDetailDTO.setEmployeeId(referralDetail.getEmployeeDetail().getEmployeeId());
		  referralDetailDTO.setEmployeeFirstName(referralDetail.getEmployeeDetail().getFirstName());
		  referralDetailDTO.setEmployeeLastName(referralDetail.getEmployeeDetail().getLastName());
		  referralDetailDTO.setEmployeeMiddleName(referralDetail.getEmployeeDetail().getMiddleName());
	  }
	  
	  if(referralDetail.getApprovalStatus()!= null){
		  referralDetailDTO.setApprovalStatusId(referralDetail.getApprovalStatus().getGeneralDetailId());
		  referralDetailDTO.setApprovalStatusCode(referralDetail.getApprovalStatus().getGeneralDetailCode());
		  referralDetailDTO.setApprovalStatusDisplayName(referralDetail.getApprovalStatus().getGeneralDetailDisplayName());
	  }
	  
	  if (referralDetail.getStatusEmployeeDetail() != null) {
		  referralDetailDTO.setStatusEmployeeId(referralDetail.getStatusEmployeeDetail().getEmployeeId());
		  referralDetailDTO.setStatusEmployeeFirstName(referralDetail.getStatusEmployeeDetail().getFirstName());
		  referralDetailDTO.setStatusEmployeeLastName(referralDetail.getStatusEmployeeDetail().getLastName());
		  referralDetailDTO.setStatusEmployeeMiddleName(referralDetail.getStatusEmployeeDetail().getMiddleName());
	  }
	  
	  if (referralDetail.getPaidEmployeeDetail() != null) {
		  referralDetailDTO.setPaidEmployeeId(referralDetail.getPaidEmployeeDetail().getEmployeeId());
		  referralDetailDTO.setPaidEmployeeFirstName(referralDetail.getPaidEmployeeDetail().getFirstName());
		  referralDetailDTO.setPaidEmployeeLastName(referralDetail.getPaidEmployeeDetail().getLastName());
		  referralDetailDTO.setPaidEmployeeMiddleName(referralDetail.getPaidEmployeeDetail().getMiddleName());
	  }
	  
	  if (referralDetail.getFinTransaction()!= null) {
		  referralDetailDTO.setFinTransactionId(referralDetail.getFinTransaction().getFinTransactionId());
	  }
	  
	}
	return referralDetailDTO;
}
@Override
public ReferralDetail convertDTOtoEntity(ReferralDetailDTO referralDetailDTO,ReferralDetail referralDetail) {
	if ( null == referralDetail ) {
	 referralDetail = new ReferralDetail();
	 referralDetail.setIsActive(Status.ACTIVE.getId());
	 referralDetail.setCreatedDt(new Date());
	 referralDetail.setCreatedUser(SecurityUtil.getCurrentUser());
	}else{
	 referralDetail.setIsActive(referralDetailDTO.getIsActive());
	}
	  referralDetail.setUpdatedDt(new Date());
	  referralDetail.setUpdatedUser(SecurityUtil.getCurrentUser());
	  referralDetail.setNotes(referralDetailDTO.getNotes());
	 if(referralDetailDTO.getAgentdetialId()!= null){
		 Agentdetail agentdetail = new Agentdetail();
		 agentdetail.setAgentdetialId(referralDetailDTO.getAgentdetialId());
		 referralDetail.setAgentdetail(agentdetail);
	 }
	  referralDetail.setStatusDate(referralDetailDTO.getStatusDate());
	  referralDetail.setPaidAmount(referralDetailDTO.getPaidAmount());
	 if(referralDetailDTO.getEmployeeId()!= null){
		 EmployeeDetail employeeDetail = new EmployeeDetail();
		 employeeDetail.setEmployeeId(referralDetailDTO.getEmployeeId());
		 referralDetail.setEmployeeDetail(employeeDetail);
	 }
	 if(referralDetailDTO.getSchoolId()!= null){
		 School school = new School();
		 school.setSchoolId(referralDetailDTO.getSchoolId());
		 referralDetail.setSchool(school);
	 }
	  referralDetail.setReason(referralDetailDTO.getReason());
	 if(referralDetailDTO.getAcademicYearId()!= null){
		 AcademicYear academicYear = new AcademicYear();
		 academicYear.setAcademicYearId(referralDetailDTO.getAcademicYearId());
		 referralDetail.setAcademicYear(academicYear);
	 }
	  referralDetail.setIsTransactionSettled(referralDetailDTO.getIsTransactionSettled());
	  referralDetail.setIsstudentadmitted(referralDetailDTO.getIsstudentadmitted());
	  referralDetail.setReferralDetailId(referralDetailDTO.getReferralDetailId());
	 if(referralDetailDTO.getApprovalStatusId()!= null){
		 GeneralDetail generalDetail = new GeneralDetail();
		 generalDetail.setGeneralDetailId(referralDetailDTO.getApprovalStatusId());
		 referralDetail.setApprovalStatus(generalDetail);
	 }
	 if(referralDetailDTO.getStatusEmployeeId()!= null){
		 EmployeeDetail employeeDetail = new EmployeeDetail();
		 employeeDetail.setEmployeeId(referralDetailDTO.getStatusEmployeeId());
		 referralDetail.setStatusEmployeeDetail(employeeDetail);
	 }
	 if(referralDetailDTO.getEnquiryId()!= null){
		 StudentEnquiry studentEnquiry = new StudentEnquiry();
		 studentEnquiry.setEnquiryId(referralDetailDTO.getEnquiryId());
		 referralDetail.setStudentEnquiry(studentEnquiry);
	 }
	  referralDetail.setStatusComments(referralDetailDTO.getStatusComments());
	 if(referralDetailDTO.getPaidEmployeeId()!= null){
		 EmployeeDetail employeeDetail = new EmployeeDetail();
		 employeeDetail.setEmployeeId(referralDetailDTO.getPaidEmployeeId());
		 referralDetail.setPaidEmployeeDetail(employeeDetail);
	 }
	  referralDetail.setReferralAmount(referralDetailDTO.getReferralAmount());
	  referralDetail.setTransactionCompletedDate(referralDetailDTO.getTransactionCompletedDate());
	 if(referralDetailDTO.getFinTransactionId()!= null){
		 FinTransaction finTransaction = new FinTransaction();
		 finTransaction.setFinTransactionId(referralDetailDTO.getFinTransactionId());
		 referralDetail.setFinTransaction(finTransaction);
	 }
	return referralDetail;
}
@Override
public List<ReferralDetail> convertDTOListToEntityList(List<ReferralDetailDTO> referralDetailDTOList) {
	List<ReferralDetail> referralDetailList = new ArrayList<>();
	 referralDetailDTOList.forEach(referralDetailDTO -> referralDetailList.add(convertDTOtoEntity(referralDetailDTO, null)));
	return referralDetailList;
}
@Override
public List<ReferralDetailDTO> convertEntityListToDTOList(List<ReferralDetail> referralDetailList) {
	List<ReferralDetailDTO> referralDetailDTOList = new ArrayList<>();
	 referralDetailList.forEach(referralDetail -> referralDetailDTOList.add(convertEntityToDTO(referralDetail)));
	return referralDetailDTOList;
}
}