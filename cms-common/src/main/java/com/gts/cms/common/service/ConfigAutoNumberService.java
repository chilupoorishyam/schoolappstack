package com.gts.cms.common.service;

import java.util.List;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.ConfigAutonumberDTO;

public interface ConfigAutoNumberService {

	ApiResponse<Long> addConfigAutoNumber(ConfigAutonumberDTO configAutoNumberDTO);

	ApiResponse<ConfigAutonumberDTO> getConfigAutoNumberDetails(Long autoconfigId);
	
	ApiResponse<ConfigAutonumberDTO> getAllSchoolConfigAutoNumbers(Long schoolId, String configAtttributeCode);
	
	ApiResponse<List<ConfigAutonumberDTO>> getAllConfigAutoNumbers();

	ApiResponse<Long> updateConfigAutoNumber(ConfigAutonumberDTO configAutoNumberDTO);

	ApiResponse<Long> deleteConfigAutoNumber(Long autoconfigId);

	ApiResponse<Long> addConfigAutoNumbersList(List<ConfigAutonumberDTO> configAutoNumberDTOs);
	
	ApiResponse<ConfigAutonumberDTO> getConfigAutoNumberDetailsByCode(String autoconfigId);

}
