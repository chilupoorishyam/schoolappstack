package com.gts.cms.common.service.impl;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.RolePrivilegeDTO;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.mapper.RolePrivilegeMapper;
import com.gts.cms.common.repository.RolePrivilegeRepository;
import com.gts.cms.common.service.RolePrivilegeService;
import com.gts.cms.entity.RolePrivilege;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class RolePrivilegeServiceImpl implements RolePrivilegeService {

	private static final Logger LOGGER = Logger.getLogger(RolePrivilegeServiceImpl.class);

	@Autowired
	private RolePrivilegeMapper rolePrivilegeMapper;

	@Autowired
    RolePrivilegeRepository rolePrivilegeRepository;


	@Override
	@Transactional
	public ApiResponse<Long> addRolePrivilegeList(List<RolePrivilegeDTO> rolePrivilegeDTOs) {
		ApiResponse<Long> response = null;
		LOGGER.info("RolePrivilegeServiceImpl.addRolePrivilegeList()");
		try {
			List<RolePrivilege> rolePrivilege = rolePrivilegeMapper
					.convertDTOListToEntityList(rolePrivilegeDTOs);

			rolePrivilege = rolePrivilegeRepository.saveAll(rolePrivilege);
			rolePrivilegeRepository.flush();
			if (rolePrivilege != null) {
				response = new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(), HttpStatus.OK.value());
			} else {
				throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue());
			}
		} catch (Exception e) {
			LOGGER.error("RolePrivilegeServiceImpl.addRolePrivilegeList()" + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}
		return response;
	}


	

}
