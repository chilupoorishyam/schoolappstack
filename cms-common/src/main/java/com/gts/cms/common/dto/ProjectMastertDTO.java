package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

public class ProjectMastertDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long prjMasterId;
    private Long schoolId;
    private Long academicProjectId;
    private String projectTitle;
    private String projectDescription;
    private String prjTechnologyIds;
    private String prjKeywordIds;
    private Long projecttypeCatdetId;
    private Long inchargeEmployeeId;
    private Long primaryStudentId;
    private String outsourceCompanyName;
    private String outsourceCompanyContactDet;
    private Boolean isProjectApproved;
    private Long approvedEmpId;
    private Date approvedDate;
    private String approvalComments;
    private Date nextReviewDate;
    private Boolean isProjectSubmitted;
    private Long submittedbyEmployeeId;
    private Date submittedDate;
    private String submissionComments;
    private Boolean isActive;
    private String reason;
    private Date createdDt;
    private Long createdUser;
    private Date updatedDt;
    private Long updatedUser;

    public Long getPrjMasterId() {
        return prjMasterId;
    }

    public void setPrjMasterId(Long prjMasterId) {
        this.prjMasterId = prjMasterId;
    }

    public Long getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Long schoolId) {
        this.schoolId = schoolId;
    }

    public Long getAcademicProjectId() {
        return academicProjectId;
    }

    public void setAcademicProjectId(Long academicProjectId) {
        this.academicProjectId = academicProjectId;
    }

    public String getProjectTitle() {
        return projectTitle;
    }

    public void setProjectTitle(String projectTitle) {
        this.projectTitle = projectTitle;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public String getPrjTechnologyIds() {
        return prjTechnologyIds;
    }

    public void setPrjTechnologyIds(String prjTechnologyIds) {
        this.prjTechnologyIds = prjTechnologyIds;
    }

    public String getPrjKeywordIds() {
        return prjKeywordIds;
    }

    public void setPrjKeywordIds(String prjKeywordIds) {
        this.prjKeywordIds = prjKeywordIds;
    }

    public Long getProjecttypeCatdetId() {
        return projecttypeCatdetId;
    }

    public void setProjecttypeCatdetId(Long projecttypeCatdetId) {
        this.projecttypeCatdetId = projecttypeCatdetId;
    }

    public Long getInchargeEmployeeId() {
        return inchargeEmployeeId;
    }

    public void setInchargeEmployeeId(Long inchargeEmployeeId) {
        this.inchargeEmployeeId = inchargeEmployeeId;
    }

    public Long getPrimaryStudentId() {
        return primaryStudentId;
    }

    public void setPrimaryStudentId(Long primaryStudentId) {
        this.primaryStudentId = primaryStudentId;
    }

    public String getOutsourceCompanyName() {
        return outsourceCompanyName;
    }

    public void setOutsourceCompanyName(String outsourceCompanyName) {
        this.outsourceCompanyName = outsourceCompanyName;
    }

    public String getOutsourceCompanyContactDet() {
        return outsourceCompanyContactDet;
    }

    public void setOutsourceCompanyContactDet(String outsourceCompanyContactDet) {
        this.outsourceCompanyContactDet = outsourceCompanyContactDet;
    }

    public Boolean getProjectApproved() {
        return isProjectApproved;
    }

    public void setProjectApproved(Boolean projectApproved) {
        isProjectApproved = projectApproved;
    }

    public Long getApprovedEmpId() {
        return approvedEmpId;
    }

    public void setApprovedEmpId(Long approvedEmpId) {
        this.approvedEmpId = approvedEmpId;
    }

    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getApprovalComments() {
        return approvalComments;
    }

    public void setApprovalComments(String approvalComments) {
        this.approvalComments = approvalComments;
    }

    public Date getNextReviewDate() {
        return nextReviewDate;
    }

    public void setNextReviewDate(Date nextReviewDate) {
        this.nextReviewDate = nextReviewDate;
    }

    public Boolean getProjectSubmitted() {
        return isProjectSubmitted;
    }

    public void setProjectSubmitted(Boolean projectSubmitted) {
        isProjectSubmitted = projectSubmitted;
    }

    public Long getSubmittedbyEmployeeId() {
        return submittedbyEmployeeId;
    }

    public void setSubmittedbyEmployeeId(Long submittedbyEmployeeId) {
        this.submittedbyEmployeeId = submittedbyEmployeeId;
    }

    public Date getSubmittedDate() {
        return submittedDate;
    }

    public void setSubmittedDate(Date submittedDate) {
        this.submittedDate = submittedDate;
    }

    public String getSubmissionComments() {
        return submissionComments;
    }

    public void setSubmissionComments(String submissionComments) {
        this.submissionComments = submissionComments;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    public Long getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(Long createdUser) {
        this.createdUser = createdUser;
    }

    public Date getUpdatedDt() {
        return updatedDt;
    }

    public void setUpdatedDt(Date updatedDt) {
        this.updatedDt = updatedDt;
    }

    public Long getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(Long updatedUser) {
        this.updatedUser = updatedUser;
    }
}
