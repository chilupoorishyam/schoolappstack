package com.gts.cms.common.student.service.impl;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.StudentAcademicbatchDTO;
import com.gts.cms.common.dto.StudentDiscontinueRequestDTO;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.enums.StudentDetailEnum;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.repository.GeneralDetailRepository;
import com.gts.cms.common.service.report.CommonUtils;
import com.gts.cms.common.student.repository.StudentAcademicBatchesRepository;
import com.gts.cms.common.student.repository.StudentDetailRepository;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.GeneralDetail;
import com.gts.cms.entity.StudentAcademicbatch;
import com.gts.cms.entity.StudentAttendance;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Service
public class StudentDiscontinue {
	private static final Logger LOGGER = Logger.getLogger(StudentDiscontinue.class);
	@Autowired
	GeneralDetailRepository generalDetailRepository;
	@Autowired
	StudentDetailRepository studentDetailRepository;
	@Autowired
	StudentAcademicBatchesRepository studentAcademicBatchesRepository;
	/*@Autowired
	BatchwiseStudentRepository batchwiseStudentRepository;
	@Autowired
	StudentAttendanceRepository studentAttendanceRepository;
	@Autowired
	StudentAttendanceMapper studentAttendanceMapper;
	@Autowired
	StudentAcademicbatchMapper studentAcademicbatchMapper;*/
	
	// @Override
	@Transactional
	public ApiResponse<?> discontinueStudentDetail(
			@Valid List<StudentDiscontinueRequestDTO> studentDiscontinueRequestDTOs) {
		ApiResponse<?> response = null;
		List<StudentAttendance> studentAttendanceList = null;
		List<StudentAcademicbatch> studentAcademicbatchs = null;
		StudentAcademicbatch studentAcademicbatch = null;
		List<StudentAcademicbatch> stuAcademicbatchsExist = new ArrayList<>();
		List<StudentAttendance> studentAttendancesExist = new ArrayList<>();
		Integer value = null;
		int success = 0;
		//List<StudentAttendanceDTO> studentAttendanceDTOs=null;
		List<StudentAcademicbatchDTO> studentAcademicbatchDTOs=null;
		try {
			GeneralDetail generalDetail = generalDetailRepository
					.getGeneralDetailByCode(StudentDetailEnum.DISCONTINUED.getValue());
			if (generalDetail != null) {
				for (StudentDiscontinueRequestDTO studentDiscontinueRequestDTO : studentDiscontinueRequestDTOs) {

					// for attendance check
					/*studentAttendanceList = studentAttendanceRepository.findByAbsentList(
							studentDiscontinueRequestDTO.getToDate(), studentDiscontinueRequestDTO.getStudentId());*/
					Date date = CommonUtils.getLessThanOneDate(studentDiscontinueRequestDTO.getFromDate());
					// fromDate and toDate exist
					studentAcademicbatchs = studentAcademicBatchesRepository
							.findAlreadyExistWithDate(studentDiscontinueRequestDTO.getStudentId(), date);
					// old record
					studentAcademicbatch = studentAcademicBatchesRepository.getDetails(
							studentDiscontinueRequestDTO.getSchoolId(), studentDiscontinueRequestDTO.getStudentId());
					value = CommonUtils.compareDate(studentAcademicbatch.getFromDate(), date);
					// student academic batch exist
					if (value == -1) {
						stuAcademicbatchsExist.add(studentAcademicbatch);
					}
					// student academic batch exist
					if (!(CollectionUtils.isEmpty(studentAcademicbatchs))) {
						stuAcademicbatchsExist.addAll(studentAcademicbatchs);
					}
					// attendance exist
					if (!(CollectionUtils.isEmpty(studentAttendanceList))) {
						studentAttendancesExist.addAll(studentAttendanceList);
					}

					if (CollectionUtils.isEmpty(studentAttendanceList) && CollectionUtils.isEmpty(studentAcademicbatchs)
							&& studentAcademicbatch != null && ((value == 0) || (value == 1))) {

						Long count = studentDetailRepository.updateStudentStatus(generalDetail.getGeneralDetailId(),
								studentDiscontinueRequestDTO.getSchoolId(),
								studentDiscontinueRequestDTO.getStudentId(), studentDiscontinueRequestDTO.getReason(),
								null, null, null);
						if (count > 0) {
							Date toDate = null;
							if (studentDiscontinueRequestDTO.getToDate() != null) {
								toDate = CommonUtils.getLessThanOneDate(studentDiscontinueRequestDTO.getToDate());
							}

							LOGGER.info("discontinueStudentDetail-->ToDate-->" + toDate);
							studentAcademicbatch.setToDate(toDate);
							studentAcademicbatch.setToBatch(studentAcademicbatch.getFromBatch());
							studentAcademicbatch.setToCourseYear(null);
							studentAcademicbatch.setToGroupSection(null);
							studentAcademicbatch.setReason(studentDiscontinueRequestDTO.getReason());

							studentAcademicBatchesRepository.save(studentAcademicbatch);
							studentAcademicBatchesRepository.flush();

							// new record insert for discontinue
							StudentAcademicbatch studentAcademicbatchObj = new StudentAcademicbatch();

							studentAcademicbatchObj.setSchool(studentAcademicbatch.getSchool());
							studentAcademicbatchObj.setStudentDetail(studentAcademicbatch.getStudentDetail());
							studentAcademicbatchObj.setCourse(studentAcademicbatch.getCourse());
							studentAcademicbatchObj.setAcademicYear(studentAcademicbatch.getAcademicYear());
							studentAcademicbatchObj.setFromCourseYear(studentAcademicbatch.getFromCourseYear());
							studentAcademicbatchObj.setFromBatch(studentAcademicbatch.getFromBatch());
							studentAcademicbatchObj.setFromGroupSection(studentAcademicbatch.getFromGroupSection());

							if (generalDetail.getGeneralDetailId() != null) {
								GeneralDetail studentStatus = new GeneralDetail();
								studentStatus.setGeneralDetailId(generalDetail.getGeneralDetailId());
								studentAcademicbatchObj.setStudentStatus(studentStatus);
							}
							studentAcademicbatchObj.setIsPromoted(Boolean.FALSE);
							studentAcademicbatchObj.setIsAlmuni(studentAcademicbatch.getIsAlmuni());
							studentAcademicbatchObj.setIsActive(studentAcademicbatch.getIsActive());

							Date fromDate = null;
							if (studentDiscontinueRequestDTO.getFromDate() != null) {
								fromDate = CommonUtils.getDateAndTime(studentDiscontinueRequestDTO.getFromDate());
							}

							studentAcademicbatchObj.setFromDate(fromDate);
							studentAcademicbatchObj.setToDate(null);
							studentAcademicbatchObj.setCreatedDt(new Date());
							studentAcademicbatchObj.setCreatedUser(SecurityUtil.getCurrentUser());

							studentAcademicbatchObj.setToBatch(studentAcademicbatch.getFromBatch());
							studentAcademicbatchObj.setToCourseYear(null);
							studentAcademicbatchObj.setToGroupSection(null);
							studentAcademicbatchObj.setReason(studentDiscontinueRequestDTO.getReason());

							studentAcademicBatchesRepository.save(studentAcademicbatchObj);
							studentAcademicBatchesRepository.flush();

							List<Long> nos = new ArrayList<>();
							nos.add(studentDiscontinueRequestDTO.getStudentId());
						/*	Integer updateCount = batchwiseStudentRepository.updateBatchwiseStudents(nos,
									studentDiscontinueRequestDTO.getGroupSectionId(), new Date(),
									SecurityUtil.getCurrentUser());
							LOGGER.debug("Student batches updated count : " + updateCount);*/

						}
						success += 1;
					}
				}
				/*StudentAttendanceAndDates studentAttendanceAndDates=new StudentAttendanceAndDates();
				studentAttendanceDTOs=studentAttendanceMapper.convertEntityListToDTOList(studentAttendancesExist);
				studentAcademicbatchDTOs=studentAcademicbatchMapper.convertEntityListToDTOList(stuAcademicbatchsExist);
				studentAttendanceAndDates.setStudenAttendanceDTOs(studentAttendanceDTOs);
				studentAttendanceAndDates.setStudentAcademicbatchDTOs(studentAcademicbatchDTOs);
				if(success>0) {
					response =new ApiResponse<>(Boolean.TRUE, Messages.UPDATE_SUCCESS.getValue(),studentAttendanceAndDates, HttpStatus.OK.value());
				}
				else {
					response =new ApiResponse<>(Boolean.FALSE, Messages.UPDATE_FAILURE.getValue(),studentAttendanceAndDates, HttpStatus.OK.value());

				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;
	}
}
