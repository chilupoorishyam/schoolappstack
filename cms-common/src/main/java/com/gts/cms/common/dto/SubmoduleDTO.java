package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

public class SubmoduleDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long subModuleId;
	private String displayName;
	private Integer sortOrder;
	private Set<PageDTO> pages;
	private String subModuleName;
	private String iconName;
	
	private Date createdDt;
	private Long createdUser;
	private Boolean isActive;
	private String reason;
	private String submoduleName;
	private Date updatedDt;
	private Long updatedUser;
	private Long moduleId;
	private String moduleName;
	
	public String getIconName() {
		return iconName;
	}
	public void setIconName(String iconName) {
		this.iconName = iconName;
	}
	public Long getSubModuleId() {
		return subModuleId;
	}
	public void setSubModuleId(Long subModuleId) {
		this.subModuleId = subModuleId;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public Integer getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
	public Set<PageDTO> getPages() {
		return pages;
	}
	public void setPages(Set<PageDTO> pages) {
		this.pages = pages;
	}
	public String getSubModuleName() {
		return subModuleName;
	}
	public void setSubModuleName(String subModuleName) {
		this.subModuleName = subModuleName;
	}
	@Override
	public int hashCode() {
	
	return	subModuleId.hashCode();
		
		
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubmoduleDTO other = (SubmoduleDTO) obj;
		if (subModuleId == null) {
			if (other.subModuleId != null)
				return false;
		} else if (!subModuleId.equals(other.subModuleId)) {
			return false;
		}
		return true;
	}
	public Date getCreatedDt() {
		return createdDt;
	}
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	public Long getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getSubmoduleName() {
		return submoduleName;
	}
	public void setSubmoduleName(String submoduleName) {
		this.submoduleName = submoduleName;
	}
	public Date getUpdatedDt() {
		return updatedDt;
	}
	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}
	public Long getUpdatedUser() {
		return updatedUser;
	}
	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}
	public Long getModuleId() {
		return moduleId;
	}
	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	
	
	
}
