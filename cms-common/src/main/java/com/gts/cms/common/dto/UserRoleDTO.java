package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

public class UserRoleDTO implements Serializable {
private static final long serialVersionUID = 1L;
private Long userRoleId;
private Date createdDt;
private Long createdUser;
private Boolean isActive;
private Date updatedDt;
private Long updatedUser;
private Long userId;
private String firstName;
private Long userTypeId;
private String lastName;
private String resetPasswordCode;
private String userName;
private Long roleId;
private String roleName;

public void setUserRoleId(Long userRoleId) {
 this.userRoleId = userRoleId;
}
public Long getUserRoleId(){
 return userRoleId;
}
public void setCreatedDt(Date createdDt) {
 this.createdDt = createdDt;
}
public Date getCreatedDt(){
 return createdDt;
}
public void setCreatedUser(Long createdUser) {
 this.createdUser = createdUser;
}
public Long getCreatedUser(){
 return createdUser;
}
public void setIsActive(Boolean isActive) {
 this.isActive = isActive;
}
public Boolean getIsActive(){
 return isActive;
}
public void setUpdatedDt(Date updatedDt) {
 this.updatedDt = updatedDt;
}
public Date getUpdatedDt(){
 return updatedDt;
}
public void setUpdatedUser(Long updatedUser) {
 this.updatedUser = updatedUser;
}
public Long getUpdatedUser(){
 return updatedUser;
}
public void setUserId(Long userId) {
 this.userId = userId;
}
public Long getUserId(){
 return userId;
}
public void setFirstName(String firstName) {
 this.firstName = firstName;
}
public String getFirstName(){
 return firstName;
}
public void setUserTypeId(Long userTypeId) {
 this.userTypeId = userTypeId;
}
public Long getUserTypeId(){
 return userTypeId;
}
public void setLastName(String lastName) {
 this.lastName = lastName;
}
public String getLastName(){
 return lastName;
}
public void setResetPasswordCode(String resetPasswordCode) {
 this.resetPasswordCode = resetPasswordCode;
}
public String getResetPasswordCode(){
 return resetPasswordCode;
}
public void setUserName(String userName) {
 this.userName = userName;
}
public String getUserName(){
 return userName;
}
public void setRoleId(Long roleId) {
 this.roleId = roleId;
}
public Long getRoleId(){
 return roleId;
}
public void setRoleName(String roleName) {
 this.roleName = roleName;
}
public String getRoleName(){
 return roleName;
}
}