package com.gts.cms.common.student.mapper;

import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.student.dto.StudentDetailStgDTO;
import com.gts.cms.entity.StudentDetailStg;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StudentDetailStgMapper implements BaseMapper<StudentDetailStg, StudentDetailStgDTO> {

	@Override
	public StudentDetailStgDTO convertEntityToDTO(StudentDetailStg studentDetailStg) {
		StudentDetailStgDTO studentDetailStgDTO = null;
		if (studentDetailStg != null) {
			studentDetailStgDTO = StudentDetailStgDTO.builder()
					.school(studentDetailStg.getSchool())
					.course(studentDetailStg.getCourse())
					.group(studentDetailStg.getGroup())
					.courseYear(studentDetailStg.getCourseYear())
					.section(studentDetailStg.getSection())
					.quota(studentDetailStg.getQuota())
					.batch(studentDetailStg.getBatch())
					.academicYear(studentDetailStg.getAcademicYear())
					.rollNo(studentDetailStg.getRollNo())
					.hallTicketNumber(studentDetailStg.getHallTicketNumber())
					.firstName(studentDetailStg.getFirstName())
					.middleName(studentDetailStg.getMiddleName())
					.lastName(studentDetailStg.getLastName())
					.gender(studentDetailStg.getGender())
					.dateOfBirth(studentDetailStg.getDateOfBirth())
					.mobile(studentDetailStg.getMobile())
					.studentEmailID(studentDetailStg.getStudentEmailID())
					.aadhaarNo(studentDetailStg.getAadhaarNo())
					.fatherName(studentDetailStg.getFatherName())
					.fatherMobile(studentDetailStg.getFatherMobile())
					.motherName(studentDetailStg.getMotherName())
					.motherMobile(studentDetailStg.getMotherMobile())
					.permanentAddress(studentDetailStg.getPermanentAddress())
					.city(studentDetailStg.getCity())
					.district(studentDetailStg.getDistrict())
					.build();
		}
		return studentDetailStgDTO;
	}

	@Override
	public List<StudentDetailStgDTO> convertEntityListToDTOList(List<StudentDetailStg> entityList) {
		List<StudentDetailStgDTO> detailStgDTOArrayList = new ArrayList<>();
		entityList.forEach(studentDetailStg -> detailStgDTOArrayList.add(convertEntityToDTO(studentDetailStg)));
		return detailStgDTOArrayList;
	}

	@Override
	public List<StudentDetailStg> convertDTOListToEntityList(List<StudentDetailStgDTO> dtoList) {
		return null;
	}

	@Override
	public StudentDetailStg convertDTOtoEntity(StudentDetailStgDTO studentDetailStgDTO, StudentDetailStg studentDetailStg) {
		return null;
	}
}
