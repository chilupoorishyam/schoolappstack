package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

public class ReserveBookDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long reserveBookId;
	private String comments;
	private Date createdDt;
	private Long createdUser;
	@NotNull(message = "LibMemberId is required.")
	private Long libMemberId;
	@NotNull(message = "IsActive is required.")
	private Boolean isActive;
	private Boolean iscancelled;
	private Boolean iscompleted;
	private Date issuedOn;
	private String reason;
	private Date reservedOn;
	private Date updatedDt;
	private Long updatedUser;
	@NotNull(message = "Organization Id is required.")
	private Long organizationId;
	private String orgCode;
	private String orgName;
	@NotNull(message = "Library Id is required.")
	private Long libraryId;
	private String libraryCode;
	private String libraryName;
	@NotNull(message = "Book Id is required.")
	private Long bookId;
	private String bookcode;
	private String booknumber;
	private String title;
	@NotNull(message = "PriorityCatdetId is required.")
	private Long priorityCatdetId;
	private String priorityCatdetDisplayName;
	private String priorityCatdetCode;

	public void setReserveBookId(Long reserveBookId) {
		this.reserveBookId = reserveBookId;
	}

	public Long getReserveBookId() {
		return reserveBookId;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getComments() {
		return comments;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setLibMemberId(Long libMemberId) {
		this.libMemberId = libMemberId;
	}

	public Long getLibMemberId() {
		return libMemberId;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIscancelled(Boolean iscancelled) {
		this.iscancelled = iscancelled;
	}

	public Boolean getIscancelled() {
		return iscancelled;
	}

	public void setIscompleted(Boolean iscompleted) {
		this.iscompleted = iscompleted;
	}

	public Boolean getIscompleted() {
		return iscompleted;
	}

	public void setIssuedOn(Date issuedOn) {
		this.issuedOn = issuedOn;
	}

	public Date getIssuedOn() {
		return issuedOn;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReason() {
		return reason;
	}

	public void setReservedOn(Date reservedOn) {
		this.reservedOn = reservedOn;
	}

	public Date getReservedOn() {
		return reservedOn;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setLibraryId(Long libraryId) {
		this.libraryId = libraryId;
	}

	public Long getLibraryId() {
		return libraryId;
	}

	public void setLibraryCode(String libraryCode) {
		this.libraryCode = libraryCode;
	}

	public String getLibraryCode() {
		return libraryCode;
	}

	public void setLibraryName(String libraryName) {
		this.libraryName = libraryName;
	}

	public String getLibraryName() {
		return libraryName;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public Long getBookId() {
		return bookId;
	}

	public String getBookcode() {
		return bookcode;
	}

	public void setBookcode(String bookcode) {
		this.bookcode = bookcode;
	}

	public String getBooknumber() {
		return booknumber;
	}

	public void setBooknumber(String booknumber) {
		this.booknumber = booknumber;
	}


	public void setPriorityCatdetId(Long priorityCatdetId) {
		this.priorityCatdetId = priorityCatdetId;
	}

	public Long getPriorityCatdetId() {
		return priorityCatdetId;
	}

	public void setPriorityCatdetDisplayName(String priorityCatdetDisplayName) {
		this.priorityCatdetDisplayName = priorityCatdetDisplayName;
	}

	public String getPriorityCatdetDisplayName() {
		return priorityCatdetDisplayName;
	}

	public void setPriorityCatdetCode(String priorityCatdetCode) {
		this.priorityCatdetCode = priorityCatdetCode;
	}

	public String getPriorityCatdetCode() {
		return priorityCatdetCode;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
}