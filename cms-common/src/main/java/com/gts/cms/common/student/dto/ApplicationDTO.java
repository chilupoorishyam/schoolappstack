package com.gts.cms.common.student.dto;

import com.gts.cms.common.dto.StudentAppDocCollectionDTO;

import java.io.Serializable;
import java.util.List;

public class ApplicationDTO  implements Serializable{
private Long studentAppId;
private String applicationNumber;
private Long admissionId;
private String admissionNumber;
private Long studentId;
private String studentDetailPhotoPath;
private String studentDetailMotherPhotoPath;
private String studentDetailFatherPhotoPath;


private List<StudentAppDocCollectionDTO> studentAppDocCollectionDTO;
private List<StudentDocumentCollectionDTO> studentDocumentCollectionDTO;

public Long getStudentAppId() {
	return studentAppId;
}

public void setStudentAppId(Long studentAppId) {
	this.studentAppId = studentAppId;
}

public String getApplicationNumber() {
	return applicationNumber;
}

public void setApplicationNumber(String applicationNumber) {
	this.applicationNumber = applicationNumber;
}

public Long getAdmissionId() {
	return admissionId;
}

public List<StudentDocumentCollectionDTO> getStudentDocumentCollectionDTO() {
	return studentDocumentCollectionDTO;
}

public void setStudentDocumentCollectionDTO(List<StudentDocumentCollectionDTO> studentDocumentCollectionDTO) {
	this.studentDocumentCollectionDTO = studentDocumentCollectionDTO;
}

public void setAdmissionId(Long admissionId) {
	this.admissionId = admissionId;
}

public String getAdmissionNumber() {
	return admissionNumber;
}

public void setAdmissionNumber(String admissionNumber) {
	this.admissionNumber = admissionNumber;
}

public List<StudentAppDocCollectionDTO> getStudentAppDocCollectionDTO() {
	return studentAppDocCollectionDTO;
}

public void setStudentAppDocCollectionDTO(List<StudentAppDocCollectionDTO> studentAppDocCollectionDTO) {
	this.studentAppDocCollectionDTO = studentAppDocCollectionDTO;
}

public Long getStudentId() {
	return studentId;
}

public void setStudentId(Long studentId) {
	this.studentId = studentId;
}

public String getStudentDetailPhotoPath() {
	return studentDetailPhotoPath;
}

public void setStudentDetailPhotoPath(String studentDetailPhotoPath) {
	this.studentDetailPhotoPath = studentDetailPhotoPath;
}

public String getStudentDetailMotherPhotoPath() {
	return studentDetailMotherPhotoPath;
}

public void setStudentDetailMotherPhotoPath(String studentDetailMotherPhotoPath) {
	this.studentDetailMotherPhotoPath = studentDetailMotherPhotoPath;
}

public String getStudentDetailFatherPhotoPath() {
	return studentDetailFatherPhotoPath;
}

public void setStudentDetailFatherPhotoPath(String studentDetailFatherPhotoPath) {
	this.studentDetailFatherPhotoPath = studentDetailFatherPhotoPath;
}



}
