package com.gts.cms.common.student.repository;

import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.student.repository.custom.StudentDetailRepositoryCustom;
import com.gts.cms.entity.StudentDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface StudentDetailRepository extends JpaRepository<StudentDetail, Long>, StudentDetailRepositoryCustom {
	String inSchool= Messages.INCOLLEGE.getValue();
	@Query("SELECT DISTINCT sd FROM StudentDetail sd" + " JOIN FETCH sd.stdAttendances sa " +"JOIN FETCH sa.actualClassesSchedule acs"
			+ " WHERE 1=1 and (:schoolId is null or sd.school.schoolId = :schoolId)"
			+ " AND (:attendanceDate is null or  DATE(sa.attendanceDate) = :attendanceDate)"
			+ " AND (:courseYearId is null  or sd.courseYear.courseYearId = :courseYearId)"
			+ " AND (:courseId is null or sd.course.courseId = :courseId)"
			+ " AND (:groupSectionId is null or sd.groupSection.groupSectionId = :groupSectionId)"
			+ " AND (:timetableScheduleId is null or sa.schedule.timetableScheduleId = :timetableScheduleId)"
			+ " AND (:subjectId is null or sa.subject.subjectId = :subjectId)"
			+ " AND (:studentId is null or (sd.studentId = :studentId  AND sd.isActive = true))"
			+ " AND sa.isAbsent = true "
			+ " AND (:isActive is null or sa.isActive = :isActive)"
			+ " AND (:studentbatchId is null or acs.studentbatch.studentbatchId=:studentbatchId)"
			+ " ORDER BY sd.rollNumber ASC")
	List<StudentDetail> findStudentAbsentList(@Param("isActive") Boolean status,
			@Param("schoolId") Long schoolId, @Param("courseId") Long courseId,
			@Param("courseYearId") Long courseYearId,
			@Param("groupSectionId") Long groupSectionId, 
			@Param("attendanceDate") Date attendanceDate,
			@Param("timetableScheduleId") Long timetableScheduleId,
			@Param("subjectId") Long subjectId,
			@Param("studentId") Long studentId, 
			@Param("studentbatchId") Long studentbatchId);
	
	@Query("SELECT DISTINCT sd FROM StudentDetail sd" + " JOIN FETCH sd.stdAttendances sa " +"JOIN FETCH sa.actualClassesSchedule acs"
			+ " WHERE 1=1 and (:schoolId is null or sd.school.schoolId = :schoolId)"
			+ " AND (:attendanceFormat is null or ((:attendanceDate is null or DATE(sa.attendanceDate) = :attendanceDate) and year(sa.attendanceDate)=:year) )"
			+ " AND (:courseYearId is null  or sd.courseYear.courseYearId = :courseYearId)"
			+ " AND (:courseId is null or sd.course.courseId = :courseId)"
			+ " AND (:groupSectionId is null or sd.groupSection.groupSectionId = :groupSectionId)"
			+ " AND (:timetableScheduleId is null or sa.schedule.timetableScheduleId = :timetableScheduleId)"
			+ " AND (:subjectId is null or sa.subject.subjectId = :subjectId)"
			+ " AND (:studentId is null or (sd.studentId = :studentId  AND sd.isActive = true))"
			+ " AND sa.isAbsent = true "
			+ " AND (:isActive is null or sa.isActive = :isActive)" 
			+ " AND (:studentbatchId is null or acs.studentbatch.studentbatchId=:studentbatchId)"
			+ " ORDER BY sd.rollNumber ASC")
	List<StudentDetail> findStudentAbsentDayList(@Param("isActive") Boolean status, @Param("schoolId") Long schoolId,
			@Param("courseId") Long courseId,
			@Param("courseYearId") Long courseYearId, @Param("groupSectionId") Long groupSectionId,
			@Param("attendanceDate") Date attendanceDate, @Param("attendanceFormat") String attendanceFormat,
			@Param("year") Integer year,
			@Param("timetableScheduleId") Long timetableScheduleId,
			@Param("subjectId") Long subjectId,
			@Param("studentId") Long studentId, 
			@Param("studentbatchId") Long studentbatchId);
	
	@Query("SELECT DISTINCT sd FROM StudentDetail sd" + " JOIN FETCH sd.stdAttendances sa " +"JOIN FETCH sa.actualClassesSchedule acs"
			+ " WHERE 1=1 and (:schoolId is null or sd.school.schoolId = :schoolId)"
			+ " AND (:attendanceFormat is null or "
			+ " ((:weekOfYear is null or week(sa.attendanceDate) = :weekOfYear) and year(sa.attendanceDate)=:year ))"
			+ " AND (:courseYearId is null  or sd.courseYear.courseYearId = :courseYearId)"
			+ " AND (:courseId is null or sd.course.courseId = :courseId)"
			+ " AND (:timetableScheduleId is null or sa.schedule.timetableScheduleId = :timetableScheduleId)"
			+ " AND (:groupSectionId is null or sd.groupSection.groupSectionId = :groupSectionId)"
			+ " AND (:subjectId is null or sa.subject.subjectId = :subjectId)"
			+ " AND (:studentId is null or (sd.studentId = :studentId  AND sd.isActive = true))"
			+ " AND sa.isAbsent = true "
			+ " AND (:isActive is null or sa.isActive = :isActive)" 
			+ " AND (:studentbatchId is null or acs.studentbatch.studentbatchId=:studentbatchId)"
			+ " ORDER BY sd.rollNumber ASC")
	List<StudentDetail> findStudentAbsentWeekList(@Param("isActive") Boolean status, @Param("schoolId") Long schoolId,
			@Param("courseId") Long courseId,
			@Param("courseYearId") Long courseYearId, @Param("groupSectionId") Long groupSectionId,
			@Param("attendanceFormat") String attendanceFormat,
			@Param("weekOfYear") Integer weekOfYear, @Param("year") Integer year,
			@Param("timetableScheduleId") Long timetableScheduleId,
			@Param("subjectId") Long subjectId,
			@Param("studentId") Long studentId, 
			@Param("studentbatchId") Long studentbatchId);
	
	
	@Query("SELECT DISTINCT sd FROM StudentDetail sd" + " JOIN FETCH sd.stdAttendances sa " +"JOIN FETCH sa.actualClassesSchedule acs"
			+ " WHERE 1=1 and (:schoolId is null or sd.school.schoolId = :schoolId)"
			+ " AND (:attendanceFormat is null or ((:month is null or month(sa.attendanceDate) = :month) and year(sa.attendanceDate)=:year ))"
			+ " AND (:courseYearId is null  or sd.courseYear.courseYearId = :courseYearId)"
			+ " AND (:courseId is null or sd.course.courseId = :courseId)"
			+ " AND (:groupSectionId is null or sd.groupSection.groupSectionId = :groupSectionId)"
			+ " AND (:timetableScheduleId is null or sa.schedule.timetableScheduleId = :timetableScheduleId)"
			+ " AND (:subjectId is null or sa.subject.subjectId = :subjectId)"
			+ " AND (:studentId is null or (sd.studentId = :studentId  AND sd.isActive = true))"
			+ " AND sa.isAbsent = true "
			+ " AND (:isActive is null or sa.isActive = :isActive)" 
			+ " AND (:studentbatchId is null or acs.studentbatch.studentbatchId=:studentbatchId)"
			+ " ORDER BY sd.rollNumber ASC")
	List<StudentDetail> findStudentAbsentMonthList(@Param("isActive") Boolean status, @Param("schoolId") Long schoolId,
			@Param("courseId") Long courseId,
			@Param("courseYearId") Long courseYearId, @Param("groupSectionId") Long groupSectionId,
			@Param("attendanceFormat") String attendanceFormat,
			@Param("month") Integer month, @Param("year") Integer year,
			@Param("timetableScheduleId") Long timetableScheduleId,
			@Param("subjectId") Long subjectId,
			@Param("studentId") Long studentId, 
			@Param("studentbatchId")Long studentbatchId);
	
	@Query("SELECT DISTINCT sd FROM StudentDetail sd" + " JOIN FETCH sd.stdAttendances sa " +"JOIN FETCH sa.actualClassesSchedule acs"
			+ " WHERE 1=1 and (:schoolId is null or sd.school.schoolId = :schoolId)"
			+ " AND (:attendanceFormat is null or (year(sa.attendanceDate)=:year ))"
			+ " AND (:courseYearId is null  or sd.courseYear.courseYearId = :courseYearId)"
			+ " AND (:courseId is null or sd.course.courseId = :courseId)"
			+ " AND (:groupSectionId is null or sd.groupSection.groupSectionId = :groupSectionId)"
			+ " AND (:timetableScheduleId is null or sa.schedule.timetableScheduleId = :timetableScheduleId)"
			+ " AND (:subjectId is null or sa.subject.subjectId = :subjectId)"
			+ " AND (:studentId is null or (sd.studentId = :studentId  AND sd.isActive = true))"
			+ " AND sa.isAbsent = true "
			+ " AND (:isActive is null or sa.isActive = :isActive)" 
			+ " AND (:studentbatchId is null or acs.studentbatch.studentbatchId=:studentbatchId)"
			+ " ORDER BY sd.rollNumber ASC")
	List<StudentDetail> findStudentAbsentYearList(@Param("isActive") Boolean status, @Param("schoolId") Long schoolId,
			@Param("courseId") Long courseId,
			@Param("courseYearId") Long courseYearId, @Param("groupSectionId") Long groupSectionId,
			@Param("attendanceFormat") String attendanceFormat,
			@Param("year") Integer year,
			@Param("timetableScheduleId") Long timetableScheduleId,
			@Param("subjectId") Long subjectId,
			@Param("studentId") Long studentId, 
			@Param("studentbatchId")Long studentbatchId);
	
	@Query("SELECT sd FROM StudentDetail sd" + " WHERE (:studentId is null or sd.studentId = :studentId)"
			+ " AND (:userId is null or sd.user.userId = :userId)")
	StudentDetail getOneByIds(@Param("studentId") Long studentId, @Param("userId") Long userId);

	/*@Modifying
	@Query("UPDATE StudentDetail sd SET sd.groupSection.groupSectionId = :groupSectionId" 
			+ " WHERE sd.studentId IN ( :studentsId )")
	void updateSection(@Param("studentsId") List<Long> studentsId,@Param("groupSectionId") Long groupSectionId);*/
	
	
	@Query("SELECT DISTINCT sd FROM StudentDetail sd"
			+ " JOIN sd.feeStudentData fs"
			+ " WHERE 1=1 and (:schoolId is null or sd.school.schoolId = :schoolId)"
			+ " AND (:courseYearId is null  or sd.courseYear.courseYearId = :courseYearId)"
			+ " AND (:courseId is null or sd.course.courseId = :courseId)"
			+ " AND (:groupSectionId is null or sd.groupSection.groupSectionId = :groupSectionId)"
			+ " AND (:feeStructureId is null or fs.feeStructure.feeStructureId = :feeStructureId)"	
			+ " AND (:isActive is null or sd.isActive = :isActive)"
			+ " ORDER BY sd.rollNumber ASC")
	List<StudentDetail> getStudentFeeList(@Param("isActive") Boolean status, @Param("schoolId") Long schoolId,
			@Param("courseId") Long courseId,
			@Param("courseYearId") Long courseYearId, @Param("groupSectionId") Long groupSectionId,
			@Param("feeStructureId") Long feeStructureId);
	
	@Query("SELECT sd FROM StudentDetail sd" + " WHERE 1=1 AND sd.studentId in (:studentIds)"
			+ " AND (:isActive is null or sd.isActive = :isActive)")
	List<StudentDetail> findStudentDetailsbyIds(@Param("isActive") Boolean isActive,@Param("studentIds") List<Long> studentIds);

	@Modifying
	@Query("UPDATE StudentDetail sd SET sd.rollNumber = :rollNumber" 
			+ " WHERE sd.studentId = :studentId")
	void updateRollNumber(@Param("studentId") Long studentId,@Param("rollNumber")  String rollNumber);

	@Query("SELECT sd FROM StudentDetail sd"
			+ " WHERE 1=1"
			+ " AND (:schoolId is null or sd.school.schoolId = :schoolId)"
			+ " AND (:courseYearId is null  or sd.courseYear.courseYearId = :courseYearId)"
			+ " AND (:courseId is null or sd.course.courseId = :courseId)"
			+ " AND (:groupSectionId is null or sd.groupSection.groupSectionId = :groupSectionId)"
			+ " AND (:academicYearId is null or sd.academicYear.academicYearId = :academicYearId)"
			+ " AND (:generalDetailCode is null or sd.studentStatus.generalDetailCode = :generalDetailCode)"	
			+ " AND (:isActive is null or sd.isActive = :isActive)"
			+ " ORDER BY sd.rollNumber ASC")
	List<StudentDetail> findStudentList(@Param("isActive") Boolean status,@Param("schoolId")  Long schoolId,
			@Param("courseId") Long courseId,
			@Param("courseYearId") Long courseYearId,@Param("groupSectionId")  Long groupSectionId,
			@Param("academicYearId") Long academicYearId,@Param("generalDetailCode") String generalDetailCode);
	
	@Modifying
	@Query("UPDATE StudentDetail sd SET "
			+ " sd.groupSection.groupSectionId = :groupSectionId,"
			+ " sd.courseYear.courseYearId = :courseYearId," 
			+ " sd.academicYear.academicYearId = :academicYearId,"
			+ " sd.updatedDt=:updatedDt, "
			+ " sd.updatedUser=:updatedUser " 
			+ " WHERE sd.studentId IN ( :studentsId )")
	void updateSection(@Param("studentsId") List<Long> studentsId,
			@Param("groupSectionId")  Long groupSectionId, 
			@Param("courseYearId") Long courseYearId,
			@Param("academicYearId") Long academicYearId,@Param("updatedDt") Date updatedDt,@Param("updatedUser") Long updatedUser);
	
	@Query("SELECT sd FROM StudentDetail sd"
			+ " WHERE 1=1"
			+ " AND (:schoolId is null or sd.school.schoolId = :schoolId)"
			+ " AND (:courseYearId is null  or sd.courseYear.courseYearId = :courseYearId)"
			+ " AND (:courseId is null or sd.course.courseId = :courseId)"
			+ " AND (:groupSectionId is null or sd.groupSection.groupSectionId = :groupSectionId)"
			+ " AND (:academicYearId is null or sd.academicYear.academicYearId = :academicYearId)"
			+ " AND (:generalDetailCode is null or sd.studentStatus.generalDetailCode <> :generalDetailCode)"	
			+ " AND (:isActive is null or sd.isActive = :isActive)"
			+ " AND (:isScholarship is null or sd.isScholorship = :isScholarship)")
	List<StudentDetail> findStudentScholarshipList(@Param("isActive") Boolean status,@Param("schoolId") Long schoolId, 
			@Param("courseId")Long courseId,
			@Param("courseYearId")Long courseYearId,@Param("groupSectionId") Long groupSectionId, 
			@Param("academicYearId")Long academicYearId,@Param("generalDetailCode") String generalDetailCode,
			@Param("isScholarship")Boolean isScholarship);

	/*@Query("SELECT sd FROM StudentDetail sd"
			+ " LEFT JOIN sd.feeStudentData fs "
			+ " WITH fs.academicYear.academicYearId = :academicYearId"
			+ " WHERE 1=1"
			+ " AND  fs.feeStdDataId IS NULL"
			+ " AND (:schoolId is null or sd.school.schoolId = :schoolId)"
			+ " AND (:courseYearId is null  or sd.courseYear.courseYearId = :courseYearId)"
			+ " AND sd.academicYear.academicYearId = :academicYearId"
			+ " AND (:generalDetailId is null or sd.quota.generalDetailId = :generalDetailId)"	
			+ " AND sd.isActive = true "
			)
	List<StudentDetail> findFeeMappingStudents(
			@Param("schoolId") Long schoolId, 
			@Param("courseGroupId") Long courseGroupId,
			@Param("courseYearId") Long courseYearId,
			@Param("academicYearId") Long academicYearId,
			@Param("generalDetailId") Long quotaId);
*/
	@Query("SELECT sd FROM StudentDetail sd"
			+ " WHERE 1=1"
			+ " AND (:parentId is null or sd.parentUser.userId = :parentId)")
	List<StudentDetail> findByParentId(@Param("parentId") Long parentId);
	
	@Query("SELECT sd FROM StudentDetail sd"
			+ " WHERE 1=1"
			+ " AND (:schoolId is null or sd.school.schoolId = :schoolId)"
			+ " AND (sd.courseYear.courseYearId in (:courseYearIds))"
			+ " AND (:courseId is null or sd.course.courseId = :courseId)"
			+ " AND (:groupSectionId is null or sd.groupSection.groupSectionId = :groupSectionId)"
			+ " AND (:academicYearId is null or sd.academicYear.academicYearId = :academicYearId)"
			+ " AND (:generalDetailCode is null or sd.studentStatus.generalDetailCode = :generalDetailCode)"	
			+ " AND (:isActive is null or sd.isActive = :isActive)"
			+ " ORDER BY sd.rollNumber ASC")
	List<StudentDetail> findStudentListByCY(@Param("isActive") Boolean status,@Param("schoolId")  Long schoolId,
			@Param("courseId") Long courseId,
			@Param("courseYearIds") List<Long> courseYearIds,@Param("groupSectionId")  Long groupSectionId,
			@Param("academicYearId") Long academicYearId,@Param("generalDetailCode") String generalDetailCode);
	
	@Query("SELECT DISTINCT sd FROM StudentDetail sd " 
			+ " LEFT JOIN sd.studentSubjects ss "
			+ " WHERE 1=1"
			+ " AND (:schoolId is null or sd.school.schoolId = :schoolId)"
			+ " AND (:academicYearId is null or sd.academicYear.academicYearId = :academicYearId)"
			+ " AND (:groupSectionId is null or sd.groupSection.groupSectionId = :groupSectionId)"
			+ " AND (:subjectId is null or ss.subject.subjectId = :subjectId)"
			+ " AND (:isActive is null or sd.isActive = :isActive) ORDER BY sd.rollNumber ASC")
	List<StudentDetail> findTheoryStudentList(@Param("isActive") Boolean status, @Param("schoolId") Long schoolId,
			@Param("academicYearId") Long academicYearId, @Param("groupSectionId") Long groupSectionId,
			 @Param("subjectId") Long subjectId);
/*
	@Query("SELECT v From StudentDetail sd,ViewGetStudentDetails v "
			+ "where 1=1"
			+ "AND (sd.studentId = v.studentId)"
			+ "AND sd.studentId IN (:likeStdList)"
			+ "AND sd.isActive = true"
			)
	List<ViewGetStudentDetails>findLikeStdDetails(@Param("likeStdList") List<Long> likeStdList);
	*/
	@Query("SELECT sd FROM StudentDetail sd "
			+ " LEFT JOIN LibMember lm "
			+ " WITH (sd.studentId = lm.memberStudent.studentId) "
			+ " WHERE(:organizationId is null or sd.organization.organizationId = :organizationId)"
			+ " AND (:groupSectionId is null or sd.groupSection.groupSectionId = :groupSectionId) "
			+ " AND (:courseYearId is null or sd.courseYear.courseYearId= :courseYearId) "
			+ " AND (:academicYearId is null or sd.academicYear.academicYearId = :academicYearId) "
			+ " AND (:courseId is null or sd.course.courseId = :courseId) "
			+ " AND (lm.memberStudent.studentId is null) "
			+ " AND (sd.isActive = true)"
			+ " AND (sd.rollNumber is not null)"
			+ " AND (sd.studentStatus.generalDetailCode= :inSchool)"
			)
	List<StudentDetail> findNoLibMemberShip(@Param("organizationId") Long organizationId, 
											@Param("groupSectionId") Long groupSectionId,
											@Param("courseYearId") Long courseYearId,
											@Param("academicYearId") Long academicYearId, 
											@Param("courseId") Long courseId,
											@Param("inSchool") String inSchool);

	
	@Query("SELECT sd  FROM  StudentDetail sd"
			+" where 1=1"
			+" AND(:userId is null or sd.user.userId=:userId)"
			+ " AND (sd.studentStatus.generalDetailCode = :string)"
			+ " AND(sd.isActive=true) "
			)	
	StudentDetail findByUserId(@Param("userId") Long userId, @Param("string")String string);

	@Query("SELECT sd  FROM  StudentDetail sd"
			+" WHERE 1=1"
			+" AND(:parentUserId is null or sd.parentUser.userId=:parentUserId)"
			+" AND(sd.studentStatus.generalDetailCode NOT IN(:codes))"
			+" AND(sd.isActive=true) "
			)
	List<StudentDetail> findByParentUserId(@Param("parentUserId") Long parentUserId, @Param("codes") String codes);
	
	@Query("SELECT sd  FROM  StudentDetail sd"
			+" where 1=1"
			+" AND(:studentId is null or sd.studentId=:studentId)"
			+ " AND (sd.isActive = true)"
			)	
	StudentDetail findByStudentId(Long studentId);

	@Query("SELECT DISTINCT sd FROM  StudentDetail sd"
			+" where 1=1"
			+" AND user.userId IS NULL"
			+" AND rollNumber IS NOT NULL"
			+" AND studentStatus.generalDetailCode = :status"
			+" AND (sd.isActive = true)  GROUP BY rollNumber HAVING COUNT(*)=1 "
			)	
	List<StudentDetail> getFindByUserId(@Param("status") String status);

	@Query("SELECT sd FROM  StudentDetail sd"
			+" where 1=1"
			+" AND rollNumber IS NULL"
			+" AND studentStatus.generalDetailCode = :status"
			+" AND (sd.isActive = true) "
			)	
	List<StudentDetail> getFindByRollNo(@Param("status") String status);

	@Query("SELECT sd FROM  StudentDetail sd"
			+" where 1=1"
			+" AND studentStatus.generalDetailCode = :status"
			+" AND (sd.isActive = true) GROUP BY rollNumber HAVING COUNT(*)>1"
			)	
	List<StudentDetail> getRollNumDuplicates(@Param("status") String status);

	
/*	@Query("SELECT sd  FROM StudentDetail sd"
			+ " LEFT JOIN sd.feeStudentData fs "
			+ " WITH(sd.studentId=fs.studentDetail.studentId) "
			+ " AND (fs.academicYear.academicYearId=sd.academicYear.academicYearId) "
			+ " LEFT JOIN CourseYear cy "
			+ " WITH (cy.courseYearId = fs.courseYear.courseYearId) "
			+ " AND (cy.yearNo=:yearNo) "
			+ " WHERE 1=1 "
			+ " AND (cy.courseYearId IS NULL) "
			+ " AND (:schoolId is null or sd.school.schoolId=:schoolId) " 
			+ " AND (:studentId is null or sd.studentId=:studentId) "
			+ " AND (sd.isActive=true) "
			)
	List<StudentDetail> findFeeMappingStudents(
			@Param("schoolId") Long schoolId, 
			
		//	@Param("academicYearId") Long academicYearId,
		//	@Param("generalDetailId") Long quotaId,
			@Param("yearNo") Integer yearNo,
			@Param("studentId") Long studentId);*/
	
	
	@Query("SELECT sd  FROM StudentDetail sd"
			+ " LEFT JOIN sd.feeStudentData fs "
			+ " WITH(sd.studentId=fs.studentDetail.studentId) "
			+ " AND (fs.academicYear.academicYearId=sd.academicYear.academicYearId) "
			+ " LEFT JOIN CourseYear cy "
			+ " WITH (cy.courseYearId = fs.courseYear.courseYearId) "
			+ " AND (cy.yearNo=:yearNo) "
			+ " WHERE 1=1 "
			+ " AND (cy.courseYearId IS NULL) "
			+ " AND (:schoolId is null or sd.school.schoolId=:schoolId) " 
			+ " AND (:studentId is null or sd.studentId=:studentId) "
			+ " AND (:courseYearId is null or sd.courseYear.courseYearId=:courseYearId)"
			+ " AND (:academicYearId is null or sd.academicYear.academicYearId=:academicYearId)"
			+ " AND (:quotaId is null or sd.quota.generalDetailId=:quotaId)"
			+ " AND (sd.isActive=true) "
			)
	List<StudentDetail> findFeeMappingStudents(
			@Param("schoolId") Long schoolId, 
			@Param("courseYearId") Long courseYearId,
			@Param("academicYearId") Long academicYearId,
			@Param("quotaId") Long quotaId,
			@Param("yearNo") Integer yearNo,
			@Param("studentId") Long studentId);
	
	 @Query(" SELECT sd From StudentDetail sd"
	 		+ " WHERE 1=1 "
	 		+ " AND (:studentId is null or sd.studentId=:studentId)"
	 		+ " AND (sd.isActive=true) "
	 		)
	 List<StudentDetail> findByIdAndIsActive(@Param("studentId") Long studentId);

	@Query(" SELECT sd From StudentDetail sd"
			+ " WHERE 1=1 "
			+ " AND (sd.firstName=:firstName)"
			+ " AND (sd.lastName=:lastName)"
			+ " AND (sd.isActive=true) "
	)
	List<StudentDetail> checkUserExists(@Param("firstName") String firstName,
										@Param("lastName") String lastName);

	@Query(" SELECT sd From StudentDetail sd"
			+ " WHERE 1=1 "
			+ " AND (:studentId is null or sd.studentId=:studentId)"
			+ " AND (sd.isActive=true) "
	)
	StudentDetail getByIdAndIsActive(@Param("studentId") Long studentId);

	@Query(" SELECT st FROM StudentDetail st "
			+ " LEFT JOIN User u "
			+ " ON st.user.userId = u.userId  "
			+ " WHERE 1=1 "
			+ " AND (:orgId is null or st.organization.organizationId=:orgId) "
			+ " AND (:schoolId is null or st.school.schoolId=:schoolId) "
			+ " AND (:courseYearId is null or st.courseYear.courseYearId=:courseYearId) "
			+ " AND (:groupSectionId is null or st.groupSection.groupSectionId=:groupSectionId) "
			+ " AND (:academicYearId is null or st.academicYear.academicYearId=:academicYearId) "
	)
	List<StudentDetail> getDetailsToMemberSync(@Param("orgId") Long orgId,
											   @Param("schoolId") Long schoolId,
											   @Param("courseYearId") Long courseYearId,
											   @Param("groupSectionId") Long groupSectionId,
											   @Param("academicYearId") Long academicYearId);

	@Query("SELECT sd.user.userId FROM StudentDetail sd "
			+ " WHERE 1=1"
			+ " AND (sd.studentId = :studentId)"
			+ " AND sd.isActive=true "
	)
	Long fetchStudentUserIds(Long studentId);

	@Query("SELECT sd FROM StudentDetail sd "
			+ " WHERE 1=1"
			+ " AND (sd.rollNumber = :rollNumber)"
			+ " AND sd.isActive=true "
	)
	StudentDetail checkRollNumberIsDuplicate(@Param("rollNumber") String rollNumber);
}
