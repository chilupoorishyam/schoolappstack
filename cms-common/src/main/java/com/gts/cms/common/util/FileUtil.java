package com.gts.cms.common.util;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class FileUtil {
	
	private static final  Logger LOGGER=Logger.getLogger(FileUtil.class);
	
	//private static String url = "http://ec2-13-127-125-121.ap-south-1.compute.amazonaws.com:9000";
	private static String url;

	private static String bucketName = "cms";
	private static String mainPath = "org_VISWAMBHARA/school_VCE";
	
	
   // private static String environment;
	
	
	public static String getFileSeparator() {
		String osName = System.getProperty("os.name");
		String osNameMatch = osName.toLowerCase();
		if (osNameMatch.contains("linux")) {
			return System.getProperty("file.separator");
		} else if (osNameMatch.contains("windows")) {
			return "/";
		} else if (osNameMatch.contains("solaris") || osNameMatch.contains("sunos")) {
			return System.getProperty("file.separator");
		} else if (osNameMatch.contains("mac os") || osNameMatch.contains("macos") || osNameMatch.contains("darwin")) {
			return System.getProperty("file.separator");
		} else {
			return System.getProperty("file.separator");
		}
	}

	public static String getRelativePath(String path) {
		if (path != null) {
			return path.replace(url + getFileSeparator() + bucketName + getFileSeparator(), "");
		} else {
			return null;
		}
	}
	
	public static String getRelativePathForMail(String path) {
		if (path != null) {
			return path.replace(mainPath + getFileSeparator(), "");
		} else {
			return null;
		}
	}	

	public static String getAbsolutePath(String path) {
		/*if (environment != null && path!=null && environment.equals(Messages.officelinux.getValue())) {
			LOGGER.info("FileUtil Append IP Address==>"+Messages.IPAddress.getValue());
			return Messages.IPAddress.getValue() + getFileSeparator() + bucketName + getFileSeparator() + path;
		}*/
		if (path != null) {
			return url + getFileSeparator() + bucketName + getFileSeparator() + path;
		} else {
			return null;
		}
	}
	
	@Value("${s3.url}")
    public void setUrl(String url2) {
        url = url2;
    }
	
/*	@Value("${spring.profiles}")
    public void setEnvironment(String env) {
		environment = env;
    }*/
	public static String getUploadPath(String fileName) {
		/*
		 * String finalName = null; switch (url) { case
		 * "http://ec2-13-127-125-121.ap-south-1.compute.amazonaws.com:9000": finalName
		 * = "/home/ec2-user/minio/storage/cms/"+fileName; break; case
		 * "http://103.56.30.250:9000": finalName =
		 * "F://genesis/minio/storage/cms/"+fileName; break; case
		 * "http://183.82.126.212:9000": finalName =
		 * "F://genesis/minio/storage/cms/"+fileName; break; default: finalName =
		 * "F://genesis/minio/storage/cms/"+fileName; break; } return finalName;
		 */
		return url+getFileSeparator()+bucketName + getFileSeparator() +fileName;
	}
}
