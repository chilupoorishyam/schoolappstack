package com.gts.cms.common.exception;

import org.springframework.validation.Errors;

/**
 * created by sathish on 02/09/2018.
 */
public class InternalServerProblemException extends RuntimeException {
	private Errors errors;

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new internal server problem exception.
	 */
	public InternalServerProblemException() {
		super();
	}

	/**
	 * Instantiates a new internal server problem exception.
	 *
	 * @param s the s
	 */
	public InternalServerProblemException(String s, Errors errors) {
		super(s);
		this.errors=errors;
	}

	/**
	 * Instantiates a new internal server problem exception.
	 *
	 * @param t the t
	 */
	public InternalServerProblemException(Throwable t) {
		super(t);
	}
	public Errors getErrors() {
		return errors;
	}
}
