package com.gts.cms.common.employee.repository.custom;

import com.gts.cms.entity.EmployeeDetail;

import java.util.List;

/**
 * @author Genesis
 *
 */
public interface EmployeeDetailRepositoryCustom {

	Long deleteEmployeeDetail(Long employeeId);

	Long uploadEmployeeApplicationFiles(Long employeeId, String aadharFileName, String pancardFileName,
			String passportFileName, String photoFileName, String voterIdFileName);

	List<EmployeeDetail> findAllEmployeeDetail(Long schoolId, Long empDeptId, Boolean status, String aadharNo, String empNumber, String firstName, String mobile);
	
	public List<EmployeeDetail> findEmployeeDetails(Long organizationId,Long schoolId, Long departmentId, Long designationId, Long employeeId, Boolean status) ;

	public List<EmployeeDetail> searchEmployeeDetails(Long schoolId,String firstName,String lastName,String middleName,String employeeNo,String mobile,Long deptId,String empStatus);
}
