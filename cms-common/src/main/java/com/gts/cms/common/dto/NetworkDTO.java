package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

public class NetworkDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long networkId;
	private String callAlums;
	private Date createdDt;
	private Long createdUser;
	private Integer establishedYear;
	private Long schoolId;
	private String schoolCode;
	private String schoolName;

	private Long networkCreatedProfileId;
	private String networkCityName;
	private String networkCompanyName;
	private String networkFirstName;

	private Long networkTypeId;
	private String networkTypeName;
	private String networkTypeCode;

	private Long organizationId;
	private String orgCode;
	private String orgName;
	private Boolean isActive;
	private String location;
	private String networkIcon;
	private String networkName;
	private String networkShortName;
	private String reason;
	private Date updatedDt;
	private Long updatedUser;
	private String website;

	public void setNetworkId(Long networkId) {
		this.networkId = networkId;
	}

	public Long getNetworkId() {
		return networkId;
	}

	public void setCallAlums(String callAlums) {
		this.callAlums = callAlums;
	}

	public String getCallAlums() {
		return callAlums;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setEstablishedYear(Integer establishedYear) {
		this.establishedYear = establishedYear;
	}

	public Integer getEstablishedYear() {
		return establishedYear;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLocation() {
		return location;
	}

	public void setNetworkIcon(String networkIcon) {
		this.networkIcon = networkIcon;
	}

	public String getNetworkIcon() {
		return networkIcon;
	}

	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}

	public String getNetworkName() {
		return networkName;
	}

	public void setNetworkShortName(String networkShortName) {
		this.networkShortName = networkShortName;
	}

	public String getNetworkShortName() {
		return networkShortName;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReason() {
		return reason;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getWebsite() {
		return website;
	}
	public Long getNetworkCreatedProfileId() {
		return networkCreatedProfileId;
	}

	public void setNetworkCreatedProfileId(Long networkCreatedProfileId) {
		this.networkCreatedProfileId = networkCreatedProfileId;
	}

	public Long getNetworkTypeId() {
		return networkTypeId;
	}

	public void setNetworkTypeId(Long networkTypeId) {
		this.networkTypeId = networkTypeId;
	}

	public String getNetworkTypeName() {
		return networkTypeName;
	}

	public void setNetworkTypeName(String networkTypeName) {
		this.networkTypeName = networkTypeName;
	}

	public String getNetworkTypeCode() {
		return networkTypeCode;
	}

	public void setNetworkTypeCode(String networkTypeCode) {
		this.networkTypeCode = networkTypeCode;
	}

	public String getNetworkCityName() {
		return networkCityName;
	}

	public void setNetworkCityName(String networkCityName) {
		this.networkCityName = networkCityName;
	}

	public String getNetworkCompanyName() {
		return networkCompanyName;
	}

	public void setNetworkCompanyName(String networkCompanyName) {
		this.networkCompanyName = networkCompanyName;
	}

	public String getNetworkFirstName() {
		return networkFirstName;
	}

	public void setNetworkFirstName(String networkFirstName) {
		this.networkFirstName = networkFirstName;
	}
}