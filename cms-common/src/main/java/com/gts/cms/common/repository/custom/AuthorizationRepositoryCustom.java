package com.gts.cms.common.repository.custom;
/**
 * created by Naveen on 04/09/2018 
 * 
*/
public interface AuthorizationRepositoryCustom {
	Long logoutUser(Long userId,String token);

}
