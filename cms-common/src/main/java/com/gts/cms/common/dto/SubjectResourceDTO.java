package com.gts.cms.common.dto;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

public class SubjectResourceDTO implements Serializable{
	private static final long serialVersionUID = 1L;

	private Long subjectResourceId;
	private Date fromDate;
	private Boolean isActive;
	private Boolean isTutorial;
	private String reason;
	private Date toDate;
	private Long academicYearID;
	private String academicYear;
	private Long timetableScheduleId;
	private Long subjectTypeId;
	private String subjectTypeName;
	private String subjectTypeCode;
	private Long subjectCourseYearId;
	private Long subjectId;
	private String subjectName;
	private String subjectCode;
	private String shortName;
	private Long courseYearStaffId;
	private Long staffId;
	private String staffName;
	private String empNumber;
	private Long studentBatchId;
	private String studentBatchName;
	private Long roomId;
	private String roomName;
	private Long schoolId;
	private String schoolName;
	private String schoolCode;
	
	private String colorCode;
	
	private Long groupSectionId;
	private String section;
	
	private Long courseYearId;
	private String courseYearName;
	
	private String groupName;
	private String groupCode;
	
	Long subjectRegulationId;
	
	private String cellGroupId;
	
	private Boolean isStaffUpdate;
	private Long courseId;
	private String courseName;
	private String courseCode;
	private Date createdDt;
	private Date updatedDt;
	private Long updatedUser;
	private Long createdUser;
	private Long classTimingId;
	private Time startTime;
	private Time endTime;
	private Long weekDayId;
	private String weekDay;
	private String periodName;
	
	public Long getSubjectResourceId() {
		return subjectResourceId;
	}
	public void setSubjectResourceId(Long subjectResourceId) {
		this.subjectResourceId = subjectResourceId;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public Boolean getIsTutorial() {
		return isTutorial;
	}
	public void setIsTutorial(Boolean isTutorial) {
		this.isTutorial = isTutorial;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Long getTimetableScheduleId() {
		return timetableScheduleId;
	}
	public void setTimetableScheduleId(Long timetableScheduleId) {
		this.timetableScheduleId = timetableScheduleId;
	}
	public Long getSubjectTypeId() {
		return subjectTypeId;
	}
	public void setSubjectTypeId(Long subjectTypeId) {
		this.subjectTypeId = subjectTypeId;
	}

	public Long getSubjectCourseYearId() {
		return subjectCourseYearId;
	}
	public void setSubjectCourseYearId(Long subjectCourseYearId) {
		this.subjectCourseYearId = subjectCourseYearId;
	}
	
	public Long getCourseYearStaffId() {
		return courseYearStaffId;
	}
	public void setCourseYearStaffId(Long courseYearStaffId) {
		this.courseYearStaffId = courseYearStaffId;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public Long getStudentBatchId() {
		return studentBatchId;
	}
	public void setStudentBatchId(Long studentBatchId) {
		this.studentBatchId = studentBatchId;
	}
	public Long getRoomId() {
		return roomId;
	}
	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}
	public String getSubjectTypeName() {
		return subjectTypeName;
	}
	public void setSubjectTypeName(String subjectTypeName) {
		this.subjectTypeName = subjectTypeName;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public String getStudentBatchName() {
		return studentBatchName;
	}
	public void setStudentBatchName(String studentBatchName) {
		this.studentBatchName = studentBatchName;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	public Long getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public Long getAcademicYearID() {
		return academicYearID;
	}
	public void setAcademicYearID(Long academicYearID) {
		this.academicYearID = academicYearID;
	}
	public Long getGroupSectionId() {
		return groupSectionId;
	}
	public void setGroupSectionId(Long groupSectionId) {
		this.groupSectionId = groupSectionId;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public Long getCourseYearId() {
		return courseYearId;
	}
	public void setCourseYearId(Long courseYearId) {
		this.courseYearId = courseYearId;
	}
	public String getCourseYearName() {
		return courseYearName;
	}
	public void setCourseYearName(String courseYearName) {
		this.courseYearName = courseYearName;
	}

	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getColorCode() {
		return colorCode;
	}
	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}
	public Long getSubjectRegulationId() {
		return subjectRegulationId;
	}
	public void setSubjectRegulationId(Long subjectRegulationId) {
		this.subjectRegulationId = subjectRegulationId;
	}
	public Long getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public String getCellGroupId() {
		return cellGroupId;
	}
	public void setCellGroupId(String cellGroupId) {
		this.cellGroupId = cellGroupId;
	}
	public Boolean getIsStaffUpdate() {
		return isStaffUpdate;
	}
	public void setIsStaffUpdate(Boolean isStaffUpdate) {
		this.isStaffUpdate = isStaffUpdate;
	}
	public String getSubjectTypeCode() {
		return subjectTypeCode;
	}
	public void setSubjectTypeCode(String subjectTypeCode) {
		this.subjectTypeCode = subjectTypeCode;
	}
	public Date getCreatedDt() {
		return createdDt;
	}
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	public Date getUpdatedDt() {
		return updatedDt;
	}
	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}
	public Long getUpdatedUser() {
		return updatedUser;
	}
	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}
	public Long getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}
	public String getEmpNumber() {
		return empNumber;
	}
	public void setEmpNumber(String empNumber) {
		this.empNumber = empNumber;
	}
	public String getSchoolCode() {
		return schoolCode;
	}
	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}
	public String getAcademicYear() {
		return academicYear;
	}
	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}
	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public Long getCourseId() {
		return courseId;
	}
	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getCourseCode() {
		return courseCode;
	}
	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}
	public Long getClassTimingId() {
		return classTimingId;
	}
	public void setClassTimingId(Long classTimingId) {
		this.classTimingId = classTimingId;
	}
	public Long getWeekDayId() {
		return weekDayId;
	}
	public void setWeekDayId(Long weekDayId) {
		this.weekDayId = weekDayId;
	}
	public Time getStartTime() {
		return startTime;
	}
	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}
	public Time getEndTime() {
		return endTime;
	}
	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}
	public String getWeekDay() {
		return weekDay;
	}
	public void setWeekDay(String weekDay) {
		this.weekDay = weekDay;
	}
	public String getPeriodName() {
		return periodName;
	}
	public void setPeriodName(String periodName) {
		this.periodName = periodName;
	}	
	public String getSubjectCode() {
		return subjectCode;
	}
	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
}
