package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.CourseRepositoryCustom;
import com.gts.cms.entity.Course;
import com.gts.cms.entity.QCourse;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * created by Naveen on 04/09/2018
 * 
 */
@Repository
public class CourseRepositoryImpl extends QuerydslRepositorySupport implements CourseRepositoryCustom {
	public CourseRepositoryImpl() {
		super(Course.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public Long deleteCourseById(Long courseId) {
		return update(QCourse.course).where(QCourse.course.courseId.eq(courseId))
				.set(QCourse.course.isActive, false).execute();
	}
	
	@Override
	public List<Course> findBySchoolId(Long schoolId) {

		JPQLQuery<Course> query = from(QCourse.course)
				.where(QCourse.course.school.schoolId.eq(schoolId));
		List<Course> courseList = query.fetch();
		return courseList;

	}
	
	
}

