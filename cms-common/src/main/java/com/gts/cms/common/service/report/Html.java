package com.gts.cms.common.service.report;

public class Html extends Tag {

	private Head head;
	private Body body;
	private boolean head_append = false;
	private boolean body_append = false;
	
	/*
	 * default constructor.
	 */
	public Html() {
		super("html");
	}

	/*
	 * append <head> tag to <html>
	 */
	public void append(Head head) {
		//this.append(head.toString());
		this.head = head;
	}

	/*
	 * append <body> tag to <html>
	 */
	public void append(Body body) {
		//this.append(body.toString());
		this.body = body;
	}
	
	/*
	 * overriding the superclass method.
	 * (non-Javadoc)
	 * @see cgg.gov.in.tscabinet.pdf.Tag#toString()
	 */
	public String toString() {

		/*
		 * checking the values to append to html tag
		 */
		if (this.head!=null && !head_append) {
			super.append(this.head.toString());
			// setting value to true to eliminate the duplicate values.
			head_append = true;
		}

		/*
		 * checking the values to append to html tag
		 */
		if (this.body!=null && !body_append) {
			// setting value to true to eliminate the duplicate values.
			super.append(this.body.toString());
			body_append = true;
		}
		return super.toString();
	}

	public static void main(String[] args) throws Exception {

	/*	Tag tag = new Tag("p");
		tag.addClass("text-center");
		tag.addStyle("text-indent: 50px;");
		tag.addClass("text-justify");
		
		Tag strong = new Tag("strong");
		strong.append("font in strong font.");
		tag.append("This is paragraph.");
		
		tag.append(strong.toString());
		
		System.out.println(tag.toString());*/
		
		Html html = new Html();
		Head head = new Head();
		Body body = new Body();
		html.append(body);

		Style style = new Style();

		head.append(style);

		html.append(head);

		Div div = new Div();
		div.addClass("element-center");
		div.addClass("text-center");

		Table table = new Table();

		Td td = new Td();
		td.append("Data");
		Tr tr = new Tr();
		tr.append(td);

		td = new Td();

		td.append(div);
		tr.append(td);
		table.append(tr);

		div.append(table);

		body.append(div);



		System.out.println("html : "+html.toString());
	}

}
/**
 * 
 * @author Srinu babu
 * Class: {@link Body}
 * Contains: this used to create <body> tag.
 */
class Body extends Tag {

	/*
	 * default constructor.
	 */
	public Body() {
		super("body");
	}
	
	/*
	 * appending other tags to it.
	 */
	public void append(Tag tag) {
		this.append(tag.toString());
	}
}
/**
 * 
 * @author Srinu babu
 * Class: {@link Head}
 * Contains: this used to create the <head> tag, and to add the <style> tag.
 */
class Head extends Tag {

	private Style style;

	public Head() {
		// creating <head> using super constructor.
		super("head");
	}

	/*
	 * to append <style> tag to <head> tag.
	 */
	public void append(Style style) {
		//this.append(style.toString());
		this.style = style; 
	}

	/* 	overwriting super class toString() method.  
	 * (non-Javadoc)
	 * @see flyingsaucerpdf.Tag#toString()
	 */
	public String toString() {
		if (this.style!=null)
			super.append(style.toString());
		return super.toString();
	}

}
/*
 * Class: Div
 * Contains: this class used to create the <div> tag.
 */
class Div extends Tag {

	public Div() {
		super("div");
	}
	/*
	 * this method used to add the other elements to it.
	 */
	public void append(Tag tag) {
		this.append(tag.toString());
	}
}
/*
 * Class: Table
 * Contains: this class used to create the <table> tag and append the Tr class objects to it.
 */
class Table extends Tag {

	public Table() {
		super("table");
	}
	/*
	 * used to append the Tr tag to it.
	 */
	public void append(Tr tr) {
		this.append(tr.toString());
	}
}
/*
 * Class: Tr
 * Contains: this class used to create the <tr> tag and append the <td> tags to it.
 */
class Tr extends Tag {

	public Tr () {
		super("tr");
	}
	/*
	 * used to append the <td> tag to it.
	 */
	public void append(Td td) {
		this.append(td.toString());
	}
}
/*
 * Class: Td
 * Contains: this class used to create the <td> tag and the methods to set colspan and rowspans.
 */
class Td extends Tag {

	public Td() {
		super("td");
	}
	/*
	 * used to set the colspan
	 */
	public void setColspan(int colspan) {
		this.setAttribute("colspan", colspan+"");
	}
	
	/*
	 * used to set the rowspan
	 */
	public void setRowspan(int rowspan) {
		this.setAttribute("rowspan", rowspan+"");
	}
	/*
	 * append the different elements to td instance. 
	 */
	public void append(Tag tag) throws Exception {
		String className = CommonUtils.getClassName(tag);

		/*
		 * throws exception when tr or td tags adding to the td instance.
		 */
		if (className.equals("Tr") || className.equals("Td"))
			throw new Exception("Can not add Class "+className+" to Td class.");

		else
			this.append(tag.toString());
	}

}
/*
 * used to create <span> tag.
 */
class Span extends Tag {

	public Span() {
		super("span");
	}
}

/**
 * Style Class for <style> tag.
 * @author Srinubabu
 *
 */
class Style {

	// for adding style rules.
	private StringBuilder rules = new StringBuilder();
	private boolean generated = false;
	// creating tag instance.
	private Tag style;
	// local variable filePath. 
	private String filePath = "";

	/*
	 * for A4 size paper. 
	 */
	public static final int A4_LANDSCAPE = 1;
	public static final int A4_PORTRAIT = 2;

	/*
	 * for letter size paper.
	 */

	public static final int LETTER_LANDSCAPE = 3;
	public static final int LETTER_PORTRAIT = 4;

	/*
	 * for legal size paper.
	 */

	public static final int LEGAL_LANDSCAPE = 5;
	public static final int LEGAL_PORTRAIT = 6;
	
	public static final int CUSTOM_PORTRAIT = 7;

	private int pageSize = 2;

	// global constructor.
	public Style() {
		style = new Tag("style");
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public Style(String filePath) {
		style = new Tag("style");
		this.filePath = filePath;
	}

	public void addRule(String rule) {
		rules.append(rule);
	}
	/*
	 * overriding the super class method to give expected output.
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {

		if (!generated) {
			generated = true;

			style.setAttribute("type", "text/css");

			style.append("@page {");
			
			/*
			 * this used to add the page size.
			 */
			switch (this.pageSize) {
			
			case A4_PORTRAIT:
				style.append("size: portrait;");
				break;
				
			case A4_LANDSCAPE:
				style.append("size: landscape;");
				break;
				
			case LETTER_PORTRAIT:
				style.append("size: letter portrait;");
				break;
				
			case LETTER_LANDSCAPE:
				style.append("size: letter landscape;");
				break;
				
			case LEGAL_PORTRAIT:
				style.append("size: legal portrait;");
				break;
				
			case LEGAL_LANDSCAPE:
				style.append("size: legal landscape;");
				break;
			
			case CUSTOM_PORTRAIT:
				style.append("size: 10.5in 14in;");
				break;
				
			default:
				style.append("size: portrait;");
				break;
			}

			style.append("}");
			
			style.append(rules.toString());

			/*
			 * if any css file given to load the css rules.
			 */
			if (!(filePath==null || "".equals(filePath))) {
				try {
					style.append("\n"+ReadTextFile.readTextFile(filePath));

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return style.toString();
	}
}