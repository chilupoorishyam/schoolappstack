package com.gts.cms.common.service;

import java.util.Date;
import java.util.List;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.FinancialYearDTO;

public interface FinancialYearService {

	/*ApiResponse<Long> addFinancialYear(@Valid FinancialYearDTO financialYear);

	ApiResponse<FinancialYearDTO> getFinancialYearDetails(Long financialYearId);

	ApiResponse<List<FinancialYearDTO>> getAllFinancialYears(Long schoolId);

	ApiResponse<Long> updateFinancialYear(@Valid FinancialYearDTO financialYear);

	ApiResponse<Long> deleteFinancialYear(Long financialYearId);
*/
	ApiResponse<List<FinancialYearDTO>> getFinancialYearDate(Long schoolId, Date checkDate);
}
