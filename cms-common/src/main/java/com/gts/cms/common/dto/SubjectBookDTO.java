package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

public class SubjectBookDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long subBookId;
	private Date createdDt;
	private Long createdUser;
	private Boolean isActive;
	private Boolean isOnlinecourse;
	private Boolean isReference;
	private Boolean isTextbook;
	private String reason;
	private Date updatedDt;
	private Long updatedUser;

	@NotNull(message = "Book Id is required")
	private Long bookId;
	@NotNull(message = "School Id is required")
	private Long schoolId;
	private String schoolCode;
	private String schoolName;

	private String booknumber;
	
	private String edition;
	
	private String isbn;
	
	private String title;
	

	public void setSubBookId(Long subBookId) {
		this.subBookId = subBookId;
	}

	public Long getSubBookId() {
		return subBookId;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsOnlinecourse(Boolean isOnlinecourse) {
		this.isOnlinecourse = isOnlinecourse;
	}

	public Boolean getIsOnlinecourse() {
		return isOnlinecourse;
	}

	public void setIsReference(Boolean isReference) {
		this.isReference = isReference;
	}

	public Boolean getIsReference() {
		return isReference;
	}

	public void setIsTextbook(Boolean isTextbook) {
		this.isTextbook = isTextbook;
	}

	public Boolean getIsTextbook() {
		return isTextbook;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReason() {
		return reason;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public Long getBookId() {
		return bookId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public String getBooknumber() {
		return booknumber;
	}

	public void setBooknumber(String booknumber) {
		this.booknumber = booknumber;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}


}