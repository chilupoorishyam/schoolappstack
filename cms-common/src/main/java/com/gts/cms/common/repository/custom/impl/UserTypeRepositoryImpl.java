package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.UserTypeRepositoryCustom;
import com.gts.cms.entity.QUsertype;
import com.gts.cms.entity.Usertype;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * created by Naveen on 04/09/2018
 * 
 */
@Repository
public class UserTypeRepositoryImpl extends QuerydslRepositorySupport implements UserTypeRepositoryCustom {
	
	public UserTypeRepositoryImpl() {
		super(Usertype.class);
	}

	@PersistenceContext
	private EntityManager em;
	
	

	@Override
	public Usertype getUserTypeByCode(String userTypeCode) {
		JPQLQuery<Usertype> query = from(QUsertype.usertype)
				.where(QUsertype.usertype.userTypeCode.eq(userTypeCode).and(QUsertype.usertype.isActive.eq(Boolean.TRUE)));
		return query.fetchOne();
	}
	
}
