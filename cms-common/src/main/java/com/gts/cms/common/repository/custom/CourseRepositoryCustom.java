package com.gts.cms.common.repository.custom;

import com.gts.cms.entity.Course;

import java.util.List;

/**
 * created by Naveen on 04/09/2018 
 * 
*/
public interface CourseRepositoryCustom {

	Long deleteCourseById(Long courseId);
	
	List<Course> findBySchoolId(Long schoolId);
}
