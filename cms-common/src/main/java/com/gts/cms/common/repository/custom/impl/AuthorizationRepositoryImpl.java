package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.AuthorizationRepositoryCustom;
import com.gts.cms.entity.Authorization;
import com.gts.cms.entity.QAuthorization;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;

/**
 * created by Naveen on 04/09/2018
 * 
 */
@Repository
public class AuthorizationRepositoryImpl extends QuerydslRepositorySupport implements AuthorizationRepositoryCustom {
	public AuthorizationRepositoryImpl() {
		super(Authorization.class);
	}
	
	@Override
	@Transactional
	public Long logoutUser(Long userId,String token) {
		return update(QAuthorization.authorization).where(QAuthorization.authorization.user.userId.eq(userId)
				.and(QAuthorization.authorization.authorizationKey.eq(token)))
				.set(QAuthorization.authorization.logoutTime, new Date())
				.set(QAuthorization.authorization.updatedDt, new Date())
				.execute();
	}
	
}
