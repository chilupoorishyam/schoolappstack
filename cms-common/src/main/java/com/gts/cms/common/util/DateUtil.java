package com.gts.cms.common.util;

import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author Shyamsunder
 */
public final class DateUtil {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(DateUtil.class);


    /**
     * Private constructor prevents instantiation.
     */
    private DateUtil() {
    }

    /**
     * Default locale
     */
    public static final String DEFAULT_LOCALE = "en_US";

    /**
     * Default date format.
     */
    public static final String DEFAULT_DATE_FORMAT = "dd-MM-yyyy";
    public static final String DEFAULT_DATE_DB_FORMAT = "yyyy-MM-dd";
    public static final String DATE_MON_FORMAT = "dd-MMM-yyyy";
    public static final String MONTH_YEAR_FORMAT = "MMM-yyyy";

    /**
     * Default date format holder.
     */
    public static final ThreadLocal<DateFormat> DEFAULT_DATE_FORMAT_HOLDER = new ThreadLocal<DateFormat>() {

        /**
         * Gets initial value.
         *
         * @return the initial value
         */
        @Override
        protected final DateFormat initialValue() {
            return new SimpleDateFormat(DEFAULT_DATE_FORMAT);
        }
    };

    /**
     * Default time format.
     */
    public static final String DEFAULT_TIME_FORMAT = "hh:mm aa";

    /**
     * Default date-time format.
     */
    public static final String DEFAULT_DATE_TIME_FORMAT = "dd-MM-yyyy HH:mm:ss";

    public static final String ZERO_STRING = "0";
    public static final String EMPTY_STRING = "";
    public static final int NUMBER_ZERO = 0;
    public static final String PLUS_SIGN = "+";
    public static final String MINUS_SIGN = "-";
    public static final int MINUS_ONE = -1;
    public static final int MINUTES_PER_HOUR = 60;
    public static final int TEN = 10;

    /**
     * Factory instance for XMLGregorianCalendar creation.
     */
    private static volatile DatatypeFactory datatypeFactory;

    /**
     * Converts from {@code Date} to {@code String}.
     *
     * @param format the date format
     * @param date   the date object
     * @return {@code String} representation of the {@code Date} object, or null
     */
    public static String convertDateToString(final String format, final Date date) {
        String dateConverted = null;
        if (null != date) {
            try {
                final DateFormat formatter = (StringUtils.isEmpty(format) ? DEFAULT_DATE_FORMAT_HOLDER.get() : new SimpleDateFormat(format));
                dateConverted = formatter.format(date);
            } catch (final RuntimeException e) {
                LOGGER.warn("Failed to convertDateToString", e);
            }
        }
        return dateConverted;
    }

    /**
     * Converts {@code String} representation of date to {@code Date} object.
     *
     * @param format  the date format
     * @param strDate the date in {@code String} format
     * @return {@code Date} representation of the date, or null
     */
    public static Date convertStringToDate(final String format, final String strDate) {
        Date dateConverted = null;
        if (null != strDate) {
            try {
                final DateFormat formatter = (StringUtils.isEmpty(format) ? DEFAULT_DATE_FORMAT_HOLDER.get() : new SimpleDateFormat(format));
                dateConverted = formatter.parse(strDate);
            } catch (final RuntimeException | ParseException e) {
                LOGGER.warn("Failed to convertStringToDate", e);
            }
        }
        return dateConverted;
    }

    /**
     * Gets the SQL Date from String
     *
     * @param dateStr    The String Date
     * @param dateFormat The Format of the Date string
     * @return java.sql.Date Object
     */
    public static java.sql.Date getSqlDateFromString(String dateStr, String dateFormat) {
        final DateFormat formatter = (StringUtils.isEmpty(dateFormat) ? DateUtil.DEFAULT_DATE_FORMAT_HOLDER.get() : new SimpleDateFormat(dateFormat));
        Date date = null;
        try {
            date = formatter.parse(dateStr);
        } catch (ParseException e) {
            LOGGER.warn("Failed to convertSqlDateFromString", e);
        }
        java.sql.Date sqlStartDate = new java.sql.Date(date.getTime());
        return sqlStartDate;
    }

    /**
     * This method returns the current Date in the form of a java.sql.Date Object
     *
     * @return java.sql.Date Object
     */
    public static java.sql.Date getCurrentSqlDate() {
        return new java.sql.Date(Calendar.getInstance().getTime().getTime());
    }

    public static java.sql.Date decrementDaysFromCurrentSqlDate(int numberOfDays) {
        Date utilDate = new Date(Calendar.getInstance().getTime().getTime());
        utilDate = decrementDays(utilDate, numberOfDays);
        return new java.sql.Date(utilDate.getTime());
    }

    /**
     * Converts {@code XMLGregorianCalendar} object to {@code String} representation.
     *
     * @param calendar the calendar
     * @param format   the expected date format
     * @return {@code String} representation of the calendar
     */
    public static String convertXMLGregorianCalendarToString(final XMLGregorianCalendar calendar, final String format) {
        String dateConverted = null;
        if (null != calendar) {
            try {
                final DateFormat formatter = (StringUtils.isEmpty(format) ? DEFAULT_DATE_FORMAT_HOLDER.get() : new SimpleDateFormat(format));
                dateConverted = formatter.format(calendar.toGregorianCalendar().getTime());
            } catch (final RuntimeException e) {
                LOGGER.warn("Failed to convertXMLGregorianCalendarToString", e);
            }
        }
        return dateConverted;
    }

    /**
     * Checks if the date is in the past.
     *
     * @param date the date object
     * @return true if the date is in the past, otherwise false
     */
    public static boolean isDateInThePast(final Date date) {
        final DateTime currentDate = new DateTime();
        return currentDate.isAfter(date.getTime());
    }

    /**
     * Decrements specified number of minutes from the date.
     *
     * @param date    the date
     * @param minutes the minutes
     * @return the updated date object
     */
    public static Date decrementMinutes(final Date date, final int minutes) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, -minutes);
        return cal.getTime();
    }

    /**
     * Increments specified number of minutes to the date.
     *
     * @param date    the date
     * @param minutes the minutes
     * @return the updated date object
     */
    public static Date incrementMinutes(final Date date, final int minutes) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, minutes);
        return cal.getTime();
    }

    /**
     * Decrement days
     *
     * @param date date
     * @param days a number of days to be subtracted from date
     * @return date
     */
    public static Date decrementDays(Date date, int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, -days);
        return calendar.getTime();
    }

    /**
     * Increment days
     *
     * @param date date
     * @param days a number of days to be added  from date
     * @return date
     */
    public static Date incrementDays(Date date, int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, days);
        return calendar.getTime();
    }

    /**
     * Converts a {@code Date} object to {@code XMLGregorianCalendar}.
     *
     * @param date the date
     * @return {@code XMLGregorianCalendar} representation of the {@code Date}, or null if some exception happens during the conversion
     */
    public static XMLGregorianCalendar toXMLGregorianCalendar(final Date date) {
        XMLGregorianCalendar dateConverted = null;
        if (date != null) {
            try {
                final GregorianCalendar calendar = new GregorianCalendar();
                calendar.setTime(date);
                final XMLGregorianCalendar xmlGregorianCalendar = (datatypeFactory == null ? DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar) : datatypeFactory.newXMLGregorianCalendar(calendar));
                xmlGregorianCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
                dateConverted = xmlGregorianCalendar;
            } catch (final DatatypeConfigurationException e) {
                LOGGER.error("Failed to convertToXMLGregorianCalendar", e);
            }
        }
        return dateConverted;
    }

    /**
     * Converts a {@code Date} object to {@code XMLGregorianCalendar} using 'Year', 'Month' and 'Day' values.
     *
     * @param date the date
     * @return {@code XMLGregorianCalendar} representation of the {@code Date}, or null if some exception happens during the conversion
     */
    public static XMLGregorianCalendar toXMLGregorianCalendarYMD(final Date date) {
        XMLGregorianCalendar dateConverted = null;
        if (date != null) {
            try {
                final Calendar calendar = new GregorianCalendar();
                calendar.setTime(date);
                final int year = calendar.get(Calendar.YEAR);
                final int month = calendar.get(Calendar.MONTH) + 1;
                final int day = calendar.get(Calendar.DATE);
                final XMLGregorianCalendar xmlGregorianCalendar = (datatypeFactory == null ? DatatypeFactory.newInstance().newXMLGregorianCalendar() : datatypeFactory.newXMLGregorianCalendar());
                xmlGregorianCalendar.setYear(year);
                xmlGregorianCalendar.setMonth(month);
                xmlGregorianCalendar.setDay(day);
                return xmlGregorianCalendar;
            } catch (final DatatypeConfigurationException e) {
                LOGGER.error("Failed to toXMLGregorianCalendarYMD", e);
            }
        }
        return dateConverted;
    }

    /**
     * Converts a date represented as {@code String} to {@code XMLGregorianCalendar} object using {@link #DEFAULT_DATE_FORMAT} formatting.
     *
     * @param date the date
     * @return result of {@link #toXMLGregorianCalendar(String, String)} execution with {@link #DEFAULT_DATE_FORMAT} format
     */
    public static XMLGregorianCalendar toXMLGregorianCalendar(final String date) {
        return toXMLGregorianCalendar(DEFAULT_DATE_FORMAT, date);
    }

    /**
     * Converts a date represented as {@code String} to {@code XMLGregorianCalendar} object using 'Year', 'Month' and 'Day' values.
     *
     * @param date the date
     * @return result of {@link #toXMLGregorianCalendarYMD(Date)} execution with {@link #DEFAULT_DATE_FORMAT} format
     */
    public static XMLGregorianCalendar toXMLGregorianCalendarYMD(final String date) {
        return toXMLGregorianCalendarYMD(DEFAULT_DATE_FORMAT, date);
    }

    /**
     * Converts a date represented as {@code String} to {@code XMLGregorianCalendar} object using specified date format.
     *
     * @param format the date format
     * @param date   the date
     * @return the {@code XMLGregorianCalendar} instance, if {@code format} and {@code date} are not empty; otherwise, {@code null}
     */
    public static XMLGregorianCalendar toXMLGregorianCalendar(final String format, final String date) {
        XMLGregorianCalendar dateConverted = null;
        if (!StringUtils.isEmpty(format) && !StringUtils.isEmpty(date)) {
            dateConverted = toXMLGregorianCalendar(convertStringToDate(format, date));
        }
        return dateConverted;
    }

    /**
     * Converts a date represented as {@code String} to {@code XMLGregorianCalendar} object using specified date format and 'Year', 'Month' and 'Day' values.
     *
     * @param format the date format
     * @param date   the date
     * @return the {@code XMLGregorianCalendar} instance, if {@code format} and {@code date} are not empty; otherwise, {@code null}
     */
    public static XMLGregorianCalendar toXMLGregorianCalendarYMD(final String format, final String date) {
        return toXMLGregorianCalendarYMD(convertStringToDate(format, date));
    }

    /**
     * XMLGregorianCalendar to date
     *
     * @param calendar XMLGregorianCalendar
     * @return date
     */
    public static Date toDate(final XMLGregorianCalendar calendar) {
        Date dateConverted = null;
        if (calendar != null) {
            dateConverted = calendar.toGregorianCalendar().getTime();
        }
        return dateConverted;
    }

    /**
     * Gets difference in days throwing a RuntimeException if at least one date is not set.
     *
     * @param start start date
     * @param end   end date
     * @return difference in days
     */
    public static int getDifferenceInDays(final Date start, final Date end) {
        if (start == null || end == null) {
            throw new RuntimeException("Both start and end dates can not be null");
        }
        final DateTime startDateTime = new DateTime(start);
        final DateTime endDateTime = new DateTime(end);

        return Days.daysBetween(startDateTime, endDateTime).getDays();
    }

    /**
     * Gets difference in days throwing a RuntimeException if at least one date is not set.
     *
     * @param start java.sql.Date StartDate
     * @param end   java.sql.Date EndDate
     * @return
     */
    public static int getDifferenceBtwnSqlDates(final java.sql.Date start, final java.sql.Date end) {
        if (start == null || end == null) {
            throw new RuntimeException("Both start and end dates can not be null");
        }
        final DateTime startDateTime = new DateTime(start);
        final DateTime endDateTime = new DateTime(end);

        return Days.daysBetween(startDateTime, endDateTime).getDays();
    }

    /**
     * Gets the difference between two dates as {@code int}.
     *
     * @param from the first date
     * @param to   the second date
     * @return the days between
     */
    public static int getDaysBetween(final Date from, final Date to) {
        return Days.daysBetween(new DateTime(from), new DateTime(to)).getDays();
    }

    /**
     * Gets the difference between two dates represented as calendars.
     *
     * @param cal1 the first date
     * @param cal2 the second date
     * @return the days between
     */
    public static long getDaysBetween(final Calendar cal1, final Calendar cal2) {
        return Days.daysBetween(new DateTime(cal1), new DateTime(cal2)).getDays();
    }


    /**
     * Sets timezone to default if it is undefined.
     *
     * @param xmlGregorianCalendar the xml gregorian calendar
     */
    public static XMLGregorianCalendar setTimeZoneIfUndefined(final XMLGregorianCalendar xmlGregorianCalendar) {
        if (xmlGregorianCalendar == null) {
            return null;
        }
        final XMLGregorianCalendar calendar = (XMLGregorianCalendar) xmlGregorianCalendar.clone();
        if (calendar != null && calendar.getTimezone() == DatatypeConstants.FIELD_UNDEFINED) {
            calendar.setTimezone((int) TimeUnit.MILLISECONDS.toMinutes(TimeZone.getDefault().getRawOffset()));
        }

        return calendar;
    }

    /**
     * Converts XML gregorian calendar to joda date time.
     *
     * @param date the XML gregorian calendar
     * @return the joda date time
     */
    public static DateTime toJodaDateTime(final XMLGregorianCalendar date) {
        if (date == null) {
            return null;
        }

        return new DateTime(date.toGregorianCalendar().getTime());
    }

    /**
     * Converts XML gregorian calendar to joda date time.
     *
     * @param date the XML gregorian calendar
     * @return the joda date time
     */
    public static DateMidnight toJodaDateMidnight(final XMLGregorianCalendar date) {
        if (date == null) {
            return null;
        }

        return new DateTime(date.toGregorianCalendar().getTime()).toDateMidnight();
    }

    /**
     * Verifies if the date is within the specified range.
     *
     * @param dateToVerify the date to verify.
     * @param fromDate     the date to start the verification.
     * @param endDate      the date to end the verification.
     * @return true if the date is within the range.
     */
    public static boolean isJodaDateWithinRanges(final DateTime dateToVerify, final DateTime fromDate, final DateTime endDate) {
        if (dateToVerify != null && fromDate != null && endDate != null) {
            if (dateToVerify.toDateMidnight().isEqual(fromDate.toDateMidnight())) {
                return true;
            }
            if (dateToVerify.toDateMidnight().isAfter(fromDate.toDateMidnight())
                    && dateToVerify.toDateMidnight().isBefore(endDate.toDateMidnight())) {
                return true;
            }
        }
        return false;
    }


    /**
     * Previous midnight date
     *
     * @param calendar the gregorian calendar instance
     * @return the {@link Date} that corresponds to the previous midnight
     */
    public static Date previousMidnightDate(final GregorianCalendar calendar) {
        GregorianCalendar previousMidnight = new GregorianCalendar(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        return new Date(previousMidnight.getTimeInMillis());
    }

    /**
     * Check if the dates are equal
     *
     * @param date1 first date
     * @param date2 second date
     * @return true, if dates are equal
     */
    public static boolean equals(final Date date1, final Date date2) {
        return date1 == null ? date2 == null : date1.equals(date2);
    }

    /**
     * Checks if date1 is before or equals date2. Null is always before any other date. If both dates are null they are considered equal.
     * <p>
     * DateUtil.beforeOrEquals(null, date) = true
     * DateUtil.beforeOrEquals(date, null) = false
     * DateUtil.beforeOrEquals(null, null) = true
     *
     * @param date1 first date
     * @param date2 second date
     * @return true, if the first date is before or equals the second one, or both dates are null
     */
    public static boolean beforeOrEquals(final Date date1, final Date date2) {
        if (date1 == date2) {
            return true;
        }

        if (date1 == null) {
            return true;
        }

        return date2 != null && (date1.before(date2) || date1.equals(date2));
    }

    /**
     * Checks if date1 is after or equals date2. Null is always before any other date. If both dates are null they are considered equal.
     * <p>
     * DateUtil.afterOrEquals(null, date) = false
     * DateUtil.afterOrEquals(date, null) = true
     * DateUtil.afterOrEquals(null, null) = true
     *
     * @param date1 first date
     * @param date2 second date
     * @return true, if the first date is after or equals the second one, or both dates are null
     */
    public static boolean afterOrEquals(final Date date1, final Date date2) {
        if (date1 == date2) {
            return true;
        }

        if (date1 == null) {
            return false;
        }

        return date2 == null || (date1.after(date2) || date1.equals(date2));
    }

    /**
     * Checks if date1 is after date2. Null is always before any other date. If both dates are null they are considered equal.
     * <p>
     * DateUtil.after(null, null) = false
     * DateUtil.after(date, null) = true
     * DateUtil.after(null, date) = false
     *
     * @param date1 first date,
     * @param date2 second date.
     * @return true, if the first date is after the second one.
     */
    public static boolean after(final Date date1, final Date date2) {
        if (date1 == date2) {
            return false;
        }

        if (date1 == null) {
            return false;
        }

        return date2 == null || date1.after(date2);
    }

    /**
     * Converts {@link DateMidnight} instance to {@link Date}.
     *
     * @param dateMidnight {@link DateMidnight} object to convert to date.
     * @return {@link Date} object or null if {@code dateMidnight} is null.
     */
    public static Date convertToDate(final DateMidnight dateMidnight) {
        return dateMidnight != null ? dateMidnight.toDate() : null;
    }

    /**
     * This Method takes the negative/postive value of UTC Minutes and returns a String with GMT OffSet Value
     *
     * @param minutesInUtc
     * @return
     */
    public static String getGmtOffsetFromUtcMinutes(int minutesInUtc) {
        boolean isNegativeTime = false;
        if (minutesInUtc < NUMBER_ZERO) {
            isNegativeTime = true;
            minutesInUtc = minutesInUtc * MINUS_ONE;//Make the value positive by mutiplying with -1
        }
        int hours = minutesInUtc / MINUTES_PER_HOUR;
        int minutes = minutesInUtc % MINUTES_PER_HOUR;
        String gmtHours = EMPTY_STRING + hours;
        String gmtMinutes = EMPTY_STRING + minutes;
        if (hours < TEN) {
            gmtHours = ZERO_STRING + hours;
        }
        if (minutes < TEN) {
            gmtMinutes = ZERO_STRING + minutes;
        }
        String gmtOffset = gmtHours + ":" + gmtMinutes;
        if (isNegativeTime) {
            gmtOffset = MINUS_SIGN + gmtOffset;
        } else {
            gmtOffset = PLUS_SIGN + gmtOffset;
        }
        LOGGER.debug("Returning GMT Offset as  = " + gmtOffset);
        return gmtOffset;
    }

    /* Returns the String format and timezone as requested
     * @param dateFormat
     * @param timezone
     * @return
     */
    public static final String getCurrentDateTimeByFormatZone(String dateFormat, String timezone) {
        final SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setTimeZone(TimeZone.getTimeZone(timezone));
        final String utcTime = sdf.format(new Date());
        return utcTime;
    }

    /**
     * @param format the format
     * @param date   the date
     */
    public static String convertLocalDateToString(final String format, final LocalDate date, Locale locale) {
        String dateConverted = null;
        if (null != date) {
            try {
                final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format).withLocale(locale);
                dateConverted = date.format(dateTimeFormatter);
            } catch (final RuntimeException e) {
                LOGGER.warn("Failed to convertDateToString", e);
            }
        }
        return dateConverted;
    }

    /**
     * Converts {@code String} representation of date to {@code Date} object.
     *
     * @param format  the date format
     * @param strDate the date in {@code String} format
     * @return {@code Date} representation of the date, or null
     */
    public static LocalDate convertStringToLocalDate(final String format, final String strDate) {
        LocalDate dateConverted = null;
        if (null != strDate) {
            try {
                dateConverted = LocalDate.parse(strDate, DateTimeFormatter.ofPattern(format));
            } catch (final RuntimeException e) {
                LOGGER.warn("Failed to convertStringToDate", e);
            }
        }
        return dateConverted;
    }

    /**
     * @param start the start date
     * @param end   the end date
     */
    public static long getDifferenceInLocalDays(final LocalDate start, final LocalDate end) {
        if (start == null || end == null) {
            throw new RuntimeException("Both start and end dates can not be null");
        }
        return Duration.between(start.atStartOfDay(), end.atStartOfDay()).toDays();
    }

    /**
     * @param date the date
     */
    public static LocalDate convertDateToLocalDate(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }
    
	public static String getDate(Date date) {
		SimpleDateFormat DateFor = new SimpleDateFormat("E, dd MMM yyyy");
		String paidDate = DateFor.format(date);
		return paidDate;
	}
   

}