package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.SubCategoryDTO;
import com.gts.cms.common.util.FileUtil;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.Category;
import com.gts.cms.entity.Organization;
import com.gts.cms.entity.SubCategory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class SubCategoryMapper implements BaseMapper<SubCategory,SubCategoryDTO>{

	@Override
	public SubCategoryDTO convertEntityToDTO(SubCategory entityObject) {
		SubCategoryDTO dtoObject = null;
		if(entityObject != null) {
			dtoObject = new SubCategoryDTO();
			dtoObject.setSubCategoryId(entityObject.getSubcategoryId());
			if(entityObject.getOrganization() != null) {
				  dtoObject.setOrganizationId(entityObject.getOrganization().getOrganizationId());
				  dtoObject.setOrgCode(entityObject.getOrganization().getOrgCode());
				  dtoObject.setOrgName(entityObject.getOrganization().getOrgName());
		
				}
			dtoObject.setAchievementSubcategory(entityObject.getAchievementSubcategory());
			dtoObject.setAchievementSubcategoryCode(entityObject.getAchievementSubcategoryCode());
			if (entityObject.getSubcategoryLogoPath() != null) {

				dtoObject.setSubcategoryLogoPath(FileUtil.getAbsolutePath(entityObject.getSubcategoryLogoPath()));
			}
			dtoObject.setIsActive(entityObject.getIsActive());
			dtoObject.setReason(entityObject.getReason());
			dtoObject.setCreatedDt(entityObject.getCreatedDt());
			dtoObject.setCreatedUser(entityObject.getCreatedUser());
		dtoObject.setUpdatedDt(entityObject.getUpdatedDt());
		dtoObject.setUpdatedUser(entityObject.getUpdatedUser());
		if(entityObject.getCategory() != null) {
			  dtoObject.setCategoryId(entityObject.getCategory().getCategoryId());
			  dtoObject.setAchievementCategoryCode(entityObject.getCategory().getAchievementCategoryCode());
			}
		}
		return dtoObject;
	}

	@Override
	public List<SubCategoryDTO> convertEntityListToDTOList(List<SubCategory> entityObjectList) {
		List<SubCategoryDTO> dtoObjectList = new ArrayList<>();
		entityObjectList.forEach(entityObject -> dtoObjectList.add(convertEntityToDTO(entityObject)));
		return dtoObjectList;
	}

	@Override
	public List<SubCategory> convertDTOListToEntityList(List<SubCategoryDTO> dtoObjectList) {
		List<SubCategory> entityObjectList = new ArrayList<>();
		dtoObjectList.forEach(dtoObject -> entityObjectList.add(convertDTOtoEntity(dtoObject, null)));
		return entityObjectList;
	}

	@Override
	public SubCategory convertDTOtoEntity(SubCategoryDTO dtoObject, SubCategory entityObject) {
		if (null == entityObject) {
			entityObject = new  SubCategory();
			entityObject.setIsActive(Boolean.TRUE);
			entityObject.setCreatedDt(new Date());
			entityObject.setCreatedUser(SecurityUtil.getCurrentUser());
		} else {
			entityObject.setIsActive(dtoObject.getIsActive());
		}
		entityObject.setSubcategoryId(dtoObject.getSubCategoryId());
		
		if (dtoObject.getOrganizationId() != null) {
			Organization organization = new Organization();
			organization.setOrganizationId(dtoObject.getOrganizationId());
			
			entityObject.setOrganization(organization);
		}
		entityObject.setAchievementSubcategory(dtoObject.getAchievementSubcategory());
		entityObject.setAchievementSubcategoryCode(dtoObject.getAchievementSubcategoryCode());
		entityObject.setSubcategoryLogoPath(dtoObject.getSubcategoryLogoPath());
		entityObject.setReason(dtoObject.getReason());
		entityObject.setUpdatedDt(dtoObject.getUpdatedDt());
		entityObject.setUpdatedUser(dtoObject.getUpdatedUser());
		if (dtoObject.getCategoryId() != null) {
			Category category = new Category();
			category.setCategoryId(dtoObject.getCategoryId());
			
			entityObject.setCategory(category);
		}
		
		return entityObject;
	}

	

}
