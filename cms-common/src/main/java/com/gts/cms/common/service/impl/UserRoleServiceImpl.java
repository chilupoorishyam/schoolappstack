package com.gts.cms.common.service.impl;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.UserRoleDTO;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.mapper.UserRoleMapper;
import com.gts.cms.common.repository.UserRoleRepository;
import com.gts.cms.common.service.UserRoleService;
import com.gts.cms.entity.UserRole;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class UserRoleServiceImpl implements UserRoleService {

	private static final Logger LOGGER = Logger.getLogger(UserRoleServiceImpl.class);

	@Autowired
	private UserRoleMapper userRoleMapper;

	@Autowired
    UserRoleRepository userRoleRepository;

	@Override
	@Transactional
	public ApiResponse<Long> addUserRole(List<UserRoleDTO> userRoleDTOs) {
		ApiResponse<Long> response = null;
		LOGGER.info("UserRoleServiceImpl.addUserRole()");
		try {
			List<UserRole> userRole = userRoleMapper.convertDTOListToEntityList(userRoleDTOs);

			userRole = userRoleRepository.saveAll(userRole);
			userRoleRepository.flush();
			if (userRole != null) {
				response = new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(), HttpStatus.OK.value());
			} else {
				throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue());
			}
		} catch (Exception e) {
			LOGGER.error("UserRoleServiceImpl.addUserRole()");
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}
		return response;
	}

}
