package com.gts.cms.common.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class SubCasteDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long subCasteId;
	@NotNull(message = "Caste Id is required.")
	private Long casteId;

	private String caste;

	private String subCaste;

	private Integer sortOrder;

	private Boolean isEligibleForReservation;
	@NotNull(message = "IsActive is required.")
	private Boolean isActive;

	public Long getSubCasteId() {
		return subCasteId;
	}

	public void setSubCasteId(Long subCasteId) {
		this.subCasteId = subCasteId;
	}

	public Long getCasteId() {
		return casteId;
	}

	public void setCasteId(Long casteId) {
		this.casteId = casteId;
	}

	public String getCaste() {
		return caste;
	}

	public void setCaste(String caste) {
		this.caste = caste;
	}

	public String getSubCaste() {
		return subCaste;
	}

	public void setSubCaste(String subCaste) {
		this.subCaste = subCaste;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Boolean getIsEligibleForReservation() {
		return isEligibleForReservation;
	}

	public void setIsEligibleForReservation(Boolean isEligibleForReservation) {
		this.isEligibleForReservation = isEligibleForReservation;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}
