package com.gts.cms.common.exception;

public class MalformedQueryException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MalformedQueryException() {
		super();
	}

	public MalformedQueryException(String message, Throwable cause) {
		super(message, cause);
	}

	public MalformedQueryException(String message) {
		super(message);
	}

}
