package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

public class StudentDiscontinueRequestDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull(message = "Student is required")
	private Long studentId;

	@NotNull(message = "School is required")
	private Long schoolId;

	private String reason;

	private Long academicYearId;

	private Long courseId;


	private Long courseYearId;

	private Long groupSectionId;

	private Long quotaId;
	
	private Date toDate;
	private Date fromDate;

	public Long getStudentId() {
		return studentId;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public String getReason() {
		return reason;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Long getAcademicYearId() {
		return academicYearId;
	}

	public Long getCourseId() {
		return courseId;
	}



	public Long getCourseYearId() {
		return courseYearId;
	}

	public Long getGroupSectionId() {
		return groupSectionId;
	}

	public void setGroupSectionId(Long groupSectionId) {
		this.groupSectionId = groupSectionId;
	}

	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}


	public void setCourseYearId(Long courseYearId) {
		this.courseYearId = courseYearId;
	}

	public Long getQuotaId() {
		return quotaId;
	}

	public void setQuotaId(Long quotaId) {
		this.quotaId = quotaId;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

}
