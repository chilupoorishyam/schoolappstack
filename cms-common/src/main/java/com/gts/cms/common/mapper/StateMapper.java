package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.StateDTO;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.Country;
import com.gts.cms.entity.State;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * created by sathish on 08/09/2018
 */
@Component
public class StateMapper implements BaseMapper<State, StateDTO>{

	public StateDTO convertEntityToDTO(State state) {
		StateDTO stateDTO = null;
		if (state != null) {
			stateDTO = new StateDTO();
			stateDTO.setStateId(state.getStateId());
			stateDTO.setStateCode(state.getStateCode());
			stateDTO.setStateName(state.getStateName());
			
			if (state.getCountry().getCountryId() != null) {
				stateDTO.setCountryId(state.getCountry().getCountryId());
			}		}
		return stateDTO;
	}

	public State convertDTOtoEntity(StateDTO stateDTO,State state) {
		if (null == state) {
			state = new State();
			state.setIsActive(Boolean.TRUE);
			state.setCreatedDt(new Date());
			state.setCreatedUser(SecurityUtil.getCurrentUser());
		}
		
		state.setStateId(stateDTO.getStateId());
		state.setStateName(stateDTO.getStateName());
		state.setStateCode(stateDTO.getStateCode());
		state.setCreatedDt(new Date());
		state.setUpdatedUser(SecurityUtil.getCurrentUser());
		state.setIsActive(stateDTO.getIsActive());
		if (stateDTO.getCountryId() != null) {
			Country country = new Country();
			country.setCountryId(stateDTO.getCountryId());
			state.setCountry(country);
		}
     	
		return state;
	}

	public List<State> convertDTOListToEntityList(List<StateDTO> stateDTOList) {
		List<State> stateList = new ArrayList<>();
		stateDTOList.forEach(stateDTO -> stateList.add(convertDTOtoEntity(stateDTO,null)));
		return stateList;

	}

	public List<StateDTO> convertEntityListToDTOList(List<State> stateList) {
		List<StateDTO> stateDTOList = new ArrayList<>();
		stateList.forEach(state -> stateDTOList.add(convertEntityToDTO(state)));
		return stateDTOList;
	}
}
