package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.entity.QConfigAutonumber;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.repository.custom.ConfigAutonumberRepositoryCustom;
import com.gts.cms.entity.ConfigAutonumber;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ConfigAutonumberRepositoryImpl extends QuerydslRepositorySupport
		implements ConfigAutonumberRepositoryCustom {

	public ConfigAutonumberRepositoryImpl() {
		super(ConfigAutonumber.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public Long deleteConfigAutoNumber(Long autoconfigId) {
		return update(QConfigAutonumber.configAutonumber).where(QConfigAutonumber.configAutonumber.autoconfigId.eq(autoconfigId))
				.set(QConfigAutonumber.configAutonumber.isActive, Status.INACTIVE.getId()).execute();
	}

	@Override
	public List<ConfigAutonumber> findAllConfigAutoNumbers() {
		
		JPQLQuery<ConfigAutonumber> query = from(QConfigAutonumber.configAutonumber).orderBy(QConfigAutonumber.configAutonumber.createdDt.asc());
		return query.fetch();
	}
	
	//@Override
	public ConfigAutonumber findSchoolConfigAutoNumbers(Long schoolId,String configAtttributeCode) {
		JPQLQuery<ConfigAutonumber> query = from(QConfigAutonumber.configAutonumber).where(
				QConfigAutonumber.configAutonumber.school.schoolId.eq(schoolId).and(QConfigAutonumber.configAutonumber.configAtttributeCode.eq(configAtttributeCode))
						.and(QConfigAutonumber.configAutonumber.isActive.eq(Boolean.TRUE)));
		return query.fetchOne();
	}
	
	public ConfigAutonumber findConfigAutoNumberByCode(String configAtttributeCode) {
		JPQLQuery<ConfigAutonumber> query = from(QConfigAutonumber.configAutonumber).where(QConfigAutonumber.configAutonumber.configAtttributeCode.eq(configAtttributeCode)
						.and(QConfigAutonumber.configAutonumber.isActive.eq(Boolean.TRUE)));
		return query.fetchOne();
	}

	@Override
	public Long updateCurrentNumber(ConfigAutonumber configAutoNumberObj) {
		return update(QConfigAutonumber.configAutonumber).where(QConfigAutonumber.configAutonumber.autoconfigId.eq(configAutoNumberObj.getAutoconfigId()))
				.set(QConfigAutonumber.configAutonumber.currentNumber,configAutoNumberObj.getCurrentNumber() ).execute();
	}

}
