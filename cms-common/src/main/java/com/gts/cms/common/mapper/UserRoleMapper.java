package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.UserRoleDTO;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.Role;
import com.gts.cms.entity.User;
import com.gts.cms.entity.UserRole;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class UserRoleMapper implements BaseMapper<UserRole, UserRoleDTO> {
@Override
public UserRoleDTO convertEntityToDTO(UserRole userRole) {
	UserRoleDTO userRoleDTO = null;
	if (userRole != null) {
	userRoleDTO = new UserRoleDTO();
	  userRoleDTO.setCreatedDt(userRole.getCreatedDt());
	  userRoleDTO.setUpdatedDt(userRole.getUpdatedDt());
	  userRoleDTO.setUpdatedUser(userRole.getUpdatedUser());
	  userRoleDTO.setIsActive(userRole.getIsActive());
	  userRoleDTO.setCreatedUser(userRole.getCreatedUser());
	  userRoleDTO.setUserRoleId(userRole.getUserRoleId());
	  if(userRole.getUser()!= null){
		  userRoleDTO.setUserId(userRole.getUser().getUserId());
		  userRoleDTO.setUserName(userRole.getUser().getUserName());
	}
	  if(userRole.getRole()!= null){
		  userRoleDTO.setRoleId(userRole.getRole().getRoleId());
		  userRoleDTO.setRoleName(userRole.getRole().getRoleName());
	}
	  
	}
	return userRoleDTO;
}

	@Override
	public UserRole convertDTOtoEntity(UserRoleDTO userRoleDTO, UserRole userRole) {
		if (userRoleDTO != null) {
			if (userRole == null) {
				userRole = new UserRole();
				if (userRoleDTO.getIsActive() != null) {
					userRole.setIsActive(userRoleDTO.getIsActive());
				} else {
					userRole.setIsActive(Status.ACTIVE.getId());
				}
				userRole.setCreatedDt(new Date());
				userRole.setCreatedUser(SecurityUtil.getCurrentUser());
			} else {
				userRole.setIsActive(userRoleDTO.getIsActive());
			}
			userRole.setUpdatedDt(new Date());
			userRole.setUpdatedUser(SecurityUtil.getCurrentUser());
			userRole.setUserRoleId(userRoleDTO.getUserRoleId());
			if (userRoleDTO.getUserId() != null) {
				User user = new User();
				user.setUserId(userRoleDTO.getUserId());
				userRole.setUser(user);
			}
			if (userRoleDTO.getRoleId() != null) {
				Role role = new Role();
				role.setRoleId(userRoleDTO.getRoleId());
				userRole.setRole(role);
			}
		}
		return userRole;
	}
@Override
public List<UserRole> convertDTOListToEntityList(List<UserRoleDTO> userRoleDTOList) {
	List<UserRole> userRoleList = new ArrayList<>();
	 userRoleDTOList.forEach(userRoleDTO -> userRoleList.add(convertDTOtoEntity(userRoleDTO, null)));
	return userRoleList;
}
@Override
public List<UserRoleDTO> convertEntityListToDTOList(List<UserRole> userRoleList) {
	List<UserRoleDTO> userRoleDTOList = new ArrayList<>();
	 userRoleList.forEach(userRole -> userRoleDTOList.add(convertEntityToDTO(userRole)));
	return userRoleDTOList;
}
}