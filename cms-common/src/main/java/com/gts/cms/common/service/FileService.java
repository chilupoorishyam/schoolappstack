package com.gts.cms.common.service;

import org.springframework.web.multipart.MultipartFile;

public interface FileService {
	public String uploadFile(MultipartFile multipartFile, String fileName);
	String uploadFile(MultipartFile multipartFile, String fileName, Long pkId, String type);
}
