package com.gts.cms.common.student.repository.custom;

import com.gts.cms.entity.StudentDetail;

import java.util.List;

public interface StudentDetailRepositoryCustom {
	public List<StudentDetail> findByStudentData(String studentData);

	public Long updateStudentAdmissionPhotFileNames(Long studentId, String studentAadharFilePath,
			String studentPancardFilePath, String studentPhotoFilePath, String fatherPhotoFilePath,
			String motherPhotoFilePath, String spousePhotoFileName);

	public List<StudentDetail> findAllStudentsDetails(Long orgId, Boolean status);

	public Long deleteStudentAdmissionDetail(Long id);

	/*public List<StudentDetail> findStudentList(Boolean status, Long schoolId, Long courseId, Long courseGroupId,
			Long courseYearId, Long groupSectionId, Long academicYearId);*/

	public List<StudentDetail> findStudentListByStructure(Boolean status, Long schoolId, Long academicYearId,
			Long feeStructureId);

	void updateRegulationAndGroupsection(List<StudentDetail> studentDetailList);

	/*
	 * public List<StudentDetail> findStudentAttendanceList(Boolean status, Long
	 * schoolId, Long courseId, Long courseGroupId, Long courseYearId, Long
	 * groupSectionId, Date attendanceDate);
	 */
	public Long updateStudentAadharFile(Long admissionId, String studentAadharFileName);

	public Long updateStudentPanCard(Long admissionId, String studentPancardFileName);

	public Long updateStudentMotherPhotoFile(Long admissionId, String motherPhotoFileName);

	public Long updateStudentFatherPhotoFile(Long admissionId, String fatherPhotoFileName);

	public Long updateStudentPhotoFile(Long admissionId, String studentPhotoFileName);

	public List<StudentDetail> searchStudentDetails(String firstName, String lastName, String middleName,
			String admissionNo, String mobile, Long schoolId, Long academicYearId,Boolean status,Long courseId,Long courseYearId);

	public Long updateStudentStatus(Long generalDetailId, Long schoolId, Long studentId, String reason,
			Long academicYearId, Long courseYearId, Long groupSectionId);

	public List<StudentDetail> findDetainStudentsList(Boolean status, Long schoolId, Long courseId,
			Long courseYearId, Long groupSectionId, Long academicYearId, Long detailId);

	public Long updateStudentSection(Long schoolId, Long studentId, Long groupSectionId);

	public List<StudentDetail> getBirthdayList(Long organizationId, Long schoolId, Integer day, Integer month);

	public List<StudentDetail> findFeeMappingStudentList(Long schoolId, Long courseYearId, Long academicYearId, Long quotaId);
	
	public StudentDetail findByStudenAdmissionNo(String admissionNo);
	
	public StudentDetail findByStudentRollNo(String rollNo);

	/*Long updateSection(List<Long> studentsId, Long toGroupSectionId, Long toBatchId, Long toCourseYearId, Date toDate,
			Long academicYearId);*/
	
}
