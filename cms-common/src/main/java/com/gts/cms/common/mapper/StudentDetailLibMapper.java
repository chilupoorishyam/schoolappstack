package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.StudentDetailLibDTO;
import com.gts.cms.entity.StudentDetail;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StudentDetailLibMapper implements BaseMapper<StudentDetail, StudentDetailLibDTO> {

	@Override
	public StudentDetailLibDTO convertEntityToDTO(StudentDetail entity) {

		StudentDetailLibDTO studentDetailLibListDTO = null;
		if (entity != null) {
			studentDetailLibListDTO = new StudentDetailLibDTO();
			studentDetailLibListDTO.setStudentId(entity.getStudentId());
			studentDetailLibListDTO.setRollNumber(entity.getRollNumber());
			studentDetailLibListDTO.setStudentName(entity.getFirstName());

			if (entity.getAcademicYear() != null) {
				studentDetailLibListDTO.setAcademicYearId(entity.getAcademicYear().getAcademicYearId());
				studentDetailLibListDTO.setAcademicYear(entity.getAcademicYear().getAcademicYear());
			}

			if (entity.getCourse() != null) {
				studentDetailLibListDTO.setCourseId(entity.getCourse().getCourseId());
				studentDetailLibListDTO.setCourseName(entity.getCourse().getCourseName());
				studentDetailLibListDTO.setCourseCode(entity.getCourse().getCourseCode());
			}



			if (entity.getCourseYear() != null) {
				studentDetailLibListDTO.setCourseYearId(entity.getCourseYear().getCourseYearId());
				studentDetailLibListDTO.setCourseYearCode(entity.getCourseYear().getCourseYearCode());
				studentDetailLibListDTO.setCourseYearName(entity.getCourseYear().getCourseYearName());
			}

			if (entity.getGroupSection() != null) {
				studentDetailLibListDTO.setGroupSectionId(entity.getGroupSection().getGroupSectionId());
				studentDetailLibListDTO.setSection(entity.getGroupSection().getSection());
			}
			
			if(entity.getOrganization()!=null) {
				studentDetailLibListDTO.setOrgId(entity.getOrganization().getOrganizationId());
				studentDetailLibListDTO.setOrgName(entity.getOrganization().getOrgName());
				studentDetailLibListDTO.setOrgCode(entity.getOrganization().getOrgCode());
			}
			
			/*if(entity.getSchool()!=null) {
				studentDetailLibListDTO.setSchoolId(entity.getSchool().getSchoolId());
				studentDetailLibListDTO.setSchoolName(entity.getSchool().getSchoolName());
				studentDetailLibListDTO.setSchoolCode(entity.getSchool().getSchoolCode());
			}*/
		}
		return studentDetailLibListDTO;
	}

	
		

	@Override
	public List<StudentDetailLibDTO> convertEntityListToDTOList(List<StudentDetail> entityList) {
		List<StudentDetailLibDTO> studentDetailLibDTOList = new ArrayList<>();
		entityList.forEach(studentDetail -> studentDetailLibDTOList.add(convertEntityToDTO(studentDetail)));
		return studentDetailLibDTOList;
	}

	@Override
	public List<StudentDetail> convertDTOListToEntityList(List<StudentDetailLibDTO> dtoList) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StudentDetail convertDTOtoEntity(StudentDetailLibDTO d, StudentDetail e) {
		// TODO Auto-generated method stub
		return null;
	}

}
