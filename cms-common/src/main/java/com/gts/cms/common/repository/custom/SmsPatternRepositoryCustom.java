package com.gts.cms.common.repository.custom;

import com.gts.cms.entity.SmsPattern;

/**
 * @author Genesis
 *
 */
public interface SmsPatternRepositoryCustom {

	public SmsPattern getSmsPatternByCode(Long schoolId, String patternCode);
	public SmsPattern getEmailPatternByCode(Long schoolId,String patternCode);
	public SmsPattern getSmsPatternByCode(String patternCode);
	public SmsPattern getSmsPatternByCodeAndClgId(String patternCode,Long schoolId);
}
