package com.gts.cms.common.mapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.gts.cms.common.dto.AuthorizationDTO;
import com.gts.cms.entity.Authorization;
import com.gts.cms.entity.User;

@Component
public class AuthorizationMapper implements BaseMapper<Authorization, AuthorizationDTO> {
	@Override
	public AuthorizationDTO convertEntityToDTO(Authorization entityObject) {
		AuthorizationDTO dtoObject = null;
		if (entityObject != null) {
			dtoObject = new AuthorizationDTO();
			dtoObject.setIsActive(entityObject.getIsActive());
			dtoObject.setCreatedDt(entityObject.getCreatedDt());
			dtoObject.setUpdatedDt(entityObject.getUpdatedDt());
			dtoObject.setAuthorizationId(entityObject.getAuthorizationId());
			dtoObject.setAuthorizationKey(entityObject.getAuthorizationKey());
			dtoObject.setUserAgent(entityObject.getUserAgent());
			dtoObject.setIpAddress(entityObject.getIpAddress());
			dtoObject.setLoginTime(entityObject.getLoginTime());
			dtoObject.setLogoutTime(entityObject.getLogoutTime());
			
			if(entityObject.getUser()!=null) {
				dtoObject.setUserId(entityObject.getUser().getUserId());
				dtoObject.setUserName(entityObject.getUser().getFirstName());
			}
		}
		return dtoObject;
	}

	@Override
	public Authorization convertDTOtoEntity(AuthorizationDTO dtoObject, Authorization entityObject) {
		if (dtoObject != null) {
			if (entityObject == null) {
				entityObject = new Authorization();
				entityObject.setIsActive(Boolean.TRUE);
				entityObject.setCreatedDt(new Date());
			}
			if (dtoObject.getAuthorizationId() != null) {
				entityObject.setCreatedDt(dtoObject.getCreatedDt());
				entityObject.setUpdatedDt(new Date());
				entityObject.setIsActive(dtoObject.getIsActive());

			}
			if(dtoObject.getUserId()!=null) {
				User user =new User();
				user.setUserId(dtoObject.getUserId());
				entityObject.setUser(user);
			}
			entityObject.setAuthorizationId(dtoObject.getAuthorizationId());
			entityObject.setAuthorizationKey(dtoObject.getAuthorizationKey());
			entityObject.setUserAgent(dtoObject.getUserAgent());
			entityObject.setIpAddress(dtoObject.getIpAddress());
			entityObject.setLoginTime(dtoObject.getLoginTime());
			entityObject.setLogoutTime(dtoObject.getLogoutTime());
		}
		return entityObject;
	}

	@Override
	public List<Authorization> convertDTOListToEntityList(List<AuthorizationDTO> authorizationDTOList) {
		List<Authorization> authorizationList = new ArrayList<>();
		authorizationDTOList.forEach(dtoObject -> authorizationList.add(convertDTOtoEntity(dtoObject, null)));
		return authorizationList;
	}

	@Override
	public List<AuthorizationDTO> convertEntityListToDTOList(List<Authorization> authorizationList) {
		List<AuthorizationDTO> authorizationDTOList = new ArrayList<>();
		authorizationList.forEach(entityObject -> authorizationDTOList.add(convertEntityToDTO(entityObject)));
		return authorizationDTOList;
	}
}
