package com.gts.cms.common.repository;

import com.gts.cms.entity.SubjectStg;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubjectStgRepository extends JpaRepository<SubjectStg, Long> {
    List<SubjectStg> findBySubjectCode(@Param("subjectCode") String subjectCode);
}
