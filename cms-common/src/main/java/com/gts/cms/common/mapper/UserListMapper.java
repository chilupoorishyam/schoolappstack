package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.UserListsDTO;
import com.gts.cms.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserListMapper implements BaseMapper<User, UserListsDTO> {

	@Autowired
	UserRoleMapper userRoleMapper;

	/*@Autowired
	StudentDetailListMapper studentDetailListMapper;*/

	@Override
	public UserListsDTO convertEntityToDTO(User user) {
		UserListsDTO userListDTO = null;
		if (user != null) {
			userListDTO = new UserListsDTO();
			userListDTO.setIsActive(user.getIsActive());
			userListDTO.setFirstName(user.getFirstName());
			userListDTO.setUserName(user.getUserName());
			userListDTO.setIsReset(user.getIsReset());
			userListDTO.setLastName(user.getLastName());
			//userListDTO.setUserTypeId(user.getUserType().getUserTypeId());
			userListDTO.setUserId(user.getUserId());
			userListDTO.setIsEditable(user.getIsEditable());
			userListDTO.setMobileNumber(user.getMobileNumber());
			userListDTO.setEmail(user.getEmail());
			userListDTO.setPassword(user.getPassword());
			if (user.getOrganization() != null) {
				userListDTO.setOrganizationId(user.getOrganization().getOrganizationId());
				userListDTO.setOrgCode(user.getOrganization().getOrgCode());
				userListDTO.setOrgName(user.getOrganization().getOrgName());
				
			}
			if (user.getSchool() != null) {
				userListDTO.setSchoolId(user.getSchool().getSchoolId());
				userListDTO.setSchoolName(user.getSchool().getSchoolName());
				userListDTO.setSchoolCode(user.getSchool().getSchoolCode());
			}
			if (user.getUserType() != null) {
				userListDTO.setUserTypeId(user.getUserType().getUserTypeId());
			}

			/*if (user.getEmpDetails() != null) {
				userListDTO.setEmployeeListDTOs(employeeListMapper.convertEntityListToDTOList(user.getEmpDetails()));
			}
			if (user.getStdStudentDetails2() != null) {
				userListDTO.setStudentDetailListDTOs(studentDetailListMapper.convertEntityListToDTOList(user.getStdStudentDetails2()));
			}*/
			if (user.getUserRoles() != null) {
				userListDTO.setUserRolesDTOs(
						userRoleMapper.convertEntityListToDTOList(user.getUserRoles()));
			}
		}
		return userListDTO;
	}

	@Override
	public List<UserListsDTO> convertEntityListToDTOList(List<User> userList) {
		List<UserListsDTO> userDTOList = new ArrayList<>();
		userList.forEach(user -> userDTOList.add(convertEntityToDTO(user)));
		return userDTOList;
	}

	@Override
	public List<User> convertDTOListToEntityList(List<UserListsDTO> dtoList) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User convertDTOtoEntity(UserListsDTO d, User e) {
		// TODO Auto-generated method stub
		return null;
	}
}