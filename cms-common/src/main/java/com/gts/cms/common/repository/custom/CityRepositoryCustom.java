package com.gts.cms.common.repository.custom;

import com.gts.cms.entity.City;

import java.util.List;

/**
 * created by Naveen on 04/09/2018 
 * 
*/
public interface CityRepositoryCustom {
	List<City> getCitiesByDistrictId(Long districtId);
}
