package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.SubjectDTO;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.Course;
import com.gts.cms.entity.GeneralDetail;
import com.gts.cms.entity.School;
import com.gts.cms.entity.Subject;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class SubjectMapper implements BaseMapper<Subject, SubjectDTO> {
private static final Logger LOGGER=Logger.getLogger(SubjectMapper.class);
	@Override
	public SubjectDTO convertEntityToDTO(Subject subject) {
		SubjectDTO subjectDTO = null;
		if (subject != null) {
			subjectDTO = new SubjectDTO();
			subjectDTO.setSubjectId(subject.getSubjectId());
			subjectDTO.setSubjectName(subject.getSubjectName());
			subjectDTO.setSubjectCode(subject.getSubjectCode());
			subjectDTO.setOrderNo(subject.getOrderNo());
			subjectDTO.setSubCreditHrs(subject.getSubCreditHrs());
			subjectDTO.setSubCredits(subject.getSubCredits());
			subjectDTO.setShortName(subject.getShortName());
			subjectDTO.setIsLanguage(subject.getIsLanguage());

			subjectDTO.setIsActive(subject.getIsActive());
			subjectDTO.setReason(subject.getReason());
			subjectDTO.setCreatedDt(subject.getCreatedDt());
			subjectDTO.setCreatedUser(subject.getCreatedUser());
			subjectDTO.setUpdatedDt(subject.getUpdatedDt());
			subjectDTO.setUpdatedUser(subject.getUpdatedUser());

			if (subject.getSchool() != null) {
				subjectDTO.setSchoolId(subject.getSchool().getSchoolId());
				subjectDTO.setSchoolName(subject.getSchool().getSchoolName());
				subjectDTO.setSchoolCode(subject.getSchool().getSchoolCode());
			}

			if (subject.getCourse() != null) {
				subjectDTO.setCourseId(subject.getCourse().getCourseId());
				subjectDTO.setCourseName(subject.getCourse().getCourseName());
				subjectDTO.setCourseCode(subject.getCourse().getCourseCode());
			}
		/*	if(subject.getSubjectregulations()!=null && subject.getSubjectregulations().size()>0)
			{
				for(Subjectregulation subjectregulation:subject.getSubjectregulations())
				{
				subjectDTO.setSubjectRegulationId(subjectregulation.getSubjectRegulationId());
				LOGGER.info("SubjectMapper.convertEntityToDTO()"+subjectregulation.getSubjectRegulationId());
				subjectDTO.setRegulationId(subjectregulation.getRegulation().getRegulationId());
				subjectDTO.setRegulationName(subjectregulation.getRegulation().getRegulationName());
				subjectDTO.setRegulationCode(subjectregulation.getRegulation().getRegulationCode());
				
			}
			}*/
			if (subject.getSubjecttype() != null) {
				subjectDTO.setSubjectTypeId(subject.getSubjecttype().getGeneralDetailId());
				subjectDTO.setSubjectTypeName(subject.getSubjecttype().getGeneralDetailDisplayName());
			}

			if (subject.getSubjectCategory() != null) {
				subjectDTO.setSubjectCategoryId(subject.getSubjectCategory().getGeneralDetailId());
				subjectDTO.setSubjectCategoryName(subject.getSubjectCategory().getGeneralDetailDisplayName());
			}
			
		}
		return subjectDTO;
	}

	@Override
	public List<SubjectDTO> convertEntityListToDTOList(List<Subject> subjectList) {

		List<SubjectDTO> subjectDTOList = new ArrayList<>();
		subjectList.forEach(subject -> subjectDTOList.add(convertEntityToDTO(subject)));
		return subjectDTOList;
	}

	@Override
	public List<Subject> convertDTOListToEntityList(List<SubjectDTO> subjectDtoList) {

		List<Subject> subjectList = new ArrayList<>();
		subjectDtoList.forEach(subjectDTO -> subjectList.add(convertDTOtoEntity(subjectDTO, null)));
		return subjectList;

	}

	@Override
	public Subject convertDTOtoEntity(SubjectDTO subjectDTO, Subject subject) {
		if (null == subject) {
			subject = new Subject();
			subject.setCreatedDt(new Date());
			subject.setIsActive(Status.ACTIVE.getId());
			subject.setCreatedUser(SecurityUtil.getCurrentUser());
		}
		else {
			subject.setIsActive(subjectDTO.getIsActive());
		}
		if (subjectDTO.getSchoolId() != null) {
			School school = new School();
			school.setSchoolId(subjectDTO.getSchoolId());
			school.setSchoolName(subjectDTO.getSchoolName());
			subject.setSchool(school);
		}

		if (subjectDTO.getCourseId() != null) {
			Course course = new Course();
			course.setCourseId(subjectDTO.getCourseId());
			course.setCourseName(subjectDTO.getCourseName());
			subject.setCourse(course);
		}

		subject.setSubjectName(subjectDTO.getSubjectName());
		subject.setSubjectCode(subjectDTO.getSubjectCode());
		subject.setOrderNo(subjectDTO.getOrderNo());

		if (subjectDTO.getSubjectTypeId() != null) {
			GeneralDetail subjectType = new GeneralDetail();
			subjectType.setGeneralDetailId(subjectDTO.getSubjectTypeId());
			subjectType.setGeneralDetailDisplayName(subjectDTO.getSubjectTypeName());
			subject.setSubjecttype(subjectType);
		}

		if (subjectDTO.getSubjectCategoryId() != null) {
			GeneralDetail subjectCategory = new GeneralDetail();
			subjectCategory.setGeneralDetailId(subjectDTO.getSubjectCategoryId());
			subjectCategory.setGeneralDetailDisplayName(subjectDTO.getSubjectCategoryName());
			subject.setSubjectCategory(subjectCategory);
		}

		subject.setSubCredits(subjectDTO.getSubCredits());

		subject.setSubCreditHrs(subjectDTO.getSubCreditHrs());

		subject.setShortName(subjectDTO.getShortName());

		subject.setIsLanguage(subjectDTO.getIsLanguage());
		subject.setReason(subjectDTO.getReason());
		subject.setUpdatedDt(new Date());
		subject.setUpdatedUser(SecurityUtil.getCurrentUser());

		return subject;
	}

}
