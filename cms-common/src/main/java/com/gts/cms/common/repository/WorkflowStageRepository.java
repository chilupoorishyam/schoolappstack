package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.WorkflowStageRepositoryCustom;
import com.gts.cms.entity.WorkflowStage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkflowStageRepository extends JpaRepository<WorkflowStage, Long>, WorkflowStageRepositoryCustom {
	
}
