package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.UserRepositoryCustom;
import com.gts.cms.entity.QUser;
import com.gts.cms.entity.User;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * created by Raghu on 02/09/2018
 * 
 */

public class UserRepositoryImpl extends QuerydslRepositorySupport implements UserRepositoryCustom {
	@PersistenceContext
	private EntityManager em;

	public UserRepositoryImpl() {
		super(User.class);
	}

	@Override
	public User getUserDetails(Long userId,Boolean status) {
		/*JPQLQuery<User> query = from(user).join(user.TSecUserRoles, userRole).fetchJoin().join(userRole.TSecRole, role)
				.fetchJoin().join(role.TSecRolePrivileges, rolePrivilege).fetchJoin().join(rolePrivilege.TSecPage, page)
				.fetchJoin().join(rolePrivilege.Module, module).fetchJoin().join(rolePrivilege.Submodule, submodule)
				.fetchJoin();
		query.where(user.userId.eq(userId)).where(user.isActive.eq(Status.ACTIVE.getId()))
				.where(role.isActive.eq(Status.ACTIVE.getId())).where(rolePrivilege.isActive.eq(Status.ACTIVE.getId()))
				.where(module.isActive.eq(Status.ACTIVE.getId())).where(submodule.isActive.eq(Status.ACTIVE.getId()))
				.where(page.isActive.eq(Status.ACTIVE.getId()));
		return query.fetchOne();*/
		JPQLQuery<User> query = from(QUser.user);
		query.where(QUser.user.userId.eq(userId));
		if (status != null) {
		query.where(QUser.user.isActive.eq(status));
		}
		//query.where(user.isActive.eq(true));
		return query.fetchOne();
		
	}
	/*
	 * @Override public User getUserDetails(Long userId) { JPQLQuery<User> query =
	 * from(user).join(user.TSecUserRoles,userRole).fetchJoin().join(userRole.
	 * TSecRole,role).fetchJoin().join(role.TSecRolePrivileges,rolePrivilege).
	 * fetchJoin(); query.where(user.userId.eq(userId)).orderBy(rolePrivilege);
	 * return query.fetchOne();
	 * 
	 * }
	 */

	@Override
	public List<User> findUsersbyUserType(Boolean status, Long userTypeId,Long userId) {

		// JPQLQuery<BatchwiseStudent>
		JPQLQuery<User> query = from(QUser.user);
		if (status != null) {
			query.where(QUser.user.isActive.eq(status));

		} /*else {
			query.where(user.isActive.eq(Boolean.TRUE));
		}*/
		if(userId != null) {
			query.where(QUser.user.userId.eq(userId));
		}
		if(userTypeId != null) {
			query.where(QUser.user.userType.userTypeId.eq(userTypeId));
		}
		
		return query.fetch();
	
	}
	
	
	@Override
	public List<User> searchUserDetails(Long schoolId, Long userTypeId, String firstName,String lastName,String userName,Boolean status) {
		JPQLQuery<User> jpqlQuery = from(QUser.user);

		BooleanBuilder builder = new BooleanBuilder();
		if (firstName != null) {
			builder.or(QUser.user.firstName.like("%" + firstName + "%"));
		}
		if (lastName != null) {
			builder.or(QUser.user.lastName.like("%" + lastName + "%"));
		}
		if(userName!=null) {
			builder.or(QUser.user.userName.like("%" + userName + "%"));
		}
		if(status != null) {
			builder.and(QUser.user.isActive.eq(status));
		}
		if (schoolId != null) {
			builder.and(QUser.user.school.schoolId.eq(schoolId));
		}
		
		jpqlQuery.where(builder).orderBy(QUser.user.userId.asc());
		jpqlQuery.limit(50);
		return jpqlQuery.fetch();
	}

}

/*
 * from(rolePrivilege).leftJoin(rolePrivilege.TSecPage, page)
 * .select(Projections.constructor(AuthorityRepoDTO.class,
 * rolePrivilege.rolePrivilegeId, page.pageName, rolePrivilege.canView,
 * rolePrivilege.canAdd, rolePrivilege.canEdit, rolePrivilege.canDelete))
 * .where(rolePrivilege.TMSchool.schoolId.eq(schoolId).and(rolePrivilege.
 * TMOrganization.organizationId
 * .eq(orgId).and(rolePrivilege.TSecRole.roleId.eq(roleId)))) .fetch();
 */



