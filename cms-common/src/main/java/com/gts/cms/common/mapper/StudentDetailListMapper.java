package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.StudentDetailListDTO;
import com.gts.cms.common.util.FileUtil;
import com.gts.cms.entity.AcademicYear;
import com.gts.cms.entity.GeneralDetail;
import com.gts.cms.entity.GroupSection;
import com.gts.cms.entity.StudentDetail;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StudentDetailListMapper implements BaseMapper<StudentDetail, StudentDetailListDTO> {

	@Value("${s3.url}")
	private String url;

	@Value("${s3.bucket-name}")
	private String bucketName;
	private static final String fileSeparator = FileUtil.getFileSeparator();

	

	@Override
	public StudentDetailListDTO convertEntityToDTO(StudentDetail entity) {
		StudentDetailListDTO studentDetailListDTO = null;
		if (entity != null) {
			studentDetailListDTO = new StudentDetailListDTO();
			studentDetailListDTO.setStudentId(entity.getStudentId());
			studentDetailListDTO.setApplicationNo(entity.getApplicationNo());
			studentDetailListDTO.setAdmissionNumber(entity.getAdmissionNumber());
			studentDetailListDTO.setDateOfBirth(entity.getDateOfBirth());
			studentDetailListDTO.setStdEmailId(entity.getStdEmailId());
			studentDetailListDTO.setFatherEmailId(entity.getFatherEmailId());
			studentDetailListDTO.setFatherAddress(entity.getFatherAddress());
			studentDetailListDTO.setFatherMobileNo(entity.getFatherMobileNo());
			studentDetailListDTO.setFatherName(entity.getFatherName());
			studentDetailListDTO.setFatherQualification(entity.getFatherQualification());
			studentDetailListDTO.setFirstName(entity.getFirstName());
			studentDetailListDTO.setGuardianAddress(entity.getGuardianAddress());
			studentDetailListDTO.setGuardianEmailId(entity.getGuardianEmailId());
			studentDetailListDTO.setGuardianName(entity.getGuardianName());
			studentDetailListDTO.setHallticketNumber(entity.getHallticketNumber());
			studentDetailListDTO.setIsActive(entity.getIsActive());
			studentDetailListDTO.setLastName(entity.getLastName());
			studentDetailListDTO.setMiddleName(entity.getMiddleName());
			studentDetailListDTO.setMobile(entity.getMobile());
			studentDetailListDTO.setMotherEmailId(entity.getMotherEmailId());
			studentDetailListDTO.setMotherMobileNo(entity.getMotherMobileNo());
			studentDetailListDTO.setMotherName(entity.getMotherName());
			studentDetailListDTO.setPermanentAddress(entity.getPermanentAddress());
			studentDetailListDTO.setPermanentPincode(entity.getPermanentPincode());
			studentDetailListDTO.setPermanentStreet(entity.getPermanentStreet());
			studentDetailListDTO.setPresentAddress(entity.getPresentAddress());
			studentDetailListDTO.setPresentMandal(entity.getPresentMandal());
			studentDetailListDTO.setPresentPincode(entity.getPresentPincode());
			studentDetailListDTO.setPrimaryContact(entity.getPrimaryContact());
			studentDetailListDTO.setRollNumber(entity.getRollNumber());
			studentDetailListDTO.setStudentPhotoPath(FileUtil.getAbsolutePath(entity.getStudentPhotoPath()));
			studentDetailListDTO.setIsScholarship(entity.getIsScholorship());
			studentDetailListDTO.setIsLateral(entity.getIsLateral());
			studentDetailListDTO.setIsMinority(entity.getIsMinority());
			if (entity.getGender() != null) {
				studentDetailListDTO.setGenderId(entity.getGender().getGeneralDetailId());
				studentDetailListDTO.setGenderDisplayName(entity.getGender().getGeneralDetailDisplayName());
			}
			
			if (entity.getQualifying() != null) {
				studentDetailListDTO.setQualifyingId(entity.getQualifying().getGeneralDetailId());
				studentDetailListDTO.setQualifyingName(entity.getQualifying().getGeneralDetailDisplayName());
				studentDetailListDTO.setQualifyingCode(entity.getQualifying().getGeneralDetailCode());
			}

			if (entity.getQuota() != null) {
				studentDetailListDTO.setQuotaId(entity.getQuota().getGeneralDetailId());
				studentDetailListDTO.setQuotaDisplayName(entity.getQuota().getGeneralDetailDisplayName());
			}

			if (entity.getGroupSection() != null) {
				studentDetailListDTO.setGroupSectionId(entity.getGroupSection().getGroupSectionId());
				studentDetailListDTO.setSection(entity.getGroupSection().getSection());
			}
			
			if (entity.getBatch() != null) {
				studentDetailListDTO.setBatchId(entity.getBatch().getBatchId());
			}

			if (entity.getAcademicYear() != null) {
				studentDetailListDTO.setAcademicYearId(entity.getAcademicYear().getAcademicYearId());
				studentDetailListDTO.setAcademicYear(entity.getAcademicYear().getAcademicYear());
			}

			if (entity.getSchool() != null) {
				studentDetailListDTO.setSchoolId(entity.getSchool().getSchoolId());
				studentDetailListDTO.setSchoolCode(entity.getSchool().getSchoolCode());
				studentDetailListDTO.setSchoolName(entity.getSchool().getSchoolName());
			}
			if (entity.getCourse() != null) {
				studentDetailListDTO.setCourseId(entity.getCourse().getCourseId());
				studentDetailListDTO.setCourseCode(entity.getCourse().getCourseCode());
			}

			if (entity.getCourseYear() != null) {
				studentDetailListDTO.setCourseYearId(entity.getCourseYear().getCourseYearId());
				studentDetailListDTO.setCourseYearCode(entity.getCourseYear().getCourseYearCode());
				studentDetailListDTO.setCourseYearName(entity.getCourseYear().getCourseYearName());
			}
			if (entity.getUser() != null) {
				studentDetailListDTO.setUserId(entity.getUser().getUserId());
			}
			/*
			 * if (entity.getStdAttendances() != null) {
			 * studentDetailListDTO.setStudentAttendances(
			 * studentAttendanceMapper.convertEntityListToDTOList(entity.getStdAttendances()
			 * )); }
			 */
			

			if (entity.getStudentStatus() != null) {
				studentDetailListDTO.setStudentStatusId(entity.getStudentStatus().getGeneralDetailId());
				studentDetailListDTO.setStudentStatusDisplayName(entity.getStudentStatus().getGeneralDetailDisplayName());
				studentDetailListDTO.setStudentStatusCode(entity.getStudentStatus().getGeneralDetailCode());
			}
			studentDetailListDTO.setReason(entity.getReason());
			studentDetailListDTO.setCreatedDt(entity.getCreatedDt());
			studentDetailListDTO.setCreatedUser(entity.getCreatedUser());
			
			if(entity.getOrganization()!=null) {
				
				studentDetailListDTO.setOrganizationId(entity.getOrganization().getOrganizationId());
			}


		}
		return studentDetailListDTO;
	}

	@Override
	public List<StudentDetailListDTO> convertEntityListToDTOList(List<StudentDetail> entityList) {
		List<StudentDetailListDTO> studentDetailDTOList = new ArrayList<>();
		entityList.forEach(studentDetail -> studentDetailDTOList.add(convertEntityToDTO(studentDetail)));
		return studentDetailDTOList;
	}
	
	//@Override
	/*public List<StudentFeeListDTO> convertEntityListToFeeDTOList(List<FeeStudentData> entityList) {
		List<StudentFeeListDTO> studentFeeListDTOs = new ArrayList<>();
		entityList.forEach(studentDetail -> studentFeeListDTOs.add(convertEntityToFeeDTO(studentDetail)));
		return studentFeeListDTOs;
	}*/
	
	//@Override
	/*public StudentFeeListDTO convertEntityToFeeDTO(FeeStudentData entity) {
		StudentFeeListDTO studentFeeList = null;
		if (entity != null) {
			studentFeeList = new StudentFeeListDTO();
			if (entity.getStudentDetail()!=null) {
				studentFeeList.setStudentId(entity.getStudentDetail().getStudentId());
				studentFeeList.setApplicationNo(entity.getStudentDetail().getApplicationNo());
				studentFeeList.setAdmissionNumber(entity.getStudentDetail().getAdmissionNumber());
				studentFeeList.setDateOfBirth(entity.getStudentDetail().getDateOfBirth());
				studentFeeList.setFirstName(entity.getStudentDetail().getFirstName());
				studentFeeList.setHallticketNumber(entity.getStudentDetail().getHallticketNumber());
				studentFeeList.setIsActive(entity.getStudentDetail().getIsActive());
				studentFeeList.setLastName(entity.getStudentDetail().getLastName());
				studentFeeList.setMiddleName(entity.getStudentDetail().getMiddleName());
				studentFeeList.setMobile(entity.getStudentDetail().getMobile());
				studentFeeList.setRollNo(entity.getStudentDetail().getRollNumber());
				studentFeeList.setIsLateral(entity.getStudentDetail().getIsLateral());
				if (entity.getStudentDetail().getGroupSection() != null) {
					studentFeeList.setGroupSectionId(entity.getStudentDetail().getGroupSection().getGroupSectionId());
					studentFeeList.setSection(entity.getStudentDetail().getGroupSection().getSection());
				}
			
				*//*if (entity.getStudentDetail().getCourseGroup() != null) {
					studentFeeList.setCourseGroupId(entity.getStudentDetail().getCourseGroup().getCourseGroupId());
					studentFeeList.setGroupName(entity.getStudentDetail().getCourseGroup().getGroupName());
				}
				if (entity.getStudentDetail().getCourseYear() != null) {
					studentFeeList.setCourseYearId(entity.getStudentDetail().getCourseYear().getCourseYearId());
					studentFeeList.setCourseYearName(entity.getStudentDetail().getCourseYear().getCourseYearName());
				}*//*
				
				*//*if (entity.getStudentDetail().getCourse() != null) {
					studentFeeList.setCourseId(entity.getStudentDetail().getCourse().getCourseId());
					studentFeeList.setCourseName(entity.getStudentDetail().getCourse().getCourseName());
				}*//*
				
				if(entity.getCourseGroup()!=null) {
					studentFeeList.setCourseGroupId(entity.getCourseGroup().getCourseGroupId());
					studentFeeList.setGroupName(entity.getCourseGroup().getGroupName());
					studentFeeList.setGroupCode(entity.getCourseGroup().getGroupCode());
					if(entity.getCourseGroup().getCourse()!=null) {
					studentFeeList.setCourseId(entity.getCourseGroup().getCourse().getCourseId());
					studentFeeList.setCourseName(entity.getCourseGroup().getCourse().getCourseName());
					studentFeeList.setCourseCode(entity.getCourseGroup().getCourse().getCourseCode());
					}
				}
				
				if(entity.getCourseYear()!=null) {
					studentFeeList.setCourseYearId(entity.getCourseYear().getCourseYearId());
					studentFeeList.setCourseYearName(entity.getCourseYear().getCourseYearName());
					studentFeeList.setCourseYearNo(entity.getCourseYear().getYearNo());
				}
				
				*//*if (entity.getStudentDetail().getRegulation() != null) {
					studentFeeList.setRegulationId(entity.getStudentDetail().getRegulation().getRegulationId());
					studentFeeList.setRegulationName(entity.getStudentDetail().getRegulation().getRegulationName());
				}
				if (entity.getStudentDetail().getBatch() != null) {
					studentFeeList.setBatchId(entity.getStudentDetail().getBatch().getBatchId());
				}*//*

				if (entity.getAcademicYear() != null) {
					studentFeeList.setAcademicYearId(entity.getAcademicYear().getAcademicYearId());
					studentFeeList.setAcademicYear(entity.getAcademicYear().getAcademicYear());
				}
			}
			if(entity.getFeeStructure()!=null) {
				studentFeeList.setFeeStructureId(entity.getFeeStructure().getFeeStructureId());
				studentFeeList.setStructureName(entity.getFeeStructure().getClassGroupName());
			}
			studentFeeList.setBalanceAmount(entity.getBalanceAmount());
			studentFeeList.setGrossAmount(entity.getGrossAmount());
			studentFeeList.setNetAmount(entity.getNetAmount());
			studentFeeList.setPaidAmount(entity.getPaidAmount());
			studentFeeList.setDiscountAmount(entity.getDiscountAmount());
			studentFeeList.setRefundAmount(entity.getRefundAmount());
			studentFeeList.setFineAmount(entity.getFineAmount());
			
			
			if(entity.getFeeStructure()!=null) {
				if(entity.getFeeStructure().getQuota()!=null) {
					studentFeeList.setQuotaId(entity.getFeeStructure().getQuota().getGeneralDetailId());
					studentFeeList.setQuotaName(entity.getFeeStructure().getQuota().getGeneralDetailDisplayName());
					studentFeeList.setQuotaCode(entity.getFeeStructure().getQuota().getGeneralDetailCode());
				}
			}
			
			studentFeeList.setFeeStudentDataDTO(feeStudentDataMapper.convertEntityToDTO(entity));
			
			
		}
		return studentFeeList;
	}*/

	@Override
	public List<StudentDetail> convertDTOListToEntityList(List<StudentDetailListDTO> dtoList) {
		List<StudentDetail> studentDetailList = new ArrayList<>();
		dtoList.forEach(studentDetailDto -> studentDetailList.add(convertDTOtoEntity(studentDetailDto, null)));
		return studentDetailList;
	}

	@Override
	public StudentDetail convertDTOtoEntity(StudentDetailListDTO dto, StudentDetail entity) {
		if (dto != null) {
			entity = new StudentDetail();
			entity.setStudentId(dto.getStudentId());
			entity.setApplicationNo(dto.getApplicationNo());
			entity.setAdmissionNumber(dto.getAdmissionNumber());
			entity.setDateOfBirth(dto.getDateOfBirth());
			entity.setStdEmailId(dto.getStdEmailId());
			entity.setFatherEmailId(dto.getFatherEmailId());
			entity.setFatherAddress(dto.getFatherAddress());
			entity.setFatherMobileNo(dto.getFatherMobileNo());
			entity.setFatherName(dto.getFatherName());
			entity.setFatherQualification(dto.getFatherQualification());
			entity.setFirstName(dto.getFirstName());
			entity.setGuardianAddress(dto.getGuardianAddress());
			entity.setGuardianEmailId(dto.getGuardianEmailId());
			entity.setGuardianName(dto.getGuardianName());
			entity.setHallticketNumber(dto.getHallticketNumber());
			entity.setIsActive(dto.getIsActive());
			entity.setLastName(dto.getLastName());
			entity.setMiddleName(dto.getMiddleName());
			entity.setMobile(dto.getMobile());
			entity.setMotherEmailId(dto.getMotherEmailId());
			entity.setMotherMobileNo(dto.getMotherMobileNo());
			entity.setMotherName(dto.getMotherName());
			entity.setPermanentAddress(dto.getPermanentAddress());
			entity.setPermanentPincode(dto.getPermanentPincode());
			entity.setPermanentStreet(dto.getPermanentStreet());
			entity.setPresentAddress(dto.getPresentAddress());
			entity.setPresentMandal(dto.getPresentMandal());
			entity.setPresentPincode(dto.getPresentPincode());
			entity.setPrimaryContact(dto.getPrimaryContact());
			entity.setRollNumber(dto.getRollNumber());
			entity.setIsScholorship(dto.getIsScholarship());
			entity.setIsLateral(dto.getIsLateral());
			GroupSection groupSection = new GroupSection();
			groupSection.setGroupSectionId(dto.getGroupSectionId());
			entity.setGroupSection(groupSection);
			

			AcademicYear academicYear = new AcademicYear();
			academicYear.setAcademicYearId(dto.getAcademicYearId());
			entity.setAcademicYear(academicYear);

			if (dto.getGenderId() != null) {
				GeneralDetail gender = new GeneralDetail();
				gender.setGeneralDetailId(dto.getGenderId());
				entity.setGender(gender);
			}

			if (dto.getQuotaId() != null) {
				GeneralDetail quota = new GeneralDetail();
				quota.setGeneralDetailId(dto.getQuotaId());
				entity.setQuota(quota);
			}
			if (dto.getQualifyingId() != null) {
				GeneralDetail generalDetail = new GeneralDetail();
				generalDetail.setGeneralDetailId(dto.getQualifyingId());
				entity.setQualifying(generalDetail);
			}
		}
		return entity;
	}
}
