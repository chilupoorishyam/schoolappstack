package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

public class ConfigAutonumberDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long autoconfigId;

	@NotNull(message = "Configure Attribute Name is required.")
	private String configAttributeName;

	private String configAtttributeCode;

	private Date createdDt;

	private Long createdUser;

	private String currentNumber;

	private String formula;

	@NotNull(message = "isActive is required.")
	private Boolean isActive;

	private Boolean isAutoIncRequired;

	private Boolean isFormula;

	private String prefix;

	private String reason;

	private String resetBy;

	private String suffix;

	private Date updatedDt;

	private Long updatedUser;

	@NotNull(message = "OrganizationId is required.")
	private Long organizationId;
	
	private String orgName;
	
	@NotNull(message = "SchoolId is required.")
	private Long schoolId;
	
	private String schoolName;

	private Long courseId;
	
	private String courseName;

	private String groupName;
	
	private String orgCode;

	private String schoolCode;

	private String courseCode;

	private String groupCode;


	public Long getAutoconfigId() {
		return autoconfigId;
	}

	public void setAutoconfigId(Long autoconfigId) {
		this.autoconfigId = autoconfigId;
	}

	public String getConfigAttributeName() {
		return configAttributeName;
	}

	public void setConfigAttributeName(String configAttributeName) {
		this.configAttributeName = configAttributeName;
	}

	public String getConfigAtttributeCode() {
		return configAtttributeCode;
	}

	public void setConfigAtttributeCode(String configAtttributeCode) {
		this.configAtttributeCode = configAtttributeCode;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getCurrentNumber() {
		return currentNumber;
	}

	public void setCurrentNumber(String currentNumber) {
		this.currentNumber = currentNumber;
	}

	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsAutoIncRequired() {
		return isAutoIncRequired;
	}

	public void setIsAutoIncRequired(Boolean isAutoIncRequired) {
		this.isAutoIncRequired = isAutoIncRequired;
	}

	public Boolean getIsFormula() {
		return isFormula;
	}

	public void setIsFormula(Boolean isFormula) {
		this.isFormula = isFormula;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getResetBy() {
		return resetBy;
	}

	public void setResetBy(String resetBy) {
		this.resetBy = resetBy;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}


	
	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public String getOrgCode() {
	       return this.orgCode;
	}
	public void setOrgCode(String orgCode) {
	        this.orgCode = orgCode;
	}
	public String getSchoolCode() {
	       return this.schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
	       this.schoolCode = schoolCode;
	}

	public String getCourseCode() {
	          return this.courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public String getGroupCode() {
		return this.groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}


}