package com.gts.cms.common.student.repository;

import com.gts.cms.common.student.repository.custom.StudentDocumentCollectionRepositoryCustom;
import com.gts.cms.entity.StudentDocumentCollection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Genesis
 *
 */
@Repository
public interface StudentDocumentCollectionRepository
		extends JpaRepository<StudentDocumentCollection, Long>, StudentDocumentCollectionRepositoryCustom {

}
