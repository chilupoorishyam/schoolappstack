package com.gts.cms.common.student.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.util.Date;
@JsonInclude(Include.NON_NULL)
public class StudentDocumentCollectionDTO {

	private Long stdDocCollId;
	@JsonInclude(Include.NON_NULL)
	private Date createdDt;
	@JsonInclude(Include.NON_NULL)
	private Long createdUser;
	@JsonInclude(Include.NON_NULL)
	private String fileName;
	@JsonInclude(Include.NON_NULL)
	private String filePath;
	@JsonInclude(Include.NON_NULL)
	private Boolean isActive;
	@JsonInclude(Include.NON_NULL)
	private Boolean isHardCopy;
	@JsonInclude(Include.NON_NULL)
	private Boolean isOriginal;
	@JsonInclude(Include.NON_NULL)
	private Boolean isSoftCopy;
	@JsonInclude(Include.NON_NULL)
	private Boolean isVerified;
	@JsonInclude(Include.NON_NULL)
	private String rackNumber;
	@JsonInclude(Include.NON_NULL)
	private String reason;
	@JsonInclude(Include.NON_NULL)
	private Long appDocCollId;
	@JsonInclude(Include.NON_NULL)
	private Long studentId;
	private Long docRepId;
	@JsonInclude(Include.NON_NULL)
	private Long verifiedByEmpId;
	
	private Long documentRepositoryId;
	

	public Long getDocumentRepositoryId() {
		return documentRepositoryId;
	}

	public void setDocumentRepositoryId(Long documentRepositoryId) {
		this.documentRepositoryId = documentRepositoryId;
	}

	public Long getStdDocCollId() {
		return stdDocCollId;
	}

	public void setStdDocCollId(Long stdDocCollId) {
		this.stdDocCollId = stdDocCollId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsHardCopy() {
		return isHardCopy;
	}

	public void setIsHardCopy(Boolean isHardCopy) {
		this.isHardCopy = isHardCopy;
	}

	public Boolean getIsOriginal() {
		return isOriginal;
	}

	public void setIsOriginal(Boolean isOriginal) {
		this.isOriginal = isOriginal;
	}

	public Boolean getIsSoftCopy() {
		return isSoftCopy;
	}

	public void setIsSoftCopy(Boolean isSoftCopy) {
		this.isSoftCopy = isSoftCopy;
	}

	public Boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	public String getRackNumber() {
		return rackNumber;
	}

	public void setRackNumber(String rackNumber) {
		this.rackNumber = rackNumber;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Long getAppDocCollId() {
		return appDocCollId;
	}

	public void setAppDocCollId(Long appDocCollId) {
		this.appDocCollId = appDocCollId;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getDocRepId() {
		return docRepId;
	}

	public void setDocRepId(Long docRepId) {
		this.docRepId = docRepId;
	}

	public Long getVerifiedByEmpId() {
		return verifiedByEmpId;
	}

	public void setVerifiedByEmpId(Long verifiedByEmpId) {
		this.verifiedByEmpId = verifiedByEmpId;
	}

}
