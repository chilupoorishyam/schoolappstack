package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.BatchRepositoryCustom;
import com.gts.cms.entity.Batch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * created by Naveen(Auto) on 02/09/2018
 * 
 */
@Repository
public interface BatchRepository extends JpaRepository<Batch, Long>, BatchRepositoryCustom {
	
	
	@Query("SELECT b FROM Batch b"
			+ " WHERE 1=1"
			+ " AND(:courseId is null or  b.course.courseId = :courseId)"
			+ " AND(b.isActive = true) "
			+ " ORDER BY b.batchName DESC "
			)
	List<Batch> getBatch(@Param("courseId") Long courseId);



}
