package com.gts.cms.common.student.repository;

import com.gts.cms.common.student.repository.custom.StudentAcademicBatchesRepositoryCustom;
import com.gts.cms.entity.StudentAcademicbatch;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface StudentAcademicBatchesRepository
		extends JpaRepository<StudentAcademicbatch, Long>, StudentAcademicBatchesRepositoryCustom {

	@Query("SELECT s FROM StudentAcademicbatch s"
			+ " WHERE s.studentDetail.studentId = :studentId AND s.isActive = true"
			+ " ORDER BY s.studentAcademicbatchId DESC")
	List<StudentAcademicbatch> getStudentAcademicBatchDetails(@Param("studentId") Long studentId, Pageable pageable);

	@Modifying
	@Query("UPDATE StudentAcademicbatch sb " + " SET sb.toCourseYear.courseYearId = :courseYearId"
			+ "  ,sb.toGroupSection.groupSectionId = :groupSectionId,sb.toDate = :toDate"
			+ " ,sb.updatedDt=:currentDate"
			+ " ,sb.updatedUser=:updatedUser"
			+ " WHERE sb.studentDetail.studentId IN ( :studentsId ) AND sb.toDate IS NULL"
			+ " AND sb.isActive = true")
	void updatePreviouSection(@Param("studentsId") List<Long> studentsId, 
							  @Param("courseYearId") Long courseYearId,
							  @Param("groupSectionId") Long groupSectionId, @Param("toDate") Date toDate,
							  @Param("currentDate") Date currentDate,
							  @Param("updatedUser") Long updatedUser);

	
	
	@Query("SELECT DISTINCT s FROM StudentAcademicbatch s"
			+ " WHERE s.studentDetail.studentId = :studentId "
			+ " AND (s.fromDate) in (SELECT MAX(s.fromDate) FROM s WHERE s.studentDetail.studentId = :studentId"
			+ " AND isActive=true)"
			+ " AND s.isActive = true "
			)
	List<StudentAcademicbatch> findByStudentId(@Param("studentId") Long studentId);

	@Query("SELECT s FROM StudentAcademicbatch s"
			+ " WHERE s.studentAcademicbatchId = :studentAcademicbatchId ")
	StudentAcademicbatch findByAcademicBatchId(@Param("studentAcademicbatchId")Long studentAcademicbatchId);
	/*SELECT * FROM `t_std_student_academicbatches` 
	WHERE 1=1
	AND ( '2020-04-28'>= CAST(from_date AS DATE)
	AND '2020-04-28'<= CAST(to_date AS DATE)) 
	AND `fk_student_id`=681075
	AND is_active=1*/

	/*@Query("SELECT sab FROM StudentAcademicbatch sab"
			+ "WHERE 1=1"
			+ "AND ((:date)>= DATE(sab.fromDate)"
			+ "AND (:date)<=DATE(sab.toDate))"
			+ "AND sab.studentDetail.studentId=:studentId"
			+ "AND sab.isActive=1"
			)*/
	@Query(value=" SELECT" + 
			"        sab.*" + 
			"    FROM" + 
			"       t_std_student_academicbatches sab " + 
			"    WHERE" + 
			"        1=1  " + 
			"        AND " + 
			"            (:date>=CAST(sab.from_date AS DATE) " + 
			"                AND(:date)<=CAST(sab.to_date AS DATE) " + 
			"            )" + 
			"                AND sab.fk_student_id= :studentId" + 
			"                AND sab.is_Active=1", nativeQuery = true)
	List<StudentAcademicbatch> findAlreadyExistWithDate(@Param("studentId") Long studentId,@Param("date") Date date);

}
//"SELECT p FROM Product p WHERE p.price < (SELECT MAX(p.price) FROM p)"