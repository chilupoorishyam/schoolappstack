package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

public class AuthorizationDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long authorizationId;
	private String authorizationKey;
	private Date createdDt;
	private String ipAddress;
	private Boolean isActive;
	private Date loginTime;
	private Date logoutTime;
	private Date updatedDt;
	private String userAgent;
	private Long userId;
	private String firstName;
	private String lastName;
	private String resetPasswordCode;
	private String userName;

	public void setAuthorizationId(Long authorizationId) {
		this.authorizationId = authorizationId;
	}

	public Long getAuthorizationId() {
		return authorizationId;
	}

	public void setAuthorizationKey(String authorizationKey) {
		this.authorizationKey = authorizationKey;
	}

	public String getAuthorizationKey() {
		return authorizationKey;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLogoutTime(Date logoutTime) {
		this.logoutTime = logoutTime;
	}

	public Date getLogoutTime() {
		return logoutTime;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setResetPasswordCode(String resetPasswordCode) {
		this.resetPasswordCode = resetPasswordCode;
	}

	public String getResetPasswordCode() {
		return resetPasswordCode;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserName() {
		return userName;
	}
}
