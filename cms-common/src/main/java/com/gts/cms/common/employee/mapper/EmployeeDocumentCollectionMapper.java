package com.gts.cms.common.employee.mapper;

import com.gts.cms.common.employee.dto.EmployeeDocumentCollectionDTO;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.util.FileUtil;
import com.gts.cms.common.util.OptionalUtil;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.DocumentRepository;
import com.gts.cms.entity.EmployeeDetail;
import com.gts.cms.entity.EmployeeDocumentCollection;
import com.gts.cms.entity.GeneralDetail;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Genesis
 *
 */
@Component
public class EmployeeDocumentCollectionMapper
		implements BaseMapper<EmployeeDocumentCollection, EmployeeDocumentCollectionDTO> {
	
	@Value("${s3.url}")
	private String url;

	@Value("${s3.bucket-name}")
	private String bucketName;
	private static final String fileSeparator = FileUtil.getFileSeparator();

	public EmployeeDocumentCollectionDTO convertEntityToDTO(EmployeeDocumentCollection empDocumentCollection) {
		EmployeeDocumentCollectionDTO empDocumentCollectionDTO = null;
		if (empDocumentCollection != null) {
			empDocumentCollectionDTO = new EmployeeDocumentCollectionDTO();

			empDocumentCollectionDTO.setEmployeeDocCollId(empDocumentCollection.getEmployeeDocCollId());
			empDocumentCollectionDTO.setCreatedDt(empDocumentCollection.getCreatedDt());
			empDocumentCollectionDTO.setCreatedUser(empDocumentCollection.getCreatedUser());
			empDocumentCollectionDTO.setFileName(empDocumentCollection.getFileName());
			
			if (empDocumentCollection.getFilePath() != null) {
				StringBuilder filePath = new StringBuilder();
				filePath = filePath.append(url).append(fileSeparator).append(bucketName)
						.append(fileSeparator).append(empDocumentCollection.getFilePath());
				empDocumentCollectionDTO.setFilePath(filePath.toString());
			}
			
			empDocumentCollectionDTO.setIsActive(empDocumentCollection.getIsActive());
			empDocumentCollectionDTO.setIsHardCopy(empDocumentCollection.getIsHardCopy());
			empDocumentCollectionDTO.setIsOriginal(empDocumentCollection.getIsOriginal());
			empDocumentCollectionDTO.setIsSoftCopy(empDocumentCollection.getIsSoftCopy());
			empDocumentCollectionDTO.setIsVerified(empDocumentCollection.getIsVerified());
			empDocumentCollectionDTO.setRackNumber(empDocumentCollection.getRackNumber());
			empDocumentCollectionDTO.setReason(empDocumentCollection.getReason());

			empDocumentCollectionDTO.setEmpId(
					OptionalUtil.resolve(() -> empDocumentCollection.getEmployeeDetail().getEmployeeId()).orElse(null));
			empDocumentCollectionDTO.setDocumentRepositoryId(
					OptionalUtil.resolve(() -> empDocumentCollection.getDocumentRepository().getDocumentRepositoryId())
							.orElse(null));
			empDocumentCollectionDTO.setDoctypeCatdetId(
					OptionalUtil.resolve(() -> empDocumentCollection.getDoctype().getGeneralDetailId()).orElse(null));
			empDocumentCollectionDTO.setVerifiedbyEmpId(OptionalUtil
					.resolve(() -> empDocumentCollection.getVerifiedByEmployeeDetail().getEmployeeId()).orElse(null));
		}
		return empDocumentCollectionDTO;
	}

	public EmployeeDocumentCollection convertDTOtoEntity(EmployeeDocumentCollectionDTO empDocumentCollectionDTO,
			EmployeeDocumentCollection empDocumentCollection) {
		if (empDocumentCollectionDTO != null) {
			if (empDocumentCollection == null) {
				empDocumentCollection = new EmployeeDocumentCollection();
				empDocumentCollection.setIsActive(Boolean.TRUE);
				empDocumentCollection.setCreatedDt(new Date());
				empDocumentCollection.setCreatedUser(SecurityUtil.getCurrentUser());
			}else {
				empDocumentCollection.setIsActive(empDocumentCollectionDTO.getIsActive());
			}
			

			empDocumentCollection.setEmployeeDocCollId(empDocumentCollectionDTO.getEmployeeDocCollId());
			
			empDocumentCollection.setUpdatedDt(new Date());
			empDocumentCollection.setUpdatedUser(SecurityUtil.getCurrentUser());
			empDocumentCollection.setFileName(empDocumentCollectionDTO.getFileName());
			empDocumentCollection.setFilePath(FileUtil.getRelativePath(empDocumentCollectionDTO.getFilePath()));
			empDocumentCollection.setIsHardCopy(empDocumentCollectionDTO.getIsHardCopy());
			empDocumentCollection.setIsOriginal(empDocumentCollectionDTO.getIsOriginal());
			empDocumentCollection.setIsSoftCopy(empDocumentCollectionDTO.getIsSoftCopy());
			empDocumentCollection.setIsVerified(empDocumentCollectionDTO.getIsVerified());
			empDocumentCollection.setRackNumber(empDocumentCollectionDTO.getRackNumber());
			empDocumentCollection.setReason(empDocumentCollectionDTO.getReason());

			EmployeeDetail empDetail1 = new EmployeeDetail();
			empDetail1.setEmployeeId(empDocumentCollectionDTO.getEmpId());
			empDocumentCollection.setEmployeeDetail(empDetail1);

			DocumentRepository documentRepository = new DocumentRepository();
			documentRepository.setDocumentRepositoryId(empDocumentCollectionDTO.getDocumentRepositoryId());
			empDocumentCollection.setDocumentRepository(documentRepository);

			GeneralDetail doctype = new GeneralDetail();
			doctype.setGeneralDetailId(empDocumentCollectionDTO.getDoctypeCatdetId());
			empDocumentCollection.setDoctype(doctype);

			EmployeeDetail verifiedByEmployeeDetail = new EmployeeDetail();
			verifiedByEmployeeDetail.setEmployeeId(empDocumentCollectionDTO.getVerifiedbyEmpId());
			empDocumentCollection.setVerifiedByEmployeeDetail(verifiedByEmployeeDetail);

		}
		return empDocumentCollection;
	}

	public List<EmployeeDocumentCollection> convertDTOListToEntityList(
			List<EmployeeDocumentCollectionDTO> empDocumentCollectionList) {
		List<EmployeeDocumentCollection> empDetailList = new ArrayList<>();
		if (empDocumentCollectionList != null) {
			empDocumentCollectionList.forEach(empDocumentCollection -> empDetailList
					.add(convertDTOtoEntity(empDocumentCollection, null)));
		}
		return empDetailList;

	}

	public List<EmployeeDocumentCollectionDTO> convertEntityListToDTOList(
			List<EmployeeDocumentCollection> empDetailList) {
		List<EmployeeDocumentCollectionDTO> empDocumentCollectionList = new ArrayList<>();
		empDetailList.forEach(
				empDocumentCollection -> empDocumentCollectionList.add(convertEntityToDTO(empDocumentCollection)));
		return empDocumentCollectionList;
	}

}
