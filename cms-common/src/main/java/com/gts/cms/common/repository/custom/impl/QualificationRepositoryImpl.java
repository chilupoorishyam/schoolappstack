package com.gts.cms.common.repository.custom.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.gts.cms.entity.QQualification;
import com.gts.cms.entity.Qualification;
import com.gts.cms.common.repository.custom.QualificationRepositoryCustom;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

@Repository
public class QualificationRepositoryImpl extends QuerydslRepositorySupport implements QualificationRepositoryCustom {
	public QualificationRepositoryImpl() {
		super(Qualification.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public Long deleteQualification(Long qualificationId) {
		return update(QQualification.qualification).where(QQualification.qualification.qualificationId.eq(qualificationId))
				.set(QQualification.qualification.isActive, false).execute();
	}

}
