package com.gts.cms.common.repository;

import com.gts.cms.entity.CmCourseOutcome;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CmCourseOutcomeRepository extends JpaRepository<CmCourseOutcome, Long> {
}