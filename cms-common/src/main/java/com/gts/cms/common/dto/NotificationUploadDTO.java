package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

public class NotificationUploadDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long notificationId;

	private String notificationDocPath;

	private String notificationDocPathStatus;

	private Map<String, MultipartFile> notificationFile;
	
	private Boolean messageStatus;

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public String getNotificationDocPath() {
		return notificationDocPath;
	}

	public void setNotificationDocPath(String notificationDocPath) {
		this.notificationDocPath = notificationDocPath;
	}

	public String getNotificationDocPathStatus() {
		return notificationDocPathStatus;
	}

	public void setNotificationDocPathStatus(String notificationDocPathStatus) {
		this.notificationDocPathStatus = notificationDocPathStatus;
	}

	public Map<String, MultipartFile> getNotificationFile() {
		return notificationFile;
	}

	public void setNotificationFile(Map<String, MultipartFile> notificationFile) {
		this.notificationFile = notificationFile;
	}

	public Boolean getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(Boolean messageStatus) {
		this.messageStatus = messageStatus;
	}
}
