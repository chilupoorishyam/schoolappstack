package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;


public class EventAudienceDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	
	private Long eventAudienceId;
	
	private Long schoolId;
	
	private Long courseId;
	
	private Long eventId;
	
	private Long audienceTypeId;
	
	private String categoryName;
	
	private String categoryValue;
	
	private String reason;

	@NotNull(message="isActive is required")
	private Boolean isActive;

	
	private Date createdDt;

	private Long createdUser;

	private Date updatedDt;

	private Long updatedUser;

	private String schoolName;
	
	private String schoolCode;
	
	private String courseName;
	
	private String courseCode;
	
	private String courseShortname;
	
	private String eventName;
	
	private String audienceTypeCode;

	private String audienceTypeDescription;

	private String audienceTypeDisplayName;

	public Long getEventAudienceId() {
		return eventAudienceId;
	}

	public void setEventAudienceId(Long eventAudienceId) {
		this.eventAudienceId = eventAudienceId;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public Long getAudienceTypeId() {
		return audienceTypeId;
	}

	public void setAudienceTypeId(Long audienceTypeId) {
		this.audienceTypeId = audienceTypeId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryValue() {
		return categoryValue;
	}

	public void setCategoryValue(String categoryValue) {
		this.categoryValue = categoryValue;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public String getCourseShortname() {
		return courseShortname;
	}

	public void setCourseShortname(String courseShortname) {
		this.courseShortname = courseShortname;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getAudienceTypeCode() {
		return audienceTypeCode;
	}

	public void setAudienceTypeCode(String audienceTypeCode) {
		this.audienceTypeCode = audienceTypeCode;
	}

	public String getAudienceTypeDescription() {
		return audienceTypeDescription;
	}

	public void setAudienceTypeDescription(String audienceTypeDescription) {
		this.audienceTypeDescription = audienceTypeDescription;
	}

	public String getAudienceTypeDisplayName() {
		return audienceTypeDisplayName;
	}

	public void setAudienceTypeDisplayName(String audienceTypeDisplayName) {
		this.audienceTypeDisplayName = audienceTypeDisplayName;
	}
}