package com.gts.cms.common.enums;

/**
 * The Enum Status.
 */
public enum Status {

	/** The inactive. */
	INACTIVE((Boolean)false, "INACTIVE"), /** The active. */
    ACTIVE((Boolean)true, "ACTIVE"),
	SUCESS((Boolean)true,"SUCCESS"),
	FAILURE((Boolean)false,"FAILURE"),
	ALL(null,"ALL");
	/** The flag. */
	Boolean id;
	
	/** The name. */
	String name;

	/**
	 * Instantiates a new status.
	 *
	 * @param flag the flag
	 * @param name the name
	 */
private Status(Boolean id,String name) {
	this.id=id;
	this.name=name;
}

	public Boolean getId() {
		return id;
	}

	public void setId(Boolean id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

}
