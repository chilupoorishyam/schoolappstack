package com.gts.cms.common.service;

import java.util.List;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.RolePrivilegeDTO;

public interface RolePrivilegeService {

	ApiResponse<Long> addRolePrivilegeList(List<RolePrivilegeDTO> rolePrivilegeDTOs);

}
