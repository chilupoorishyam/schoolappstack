package com.gts.cms.common.employee.mapper;

import com.gts.cms.common.dto.EmpAttendanceEmployeeDTO;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.entity.EmpAttendanceEmployee;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EmpAttendanceEmployeeMapper implements BaseMapper<EmpAttendanceEmployee, EmpAttendanceEmployeeDTO> {

	@Override
	public EmpAttendanceEmployeeDTO convertEntityToDTO(EmpAttendanceEmployee empAttendanceEmployee) {
		return null;
	}

	@Override
	public List<EmpAttendanceEmployeeDTO> convertEntityListToDTOList(List<EmpAttendanceEmployee> entityList) {
		return null;
	}

	@Override
	public List<EmpAttendanceEmployee> convertDTOListToEntityList(List<EmpAttendanceEmployeeDTO> dtoList) {
		return null;
	}

	@Override
	public EmpAttendanceEmployee convertDTOtoEntity(EmpAttendanceEmployeeDTO empAttendanceEmployeeDTO, EmpAttendanceEmployee empAttendanceEmployee) {
		return null;
	}
}
