package com.gts.cms.common.repository.custom.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;

import com.gts.cms.common.dto.PageResponse;
import com.gts.cms.common.repository.DomainRepository;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.SimpleEntityPathResolver;
import org.springframework.stereotype.Repository;

import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.exception.BadRequestException;
import com.gts.cms.common.exception.MalformedQueryException;
import com.gts.cms.common.querydsl.support.QDslFilterBuilder;
import com.querydsl.core.NonUniqueResultException;
import com.querydsl.core.types.EntityPath;
import com.querydsl.jpa.JPQLQuery;

@Repository
public class DomainRepositoryImpl implements DomainRepository {
	private static final Logger LOGGER = Logger.getLogger(DomainRepositoryImpl.class);
	@Autowired
	private EntityManager em;

	@Override
	public Object findOneEntity(Class<?> entityClass, String queryString) {
		try {
			JPQLQuery query = QDslFilterBuilder.buildQuery(entityClass, em, queryString, null);
			return query.fetchOne();
		} catch (NonUniqueResultException e) {
			LOGGER.error("Exception occured while finding Entity: NonUniqueResultException", e);
		} catch (MalformedQueryException e) {
			LOGGER.error("Exception occured while finding Entity: MalformedQueryException", e);
			throw new ApiException(e.getLocalizedMessage());
		}
		return null;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public PageResponse findEntityList(Class<?> entityClass, String queryString, int pageNumber, int pageSize) {
		final Pageable pageable = PageRequest.of(pageNumber, pageSize);
		final EntityPath<?> entityPath = SimpleEntityPathResolver.INSTANCE.createPath(entityClass);
		final JPQLQuery query;
		final JPQLQuery pageQuery;
		final long total;
		final PageResponse pageResponse = new PageResponse();
		try {
			query = QDslFilterBuilder.buildQuery(entityClass, em, queryString, null);
			total = query.fetchCount();
			pageQuery = QDslFilterBuilder.applyPagination(query, em, entityPath, pageable);
		} catch (MalformedQueryException e) {
			throw new BadRequestException("");
		}
		final List<Object> result = (List<Object>) (total > pageable.getOffset() ? pageQuery.fetch()
				: Collections.emptyList());
		pageResponse.setCount(Long.valueOf(result.size()));
		pageResponse.setPage(Long.valueOf(pageable.getPageNumber()));
		pageResponse.setTotalCount(total);
		pageResponse.setTotalPages((total % pageable.getPageSize()==0)?(total / pageable.getPageSize()): (total / pageable.getPageSize())+ 1);
		pageResponse.setPageSize(Long.valueOf(pageable.getPageSize()));
		pageResponse.setResultList(result);
		return pageResponse;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public PageResponse searchEntityList(Class<?> entityClass, String queryString,String fields, int pageNumber, int pageSize) {
		final Pageable pageable = PageRequest.of(pageNumber, pageSize);
		final EntityPath<?> entityPath = SimpleEntityPathResolver.INSTANCE.createPath(entityClass);
		final JPQLQuery query;
		final JPQLQuery pageQuery;
		final long total;
		final PageResponse pageResponse = new PageResponse();
		try {
			query = QDslFilterBuilder.buildQuery(entityClass, em, queryString, null);
			total = query.fetchCount();
			pageQuery = QDslFilterBuilder.applyPagination(query, em, entityPath, pageable);
		} catch (MalformedQueryException e) {
			throw new BadRequestException("");
		}
		final List<Object> result = (List<Object>) (total > pageable.getOffset() ? pageQuery.fetch()
				: Collections.emptyList());
		pageResponse.setCount(Long.valueOf(result.size()));
		pageResponse.setPage(Long.valueOf(pageable.getPageNumber()));
		pageResponse.setTotalCount(total);
		pageResponse.setTotalPages((total % pageable.getPageSize()==0)?(total / pageable.getPageSize()): (total / pageable.getPageSize())+ 1);
		pageResponse.setPageSize(Long.valueOf(pageable.getPageSize()));
		pageResponse.setResultList(result);
		return pageResponse;
	}

	@Override
	public Object create(Object entity) {
		em.persist(entity);
		em.flush();
		return entity;
	}

	@Override
	public Object deleteOneEntity(Object entityPatch, String queryString) throws Exception {
		Object entity = findOneEntity(entityPatch.getClass(), queryString);
		NullAwareBeanUtilsBean copier = new NullAwareBeanUtilsBean();
		copier.copyProperties(entity, entityPatch);
		em.flush();
		return entity;
	}

	public static class NullAwareBeanUtilsBean extends BeanUtilsBean {
		@Override
		public void copyProperty(Object dest, String name, Object value)
				throws IllegalAccessException, InvocationTargetException {
			if (value == null)
				return;
			super.copyProperty(dest, name, value);
		}
	}

	
	
}
