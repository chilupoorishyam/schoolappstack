package com.gts.cms.common.service.impl;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.ConfigAutonumberDTO;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.mapper.ConfigAutonumberMapper;
import com.gts.cms.common.repository.ConfigAutonumberRepository;
import com.gts.cms.common.service.ConfigAutoNumberService;
import com.gts.cms.entity.ConfigAutonumber;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class ConfigAutoNumberServiceImpl implements ConfigAutoNumberService {

	private static final Logger LOGGER = Logger.getLogger(ConfigAutoNumberServiceImpl.class);

	@Autowired
	private ConfigAutonumberMapper configAutoNumberMapper;

	@Autowired
    ConfigAutonumberRepository configAutoNumberRepository;

	@Override
	public ApiResponse<ConfigAutonumberDTO> getConfigAutoNumberDetails(Long autoconfigId) {
		ApiResponse<ConfigAutonumberDTO> response = null;
		if (null == autoconfigId) {
			throw new ApiException(Messages.RECORD_ID_MUST_NOT_NULL.getValue());
		}
		try {
			Optional<ConfigAutonumber> configAutoNumber = configAutoNumberRepository
					.findByAutoconfigIdAndIsActiveTrue(autoconfigId);
			if (configAutoNumber.isPresent()) {
				response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(),
						configAutoNumberMapper.convertEntityToDTO(configAutoNumber.get()), HttpStatus.OK.value());
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
						HttpStatus.OK.value());
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occured while getting configAutoNumber : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;
	}

	@Override
	public ApiResponse<ConfigAutonumberDTO> getAllSchoolConfigAutoNumbers(Long schoolId,
			String configAtttributeCode) {
		ApiResponse<ConfigAutonumberDTO> response = null;
		try {
			ConfigAutonumber configAutoNumber = configAutoNumberRepository.findSchoolConfigAutoNumbers(schoolId,
					configAtttributeCode);

			if (configAutoNumber != null) {

				response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(),
						configAutoNumberMapper.convertEntityToDTO(configAutoNumber));
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
						HttpStatus.OK.value());
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occured while getting ConfigAutoNumbers : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;
	}
	
	@Override
	public ApiResponse<ConfigAutonumberDTO> getConfigAutoNumberDetailsByCode(String configAtttributeCode) {
		ApiResponse<ConfigAutonumberDTO> response = null;
		try {
			ConfigAutonumber configAutoNumber = configAutoNumberRepository.findConfigAutoNumberByCode(configAtttributeCode);

			if (configAutoNumber != null) {

				response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(),
						configAutoNumberMapper.convertEntityToDTO(configAutoNumber));
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
						HttpStatus.OK.value());
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occured while getting ConfigAutoNumbers : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;
	}

	@Override
	public ApiResponse<List<ConfigAutonumberDTO>> getAllConfigAutoNumbers() {
		ApiResponse<List<ConfigAutonumberDTO>> response = null;
		try {
			List<ConfigAutonumber> configAutoNumber = configAutoNumberRepository.findAllConfigAutoNumbers();

			if (!CollectionUtils.isEmpty(configAutoNumber)) {
				List<ConfigAutonumberDTO> configAutoNumberDto = configAutoNumberMapper
						.convertEntityListToDTOList(configAutoNumber);
				response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), configAutoNumberDto);
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
						HttpStatus.OK.value());
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occured while getting ConfigAutoNumbers : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;
	}

	@Override
	public ApiResponse<Long> addConfigAutoNumber(ConfigAutonumberDTO configAutoNumberDTO) {
		ApiResponse<Long> response = null;
		try {
			ConfigAutonumber configAutoNumber = configAutoNumberMapper.convertDTOtoEntity(configAutoNumberDTO, null);

			configAutoNumber = configAutoNumberRepository.save(configAutoNumber);
			if (configAutoNumber != null && configAutoNumber.getAutoconfigId() != 0) {
				response = new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(), configAutoNumber.getAutoconfigId(),
						HttpStatus.OK.value());
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occured while adding configAutoNumber : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;
	}

	@Override
	public ApiResponse<Long> updateConfigAutoNumber(ConfigAutonumberDTO configAutoNumberDTO) {
		ApiResponse<Long> response = null;
		try {
			ConfigAutonumber configAutoNumber = configAutoNumberMapper.convertDTOtoEntity(configAutoNumberDTO, null);
			configAutoNumber = configAutoNumberRepository.save(configAutoNumber);
			if (configAutoNumber != null && configAutoNumber.getAutoconfigId() != 0) {
				response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), configAutoNumber.getAutoconfigId(),
						HttpStatus.OK.value());
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.UPDATE_FAILURE.getValue(), HttpStatus.OK.value());
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occured while updating configAutoNumber : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;

	}

	@Override
	@Transactional
	public ApiResponse<Long> deleteConfigAutoNumber(Long autoconfigId) {
		ApiResponse<Long> response = null;
		if (null == autoconfigId) {
			throw new ApiException(Messages.RECORD_ID_MUST_NOT_NULL.getValue());
		}

		try {
			Long deleteCount = configAutoNumberRepository.deleteConfigAutoNumber(autoconfigId);
			if (deleteCount > 0) {
				response = new ApiResponse<>(Messages.DELETE_SUCCESS.getValue(), autoconfigId, HttpStatus.OK.value());
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.DELETE_FAILURE.getValue(), HttpStatus.OK.value());
			}

		} catch (Exception e) {
			LOGGER.error("Exception Occured while deleting configAutoNumber : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;
	}

	@Override
	@Transactional
	public ApiResponse<Long> addConfigAutoNumbersList(List<ConfigAutonumberDTO> configAutoNumberDTOs) {
		ApiResponse<Long> response = null;

		try {
			List<ConfigAutonumber> configAutoNumber = configAutoNumberMapper
					.convertDTOListToEntityList(configAutoNumberDTOs);

			configAutoNumber = configAutoNumberRepository.saveAll(configAutoNumber);
			configAutoNumberRepository.flush();
			if (configAutoNumber != null) {
				response = new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(), HttpStatus.OK.value());
			} else {
				throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue());
			}
		} catch (Exception e) {
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}
		return response;
	}

}
