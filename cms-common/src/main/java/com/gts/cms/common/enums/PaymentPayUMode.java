package com.gts.cms.common.enums;
public enum PaymentPayUMode {

    NB,DC,CC,CASH,EMI,CLEMI,IVR,COD
}