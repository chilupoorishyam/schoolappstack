package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.QualificationRepositoryCustom;
import com.gts.cms.entity.Qualification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface QualificationRepository extends JpaRepository<Qualification, Long>, QualificationRepositoryCustom {

    public List<Qualification> findByIsActiveTrue();

    public Optional<Qualification> findByQualificationIdAndIsActiveTrue(Long qualificationId);

    public List<Qualification> findByQualificationCode(String qualificationCode);

}
