package com.gts.cms.common.student.repository.custom;

import com.gts.cms.entity.StudentAcademicbatch;

public interface StudentAcademicBatchesRepositoryCustom {
	
	public StudentAcademicbatch getDetails(Long schoolId, Long studentId);
	
	public Long updateIsActive(Long studentAcademicbatchId, Boolean isActive);
	
	public Long updateSection(Long studentAcademicbatchId, Long groupSectionId);

}
