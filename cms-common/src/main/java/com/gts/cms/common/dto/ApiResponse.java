package com.gts.cms.common.dto;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ApiResponse<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	private Boolean success = Boolean.TRUE;

	protected String message;

	protected Integer statusCode = HttpStatus.OK.value();

	private T data;

	private Boolean paginated = Boolean.FALSE;
	
	private Long page;
	private Long totalPages;
	private Long pageSize;
	private Long totalCount;
	private Integer totalValue;

	public ApiResponse() {
		super();
	}

	public ApiResponse(boolean success, String message, Integer statusCode) {
		super();
		this.success = success;
		this.message = message;
		this.statusCode = statusCode;
	}

	public ApiResponse(String message, Integer statusCode) {
		super();
		this.message = message;
		this.statusCode = statusCode;
	}

	public ApiResponse(String message, T data) {
		this.message = message;
		this.data = data;
	}

	public ApiResponse(boolean success, String message, T data, Integer statusCode) {
		super();
		this.success = success;
		this.message = message;
		this.statusCode = statusCode;
		this.data = data;
	}

	public ApiResponse(String message, T data, Integer statusCode) {
		super();
		this.message = message;
		this.statusCode = statusCode;
		this.data = data;
	}

	public ApiResponse(String message, T data, Boolean paginated) {
		super();
		this.message = message;
		this.data = data;
		this.paginated = paginated;
	}

	public ApiResponse(Boolean success, String message, T data, Boolean paginated, Long page,
			Long pageSize, Long totalCount) {
		super();
		this.success = success;
		this.message = message;
		this.data = data;
		this.paginated = paginated;
		this.page = page;
		this.pageSize = pageSize;
		this.totalCount = totalCount;
	}
	
	public ApiResponse(Boolean success, String message, T data, Boolean paginated, Long page,
			Long pageSize, Long totalCount,Integer totalValue) {
		super();
		this.success = success;
		this.message = message;
		this.data = data;
		this.paginated = paginated;
		this.page = page;
		this.pageSize = pageSize;
		this.totalCount = totalCount;
		this.totalValue=totalValue;
	}


	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public boolean isPaginated() {
		return paginated;
	}

	public void setPaginated(boolean paginated) {
		this.paginated = paginated;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public Boolean getPaginated() {
		return paginated;
	}

	public void setPaginated(Boolean paginated) {
		this.paginated = paginated;
	}

	public Long getPage() {
		return page;
	}

	public void setPage(Long page) {
		this.page = page;
	}

	public Long getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(Long totalPages) {
		this.totalPages = totalPages;
	}

	public Long getPageSize() {
		return pageSize;
	}

	public void setPageSize(Long pageSize) {
		this.pageSize = pageSize;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(Integer totalValue) {
		this.totalValue = totalValue;
	}
}
