package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class GeneralDetailDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long generalDetailId;

	private Date createdDt;

	private Long createdUser;

	@NotNull(message = "General Detail Code is required.")
	private String generalDetailCode;

	@NotNull(message = "General Detail Description is required.")
	private String generalDetaildescription;

	@NotNull(message = "General Detail DisplayName is required.")
	private String generalDetailDisplayName;

	private Integer generalDetailSortOrder;

	@NotNull(message = "General Detail isActive is required.")
	private Boolean isActive;

	@NotNull(message = "General Detail isEditable is required.")
	private Boolean isEditable;

	private String reason;

	@NotNull(message = "General Master Id is required.")
	private Long generalMasterId;

	public Long getGeneralDetailId() {
		return generalDetailId;
	}

	public void setGeneralDetailId(Long generalDetailId) {
		this.generalDetailId = generalDetailId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getGeneralDetailCode() {
		return generalDetailCode;
	}

	public void setGeneralDetailCode(String generalDetailCode) {
		this.generalDetailCode = generalDetailCode;
	}

	public String getGeneralDetaildescription() {
		return generalDetaildescription;
	}

	public void setGeneralDetaildescription(String generalDetaildescription) {
		this.generalDetaildescription = generalDetaildescription;
	}

	public String getGeneralDetailDisplayName() {
		return generalDetailDisplayName;
	}

	public void setGeneralDetailDisplayName(String generalDetailDisplayName) {
		this.generalDetailDisplayName = generalDetailDisplayName;
	}

	public Integer getGeneralDetailSortOrder() {
		return generalDetailSortOrder;
	}

	public void setGeneralDetailSortOrder(Integer generalDetailSortOrder) {
		this.generalDetailSortOrder = generalDetailSortOrder;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsEditable() {
		return isEditable;
	}

	public void setIsEditable(Boolean isEditable) {
		this.isEditable = isEditable;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Long getGeneralMasterId() {
		return generalMasterId;
	}

	public void setGeneralMasterId(Long generalMasterId) {
		this.generalMasterId = generalMasterId;
	}

}
