package com.gts.cms.common.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class EmployeeDailyDetailDTO {

    private Long employeeDailyDetailsId;
    private Integer periodNo;
    private Integer noOfStudentsAttended;
    private String absenteesRollno;
    private String otherActivites;
    private Boolean isAttendanceUpdated;
    private Boolean isMentReportUpdated;
    private Boolean isFacultyportfolioUpdated;
    private Date insertedDate;
    private String remarks;
    private Boolean isActive;
    private String reason;
    private Date createdDt;
    private Long createdUser;
    private Date updatedDt;
    private Long updatedUser;
    private Long employeeDetailId;
    private String employeeName;
    private Long schoolId;
    private String schoolName;
    private String schoolCode;
    private Long departmentId;
    private String departmentName;
    private String departmentCode;
    private Long designationId;
    private String designationName;
    private String designationCode;
    private Long groupSectionId;
    private String groupSection;
    private String groupSectionAcademicYear;
    private String groupSectionCourseYear;
    private String groupSectionCourseGroup;
    private Long theoryLabCatdetId;
    private String theoryLabCatdetCode;
    private Long subUnitTopicId;
    private String subUnitTopicName;
    private String subjectName;
    private String subjectCode;
}
