package com.gts.cms.common.student.dto;

import com.gts.cms.common.dto.ApiResponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author Genesis
 *
 */
public class StudentMedicalCertificateUploadDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long studentMedicalCertificateId;
	private Long studentId;

	private String file1;
	private String file2;
	private String file3;

	private String fileStatus1;
	private String fileStatus2;
	private String fileStatus3;

	private String fileName1;
	private String fileName2;
	private String fileName3;

	private Map<String, MultipartFile> files;
	
	List<ApiResponse<Object>> fileStaus;

	public Long getStudentMedicalCertificateId() {
		return studentMedicalCertificateId;
	}

	public String getFile1() {
		return file1;
	}

	public String getFile2() {
		return file2;
	}

	public String getFile3() {
		return file3;
	}

	public String getFileStatus1() {
		return fileStatus1;
	}

	public String getFileStatus2() {
		return fileStatus2;
	}

	public String getFileStatus3() {
		return fileStatus3;
	}

	public String getFileName1() {
		return fileName1;
	}

	public String getFileName2() {
		return fileName2;
	}

	public String getFileName3() {
		return fileName3;
	}

	public Map<String, MultipartFile> getFiles() {
		return files;
	}

	public void setStudentMedicalCertificateId(Long studentMedicalCertificateId) {
		this.studentMedicalCertificateId = studentMedicalCertificateId;
	}

	public void setFile1(String file1) {
		this.file1 = file1;
	}

	public void setFile2(String file2) {
		this.file2 = file2;
	}

	public void setFile3(String file3) {
		this.file3 = file3;
	}

	public void setFileStatus1(String fileStatus1) {
		this.fileStatus1 = fileStatus1;
	}

	public void setFileStatus2(String fileStatus2) {
		this.fileStatus2 = fileStatus2;
	}

	public void setFileStatus3(String fileStatus3) {
		this.fileStatus3 = fileStatus3;
	}

	public void setFileName1(String fileName1) {
		this.fileName1 = fileName1;
	}

	public void setFileName2(String fileName2) {
		this.fileName2 = fileName2;
	}

	public void setFileName3(String fileName3) {
		this.fileName3 = fileName3;
	}

	public void setFiles(Map<String, MultipartFile> files) {
		this.files = files;
	}

	public List<ApiResponse<Object>> getFileStaus() {
		return fileStaus;
	}

	public void setFileStaus(List<ApiResponse<Object>> fileStaus) {
		this.fileStaus = fileStaus;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

}
