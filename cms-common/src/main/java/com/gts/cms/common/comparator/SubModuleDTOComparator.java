package com.gts.cms.common.comparator;

import java.util.Comparator;

import com.gts.cms.common.dto.SubmoduleDTO;

public class SubModuleDTOComparator implements Comparator<SubmoduleDTO>{

	@Override
	public int compare(SubmoduleDTO o1, SubmoduleDTO o2) {
		
		int var=o1.getSortOrder().compareTo(o2.getSortOrder());
		if(var==0 && !o1.getSubModuleId().equals(o2.getSubModuleId())) {
			var=1;
		}
		return var;
	}

	
}
