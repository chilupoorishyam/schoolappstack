package com.gts.cms.common.enums;

public enum FeeDataTypeEnum {

	PARTICULARS("PRTCLR"), FINES("FINES"), DISCOUNTS("DSCNT"), STUDENT_PARTICULARS("STDPTLRS"), STUDENT_FINE("STDFN"),
	STUDENT_DISCOUNT("STDDSCT");
	private String id;

	private FeeDataTypeEnum(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
