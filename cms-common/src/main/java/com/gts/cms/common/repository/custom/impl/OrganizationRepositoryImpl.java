package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.OrganizationRepositoryCustom;
import com.gts.cms.entity.Organization;
import com.gts.cms.entity.QOrganization;
import com.querydsl.core.dml.UpdateClause;
import com.querydsl.jpa.impl.JPAUpdateClause;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * created by Naveen on 04/09/2018
 * 
 */
@Repository
public class OrganizationRepositoryImpl extends QuerydslRepositorySupport implements OrganizationRepositoryCustom {
	public OrganizationRepositoryImpl() {
		super(Organization.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public Long deleteOrganization(Long organizationId) {
		return update(QOrganization.organization).where(QOrganization.organization.organizationId.eq(organizationId))
				.set(QOrganization.organization.isActive, false).execute();
	}
	
	@Override
	public Long uploadOrganizationLogo(Long orgId, String logoName) {
		UpdateClause<JPAUpdateClause> query = update(QOrganization.organization).where(QOrganization.organization.organizationId.eq(orgId));
		if (logoName != null) {
			query.set(QOrganization.organization.logoPath, logoName);
		}
		return query.execute();
	}

}
