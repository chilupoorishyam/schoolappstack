package com.gts.cms.common.student.service;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.StudentAppWorkflowDTO;
import com.gts.cms.common.student.dto.StudentApplicationDTO;
import com.gts.cms.common.student.dto.StudentApplicationImagesDTO;

import java.util.List;
/**
 *created by sathish on 19/sep/2018
 */
public interface StudentService {
	
	public ApiResponse<?> addStudentApplicationForm(StudentApplicationDTO studentApplicationDTO);
	public String getApplicationNo(Long  schoolId);
	public ApiResponse<StudentApplicationImagesDTO> uploadStudentApplicationFiles(StudentApplicationImagesDTO studentApplicationImagesDTO);
	public ApiResponse<List<StudentApplicationDTO>> getStudentApplicationForm(String studentData, Long schoolId);
	public ApiResponse<List<StudentApplicationDTO>> getAllApplicationForms(String status);
	public ApiResponse<?> updateStudentApplicationFormWorkFlow(StudentAppWorkflowDTO studentAppWorkflowDTO);
	public ApiResponse<?> updateStudentApplicationForm(StudentApplicationDTO studentApplicationDTO);
	public ApiResponse<?> deleteStudentApplicationForm(Long id);
	public ApiResponse<StudentApplicationImagesDTO> updateStudentDocumentsAndImages(
			StudentApplicationImagesDTO studentApplicationImages);
	
}
