package com.gts.cms.common.repository;

import com.gts.cms.entity.CmOutcomeMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CmOutcomeMappingRepository extends JpaRepository<CmOutcomeMapping, Long> {
}