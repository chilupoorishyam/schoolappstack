package com.gts.cms.common.dto;

import java.io.Serializable;

public class PageTabDTO implements Serializable{


	private static final long serialVersionUID = 1L;
	private Long pageTabId;
	private Integer sortOrder;

	private String tabDisplayName;

	private String tabName;

	public Long getPageTabId() {
		return pageTabId;
	}

	public void setPageTabId(Long pageTabId) {
		this.pageTabId = pageTabId;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getTabDisplayName() {
		return tabDisplayName;
	}

	public void setTabDisplayName(String tabDisplayName) {
		this.tabDisplayName = tabDisplayName;
	}

	public String getTabName() {
		return tabName;
	}

	public void setTabName(String tabName) {
		this.tabName = tabName;
	}


}
