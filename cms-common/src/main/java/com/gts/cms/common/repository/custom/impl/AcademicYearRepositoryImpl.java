package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.AcademicYearRepositoryCustom;
import com.gts.cms.entity.AcademicYear;
import com.gts.cms.entity.QAcademicYear;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class AcademicYearRepositoryImpl extends QuerydslRepositorySupport implements AcademicYearRepositoryCustom {
	public AcademicYearRepositoryImpl() {
		super(AcademicYear.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	public Long deleteAcademicYear(Long academicYearId) {
		return update(QAcademicYear.academicYear1).where(QAcademicYear.academicYear1.academicYearId.eq(academicYearId))
				.set(QAcademicYear.academicYear1.isActive, false).execute();
	}

	@Override
	public List<AcademicYear> findByOrganizationIdAndSchoolIdAndIsActiveTrue(Long schoolId) {

		JPQLQuery<AcademicYear> query = from(QAcademicYear.academicYear1)
				.where(QAcademicYear.academicYear1.school.schoolId.eq(schoolId).and(QAcademicYear.academicYear1.isActive.eq(true)));
		List<AcademicYear> academicYearList = query.fetch();
		return academicYearList;

	}

	@Override
	public AcademicYear findByAcademicYearIdAndIsActiveTrue(Long academicYearId) {
		JPQLQuery<AcademicYear> query = from(QAcademicYear.academicYear1)
				.where(QAcademicYear.academicYear1.academicYearId.eq(academicYearId).and(QAcademicYear.academicYear1.isActive.eq(true)));
		AcademicYear academicYear = query.fetchOne();
		return academicYear;
	}

}
