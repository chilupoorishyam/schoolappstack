package com.gts.cms.common.employee.mapper;

import com.gts.cms.*;
import com.gts.cms.common.employee.dto.EmployeeAttendanceListDTO;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.util.FileUtil;
import com.gts.cms.common.util.OptionalUtil;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Genesis
 *
 */
@Component
public class EmployeeAttendanceListMapper implements BaseMapper<EmployeeDetail, EmployeeAttendanceListDTO> {

	/*@Autowired
	EmployeeAttendanceMapper employeeAttendanceMapper;*/
	
	@Value("${s3.url}")
	private String url;

	@Value("${s3.bucket-name}")
	private String bucketName;
	private static final String fileSeparator = FileUtil.getFileSeparator();

	public EmployeeAttendanceListDTO convertEntityToDTO(EmployeeDetail empDetail) {
		EmployeeAttendanceListDTO emAttendanceListDTO = null;
		if (empDetail != null) {
			emAttendanceListDTO = new EmployeeAttendanceListDTO();
			emAttendanceListDTO.setEmployeeId(empDetail.getEmployeeId());
			emAttendanceListDTO.setEmail(empDetail.getEmail());
			emAttendanceListDTO.setEmpNumber(empDetail.getEmpNumber());
			emAttendanceListDTO.setFirstName(empDetail.getFirstName());
			emAttendanceListDTO.setBiometricId(empDetail.getBiometricId());
			/*emAttendanceListDTO.setReportingManagerId(empDetail.getReportingManagerId());*/
			if (empDetail.getReportingManager() != null) {
				emAttendanceListDTO.setReportingManagerId(empDetail.getReportingManager().getEmployeeId());
			}
			emAttendanceListDTO.setLastName(empDetail.getLastName());
			emAttendanceListDTO.setMiddleName(empDetail.getMiddleName());

			if (empDetail.getPhotoPath() != null) {
				StringBuilder photoPath = new StringBuilder();
				photoPath = photoPath.append(url).append(fileSeparator).append(bucketName).append(fileSeparator)
						.append(empDetail.getPhotoPath());
				emAttendanceListDTO.setPhotoPath(photoPath.toString());
			}

			emAttendanceListDTO.setMobile(empDetail.getMobile());
			emAttendanceListDTO.setOrganizationId(
					OptionalUtil.resolve(() -> empDetail.getOrganization().getOrganizationId()).orElse(null));
			emAttendanceListDTO
					.setDistrictId(OptionalUtil.resolve(() -> empDetail.getDistrict().getDistrictId()).orElse(null));
			emAttendanceListDTO.setEmpStatusId(
					OptionalUtil.resolve(() -> empDetail.getEmployeeStatus().getGeneralDetailId()).orElse(null));
			emAttendanceListDTO.setEmpTypeId(
					OptionalUtil.resolve(() -> empDetail.getEmployeeType().getGeneralDetailId()).orElse(null));

			emAttendanceListDTO.setEmpDeptId(
					OptionalUtil.resolve(() -> empDetail.getEmployeeDepartment().getDepartmentId()).orElse(null));
			emAttendanceListDTO.setEmpWorkingDeptId(OptionalUtil
					.resolve(() -> empDetail.getEmployeeWorkingDepartment().getDepartmentId()).orElse(null));

			emAttendanceListDTO.setDesignationId(
					OptionalUtil.resolve(() -> empDetail.getDesignation().getDesignationId()).orElse(null));
			emAttendanceListDTO.setWorkingDesignationId(
					OptionalUtil.resolve(() -> empDetail.getWorkingDesignation().getDesignationId()).orElse(null));

			emAttendanceListDTO
					.setSchoolId(OptionalUtil.resolve(() -> empDetail.getSchool().getSchoolId()).orElse(null));
			emAttendanceListDTO.setTeachingforId(
					OptionalUtil.resolve(() -> empDetail.getTeachingFor().getGeneralDetailId()).orElse(null));

			emAttendanceListDTO
					.setGenderId(OptionalUtil.resolve(() -> empDetail.getGender().getGeneralDetailId()).orElse(null));
			if (empDetail.getGender() != null) {
				emAttendanceListDTO.setGender(empDetail.getGender().getGeneralDetailDisplayName());
			}
			
			/*if (empDetail.getEmpAttendances1() != null) {
				emAttendanceListDTO.setEmployeeAttendances(
						employeeAttendanceMapper.convertEntityListToDTOList(empDetail.getEmpAttendances1()));
			}
*/
		}
		return emAttendanceListDTO;
	}

	public EmployeeDetail convertDTOtoEntity(EmployeeAttendanceListDTO employeeAttendanceListDTO,
			EmployeeDetail empDetail) {
		if (employeeAttendanceListDTO != null) {
			if (null == empDetail) {
				empDetail = new EmployeeDetail();
				empDetail.setIsActive(Boolean.TRUE);
				empDetail.setCreatedDt(new Date());
				empDetail.setCreatedUser(SecurityUtil.getCurrentUser());
			}

			empDetail.setEmployeeId(employeeAttendanceListDTO.getEmployeeId());

			empDetail.setUpdatedDt(new Date());
			empDetail.setUpdatedUser(SecurityUtil.getCurrentUser());

			empDetail.setEmail(employeeAttendanceListDTO.getEmail());
			empDetail.setEmpNumber(employeeAttendanceListDTO.getEmpNumber());
			empDetail.setFirstName(employeeAttendanceListDTO.getFirstName());
			empDetail.setBiometricId(employeeAttendanceListDTO.getBiometricId());
			/*empDetail.setReportingManagerId(employeeAttendanceListDTO.getReportingManagerId());*/
			if(employeeAttendanceListDTO.getReportingManagerId()!=null) {
				EmployeeDetail reportingManager=new EmployeeDetail();
				reportingManager.setEmployeeId(employeeAttendanceListDTO.getReportingManagerId());
				empDetail.setReportingManager(reportingManager);
			}
			empDetail.setLastName(employeeAttendanceListDTO.getLastName());
			empDetail.setMiddleName(employeeAttendanceListDTO.getMiddleName());
			empDetail.setPhotoPath(FileUtil.getRelativePath(employeeAttendanceListDTO.getPhotoPath()));
			empDetail.setMobile(employeeAttendanceListDTO.getMobile());

			if (employeeAttendanceListDTO.getOrganizationId() != null) {
				Organization organization = new Organization();
				organization.setOrganizationId(employeeAttendanceListDTO.getOrganizationId());
				empDetail.setOrganization(organization);
			}

			if (employeeAttendanceListDTO.getDistrictId() != null) {
				District district = new District();
				district.setDistrictId(employeeAttendanceListDTO.getDistrictId());
				empDetail.setDistrict(district);
			}

			if (employeeAttendanceListDTO.getEmpStatusId() != null) {
				GeneralDetail empStatus = new GeneralDetail();
				empStatus.setGeneralDetailId(employeeAttendanceListDTO.getEmpStatusId());
				empDetail.setEmployeeStatus(empStatus);
			}

			if (employeeAttendanceListDTO.getEmpTypeId() != null) {
				GeneralDetail empType = new GeneralDetail();
				empType.setGeneralDetailId(employeeAttendanceListDTO.getEmpTypeId());
				empDetail.setEmployeeType(empType);
			}

			if (employeeAttendanceListDTO.getEmpDeptId() != null) {
				Department empDept = new Department();
				empDept.setDepartmentId(employeeAttendanceListDTO.getEmpDeptId());
				empDetail.setEmployeeDepartment(empDept);
			}

			if (employeeAttendanceListDTO.getEmpWorkingDeptId() != null) {
				Department empWorkingDept = new Department();
				empWorkingDept.setDepartmentId(employeeAttendanceListDTO.getEmpWorkingDeptId());
				empDetail.setEmployeeWorkingDepartment(empWorkingDept);
			}

			if (employeeAttendanceListDTO.getDesignationId() != null) {
				Designation designation = new Designation();
				designation.setDesignationId(employeeAttendanceListDTO.getDesignationId());
				empDetail.setDesignation(designation);
			}

			if (employeeAttendanceListDTO.getWorkingDesignationId() != null) {
				Designation workingDesignation = new Designation();
				workingDesignation.setDesignationId(employeeAttendanceListDTO.getWorkingDesignationId());
				empDetail.setWorkingDesignation(workingDesignation);
			}

			if (employeeAttendanceListDTO.getSchoolId() != null) {
				School school = new School();
				school.setSchoolId(employeeAttendanceListDTO.getSchoolId());
				empDetail.setSchool(school);
			}

			if (employeeAttendanceListDTO.getTeachingforId() != null) {
				GeneralDetail teachingFor = new GeneralDetail();
				teachingFor.setGeneralDetailId(employeeAttendanceListDTO.getTeachingforId());
				empDetail.setTeachingFor(teachingFor);
			}

			if (SecurityUtil.getCurrentUser() != null) {
				User user = new User();
				user.setUserId(SecurityUtil.getCurrentUser());
				empDetail.setUser(user);
			}

			if (employeeAttendanceListDTO.getGenderId() != null) {
				GeneralDetail gender = new GeneralDetail();
				gender.setGeneralDetailId(employeeAttendanceListDTO.getGenderId());
				empDetail.setGender(gender);
			}

		}
		return empDetail;
	}

	public List<EmployeeDetail> convertDTOListToEntityList(List<EmployeeAttendanceListDTO> empDetailDTOList) {
		List<EmployeeDetail> empDetailList = new ArrayList<>();
		if (empDetailDTOList != null) {
			empDetailDTOList.forEach(empDetailDTO -> empDetailList.add(convertDTOtoEntity(empDetailDTO, null)));
		}
		return empDetailList;

	}

	public List<EmployeeAttendanceListDTO> convertEntityListToDTOList(List<EmployeeDetail> empDetailList) {
		List<EmployeeAttendanceListDTO> empDetailDTOList = new ArrayList<>();
		empDetailList.forEach(empDetail -> empDetailDTOList.add(convertEntityToDTO(empDetail)));
		return empDetailDTOList;
	}

}
