package com.gts.cms.common.dto;

import java.util.Date;

public class StudentAppEducationDTO {
	
	private Long appEducationId;
	private String address;

	private String board;

	private Long createdUser;

	private String gradeClassSecured;

	private Boolean isActive;

	private String majorSubjects;

	private String medium;

	private String nameOfInstitution;

	private Double precentage;

	private String reason;

	private Date updatedDt;

	private Long updatedUser;

	private String yearOfCompletion;

	public Long getAppEducationId() {
		return appEducationId;
	}

	public void setAppEducationId(Long appEducationId) {
		this.appEducationId = appEducationId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public String getGradeClassSecured() {
		return gradeClassSecured;
	}

	public void setGradeClassSecured(String gradeClassSecured) {
		this.gradeClassSecured = gradeClassSecured;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getMajorSubjects() {
		return majorSubjects;
	}

	public void setMajorSubjects(String majorSubjects) {
		this.majorSubjects = majorSubjects;
	}

	public String getMedium() {
		return medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

	public String getNameOfInstitution() {
		return nameOfInstitution;
	}

	public void setNameOfInstitution(String nameOfInstitution) {
		this.nameOfInstitution = nameOfInstitution;
	}

	

	public Double getPrecentage() {
		return precentage;
	}

	public void setPrecentage(Double precentage) {
		this.precentage = precentage;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getYearOfCompletion() {
		return yearOfCompletion;
	}

	public void setYearOfCompletion(String yearOfCompletion) {
		this.yearOfCompletion = yearOfCompletion;
	}

}
