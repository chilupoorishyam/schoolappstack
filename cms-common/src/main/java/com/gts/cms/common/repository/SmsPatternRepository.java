package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.SmsPatternRepositoryCustom;
import com.gts.cms.entity.SmsPattern;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Genesis
 *
 */
@Repository
public interface SmsPatternRepository extends JpaRepository<SmsPattern, Long>, SmsPatternRepositoryCustom {
	
	
}
