package com.gts.cms.common.student.service;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.StudentDetailListDTO;
import com.gts.cms.common.dto.StudentSubjectDTO;

import java.util.List;

/**
 * @author Genesis
 *
 */
public interface StudentSubjectService {

	public ApiResponse<Long> addStudentSubjects(List<StudentDetailListDTO> studentDetailListDTOs);
	public ApiResponse<List<StudentSubjectDTO>> getStudentSubject(Long schoolId, Long academicYearId,Long courseYearId,Long subjectId);
}

