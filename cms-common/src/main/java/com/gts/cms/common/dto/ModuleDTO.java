package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

public class ModuleDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long moduleId;
	private String displayName;
	private Integer sortOrder;
	private Set<PageDTO> pages;
	private String moduleName;
	private String iconName;
	private Set<SubmoduleDTO> subModules;
	private Date createdDt;
	private Long createdUser;
	private Date updatedDt;
	private Long updatedUser;
	private Boolean isActive;
	private String url;
	private String reason;
	
	public String getIconName() {
		return iconName;
	}
	public void setIconName(String iconName) {
		this.iconName = iconName;
	}
	public Long getModuleId() {
		return moduleId;
	}
	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public Integer getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
	public Set<PageDTO> getPages() {
		return pages;
	}
	public void setPages(Set<PageDTO> pages) {
		this.pages = pages;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public Set<SubmoduleDTO> getSubModules() {
		return subModules;
	}
	public void setSubModules(Set<SubmoduleDTO> subModules) {
		this.subModules = subModules;
	}
	@Override
	public int hashCode() {
		return moduleId.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModuleDTO other = (ModuleDTO) obj;
		if (moduleId == null) {
			if (other.moduleId != null)
				return false;
		} else if (!moduleId.equals(other.moduleId)) {
			return false;
		}
		return true;
	}
	public Date getCreatedDt() {
		return createdDt;
	}
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	public Long getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}
	public Date getUpdatedDt() {
		return updatedDt;
	}
	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}
	public Long getUpdatedUser() {
		return updatedUser;
	}
	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
}
