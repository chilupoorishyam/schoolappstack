package com.gts.cms.common.employee.service.impl;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.employee.dto.EmployeeDetailDTO;
import com.gts.cms.common.employee.dto.EmployeeDetailStgDTO;
import com.gts.cms.common.employee.mapper.EmployeeDetailMapper;
import com.gts.cms.common.employee.mapper.EmployeeDetailStgMapper;
import com.gts.cms.common.employee.repository.EmployeeDetailRepository;
import com.gts.cms.common.employee.repository.EmployeeDetailStgRepository;
import com.gts.cms.common.employee.service.EmployeeDetailStgService;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.repository.*;
import com.gts.cms.entity.*;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeDetailStgServiceImpl implements EmployeeDetailStgService {
    private static final Logger LOGGER = Logger.getLogger(EmployeeDetailStgServiceImpl.class);

    private static final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    @Autowired
    private EmployeeDetailRepository employeeDetailRepository;

    @Autowired
    private EmployeeDetailStgRepository employeeDetailStgRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserTypeRepository userTypeRepository;

    @Autowired
    private EmployeeDetailStgMapper employeeDetailStgMapper;

    @Autowired
    EmployeeDetailMapper employeeDetailMapper;

    @Autowired
    OrganizationRepository organizationRepository;

    @Autowired
    SchoolRepository schoolRepository;

    @Autowired
    GeneralDetailRepository generalDetailRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    QualificationRepository qualificationRepository;

    @Autowired
    CityRepository cityRepository;

    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    DesignationRepository designationRepository;

    @Override
    public ApiResponse<List<EmployeeDetailStgDTO>> uploadExcel(EmployeeDetailStgDTO employeeDetailStgDTO) {
        LOGGER.info("EmployeeDetailStgServiceImpl.uploadExcel()" + System.currentTimeMillis());
        Long startTime = System.currentTimeMillis();
        ApiResponse<List<EmployeeDetailStgDTO>> response = null;
        Workbook workbook = null;
        Sheet sheet = null;
        Cell cell = null;
        List<EmployeeDetailStg> employeeDetailStgList = new ArrayList<>();
        try {
            MultipartFile excelFile = employeeDetailStgDTO.getFiles().get("file");
            workbook = getWorkbook(excelFile, workbook);
            LOGGER.info("Excel file ==>" + excelFile.getName());
            if (workbook != null) {
                sheet = workbook.getSheetAt(0);
                for (Integer i = 1; i <= sheet.getLastRowNum(); i++) {
                    Row row = sheet.getRow(i);
                    LOGGER.info("Row Count of excel ==>" + i);
                    EmployeeDetailStg employeeDetailStg = new EmployeeDetailStg();
                    if (row.getCell(0) != null) {
                        employeeDetailStg.setOrganization(row.getCell(0).getStringCellValue());
                    }
                    if (row.getCell(1) != null) {
                        employeeDetailStg.setSchool(row.getCell(1).getStringCellValue());
                    }
                    if (row.getCell(2) != null) {
                        cell = row.getCell(2);
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        employeeDetailStg.setEmpNo(cell.getStringCellValue());
                    }
                    if (row.getCell(3) != null && !row.getCell(3).getStringCellValue().isEmpty()) {
                        try {
                            Date dateOfJoin = row.getCell(3).getDateCellValue();
                            String strDateOfJoin = dateFormat.format(dateOfJoin);
                            employeeDetailStg.setDateOfJoin(strDateOfJoin);
                        } catch (Exception e) {
                            throw new ApiException("Check DOJ format it should be DD-MM-YYYY");
                        }
                    }
                    if (row.getCell(4) != null) {
                        employeeDetailStg.setFirstName(row.getCell(4).getStringCellValue());
                    }
                    if (row.getCell(5) != null) {
                        employeeDetailStg.setMiddleName(row.getCell(5).getStringCellValue());
                    }
                    if (row.getCell(6) != null) {
                        employeeDetailStg.setLastName(row.getCell(6).getStringCellValue());
                    }
                    if (row.getCell(7) != null) {
                        employeeDetailStg.setNationality(row.getCell(7).getStringCellValue());
                    }
                    if (row.getCell(10) != null) {
                        employeeDetailStg.setGender(row.getCell(10).getStringCellValue());
                    }
                    if (row.getCell(11) != null && !row.getCell(11).getStringCellValue().isEmpty()) {
                        if (i == 135) {
                            try {
                                Date dateOfBirth = row.getCell(11).getDateCellValue();
                                String strDateOfBirth = dateFormat.format(dateOfBirth);
                                employeeDetailStg.setDateOfBirth(strDateOfBirth);
                            } catch (Exception e) {
                                throw new ApiException("Check Excel Row number " + i + "  DOB format it should be DD-MM-YYYY");
                            }
                        }
                    }
                    //String mobileNo = row.getCell(12).getStringCellValue();
                    if (row.getCell(12) != null) {
                        cell = row.getCell(12);
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        employeeDetailStg.setMobileNumber(cell.getStringCellValue());
                    }
                    if (row.getCell(13) != null) {
                        cell = row.getCell(13);
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        employeeDetailStg.setEmail(cell.getStringCellValue());
                    }
                    if (row.getCell(14) != null) {
                        cell = row.getCell(14);
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        employeeDetailStg.setAadhaarNo(cell.getStringCellValue());
                    }
                    if (row.getCell(15) != null) {
                        cell = row.getCell(15);
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        employeeDetailStg.setPanNo(cell.getStringCellValue());
                    }
                    if (row.getCell(16) != null) {
                        employeeDetailStg.setDepartment(row.getCell(16).getStringCellValue());
                    }
                    if (row.getCell(17) != null) {
                        employeeDetailStg.setWorkingDepartment(row.getCell(17).getStringCellValue());
                    }
                    if (row.getCell(18) != null) {
                        employeeDetailStg.setQualification(row.getCell(18).getStringCellValue());
                    }
                    if (row.getCell(19) != null) {
                        employeeDetailStg.setDesignation(row.getCell(19).getStringCellValue());
                    }
                    if (row.getCell(21) != null) {
                        employeeDetailStg.setEmployeeCategory(row.getCell(21).getStringCellValue());
                    }
                    if (row.getCell(22) != null) {
                        employeeDetailStg.setAddress(row.getCell(22).getStringCellValue());
                    }
                    if (row.getCell(29) != null) {
                        employeeDetailStg.setCity(row.getCell(29).getStringCellValue());
                    }
                    if (row.getCell(30) != null) {
                        employeeDetailStg.setDistrict(row.getCell(30).getStringCellValue());
                    }
                    employeeDetailStgList.add(employeeDetailStg);
                }
                employeeDetailStgRepository.saveAll(employeeDetailStgList);
                try {
                    workbook.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    LOGGER.error("Exception while closing sheet: " + e);
                }
                List<EmployeeDetailStgDTO> examStgMarksDTOList = employeeDetailStgMapper.convertEntityListToDTOList(employeeDetailStgList);
                response = new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(), examStgMarksDTOList);
            } else {
                response = new ApiResponse<>("Unable to read workbook", null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Exception while reading sheet: " + e);
            response = new ApiResponse<>(false, "Error while parsing workbook:: " + e, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
        Long endTime = System.currentTimeMillis();
        Long totalTime = endTime - startTime;
        LOGGER.info("EmployeeDetailStgServiceImpl.uploadExcel() total time: " + totalTime);
        return response;
    }

    @Override
    public ApiResponse<List<EmployeeDetailStgDTO>> getStgEmployeeDetails() {
        LOGGER.info("EmployeeDetailStgServiceImpl.getStgEmployeeDetails() fetching all employee srg data");
        ApiResponse<List<EmployeeDetailStgDTO>> response = null;
        List<EmployeeDetailStg> employeeDetailStgList = employeeDetailStgRepository.findAll();
        List<EmployeeDetailStgDTO> examStgMarksDTOList = employeeDetailStgMapper.convertEntityListToDTOList(employeeDetailStgList);
        if (!examStgMarksDTOList.isEmpty()) {
            response = new ApiResponse<>("Data", examStgMarksDTOList);
        } else
            response = new ApiResponse<>(Messages.RECORDS_NOT_FOUND.getValue(), examStgMarksDTOList);
        return response;
    }

    @Override
    public ApiResponse<List<EmployeeDetailDTO>> processStgEmployeeDetails() {
        LOGGER.info("EmployeeDetailStgServiceImpl.processStgEmployeeDetails()" + System.currentTimeMillis());
        Long startTime = System.currentTimeMillis();
        ApiResponse<List<EmployeeDetailDTO>> response = null;
        List<EmployeeDetail> employeeDetailList = new ArrayList<>();
        List<EmployeeDetailStg> deleteList = new ArrayList<>();
        List<User> userList = new ArrayList<>();
        try {
            List<EmployeeDetailStg> employeeDetailStgList = employeeDetailStgRepository.findAll();
            for (EmployeeDetailStg employeeDetailStg : employeeDetailStgList) {
                List<EmployeeDetail> checkEmployeeExists = employeeDetailRepository.checkEmployeeExists(employeeDetailStg.getFirstName(), employeeDetailStg.getLastName());
                if (checkEmployeeExists.isEmpty()) {
                    EmployeeDetail employeeDetail = new EmployeeDetail();
                    User user = new User();
                    Optional<Organization> organization = organizationRepository.findByOrgCode(employeeDetailStg.getOrganization());
                    if (organization.isPresent()) {
                        employeeDetail.setOrganization(organization.get());
                        user.setOrganization(organization.get());
                    }
                    Optional<School> school = schoolRepository.findBySchoolCodeAndIsActive(employeeDetailStg.getSchool(), true);
                    if (school.isPresent()) {
                        employeeDetail.setSchool(school.get());
                        user.setSchool(school.get());
                    }
                    Usertype usertype = new Usertype();
                    usertype.setUserTypeId(userTypeRepository.findByUserName("STAFF"));
                    user.setUserType(usertype);
                    employeeDetail.setEmpNumber(employeeDetailStg.getEmpNo());
                    employeeDetail.setFirstName(employeeDetailStg.getFirstName());
                    employeeDetail.setMiddleName(employeeDetailStg.getMiddleName());
                    employeeDetail.setLastName(employeeDetailStg.getLastName());
                    user.setUserName(employeeDetailStg.getEmpNo());
                    user.setFirstName(employeeDetailStg.getFirstName());
                    user.setLastName(employeeDetailStg.getLastName());
                    //Gender Details
                    GeneralDetail gender = new GeneralDetail();
                    gender.setGeneralDetailId(generalDetailRepository.findByGeneralDetailCode(employeeDetailStg.getGender()));
                    employeeDetail.setGender(gender);
                    if (employeeDetailStg.getDateOfBirth() != null) {
                        Date dateOfBirth = new SimpleDateFormat("dd/MM/yyyy").parse(employeeDetailStg.getDateOfBirth());
                        employeeDetail.setDateOfBirth(dateOfBirth);
                    }
                    if (employeeDetailStg.getDateOfJoin() != null) {
                        Date dateOfJoin = new SimpleDateFormat("dd/MM/yyyy").parse(employeeDetailStg.getDateOfJoin());
                        employeeDetail.setJoiningDate(dateOfJoin);
                    }
                    GeneralDetail nationality = new GeneralDetail();
                    nationality.setGeneralDetailId(generalDetailRepository.findByGeneralDetailCode(employeeDetailStg.getNationality()));
                    employeeDetail.setNationality(nationality);
                    GeneralDetail employeeStatus = new GeneralDetail();
                    employeeStatus.setGeneralDetailId(generalDetailRepository.findByGeneralDetailCode("ACTV"));
                    employeeDetail.setEmployeeStatus(employeeStatus);
                    employeeDetail.setMobile(employeeDetailStg.getMobileNumber());
                    employeeDetail.setOfficialMobile(employeeDetailStg.getMobileNumber());
                    employeeDetail.setEmail(employeeDetailStg.getEmail());
                    user.setMobileNumber(employeeDetailStg.getMobileNumber());
                    user.setEmail(employeeDetailStg.getEmail());
                    user.setPassword(employeeDetailStg.getEmpNo());
                    employeeDetail.setAadharNo(employeeDetailStg.getAadhaarNo());
                    employeeDetail.setPancard(employeeDetailStg.getPanNo());
                    List<Department> department = departmentRepository.findByDeptCode(employeeDetailStg.getDepartment());
                    if (!department.isEmpty()) {
                        employeeDetail.setEmployeeDepartment(department.get(0));
                        employeeDetail.setEmployeeWorkingDepartment(department.get(0));
                    }

                    List<Qualification> qualification = qualificationRepository.
                            findByQualificationCode(employeeDetailStg.getQualification());
                    if (!qualification.isEmpty()) {
                        employeeDetail.setQualification(qualification.get(0));
                    }
                    Optional<Designation> designation = designationRepository.findByDesignationName(employeeDetailStg.getDesignation());
                    if (designation.isPresent()) {
                        employeeDetail.setDesignation(designation.get());
                    }
                    employeeDetail.setAddress(employeeDetailStg.getAddress());

                    Optional<City> city = cityRepository.
                            findByCityName(employeeDetailStg.getCity());
                    if (city.isPresent()) {
                        employeeDetail.setPresentCity(city.get());
                    }

                    Optional<District> district = districtRepository.
                            findByDistrictName(employeeDetailStg.getDistrict());
                    if (district.isPresent()) {
                        employeeDetail.setDistrict(district.get());
                    }
                    String createdDate = dateFormat.format(new Date());
                    Date todaysDate = dateFormat.parse(createdDate);
                    employeeDetail.setCreatedDt(todaysDate);
                    employeeDetail.setIsActive(Boolean.TRUE);
                    user.setCreatedDt(todaysDate);
                    user.setIsActive(Boolean.TRUE);
                    user.setIsEditable(Boolean.TRUE);
                    user.setIsReset(Boolean.FALSE);
                    employeeDetailList.add(employeeDetail);
                    userList.add(user);
                    deleteList.add(employeeDetailStg);

                }
            }
            employeeDetailRepository.saveAll(employeeDetailList);
            /*Commented below line - failing due to mandatory fields like email and other. This process will be handled in stored procedure*/
            //userRepository.saveAll(userList);
            employeeDetailStgRepository.deleteAll(deleteList);
            List<EmployeeDetailDTO> employeeDetailDTOS = employeeDetailMapper.convertEntityListToDTOList(employeeDetailList);
            response = new ApiResponse<>("Data", employeeDetailDTOS);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Error while processing the Employee Data");
            response = new ApiResponse<>(false, "Error while processing the Employee Data:: " + e, HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
        Long endTime = System.currentTimeMillis();
        Long totalTime = endTime - startTime;
        LOGGER.info("EmployeeDetailStgServiceImpl.processStgEmployeeDetails() total time: " + totalTime);
        return response;
    }

    private Workbook getWorkbook(MultipartFile excelFile, Workbook workbook) {
        try {
            if (excelFile != null) {
                String originalFileName = excelFile.getOriginalFilename();
                if (originalFileName != null && originalFileName.length() > 0) {
                    switch (originalFileName.substring(originalFileName.lastIndexOf('.') + 1,
                            originalFileName.length())) {
                        case "xls":
                            try {
                                workbook = WorkbookFactory.create(excelFile.getInputStream());
                            } catch (org.apache.poi.openxml4j.exceptions.InvalidFormatException ie) {
                                LOGGER.error("Malformed Excel");
                                throw new IOException();
                            }
                            break;
                        case "xlsx":
                            try {
                                workbook = WorkbookFactory.create(excelFile.getInputStream());
                            } catch (org.apache.poi.openxml4j.exceptions.InvalidFormatException ie) {
                                LOGGER.error("Malformed Excel");
                                throw new IOException();
                            }
                            break;
                        default:
                            LOGGER.error("File type is not  recognized  Excell type");
                            throw new IOException();
                    }

                } else {
                    LOGGER.error("Can Not Read File Name");
                    throw new IOException();
                }
            } else {
                LOGGER.error("Did not select a file");
                throw new IOException();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return workbook;
    }
}
