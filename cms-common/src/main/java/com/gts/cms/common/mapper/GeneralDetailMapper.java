package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.GeneralDetailDTO;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.GeneralDetail;
import com.gts.cms.entity.GeneralMaster;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class GeneralDetailMapper implements BaseMapper<GeneralDetail, GeneralDetailDTO> {

	public GeneralDetailDTO convertEntityToDTO(GeneralDetail generalDetail) {
		GeneralDetailDTO generalDetailDTO = null;
		if (generalDetail != null) {
			generalDetailDTO = new GeneralDetailDTO();

			generalDetailDTO.setGeneralDetailId(generalDetail.getGeneralDetailId());
			if (generalDetail.getGeneralMaster() != null) {
				generalDetailDTO.setGeneralMasterId(generalDetail.getGeneralMaster().getGeneralMasterId());
			}
			generalDetailDTO.setGeneralDetailCode(generalDetail.getGeneralDetailCode());
			generalDetailDTO.setGeneralDetaildescription(generalDetail.getGeneralDetailDescription());
			generalDetailDTO.setGeneralDetailDisplayName(generalDetail.getGeneralDetailDisplayName());
			generalDetailDTO.setGeneralDetailSortOrder(generalDetail.getGeneralDetailSortOrder());
			generalDetailDTO.setCreatedDt(generalDetail.getCreatedDt());
			generalDetailDTO.setCreatedUser(generalDetail.getCreatedUser());
			generalDetailDTO.setIsActive(generalDetail.getIsActive());
			generalDetailDTO.setIsEditable(generalDetail.getIsEditable());
			generalDetailDTO.setReason(generalDetail.getReason());
		}
		return generalDetailDTO;
	}

	public GeneralDetail convertDTOtoEntity(GeneralDetailDTO generalDetailDTO, GeneralDetail generalDetail) {
		if (null == generalDetail) {
			generalDetail = new GeneralDetail();
			generalDetail.setIsActive(Boolean.TRUE);
			generalDetail.setCreatedDt(new Date());
			generalDetail.setCreatedUser(SecurityUtil.getCurrentUser());
		}
		generalDetail.setGeneralDetailId(generalDetailDTO.getGeneralDetailId());
		GeneralMaster generalMaster = new GeneralMaster();
		generalMaster.setGeneralMasterId(generalDetailDTO.getGeneralMasterId());
		generalDetail.setGeneralMaster(generalMaster);
		generalDetail.setGeneralDetailDisplayName(generalDetailDTO.getGeneralDetailDisplayName());
		generalDetail.setGeneralDetailCode(generalDetailDTO.getGeneralDetailCode());
		generalDetail.setGeneralDetailDescription(generalDetailDTO.getGeneralDetaildescription());
		generalDetail.setGeneralDetailSortOrder(generalDetailDTO.getGeneralDetailSortOrder());
		generalDetail.setIsEditable(generalDetailDTO.getIsEditable());
		generalDetail.setReason(generalDetailDTO.getReason());

		generalDetail.setUpdatedDt(new Date());
		generalDetail.setUpdatedUser(SecurityUtil.getCurrentUser());
		return generalDetail;
	}

	public List<GeneralDetail> convertDTOListToEntityList(List<GeneralDetailDTO> generalDetailDTOList) {
		List<GeneralDetail> generalDetailList = new ArrayList<>();
		generalDetailDTOList
				.forEach(generalDetailDTO -> generalDetailList.add(convertDTOtoEntity(generalDetailDTO, null)));
		return generalDetailList;

	}

	public List<GeneralDetailDTO> convertEntityListToDTOList(List<GeneralDetail> generalDetailList) {
		List<GeneralDetailDTO> generalDetailDTOList = new ArrayList<>();
		generalDetailList.forEach(generalDetail -> generalDetailDTOList.add(convertEntityToDTO(generalDetail)));
		return generalDetailDTOList;
	}

}
