package com.gts.cms.common.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class EmployeeAttendanceDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long employeeAttendanceId;
	
	private Long schoolId;
	
	private Long EmployeeId;
	
	private Long leavetypeId;
	
	private Date attendanceDate;
	
	private Date loginDateTime;
	
	private Date logoutDateTime;
	
	private Boolean isHalfday;
	
	private Boolean isPresent;
	
	private Boolean isManual;
	
	private Boolean isEditable;
	
	private Boolean isVerified;
	
	private String verfiedReason;
	
	private Boolean isFreeze;
	
	private String reason;
	
	private String comments;
	
	private String punchRecords;
	
	private Integer earlyBy;
	
	@NotNull(message="isActive is required")
	private Boolean isActive;

	
	private Date createdDt;
	
	private Long createdUser;
	
	private Date updatedDt;
	
	private Long updatedUser;
	
	private String schoolCode;
	private String schoolName;
	private String firstName;
	private String middleName;	
	private String lastName;
	private String leaveCode;
	private BigDecimal leaveCount;
	private String leaveName;
	private Boolean isMissedInPunch;
	private Boolean isMissedOutPunch;
	
	public Long getEmployeeAttendanceId() {
		return employeeAttendanceId;
	}
	public void setEmployeeAttendanceId(Long employeeAttendanceId) {
		this.employeeAttendanceId = employeeAttendanceId;
	}
	public Long getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}
	public Long getEmployeeId() {
		return EmployeeId;
	}
	public void setEmployeeId(Long employeeId) {
		EmployeeId = employeeId;
	}
	public Long getLeavetypeId() {
		return leavetypeId;
	}
	public void setLeavetypeId(Long leavetypeId) {
		this.leavetypeId = leavetypeId;
	}
	public Date getAttendanceDate() {
		return attendanceDate;
	}
	public void setAttendanceDate(Date attendanceDate) {
		this.attendanceDate = attendanceDate;
	}
	public Date getLoginDateTime() {
		return loginDateTime;
	}
	public void setLoginDateTime(Date loginDateTime) {
		this.loginDateTime = loginDateTime;
	}
	public Date getLogoutDateTime() {
		return logoutDateTime;
	}
	public void setLogoutDateTime(Date logoutDateTime) {
		this.logoutDateTime = logoutDateTime;
	}
	public Boolean getIsHalfday() {
		return isHalfday;
	}
	public void setIsHalfday(Boolean isHalfday) {
		this.isHalfday = isHalfday;
	}
	public Boolean getIsPresent() {
		return isPresent;
	}
	public void setIsPresent(Boolean isPresent) {
		this.isPresent = isPresent;
	}
	public Boolean getIsManual() {
		return isManual;
	}
	public void setIsManual(Boolean isManual) {
		this.isManual = isManual;
	}
	public Boolean getIsEditable() {
		return isEditable;
	}
	public void setIsEditable(Boolean isEditable) {
		this.isEditable = isEditable;
	}
	public Boolean getIsVerified() {
		return isVerified;
	}
	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}
	public String getVerfiedReason() {
		return verfiedReason;
	}
	public void setVerfiedReason(String verfiedReason) {
		this.verfiedReason = verfiedReason;
	}
	public Boolean getIsFreeze() {
		return isFreeze;
	}
	public void setIsFreeze(Boolean isFreeze) {
		this.isFreeze = isFreeze;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public Date getCreatedDt() {
		return createdDt;
	}
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	public Long getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}
	public Date getUpdatedDt() {
		return updatedDt;
	}
	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}
	public Long getUpdatedUser() {
		return updatedUser;
	}
	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}
	public String getSchoolCode() {
		return schoolCode;
	}
	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getLeaveCode() {
		return leaveCode;
	}
	public void setLeaveCode(String leaveCode) {
		this.leaveCode = leaveCode;
	}
	
	public String getLeaveName() {
		return leaveName;
	}
	public void setLeaveName(String leaveName) {
		this.leaveName = leaveName;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getPunchRecords() {
		return punchRecords;
	}
	public void setPunchRecords(String punchRecords) {
		this.punchRecords = punchRecords;
	}
	public BigDecimal getLeaveCount() {
		return leaveCount;
	}
	public void setLeaveCount(BigDecimal leaveCount) {
		this.leaveCount = leaveCount;
	}
	public Integer getEarlyBy() {
		return earlyBy;
	}
	public void setEarlyBy(Integer earlyBy) {
		this.earlyBy = earlyBy;
	}
	public Boolean getIsMissedInPunch() {
		return isMissedInPunch;
	}
	public void setIsMissedInPunch(Boolean isMissedInPunch) {
		this.isMissedInPunch = isMissedInPunch;
	}
	public Boolean getIsMissedOutPunch() {
		return isMissedOutPunch;
	}
	public void setIsMissedOutPunch(Boolean isMissedOutPunch) {
		this.isMissedOutPunch = isMissedOutPunch;
	}
}
