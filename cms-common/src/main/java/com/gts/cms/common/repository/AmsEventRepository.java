package com.gts.cms.common.repository;

import com.gts.cms.entity.AmsEvent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AmsEventRepository extends  JpaRepository<AmsEvent, Long> {

}
