package com.gts.cms.common.student.repository;

import com.gts.cms.entity.StudentDetailStg;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentDetailStgRepository extends JpaRepository<StudentDetailStg, Long> {

}
