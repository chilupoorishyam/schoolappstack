package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.PageRepositoryCustom;
import com.gts.cms.entity.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * created by Naveen(Auto) on 02/09/2018
 * 
 */
@Repository
public interface PageRepository extends JpaRepository<Page, Long>, PageRepositoryCustom {


}
