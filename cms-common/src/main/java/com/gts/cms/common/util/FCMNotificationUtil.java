package com.gts.cms.common.util;

import com.fasterxml.jackson.databind.JsonMappingException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

@Service
public class FCMNotificationUtil {
    private static final Logger LOGGER = Logger.getLogger(FCMNotificationUtil.class);

    @Value("${fcm.url}")
    private static String fcmUrl;

    @Value("${fcm.api-key}")
    private String apiKey;

    public static void post(String apiKey, FCMContent content) {

        try {

            // 1. URL
            URL url = new URL(fcmUrl);

            // 2. Open connection
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            // 3. Specify POST method
            conn.setRequestMethod("POST");

            // 4. Set the headers
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization", "key=" + apiKey);

            conn.setDoOutput(true);

            // 5. Add JSON data into POST request body

            // `5.1 Use Jackson object mapper to convert Contnet object into JSON
            //	ObjectMapper mapper = new ObjectMapper();

            // 5.2 Get connection output stream
            OutputStreamWriter wr = new OutputStreamWriter(
                    conn.getOutputStream());

            // 5.3 Copy Content "JSON" into
            wr.write(content.toString());

            // 5.4 Send the request
            wr.flush();

            // 5.5 close
            wr.close();

            // 6. Get the response
            int responseCode = conn.getResponseCode();
            LOGGER.info("Sending 'POST' request to URL : " + url);
            LOGGER.info("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // 7. Print result
            LOGGER.info("Response : " + response.toString());

        } catch (JsonMappingException e) {
            LOGGER.error("JsonMappingException  " + e);
        } catch (MalformedURLException e) {
            LOGGER.error("MalformedURLException  " + e);
        } catch (IOException e) {
            LOGGER.error("IOException  " + e);
        }
    }

}
