package com.gts.cms.common.employee.service.impl;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.EmployeeListDTO;
import com.gts.cms.common.dto.EmployeeReportingDTO;
import com.gts.cms.common.dto.SubjectResourceDTO;
import com.gts.cms.common.employee.dto.*;
import com.gts.cms.common.employee.mapper.*;
import com.gts.cms.common.employee.repository.*;
import com.gts.cms.common.employee.service.EmployeeDetailService;
import com.gts.cms.common.enums.ConfigAutoNumbersCodeEnum;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.exception.BadRequestException;
import com.gts.cms.common.mapper.EmployeeListMapper;
import com.gts.cms.common.repository.ConfigAutonumberRepository;
import com.gts.cms.common.repository.GeneralDetailRepository;
import com.gts.cms.common.service.FileService;
import com.gts.cms.common.util.FileUtil;
import com.gts.cms.entity.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

/**
 * @author Genesis
 */
@Service
public class EmployeeDetailServiceImpl implements EmployeeDetailService {
    private static final Logger LOGGER = Logger.getLogger(EmployeeDetailServiceImpl.class);

    private static final String FILE_ORG = "org_";
    private static final String FILE_COLLEGE = "school_";
    private static final String FILE_APP = "empapp_";

    private static final String FILE_AADHAR = "aadhar.";
    private static final String FILE_PAN = "pan.";
    private static final String FILE_PASSPORT = "passport.";
    private static final String FILE_PHOTO = "photo.";
    private static final String FILE_VOTER = "voterid.";

    private static final String FILE_SEPARATOR = "file.separator";

    private static final String fileSeparator = FileUtil.getFileSeparator();

    @Autowired
    private EmployeeDetailMapper employeeDetailMapper;

    @Autowired
    private EmployeeListMapper employeeListMapper;

    @Autowired
    EmployeeBankDetailMapper employeeBankDetailMapper;

    @Autowired
    EmployeeDocumentCollectionMapper employeeDocumentCollectionMapper;

    @Autowired
    EmployeeDetailRepository employeeDetailRepository;

    @Autowired
    EmployeeBankDetailRepository employeeBankDetailRepository;

    @Autowired
    EmployeeDocumentCollectionRepository employeeDocumentCollectionRepository;

    @Autowired
    FileService fileService;

    @Autowired
    private ConfigAutonumberRepository configAutoNumberRepository;

    @Autowired
    private EmployeeEducationMapper employeeEducationMapper;

    @Autowired
    private EmployeeReportingMapper employeeReportingMapper;

    @Autowired
    private EmployeeExperienceDetailMapper employeeExperienceDetailMapper;

   /* @Autowired
    StaffCourseyrSubjectsRepository staffCourseyrSubjectsRepository;

    @Autowired
    StaffCourseyrSubjectMapper staffCourseyrSubjectMapper;*/

    @Autowired
    EmployeeReportingRepository employeeReportingRepository;

    @Autowired
    EmpDeptHeadsRepository empDeptHeadRepository;
    @Autowired
    private GeneralDetailRepository generalDetailRepository;

   /* @Autowired
    SubjectResourceRepository subjectResourceRepository;
    @Autowired
    SubjectResourceMapper subjectResourceMapper;
    @Autowired
    CounselorMappingRepository counselorMappingRepository;
    @Autowired
    CounselorMappingMapper counselorMappingMapper;
    @Autowired
    StudentAttendanceRepository studentAttendanceRepository;
    @Autowired
    StudentAttendanceListMapper studentAttendanceListMapper;
    @Autowired
    StudentAttendanceMapper studentAttendanceMapper;*/
    @Autowired
    EmployeeReportingRepository empReportingRepo;

    @Override
    @Transactional
    public ApiResponse<?> addEmployeeDetailForm(EmployeeDetailDTO employeeDetailDTO) {

        ApiResponse<Object> response = null;
        try {
            EmployeeDetail employeeDetail = employeeDetailMapper.convertDTOtoEntity(employeeDetailDTO, null);

            String empNumber = null;
            Long currentNumber = null;
            ConfigAutonumber configAutoNumber = configAutoNumberRepository.findSchoolConfigAutoNumbers(
                    employeeDetailDTO.getSchoolId(), ConfigAutoNumbersCodeEnum.EMP_APP.getId());

            if (configAutoNumber != null && configAutoNumber.getAutoconfigId() != null
                    && configAutoNumber.getPrefix() != null && configAutoNumber.getSuffix() != null
                    && configAutoNumber.getCurrentNumber() != null) {
                currentNumber = Long.parseLong(configAutoNumber.getCurrentNumber()) + 1;
                empNumber = configAutoNumber.getSchool().getSchoolCode() + "/" + configAutoNumber.getSuffix() + "/" + currentNumber;
                configAutoNumber.setCurrentNumber(currentNumber.toString());
            }
            EmployeeDetail checkEmpExists = employeeDetailRepository.findByEmpNumber(empNumber);
            if (checkEmpExists == null) {
                if (empNumber != null) {
                    employeeDetail.setEmpNumber(empNumber);
                    employeeDetail = employeeDetailRepository.save(employeeDetail);
                    employeeDetailRepository.flush();
                    if (employeeDetail != null && employeeDetail.getEmployeeId() != null) {

                        EmployeeDetailDTO applicationDTO = new EmployeeDetailDTO();
                        applicationDTO.setEmployeeId(employeeDetail.getEmployeeId());
                        applicationDTO.setEmpNumber(empNumber);

                        if (null != employeeDetail.getEmpDocumentCollections1()) {
                            List<EmployeeDocumentCollection> employeeDocumentCollections = employeeDetail
                                    .getEmpDocumentCollections1();
                            List<EmployeeDocumentCollectionDTO> employeeDocumentCollectionDTOs = employeeDocumentCollectionMapper
                                    .convertEntityListToDTOList(employeeDocumentCollections);
                            applicationDTO.setEmployeeDocumentCollection(employeeDocumentCollectionDTOs);
                        }
                        if (null != employeeDetail.getEmpEmployeeEducations()) {
                            List<EmployeeEducation> employeeEducations = employeeDetail.getEmpEmployeeEducations();
                            List<EmployeeEducationDTO> employeeEducationDTOs = employeeEducationMapper
                                    .convertEntityListToDTOList(employeeEducations);
                            applicationDTO.setEmployeeEducations(employeeEducationDTOs);
                        }
                        if (null != employeeDetail.getEmpEmployeeBankDetails()) {
                            List<EmployeeBankDetail> employeeBankDetails = employeeDetail.getEmpEmployeeBankDetails();
                            List<EmployeeBankDetailDTO> employeeBankDetailDTOs = employeeBankDetailMapper
                                    .convertEntityListToDTOList(employeeBankDetails);
                            applicationDTO.setEmployeeBankDetails(employeeBankDetailDTOs);
                        }
                        if (null != employeeDetail.getEmpEmployeeReportings1()) {
                            List<EmployeeReporting> employeeReportings = employeeDetail.getEmpEmployeeReportings1();
                            List<EmployeeReportingDTO> employeeReportingDTOs = employeeReportingMapper
                                    .convertEntityListToDTOList(employeeReportings);
                            applicationDTO.setEmployeeReportings(employeeReportingDTOs);
                        }
                        if (null != employeeDetail.getEmpExperienceDetails()) {
                            List<EmployeeExperienceDetail> employeeExperienceDetails = employeeDetail
                                    .getEmpExperienceDetails();
                            List<EmployeeExperienceDetailDTO> employeeExperienceDetailDTOs = employeeExperienceDetailMapper
                                    .convertEntityListToDTOList(employeeExperienceDetails);
                            applicationDTO.setEmpExperienceDetails(employeeExperienceDetailDTOs);
                        }

                        response = new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(), applicationDTO,
                                HttpStatus.OK.value());
                        try {
                            Long count = configAutoNumberRepository.updateCurrentNumber(configAutoNumber);
                        } catch (Exception e) {
                            LOGGER.error("Exception Occured while updating currentnumber in ConfigAutoNumber  : " + e);
                        }
                    }
                } else {
                    throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue());
                }
            } else {
                return new ApiResponse<>(false, "(" + empNumber + ") is Duplicate!", null, HttpStatus.OK.value());
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("Exception Occured while adding Employee : " + e);
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }

    @Override
    public ApiResponse<EmployeeDetailDTO> getEmployeeDetail(Long employeeId, Long userId) {
        ApiResponse<EmployeeDetailDTO> response = null;
        try {
            Optional<EmployeeDetail> employeeDetail = employeeDetailRepository
                    .findByEmployeeIdAndIsActiveTrue(employeeId, userId);
            if (employeeDetail.isPresent()) {
                response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(),
                        employeeDetailMapper.convertEntityToDTO(employeeDetail.get()), HttpStatus.OK.value());
            } else {
                response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
                        HttpStatus.OK.value());
            }
        } catch (Exception e) {
            LOGGER.error("Exception Occured while getting employeeDetail : " + e);
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }

	/*@Override
	@Transactional
	public ApiResponse<?> updateEmployeeDetail(EmployeeDetailDTO employeeDetailDTO) {
		ApiResponse<Object> response = null;
		EmployeeDetail empDtl = null;
		Optional<GeneralDetail> generalDetail=null;
		try {
			Optional<EmployeeDetail> employee = employeeDetailRepository.findById(employeeDetailDTO.getEmployeeId());
			if(employeeDetailDTO.getEmpStatusId()!=null) {
				 generalDetail=generalDetailRepository.findById(employeeDetailDTO.getEmpStatusId());
			}
			
			if (employee.isPresent()) {
				empDtl = employee.get();
			}
			if(generalDetail!=null && generalDetail.isPresent() && generalDetail.get().getGeneralDetailCode().equals(Messages.INACTIVE.getValue())) {
			List<StaffCourseyrSubject> courseYrSubjects = staffCourseyrSubjectsRepository.getStaffCourseYrSubjectsByEmpId(Boolean.TRUE, empDtl.getEmployeeId());
			
				if(courseYrSubjects.size()>0) {
					response = new ApiResponse<>(Boolean.FALSE,"Staff is assigned to Subject", staffCourseyrSubjectMapper.convertEntityListToDTOList(courseYrSubjects), HttpStatus.OK.value());
					return response;
				}
			}
			
			EmployeeDetail employeeDetail = employeeDetailMapper.convertDTOtoEntity(employeeDetailDTO, empDtl);
			employeeDetail = employeeDetailRepository.save(employeeDetail);
			employeeDetailRepository.flush();
			if (employeeDetail != null && employeeDetail.getEmployeeId() != 0) {
					EmployeeDetailDTO applicationDTO = new EmployeeDetailDTO();
					applicationDTO.setEmployeeId(employeeDetail.getEmployeeId());
					applicationDTO.setEmpNumber(employeeDetail.getEmpNumber());

					if (null != employeeDetail.getEmpDocumentCollections1()) {
						List<EmployeeDocumentCollection> employeeDocumentCollections = employeeDetail
								.getEmpDocumentCollections1();
						List<EmployeeDocumentCollectionDTO> employeeDocumentCollectionDTOs = employeeDocumentCollectionMapper
								.convertEntityListToDTOList(employeeDocumentCollections);
						applicationDTO.setEmployeeDocumentCollection(employeeDocumentCollectionDTOs);
					}
					if (null != employeeDetail.getEmpEmployeeEducations()) {
						List<EmployeeEducation> employeeEducations = employeeDetail.getEmpEmployeeEducations();
						List<EmployeeEducationDTO> employeeEducationDTOs = employeeEducationMapper
								.convertEntityListToDTOList(employeeEducations);
						applicationDTO.setEmployeeEducations(employeeEducationDTOs);
					}
					if (null != employeeDetail.getEmpEmployeeBankDetails()) {
						List<EmployeeBankDetail> employeeBankDetails = employeeDetail.getEmpEmployeeBankDetails();
						List<EmployeeBankDetailDTO> employeeBankDetailDTOs = employeeBankDetailMapper
								.convertEntityListToDTOList(employeeBankDetails);
						applicationDTO.setEmployeeBankDetails(employeeBankDetailDTOs);
					}
					if (null != employeeDetail.getEmpEmployeeReportings1()) {
						List<EmployeeReporting> employeeReportings = employeeDetail.getEmpEmployeeReportings1();
						List<EmployeeReportingDTO> employeeReportingDTOs = employeeReportingMapper
								.convertEntityListToDTOList(employeeReportings);
						applicationDTO.setEmployeeReportings(employeeReportingDTOs);
					}
					if (null != employeeDetail.getEmpExperienceDetails()) {
						List<EmployeeExperienceDetail> employeeExperienceDetails = employeeDetail
								.getEmpExperienceDetails();
						List<EmployeeExperienceDetailDTO> employeeExperienceDetailDTOs = employeeExperienceDetailMapper
								.convertEntityListToDTOList(employeeExperienceDetails);
						applicationDTO.setEmpExperienceDetails(employeeExperienceDetailDTOs);
					}
				
				response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), applicationDTO,
						HttpStatus.OK.value());
			} else {
				response = new ApiResponse<>(Boolean.FALSE, Messages.UPDATE_FAILURE.getValue(), HttpStatus.OK.value());
			}
		} catch (Exception e) {
			LOGGER.error("Exception Occured while updating employeeDetail : " + e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;

	}*/

    @Override
    @Transactional
    public ApiResponse<Long> deleteEmployeeDetail(Long employeeId) {
        ApiResponse<Long> response = null;
        if (null == employeeId) {
            throw new ApiException(Messages.RECORD_ID_MUST_NOT_NULL.getValue());
        }

        try {
            Long deleteCount = employeeDetailRepository.deleteEmployeeDetail(employeeId);
            if (deleteCount > 0) {
                response = new ApiResponse<>(Messages.DELETE_SUCCESS.getValue(), employeeId, HttpStatus.OK.value());
            } else {
                response = new ApiResponse<>(Boolean.FALSE, Messages.DELETE_FAILURE.getValue(), HttpStatus.OK.value());
            }

        } catch (Exception e) {
            LOGGER.error("Exception Occured while deleting employeeDetail : " + e);
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }
        return response;
    }

    @Override
    @Transactional
    public ApiResponse<EmployeeDetailUploadDTO> uploadEmployeeApplicationFiles(
            EmployeeDetailUploadDTO employeeDetailUploadDTO) {
        ApiResponse<EmployeeDetailUploadDTO> response = null;

        if (null == employeeDetailUploadDTO || null == employeeDetailUploadDTO.getEmpNumber()
                || null == employeeDetailUploadDTO.getSchoolCode() || null == employeeDetailUploadDTO.getOrgCode()) {
            throw new ApiException(Messages.RECORD_ID_MUST_NOT_NULL.getValue());
        }

        try {
            String aadharFileName = null;
            String pancardFileName = null;
            String passportFileName = null;
            String photoFileName = null;
            String voterIdFileName = null;
            String docName = null;
            if (employeeDetailUploadDTO.getFiles() == null) {
                throw new ApiException(Messages.NO_FILES_FOUND.getValue());
            } else {

                MultipartFile aadharFile = employeeDetailUploadDTO.getFiles().get("aadharFile");
                MultipartFile pancardFile = employeeDetailUploadDTO.getFiles().get("pancardFile");
                MultipartFile passportFile = employeeDetailUploadDTO.getFiles().get("passportFile");
                MultipartFile photoFile = employeeDetailUploadDTO.getFiles().get("photoFile");
                MultipartFile voterIdFile = employeeDetailUploadDTO.getFiles().get("voterIdFile");

                if (aadharFile != null) {
                    try {
                        aadharFileName = fileService.uploadFile(aadharFile,
                                FILE_ORG + employeeDetailUploadDTO.getOrgCode() + fileSeparator + FILE_COLLEGE
                                        + employeeDetailUploadDTO.getSchoolCode() + fileSeparator + FILE_APP
                                        + employeeDetailUploadDTO.getEmpNumber() + fileSeparator + FILE_AADHAR
                                        + FilenameUtils.getExtension(aadharFile.getOriginalFilename()));
                        employeeDetailUploadDTO.setAadharFileStatus(Status.SUCESS.getName());

                    } catch (Exception e) {
                        employeeDetailUploadDTO.setAadharFileStatus(Messages.UPLOAD_AADHAR_FAILURE.getValue());
                    }
                }

                if (pancardFile != null) {
                    try {
                        pancardFileName = fileService.uploadFile(pancardFile,
                                FILE_ORG + employeeDetailUploadDTO.getOrgCode() + fileSeparator + FILE_COLLEGE
                                        + employeeDetailUploadDTO.getSchoolCode() + fileSeparator + FILE_APP
                                        + employeeDetailUploadDTO.getEmpNumber() + fileSeparator + FILE_PAN
                                        + FilenameUtils.getExtension(pancardFile.getOriginalFilename()));
                        employeeDetailUploadDTO.setPancardFileStatus(Status.SUCESS.getName());
                    } catch (Exception e) {
                        employeeDetailUploadDTO.setPancardFileStatus(Messages.UPLOAD_PAN_FAILURE.getValue());
                    }
                }

                if (passportFile != null) {
                    try {
                        passportFileName = fileService.uploadFile(passportFile,
                                FILE_ORG + employeeDetailUploadDTO.getOrgCode() + fileSeparator + FILE_COLLEGE
                                        + employeeDetailUploadDTO.getSchoolCode() + fileSeparator + FILE_APP
                                        + employeeDetailUploadDTO.getEmpNumber() + fileSeparator + FILE_PASSPORT
                                        + FilenameUtils.getExtension(passportFile.getOriginalFilename()));
                        employeeDetailUploadDTO.setPassportFileStatus(Status.SUCESS.getName());
                    } catch (Exception e) {
                        employeeDetailUploadDTO.setPassportFileStatus(Messages.UPLOAD_PASSPORT_FAILURE.getValue());
                    }
                }

                if (photoFile != null) {
                    try {
                        photoFileName = fileService.uploadFile(photoFile,
                                FILE_ORG + employeeDetailUploadDTO.getOrgCode() + fileSeparator + FILE_COLLEGE
                                        + employeeDetailUploadDTO.getSchoolCode() + fileSeparator + FILE_APP
                                        + employeeDetailUploadDTO.getEmpNumber() + fileSeparator + FILE_PHOTO
                                        + FilenameUtils.getExtension(photoFile.getOriginalFilename()));
                        employeeDetailUploadDTO.setPhotoFileStatus(Status.SUCESS.getName());
                    } catch (Exception e) {
                        employeeDetailUploadDTO.setPhotoFileStatus(Messages.UPLOAD_PHOTO_FAILURE.getValue());
                    }
                }

                if (voterIdFile != null) {
                    try {
                        voterIdFileName = fileService.uploadFile(voterIdFile,
                                FILE_ORG + employeeDetailUploadDTO.getOrgCode() + fileSeparator + FILE_COLLEGE
                                        + employeeDetailUploadDTO.getSchoolCode() + fileSeparator + FILE_APP
                                        + employeeDetailUploadDTO.getEmpNumber() + fileSeparator + FILE_VOTER
                                        + FilenameUtils.getExtension(voterIdFile.getOriginalFilename()));
                        employeeDetailUploadDTO.setVoterIdFileStatus(Status.SUCESS.getName());
                    } catch (Exception e) {
                        employeeDetailUploadDTO.setVoterIdFileStatus(Messages.UPLOAD_VOTER_FAILURE.getValue());
                    }
                }

                if (aadharFileName != null || pancardFileName != null || pancardFileName != null
                        || photoFileName != null || voterIdFileName != null) {
                    try {
                        Long updatedCount = employeeDetailRepository.uploadEmployeeApplicationFiles(
                                employeeDetailUploadDTO.getEmployeeId(), aadharFileName, pancardFileName,
                                passportFileName, photoFileName, voterIdFileName);

                        if (updatedCount != null && updatedCount != 0) {
                            response = new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(), employeeDetailUploadDTO,
                                    HttpStatus.OK.value());
                        } else {
                            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue());
                        }
                    } catch (Exception e) {
                        response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), employeeDetailUploadDTO,
                                HttpStatus.OK.value());
                    }
                } else {
                    response = new ApiResponse<>(Messages.FAILED_TO_UPLOAD_IMAGES.getValue(), employeeDetailUploadDTO,
                            HttpStatus.OK.value());
                }

                //String docPath = null;

                Map<String, MultipartFile> files = employeeDetailUploadDTO.getFiles();
                for (Entry<String, MultipartFile> entry : files.entrySet()) {
                    try {
                        String docRepId = entry.getKey();
                        if (docRepId.matches("[0-9]+")) {
                            MultipartFile appFile = entry.getValue();
                            String fileName = FilenameUtils.getBaseName(appFile.getOriginalFilename());
                            String fileExtension = FilenameUtils.getExtension(appFile.getOriginalFilename());
                            docName = fileService.uploadFile(appFile,
                                    FILE_ORG + employeeDetailUploadDTO.getOrgCode() + fileSeparator + FILE_COLLEGE
                                            + employeeDetailUploadDTO.getSchoolCode() + fileSeparator + FILE_APP
                                            + employeeDetailUploadDTO.getEmpNumber() + fileSeparator + fileName + "."
                                            + fileExtension);
                            employeeDetailUploadDTO.setAllEmpDocumentsStatus(Status.SUCESS.getName());
                            //docPath = FileUtil.getAbsolutePath(docName);
                            employeeDetailUploadDTO.setFiles(null);
                            if (docName != null) {

                                Long updatedCount = employeeDocumentCollectionRepository.uploadEmployeeDocumentFiles(
                                        employeeDetailUploadDTO.getEmployeeId(), docRepId, docName);
                                if (updatedCount == null || updatedCount == 0) {
                                    response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(),
                                            employeeDetailUploadDTO, HttpStatus.OK.value());

                                } else {
                                    response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(),
                                            employeeDetailUploadDTO, HttpStatus.OK.value());
                                }

                            } else {
                                response = new ApiResponse<>(Messages.FAILED_TO_UPLOAD_DOCUMENTS.getValue(),
                                        employeeDetailUploadDTO, HttpStatus.OK.value());
                            }
                        } else {
                            employeeDetailUploadDTO.setFiles(null);
                            response = new ApiResponse<>(Messages.RECORD_ID_MUST_NOT_NULL.getValue(),
                                    employeeDetailUploadDTO, HttpStatus.OK.value());
                        }
                    } catch (Exception e) {
                        employeeDetailUploadDTO.setFiles(null);
                        employeeDetailUploadDTO.setEmpDocumentsStatus("Failed to upload Employee Documents");
                        response = new ApiResponse<>(Messages.UPDATE_FAILURE.getValue(), employeeDetailUploadDTO,
                                HttpStatus.INTERNAL_SERVER_ERROR.value());
                    }
                }
            }

        } catch (Exception e) {
            LOGGER.error("Exception Occured while upload EmployeeApplicationFiles: " + e);
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }
        return response;
    }

    @Override
    public ApiResponse<List<EmployeeDetailDTO>> getAllEmployeesDetails(Long schoolId, Long empDeptId, Boolean status,
                                                                       String aadharNo, String empNumber, String firstName, String mobile) {
        ApiResponse<List<EmployeeDetailDTO>> response = null;
        try {
            List<EmployeeDetail> employeeDetails = employeeDetailRepository.findAllEmployeeDetail(schoolId, empDeptId,
                    status, aadharNo, empNumber, firstName, mobile);

            if (!CollectionUtils.isEmpty(employeeDetails)) {
                List<EmployeeDetailDTO> employeeDetailDTOs = employeeDetailMapper
                        .convertEntityListToDTOList(employeeDetails);
                response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), employeeDetailDTOs);
            } else {
                response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
                        HttpStatus.OK.value());
            }
        } catch (Exception e) {
            LOGGER.error("Exception Occured while getting ConfigAutoNumbers : " + e);
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }

    @Override
    public ApiResponse<List<EmployeeListDTO>> getEmployeesList(Long schoolId, Long empDeptId, Boolean status,
                                                               String aadharNo, String empNumber, String firstName, String mobile) {
        ApiResponse<List<EmployeeListDTO>> response = null;
        try {
            List<EmployeeDetail> employeeDetails = employeeDetailRepository.findAllEmployeeDetail(schoolId, empDeptId,
                    status, aadharNo, empNumber, firstName, mobile);

            if (!CollectionUtils.isEmpty(employeeDetails)) {
                List<EmployeeListDTO> employeeDetailDTOs = employeeListMapper
                        .convertEntityListToDTOList(employeeDetails);
                response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), employeeDetailDTOs);
            } else {
                response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
                        HttpStatus.OK.value());
            }
        } catch (Exception e) {
            LOGGER.error("Exception Occured while getting ConfigAutoNumbers : " + e);
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }

    @Override
    public ApiResponse<List<EmployeeListDTO>> searchEmployeeDetail(Long schoolId, String query, Long deptId, String empStatus) {
        ApiResponse<List<EmployeeListDTO>> response = null;
        //query = query.trim();
        if ((query == null) || (query != null && query.length() < 4)) {
            throw new BadRequestException("Query String length cannot be lessthan 4 characters.");
        }
        try {
            String firstName = query, lastName = query, middleName = query, employeeNo = query, mobile = query;
            List<EmployeeDetail> employeeDetailsList;
            if (query.matches("[0-9]+")) {
				/*if (query.length() < 10) {
					throw new BadRequestException("Mobile Number cannot be lessthan 10 characters.");
				}*/
                employeeDetailsList = employeeDetailRepository.searchEmployeeDetails(schoolId, null, null, null, null, mobile, deptId, empStatus);
            } else if (query.chars().allMatch(Character::isLetter)) {
                employeeDetailsList = employeeDetailRepository.searchEmployeeDetails(schoolId, firstName, lastName, middleName, null, null, deptId, empStatus);
            } else {
                employeeDetailsList = employeeDetailRepository.searchEmployeeDetails(schoolId, firstName, null, null, employeeNo, null, deptId, empStatus);
            }
            if (!CollectionUtils.isEmpty(employeeDetailsList)) {
                List<EmployeeListDTO> empDetailListDto = employeeListMapper.convertEntityListToDTOList(employeeDetailsList);
                response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), empDetailListDto);
            } else {
                response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
                        HttpStatus.OK.value());
            }
        } catch (Exception e) {
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }

    @Override
    @Transactional
    public ApiResponse<?> addEmployeeManager(EmployeeReportingDTO employeeReportingDTO) {
        LOGGER.info("EmployeeDetailServiceImpl.addEmployeeManager()");
        ApiResponse<Object> response = null;
        EmployeeReporting empReporting = null;
        try {
            EmployeeDetail employeeDetail = employeeDetailRepository.findById(employeeReportingDTO.getEmpId()).get();

            if (employeeDetail != null) {
                EmployeeDetail manager = new EmployeeDetail();

                manager.setEmployeeId(employeeReportingDTO.getManagerEmpId());

                Designation designation = new Designation();
                designation.setDesignationId(employeeReportingDTO.getEmpDesignationId());

                employeeDetail.setDesignation(designation);
                employeeDetail.setReportingManager(manager);
                employeeDetail = employeeDetailRepository.save(employeeDetail);
                employeeDetailRepository.flush();

                if (employeeDetail != null) {

                    empReporting = employeeReportingMapper.convertDTOtoEntity(employeeReportingDTO, null);
                }

                if (empReporting != null) {
                    empReporting = employeeReportingRepository.save(empReporting);
                    employeeReportingRepository.flush();
                }

                if (empReporting != null) {
                    response = new ApiResponse<>(Boolean.TRUE, Messages.ADDED_SUCCESS.getValue(), employeeReportingDTO, HttpStatus.OK.value());
                } else {
                    response = new ApiResponse<>(Boolean.FALSE, Messages.ADDED_FALURE.getValue(), HttpStatus.OK.value());
                }

            } else {
                throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue());
            }
        } catch (Exception e) {
            LOGGER.error("Exception Occured while adding Employee : " + e);
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }

    @Override
    public ApiResponse<List<EmployeeDetailDTO>> getAllEmployeesDetailsByCourseGroup(Boolean status, Long schoolId) {
        ApiResponse<List<EmployeeDetailDTO>> response = null;
        LOGGER.info("EmployeeDetailServiceImpl.getAllEmployeesDetailsByCourseGroup ");
        try {

            List<EmployeeDetail> employeeDetail = new ArrayList<EmployeeDetail>();
            EmpDeptHeads empDeptHeads = empDeptHeadRepository.findByCourseGroupIdAndSchoolId(schoolId);

            //Long school=empDeptHeads.getSchool().getSchoolId();
            if (empDeptHeads != null && empDeptHeads.getDepartment() != null) {
                employeeDetail = employeeDetailRepository.findAllEmployeeDetail(schoolId, empDeptHeads.getDepartment().getDepartmentId(), status, null, null, null, null);
            }


            List<EmployeeDetailDTO> dtoList = employeeDetailMapper.convertEntityListToDTOList(employeeDetail);

            if (employeeDetail.size() > 0) {
                response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), dtoList, HttpStatus.OK.value());
            } else {
                response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
                        HttpStatus.OK.value());
            }
        } catch (Exception e) {
            LOGGER.error("Exception Occured while getting employeeDetail : " + e);
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }

    @Override
    public ApiResponse<List<EmployeeDetailDTO>> getDeptWiseCounsellor(Long schoolId, Long empDeptId, Boolean status) {
        ApiResponse<List<EmployeeDetailDTO>> response = null;
        LOGGER.info("EmployeeDetailServiceImpl.getDeptWiseCounsellor ");
        try {

            List<EmployeeDetail> employeeDetail = new ArrayList<EmployeeDetail>();
            employeeDetail = employeeDetailRepository.findByDeptWiseCounselors(schoolId, empDeptId, status);


            List<EmployeeDetailDTO> dtoList = employeeDetailMapper.convertEntityListToDTOList(employeeDetail);

            if (employeeDetail.size() > 0) {
                response = new ApiResponse<>(Messages.RETRIEVED_SUCCESS.getValue(), dtoList, HttpStatus.OK.value());
            } else {
                response = new ApiResponse<>(Boolean.FALSE, Messages.RECORDS_NOT_FOUND.getValue(),
                        HttpStatus.OK.value());
            }
        } catch (Exception e) {
            LOGGER.error("Exception Occured while getting getDeptWiseCounsellor : " + e);
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;
    }

    /**
     * when staff is going to resign, we are checking in staffcourseyear subjects, subject resources, counsellor mapping
     */
    @Override
    @Transactional
    public ApiResponse<?> updateEmployeeDetail(EmployeeDetailDTO employeeDetailDTO) {
        LOGGER.info("EmployeeDetailServiceImpl.updateEmployeeDetail()");
        ApiResponse<Object> response = null;
        EmployeeDetail empDtl = null;
        Optional<GeneralDetail> generalDetail = null;
        List<StaffCourseyrSubject> staffCySubject = null;
        EmployeeResignedDTO employeeResignedDTO = null;
        //List<StaffCourseyrSubjectDTO> staffCourseyrSubjectDTOs = null;
        List<SubjectResource> subjectResources = null;
        List<SubjectResourceDTO> subjectResourcesDTO = null;
        List<CounselorMapping> counselorMappings = null;
        //List<CounselorMappingDTO> counselorMappingDTOs = null;
        List<EmployeeReportingDTO> employeeDetailRepotingDTO = null;
        //List<StudentAttendanceDTO> studentAttendanceDTOs = null;

        List<EmployeeReporting> employeeDetailRepoting = null;
        List<StudentAttendance> studentAttendances = null;
        try {
            Optional<EmployeeDetail> employee = employeeDetailRepository.findById(employeeDetailDTO.getEmployeeId());
            if (employeeDetailDTO.getEmpStatusId() != null) {
                generalDetail = generalDetailRepository.findById(employeeDetailDTO.getEmpStatusId());
            }

            if (employee.isPresent()) {
                empDtl = employee.get();
            }
            if (generalDetail != null && generalDetail.isPresent() && generalDetail.get().getGeneralDetailCode().equals(Messages.INACTIVE.getValue())) {
                employeeResignedDTO = new EmployeeResignedDTO();

                //checking staffcourse year subject
                //staffCySubject = staffCourseyrSubjectsRepository.findStaffCourseYrSubject(employeeDetailDTO.getEmployeeId(), employeeDetailDTO.getDateOfRelieving());

                //checking subject resources
                //subjectResources = subjectResourceRepository.findSubjectResources(employeeDetailDTO.getEmployeeId(), employeeDetailDTO.getDateOfRelieving());

                //checking counsellor
                //counselorMappings = counselorMappingRepository.findCounselorMappings(employeeDetailDTO.getEmployeeId(), employeeDetailDTO.getDateOfRelieving());

                //reporting check
                employeeDetailRepoting = empReportingRepo.findReportingDetails(employeeDetailDTO.getEmployeeId(), employeeDetailDTO.getDateOfRelieving());

                //attendance check
               // studentAttendances = studentAttendanceRepository.findStudentAttendance(employeeDetailDTO.getEmployeeId(), employeeDetailDTO.getDateOfRelieving());

                if (!(CollectionUtils.isEmpty(staffCySubject)) || !(CollectionUtils.isEmpty(subjectResources)) || !(CollectionUtils.isEmpty(staffCySubject)) ||
                        !(CollectionUtils.isEmpty(employeeDetailRepoting)) || !(CollectionUtils.isEmpty(studentAttendances))) {

                    /*staffCourseyrSubjectDTOs = staffCourseyrSubjectMapper.convertEntityListToDTOList(staffCySubject);
                    subjectResourcesDTO = subjectResourceMapper.convertEntityListToDTOList(subjectResources);
                    counselorMappingDTOs = counselorMappingMapper.convertEntityListToDTOList(counselorMappings);
                    employeeDetailRepotingDTO = employeeReportingMapper.convertEntityListToDTOList(employeeDetailRepoting);
                    studentAttendanceDTOs = studentAttendanceMapper.convertEntityListToDTOList(studentAttendances);
*/
                    //employeeResignedDTO.setStaffCourseYrSubjectDTOs(staffCourseyrSubjectDTOs);
                    employeeResignedDTO.setSubjectResourceDTOs(subjectResourcesDTO);
                   // employeeResignedDTO.setCounselorMappingDTOs(counselorMappingDTOs);
                    //employeeResignedDTO.setStudentAttendanceDTOs(studentAttendanceDTOs);
                    employeeResignedDTO.setEmployeeReportingDTOs(employeeDetailRepotingDTO);

                    if (employeeResignedDTO != null) {
                        return response = new ApiResponse<>(Boolean.FALSE, "Staff is assigned to Subject",
                                employeeResignedDTO, HttpStatus.OK.value());
                    }
                }
            }

            EmployeeDetail employeeDetail = employeeDetailMapper.convertDTOtoEntity(employeeDetailDTO, empDtl);
            employeeDetail = employeeDetailRepository.save(employeeDetail);
            employeeDetailRepository.flush();

            if (employeeDetail != null && employeeDetail.getEmployeeId() != 0) {
                EmployeeDetailDTO applicationDTO = new EmployeeDetailDTO();
                applicationDTO.setEmployeeId(employeeDetail.getEmployeeId());
                applicationDTO.setEmpNumber(employeeDetail.getEmpNumber());

                if (null != employeeDetail.getEmpDocumentCollections1()) {
                    List<EmployeeDocumentCollection> employeeDocumentCollections = employeeDetail
                            .getEmpDocumentCollections1();
                    List<EmployeeDocumentCollectionDTO> employeeDocumentCollectionDTOs = employeeDocumentCollectionMapper
                            .convertEntityListToDTOList(employeeDocumentCollections);
                    applicationDTO.setEmployeeDocumentCollection(employeeDocumentCollectionDTOs);
                }
                if (null != employeeDetail.getEmpEmployeeEducations()) {
                    List<EmployeeEducation> employeeEducations = employeeDetail.getEmpEmployeeEducations();
                    List<EmployeeEducationDTO> employeeEducationDTOs = employeeEducationMapper
                            .convertEntityListToDTOList(employeeEducations);
                    applicationDTO.setEmployeeEducations(employeeEducationDTOs);
                }
                if (null != employeeDetail.getEmpEmployeeBankDetails()) {
                    List<EmployeeBankDetail> employeeBankDetails = employeeDetail.getEmpEmployeeBankDetails();
                    List<EmployeeBankDetailDTO> employeeBankDetailDTOs = employeeBankDetailMapper
                            .convertEntityListToDTOList(employeeBankDetails);
                    applicationDTO.setEmployeeBankDetails(employeeBankDetailDTOs);
                }
                if (null != employeeDetail.getEmpEmployeeReportings1()) {
                    List<EmployeeReporting> employeeReportings = employeeDetail.getEmpEmployeeReportings1();
                    List<EmployeeReportingDTO> employeeReportingDTOs = employeeReportingMapper
                            .convertEntityListToDTOList(employeeReportings);
                    applicationDTO.setEmployeeReportings(employeeReportingDTOs);
                }
                if (null != employeeDetail.getEmpExperienceDetails()) {
                    List<EmployeeExperienceDetail> employeeExperienceDetails = employeeDetail
                            .getEmpExperienceDetails();
                    List<EmployeeExperienceDetailDTO> employeeExperienceDetailDTOs = employeeExperienceDetailMapper
                            .convertEntityListToDTOList(employeeExperienceDetails);
                    applicationDTO.setEmpExperienceDetails(employeeExperienceDetailDTOs);
                }

                response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), applicationDTO,
                        HttpStatus.OK.value());
            } else {
                response = new ApiResponse<>(Boolean.FALSE, Messages.UPDATE_FAILURE.getValue(), HttpStatus.OK.value());
            }
        } catch (Exception e) {
            LOGGER.error("Exception Occured while updating employeeDetail : " + e);
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }

        return response;

    }

    @Override
    public ApiResponse<?> addEmployeesManager(List<EmployeeReportingDTO> employeeReportingDTOs) {
        LOGGER.info("EmployeeDetailServiceImpl.addEmployeesManager()");
        ApiResponse<?> response = null;
        Integer count = 0;
        try {

            if (!(CollectionUtils.isEmpty(employeeReportingDTOs))) {

                for (EmployeeReportingDTO employeeReportingDTO : employeeReportingDTOs) {

                    response = addEmployeeManager(employeeReportingDTO);

                    if (response != null && response.getSuccess() && response.getData() != null) {

                        count = +1;
                    }
                }

                if (employeeReportingDTOs.size() == count) {

                    response = new ApiResponse<>(Messages.UPDATE_SUCCESS.getValue(), HttpStatus.OK.value());
                } else {
                    response = new ApiResponse<>(Boolean.FALSE, Messages.UPDATE_FAILURE.getValue(), HttpStatus.OK.value());
                }

            }
        } catch (Exception e) {
            LOGGER.error("EmployeeDetailServiceImpl.addEmployeesManager()" + e);
            throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
        }
        return response;
    }
}
