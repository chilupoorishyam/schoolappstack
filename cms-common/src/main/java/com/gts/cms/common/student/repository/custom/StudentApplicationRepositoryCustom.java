package com.gts.cms.common.student.repository.custom;

import com.gts.cms.entity.StudentApplication;

import java.util.Date;
import java.util.List;

public interface StudentApplicationRepositoryCustom {

	List<StudentApplication> findByStudentData(String studentData, Long schoolId);
	Long updateStudentApplicationPhotFileNames(Long applicationId,
			String studentAadharFileName, String studentPancardFileName, String studentPhotoFileName,
			String fatherPhotoFileName, String motherPhotoFileName);
	
	List<StudentApplication> findAllStudentsApplicationForms(Long schoolId,String role,Integer workFlowStep);
	Long updateStudentAdmissionNumber(Long studentAppId, Date adminssionDate, String admissionNumber);

	
	
	List<StudentApplication> findAllStudentsApplicationForms(Long schoolId,Boolean status);

	Long deleteStudentApplicationDetail(Long id);

	Long updateStudentAadharFile(Long applicationId, String studentAadharFileName);

	Long updateStudentPancardFile(Long applicationId, String studentPancardFileName);
	Long updateStudentMotherPhotoFile(Long applicationId, String motherPhotoFileName);

	Long updateStudentFatherPhotoFile(Long applicationId, String fatherPhotoFileName);

	Long updateStudentPhotoFile(Long applicationId, String studentPhotoFileName);


}
