package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.AuthorizationRepositoryCustom;
import com.gts.cms.entity.Authorization;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * created by Naveen(Auto) on 02/09/2018
 * 
 */
@Repository
public interface AuthorizationRepository extends JpaRepository<Authorization, Long>, AuthorizationRepositoryCustom {

	@Query("SELECT a FROM Authorization a"
			+ " WHERE 1=1 "
			+ " AND (:loginTime is null or DATE(a.loginTime) =:loginTime) "
			+ " AND (:logoutTime is null or DATE(a.logoutTime) =:logoutTime) "
			+ " AND (:userId is null or a.user.userId=:userId) "
			+ " AND (:isActive is null or a.isActive =:isActive) "
			+ " ORDER BY a.logoutTime DESC"
			)
	Page<Authorization> fetchAuthDetails(Date loginTime, Date logoutTime, Long userId, Boolean isActive, Pageable pageable);


}
