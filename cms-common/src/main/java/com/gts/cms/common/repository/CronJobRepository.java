package com.gts.cms.common.repository;

import com.gts.cms.entity.CronJob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CronJobRepository extends JpaRepository<CronJob, Long> {
	
	List<CronJob> findAllByCronJobCode(String code);
	CronJob findByCronJobCode(String code);
	
}
