package com.gts.cms.common.repository;

import com.gts.cms.common.student.repository.custom.StudentAppWorkflowRepositoryCustom;
import com.gts.cms.entity.StudentAppWorkflow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentAppWorkflowRepository extends JpaRepository<StudentAppWorkflow, Long>, StudentAppWorkflowRepositoryCustom {

}
