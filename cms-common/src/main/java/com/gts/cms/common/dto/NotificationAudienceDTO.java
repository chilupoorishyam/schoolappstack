package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;


public class NotificationAudienceDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	
	private Long notificationAudienceId;
	
	private Long schoolId;
	
	private Long notificationId;
	
	private Long audienceTypeId;
	
	private String categoryName;
	
	private String categoryValue;
	
	private String reason;

	@NotNull(message="isActive is required")
	private Boolean isActive;

	
	private Date createdDt;

	private Long createdUser;

	private Date updatedDt;

	private Long updatedUser;

	private String schoolName;
	
	private String schoolCode;
	
	private String notificationTitle;
	
	private String audienceTypeCode;

	private String audienceTypeDescription;

	private String audienceTypeDisplayName;

	public Long getNotificationAudienceId() {
		return notificationAudienceId;
	}

	public void setNotificationAudienceId(Long notificationAudienceId) {
		this.notificationAudienceId = notificationAudienceId;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public Long getAudienceTypeId() {
		return audienceTypeId;
	}

	public void setAudienceTypeId(Long audienceTypeId) {
		this.audienceTypeId = audienceTypeId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryValue() {
		return categoryValue;
	}

	public void setCategoryValue(String categoryValue) {
		this.categoryValue = categoryValue;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	
	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getNotificationTitle() {
		return notificationTitle;
	}

	public void setNotificationTitle(String notificationTitle) {
		this.notificationTitle = notificationTitle;
	}

	public String getAudienceTypeCode() {
		return audienceTypeCode;
	}

	public void setAudienceTypeCode(String audienceTypeCode) {
		this.audienceTypeCode = audienceTypeCode;
	}

	public String getAudienceTypeDescription() {
		return audienceTypeDescription;
	}

	public void setAudienceTypeDescription(String audienceTypeDescription) {
		this.audienceTypeDescription = audienceTypeDescription;
	}

	public String getAudienceTypeDisplayName() {
		return audienceTypeDisplayName;
	}

	public void setAudienceTypeDisplayName(String audienceTypeDisplayName) {
		this.audienceTypeDisplayName = audienceTypeDisplayName;
	}
}