package com.gts.cms.common.querydsl.support;

import java.beans.PropertyDescriptor;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import javax.persistence.Entity;

import static com.google.common.base.Predicates.not;
import static com.google.common.base.Predicates.or;
import static org.springframework.beans.BeanUtils.getPropertyDescriptors;

import com.google.common.base.Predicate;

import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

import io.jsonwebtoken.lang.Assert;

public final class EntityDescriptorFactory {
	public static final String INVALID_PATH = "Property path %s is not valid for graph defined by %s";
	private static final String NOT_NULL_MESSAGE = "PropertyDescriptor may not be null";

	private EntityDescriptorFactory() {

	}

	private static Predicate<PropertyDescriptor> collectionPredicate = new Predicate<PropertyDescriptor>() {

		@Override
		public boolean apply(PropertyDescriptor input) {
			if (null == input) {
				throw new IllegalArgumentException(NOT_NULL_MESSAGE);
			}
			return Collection.class.isAssignableFrom(input.getPropertyType());
		}

	};

	private static Predicate<PropertyDescriptor> classPredicate = new Predicate<PropertyDescriptor>() {

		@Override
		public boolean apply(PropertyDescriptor input) {
			if (null == input) {
				throw new IllegalArgumentException(NOT_NULL_MESSAGE);
			}
			return "class".equals(input.getName());
		}
	};

	private static Predicate<PropertyDescriptor> entityPredicate = new Predicate<PropertyDescriptor>() {

		@Override
		public boolean apply(PropertyDescriptor input) {
			if (null == input) {
				throw new IllegalArgumentException(NOT_NULL_MESSAGE);
			}
			return input.getPropertyType().getAnnotation(Entity.class) != null;
		}
	};

	private static class PropertyPredicate implements Predicate<PropertyDescriptor> {
		private final String propertyName;

		public PropertyPredicate(final String propertyName) {
			this.propertyName = propertyName;
		}

		@Override
		public boolean apply(PropertyDescriptor input) {
			return input.getName().equalsIgnoreCase(propertyName);
		}

	}

	private static PropertyDescriptor selectProperty(Collection<PropertyDescriptor> descriptors, String name) {
		Iterable<PropertyDescriptor> result = Iterables.filter(descriptors, new PropertyPredicate(name));
		if (result.iterator().hasNext()) {
			return result.iterator().next();
		}
		return null;

	}

	public static boolean isEntity(PropertyDescriptor descriptor) {
		return entityPredicate.apply(descriptor);
	}

	public static class EntityDescriptor {
		private final Set<PropertyDescriptor> allDescriptors;
		private final Set<PropertyDescriptor> attributeDescriptors;
		private final Set<PropertyDescriptor> collectionDescriptors;
		private final Set<PropertyDescriptor> entityDescriptors;

		public EntityDescriptor(Class<?> clazz) {
			allDescriptors = Sets.filter(Sets.newHashSet(getPropertyDescriptors(clazz)), not(classPredicate));
			collectionDescriptors = Sets.filter(allDescriptors, collectionPredicate);
			entityDescriptors = Sets.filter(allDescriptors, entityPredicate);
			attributeDescriptors = Sets.filter(allDescriptors, not(or(collectionPredicate, entityPredicate)));
		}

		public Set<PropertyDescriptor> getAllDescriptors() {
			return allDescriptors;
		}

		public Set<PropertyDescriptor> getAttributeDescriptors() {
			return attributeDescriptors;
		}

		public Set<PropertyDescriptor> getCollectionDescriptors() {
			return collectionDescriptors;
		}

		public Set<PropertyDescriptor> getEntityDescriptors() {
			return entityDescriptors;
		}

		public PropertyDescriptor getProperty(final String propertyName) {
			return selectProperty(getAllDescriptors(), propertyName);
		}

		public boolean isCollectiondescriptor(PropertyDescriptor propertyDescriptor) {
			return getCollectionDescriptors().contains(propertyDescriptor);
		}

		public boolean isEntityDesriptor(PropertyDescriptor propertyDescriptor) {
			return getEntityDescriptors().contains(propertyDescriptor);
		}

		public EntityDescriptor describe(PropertyDescriptor descriptor) {
			return EntityDescriptorFactory.describe(getType(descriptor));
		}

	}

	public static void validatePath(Class<?> klass, List<String> path) {
		Assert.isTrue(!path.isEmpty(), "Path to property is empty");
		EntityDescriptor descriptor = EntityDescriptorFactory.describe(klass);

		PropertyDescriptor propertyDescriptor = descriptor.getProperty(path.get(0));
		Assert.notNull(propertyDescriptor, String.format(INVALID_PATH, path, klass));
		if (path.size() > 1) {
			validatePath(EntityDescriptorFactory.getType(propertyDescriptor), path.subList(1, path.size()));
		}
	}

	public static Class<?> getType(PropertyDescriptor descriptor) {
		if (collectionPredicate.apply(descriptor)) {
			ParameterizedType type = (ParameterizedType) descriptor.getReadMethod().getGenericReturnType();
			return (Class<?>) type.getActualTypeArguments()[0];
		}
		return descriptor.getPropertyType();
	}

	public static Object getValue(Object object, PropertyDescriptor descriptor) {
		try {
			return descriptor.getReadMethod().invoke(object, new Object[0]);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static EntityDescriptor describe(Class<?> klass) {
		return new EntityDescriptor(klass);
	}
}
