package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.CityDTO;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.City;
import com.gts.cms.entity.District;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class CityMapper implements BaseMapper<City, CityDTO>{
	@Override
	public CityDTO convertEntityToDTO(City city) {
		CityDTO cityDTO = null;
		if (city != null) {
			cityDTO = new CityDTO();
			cityDTO.setCityId(city.getCityId());
			cityDTO.setCityName(city.getCityName());
			cityDTO.setCityCode(city.getCityCode());
			if (city.getDistrict() != null) {
				cityDTO.setDistrictId(city.getDistrict().getDistrictId());
			}
		}
		return cityDTO;
	}
	@Override
	public City convertDTOtoEntity(CityDTO cityDTO,City city) {
		if (null == city) {
			city = new City();
			city.setIsActive(Boolean.TRUE);
			city.setCreatedDt(new Date());
			city.setCreatedUser(SecurityUtil.getCurrentUser());
		}
		city.setCityId(cityDTO.getCityId());
		city.setCityName(cityDTO.getCityName());
		city.setCityCode(cityDTO.getCityCode());
		city.setUpdatedUser(SecurityUtil.getCurrentUser());
		city.setIsActive(Status.ACTIVE.getId());
		if (cityDTO.getDistrictId() != null) {
			District district=new District();
			district.setDistrictId(cityDTO.getDistrictId());
			city.setDistrict(district);
		}
		return city;
		
	}
	@Override
	public List<City> convertDTOListToEntityList(List<CityDTO> cityDTOList) {
		List<City> cityList = new ArrayList<>();
		cityDTOList.forEach(cityDTO -> cityList.add(convertDTOtoEntity(cityDTO,null)));
		return cityList;

	}
	@Override
	public List<CityDTO> convertEntityListToDTOList(List<City> cityList) {
		List<CityDTO> cityDTOList = new ArrayList<>();
		cityList.forEach(city -> cityDTOList.add(convertEntityToDTO(city)));
		return cityDTOList;
	}
	
}
