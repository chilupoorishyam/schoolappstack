package com.gts.cms.common.enums;

public enum ElectiveGroupYrMappingEnum {

	ELECTIVEGROUPYRMAPPING_SUCCESS("ElectiveGroupYrMapping added successfully!"),
	FAILURE_MSG("ElectiveGroupYrMapping(s) not added!");
	private String value;

	private ElectiveGroupYrMappingEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
