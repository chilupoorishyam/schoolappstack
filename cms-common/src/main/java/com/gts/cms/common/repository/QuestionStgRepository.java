package com.gts.cms.common.repository;

import com.gts.cms.entity.QuestionStg;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionStgRepository extends JpaRepository<QuestionStg, Long> {
    @Query("SELECT r FROM QuestionStg r"
            + " WHERE 1=1"
            + " AND(r.question = :question) "
    )
    List<QuestionStg> isQuestionAvailable(@Param("question") String question);
}
