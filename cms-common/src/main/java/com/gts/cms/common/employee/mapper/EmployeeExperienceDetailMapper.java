package com.gts.cms.common.employee.mapper;

import com.gts.cms.common.employee.dto.EmployeeExperienceDetailDTO;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.util.OptionalUtil;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.EmployeeDetail;
import com.gts.cms.entity.EmployeeExperienceDetail;
import com.gts.cms.entity.Organization;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Genesis
 *
 */
@Component
public class EmployeeExperienceDetailMapper
		implements BaseMapper<EmployeeExperienceDetail, EmployeeExperienceDetailDTO> {

	public EmployeeExperienceDetailDTO convertEntityToDTO(EmployeeExperienceDetail employeeExperienceDetail) {
		EmployeeExperienceDetailDTO employeeExperienceDetailDTO = null;
		if (employeeExperienceDetail != null) {
			employeeExperienceDetailDTO = new EmployeeExperienceDetailDTO();

			employeeExperienceDetailDTO
					.setEmpExperiencedetailId(employeeExperienceDetail.getEmployeeExperiencedetailId());
			employeeExperienceDetailDTO.setCreatedDt(employeeExperienceDetail.getCreatedDt());
			employeeExperienceDetailDTO.setCreatedUser(employeeExperienceDetail.getCreatedUser());
			employeeExperienceDetailDTO.setDesignation(employeeExperienceDetail.getDesignation());
			employeeExperienceDetailDTO.setExperienceDetail(employeeExperienceDetail.getExperienceDetail());
			employeeExperienceDetailDTO.setExperienceMonth(employeeExperienceDetail.getExperienceMonth());
			employeeExperienceDetailDTO.setExperienceYear(employeeExperienceDetail.getExperienceYear());
			employeeExperienceDetailDTO.setFromYrMonth(employeeExperienceDetail.getFromYrMonth());
			employeeExperienceDetailDTO.setIsActive(employeeExperienceDetail.getIsActive());
			employeeExperienceDetailDTO.setPrevoiusInstitutions(employeeExperienceDetail.getPrevoiusInstitutions());
			employeeExperienceDetailDTO.setReason(employeeExperienceDetail.getReason());
			employeeExperienceDetailDTO.setSubjects(employeeExperienceDetail.getSubjects());
			employeeExperienceDetailDTO.setToYrMonth(employeeExperienceDetail.getToYrMonth());
			employeeExperienceDetailDTO.setEmpId(employeeExperienceDetail.getEmployeeDetail().getEmployeeId());
			employeeExperienceDetailDTO.setOrganizationId(OptionalUtil
					.resolve(() -> employeeExperienceDetail.getOrganization().getOrganizationId()).orElse(null));

		}
		return employeeExperienceDetailDTO;
	}

	public EmployeeExperienceDetail convertDTOtoEntity(EmployeeExperienceDetailDTO employeeExperienceDetailDTO,
			EmployeeExperienceDetail employeeExperienceDetail) {
		if (employeeExperienceDetailDTO != null) {
			if (employeeExperienceDetail == null) {
				employeeExperienceDetail = new EmployeeExperienceDetail();
				employeeExperienceDetail.setIsActive(Status.INACTIVE.getId());
				employeeExperienceDetail.setCreatedDt(new Date());
				employeeExperienceDetail.setCreatedUser(SecurityUtil.getCurrentUser());
			}else {
				employeeExperienceDetail.setIsActive(employeeExperienceDetailDTO.getIsActive());
			}

			employeeExperienceDetail
					.setEmployeeExperiencedetailId(employeeExperienceDetailDTO.getEmpExperiencedetailId());
			employeeExperienceDetail.setDesignation(employeeExperienceDetailDTO.getDesignation());
			employeeExperienceDetail.setExperienceDetail(employeeExperienceDetailDTO.getExperienceDetail());
			employeeExperienceDetail.setExperienceMonth(employeeExperienceDetailDTO.getExperienceMonth());
			employeeExperienceDetail.setExperienceYear(employeeExperienceDetailDTO.getExperienceYear());
			employeeExperienceDetail.setFromYrMonth(employeeExperienceDetailDTO.getFromYrMonth());
			
			employeeExperienceDetail.setPrevoiusInstitutions(employeeExperienceDetailDTO.getPrevoiusInstitutions());
			employeeExperienceDetail.setReason(employeeExperienceDetailDTO.getReason());
			employeeExperienceDetail.setSubjects(employeeExperienceDetailDTO.getSubjects());
			employeeExperienceDetail.setToYrMonth(employeeExperienceDetailDTO.getToYrMonth());

			employeeExperienceDetail.setUpdatedDt(new Date());
			employeeExperienceDetail.setUpdatedUser(SecurityUtil.getCurrentUser());

			EmployeeDetail employeeDetail = new EmployeeDetail();
			employeeDetail.setEmployeeId(employeeExperienceDetailDTO.getEmpId());
			employeeExperienceDetail.setEmployeeDetail(employeeDetail);

			Organization organization = new Organization();
			organization.setOrganizationId(employeeExperienceDetailDTO.getOrganizationId());
			employeeExperienceDetail.setOrganization(organization);

		}
		return employeeExperienceDetail;
	}

	public List<EmployeeExperienceDetail> convertDTOListToEntityList(
			List<EmployeeExperienceDetailDTO> employeeExperienceDetailDTOList) {
		List<EmployeeExperienceDetail> employeeExperienceDetailList = new ArrayList<>();
		if (employeeExperienceDetailDTOList != null) {
			employeeExperienceDetailDTOList.forEach(employeeExperienceDetail -> employeeExperienceDetailList
					.add(convertDTOtoEntity(employeeExperienceDetail, null)));
		}
		return employeeExperienceDetailList;

	}

	public List<EmployeeExperienceDetailDTO> convertEntityListToDTOList(
			List<EmployeeExperienceDetail> employeeExperienceDetailList) {
		List<EmployeeExperienceDetailDTO> employeeExperienceDetailDTOList = new ArrayList<>();
		employeeExperienceDetailList.forEach(employeeExperienceDetail -> employeeExperienceDetailDTOList
				.add(convertEntityToDTO(employeeExperienceDetail)));
		return employeeExperienceDetailDTOList;
	}

}
