package com.gts.cms.common.querydsl.support;

public class QDslFilterPredicate {
	public static final String LIST_SEPARATOR = ",";

	public enum Junction implements FilterOperator {
		AND(".and."), OR(".or.");
		private String value;
		private String regex;

		Junction(String value) {
			this.value = value;
			regex = escape(value);
		}

		public String value() {
			return value;
		}

		public String regex() {
			return regex;
		}

		public static Junction get(String text) {
			if (text == null) {
				return null;
			}
			for (Junction junction : Junction.values()) {
				if (junction.value.equalsIgnoreCase(text)) {
					return junction;
				}
			}
			return null;
		}

		@Override
		public String toString() {
			return value();
		}

	}

	public enum Operator implements FilterOperator {
		EQUAL("=="), LIKE("~"), GREATER(".gt."), LESS(".lt."), BETWEEN(".between."), GREATEROREQUAL(".gte."),
		LESSOREQUAL(".lte."), NOTEQUAL("!="), IN(".in."), STARTSWITH(".startswith."), ENDSWITH(".endswith."),
		CONTAINS(".contains.");
		private String value;
		private String regex;

		Operator(String value) {
			this.value = value;
			regex = escape(value);
		}

		public String value() {
			return value;
		}

		public String regex() {
			return regex;
		}

		public static Operator get(String text) {
			if (text == null) {
				return null;
			}
			for (Operator operatorType : Operator.values()) {
				if (operatorType.value.equalsIgnoreCase(text)) {
					return operatorType;
				}
			}
			return null;
		}

		@Override
		public String toString() {
			return value();
		}

	}

	public interface FilterOperator {
		String value();

		String regex();
	}

	private static String escape(final String value) {
		final String[][] replacementPairs = new String[][] { { "\\.", "\\\\." }, { "\\|", "\\\\|" }, { "\\%", "\\\\%" },
				{ "\\^", "\\\\^" }, { "\\-", "\\\\-" }, { "\\[", "\\\\[" }, { "\\]", "\\\\]" }, { "\\{", "\\\\{" },
				{ "\\}", "\\\\}" }, { "\\$", "\\\\$" }, { "\\?", "\\\\?" },

		};
		String result = value;
		for (final String[] replacement : replacementPairs) {
			result = result.replaceAll(replacement[0], replacement[1]);
		}
		return result;
	}
}
