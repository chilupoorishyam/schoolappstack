package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

public class StudentAppWorkflowDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private String comments;

	private Date createdDt;

	private Long createdUser;

	private Boolean isActive;

	private Boolean isCurrentStatus;

	private String reason;

	private Date updatedDt;
	private Long updatedUser;

	private Long currentWorkflowStatusId;
	private String currentWorkflowName;

	private Long toAppWorkflowStatusId;
	private String toAppWorkflowName;
	private Long studentAppWorkflowId;
	private Long studentAppId;
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Date getCreatedDt() {
		return createdDt;
	}
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	public Long getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public Boolean getIsCurrentStatus() {
		return isCurrentStatus;
	}
	public void setIsCurrentStatus(Boolean isCurrentStatus) {
		this.isCurrentStatus = isCurrentStatus;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Date getUpdatedDt() {
		return updatedDt;
	}
	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}
	public Long getUpdatedUser() {
		return updatedUser;
	}
	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}
	public Long getCurrentWorkflowStatusId() {
		return currentWorkflowStatusId;
	}
	public void setCurrentWorkflowStatusId(Long currentWorkflowStatusId) {
		this.currentWorkflowStatusId = currentWorkflowStatusId;
	}
	public String getCurrentWorkflowName() {
		return currentWorkflowName;
	}
	public void setCurrentWorkflowName(String currentWorkflowName) {
		this.currentWorkflowName = currentWorkflowName;
	}
	public Long getToAppWorkflowStatusId() {
		return toAppWorkflowStatusId;
	}
	public void setToAppWorkflowStatusId(Long toAppWorkflowStatusId) {
		this.toAppWorkflowStatusId = toAppWorkflowStatusId;
	}
	public String getToAppWorkflowName() {
		return toAppWorkflowName;
	}
	public void setToAppWorkflowName(String toAppWorkflowName) {
		this.toAppWorkflowName = toAppWorkflowName;
	}
	public Long getStudentAppWorkflowId() {
		return studentAppWorkflowId;
	}
	public void setStudentAppWorkflowId(Long studentAppWorkflowId) {
		this.studentAppWorkflowId = studentAppWorkflowId;
	}
	public Long getStudentAppId() {
		return studentAppId;
	}
	public void setStudentAppId(Long studentAppId) {
		this.studentAppId = studentAppId;
	}
	
	
}
