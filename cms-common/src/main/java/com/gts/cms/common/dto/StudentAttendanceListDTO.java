package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class StudentAttendanceListDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Long studentId;
	private String admissionNumber;
	private String applicationNo;
	private Date dateOfBirth;
	private String firstName;
	private String hallticketNumber;
	private Boolean isActive;
	private String lastName;
	private String middleName;
	private String mobile;
	private String rfid;
	private String rollNumber;
	private Long studentAppId;
	private String studentEmailId;
	private Long groupSectionId;
	private Long batchId;
	private Long academicYearId;
	
	//private List<StudentAttendanceDTO> studentAttendances;

	public Long getStudentId() {
		return studentId;
	}

	public String getAdmissionNumber() {
		return admissionNumber;
	}

	public String getApplicationNo() {
		return applicationNo;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getHallticketNumber() {
		return hallticketNumber;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public String getLastName() {
		return lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public String getMobile() {
		return mobile;
	}

	public String getRfid() {
		return rfid;
	}

	public String getRollNumber() {
		return rollNumber;
	}

	public Long getStudentAppId() {
		return studentAppId;
	}

	public String getStudentEmailId() {
		return studentEmailId;
	}

	public Long getGroupSectionId() {
		return groupSectionId;
	}




	public Long getBatchId() {
		return batchId;
	}

	public Long getAcademicYearId() {
		return academicYearId;
	}

	/*public List<StudentAttendanceDTO> getStudentAttendances() {
		return studentAttendances;
	}*/

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public void setAdmissionNumber(String admissionNumber) {
		this.admissionNumber = admissionNumber;
	}

	public void setApplicationNo(String applicationNo) {
		this.applicationNo = applicationNo;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setHallticketNumber(String hallticketNumber) {
		this.hallticketNumber = hallticketNumber;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public void setRfid(String rfid) {
		this.rfid = rfid;
	}

	public void setRollNumber(String rollNumber) {
		this.rollNumber = rollNumber;
	}

	public void setStudentAppId(Long studentAppId) {
		this.studentAppId = studentAppId;
	}

	public void setStudentEmailId(String studentEmailId) {
		this.studentEmailId = studentEmailId;
	}

	public void setGroupSectionId(Long groupSectionId) {
		this.groupSectionId = groupSectionId;
	}


	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}
/*

	public void setStudentAttendances(List<StudentAttendanceDTO> studentAttendances) {
		this.studentAttendances = studentAttendances;
	}
*/

	
}