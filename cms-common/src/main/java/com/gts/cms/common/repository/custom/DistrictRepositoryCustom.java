package com.gts.cms.common.repository.custom;

import com.gts.cms.entity.District;

import java.util.List;

/**
 * created by Naveen on 04/09/2018
 * 
 */
public interface DistrictRepositoryCustom {

	List<District> findAllByStateId(Long stateId);

}
