package com.gts.cms.common.employee.dto;

import com.gts.cms.common.dto.ApiResponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class EmployeeDetailUploadDTO  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long employeeId;
	private String empNumber;
	private String orgCode;
	private String schoolCode;

	//private MultipartFile aadharFile;
	private String aadharFileName;
	private String aadharFileStatus;

	//private MultipartFile pancardFile;
	private String pancardFileName;
	private String pancardFileStatus;

	//private MultipartFile passportFile;
	private String passportFileName;
	private String passportFileStatus;

	//private MultipartFile photoFile;
	private String photoFileName;
	private String photoFileStatus;

	//private MultipartFile voterIdFile;
	private String voterIdFileName;
	private String voterIdFileStatus;
	
	private String allEmpDocumentsStatus;
	
	private String empDocumentsStatus;
	private Long documentRepositoryId;
	
	private Map<String,MultipartFile> files;
	
	List<ApiResponse<Object>> fileStaus;
	
	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmpNumber() {
		return empNumber;
	}

	public void setEmpNumber(String empNumber) {
		this.empNumber = empNumber;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getAadharFileName() {
		return aadharFileName;
	}

	public void setAadharFileName(String aadharFileName) {
		this.aadharFileName = aadharFileName;
	}

	public String getAadharFileStatus() {
		return aadharFileStatus;
	}

	public void setAadharFileStatus(String aadharFileStatus) {
		this.aadharFileStatus = aadharFileStatus;
	}

	public String getPancardFileName() {
		return pancardFileName;
	}

	public void setPancardFileName(String pancardFileName) {
		this.pancardFileName = pancardFileName;
	}

	public String getPancardFileStatus() {
		return pancardFileStatus;
	}

	public void setPancardFileStatus(String pancardFileStatus) {
		this.pancardFileStatus = pancardFileStatus;
	}

	public String getPassportFileName() {
		return passportFileName;
	}

	public void setPassportFileName(String passportFileName) {
		this.passportFileName = passportFileName;
	}

	public String getPassportFileStatus() {
		return passportFileStatus;
	}

	public void setPassportFileStatus(String passportFileStatus) {
		this.passportFileStatus = passportFileStatus;
	}

	public String getPhotoFileName() {
		return photoFileName;
	}

	public void setPhotoFileName(String photoFileName) {
		this.photoFileName = photoFileName;
	}

	public String getPhotoFileStatus() {
		return photoFileStatus;
	}

	public void setPhotoFileStatus(String photoFileStatus) {
		this.photoFileStatus = photoFileStatus;
	}

	public String getVoterIdFileName() {
		return voterIdFileName;
	}

	public void setVoterIdFileName(String voterIdFileName) {
		this.voterIdFileName = voterIdFileName;
	}

	public String getVoterIdFileStatus() {
		return voterIdFileStatus;
	}

	public void setVoterIdFileStatus(String voterIdFileStatus) {
		this.voterIdFileStatus = voterIdFileStatus;
	}

	public String getAllEmpDocumentsStatus() {
		return allEmpDocumentsStatus;
	}

	public void setAllEmpDocumentsStatus(String allEmpDocumentsStatus) {
		this.allEmpDocumentsStatus = allEmpDocumentsStatus;
	}

	public String getEmpDocumentsStatus() {
		return empDocumentsStatus;
	}

	public void setEmpDocumentsStatus(String empDocumentsStatus) {
		this.empDocumentsStatus = empDocumentsStatus;
	}

	public Long getDocumentRepositoryId() {
		return documentRepositoryId;
	}

	public void setDocumentRepositoryId(Long documentRepositoryId) {
		this.documentRepositoryId = documentRepositoryId;
	}

	public Map<String, MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(Map<String, MultipartFile> files) {
		this.files = files;
	}

	public List<ApiResponse<Object>> getFileStaus() {
		return fileStaus;
	}

	public void setFileStaus(List<ApiResponse<Object>> fileStaus) {
		this.fileStaus = fileStaus;
	}

}
