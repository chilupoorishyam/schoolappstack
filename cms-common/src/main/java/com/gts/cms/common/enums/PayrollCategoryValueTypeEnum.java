package com.gts.cms.common.enums;

public enum PayrollCategoryValueTypeEnum {
	NUMERIC("N"),
	FORMULA("F"),
	CONDITIONWITHFORMULA("C"); 
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	private PayrollCategoryValueTypeEnum(String value) {
		this.value = value;
	}

}
