package com.gts.cms.common.student.service.impl;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.StudentAcademicbatchDTO;
import com.gts.cms.common.dto.StudentDetailListDTO;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.repository.CourseYearRepository;
import com.gts.cms.common.service.report.CommonUtils;
import com.gts.cms.common.student.dto.PromotionDTO;
import com.gts.cms.common.student.repository.StudentAcademicBatchesRepository;
import com.gts.cms.common.student.repository.StudentDetailRepository;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Service
public class PromoteStudent {

	/*@Autowired
	StudentAcademicbatchMapper studentAcademicbatchMapper;*/

	@Autowired
	StudentAcademicBatchesRepository studentAcademicBatchesRepository;

	@Autowired
	StudentDetailRepository studentDetailRepository;

	/*@Autowired
	FeeStudentDataService feeStudentDataService;*/

	@Autowired
	CourseYearRepository courseYearRepository;

	/*@Autowired
	BatchwiseStudentRepository batchwiseStudentRepository;

	@Autowired
	StudentAttendanceRepository studentAttendanceRepository;

	@Autowired
	StudentAttendanceMapper studentAttendanceMapper;*/
	

	private static final Logger LOGGER = Logger.getLogger(PromoteStudent.class);

	// @Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ApiResponse<?> promoteStudent(PromotionDTO promotionDTO) {
		LOGGER.info("PromoteStudent.promoteStudent()");
		ApiResponse<?> response = null;
		List<StudentAttendance> studentAttendanceList = null;
		List<StudentAttendance> studentAttendanceListofList = new ArrayList<>();
		try {
			List<StudentDetailListDTO> students = promotionDTO.getStudentIdsList();
			List<StudentAcademicbatch> studentAcademicbatchList = new ArrayList<>();
			List<StudentAcademicbatch> studentAcademicbatchs=null;
			List<StudentAcademicbatch> stuAcademicbatchsExist=new ArrayList<>();
			//StudentAttendanceAndDates studentAttendanceAndDates=new StudentAttendanceAndDates();
			List<StudentAcademicbatchDTO> studentAcademicbatchDTOs=null;
			List<StudentAcademicbatch> stdAcadBatchSave=null;
			List<Long> studentsId = new ArrayList<>();
			//List<StudentAttendanceDTO> studentAttendanceDTOs=null;
			StudentAcademicbatch studentAcademicbatch=null;
			Integer value=null;
			for (StudentDetailListDTO studentDto : students) {
				//attendance check
				//studentAttendanceList = studentAttendanceRepository.findByAbsentList(promotionDTO.getToDate(),studentDto.getStudentId());
				Date date=CommonUtils.getLessThanOneDate(promotionDTO.getFromDate());
				//fromDate and toDate exist 
				studentAcademicbatchs= studentAcademicBatchesRepository.findAlreadyExistWithDate(studentDto.getStudentId(),date);
				//old record
				studentAcademicbatch = studentAcademicBatchesRepository.getDetails(studentDto.getSchoolId(), studentDto.getStudentId());
				if(studentAcademicbatch!=null) {
			    value=CommonUtils.compareDate(studentAcademicbatch.getFromDate(),date);
				}
				if (!CollectionUtils.isEmpty(studentAttendanceList)) {
					studentAttendanceListofList.addAll(studentAttendanceList);
				}
				if(!(CollectionUtils.isEmpty(studentAcademicbatchs))) {
					stuAcademicbatchsExist.addAll(studentAcademicbatchs);
				}
				if(value!=null && value==-1) {
					stuAcademicbatchsExist.add(studentAcademicbatch);
				}
				
				if (value!=null && studentAttendanceList.isEmpty() && studentAcademicbatchs.isEmpty() && 
						studentAcademicbatch!=null && ((value==0)||(value==1))) {
					//new record
					StudentAcademicbatch studentAcademicBatch = new StudentAcademicbatch();
					
					if(promotionDTO.getSchoolId()!=null) {
					School school = new School();
					school.setSchoolId(promotionDTO.getSchoolId());
					studentAcademicBatch.setSchool(school);
					}
				
					if(studentDto.getStudentId()!=null) {
					StudentDetail student = new StudentDetail();
					student.setStudentId(studentDto.getStudentId());
					studentAcademicBatch.setStudentDetail(student);
					}
					
					if(promotionDTO.getCourseId()!=null) {
					Course course = new Course();
					course.setCourseId(promotionDTO.getCourseId());
					studentAcademicBatch.setCourse(course);
					}
					
					if (promotionDTO.getAcademicYearId() != null) {
						AcademicYear academicYear = new AcademicYear();
						academicYear.setAcademicYearId(promotionDTO.getAcademicYearId());
						studentAcademicBatch.setAcademicYear(academicYear);
					}

					if (promotionDTO.getToCourseYearId() != null) {
						CourseYear courseYear = new CourseYear();
						courseYear.setCourseYearId(promotionDTO.getToCourseYearId());
						studentAcademicBatch.setFromCourseYear(courseYear);
					}

					if (promotionDTO.getToBatchId() != null && studentDto.getBatchId()!=null) {
						Batch batch = new Batch();
						batch.setBatchId(studentDto.getBatchId());
						studentAcademicBatch.setFromBatch(batch);
					}
					
					if (promotionDTO.getToBatchId() != null && studentDto.getBatchId()!=null) {
						Batch batch = new Batch();
						batch.setBatchId(studentDto.getBatchId());
						studentAcademicBatch.setToBatch(batch);
					}
					if (promotionDTO.getToGroupSectionId() != null) {
						GroupSection groupSection = new GroupSection();
						groupSection.setGroupSectionId(promotionDTO.getToGroupSectionId());
						studentAcademicBatch.setFromGroupSection(groupSection);
					}

					studentAcademicBatch.setIsActive(Boolean.TRUE);
					Date fromDate=null;
					if(promotionDTO.getToDate()!=null) {
					fromDate=CommonUtils.getDateAndTime(promotionDTO.getToDate());
					}
					LOGGER.info("StudentAcademicBatch ==>new record==>fromDate==>"+fromDate);
					studentAcademicBatch.setFromDate(fromDate);
					studentAcademicBatch.setCreatedDt(new Date());
					studentAcademicBatch.setIsPromoted(Boolean.TRUE);
					studentAcademicBatch.setReason("Promotion");
					if (studentDto.getStudentStatusId() != null) {
						GeneralDetail studentStatus = new GeneralDetail();
						studentStatus.setGeneralDetailId(studentDto.getStudentStatusId());
						studentAcademicBatch.setStudentStatus(studentStatus);
					}


					studentAcademicBatch.setCreatedDt(new Date());
					studentAcademicBatch.setCreatedUser(SecurityUtil.getCurrentUser());
					studentAcademicbatchList.add(studentAcademicBatch);

					studentsId.add(studentDto.getStudentId());
				}

			}
			
			Date date=CommonUtils.getLessThanOneDate(promotionDTO.getToDate());

			if (!studentsId.isEmpty()) {
				studentDetailRepository.updateSection(studentsId, promotionDTO.getToGroupSectionId(),
						promotionDTO.getToCourseYearId(), promotionDTO.getAcademicYearId(), new Date(),
						SecurityUtil.getCurrentUser());
				studentAcademicBatchesRepository.updatePreviouSection(studentsId, promotionDTO.getToCourseYearId(),
						promotionDTO.getToGroupSectionId(), date,new Date(),SecurityUtil.getCurrentUser());
				/*Integer updateCount = batchwiseStudentRepository.updateBatchwiseStudents(studentsId,
						promotionDTO.getFromGroupSectionId(), new Date(), SecurityUtil.getCurrentUser());
				LOGGER.debug("Student batches updated count : " + updateCount);*/
			}

			if (!studentAcademicbatchList.isEmpty()) {
				stdAcadBatchSave =studentAcademicBatchesRepository.saveAll(studentAcademicbatchList);
				studentAcademicBatchesRepository.flush();
			}
			
			/*studentAttendanceDTOs=studentAttendanceMapper.convertEntityListToDTOList(studentAttendanceListofList);
			studentAcademicbatchDTOs=studentAcademicbatchMapper.convertEntityListToDTOList(stuAcademicbatchsExist);
			studentAttendanceAndDates.setStudenAttendanceDTOs(studentAttendanceDTOs);
			studentAttendanceAndDates.setStudentAcademicbatchDTOs(studentAcademicbatchDTOs);*/
			
			if(!CollectionUtils.isEmpty(stdAcadBatchSave)) {
				response = new ApiResponse<>(Boolean.TRUE, Messages.UPDATE_SUCCESS.getValue(),HttpStatus.OK.value());
			}
			else {
				//response = new ApiResponse<>(Boolean.FALSE,Messages.UPDATE_FAILURE.getValue(),studentAttendanceAndDates,HttpStatus.OK.value());
			}
			LOGGER.info("PromoteStudent.promoteStudent()===>executed");
		} catch (Exception e) {
			LOGGER.error("PromoteStudent.promoteStudent()"+e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}

		return response;
	}
}
