package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.UserRoleRepositoryCustom;
import com.gts.cms.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * created by Naveen(Auto) on 02/09/2018
 * 
 */
@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long>, UserRoleRepositoryCustom {

    @Query("SELECT r FROM  UserRole r"
            + " WHERE r.role.roleId = :roleId"
    )
    List<UserRole> findUserRoles(@Param("roleId") Long roleId);

    @Query("SELECT r FROM  UserRole r"
            + " WHERE r.user.userId = :userId"
            + " AND r.isActive = true"
    )
    List<UserRole> findUserRolesByUserId(@Param("userId") Long userId);
}
