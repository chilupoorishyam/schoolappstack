package com.gts.cms.common.dto;

import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

public class SubjectUnitDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long subjectUnitsId;
    private Date createdDt;
    private Long createdUser;
    private Boolean isActive;
    private String reason;
    private String unitCode;
    private String unitDescription;
    private String unitName;
    private Date updatedDt;
    private Long updatedUser;

    @NotNull(message = "School Id is required")
    private Long schoolId;
    private String schoolCode;
    private String schoolName;
    private Integer noOfPeriods;
    private Map<String, MultipartFile> files;

    private List<SubjectUnitTopicDTO> subjectUnitTopicsDTOs;

    public void setSubjectUnitsId(Long subjectUnitsId) {
        this.subjectUnitsId = subjectUnitsId;
    }

    public Long getSubjectUnitsId() {
        return subjectUnitsId;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    public Date getCreatedDt() {
        return createdDt;
    }

    public void setCreatedUser(Long createdUser) {
        this.createdUser = createdUser;
    }

    public Long getCreatedUser() {
        return createdUser;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

    public void setUnitCode(String unitCode) {
        this.unitCode = unitCode;
    }

    public String getUnitCode() {
        return unitCode;
    }

    public void setUnitDescription(String unitDescription) {
        this.unitDescription = unitDescription;
    }

    public String getUnitDescription() {
        return unitDescription;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUpdatedDt(Date updatedDt) {
        this.updatedDt = updatedDt;
    }

    public Date getUpdatedDt() {
        return updatedDt;
    }

    public void setUpdatedUser(Long updatedUser) {
        this.updatedUser = updatedUser;
    }

    public Long getUpdatedUser() {
        return updatedUser;
    }


    public void setSchoolId(Long schoolId) {
        this.schoolId = schoolId;
    }

    public Long getSchoolId() {
        return schoolId;
    }

    public void setSchoolCode(String schoolCode) {
        this.schoolCode = schoolCode;
    }

    public String getSchoolCode() {
        return schoolCode;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public List<SubjectUnitTopicDTO> getSubjectUnitTopicsDTOs() {
        return subjectUnitTopicsDTOs;
    }

    public void setSubjectUnitTopicsDTOs(List<SubjectUnitTopicDTO> subjectUnitTopicsDTOs) {
        this.subjectUnitTopicsDTOs = subjectUnitTopicsDTOs;
    }

    public Integer getNoOfPeriods() {
        return noOfPeriods;
    }

    public void setNoOfPeriods(Integer noOfPeriods) {
        this.noOfPeriods = noOfPeriods;
    }

    public Map<String, MultipartFile> getFiles() {
        return files;
    }

    public void setFiles(Map<String, MultipartFile> files) {
        this.files = files;
    }
}