package com.gts.cms.common.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class StudentCategoryDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long studentCatId;

	@NotNull(message = "Organization Id is required.")
	private Long organizationId;

	private String orgName;
	private String orgCode;

	private String studentCategory;

	private Integer sortOrder;

	private String reason;
	@NotNull(message = "IsActive is required.")
	private Boolean isActive;

	public Long getStudentCatId() {
		return studentCatId;
	}

	public void setStudentCatId(Long studentCatId) {
		this.studentCatId = studentCatId;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgCode() {
		return this.orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getStudentCategory() {
		return studentCategory;
	}

	public void setStudentCategory(String studentCategory) {
		this.studentCategory = studentCategory;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}
