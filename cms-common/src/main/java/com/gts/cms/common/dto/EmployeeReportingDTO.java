package com.gts.cms.common.dto;

import java.util.Date;

public class EmployeeReportingDTO {

	private static final long serialVersionUID = 1L;

	private Long empReportingId;

	private Date createdDt;

	private Long createdUser;

	private Date fromDate;

	private Date toDate;

	private Long organizationId;
	private String orgCode;
	private String orgName;

	private Long empId;
	private String empName;
	private String empNumber;

	private Long managerEmpId;
	private String managerEmpName;
	private String managerEmpNumber;

	private Long empDesignationId;
	private String desEmpName;
	private String desEmpNumber;

	private Boolean isActive;

	private String reason;

	public Long getEmpReportingId() {
		return empReportingId;
	}

	public void setEmpReportingId(Long empReportingId) {
		this.empReportingId = empReportingId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public Long getEmpId() {
		return empId;
	}

	public void setEmpId(Long empId) {
		this.empId = empId;
	}

	public Long getManagerEmpId() {
		return managerEmpId;
	}

	public void setManagerEmpId(Long managerEmpId) {
		this.managerEmpId = managerEmpId;
	}

	public Long getEmpDesignationId() {
		return empDesignationId;
	}

	public void setEmpDesignationId(Long empDesignationId) {
		this.empDesignationId = empDesignationId;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public String getReason() {
		return reason;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpNumber() {
		return empNumber;
	}

	public void setEmpNumber(String empNumber) {
		this.empNumber = empNumber;
	}

	public String getManagerEmpName() {
		return managerEmpName;
	}

	public void setManagerEmpName(String managerEmpName) {
		this.managerEmpName = managerEmpName;
	}

	public String getManagerEmpNumber() {
		return managerEmpNumber;
	}

	public void setManagerEmpNumber(String managerEmpNumber) {
		this.managerEmpNumber = managerEmpNumber;
	}

	public String getDesEmpName() {
		return desEmpName;
	}

	public void setDesEmpName(String desEmpName) {
		this.desEmpName = desEmpName;
	}

	public String getDesEmpNumber() {
		return desEmpNumber;
	}

	public void setDesEmpNumber(String desEmpNumber) {
		this.desEmpNumber = desEmpNumber;
	}

}
