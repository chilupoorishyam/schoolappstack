package com.gts.cms.common.student.service.impl;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.StudentDetailListDTO;
import com.gts.cms.common.dto.StudentSubjectDTO;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.exception.ApiException;
import com.gts.cms.common.student.mapper.StudentSubjectMapper;
import com.gts.cms.common.student.repository.StudentSubjectRepository;
import com.gts.cms.common.student.service.StudentSubjectService;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Genesis
 *
 */
@Service
public class StudentSubjectServiceImpl implements StudentSubjectService {

	private static final Logger LOGGER = Logger.getLogger(StudentSubjectServiceImpl.class);

	@Autowired
    StudentSubjectRepository studentSubjectRepository;

	/*@Autowired
	SubjectCourseyrServiceImpl subjectCourseyrServiceImpl;
	
	@Autowired
	private SubjectCourseyearMapper subjectCourseyearMapper;*/
	
	@Autowired
	private StudentSubjectMapper studentSubjectMapper;
	/*@Autowired
	ExamSubjectStudentsMapper examSubjectStudentsMapper;*/



	public ApiResponse<Long> setStudentSubjects(List<StudentDetail> studentDetailList,
			List<Subjectregulation> subjectregulationList) {
		ApiResponse<Long> response = null;
		try {

			List<StudentSubject> studentSubjects = new ArrayList<>();
			for (StudentDetail studentDetail : studentDetailList) {
				for (Subjectregulation subjectregulation : subjectregulationList) {
					StudentSubject studentSubject = new StudentSubject();
					studentSubject.setCreatedDt(new Date());
					studentSubject.setCreatedUser(SecurityUtil.getCurrentUser());
					studentSubject.setIsActive(Boolean.TRUE);
					studentSubject.setSchool(studentDetail.getSchool());
					studentSubject.setStudentDetail(studentDetail);
					studentSubject.setAcademicYear(studentDetail.getAcademicYear());
					studentSubject.setCourse(studentDetail.getCourse());
					studentSubject.setCourseYear(studentDetail.getCourseYear());
					//studentSubject.setGroupSection(studentDetail.getGroupSection());
					//studentSubject.setStudentbatch(null);
					if (subjectregulation.getSubject() != null) {
						Subject subject = new Subject();
						subject.setSubjectId(subjectregulation.getSubject().getSubjectId());
						studentSubject.setSubject(subject);
						if (subjectregulation.getSubject().getSubjecttype() != null) {
							GeneralDetail subjectType = new GeneralDetail();
							subjectType.setGeneralDetailId(
									subjectregulation.getSubject().getSubjecttype().getGeneralDetailId());
							studentSubject.setSubjectType(subjectType);
						}
					}

					studentSubjects.add(studentSubject);
				}
			}
			
			if(!studentSubjects.isEmpty()) {
				studentSubjectRepository.saveAll(studentSubjects);
				studentSubjectRepository.flush();
				response = new ApiResponse<>(Messages.ADDED_SUCCESS.getValue(), null);
			}else {
				throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue());
			}
			
		} catch (Exception e) {
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue(), e);
		}
		return response;
	}

	@Override
	public ApiResponse<Long> addStudentSubjects(List<StudentDetailListDTO> studentDetailListDTOs) {
		return null;
	}

	@Override
	public ApiResponse<List<StudentSubjectDTO>> getStudentSubject(Long schoolId, Long academicYearId,Long courseYearId, Long subjectId) {
		LOGGER.info("StudentSubjectServiceImpl.getStudentSubject()");
		ApiResponse<List<StudentSubjectDTO>> response =null;
		List<StudentSubjectDTO> studentSubjectsDtos=null;
		List<StudentSubject> studentSubjects=null;
		String inSchool=Messages.INCOLLEGE.getValue();
		try {
			//fetching record from db with given values
			studentSubjects=studentSubjectRepository.getStudentSubject(schoolId, academicYearId, courseYearId, subjectId,inSchool);
			
			if(!(CollectionUtils.isEmpty(studentSubjects))) {
				studentSubjectsDtos=studentSubjectMapper.convertEntityListToDTOList(studentSubjects);
			}
			
			if(!(CollectionUtils.isEmpty(studentSubjects))) {
				response=new ApiResponse<List<StudentSubjectDTO>>(Boolean.TRUE,Messages.RETRIEVED_SUCCESS.getValue(), studentSubjectsDtos, HttpStatus.OK.value());
			}
			else {
				response=new ApiResponse<List<StudentSubjectDTO>>(Boolean.FALSE,Messages.RECORDS_NOT_FOUND.getValue(),HttpStatus.OK.value());
			}
			
		}
		catch (Exception e) {
			LOGGER.info("StudentSubjectServiceImpl.getStudentSubject()"+e);
			throw new ApiException(Messages.EXCEPTION_MESSAGE.getValue());
		}
		return response;
	}




}
