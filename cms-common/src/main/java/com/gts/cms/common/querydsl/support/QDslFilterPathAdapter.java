package com.gts.cms.common.querydsl.support;

public interface QDslFilterPathAdapter {
	String adaptPath(String path);
}
