package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

public class SmsOtherDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String salutation;
	private String message;
	private Map<String,MultipartFile> files;
	private Boolean messageStatus;

	public String getSalutation() {
		return salutation;
	}

	public String getMessage() {
		return message;
	}

	public Boolean getMessageStatus() {
		return messageStatus;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setMessageStatus(Boolean messageStatus) {
		this.messageStatus = messageStatus;
	}

	public Map<String, MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(Map<String, MultipartFile> files) {
		this.files = files;
	}

}
