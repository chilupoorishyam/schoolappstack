package com.gts.cms.common.dto;

import java.io.Serializable;

public class SmsToUsersDTO implements Serializable{
	private static final long serialVersionUID = 1L ;
	
	
	private Long userId;
	private String firstName;
	private String email;
	private String mobileNumber;
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	
	

}
