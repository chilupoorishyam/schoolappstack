package com.gts.cms.common.student.dto;

import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.Map;

public class StudentApplicationImagesDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long applicationId;
	private String applicationNumber;
	private String orgCode;
	private String schoolCode;
	private String admisssionNo;

//private MultipartFile studentPhotoFile;
//private String studentPhotoFileName;
	private String studentPhotoFileStatus;

	private Long admissionId;
//private MultipartFile fatherPhotoFile;
	private String fatherPhotoFileStatus;
	private String fatherPhotoFileName;

//private MultipartFile motherPhotoFile;
//private String motherPhotoFileName;
	private String motherPhotoFileStatus;

//private MultipartFile studentAadharFile;
//private String studentAadharFileName;
	private String studentAadharFileStatus;

//private MultipartFile spousePhotoFile;
//private String spousePhotoFileName;
	private String spousePhotoFileStatus;

//private MultipartFile studentPanCardFile;
//private String studentPanCardFileName;
	private String studentPanCardFileStatus;
	private String studentDocumentsStatus;
	private Long documentRepositoryId;

	private Map<String, MultipartFile> files;

	public String getFatherPhotoFileStatus() {
		return fatherPhotoFileStatus;
	}

	public void setFatherPhotoFileStatus(String fatherPhotoFileStatus) {
		this.fatherPhotoFileStatus = fatherPhotoFileStatus;
	}

	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	public String getApplicationNumber() {
		return applicationNumber;
	}

	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getStudentPhotoFileStatus() {
		return studentPhotoFileStatus;
	}

	public void setStudentPhotoFileStatus(String studentPhotoFileStatus) {
		this.studentPhotoFileStatus = studentPhotoFileStatus;
	}

	public Long getAdmissionId() {
		return admissionId;
	}

	public void setAdmissionId(Long admissionId) {
		this.admissionId = admissionId;
	}

	public String getFatherPhotoFileName() {
		return fatherPhotoFileName;
	}

	public void setFatherPhotoFileName(String fatherPhotoFileName) {
		this.fatherPhotoFileName = fatherPhotoFileName;
	}

	public String getMotherPhotoFileStatus() {
		return motherPhotoFileStatus;
	}

	public void setMotherPhotoFileStatus(String motherPhotoFileStatus) {
		this.motherPhotoFileStatus = motherPhotoFileStatus;
	}

	public String getStudentAadharFileStatus() {
		return studentAadharFileStatus;
	}

	public void setStudentAadharFileStatus(String studentAadharFileStatus) {
		this.studentAadharFileStatus = studentAadharFileStatus;
	}

	public String getStudentPanCardFileStatus() {
		return studentPanCardFileStatus;
	}

	public void setStudentPanCardFileStatus(String studentPanCardFileStatus) {
		this.studentPanCardFileStatus = studentPanCardFileStatus;
	}

	public String getStudentDocumentsStatus() {
		return studentDocumentsStatus;
	}

	public void setStudentDocumentsStatus(String studentDocumentsStatus) {
		this.studentDocumentsStatus = studentDocumentsStatus;
	}

	public Long getDocumentRepositoryId() {
		return documentRepositoryId;
	}

	public void setDocumentRepositoryId(Long documentRepositoryId) {
		this.documentRepositoryId = documentRepositoryId;
	}

	public Map<String, MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(Map<String, MultipartFile> files) {
		this.files = files;
	}

	public String getSpousePhotoFileStatus() {
		return spousePhotoFileStatus;
	}

	public void setSpousePhotoFileStatus(String spousePhotoFileStatus) {
		this.spousePhotoFileStatus = spousePhotoFileStatus;
	}

	public String getAdmisssionNo() {
		return admisssionNo;
	}

	public void setAdmisssionNo(String admisssionNo) {
		this.admisssionNo = admisssionNo;
	}
	
	

}
