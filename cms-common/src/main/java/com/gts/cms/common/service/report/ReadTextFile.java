package com.gts.cms.common.service.report;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;

/**
 * @author Genesis
 *
 */
public class ReadTextFile {

	public static synchronized String readTextFile(String filePath, String fileName) throws Exception {
		String path = "";
		if (filePath.lastIndexOf("/") == filePath.length() - 1)
			path = filePath + fileName;
		else
			path = filePath + "/" + fileName;

		return readTextFile(path);
	}

	public static synchronized String readTextFile(String str) throws Exception {
		File file = new File(str);

		return readTextFile(file);
	}

	public static synchronized String readTextFile(File file) throws Exception {

		StringBuilder builder = null;

		builder = new StringBuilder();

		if (file == null || !file.exists())
			throw new FileNotFoundException("Given File not Found");

		else {
			String extension = CommonUtils.getFileExtension(file);

			String[] extensions = { "html", "xhtml", "css", "xml", "log", "txt", "sql", "csv" };

			if (!Arrays.asList(extensions).contains(extension))
				throw new Exception(
						"File extension not valid. valid file extensions " + Arrays.asList(extensions).toString());

		}

		FileReader fileReader = new FileReader(file);
		BufferedReader buferReader = new BufferedReader(fileReader);

		String line;

		while ((line = buferReader.readLine()) != null)
			builder.append(line + "\n");

		return builder.toString();
	}
}
