package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

public class ProfileVisitDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long profileVisitId;
	private Date createdDt;
	private Long createdUser;
	private Long profileId;
	private String cityName;
	private String companyName;
	private String firstName;
	private Long visitorProfileId;
	private String visitorCityName;
	private String visitorCompanyName;
	private String visitorFirstName;
	private Boolean isActive;
	private String reason;
	private Date updatedDt;
	private Long updatedUser;
	private Date visitedTime;

	public void setProfileVisitId(Long profileVisitId) {
		this.profileVisitId = profileVisitId;
	}

	public Long getProfileVisitId() {
		return profileVisitId;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}

	public Long getCreatedUser() {
		return createdUser;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReason() {
		return reason;
	}

	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}

	public Long getUpdatedUser() {
		return updatedUser;
	}

	public void setVisitedTime(Date visitedTime) {
		this.visitedTime = visitedTime;
	}

	public Date getVisitedTime() {
		return visitedTime;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Long getVisitorProfileId() {
		return visitorProfileId;
	}

	public void setVisitorProfileId(Long visitorProfileId) {
		this.visitorProfileId = visitorProfileId;
	}

	public String getVisitorCityName() {
		return visitorCityName;
	}

	public void setVisitorCityName(String visitorCityName) {
		this.visitorCityName = visitorCityName;
	}

	public String getVisitorCompanyName() {
		return visitorCompanyName;
	}

	public void setVisitorCompanyName(String visitorCompanyName) {
		this.visitorCompanyName = visitorCompanyName;
	}

	public String getVisitorFirstName() {
		return visitorFirstName;
	}

	public void setVisitorFirstName(String visitorFirstName) {
		this.visitorFirstName = visitorFirstName;
	}

	public Long getProfileId() {
		return profileId;
	}

	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
}