package com.gts.cms.common.enums;

public enum UserRolesEnum {
	ROLE_ADMIN("ADMIN"), 
	ROLE_VERIFICATION_OFFICER("VERIFICATIONOFFICER");
	private String roleName;

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	private UserRolesEnum(String roleName) {
		this.roleName = roleName;
	}

}
