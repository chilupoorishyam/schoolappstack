package com.gts.cms.common.enums;

public enum PayslipGenerationEnum {
	PAYSLIP("PAYSLIP"); 
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	private PayslipGenerationEnum(String value) {
		this.value = value;
	}

}
