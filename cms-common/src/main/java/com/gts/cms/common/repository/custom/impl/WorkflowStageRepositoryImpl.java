package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.WorkflowStageRepositoryCustom;
import com.gts.cms.entity.QSchool;
import com.gts.cms.entity.QWorkflowStage;
import com.gts.cms.entity.WorkflowStage;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class WorkflowStageRepositoryImpl extends QuerydslRepositorySupport implements WorkflowStageRepositoryCustom {
	@PersistenceContext
	private EntityManager em;

	public WorkflowStageRepositoryImpl() {
		super(WorkflowStage.class);
	}

	@Override
	public WorkflowStage getWorkflowStageStatus(Long schoolId, String workflowCode, Integer stage) {

		return from(QWorkflowStage.workflowStage).join(QWorkflowStage.workflowStage.school, QSchool.school).where(QWorkflowStage.workflowStage.wfForCode.eq(workflowCode)
				.and(QWorkflowStage.workflowStage.wfStage.eq(stage)).and(QSchool.school.schoolId.eq(schoolId))).fetchOne();
	}

	@Override
	public WorkflowStage getworkflowStageByCode(Long schoolId, String workflowStageCode,String workflowForCode) {
		JPQLQuery<WorkflowStage> query = from(QWorkflowStage.workflowStage).where(QWorkflowStage.workflowStage.wfCode.eq(workflowStageCode)
				.and(QWorkflowStage.workflowStage.school.schoolId.eq(schoolId)).and(QWorkflowStage.workflowStage.isActive.eq(true)
						.and(QWorkflowStage.workflowStage.wfForCode.eq(workflowForCode))));
		return query.fetchOne();
	}
	
	@Override
	public WorkflowStage getworkflowForStageByCode(Long schoolId, String workflowForCode,String workflowStageCode) {
		JPQLQuery<WorkflowStage> query = from(QWorkflowStage.workflowStage).where(QWorkflowStage.workflowStage.wfCode.eq(workflowStageCode).and(QWorkflowStage.workflowStage.wfForCode.eq(workflowForCode))
				.and(QWorkflowStage.workflowStage.school.schoolId.eq(schoolId)).and(QWorkflowStage.workflowStage.isActive.eq(true)));
		return query.fetchOne();
	}

}
