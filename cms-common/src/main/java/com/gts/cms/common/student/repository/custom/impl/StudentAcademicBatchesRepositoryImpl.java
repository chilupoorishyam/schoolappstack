package com.gts.cms.common.student.repository.custom.impl;

import com.gts.cms.common.student.repository.custom.StudentAcademicBatchesRepositoryCustom;
import com.gts.cms.entity.StudentAcademicbatch;
import com.querydsl.jpa.JPQLQuery;
import org.apache.log4j.Logger;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import static com.gts.cms.entity.QStudentAcademicbatch.studentAcademicbatch;

/**
 * @author Genesis
 *
 */
@Repository
public class StudentAcademicBatchesRepositoryImpl extends QuerydslRepositorySupport
		implements StudentAcademicBatchesRepositoryCustom {
	private static final Logger LOGGER = Logger.getLogger(StudentAcademicBatchesRepositoryImpl.class);

	public StudentAcademicBatchesRepositoryImpl() {
		super(StudentAcademicBatchesRepositoryImpl.class);
	}

	public StudentAcademicbatch getDetails(Long schoolId, Long studentId) {
		JPQLQuery<StudentAcademicbatch> query = from(studentAcademicbatch).where(studentAcademicbatch.school.schoolId
				.eq(schoolId).and(studentAcademicbatch.studentDetail.studentId.eq(studentId))
				.and(studentAcademicbatch.isActive.eq(true)).and(studentAcademicbatch.toDate.isNull()));
		query.orderBy(studentAcademicbatch.createdDt.asc());
		return query.fetchFirst();

	}

	@Override
	public Long updateIsActive(Long studentAcademicbatchId, Boolean isActive) {
		return update(studentAcademicbatch)
				.where(studentAcademicbatch.studentAcademicbatchId.eq(studentAcademicbatchId))
				.set(studentAcademicbatch.isActive, isActive).execute();

	}

	@Override
	public Long updateSection(Long studentAcademicbatchId, Long groupSectionId) {
		return update(studentAcademicbatch)
				.where(studentAcademicbatch.studentAcademicbatchId.eq(studentAcademicbatchId))
				.set(studentAcademicbatch.toGroupSection.groupSectionId, groupSectionId).set(studentAcademicbatch.isActive, Boolean.FALSE).execute();
	}

}
