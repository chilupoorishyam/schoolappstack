package com.gts.cms.common.enums;

public enum SubjectTypeEnum {
	GENERALSUBJECT("GeneralSubject"), 
	LAB("Lab"), 
	ELECTIVE("ELECTIVE");
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	private SubjectTypeEnum(String value) {
		this.value = value;
	}

}
