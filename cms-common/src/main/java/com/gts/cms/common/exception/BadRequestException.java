package com.gts.cms.common.exception;

import org.springframework.validation.Errors;

/**
 * created by sathish on 02/09/2018.
 */
public class BadRequestException extends RuntimeException {

	private Errors errors;

	public BadRequestException(String message) {
		super(message);
	}

	public BadRequestException(String message, Errors errors) {
		super(message);
		this.errors = errors;
	}
	public BadRequestException(Errors errors) {
		this.errors = errors;
	}

	public Errors getErrors() {
		return errors;
	}
}
