package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.PageDTO;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.Module;
import com.gts.cms.entity.Page;
import com.gts.cms.entity.Submodule;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class PageMapper implements BaseMapper<Page, PageDTO> {
@Override
public PageDTO convertEntityToDTO(Page page) {
	PageDTO pageDTO = null;
	if (page != null) {
	pageDTO = new PageDTO();
	  pageDTO.setCreatedDt(page.getCreatedDt());
	  pageDTO.setUpdatedDt(page.getUpdatedDt());
	  pageDTO.setUpdatedUser(page.getUpdatedUser());
	  pageDTO.setIsActive(page.getIsActive());
	  pageDTO.setCreatedUser(page.getCreatedUser());
	  pageDTO.setUrl(page.getUrl());
	  pageDTO.setPageId(page.getPageId());
	  pageDTO.setPageCode(page.getPageCode());
	  pageDTO.setReason(page.getReason());
	  pageDTO.setPageNo(page.getPageNo());
	  pageDTO.setIconName(page.getIconName());
	  pageDTO.setSortOrder(page.getSortOrder());
	  pageDTO.setDisplayName(page.getDisplayName());
	  pageDTO.setPageName(page.getPageName());
	  if(page.getModule()!= null){
		  pageDTO.setModuleId(page.getModule().getModuleId());
		  pageDTO.setModuleName(page.getModule().getModuleName());
		 }
	  if(page.getSubmodule()!= null){
		  pageDTO.setSubmoduleId(page.getSubmodule().getSubmoduleId());
		  pageDTO.setSubmoduleName(page.getSubmodule().getSubmoduleName());
		}
	}
	return pageDTO;
}
@Override
public Page convertDTOtoEntity(PageDTO pageDTO,Page page) {
	if ( null == page ) {
	 page = new Page();
	 page.setIsActive(Status.ACTIVE.getId());
	 page.setCreatedDt(new Date());
	 page.setCreatedUser(SecurityUtil.getCurrentUser());
	}else{
	 page.setIsActive(pageDTO.getIsActive());
	}
	  page.setUpdatedDt(new Date());
	  page.setUpdatedUser(SecurityUtil.getCurrentUser());
	  page.setUrl(pageDTO.getUrl());
	 if(pageDTO.getModuleId()!= null){
		 Module module = new Module();
		 module.setModuleId(pageDTO.getModuleId());
		 page.setModule(module);
	 }
	 if(pageDTO.getSubmoduleId()!= null){
		 Submodule submodule = new Submodule();
		 submodule.setSubmoduleId(pageDTO.getSubmoduleId());
		 page.setSubmodule(submodule);
	 }
	  page.setPageId(pageDTO.getPageId());
	  page.setPageCode(pageDTO.getPageCode());
	  page.setReason(pageDTO.getReason());
	  page.setPageNo(pageDTO.getPageNo());
	  page.setIconName(pageDTO.getIconName());
	  page.setSortOrder(pageDTO.getSortOrder());
	  page.setDisplayName(pageDTO.getDisplayName());
	  page.setPageName(pageDTO.getPageName());

	return page;
}
@Override
public List<Page> convertDTOListToEntityList(List<PageDTO> pageDTOList) {
	List<Page> pageList = new ArrayList<>();
	 pageDTOList.forEach(pageDTO -> pageList.add(convertDTOtoEntity(pageDTO, null)));
	return pageList;
}
@Override
public List<PageDTO> convertEntityListToDTOList(List<Page> pageList) {
	List<PageDTO> pageDTOList = new ArrayList<>();
	 pageList.forEach(page -> pageDTOList.add(convertEntityToDTO(page)));
	return pageDTOList;
}
}