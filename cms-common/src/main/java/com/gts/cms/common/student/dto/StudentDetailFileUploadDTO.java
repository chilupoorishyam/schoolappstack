package com.gts.cms.common.student.dto;

import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.Map;

public class StudentDetailFileUploadDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long studentId;
	
	private String admissionNumber;
	
	private String orgCode;
	
	private String schoolCode;

	private String studentPhotoFileStatus;

	private String fatherPhotoFileStatus;

	private String motherPhotoFileStatus;

	private String studentAadharFileStatus;

	private String spousePhotoFileStatus;

	private String studentPanCardFileStatus;

	private String studentDocumentsStatus;

	private Long documentRepositoryId;
	
	private String studentAadharFileName ;
	private String studentPancardFileName ;
	private String studentPhotoFileName;
	private String fatherPhotoFileName;
	private String motherPhotoFileNamel;
	private String spousePhotoFileName;

	private Map<String, MultipartFile> files;

	public Long getStudentId() {
		return studentId;
	}

	public String getAdmissionNumber() {
		return admissionNumber;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public String getStudentPhotoFileStatus() {
		return studentPhotoFileStatus;
	}

	public String getFatherPhotoFileStatus() {
		return fatherPhotoFileStatus;
	}

	public String getMotherPhotoFileStatus() {
		return motherPhotoFileStatus;
	}

	public String getStudentAadharFileStatus() {
		return studentAadharFileStatus;
	}

	public String getSpousePhotoFileStatus() {
		return spousePhotoFileStatus;
	}

	public String getStudentPanCardFileStatus() {
		return studentPanCardFileStatus;
	}

	public String getStudentDocumentsStatus() {
		return studentDocumentsStatus;
	}

	public Long getDocumentRepositoryId() {
		return documentRepositoryId;
	}

	public Map<String, MultipartFile> getFiles() {
		return files;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public void setAdmissionNumber(String admissionNumber) {
		this.admissionNumber = admissionNumber;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public void setStudentPhotoFileStatus(String studentPhotoFileStatus) {
		this.studentPhotoFileStatus = studentPhotoFileStatus;
	}

	public void setFatherPhotoFileStatus(String fatherPhotoFileStatus) {
		this.fatherPhotoFileStatus = fatherPhotoFileStatus;
	}

	public void setMotherPhotoFileStatus(String motherPhotoFileStatus) {
		this.motherPhotoFileStatus = motherPhotoFileStatus;
	}

	public void setStudentAadharFileStatus(String studentAadharFileStatus) {
		this.studentAadharFileStatus = studentAadharFileStatus;
	}

	public void setSpousePhotoFileStatus(String spousePhotoFileStatus) {
		this.spousePhotoFileStatus = spousePhotoFileStatus;
	}

	public void setStudentPanCardFileStatus(String studentPanCardFileStatus) {
		this.studentPanCardFileStatus = studentPanCardFileStatus;
	}

	public void setStudentDocumentsStatus(String studentDocumentsStatus) {
		this.studentDocumentsStatus = studentDocumentsStatus;
	}

	public void setDocumentRepositoryId(Long documentRepositoryId) {
		this.documentRepositoryId = documentRepositoryId;
	}

	public void setFiles(Map<String, MultipartFile> files) {
		this.files = files;
	}

	public String getStudentAadharFileName() {
		return studentAadharFileName;
	}

	public void setStudentAadharFileName(String studentAadharFileName) {
		this.studentAadharFileName = studentAadharFileName;
	}

	public String getStudentPancardFileName() {
		return studentPancardFileName;
	}

	public void setStudentPancardFileName(String studentPancardFileName) {
		this.studentPancardFileName = studentPancardFileName;
	}

	public String getStudentPhotoFileName() {
		return studentPhotoFileName;
	}

	public void setStudentPhotoFileName(String studentPhotoFileName) {
		this.studentPhotoFileName = studentPhotoFileName;
	}

	public String getFatherPhotoFileName() {
		return fatherPhotoFileName;
	}

	public void setFatherPhotoFileName(String fatherPhotoFileName) {
		this.fatherPhotoFileName = fatherPhotoFileName;
	}

	public String getMotherPhotoFileNamel() {
		return motherPhotoFileNamel;
	}

	public void setMotherPhotoFileNamel(String motherPhotoFileNamel) {
		this.motherPhotoFileNamel = motherPhotoFileNamel;
	}

	public String getSpousePhotoFileName() {
		return spousePhotoFileName;
	}

	public void setSpousePhotoFileName(String spousePhotoFileName) {
		this.spousePhotoFileName = spousePhotoFileName;
	}
	
	

}
