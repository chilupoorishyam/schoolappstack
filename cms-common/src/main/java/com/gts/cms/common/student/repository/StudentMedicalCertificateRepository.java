package com.gts.cms.common.student.repository;

import com.gts.cms.common.student.repository.custom.StudentMedicalCertificateRepositoryCustom;
import com.gts.cms.entity.StudentMedicalCertificate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Genesis
 *
 */
@Repository
public interface StudentMedicalCertificateRepository
		extends JpaRepository<StudentMedicalCertificate, Long>, StudentMedicalCertificateRepositoryCustom {

}
