package com.gts.cms.common.repository.custom.impl;

import com.gts.cms.common.repository.custom.StateRepositoryCustom;
import com.gts.cms.entity.QState;
import com.gts.cms.entity.State;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * created by Naveen on 04/09/2018
 * 
 */
@Repository
public class StateRepositoryImpl extends QuerydslRepositorySupport implements StateRepositoryCustom {

	public StateRepositoryImpl() {
		super(State.class);
	}

	@PersistenceContext
	private EntityManager em;

	@Override
	@Transactional
	public Long deleteStateById(Long stateId) {
		return update(QState.state).where(QState.state.stateId.eq(stateId))
				.set(QState.state.isActive, false).execute();
	}

	@Override
	public List<State> findAllByCountryId(Long countryId) {
		JPQLQuery<State> query = from(QState.state)
				.where(QState.state.country.countryId.eq(countryId).and(QState.state.isActive.eq(true)));
		return query.fetch();
	}
}
