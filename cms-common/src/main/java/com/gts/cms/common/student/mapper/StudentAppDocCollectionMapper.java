package com.gts.cms.common.student.mapper;

import com.gts.cms.common.dto.StudentAppDocCollectionDTO;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.mapper.BaseMapper;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.DocumentRepository;
import com.gts.cms.entity.EmployeeDetail;
import com.gts.cms.entity.StudentAppDocCollection;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Genesis
 *
 */
@Component
public class StudentAppDocCollectionMapper implements BaseMapper<StudentAppDocCollection, StudentAppDocCollectionDTO> {
	@Override
	public StudentAppDocCollectionDTO convertEntityToDTO(StudentAppDocCollection studentAppDocCollection) {
		StudentAppDocCollectionDTO studentAppDocCollectionDTO = null;
		if (studentAppDocCollection != null) {
			studentAppDocCollectionDTO = new StudentAppDocCollectionDTO();

			studentAppDocCollectionDTO.setAppDocCollId(studentAppDocCollection.getAppDocCollId());
			
			if (studentAppDocCollection.getStudentApplication() != null) {
				studentAppDocCollectionDTO.setStudentAppId(studentAppDocCollection.getStudentApplication().getStudentAppId());
				studentAppDocCollectionDTO.setStudentFirstName(studentAppDocCollection.getStudentApplication().getFirstName());
			}
			
			if (studentAppDocCollection.getDocumentRepository() != null) {
				studentAppDocCollectionDTO.setDocumentRepositoryId(studentAppDocCollection.getDocumentRepository().getDocumentRepositoryId());
				studentAppDocCollectionDTO.setDocName(studentAppDocCollection.getDocumentRepository().getDocName());
			}

			studentAppDocCollectionDTO.setFilePath(studentAppDocCollection.getFilePath());
			studentAppDocCollectionDTO.setFileName(studentAppDocCollection.getFileName());
			studentAppDocCollectionDTO.setIsSoftCopy(studentAppDocCollection.getIsSoftCopy());
			studentAppDocCollectionDTO.setIsHardCopy(studentAppDocCollection.getIsHardCopy());
			studentAppDocCollectionDTO.setIsOriginal(studentAppDocCollection.getIsOriginal());
			studentAppDocCollectionDTO.setRackNumber(studentAppDocCollection.getRackNumber());
			studentAppDocCollectionDTO.setIsVerified(studentAppDocCollection.getIsVerified());

			if (studentAppDocCollection.getVerifiedbyEmployeeDetail() != null) {
				studentAppDocCollectionDTO.setVerfiedEmployeeId(studentAppDocCollection.getVerifiedbyEmployeeDetail().getEmployeeId());
				studentAppDocCollectionDTO.setVerifiedEmployeeName(studentAppDocCollection.getVerifiedbyEmployeeDetail().getFirstName());
			}
			studentAppDocCollectionDTO.setIsActive(studentAppDocCollection.getIsActive());
			studentAppDocCollectionDTO.setReason(studentAppDocCollection.getReason());
			studentAppDocCollectionDTO.setCreatedDt(studentAppDocCollection.getCreatedDt());
			studentAppDocCollectionDTO.setCreatedUser(studentAppDocCollection.getCreatedUser());
		}
		return studentAppDocCollectionDTO;
	}
	
	@Override
	public StudentAppDocCollection convertDTOtoEntity(StudentAppDocCollectionDTO studentAppDocCollectionDTO,
			StudentAppDocCollection studentAppDocCollection) {
		if (null == studentAppDocCollection) {
			studentAppDocCollection = new StudentAppDocCollection();
			studentAppDocCollection.setIsActive(Status.ACTIVE.getId());
			studentAppDocCollection.setCreatedDt(new Date());
			studentAppDocCollection.setCreatedUser(SecurityUtil.getCurrentUser());
		} else {
			studentAppDocCollection.setIsActive(studentAppDocCollectionDTO.getIsActive());
		}

		
		if (studentAppDocCollectionDTO.getDocumentRepositoryId() != null) {
			DocumentRepository documentRepository = new DocumentRepository();
			documentRepository.setDocumentRepositoryId(studentAppDocCollectionDTO.getDocumentRepositoryId());
			documentRepository.setDocName(studentAppDocCollectionDTO.getDocName());
			studentAppDocCollection.setDocumentRepository(documentRepository);
		}
		studentAppDocCollection.setFilePath(studentAppDocCollectionDTO.getFilePath());
		studentAppDocCollection.setFileName(studentAppDocCollectionDTO.getFileName());
		
		studentAppDocCollection.setIsSoftCopy(studentAppDocCollectionDTO.getIsSoftCopy());
		studentAppDocCollection.setIsHardCopy(studentAppDocCollectionDTO.getIsHardCopy());
		studentAppDocCollection.setIsOriginal(studentAppDocCollectionDTO.getIsOriginal());
		studentAppDocCollection.setRackNumber(studentAppDocCollectionDTO.getRackNumber());
		studentAppDocCollection.setIsVerified(studentAppDocCollectionDTO.getIsVerified());

		if (studentAppDocCollectionDTO.getVerfiedEmployeeId() != null) {
			EmployeeDetail employeeDetail = new EmployeeDetail();
			employeeDetail.setEmployeeId(studentAppDocCollectionDTO.getVerfiedEmployeeId());
			employeeDetail.setFirstName(studentAppDocCollectionDTO.getVerifiedEmployeeName());
			studentAppDocCollection.setVerifiedbyEmployeeDetail(employeeDetail);
		}
		
		studentAppDocCollection.setReason(studentAppDocCollectionDTO.getReason());

		return studentAppDocCollection;

	}

	@Override
	public List<StudentAppDocCollection> convertDTOListToEntityList(
			List<StudentAppDocCollectionDTO> studentAppDocCollectionDTOList) {
		List<StudentAppDocCollection> studentAppDocCollectionList = new ArrayList<>();
		if(studentAppDocCollectionDTOList != null) {
		studentAppDocCollectionDTOList.forEach(studentAppDocCollection -> studentAppDocCollectionList
				.add(convertDTOtoEntity(studentAppDocCollection,null)));
		}
		return studentAppDocCollectionList;

	}
	@Override
	public List<StudentAppDocCollectionDTO> convertEntityListToDTOList(
			List<StudentAppDocCollection> studentAppDocCollectionList) {
		List<StudentAppDocCollectionDTO> studentAppDocCollectionDTOList = new ArrayList<>();
		studentAppDocCollectionList.forEach(studentAppDocCollection -> studentAppDocCollectionDTOList
				.add(convertEntityToDTO(studentAppDocCollection)));
		return studentAppDocCollectionDTOList;
	}

}
