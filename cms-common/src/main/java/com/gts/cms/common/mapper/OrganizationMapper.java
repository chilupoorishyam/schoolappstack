package com.gts.cms.common.mapper;

import com.gts.cms.common.dto.OrganizationDTO;
import com.gts.cms.common.enums.Status;
import com.gts.cms.common.util.FileUtil;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class OrganizationMapper implements BaseMapper<Organization, OrganizationDTO> {

	@Override
	public OrganizationDTO convertEntityToDTO(Organization organization) {
		OrganizationDTO organizationDTO = null;
		if (organization != null) {
			organizationDTO = new OrganizationDTO();
			organizationDTO.setOrganizationId(organization.getOrganizationId());
			organizationDTO.setOrgName(organization.getOrgName());
			organizationDTO.setOrgCode(organization.getOrgCode());
			organizationDTO.setAddress(organization.getAddress());
			organizationDTO.setMobileNumber(organization.getMobileNumber());
			organizationDTO.setMandal(organization.getMandal());
			organizationDTO.setPincode(organization.getPincode());
			organizationDTO.setLogoFilename(organization.getLogoFilename());
			organizationDTO.setLogoPath(FileUtil.getAbsolutePath(organization.getLogoPath()));
			organizationDTO.setLandlineNumber(organization.getLandlineNumber());
			organizationDTO.setFax(organization.getFax());
			organizationDTO.setEmail(organization.getEmail());
			organizationDTO.setFacebookUrl(organization.getFacebookUrl());
			organizationDTO.setGoogleUrl(organization.getGoogleUrl());
			organizationDTO.setLinkedinUrl(organization.getLinkedinUrl());
			organizationDTO.setLicenseFdate(organization.getLicenseFdate());
			organizationDTO.setLicenseTdate(organization.getLicenseTdate());
			organizationDTO.setNoIssuedLicenses(organization.getNoIssuedLicenses());
			organizationDTO.setUrl(organization.getUrl());
			organizationDTO.setIsActive(organization.getIsActive());
			organizationDTO.setReason(organization.getReason());
			if (organization.getDistrict() != null) {
				organizationDTO.setDistrictId(organization.getDistrict().getDistrictId());
				organizationDTO.setDistrictName(organization.getDistrict().getDistrictName());
				if (organization.getDistrict().getState() != null) {
					organizationDTO.setStateId(organization.getDistrict().getState().getStateId());
					organizationDTO.setStateName(organization.getDistrict().getState().getStateName());
					if (organization.getDistrict().getState().getCountry() != null) {
						organizationDTO.setCountryId(organization.getDistrict().getState().getCountry().getCountryId());
						organizationDTO.setCountryName(organization.getDistrict().getState().getCountry().getCountryName());
					}
				}
			}
			if (organization.getCity() != null) {
				organizationDTO.setCityId(organization.getCity().getCityId());
				organizationDTO.setCityName(organization.getCity().getCityName());
			}
			
		}
		return organizationDTO;
	}

	@Override
	public Organization convertDTOtoEntity(OrganizationDTO organizationDto, Organization organization) {
		if (null == organization) {
			organization = new Organization();
			organization.setIsActive(Status.ACTIVE.getId());
			organization.setCreatedDt(new Date());
			organization.setCreatedUser(SecurityUtil.getCurrentUser());
		}else{
			organization.setIsActive(organizationDto.getIsActive());
		}
		organization.setOrganizationId(organizationDto.getOrganizationId());
		organization.setOrgName(organizationDto.getOrgName());
		organization.setOrgCode(organizationDto.getOrgCode());
		organization.setAddress(organizationDto.getAddress());
		organization.setMobileNumber(organizationDto.getMobileNumber());
		organization.setMandal(organizationDto.getMandal());
		organization.setPincode(organizationDto.getPincode());
		organization.setLogoFilename(organizationDto.getLogoFilename());
		organization.setLogoPath(FileUtil.getRelativePath(organizationDto.getLogoPath()));
		organization.setLandlineNumber(organizationDto.getLandlineNumber());
		organization.setFax(organizationDto.getFax());
		organization.setEmail(organizationDto.getEmail());
		organization.setFacebookUrl(organizationDto.getFacebookUrl());
		organization.setGoogleUrl(organizationDto.getGoogleUrl());
		organization.setLinkedinUrl(organizationDto.getLinkedinUrl());
		organization.setLicenseFdate(organizationDto.getLicenseFdate());
		organization.setLicenseTdate(organizationDto.getLicenseTdate());
		organization.setNoIssuedLicenses(organizationDto.getNoIssuedLicenses());
		organization.setUrl(organizationDto.getUrl());

		organization.setReason(organizationDto.getReason());
		organization.setUpdatedDt(new Date());
		organization.setUpdatedUser(SecurityUtil.getCurrentUser());
		if (organizationDto.getDistrictId() != null) {
			District district = new District();
			district.setDistrictId(organizationDto.getDistrictId());
			district.setDistrictName(organizationDto.getDistrictName());
			
			if (organizationDto.getStateId() != null) {
				State state=new State();
				 state.setStateId(organizationDto.getStateId());
				 state.setStateName(organizationDto.getStateName());
				  district.setState(state);
				if(organizationDto.getCountryId()!=null) {
					Country country=new Country();
					country.setCountryId(organizationDto.getCountryId());
					country.setCountryName(organizationDto.getCountryName());
					state.setCountry(country);
				}
			}
			
			organization.setDistrict(district);
		}
		if (organizationDto.getCityId() != null) {
			City city = new City();
			city.setCityId(organizationDto.getCityId());
			city.setCityName(organizationDto.getCityName());
			city.setDistrict(organization.getDistrict());
			organization.setCity(city);
		}
		
		
		
		
		return organization;
	}

	@Override
	public List<Organization> convertDTOListToEntityList(List<OrganizationDTO> organizationDTOList) {
		List<Organization> organizationList = new ArrayList<>();
		organizationDTOList
				.forEach(organizationDTO -> organizationList.add(convertDTOtoEntity(organizationDTO, null)));
		return organizationList;

	}

	@Override
	public List<OrganizationDTO> convertEntityListToDTOList(List<Organization> organizationList) {
		List<OrganizationDTO> organizationDTOList = new ArrayList<>();
		organizationList.forEach(organization -> organizationDTOList.add(convertEntityToDTO(organization)));
		return organizationDTOList;
	}


}
