package com.gts.cms.common.employee.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDetailStgDTO {

    private String organization;
    private String school;
    private String empNo;
    private String dateOfJoin;
    private String firstName;
    private String middleName;
    private String lastName;
    private String gender;
    private String nationality;
    private String dateOfBirth;
    private String mobileNumber;
    private String email;
    private String aadhaarNo;
    private String panNo;
    private String department;
    private String workingDepartment;
    private String qualification;
    private String designation;
    private String employeeCategory;
    private String address;
    private String city;
    private String district;
    @JsonIgnore
    private Map<String, MultipartFile> files;


}
