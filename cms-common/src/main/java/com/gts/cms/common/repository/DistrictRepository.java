package com.gts.cms.common.repository;

import com.gts.cms.common.repository.custom.DistrictRepositoryCustom;
import com.gts.cms.entity.District;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * created by Naveen(Auto) on 02/09/2018
 * 
 */
@Repository
public interface DistrictRepository extends JpaRepository<District, Long>, DistrictRepositoryCustom {
    Optional<District> findByDistrictName(String districtCode);
}
