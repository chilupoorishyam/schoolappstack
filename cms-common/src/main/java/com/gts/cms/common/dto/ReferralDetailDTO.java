package com.gts.cms.common.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ReferralDetailDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long referralDetailId;
	private Date createdDt;
	private Long createdUser;
	private Boolean isActive;
	private Boolean isTransactionSettled;
	private Boolean isstudentadmitted;
	private String notes;
	private BigDecimal paidAmount;
	private String reason;
	private BigDecimal referralAmount;
	private String statusComments;
	private Date statusDate;
	private Date transactionCompletedDate;
	private Date updatedDt;
	private Long updatedUser;
	private Long employeeId;
	private String employeeFirstName;
	private String employeeLastName;
	private String employeeMiddleName;
	private Long statusEmployeeId;
	private String statusEmployeeFirstName;
	private String statusEmployeeLastName;
	private String statusEmployeeMiddleName;
	private Long paidEmployeeId;
	private String paidEmployeeFirstName;
	private String paidEmployeeLastName;
	private String paidEmployeeMiddleName;
	private Long finTransactionId;
	private Long academicYearId;
	private String academicYear;
	private Long schoolId;
	private String schoolCode;
	private String schoolName;
	private Long approvalStatusId;
	private String approvalStatusDisplayName;
	private String approvalStatusCode;
	private Long agentdetialId;
	private String agentName;
	private String emailId;
	private Long enquiryId;
	private String studentName;
	
	public Long getReferralDetailId() {
		return referralDetailId;
	}
	public void setReferralDetailId(Long referralDetailId) {
		this.referralDetailId = referralDetailId;
	}
	public Date getCreatedDt() {
		return createdDt;
	}
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	public Long getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(Long createdUser) {
		this.createdUser = createdUser;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public Boolean getIsTransactionSettled() {
		return isTransactionSettled;
	}
	public void setIsTransactionSettled(Boolean isTransactionSettled) {
		this.isTransactionSettled = isTransactionSettled;
	}
	public Boolean getIsstudentadmitted() {
		return isstudentadmitted;
	}
	public void setIsstudentadmitted(Boolean isstudentadmitted) {
		this.isstudentadmitted = isstudentadmitted;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public BigDecimal getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public BigDecimal getReferralAmount() {
		return referralAmount;
	}
	public void setReferralAmount(BigDecimal referralAmount) {
		this.referralAmount = referralAmount;
	}
	public String getStatusComments() {
		return statusComments;
	}
	public void setStatusComments(String statusComments) {
		this.statusComments = statusComments;
	}
	public Date getStatusDate() {
		return statusDate;
	}
	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}
	public Date getTransactionCompletedDate() {
		return transactionCompletedDate;
	}
	public void setTransactionCompletedDate(Date transactionCompletedDate) {
		this.transactionCompletedDate = transactionCompletedDate;
	}
	public Date getUpdatedDt() {
		return updatedDt;
	}
	public void setUpdatedDt(Date updatedDt) {
		this.updatedDt = updatedDt;
	}
	public Long getUpdatedUser() {
		return updatedUser;
	}
	public void setUpdatedUser(Long updatedUser) {
		this.updatedUser = updatedUser;
	}
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeFirstName() {
		return employeeFirstName;
	}
	public void setEmployeeFirstName(String employeeFirstName) {
		this.employeeFirstName = employeeFirstName;
	}
	public String getEmployeeLastName() {
		return employeeLastName;
	}
	public void setEmployeeLastName(String employeeLastName) {
		this.employeeLastName = employeeLastName;
	}
	public String getEmployeeMiddleName() {
		return employeeMiddleName;
	}
	public void setEmployeeMiddleName(String employeeMiddleName) {
		this.employeeMiddleName = employeeMiddleName;
	}
	public Long getStatusEmployeeId() {
		return statusEmployeeId;
	}
	public void setStatusEmployeeId(Long statusEmployeeId) {
		this.statusEmployeeId = statusEmployeeId;
	}
	public String getStatusEmployeeFirstName() {
		return statusEmployeeFirstName;
	}
	public void setStatusEmployeeFirstName(String statusEmployeeFirstName) {
		this.statusEmployeeFirstName = statusEmployeeFirstName;
	}
	public String getStatusEmployeeLastName() {
		return statusEmployeeLastName;
	}
	public void setStatusEmployeeLastName(String statusEmployeeLastName) {
		this.statusEmployeeLastName = statusEmployeeLastName;
	}
	public String getStatusEmployeeMiddleName() {
		return statusEmployeeMiddleName;
	}
	public void setStatusEmployeeMiddleName(String statusEmployeeMiddleName) {
		this.statusEmployeeMiddleName = statusEmployeeMiddleName;
	}
	public Long getPaidEmployeeId() {
		return paidEmployeeId;
	}
	public void setPaidEmployeeId(Long paidEmployeeId) {
		this.paidEmployeeId = paidEmployeeId;
	}
	public String getPaidEmployeeFirstName() {
		return paidEmployeeFirstName;
	}
	public void setPaidEmployeeFirstName(String paidEmployeeFirstName) {
		this.paidEmployeeFirstName = paidEmployeeFirstName;
	}
	public String getPaidEmployeeLastName() {
		return paidEmployeeLastName;
	}
	public void setPaidEmployeeLastName(String paidEmployeeLastName) {
		this.paidEmployeeLastName = paidEmployeeLastName;
	}
	public String getPaidEmployeeMiddleName() {
		return paidEmployeeMiddleName;
	}
	public void setPaidEmployeeMiddleName(String paidEmployeeMiddleName) {
		this.paidEmployeeMiddleName = paidEmployeeMiddleName;
	}
	public Long getFinTransactionId() {
		return finTransactionId;
	}
	public void setFinTransactionId(Long finTransactionId) {
		this.finTransactionId = finTransactionId;
	}
	public Long getAcademicYearId() {
		return academicYearId;
	}
	public void setAcademicYearId(Long academicYearId) {
		this.academicYearId = academicYearId;
	}
	public Long getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}
	public String getSchoolCode() {
		return schoolCode;
	}
	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public Long getApprovalStatusId() {
		return approvalStatusId;
	}
	public void setApprovalStatusId(Long approvalStatusId) {
		this.approvalStatusId = approvalStatusId;
	}
	public String getApprovalStatusDisplayName() {
		return approvalStatusDisplayName;
	}
	public void setApprovalStatusDisplayName(String approvalStatusDisplayName) {
		this.approvalStatusDisplayName = approvalStatusDisplayName;
	}
	public String getApprovalStatusCode() {
		return approvalStatusCode;
	}
	public void setApprovalStatusCode(String approvalStatusCode) {
		this.approvalStatusCode = approvalStatusCode;
	}
	public Long getAgentdetialId() {
		return agentdetialId;
	}
	public void setAgentdetialId(Long agentdetialId) {
		this.agentdetialId = agentdetialId;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public Long getEnquiryId() {
		return enquiryId;
	}
	public void setEnquiryId(Long enquiryId) {
		this.enquiryId = enquiryId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getAcademicYear() {
		return academicYear;
	}
	public void setAcademicYear(String academicYear) {
		this.academicYear = academicYear;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	
	

}
