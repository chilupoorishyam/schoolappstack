package com.gts.cms.common.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

public class UsertypeDTO implements Serializable {
private static final long serialVersionUID = 1L;
private Long userTypeId;
private Date createdDt;
private Long createdUser;
@NotNull(message = "isActive is required.")
private Boolean isActive;
private String reason;
private Date updatedDt;
private Long updatedUser;
private String userTypeCode;
private String userTypeName;
@NotNull(message = "organizationId is required.")
private Long organizationId;
private String orgCode;
private String orgName;

public void setUserTypeId(Long userTypeId) {
 this.userTypeId = userTypeId;
}
public Long getUserTypeId(){
 return userTypeId;
}
public void setCreatedDt(Date createdDt) {
 this.createdDt = createdDt;
}
public Date getCreatedDt(){
 return createdDt;
}
public void setCreatedUser(Long createdUser) {
 this.createdUser = createdUser;
}
public Long getCreatedUser(){
 return createdUser;
}
public void setIsActive(Boolean isActive) {
 this.isActive = isActive;
}
public Boolean getIsActive(){
 return isActive;
}
public void setReason(String reason) {
 this.reason = reason;
}
public String getReason(){
 return reason;
}
public void setUpdatedDt(Date updatedDt) {
 this.updatedDt = updatedDt;
}
public Date getUpdatedDt(){
 return updatedDt;
}
public void setUpdatedUser(Long updatedUser) {
 this.updatedUser = updatedUser;
}
public Long getUpdatedUser(){
 return updatedUser;
}
public void setUserTypeCode(String userTypeCode) {
 this.userTypeCode = userTypeCode;
}
public String getUserTypeCode(){
 return userTypeCode;
}
public void setUserTypeName(String userTypeName) {
 this.userTypeName = userTypeName;
}
public String getUserTypeName(){
 return userTypeName;
}
public void setOrganizationId(Long organizationId) {
 this.organizationId = organizationId;
}
public Long getOrganizationId(){
 return organizationId;
}
public void setOrgCode(String orgCode) {
 this.orgCode = orgCode;
}
public String getOrgCode(){
 return orgCode;
}
public void setOrgName(String orgName) {
 this.orgName = orgName;
}
public String getOrgName(){
 return orgName;
}
}