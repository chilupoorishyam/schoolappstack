package com.gts.cms.common.repository;

import com.gts.cms.entity.CmProgramOutcome;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CmProgramOutcomeRepository extends JpaRepository<CmProgramOutcome, Long> {
}