package com.gts.cms.common.controller;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.PasswordDTO;
import com.gts.cms.common.employee.repository.EmployeeDetailRepository;
import com.gts.cms.common.enums.Messages;
import com.gts.cms.common.repository.AuthorizationRepository;
import com.gts.cms.common.repository.UserRepository;
import com.gts.cms.common.service.AuthorizationService;
import com.gts.cms.common.service.UserSecurityService;
import com.gts.cms.common.service.UserService;
import com.gts.cms.common.student.repository.StudentDetailRepository;
import com.gts.cms.common.util.SecurityUtil;
import com.gts.cms.entity.Authorization;
import com.gts.cms.entity.EmployeeDetail;
import com.gts.cms.entity.StudentDetail;
import com.gts.cms.entity.User;
import com.gts.cms.common.security.JwtTokenProvider;
import com.gts.cms.security.LoginRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    AuthorizationRepository authorizationRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;
    
    /*@Autowired 
    BCryptPasswordEncoder bCryptPasswordEncoder;*/

    @Autowired
    JwtTokenProvider tokenProvider;

    @Autowired
    EmployeeDetailRepository employeeDetailRepository;

    @Autowired
    StudentDetailRepository studentDetailRepository;

    @Autowired
    UserService userService;

    @Autowired
    UserSecurityService userSecurityService;

    @Autowired
    AuthorizationService authService;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest, HttpServletRequest request) {
        if (loginRequest.getUsernameOrEmail() == null || loginRequest.getUsernameOrEmail().trim() == "" || loginRequest.getPassword().trim() == "") {
            return ResponseEntity.ok(new ApiResponse<String>(Messages.InvalidUsernameorPassword.getValue(), ""));
        } else {
            User user = userRepository.findByUserNameAndPassword(loginRequest.getUsernameOrEmail(), loginRequest.getPassword());

            if (user == null)
                return ResponseEntity.ok(new ApiResponse<>(Boolean.FALSE, Messages.InvalidUsernameorPassword.getValue(), HttpStatus.OK.value()));

            String userType = null;
            if (user.getUserType() != null) {
                userType = user.getUserType().getUserTypeCode();
            }
            if (userType.equals(Messages.STAFF.getValue())) {
/*
                EmployeeDetail employeeDetail = employeeDetailRepository.findByUserId(user.getUserId(), Messages.STAFF.getValue());
*/
                EmployeeDetail employeeDetail = employeeDetailRepository.findByUserId(user.getUserId());
                if (employeeDetail == null) {
                    return ResponseEntity.ok(new ApiResponse<>(Boolean.FALSE, Messages.UNAUTHORIZED_USER.getValue(), HttpStatus.OK.value()));
                }
            } else if (userType.equals(Messages.PARENT.getValue())) {
                List<StudentDetail> parentDetail = studentDetailRepository.findByParentUserId(user.getUserId(), Messages.DISCONTINUED.getValue() + "," + Messages.PASSEDOUT.getValue());
                if (parentDetail == null) {
                    return ResponseEntity.ok(new ApiResponse<>(Boolean.FALSE, Messages.UNAUTHORIZED_USER.getValue(), HttpStatus.OK.value()));
                }
            } else if (userType.equals(Messages.STUDENT.getValue())) {
                StudentDetail studentDetail = studentDetailRepository.findByUserId(user.getUserId(), Messages.INCOLLEGE.getValue());
                if (studentDetail == null) {
                    return ResponseEntity.ok(new ApiResponse<>(Boolean.FALSE, Messages.UNAUTHORIZED_USER.getValue(), HttpStatus.OK.value()));
                }
            }
        }
        //loginRequest.setPassword(MD5Hashing.md5EncyptionUsingHex(loginRequest.getPassword()));
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.generateToken(authentication);
        Long userId = SecurityUtil.getCurrentUser();
        Authorization auth = new Authorization();
        auth.setIpAddress(getClientIp(request));
        auth.setUserAgent(request.getHeader("User-Agent"));
        auth.setAuthorizationKey(jwt);
        auth.setLoginTime(new Date());
        auth.setCreatedDt(new Date());
        User user = new User();
        user.setUserId(userId);
        auth.setUser(user);
        auth.setIsActive(Boolean.TRUE);
        authorizationRepository.save(auth);
        //Update last login details
        User loginUser = userRepository.findById(userId).get();
        if (loginRequest.getIsMobile() != null && !loginRequest.getIsMobile()) {
            loginUser.setLastLogin(new Date());
            userRepository.save(loginUser);
        } else if (loginRequest.getIsMobile() != null && loginRequest.getIsMobile()) {
            loginUser.setMobileAppLastLogin(new Date());
            loginUser.setMobileAppVersion(loginRequest.getAppVersion());
            userRepository.save(loginUser);
        }

        return ResponseEntity.ok(new ApiResponse<String>(Messages.LOGIN_SUCCESS.getValue(), jwt));
    }

    @PostMapping("/logout")
    public ResponseEntity<?> logoutUser(@Valid @RequestBody LoginRequest loginRequest, HttpServletRequest request) {
        authorizationRepository.logoutUser(loginRequest.getUserId(), loginRequest.getToken());
        return ResponseEntity.ok(new ApiResponse<String>("Session closed successfully.", ""));
    }

    private static String getClientIp(HttpServletRequest req) {
        String remoteAddr = "";
        if (req != null) {
            remoteAddr = req.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = req.getRemoteAddr();
            }
        }
        return remoteAddr;
    }

    @PostMapping("/resetPassword")
    public ResponseEntity<?> resetPassword(HttpServletRequest request,
                                           @RequestParam("usernameOrEmail") String usernameOrEmail) {
        try {
            PasswordDTO passwordDTO = null;
            User user = userRepository.findByUserNameOrEmail(usernameOrEmail);
            Predicate<User> pUser = u -> Objects.isNull(u);
            Predicate<String> pStr = str -> Objects.isNull(str);
            if (pUser.test(user)) {
                return ResponseEntity.ok(new ApiResponse<>(Boolean.FALSE, Messages.RESET_PWD_FAILED_EMAIL_NOT_REG.getValue(), HttpStatus.NOT_FOUND.value()));
            } else if (pUser.negate().test(user) && pStr.test(user.getEmail())) {
                return ResponseEntity.ok(new ApiResponse<>(Boolean.FALSE, Messages.RESET_PWD_FAILED_EMAIL_NOT_REG.getValue(), HttpStatus.NOT_FOUND.value()));
            } else {
                passwordDTO = new PasswordDTO();
                passwordDTO.setUserName(usernameOrEmail);
                userService.createPasswordResetTokenForUser(user);
            }
            return ResponseEntity.ok(new ApiResponse<>(Messages.RESET_PWD_LINK_SUCCESS.getValue(), passwordDTO));
        } catch (IncorrectResultSizeDataAccessException e) {
            return ResponseEntity.ok(new ApiResponse<>(Boolean.FALSE, "Email configured to multiple employees.", HttpStatus.BAD_REQUEST.value()));
        }
    }

    @PostMapping("/savePassword")
    public ResponseEntity<?> savePassword(@Valid @RequestBody PasswordDTO passwordDto) {

        Predicate<String> pStr = str -> Objects.nonNull(str);
        Predicate<User> pUser = user -> Objects.nonNull(user);
        Predicate<Object> pObj = obj -> Objects.nonNull(obj);
        if (pObj.test(passwordDto) && pStr.test(passwordDto.getNewPassword()) && pStr.test(passwordDto.getConfirmPassword()) && passwordDto.getNewPassword().trim().equals(passwordDto.getConfirmPassword().trim())) {
            final String result = userSecurityService.validatePasswordResetToken(passwordDto);
            if (pStr.test(result)) {
                return ResponseEntity.ok(new ApiResponse<>(result, ""));
            }

            User user = userService.getUserByPasswordResetToken(passwordDto);
            if (pUser.test(user)) {
                userService.changeUserPassword(user, passwordDto.getNewPassword());
                return ResponseEntity.ok(new ApiResponse<>(Messages.RESET_PWD_SUCCESS.getValue(), ""));
            } else {
                return ResponseEntity.ok(new ApiResponse<>(Messages.RESET_PWD_FAILED.getValue(), ""));
            }
        } else {
            return ResponseEntity.ok(new ApiResponse<>(Messages.SAVE_PWD_FAILED.getValue(), ""));
        }
    }


    @GetMapping("authdetails")
    public ApiResponse<?> getAuthDetails(
            @RequestParam(value = "loginTime", required = false) @DateTimeFormat(pattern = "yyyy/MM/dd") Date loginTime,
            @RequestParam(value = "logoutTime", required = false) @DateTimeFormat(pattern = "yyyy/MM/dd") Date logoutTime,
            @RequestPart(value = "userId", required = false) Long userId,
            @RequestPart(value = "isActive", required = false) Boolean isActive,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size) {
        return authService.getAuthDetails(loginTime, logoutTime, userId, isActive, page, size);
    }


}
