package com.gts.cms.common.controller;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.service.MinioService;
import com.gts.cms.videozoomapi.util.tribute.request.PostObjectRequest;
import com.gts.cms.videozoomapi.util.tribute.response.GetObjectResponse;
import com.gts.cms.videozoomapi.util.tribute.response.PostObjectResponse;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.hibernate.validator.constraints.URL;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@RestController
@RequestMapping("/storage")
@RequiredArgsConstructor
/* @Slf4j */
@Validated
public class StorageController {

    private final MinioService minioService;

    @PostMapping(value = "/upload", consumes = "multipart/form-data")
    @ResponseBody
    public GetObjectResponse uploadFile(@RequestParam("subject") String subject,
                                        @RequestParam(name = "file") MultipartFile file) {

        return minioService.uploadFile(subject, file);
    }

    @SneakyThrows
    @GetMapping(value = "/presignedUri")
    @ResponseBody
    public GetObjectResponse getObjectResponse(@RequestParam(value = "uri") @URL String uri) {
        String response = minioService.generateGetObjectRequest(uri);

        return new GetObjectResponse(response);
    }

    @SneakyThrows
    @PostMapping(value = "/presignedUri")
    @ResponseBody
    public PostObjectResponse postObjectResponse(@Valid @RequestBody PostObjectRequest request) {

        return minioService.generatePutObjectRequest(request);
    }

    @SneakyThrows
    @DeleteMapping
    @ResponseBody
    public ApiResponse<Long> presignedDeleteUri(@RequestParam(value = "uri") String uri,
                                                @RequestParam(value = "courseLessonTopicId") Long courseLessonTopicId) {

        return null;//minioService.deleteObjectAndUpdateTable(uri, courseLessonTopicId);
    }
    
    
    @GetMapping(value = "/hello")
    @ResponseBody
    public String testMethod() {
        return "Hello, world";
    }
    
    @PostMapping(value = "/uploadUnit", consumes = "multipart/form-data")
    @ResponseBody
    public GetObjectResponse uploadFile(@RequestParam("subject") String subject,
    									@RequestParam("unit") String unit,
                                        @RequestParam(name = "file") MultipartFile file) {

        return minioService.uploadFile(subject,unit, file);
    }
    
    @PostMapping(value = "/uploadUnitTopic", consumes = "multipart/form-data")
    @ResponseBody
    public GetObjectResponse uploadFile(@RequestParam("subject") String subject,
    									@RequestParam("unit") String unit,
    									@RequestParam("topic") String topic,
                                        @RequestParam(name = "file") MultipartFile file) {

        return minioService.uploadFile(subject,unit,topic, file);
    }

}

