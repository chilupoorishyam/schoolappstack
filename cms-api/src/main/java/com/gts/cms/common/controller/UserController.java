package com.gts.cms.common.controller;

import com.gts.cms.common.dto.*;
import com.gts.cms.common.service.UserService;
import com.gts.cms.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class UserController {
	@Autowired
	private UserService userService;

	@GetMapping("authorization")
	@ResponseBody
	public ApiResponse<UserDTO> getUserDetails(@RequestParam(value = "status",required=false) Boolean status,
											   @RequestParam(value = "isMobile",required=false) Boolean isMobile) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
		return userService.getUserDetails(userPrincipal.getId(),status, isMobile);

	}

	@GetMapping("userdetailsbytype")
	public ApiResponse<List<UserListsDTO>> getUserDetailsbyType(
			@RequestParam(value = "userTypeId",required=false) Long userTypeId,
			@RequestParam(value="userTypeCode",required=false)String userTypeCode,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "size", required = false) Integer size){
		return userService.getUserDetailsbyType(userTypeId,userTypeCode,page,size);
	}
	
	@PostMapping("createuser")
	public ApiResponse<?> createUser(@RequestBody UserDTO user) {
		return userService.createUser(user);

	}
	
	@GetMapping("userpages")
	@ResponseBody
	public ApiResponse<Set<PageDTO>> getUserPageDetails(@RequestParam(value = "userId",required=true) Long userId,
														@RequestParam(value = "status",required=false) Boolean status,
														@RequestParam(value = "isMobile",required=false) Boolean isMobile) {
		return userService.getUserPageDetails(userId,status, isMobile);

	}
	
	@GetMapping("buildInfo")
	@ResponseBody
	public Object buildCheck() {
		return userService.getBuildInfo();

	}
	
	@GetMapping("creatinguserforstudents")
	@ResponseBody
	public ApiResponse<?> creatingUserForStudents()
	{
		return userService.getcreatingUserForStudents();
		
	}
	@GetMapping("smstolimitUser")
	public ApiResponse<List<SmsToUsersDTO>> getsmsToLimitUser(
			                                @RequestParam(value="userTypeId",required=false)Long userTypeId){
		return userService.getsmsToLimitUser(userTypeId);
	}
	
	@GetMapping("useraccess")
	@ResponseBody
	public ApiResponse<UserDTO> getUserAcess(@RequestParam(value = "userId",required=false) Long userId,
											 @RequestParam(value = "status",required=false) Boolean status,
											 @RequestParam(value = "isMobile",required=false) Boolean isMobile) {
		
		return userService.getUserDetails(userId,status, isMobile);

	}
	
	@GetMapping("usersearch")
	public ApiResponse<?> searchUserDetails(
			@RequestParam(value = "schoolId", required = false) Long schoolId,
			@RequestParam(value = "userTypeId", required = false) Long userTypeId,
			@RequestParam(value = "status", required = false) Boolean status,
			@RequestParam(value = "q", required = true) String query) {
		return userService.searchUserDetails(schoolId,userTypeId,status,query);
	}
}
