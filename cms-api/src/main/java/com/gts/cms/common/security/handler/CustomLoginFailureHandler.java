package com.gts.cms.common.security.handler;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.json.util.JsonUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Component
public class CustomLoginFailureHandler implements AuthenticationFailureHandler {
	private static final Logger LOGGER = LogManager.getLogger(CustomLoginFailureHandler.class);
	
	public static final String INVALID_CREDENTIALS="Invalid Credentials";
 
	@Override
  public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse httpServletResponse, AuthenticationException exception)
      throws IOException, ServletException {
	  LOGGER.error("Responding with unauthorized error. Message - {}", exception.getMessage());
		ApiResponse<?> response = new ApiResponse<>(false, INVALID_CREDENTIALS, HttpStatus.UNAUTHORIZED.value());
		try {
			httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			httpServletResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);
			httpServletResponse.setCharacterEncoding(StandardCharsets.UTF_8.toString());
			httpServletResponse.getWriter().write(JsonUtil.javaToJson(response));
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		}
  }


}
