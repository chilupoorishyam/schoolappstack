package com.gts.cms.common.security;

import com.gts.cms.common.repository.AcademicYearRepository;
import com.gts.cms.common.repository.RolePrivilegeRepository;
import com.gts.cms.common.repository.UserRepository;
import com.gts.cms.common.util.OptionalUtil;
import com.gts.cms.entity.AcademicYear;
import com.gts.cms.entity.User;
import com.gts.cms.entity.UserRole;
import com.gts.cms.repo.dto.AuthorityRepoDTO;
import com.gts.cms.security.UserPrincipal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	private static Logger LOGGER = LogManager.getLogger(CustomUserDetailsService.class);
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RolePrivilegeRepository rolePrivilegeRepository;
	
	@Autowired
	private AcademicYearRepository academicYearRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
		User user = userRepository.findByUserNameOrEmail(usernameOrEmail, usernameOrEmail);
		if (user == null) {
			throw new UsernameNotFoundException("User not found with username or email : " + usernameOrEmail);
		}

		return createUserPrincipal(user);
	}

	@Transactional
	public UserDetails loadUserById(Long id) {

		User user = userRepository.findById(id)
				.orElseThrow(() -> new UsernameNotFoundException("User not found with id : " + id));

		return createUserPrincipal(user);

	}

	private UserPrincipal createUserPrincipal(User user) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		Long academicYearId = null;
		String academicYear = null;
		try {
			UserRole userRole = user.getUserRoles().get(0);
			Long roleId = userRole.getRole().getRoleId();
			Long orgId = user.getOrganization().getOrganizationId();
			Long schoolId = OptionalUtil.resolve(() -> user.getSchool().getSchoolId()).get();
			
			List<AcademicYear> academicYearList = academicYearRepository.getDefaultAcademicYear(schoolId);
			if(!academicYearList.isEmpty())
			{
				AcademicYear obj = academicYearList.get(0);
				academicYearId = obj.getAcademicYearId();
				academicYear = obj.getAcademicYear();
			}
			List<AuthorityRepoDTO> authorityRepoDTOs = rolePrivilegeRepository.getAuthoritiesForUser(roleId, orgId,
					schoolId);
			StringBuilder sb;
			for (AuthorityRepoDTO authorityRepoDTO : authorityRepoDTOs) {
				sb = new StringBuilder("ROLE_");
				Optional<String> pageCode = Optional.ofNullable(authorityRepoDTO.getPageCode());
				Optional<Boolean> canView = Optional.ofNullable(authorityRepoDTO.getCanView());
				Optional<Boolean> canAdd = Optional.ofNullable(authorityRepoDTO.getCanAdd());
				Optional<Boolean> canEdit = Optional.ofNullable(authorityRepoDTO.getCanEdit());
				Optional<Boolean> canDelete = Optional.ofNullable(authorityRepoDTO.getCanDelete());
				if (pageCode.isPresent()) {
					sb.append(pageCode.get()).append("_");
				}
				if (sb.length() > 1) {
					if (canView.isPresent() && canView.get()) {
						authorities.add(createAuthority(sb.toString() + "CANVIEW"));
					}

					if (canAdd.isPresent() && canAdd.get()) {
						authorities.add(createAuthority(sb.toString() + "CANADD"));
					}
					if (canEdit.isPresent() && canEdit.get()) {
						authorities.add(createAuthority(sb.toString() + "CANEDIT"));
					}
					if (canDelete.isPresent() && canDelete.get()) {
						authorities.add(createAuthority(sb.toString() + "CANDELETE"));
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while assigning privilages to the user :" + user.getUserId(), e);

		}
		authorities.add(new SimpleGrantedAuthority("USER"));
		Long roleId = null;
		String roleName=null;
		if (!user.getUserRoles().isEmpty() && user.getUserRoles().get(0).getRole().getIsActive()) {
			roleId = user.getUserRoles().get(0).getRole().getRoleId();
			roleName=user.getUserRoles().get(0).getRole().getRoleName();
		
		}	
		
		return new UserPrincipal(new Long(user.getUserId()), user.getFirstName(), user.getUserName(), user.getEmail(),
				user.getPassword(), authorities, user.getOrganization().getOrganizationId(),
				user.getOrganization().getOrgCode(),user.getOrganization().getOrgName(), user.getSchool().getSchoolId(),
				user.getSchool().getSchoolCode(),user.getSchool().getSchoolName(),user.getMobileNumber(), roleId,roleName,
				academicYearId,academicYear);
	}

	private GrantedAuthority createAuthority(String authority) {
		return new SimpleGrantedAuthority(authority);

	}

}
