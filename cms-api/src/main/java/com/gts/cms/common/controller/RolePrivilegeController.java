package com.gts.cms.common.controller;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.RolePrivilegeDTO;
import com.gts.cms.common.exception.BadRequestException;
import com.gts.cms.common.service.RolePrivilegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RolePrivilegeController {

	@Autowired
	RolePrivilegeService rolePrivilegeService;

	@PostMapping("roleprivilegelist")
	public ApiResponse<Long> addRolePrivilege(@RequestBody List<RolePrivilegeDTO> rolePrivilegeDTOs,
                                              BindingResult result) {
		if (result.hasErrors()) {
			throw new BadRequestException(result);
		}
		return rolePrivilegeService.addRolePrivilegeList(rolePrivilegeDTOs);
	}

}
