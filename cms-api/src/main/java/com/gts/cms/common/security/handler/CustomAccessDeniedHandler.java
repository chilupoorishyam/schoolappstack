package com.gts.cms.common.security.handler;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.json.util.JsonUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Component
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

	private static final Logger LOGGER = LogManager.getLogger(CustomAccessDeniedHandler.class);

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse httpServletResponse, AccessDeniedException exc)
			throws IOException, ServletException {
		LOGGER.error("Responding with unauthorized error. Message - {}", exc.getMessage());
		ApiResponse<?> response = new ApiResponse<>(false, "You do not have permission to access this resoure", HttpStatus.FORBIDDEN.value());
		try {
			httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
			httpServletResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);
			httpServletResponse.setCharacterEncoding(StandardCharsets.UTF_8.toString());
			httpServletResponse.getWriter().write(JsonUtil.javaToJson(response));
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage());
		}
	}
}