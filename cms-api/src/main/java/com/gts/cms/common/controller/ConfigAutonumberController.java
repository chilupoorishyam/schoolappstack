package com.gts.cms.common.controller;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.ConfigAutonumberDTO;
import com.gts.cms.common.exception.BadRequestException;
import com.gts.cms.common.service.ConfigAutoNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ConfigAutonumberController {

	@Autowired
	ConfigAutoNumberService configAutoNumberService;

	@PostMapping("configautonumbers")
	public ApiResponse<Long> addConfigAutoNumber(@Valid @RequestBody ConfigAutonumberDTO configAutoNumberDTO,
                                                 BindingResult result) {
		if (result.hasErrors()) {
			throw new BadRequestException(result);
		}
		return configAutoNumberService.addConfigAutoNumber(configAutoNumberDTO);
	}

	@GetMapping("configautonumbers/{id}")
	public ApiResponse<ConfigAutonumberDTO> getConfigAutoNumberDetails(@PathVariable(value = "id") Long autoconfigId) {
		return configAutoNumberService.getConfigAutoNumberDetails(autoconfigId);
	}

	@GetMapping("configautonumbers/{schoolId}/{configAtttributeCode}")
	public ApiResponse<ConfigAutonumberDTO> getAllSchoolConfigAutoNumbers(
			@PathVariable(value = "schoolId") Long schoolId, @PathVariable(value = "configAtttributeCode") String configAtttributeCode) {
		return configAutoNumberService.getAllSchoolConfigAutoNumbers(schoolId,configAtttributeCode);
	}
	
	@GetMapping("configautonumbers")
	public ApiResponse<List<ConfigAutonumberDTO>> getAllConfigAutoNumbers() {
		return configAutoNumberService.getAllConfigAutoNumbers();
	}

	@PutMapping("configautonumbers")
	public ApiResponse<Long> updateConfigAutoNumber(@Valid @RequestBody ConfigAutonumberDTO configAutoNumberDTO,
			BindingResult result) {
		if (result.hasErrors()) {
			throw new BadRequestException(result);
		}
		return configAutoNumberService.updateConfigAutoNumber(configAutoNumberDTO);
	}

	@DeleteMapping("configautonumbers/{id}")
	public ApiResponse<Long> deleteConfigAutoNumber(@PathVariable(value = "id") Long autoconfigId) {
		return configAutoNumberService.deleteConfigAutoNumber(autoconfigId);
	}
	
	@PostMapping("configautonumberslist")
	public ApiResponse<Long> addConfigAutoNumbers(@RequestBody List<ConfigAutonumberDTO> configAutoNumberDTOs,
			BindingResult result) {
		if (result.hasErrors()) {
			throw new BadRequestException(result);
		}
		return configAutoNumberService.addConfigAutoNumbersList(configAutoNumberDTOs);
	}

}
