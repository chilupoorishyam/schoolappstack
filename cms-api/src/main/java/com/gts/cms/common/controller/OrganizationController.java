package com.gts.cms.common.controller;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.OrganizationDTO;
import com.gts.cms.common.service.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.Map;

@RestController
public class OrganizationController {
    
    @Autowired
	OrganizationService organizationService;
    
	
   // @PreAuthorize("hasAnyRole(['ROLE_ORGANIZATION_CANEDIT','ROLE_ORGANIZATION_CANADD'])")
/*	@PostMapping("/uploadOrganizationLogo/{id}")
	public  ApiResponse<String> uploadFile(@RequestParam("file") MultipartFile file,@PathVariable(value="id") Long  organizationId) {
		return organizationService.uploadOrganizationLogo(file, organizationId);
		
	}
	*/
    @PostMapping("organizationlogoupload")
	public ApiResponse<OrganizationDTO> uploadFile(MultipartHttpServletRequest request) {
    	OrganizationDTO organizationDTO = new OrganizationDTO();
    	organizationDTO.setOrganizationId(new Long(request.getParameter("organizationId")));
    	organizationDTO.setOrgCode(new String(request.getParameter("orgCode")));
    	Map<String, MultipartFile> files = request.getFileMap();
		organizationDTO.setFiles(files);
		return organizationService.uploadOrganizationLogo(organizationDTO);
	}

    
}
