package com.gts.cms.common.controller;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.UserRoleDTO;
import com.gts.cms.common.exception.BadRequestException;
import com.gts.cms.common.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserRoleController {

	@Autowired
	UserRoleService userRoleService;

	@PostMapping("userrole")
	public ApiResponse<Long> addUserRole(@RequestBody List<UserRoleDTO> userRoleDTOs,
                                         BindingResult result) {
		if (result.hasErrors()) {
			throw new BadRequestException(result);
		}
		return userRoleService.addUserRole(userRoleDTOs);
	}

}
