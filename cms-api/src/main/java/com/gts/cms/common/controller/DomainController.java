package com.gts.cms.common.controller;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.service.DomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("domain/")
public class DomainController {

	@Autowired
	DomainService domainService;

	@RequestMapping(value = "get/{entityName}", method = RequestMethod.GET, produces = "application/json")
	public ApiResponse<?> handleGetRequest(@PathVariable String entityName, @RequestParam Map<String, String> params) {
		return domainService.findOneEntity(entityName, params);
	}

	@RequestMapping(value = "list/{entityName}", method = RequestMethod.GET, produces = "application/json")
	public ApiResponse<?> handleListResult(@PathVariable String entityName, @RequestParam Map<String, String> params) {
		return domainService.findEntityList(entityName, params);
	}

	@RequestMapping(value = "create/{entityName}", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public ApiResponse<?> handleCreateRequest(@PathVariable String entityName,
			@RequestBody Map<String, Object> wrappedRequestBody) {
		return domainService.createEntity(entityName, wrappedRequestBody);
	}
	
	@RequestMapping(value = "update/{entityName}", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
	public ApiResponse<?> handleUpdateRequest(@PathVariable String entityName,
			@RequestBody Map<String, Object> wrappedRequestBody,@RequestParam Map<String, String> params) {
		return domainService.updateEntity(entityName, wrappedRequestBody,params);
	}
	
	@RequestMapping(value = "delete/{entityName}", method = RequestMethod.DELETE, produces = "application/json")
	public ApiResponse<?> handleDeleteRequest(@PathVariable String entityName, @RequestParam Map<String, String> params) {
		return domainService.deleteOneEntity(entityName, params);
	}
	
	@RequestMapping(value = "search/{entityName}", method = RequestMethod.GET, produces = "application/json")
	public ApiResponse<?> handleSearchResult(@PathVariable String entityName, @RequestParam Map<String, String> params) {
		return domainService.searchEntityList(entityName, params);
	}
}
