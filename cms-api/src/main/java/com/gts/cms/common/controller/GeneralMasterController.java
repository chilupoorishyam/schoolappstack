package com.gts.cms.common.controller;

import com.gts.cms.common.dto.ApiResponse;
import com.gts.cms.common.dto.GeneralMasterDTO;
import com.gts.cms.common.exception.BadRequestException;
import com.gts.cms.common.service.GeneralMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class GeneralMasterController {

	@Autowired
	GeneralMasterService generalMasterService;

	@PostMapping("generalmasters")
	public ApiResponse<Long> addGeneralMaster(@Valid @RequestBody GeneralMasterDTO generalMaster,
                                              BindingResult result) {
		if (result.hasErrors()) {
			throw new BadRequestException(result);
		}
		return generalMasterService.addGeneralMaster(generalMaster);
	}

	@GetMapping("generalmasters/{id}")
	public ApiResponse<GeneralMasterDTO> getGeneralMasterDetails(@PathVariable(value = "id") Long generalMasterId) {
		return generalMasterService.getGeneralMasterDetails(generalMasterId);
	}

	@GetMapping("generalmasters")
	public ApiResponse<List<GeneralMasterDTO>> getAllGeneralMasterDetails() {
		return generalMasterService.getAllGeneralMasterDetails();
	}

	@PutMapping("generalmasters")
	public ApiResponse<Long> updateGeneralMaster(@Valid @RequestBody GeneralMasterDTO generalMaster,
			BindingResult result) {
		if (result.hasErrors()) {
			throw new BadRequestException(result);
		}
		return generalMasterService.updateGeneralMaster(generalMaster);
	}

	@DeleteMapping("generalmasters/{id}")
	public ApiResponse<Long> deleteGeneralMaster(@PathVariable(value = "id") Long generalMasterId) {
		return generalMasterService.deleteGeneralMaster(generalMasterId);
	}

}
